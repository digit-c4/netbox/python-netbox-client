import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_module_type_list import PaginatedModuleTypeList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    console_ports: Union[Unset, bool] = UNSET,
    console_server_ports: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    interfaces: Union[Unset, bool] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    manufacturer: Union[Unset, List[str]] = UNSET,
    manufacturer_n: Union[Unset, List[str]] = UNSET,
    manufacturer_id: Union[Unset, List[int]] = UNSET,
    manufacturer_id_n: Union[Unset, List[int]] = UNSET,
    model: Union[Unset, List[str]] = UNSET,
    model_empty: Union[Unset, bool] = UNSET,
    model_ic: Union[Unset, List[str]] = UNSET,
    model_ie: Union[Unset, List[str]] = UNSET,
    model_iew: Union[Unset, List[str]] = UNSET,
    model_isw: Union[Unset, List[str]] = UNSET,
    model_n: Union[Unset, List[str]] = UNSET,
    model_nic: Union[Unset, List[str]] = UNSET,
    model_nie: Union[Unset, List[str]] = UNSET,
    model_niew: Union[Unset, List[str]] = UNSET,
    model_nisw: Union[Unset, List[str]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    part_number: Union[Unset, List[str]] = UNSET,
    part_number_empty: Union[Unset, bool] = UNSET,
    part_number_ic: Union[Unset, List[str]] = UNSET,
    part_number_ie: Union[Unset, List[str]] = UNSET,
    part_number_iew: Union[Unset, List[str]] = UNSET,
    part_number_isw: Union[Unset, List[str]] = UNSET,
    part_number_n: Union[Unset, List[str]] = UNSET,
    part_number_nic: Union[Unset, List[str]] = UNSET,
    part_number_nie: Union[Unset, List[str]] = UNSET,
    part_number_niew: Union[Unset, List[str]] = UNSET,
    part_number_nisw: Union[Unset, List[str]] = UNSET,
    pass_through_ports: Union[Unset, bool] = UNSET,
    power_outlets: Union[Unset, bool] = UNSET,
    power_ports: Union[Unset, bool] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    weight: Union[Unset, List[float]] = UNSET,
    weight_empty: Union[Unset, bool] = UNSET,
    weight_gt: Union[Unset, List[float]] = UNSET,
    weight_gte: Union[Unset, List[float]] = UNSET,
    weight_lt: Union[Unset, List[float]] = UNSET,
    weight_lte: Union[Unset, List[float]] = UNSET,
    weight_n: Union[Unset, List[float]] = UNSET,
    weight_unit: Union[Unset, str] = UNSET,
    weight_unit_n: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["console_ports"] = console_ports

    params["console_server_ports"] = console_server_ports

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    json_description: Union[Unset, List[str]] = UNSET
    if not isinstance(description, Unset):
        json_description = description

    params["description"] = json_description

    params["description__empty"] = description_empty

    json_description_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ic, Unset):
        json_description_ic = description_ic

    params["description__ic"] = json_description_ic

    json_description_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ie, Unset):
        json_description_ie = description_ie

    params["description__ie"] = json_description_ie

    json_description_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_iew, Unset):
        json_description_iew = description_iew

    params["description__iew"] = json_description_iew

    json_description_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_isw, Unset):
        json_description_isw = description_isw

    params["description__isw"] = json_description_isw

    json_description_n: Union[Unset, List[str]] = UNSET
    if not isinstance(description_n, Unset):
        json_description_n = description_n

    params["description__n"] = json_description_n

    json_description_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nic, Unset):
        json_description_nic = description_nic

    params["description__nic"] = json_description_nic

    json_description_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nie, Unset):
        json_description_nie = description_nie

    params["description__nie"] = json_description_nie

    json_description_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_niew, Unset):
        json_description_niew = description_niew

    params["description__niew"] = json_description_niew

    json_description_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nisw, Unset):
        json_description_nisw = description_nisw

    params["description__nisw"] = json_description_nisw

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    params["interfaces"] = interfaces

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    params["limit"] = limit

    json_manufacturer: Union[Unset, List[str]] = UNSET
    if not isinstance(manufacturer, Unset):
        json_manufacturer = manufacturer

    params["manufacturer"] = json_manufacturer

    json_manufacturer_n: Union[Unset, List[str]] = UNSET
    if not isinstance(manufacturer_n, Unset):
        json_manufacturer_n = manufacturer_n

    params["manufacturer__n"] = json_manufacturer_n

    json_manufacturer_id: Union[Unset, List[int]] = UNSET
    if not isinstance(manufacturer_id, Unset):
        json_manufacturer_id = manufacturer_id

    params["manufacturer_id"] = json_manufacturer_id

    json_manufacturer_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(manufacturer_id_n, Unset):
        json_manufacturer_id_n = manufacturer_id_n

    params["manufacturer_id__n"] = json_manufacturer_id_n

    json_model: Union[Unset, List[str]] = UNSET
    if not isinstance(model, Unset):
        json_model = model

    params["model"] = json_model

    params["model__empty"] = model_empty

    json_model_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(model_ic, Unset):
        json_model_ic = model_ic

    params["model__ic"] = json_model_ic

    json_model_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(model_ie, Unset):
        json_model_ie = model_ie

    params["model__ie"] = json_model_ie

    json_model_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(model_iew, Unset):
        json_model_iew = model_iew

    params["model__iew"] = json_model_iew

    json_model_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(model_isw, Unset):
        json_model_isw = model_isw

    params["model__isw"] = json_model_isw

    json_model_n: Union[Unset, List[str]] = UNSET
    if not isinstance(model_n, Unset):
        json_model_n = model_n

    params["model__n"] = json_model_n

    json_model_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(model_nic, Unset):
        json_model_nic = model_nic

    params["model__nic"] = json_model_nic

    json_model_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(model_nie, Unset):
        json_model_nie = model_nie

    params["model__nie"] = json_model_nie

    json_model_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(model_niew, Unset):
        json_model_niew = model_niew

    params["model__niew"] = json_model_niew

    json_model_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(model_nisw, Unset):
        json_model_nisw = model_nisw

    params["model__nisw"] = json_model_nisw

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    params["offset"] = offset

    params["ordering"] = ordering

    json_part_number: Union[Unset, List[str]] = UNSET
    if not isinstance(part_number, Unset):
        json_part_number = part_number

    params["part_number"] = json_part_number

    params["part_number__empty"] = part_number_empty

    json_part_number_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(part_number_ic, Unset):
        json_part_number_ic = part_number_ic

    params["part_number__ic"] = json_part_number_ic

    json_part_number_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(part_number_ie, Unset):
        json_part_number_ie = part_number_ie

    params["part_number__ie"] = json_part_number_ie

    json_part_number_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(part_number_iew, Unset):
        json_part_number_iew = part_number_iew

    params["part_number__iew"] = json_part_number_iew

    json_part_number_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(part_number_isw, Unset):
        json_part_number_isw = part_number_isw

    params["part_number__isw"] = json_part_number_isw

    json_part_number_n: Union[Unset, List[str]] = UNSET
    if not isinstance(part_number_n, Unset):
        json_part_number_n = part_number_n

    params["part_number__n"] = json_part_number_n

    json_part_number_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(part_number_nic, Unset):
        json_part_number_nic = part_number_nic

    params["part_number__nic"] = json_part_number_nic

    json_part_number_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(part_number_nie, Unset):
        json_part_number_nie = part_number_nie

    params["part_number__nie"] = json_part_number_nie

    json_part_number_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(part_number_niew, Unset):
        json_part_number_niew = part_number_niew

    params["part_number__niew"] = json_part_number_niew

    json_part_number_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(part_number_nisw, Unset):
        json_part_number_nisw = part_number_nisw

    params["part_number__nisw"] = json_part_number_nisw

    params["pass_through_ports"] = pass_through_ports

    params["power_outlets"] = power_outlets

    params["power_ports"] = power_ports

    params["q"] = q

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    json_weight: Union[Unset, List[float]] = UNSET
    if not isinstance(weight, Unset):
        json_weight = weight

    params["weight"] = json_weight

    params["weight__empty"] = weight_empty

    json_weight_gt: Union[Unset, List[float]] = UNSET
    if not isinstance(weight_gt, Unset):
        json_weight_gt = weight_gt

    params["weight__gt"] = json_weight_gt

    json_weight_gte: Union[Unset, List[float]] = UNSET
    if not isinstance(weight_gte, Unset):
        json_weight_gte = weight_gte

    params["weight__gte"] = json_weight_gte

    json_weight_lt: Union[Unset, List[float]] = UNSET
    if not isinstance(weight_lt, Unset):
        json_weight_lt = weight_lt

    params["weight__lt"] = json_weight_lt

    json_weight_lte: Union[Unset, List[float]] = UNSET
    if not isinstance(weight_lte, Unset):
        json_weight_lte = weight_lte

    params["weight__lte"] = json_weight_lte

    json_weight_n: Union[Unset, List[float]] = UNSET
    if not isinstance(weight_n, Unset):
        json_weight_n = weight_n

    params["weight__n"] = json_weight_n

    params["weight_unit"] = weight_unit

    params["weight_unit__n"] = weight_unit_n

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/dcim/module-types/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedModuleTypeList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedModuleTypeList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedModuleTypeList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    console_ports: Union[Unset, bool] = UNSET,
    console_server_ports: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    interfaces: Union[Unset, bool] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    manufacturer: Union[Unset, List[str]] = UNSET,
    manufacturer_n: Union[Unset, List[str]] = UNSET,
    manufacturer_id: Union[Unset, List[int]] = UNSET,
    manufacturer_id_n: Union[Unset, List[int]] = UNSET,
    model: Union[Unset, List[str]] = UNSET,
    model_empty: Union[Unset, bool] = UNSET,
    model_ic: Union[Unset, List[str]] = UNSET,
    model_ie: Union[Unset, List[str]] = UNSET,
    model_iew: Union[Unset, List[str]] = UNSET,
    model_isw: Union[Unset, List[str]] = UNSET,
    model_n: Union[Unset, List[str]] = UNSET,
    model_nic: Union[Unset, List[str]] = UNSET,
    model_nie: Union[Unset, List[str]] = UNSET,
    model_niew: Union[Unset, List[str]] = UNSET,
    model_nisw: Union[Unset, List[str]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    part_number: Union[Unset, List[str]] = UNSET,
    part_number_empty: Union[Unset, bool] = UNSET,
    part_number_ic: Union[Unset, List[str]] = UNSET,
    part_number_ie: Union[Unset, List[str]] = UNSET,
    part_number_iew: Union[Unset, List[str]] = UNSET,
    part_number_isw: Union[Unset, List[str]] = UNSET,
    part_number_n: Union[Unset, List[str]] = UNSET,
    part_number_nic: Union[Unset, List[str]] = UNSET,
    part_number_nie: Union[Unset, List[str]] = UNSET,
    part_number_niew: Union[Unset, List[str]] = UNSET,
    part_number_nisw: Union[Unset, List[str]] = UNSET,
    pass_through_ports: Union[Unset, bool] = UNSET,
    power_outlets: Union[Unset, bool] = UNSET,
    power_ports: Union[Unset, bool] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    weight: Union[Unset, List[float]] = UNSET,
    weight_empty: Union[Unset, bool] = UNSET,
    weight_gt: Union[Unset, List[float]] = UNSET,
    weight_gte: Union[Unset, List[float]] = UNSET,
    weight_lt: Union[Unset, List[float]] = UNSET,
    weight_lte: Union[Unset, List[float]] = UNSET,
    weight_n: Union[Unset, List[float]] = UNSET,
    weight_unit: Union[Unset, str] = UNSET,
    weight_unit_n: Union[Unset, str] = UNSET,
) -> Response[PaginatedModuleTypeList]:
    """Get a list of module type objects.

    Args:
        console_ports (Union[Unset, bool]):
        console_server_ports (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        interfaces (Union[Unset, bool]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        manufacturer (Union[Unset, List[str]]):
        manufacturer_n (Union[Unset, List[str]]):
        manufacturer_id (Union[Unset, List[int]]):
        manufacturer_id_n (Union[Unset, List[int]]):
        model (Union[Unset, List[str]]):
        model_empty (Union[Unset, bool]):
        model_ic (Union[Unset, List[str]]):
        model_ie (Union[Unset, List[str]]):
        model_iew (Union[Unset, List[str]]):
        model_isw (Union[Unset, List[str]]):
        model_n (Union[Unset, List[str]]):
        model_nic (Union[Unset, List[str]]):
        model_nie (Union[Unset, List[str]]):
        model_niew (Union[Unset, List[str]]):
        model_nisw (Union[Unset, List[str]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        part_number (Union[Unset, List[str]]):
        part_number_empty (Union[Unset, bool]):
        part_number_ic (Union[Unset, List[str]]):
        part_number_ie (Union[Unset, List[str]]):
        part_number_iew (Union[Unset, List[str]]):
        part_number_isw (Union[Unset, List[str]]):
        part_number_n (Union[Unset, List[str]]):
        part_number_nic (Union[Unset, List[str]]):
        part_number_nie (Union[Unset, List[str]]):
        part_number_niew (Union[Unset, List[str]]):
        part_number_nisw (Union[Unset, List[str]]):
        pass_through_ports (Union[Unset, bool]):
        power_outlets (Union[Unset, bool]):
        power_ports (Union[Unset, bool]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        weight (Union[Unset, List[float]]):
        weight_empty (Union[Unset, bool]):
        weight_gt (Union[Unset, List[float]]):
        weight_gte (Union[Unset, List[float]]):
        weight_lt (Union[Unset, List[float]]):
        weight_lte (Union[Unset, List[float]]):
        weight_n (Union[Unset, List[float]]):
        weight_unit (Union[Unset, str]):
        weight_unit_n (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedModuleTypeList]
    """

    kwargs = _get_kwargs(
        console_ports=console_ports,
        console_server_ports=console_server_ports,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        interfaces=interfaces,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        manufacturer=manufacturer,
        manufacturer_n=manufacturer_n,
        manufacturer_id=manufacturer_id,
        manufacturer_id_n=manufacturer_id_n,
        model=model,
        model_empty=model_empty,
        model_ic=model_ic,
        model_ie=model_ie,
        model_iew=model_iew,
        model_isw=model_isw,
        model_n=model_n,
        model_nic=model_nic,
        model_nie=model_nie,
        model_niew=model_niew,
        model_nisw=model_nisw,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        part_number=part_number,
        part_number_empty=part_number_empty,
        part_number_ic=part_number_ic,
        part_number_ie=part_number_ie,
        part_number_iew=part_number_iew,
        part_number_isw=part_number_isw,
        part_number_n=part_number_n,
        part_number_nic=part_number_nic,
        part_number_nie=part_number_nie,
        part_number_niew=part_number_niew,
        part_number_nisw=part_number_nisw,
        pass_through_ports=pass_through_ports,
        power_outlets=power_outlets,
        power_ports=power_ports,
        q=q,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        weight=weight,
        weight_empty=weight_empty,
        weight_gt=weight_gt,
        weight_gte=weight_gte,
        weight_lt=weight_lt,
        weight_lte=weight_lte,
        weight_n=weight_n,
        weight_unit=weight_unit,
        weight_unit_n=weight_unit_n,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    console_ports: Union[Unset, bool] = UNSET,
    console_server_ports: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    interfaces: Union[Unset, bool] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    manufacturer: Union[Unset, List[str]] = UNSET,
    manufacturer_n: Union[Unset, List[str]] = UNSET,
    manufacturer_id: Union[Unset, List[int]] = UNSET,
    manufacturer_id_n: Union[Unset, List[int]] = UNSET,
    model: Union[Unset, List[str]] = UNSET,
    model_empty: Union[Unset, bool] = UNSET,
    model_ic: Union[Unset, List[str]] = UNSET,
    model_ie: Union[Unset, List[str]] = UNSET,
    model_iew: Union[Unset, List[str]] = UNSET,
    model_isw: Union[Unset, List[str]] = UNSET,
    model_n: Union[Unset, List[str]] = UNSET,
    model_nic: Union[Unset, List[str]] = UNSET,
    model_nie: Union[Unset, List[str]] = UNSET,
    model_niew: Union[Unset, List[str]] = UNSET,
    model_nisw: Union[Unset, List[str]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    part_number: Union[Unset, List[str]] = UNSET,
    part_number_empty: Union[Unset, bool] = UNSET,
    part_number_ic: Union[Unset, List[str]] = UNSET,
    part_number_ie: Union[Unset, List[str]] = UNSET,
    part_number_iew: Union[Unset, List[str]] = UNSET,
    part_number_isw: Union[Unset, List[str]] = UNSET,
    part_number_n: Union[Unset, List[str]] = UNSET,
    part_number_nic: Union[Unset, List[str]] = UNSET,
    part_number_nie: Union[Unset, List[str]] = UNSET,
    part_number_niew: Union[Unset, List[str]] = UNSET,
    part_number_nisw: Union[Unset, List[str]] = UNSET,
    pass_through_ports: Union[Unset, bool] = UNSET,
    power_outlets: Union[Unset, bool] = UNSET,
    power_ports: Union[Unset, bool] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    weight: Union[Unset, List[float]] = UNSET,
    weight_empty: Union[Unset, bool] = UNSET,
    weight_gt: Union[Unset, List[float]] = UNSET,
    weight_gte: Union[Unset, List[float]] = UNSET,
    weight_lt: Union[Unset, List[float]] = UNSET,
    weight_lte: Union[Unset, List[float]] = UNSET,
    weight_n: Union[Unset, List[float]] = UNSET,
    weight_unit: Union[Unset, str] = UNSET,
    weight_unit_n: Union[Unset, str] = UNSET,
) -> Optional[PaginatedModuleTypeList]:
    """Get a list of module type objects.

    Args:
        console_ports (Union[Unset, bool]):
        console_server_ports (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        interfaces (Union[Unset, bool]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        manufacturer (Union[Unset, List[str]]):
        manufacturer_n (Union[Unset, List[str]]):
        manufacturer_id (Union[Unset, List[int]]):
        manufacturer_id_n (Union[Unset, List[int]]):
        model (Union[Unset, List[str]]):
        model_empty (Union[Unset, bool]):
        model_ic (Union[Unset, List[str]]):
        model_ie (Union[Unset, List[str]]):
        model_iew (Union[Unset, List[str]]):
        model_isw (Union[Unset, List[str]]):
        model_n (Union[Unset, List[str]]):
        model_nic (Union[Unset, List[str]]):
        model_nie (Union[Unset, List[str]]):
        model_niew (Union[Unset, List[str]]):
        model_nisw (Union[Unset, List[str]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        part_number (Union[Unset, List[str]]):
        part_number_empty (Union[Unset, bool]):
        part_number_ic (Union[Unset, List[str]]):
        part_number_ie (Union[Unset, List[str]]):
        part_number_iew (Union[Unset, List[str]]):
        part_number_isw (Union[Unset, List[str]]):
        part_number_n (Union[Unset, List[str]]):
        part_number_nic (Union[Unset, List[str]]):
        part_number_nie (Union[Unset, List[str]]):
        part_number_niew (Union[Unset, List[str]]):
        part_number_nisw (Union[Unset, List[str]]):
        pass_through_ports (Union[Unset, bool]):
        power_outlets (Union[Unset, bool]):
        power_ports (Union[Unset, bool]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        weight (Union[Unset, List[float]]):
        weight_empty (Union[Unset, bool]):
        weight_gt (Union[Unset, List[float]]):
        weight_gte (Union[Unset, List[float]]):
        weight_lt (Union[Unset, List[float]]):
        weight_lte (Union[Unset, List[float]]):
        weight_n (Union[Unset, List[float]]):
        weight_unit (Union[Unset, str]):
        weight_unit_n (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedModuleTypeList
    """

    return sync_detailed(
        client=client,
        console_ports=console_ports,
        console_server_ports=console_server_ports,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        interfaces=interfaces,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        manufacturer=manufacturer,
        manufacturer_n=manufacturer_n,
        manufacturer_id=manufacturer_id,
        manufacturer_id_n=manufacturer_id_n,
        model=model,
        model_empty=model_empty,
        model_ic=model_ic,
        model_ie=model_ie,
        model_iew=model_iew,
        model_isw=model_isw,
        model_n=model_n,
        model_nic=model_nic,
        model_nie=model_nie,
        model_niew=model_niew,
        model_nisw=model_nisw,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        part_number=part_number,
        part_number_empty=part_number_empty,
        part_number_ic=part_number_ic,
        part_number_ie=part_number_ie,
        part_number_iew=part_number_iew,
        part_number_isw=part_number_isw,
        part_number_n=part_number_n,
        part_number_nic=part_number_nic,
        part_number_nie=part_number_nie,
        part_number_niew=part_number_niew,
        part_number_nisw=part_number_nisw,
        pass_through_ports=pass_through_ports,
        power_outlets=power_outlets,
        power_ports=power_ports,
        q=q,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        weight=weight,
        weight_empty=weight_empty,
        weight_gt=weight_gt,
        weight_gte=weight_gte,
        weight_lt=weight_lt,
        weight_lte=weight_lte,
        weight_n=weight_n,
        weight_unit=weight_unit,
        weight_unit_n=weight_unit_n,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    console_ports: Union[Unset, bool] = UNSET,
    console_server_ports: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    interfaces: Union[Unset, bool] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    manufacturer: Union[Unset, List[str]] = UNSET,
    manufacturer_n: Union[Unset, List[str]] = UNSET,
    manufacturer_id: Union[Unset, List[int]] = UNSET,
    manufacturer_id_n: Union[Unset, List[int]] = UNSET,
    model: Union[Unset, List[str]] = UNSET,
    model_empty: Union[Unset, bool] = UNSET,
    model_ic: Union[Unset, List[str]] = UNSET,
    model_ie: Union[Unset, List[str]] = UNSET,
    model_iew: Union[Unset, List[str]] = UNSET,
    model_isw: Union[Unset, List[str]] = UNSET,
    model_n: Union[Unset, List[str]] = UNSET,
    model_nic: Union[Unset, List[str]] = UNSET,
    model_nie: Union[Unset, List[str]] = UNSET,
    model_niew: Union[Unset, List[str]] = UNSET,
    model_nisw: Union[Unset, List[str]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    part_number: Union[Unset, List[str]] = UNSET,
    part_number_empty: Union[Unset, bool] = UNSET,
    part_number_ic: Union[Unset, List[str]] = UNSET,
    part_number_ie: Union[Unset, List[str]] = UNSET,
    part_number_iew: Union[Unset, List[str]] = UNSET,
    part_number_isw: Union[Unset, List[str]] = UNSET,
    part_number_n: Union[Unset, List[str]] = UNSET,
    part_number_nic: Union[Unset, List[str]] = UNSET,
    part_number_nie: Union[Unset, List[str]] = UNSET,
    part_number_niew: Union[Unset, List[str]] = UNSET,
    part_number_nisw: Union[Unset, List[str]] = UNSET,
    pass_through_ports: Union[Unset, bool] = UNSET,
    power_outlets: Union[Unset, bool] = UNSET,
    power_ports: Union[Unset, bool] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    weight: Union[Unset, List[float]] = UNSET,
    weight_empty: Union[Unset, bool] = UNSET,
    weight_gt: Union[Unset, List[float]] = UNSET,
    weight_gte: Union[Unset, List[float]] = UNSET,
    weight_lt: Union[Unset, List[float]] = UNSET,
    weight_lte: Union[Unset, List[float]] = UNSET,
    weight_n: Union[Unset, List[float]] = UNSET,
    weight_unit: Union[Unset, str] = UNSET,
    weight_unit_n: Union[Unset, str] = UNSET,
) -> Response[PaginatedModuleTypeList]:
    """Get a list of module type objects.

    Args:
        console_ports (Union[Unset, bool]):
        console_server_ports (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        interfaces (Union[Unset, bool]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        manufacturer (Union[Unset, List[str]]):
        manufacturer_n (Union[Unset, List[str]]):
        manufacturer_id (Union[Unset, List[int]]):
        manufacturer_id_n (Union[Unset, List[int]]):
        model (Union[Unset, List[str]]):
        model_empty (Union[Unset, bool]):
        model_ic (Union[Unset, List[str]]):
        model_ie (Union[Unset, List[str]]):
        model_iew (Union[Unset, List[str]]):
        model_isw (Union[Unset, List[str]]):
        model_n (Union[Unset, List[str]]):
        model_nic (Union[Unset, List[str]]):
        model_nie (Union[Unset, List[str]]):
        model_niew (Union[Unset, List[str]]):
        model_nisw (Union[Unset, List[str]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        part_number (Union[Unset, List[str]]):
        part_number_empty (Union[Unset, bool]):
        part_number_ic (Union[Unset, List[str]]):
        part_number_ie (Union[Unset, List[str]]):
        part_number_iew (Union[Unset, List[str]]):
        part_number_isw (Union[Unset, List[str]]):
        part_number_n (Union[Unset, List[str]]):
        part_number_nic (Union[Unset, List[str]]):
        part_number_nie (Union[Unset, List[str]]):
        part_number_niew (Union[Unset, List[str]]):
        part_number_nisw (Union[Unset, List[str]]):
        pass_through_ports (Union[Unset, bool]):
        power_outlets (Union[Unset, bool]):
        power_ports (Union[Unset, bool]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        weight (Union[Unset, List[float]]):
        weight_empty (Union[Unset, bool]):
        weight_gt (Union[Unset, List[float]]):
        weight_gte (Union[Unset, List[float]]):
        weight_lt (Union[Unset, List[float]]):
        weight_lte (Union[Unset, List[float]]):
        weight_n (Union[Unset, List[float]]):
        weight_unit (Union[Unset, str]):
        weight_unit_n (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedModuleTypeList]
    """

    kwargs = _get_kwargs(
        console_ports=console_ports,
        console_server_ports=console_server_ports,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        interfaces=interfaces,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        manufacturer=manufacturer,
        manufacturer_n=manufacturer_n,
        manufacturer_id=manufacturer_id,
        manufacturer_id_n=manufacturer_id_n,
        model=model,
        model_empty=model_empty,
        model_ic=model_ic,
        model_ie=model_ie,
        model_iew=model_iew,
        model_isw=model_isw,
        model_n=model_n,
        model_nic=model_nic,
        model_nie=model_nie,
        model_niew=model_niew,
        model_nisw=model_nisw,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        part_number=part_number,
        part_number_empty=part_number_empty,
        part_number_ic=part_number_ic,
        part_number_ie=part_number_ie,
        part_number_iew=part_number_iew,
        part_number_isw=part_number_isw,
        part_number_n=part_number_n,
        part_number_nic=part_number_nic,
        part_number_nie=part_number_nie,
        part_number_niew=part_number_niew,
        part_number_nisw=part_number_nisw,
        pass_through_ports=pass_through_ports,
        power_outlets=power_outlets,
        power_ports=power_ports,
        q=q,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        weight=weight,
        weight_empty=weight_empty,
        weight_gt=weight_gt,
        weight_gte=weight_gte,
        weight_lt=weight_lt,
        weight_lte=weight_lte,
        weight_n=weight_n,
        weight_unit=weight_unit,
        weight_unit_n=weight_unit_n,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    console_ports: Union[Unset, bool] = UNSET,
    console_server_ports: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    interfaces: Union[Unset, bool] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    manufacturer: Union[Unset, List[str]] = UNSET,
    manufacturer_n: Union[Unset, List[str]] = UNSET,
    manufacturer_id: Union[Unset, List[int]] = UNSET,
    manufacturer_id_n: Union[Unset, List[int]] = UNSET,
    model: Union[Unset, List[str]] = UNSET,
    model_empty: Union[Unset, bool] = UNSET,
    model_ic: Union[Unset, List[str]] = UNSET,
    model_ie: Union[Unset, List[str]] = UNSET,
    model_iew: Union[Unset, List[str]] = UNSET,
    model_isw: Union[Unset, List[str]] = UNSET,
    model_n: Union[Unset, List[str]] = UNSET,
    model_nic: Union[Unset, List[str]] = UNSET,
    model_nie: Union[Unset, List[str]] = UNSET,
    model_niew: Union[Unset, List[str]] = UNSET,
    model_nisw: Union[Unset, List[str]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    part_number: Union[Unset, List[str]] = UNSET,
    part_number_empty: Union[Unset, bool] = UNSET,
    part_number_ic: Union[Unset, List[str]] = UNSET,
    part_number_ie: Union[Unset, List[str]] = UNSET,
    part_number_iew: Union[Unset, List[str]] = UNSET,
    part_number_isw: Union[Unset, List[str]] = UNSET,
    part_number_n: Union[Unset, List[str]] = UNSET,
    part_number_nic: Union[Unset, List[str]] = UNSET,
    part_number_nie: Union[Unset, List[str]] = UNSET,
    part_number_niew: Union[Unset, List[str]] = UNSET,
    part_number_nisw: Union[Unset, List[str]] = UNSET,
    pass_through_ports: Union[Unset, bool] = UNSET,
    power_outlets: Union[Unset, bool] = UNSET,
    power_ports: Union[Unset, bool] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    weight: Union[Unset, List[float]] = UNSET,
    weight_empty: Union[Unset, bool] = UNSET,
    weight_gt: Union[Unset, List[float]] = UNSET,
    weight_gte: Union[Unset, List[float]] = UNSET,
    weight_lt: Union[Unset, List[float]] = UNSET,
    weight_lte: Union[Unset, List[float]] = UNSET,
    weight_n: Union[Unset, List[float]] = UNSET,
    weight_unit: Union[Unset, str] = UNSET,
    weight_unit_n: Union[Unset, str] = UNSET,
) -> Optional[PaginatedModuleTypeList]:
    """Get a list of module type objects.

    Args:
        console_ports (Union[Unset, bool]):
        console_server_ports (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        interfaces (Union[Unset, bool]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        manufacturer (Union[Unset, List[str]]):
        manufacturer_n (Union[Unset, List[str]]):
        manufacturer_id (Union[Unset, List[int]]):
        manufacturer_id_n (Union[Unset, List[int]]):
        model (Union[Unset, List[str]]):
        model_empty (Union[Unset, bool]):
        model_ic (Union[Unset, List[str]]):
        model_ie (Union[Unset, List[str]]):
        model_iew (Union[Unset, List[str]]):
        model_isw (Union[Unset, List[str]]):
        model_n (Union[Unset, List[str]]):
        model_nic (Union[Unset, List[str]]):
        model_nie (Union[Unset, List[str]]):
        model_niew (Union[Unset, List[str]]):
        model_nisw (Union[Unset, List[str]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        part_number (Union[Unset, List[str]]):
        part_number_empty (Union[Unset, bool]):
        part_number_ic (Union[Unset, List[str]]):
        part_number_ie (Union[Unset, List[str]]):
        part_number_iew (Union[Unset, List[str]]):
        part_number_isw (Union[Unset, List[str]]):
        part_number_n (Union[Unset, List[str]]):
        part_number_nic (Union[Unset, List[str]]):
        part_number_nie (Union[Unset, List[str]]):
        part_number_niew (Union[Unset, List[str]]):
        part_number_nisw (Union[Unset, List[str]]):
        pass_through_ports (Union[Unset, bool]):
        power_outlets (Union[Unset, bool]):
        power_ports (Union[Unset, bool]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        weight (Union[Unset, List[float]]):
        weight_empty (Union[Unset, bool]):
        weight_gt (Union[Unset, List[float]]):
        weight_gte (Union[Unset, List[float]]):
        weight_lt (Union[Unset, List[float]]):
        weight_lte (Union[Unset, List[float]]):
        weight_n (Union[Unset, List[float]]):
        weight_unit (Union[Unset, str]):
        weight_unit_n (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedModuleTypeList
    """

    return (
        await asyncio_detailed(
            client=client,
            console_ports=console_ports,
            console_server_ports=console_server_ports,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            description=description,
            description_empty=description_empty,
            description_ic=description_ic,
            description_ie=description_ie,
            description_iew=description_iew,
            description_isw=description_isw,
            description_n=description_n,
            description_nic=description_nic,
            description_nie=description_nie,
            description_niew=description_niew,
            description_nisw=description_nisw,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            interfaces=interfaces,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            limit=limit,
            manufacturer=manufacturer,
            manufacturer_n=manufacturer_n,
            manufacturer_id=manufacturer_id,
            manufacturer_id_n=manufacturer_id_n,
            model=model,
            model_empty=model_empty,
            model_ic=model_ic,
            model_ie=model_ie,
            model_iew=model_iew,
            model_isw=model_isw,
            model_n=model_n,
            model_nic=model_nic,
            model_nie=model_nie,
            model_niew=model_niew,
            model_nisw=model_nisw,
            modified_by_request=modified_by_request,
            offset=offset,
            ordering=ordering,
            part_number=part_number,
            part_number_empty=part_number_empty,
            part_number_ic=part_number_ic,
            part_number_ie=part_number_ie,
            part_number_iew=part_number_iew,
            part_number_isw=part_number_isw,
            part_number_n=part_number_n,
            part_number_nic=part_number_nic,
            part_number_nie=part_number_nie,
            part_number_niew=part_number_niew,
            part_number_nisw=part_number_nisw,
            pass_through_ports=pass_through_ports,
            power_outlets=power_outlets,
            power_ports=power_ports,
            q=q,
            tag=tag,
            tag_n=tag_n,
            updated_by_request=updated_by_request,
            weight=weight,
            weight_empty=weight_empty,
            weight_gt=weight_gt,
            weight_gte=weight_gte,
            weight_lt=weight_lt,
            weight_lte=weight_lte,
            weight_n=weight_n,
            weight_unit=weight_unit,
            weight_unit_n=weight_unit_n,
        )
    ).parsed
