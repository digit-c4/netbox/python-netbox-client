from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.device_with_config_context import DeviceWithConfigContext
from ...models.writable_device_with_config_context_request import (
    WritableDeviceWithConfigContextRequest,
)
from ...types import Response


def _get_kwargs(
    *,
    body: Union[
        WritableDeviceWithConfigContextRequest,
        WritableDeviceWithConfigContextRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}

    _kwargs: Dict[str, Any] = {
        "method": "post",
        "url": "/api/dcim/devices/",
    }

    if _request_content_type is None:
        _body = body.to_dict()

        _kwargs["json"] = _body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "application/json":
        _json_body = body.to_dict()

        _kwargs["json"] = _json_body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "multipart/form-data":
        _files_body = body.to_multipart()

        _kwargs["files"] = _files_body
        headers["Content-Type"] = "multipart/form-data; boundary=+++"

    else:
        raise ValueError("Content type application/json not supported by the server")

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[DeviceWithConfigContext]:
    if response.status_code == HTTPStatus.CREATED:
        response_201 = DeviceWithConfigContext.from_dict(response.json())

        return response_201
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[DeviceWithConfigContext]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    body: Union[
        WritableDeviceWithConfigContextRequest,
        WritableDeviceWithConfigContextRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Response[DeviceWithConfigContext]:
    """Post a list of device objects.

    Args:
        body (WritableDeviceWithConfigContextRequest): Adds support for custom fields and tags.
        body (WritableDeviceWithConfigContextRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[DeviceWithConfigContext]
    """

    kwargs = _get_kwargs(
        body=body,
        _request_content_type=_request_content_type,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    body: Union[
        WritableDeviceWithConfigContextRequest,
        WritableDeviceWithConfigContextRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[DeviceWithConfigContext]:
    """Post a list of device objects.

    Args:
        body (WritableDeviceWithConfigContextRequest): Adds support for custom fields and tags.
        body (WritableDeviceWithConfigContextRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        DeviceWithConfigContext
    """

    return sync_detailed(
        client=client,
        body=body,
        _request_content_type=_request_content_type,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    body: Union[
        WritableDeviceWithConfigContextRequest,
        WritableDeviceWithConfigContextRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Response[DeviceWithConfigContext]:
    """Post a list of device objects.

    Args:
        body (WritableDeviceWithConfigContextRequest): Adds support for custom fields and tags.
        body (WritableDeviceWithConfigContextRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[DeviceWithConfigContext]
    """

    kwargs = _get_kwargs(
        body=body,
        _request_content_type=_request_content_type,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    body: Union[
        WritableDeviceWithConfigContextRequest,
        WritableDeviceWithConfigContextRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[DeviceWithConfigContext]:
    """Post a list of device objects.

    Args:
        body (WritableDeviceWithConfigContextRequest): Adds support for custom fields and tags.
        body (WritableDeviceWithConfigContextRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        DeviceWithConfigContext
    """

    return (
        await asyncio_detailed(
            client=client,
            body=body,
            _request_content_type=_request_content_type,
        )
    ).parsed
