from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_cable_termination_list import PaginatedCableTerminationList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    cable: Union[Unset, int] = UNSET,
    cable_n: Union[Unset, int] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    termination_id: Union[Unset, List[int]] = UNSET,
    termination_id_empty: Union[Unset, bool] = UNSET,
    termination_id_gt: Union[Unset, List[int]] = UNSET,
    termination_id_gte: Union[Unset, List[int]] = UNSET,
    termination_id_lt: Union[Unset, List[int]] = UNSET,
    termination_id_lte: Union[Unset, List[int]] = UNSET,
    termination_id_n: Union[Unset, List[int]] = UNSET,
    termination_type: Union[Unset, str] = UNSET,
    termination_type_n: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["cable"] = cable

    params["cable__n"] = cable_n

    params["cable_end"] = cable_end

    params["cable_end__n"] = cable_end_n

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    params["limit"] = limit

    params["offset"] = offset

    params["ordering"] = ordering

    json_termination_id: Union[Unset, List[int]] = UNSET
    if not isinstance(termination_id, Unset):
        json_termination_id = termination_id

    params["termination_id"] = json_termination_id

    params["termination_id__empty"] = termination_id_empty

    json_termination_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(termination_id_gt, Unset):
        json_termination_id_gt = termination_id_gt

    params["termination_id__gt"] = json_termination_id_gt

    json_termination_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(termination_id_gte, Unset):
        json_termination_id_gte = termination_id_gte

    params["termination_id__gte"] = json_termination_id_gte

    json_termination_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(termination_id_lt, Unset):
        json_termination_id_lt = termination_id_lt

    params["termination_id__lt"] = json_termination_id_lt

    json_termination_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(termination_id_lte, Unset):
        json_termination_id_lte = termination_id_lte

    params["termination_id__lte"] = json_termination_id_lte

    json_termination_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(termination_id_n, Unset):
        json_termination_id_n = termination_id_n

    params["termination_id__n"] = json_termination_id_n

    params["termination_type"] = termination_type

    params["termination_type__n"] = termination_type_n

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/dcim/cable-terminations/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedCableTerminationList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedCableTerminationList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedCableTerminationList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    cable: Union[Unset, int] = UNSET,
    cable_n: Union[Unset, int] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    termination_id: Union[Unset, List[int]] = UNSET,
    termination_id_empty: Union[Unset, bool] = UNSET,
    termination_id_gt: Union[Unset, List[int]] = UNSET,
    termination_id_gte: Union[Unset, List[int]] = UNSET,
    termination_id_lt: Union[Unset, List[int]] = UNSET,
    termination_id_lte: Union[Unset, List[int]] = UNSET,
    termination_id_n: Union[Unset, List[int]] = UNSET,
    termination_type: Union[Unset, str] = UNSET,
    termination_type_n: Union[Unset, str] = UNSET,
) -> Response[PaginatedCableTerminationList]:
    """Get a list of cable termination objects.

    Args:
        cable (Union[Unset, int]):
        cable_n (Union[Unset, int]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        termination_id (Union[Unset, List[int]]):
        termination_id_empty (Union[Unset, bool]):
        termination_id_gt (Union[Unset, List[int]]):
        termination_id_gte (Union[Unset, List[int]]):
        termination_id_lt (Union[Unset, List[int]]):
        termination_id_lte (Union[Unset, List[int]]):
        termination_id_n (Union[Unset, List[int]]):
        termination_type (Union[Unset, str]):
        termination_type_n (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCableTerminationList]
    """

    kwargs = _get_kwargs(
        cable=cable,
        cable_n=cable_n,
        cable_end=cable_end,
        cable_end_n=cable_end_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        offset=offset,
        ordering=ordering,
        termination_id=termination_id,
        termination_id_empty=termination_id_empty,
        termination_id_gt=termination_id_gt,
        termination_id_gte=termination_id_gte,
        termination_id_lt=termination_id_lt,
        termination_id_lte=termination_id_lte,
        termination_id_n=termination_id_n,
        termination_type=termination_type,
        termination_type_n=termination_type_n,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    cable: Union[Unset, int] = UNSET,
    cable_n: Union[Unset, int] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    termination_id: Union[Unset, List[int]] = UNSET,
    termination_id_empty: Union[Unset, bool] = UNSET,
    termination_id_gt: Union[Unset, List[int]] = UNSET,
    termination_id_gte: Union[Unset, List[int]] = UNSET,
    termination_id_lt: Union[Unset, List[int]] = UNSET,
    termination_id_lte: Union[Unset, List[int]] = UNSET,
    termination_id_n: Union[Unset, List[int]] = UNSET,
    termination_type: Union[Unset, str] = UNSET,
    termination_type_n: Union[Unset, str] = UNSET,
) -> Optional[PaginatedCableTerminationList]:
    """Get a list of cable termination objects.

    Args:
        cable (Union[Unset, int]):
        cable_n (Union[Unset, int]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        termination_id (Union[Unset, List[int]]):
        termination_id_empty (Union[Unset, bool]):
        termination_id_gt (Union[Unset, List[int]]):
        termination_id_gte (Union[Unset, List[int]]):
        termination_id_lt (Union[Unset, List[int]]):
        termination_id_lte (Union[Unset, List[int]]):
        termination_id_n (Union[Unset, List[int]]):
        termination_type (Union[Unset, str]):
        termination_type_n (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCableTerminationList
    """

    return sync_detailed(
        client=client,
        cable=cable,
        cable_n=cable_n,
        cable_end=cable_end,
        cable_end_n=cable_end_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        offset=offset,
        ordering=ordering,
        termination_id=termination_id,
        termination_id_empty=termination_id_empty,
        termination_id_gt=termination_id_gt,
        termination_id_gte=termination_id_gte,
        termination_id_lt=termination_id_lt,
        termination_id_lte=termination_id_lte,
        termination_id_n=termination_id_n,
        termination_type=termination_type,
        termination_type_n=termination_type_n,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    cable: Union[Unset, int] = UNSET,
    cable_n: Union[Unset, int] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    termination_id: Union[Unset, List[int]] = UNSET,
    termination_id_empty: Union[Unset, bool] = UNSET,
    termination_id_gt: Union[Unset, List[int]] = UNSET,
    termination_id_gte: Union[Unset, List[int]] = UNSET,
    termination_id_lt: Union[Unset, List[int]] = UNSET,
    termination_id_lte: Union[Unset, List[int]] = UNSET,
    termination_id_n: Union[Unset, List[int]] = UNSET,
    termination_type: Union[Unset, str] = UNSET,
    termination_type_n: Union[Unset, str] = UNSET,
) -> Response[PaginatedCableTerminationList]:
    """Get a list of cable termination objects.

    Args:
        cable (Union[Unset, int]):
        cable_n (Union[Unset, int]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        termination_id (Union[Unset, List[int]]):
        termination_id_empty (Union[Unset, bool]):
        termination_id_gt (Union[Unset, List[int]]):
        termination_id_gte (Union[Unset, List[int]]):
        termination_id_lt (Union[Unset, List[int]]):
        termination_id_lte (Union[Unset, List[int]]):
        termination_id_n (Union[Unset, List[int]]):
        termination_type (Union[Unset, str]):
        termination_type_n (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCableTerminationList]
    """

    kwargs = _get_kwargs(
        cable=cable,
        cable_n=cable_n,
        cable_end=cable_end,
        cable_end_n=cable_end_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        offset=offset,
        ordering=ordering,
        termination_id=termination_id,
        termination_id_empty=termination_id_empty,
        termination_id_gt=termination_id_gt,
        termination_id_gte=termination_id_gte,
        termination_id_lt=termination_id_lt,
        termination_id_lte=termination_id_lte,
        termination_id_n=termination_id_n,
        termination_type=termination_type,
        termination_type_n=termination_type_n,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    cable: Union[Unset, int] = UNSET,
    cable_n: Union[Unset, int] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    termination_id: Union[Unset, List[int]] = UNSET,
    termination_id_empty: Union[Unset, bool] = UNSET,
    termination_id_gt: Union[Unset, List[int]] = UNSET,
    termination_id_gte: Union[Unset, List[int]] = UNSET,
    termination_id_lt: Union[Unset, List[int]] = UNSET,
    termination_id_lte: Union[Unset, List[int]] = UNSET,
    termination_id_n: Union[Unset, List[int]] = UNSET,
    termination_type: Union[Unset, str] = UNSET,
    termination_type_n: Union[Unset, str] = UNSET,
) -> Optional[PaginatedCableTerminationList]:
    """Get a list of cable termination objects.

    Args:
        cable (Union[Unset, int]):
        cable_n (Union[Unset, int]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        termination_id (Union[Unset, List[int]]):
        termination_id_empty (Union[Unset, bool]):
        termination_id_gt (Union[Unset, List[int]]):
        termination_id_gte (Union[Unset, List[int]]):
        termination_id_lt (Union[Unset, List[int]]):
        termination_id_lte (Union[Unset, List[int]]):
        termination_id_n (Union[Unset, List[int]]):
        termination_type (Union[Unset, str]):
        termination_type_n (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCableTerminationList
    """

    return (
        await asyncio_detailed(
            client=client,
            cable=cable,
            cable_n=cable_n,
            cable_end=cable_end,
            cable_end_n=cable_end_n,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            limit=limit,
            offset=offset,
            ordering=ordering,
            termination_id=termination_id,
            termination_id_empty=termination_id_empty,
            termination_id_gt=termination_id_gt,
            termination_id_gte=termination_id_gte,
            termination_id_lt=termination_id_lt,
            termination_id_lte=termination_id_lte,
            termination_id_n=termination_id_n,
            termination_type=termination_type,
            termination_type_n=termination_type_n,
        )
    ).parsed
