import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_power_feed_list import PaginatedPowerFeedList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    amperage: Union[Unset, List[int]] = UNSET,
    amperage_empty: Union[Unset, bool] = UNSET,
    amperage_gt: Union[Unset, List[int]] = UNSET,
    amperage_gte: Union[Unset, List[int]] = UNSET,
    amperage_lt: Union[Unset, List[int]] = UNSET,
    amperage_lte: Union[Unset, List[int]] = UNSET,
    amperage_n: Union[Unset, List[int]] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    cabled: Union[Unset, bool] = UNSET,
    connected: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    max_utilization: Union[Unset, List[int]] = UNSET,
    max_utilization_empty: Union[Unset, bool] = UNSET,
    max_utilization_gt: Union[Unset, List[int]] = UNSET,
    max_utilization_gte: Union[Unset, List[int]] = UNSET,
    max_utilization_lt: Union[Unset, List[int]] = UNSET,
    max_utilization_lte: Union[Unset, List[int]] = UNSET,
    max_utilization_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    occupied: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    phase: Union[Unset, str] = UNSET,
    phase_n: Union[Unset, str] = UNSET,
    power_panel_id: Union[Unset, List[int]] = UNSET,
    power_panel_id_n: Union[Unset, List[int]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    supply: Union[Unset, str] = UNSET,
    supply_n: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    type: Union[Unset, str] = UNSET,
    type_n: Union[Unset, str] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    voltage: Union[Unset, List[int]] = UNSET,
    voltage_empty: Union[Unset, bool] = UNSET,
    voltage_gt: Union[Unset, List[int]] = UNSET,
    voltage_gte: Union[Unset, List[int]] = UNSET,
    voltage_lt: Union[Unset, List[int]] = UNSET,
    voltage_lte: Union[Unset, List[int]] = UNSET,
    voltage_n: Union[Unset, List[int]] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_amperage: Union[Unset, List[int]] = UNSET
    if not isinstance(amperage, Unset):
        json_amperage = amperage

    params["amperage"] = json_amperage

    params["amperage__empty"] = amperage_empty

    json_amperage_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(amperage_gt, Unset):
        json_amperage_gt = amperage_gt

    params["amperage__gt"] = json_amperage_gt

    json_amperage_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(amperage_gte, Unset):
        json_amperage_gte = amperage_gte

    params["amperage__gte"] = json_amperage_gte

    json_amperage_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(amperage_lt, Unset):
        json_amperage_lt = amperage_lt

    params["amperage__lt"] = json_amperage_lt

    json_amperage_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(amperage_lte, Unset):
        json_amperage_lte = amperage_lte

    params["amperage__lte"] = json_amperage_lte

    json_amperage_n: Union[Unset, List[int]] = UNSET
    if not isinstance(amperage_n, Unset):
        json_amperage_n = amperage_n

    params["amperage__n"] = json_amperage_n

    params["cable_end"] = cable_end

    params["cable_end__n"] = cable_end_n

    params["cabled"] = cabled

    params["connected"] = connected

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    json_description: Union[Unset, List[str]] = UNSET
    if not isinstance(description, Unset):
        json_description = description

    params["description"] = json_description

    params["description__empty"] = description_empty

    json_description_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ic, Unset):
        json_description_ic = description_ic

    params["description__ic"] = json_description_ic

    json_description_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ie, Unset):
        json_description_ie = description_ie

    params["description__ie"] = json_description_ie

    json_description_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_iew, Unset):
        json_description_iew = description_iew

    params["description__iew"] = json_description_iew

    json_description_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_isw, Unset):
        json_description_isw = description_isw

    params["description__isw"] = json_description_isw

    json_description_n: Union[Unset, List[str]] = UNSET
    if not isinstance(description_n, Unset):
        json_description_n = description_n

    params["description__n"] = json_description_n

    json_description_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nic, Unset):
        json_description_nic = description_nic

    params["description__nic"] = json_description_nic

    json_description_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nie, Unset):
        json_description_nie = description_nie

    params["description__nie"] = json_description_nie

    json_description_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_niew, Unset):
        json_description_niew = description_niew

    params["description__niew"] = json_description_niew

    json_description_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nisw, Unset):
        json_description_nisw = description_nisw

    params["description__nisw"] = json_description_nisw

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    params["limit"] = limit

    json_max_utilization: Union[Unset, List[int]] = UNSET
    if not isinstance(max_utilization, Unset):
        json_max_utilization = max_utilization

    params["max_utilization"] = json_max_utilization

    params["max_utilization__empty"] = max_utilization_empty

    json_max_utilization_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(max_utilization_gt, Unset):
        json_max_utilization_gt = max_utilization_gt

    params["max_utilization__gt"] = json_max_utilization_gt

    json_max_utilization_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(max_utilization_gte, Unset):
        json_max_utilization_gte = max_utilization_gte

    params["max_utilization__gte"] = json_max_utilization_gte

    json_max_utilization_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(max_utilization_lt, Unset):
        json_max_utilization_lt = max_utilization_lt

    params["max_utilization__lt"] = json_max_utilization_lt

    json_max_utilization_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(max_utilization_lte, Unset):
        json_max_utilization_lte = max_utilization_lte

    params["max_utilization__lte"] = json_max_utilization_lte

    json_max_utilization_n: Union[Unset, List[int]] = UNSET
    if not isinstance(max_utilization_n, Unset):
        json_max_utilization_n = max_utilization_n

    params["max_utilization__n"] = json_max_utilization_n

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    json_name: Union[Unset, List[str]] = UNSET
    if not isinstance(name, Unset):
        json_name = name

    params["name"] = json_name

    params["name__empty"] = name_empty

    json_name_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ic, Unset):
        json_name_ic = name_ic

    params["name__ic"] = json_name_ic

    json_name_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ie, Unset):
        json_name_ie = name_ie

    params["name__ie"] = json_name_ie

    json_name_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_iew, Unset):
        json_name_iew = name_iew

    params["name__iew"] = json_name_iew

    json_name_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_isw, Unset):
        json_name_isw = name_isw

    params["name__isw"] = json_name_isw

    json_name_n: Union[Unset, List[str]] = UNSET
    if not isinstance(name_n, Unset):
        json_name_n = name_n

    params["name__n"] = json_name_n

    json_name_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nic, Unset):
        json_name_nic = name_nic

    params["name__nic"] = json_name_nic

    json_name_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nie, Unset):
        json_name_nie = name_nie

    params["name__nie"] = json_name_nie

    json_name_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_niew, Unset):
        json_name_niew = name_niew

    params["name__niew"] = json_name_niew

    json_name_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nisw, Unset):
        json_name_nisw = name_nisw

    params["name__nisw"] = json_name_nisw

    params["occupied"] = occupied

    params["offset"] = offset

    params["ordering"] = ordering

    params["phase"] = phase

    params["phase__n"] = phase_n

    json_power_panel_id: Union[Unset, List[int]] = UNSET
    if not isinstance(power_panel_id, Unset):
        json_power_panel_id = power_panel_id

    params["power_panel_id"] = json_power_panel_id

    json_power_panel_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(power_panel_id_n, Unset):
        json_power_panel_id_n = power_panel_id_n

    params["power_panel_id__n"] = json_power_panel_id_n

    params["q"] = q

    json_rack_id: Union[Unset, List[int]] = UNSET
    if not isinstance(rack_id, Unset):
        json_rack_id = rack_id

    params["rack_id"] = json_rack_id

    json_rack_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(rack_id_n, Unset):
        json_rack_id_n = rack_id_n

    params["rack_id__n"] = json_rack_id_n

    json_region: Union[Unset, List[int]] = UNSET
    if not isinstance(region, Unset):
        json_region = region

    params["region"] = json_region

    json_region_n: Union[Unset, List[int]] = UNSET
    if not isinstance(region_n, Unset):
        json_region_n = region_n

    params["region__n"] = json_region_n

    json_region_id: Union[Unset, List[int]] = UNSET
    if not isinstance(region_id, Unset):
        json_region_id = region_id

    params["region_id"] = json_region_id

    json_region_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(region_id_n, Unset):
        json_region_id_n = region_id_n

    params["region_id__n"] = json_region_id_n

    json_site: Union[Unset, List[str]] = UNSET
    if not isinstance(site, Unset):
        json_site = site

    params["site"] = json_site

    json_site_n: Union[Unset, List[str]] = UNSET
    if not isinstance(site_n, Unset):
        json_site_n = site_n

    params["site__n"] = json_site_n

    json_site_group: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group, Unset):
        json_site_group = site_group

    params["site_group"] = json_site_group

    json_site_group_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_n, Unset):
        json_site_group_n = site_group_n

    params["site_group__n"] = json_site_group_n

    json_site_group_id: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_id, Unset):
        json_site_group_id = site_group_id

    params["site_group_id"] = json_site_group_id

    json_site_group_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_id_n, Unset):
        json_site_group_id_n = site_group_id_n

    params["site_group_id__n"] = json_site_group_id_n

    json_site_id: Union[Unset, List[int]] = UNSET
    if not isinstance(site_id, Unset):
        json_site_id = site_id

    params["site_id"] = json_site_id

    json_site_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_id_n, Unset):
        json_site_id_n = site_id_n

    params["site_id__n"] = json_site_id_n

    json_status: Union[Unset, List[str]] = UNSET
    if not isinstance(status, Unset):
        json_status = status

    params["status"] = json_status

    json_status_n: Union[Unset, List[str]] = UNSET
    if not isinstance(status_n, Unset):
        json_status_n = status_n

    params["status__n"] = json_status_n

    params["supply"] = supply

    params["supply__n"] = supply_n

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_tenant: Union[Unset, List[str]] = UNSET
    if not isinstance(tenant, Unset):
        json_tenant = tenant

    params["tenant"] = json_tenant

    json_tenant_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tenant_n, Unset):
        json_tenant_n = tenant_n

    params["tenant__n"] = json_tenant_n

    json_tenant_group: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group, Unset):
        json_tenant_group = tenant_group

    params["tenant_group"] = json_tenant_group

    json_tenant_group_n: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group_n, Unset):
        json_tenant_group_n = tenant_group_n

    params["tenant_group__n"] = json_tenant_group_n

    json_tenant_group_id: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group_id, Unset):
        json_tenant_group_id = tenant_group_id

    params["tenant_group_id"] = json_tenant_group_id

    json_tenant_group_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group_id_n, Unset):
        json_tenant_group_id_n = tenant_group_id_n

    params["tenant_group_id__n"] = json_tenant_group_id_n

    json_tenant_id: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(tenant_id, Unset):
        json_tenant_id = []
        for tenant_id_item_data in tenant_id:
            tenant_id_item: Union[None, int]
            tenant_id_item = tenant_id_item_data
            json_tenant_id.append(tenant_id_item)

    params["tenant_id"] = json_tenant_id

    json_tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(tenant_id_n, Unset):
        json_tenant_id_n = []
        for tenant_id_n_item_data in tenant_id_n:
            tenant_id_n_item: Union[None, int]
            tenant_id_n_item = tenant_id_n_item_data
            json_tenant_id_n.append(tenant_id_n_item)

    params["tenant_id__n"] = json_tenant_id_n

    params["type"] = type

    params["type__n"] = type_n

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    json_voltage: Union[Unset, List[int]] = UNSET
    if not isinstance(voltage, Unset):
        json_voltage = voltage

    params["voltage"] = json_voltage

    params["voltage__empty"] = voltage_empty

    json_voltage_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(voltage_gt, Unset):
        json_voltage_gt = voltage_gt

    params["voltage__gt"] = json_voltage_gt

    json_voltage_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(voltage_gte, Unset):
        json_voltage_gte = voltage_gte

    params["voltage__gte"] = json_voltage_gte

    json_voltage_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(voltage_lt, Unset):
        json_voltage_lt = voltage_lt

    params["voltage__lt"] = json_voltage_lt

    json_voltage_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(voltage_lte, Unset):
        json_voltage_lte = voltage_lte

    params["voltage__lte"] = json_voltage_lte

    json_voltage_n: Union[Unset, List[int]] = UNSET
    if not isinstance(voltage_n, Unset):
        json_voltage_n = voltage_n

    params["voltage__n"] = json_voltage_n

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/dcim/power-feeds/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedPowerFeedList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedPowerFeedList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedPowerFeedList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    amperage: Union[Unset, List[int]] = UNSET,
    amperage_empty: Union[Unset, bool] = UNSET,
    amperage_gt: Union[Unset, List[int]] = UNSET,
    amperage_gte: Union[Unset, List[int]] = UNSET,
    amperage_lt: Union[Unset, List[int]] = UNSET,
    amperage_lte: Union[Unset, List[int]] = UNSET,
    amperage_n: Union[Unset, List[int]] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    cabled: Union[Unset, bool] = UNSET,
    connected: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    max_utilization: Union[Unset, List[int]] = UNSET,
    max_utilization_empty: Union[Unset, bool] = UNSET,
    max_utilization_gt: Union[Unset, List[int]] = UNSET,
    max_utilization_gte: Union[Unset, List[int]] = UNSET,
    max_utilization_lt: Union[Unset, List[int]] = UNSET,
    max_utilization_lte: Union[Unset, List[int]] = UNSET,
    max_utilization_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    occupied: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    phase: Union[Unset, str] = UNSET,
    phase_n: Union[Unset, str] = UNSET,
    power_panel_id: Union[Unset, List[int]] = UNSET,
    power_panel_id_n: Union[Unset, List[int]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    supply: Union[Unset, str] = UNSET,
    supply_n: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    type: Union[Unset, str] = UNSET,
    type_n: Union[Unset, str] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    voltage: Union[Unset, List[int]] = UNSET,
    voltage_empty: Union[Unset, bool] = UNSET,
    voltage_gt: Union[Unset, List[int]] = UNSET,
    voltage_gte: Union[Unset, List[int]] = UNSET,
    voltage_lt: Union[Unset, List[int]] = UNSET,
    voltage_lte: Union[Unset, List[int]] = UNSET,
    voltage_n: Union[Unset, List[int]] = UNSET,
) -> Response[PaginatedPowerFeedList]:
    """Get a list of power feed objects.

    Args:
        amperage (Union[Unset, List[int]]):
        amperage_empty (Union[Unset, bool]):
        amperage_gt (Union[Unset, List[int]]):
        amperage_gte (Union[Unset, List[int]]):
        amperage_lt (Union[Unset, List[int]]):
        amperage_lte (Union[Unset, List[int]]):
        amperage_n (Union[Unset, List[int]]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        cabled (Union[Unset, bool]):
        connected (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        max_utilization (Union[Unset, List[int]]):
        max_utilization_empty (Union[Unset, bool]):
        max_utilization_gt (Union[Unset, List[int]]):
        max_utilization_gte (Union[Unset, List[int]]):
        max_utilization_lt (Union[Unset, List[int]]):
        max_utilization_lte (Union[Unset, List[int]]):
        max_utilization_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        occupied (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        phase (Union[Unset, str]):
        phase_n (Union[Unset, str]):
        power_panel_id (Union[Unset, List[int]]):
        power_panel_id_n (Union[Unset, List[int]]):
        q (Union[Unset, str]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        supply (Union[Unset, str]):
        supply_n (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        type (Union[Unset, str]):
        type_n (Union[Unset, str]):
        updated_by_request (Union[Unset, UUID]):
        voltage (Union[Unset, List[int]]):
        voltage_empty (Union[Unset, bool]):
        voltage_gt (Union[Unset, List[int]]):
        voltage_gte (Union[Unset, List[int]]):
        voltage_lt (Union[Unset, List[int]]):
        voltage_lte (Union[Unset, List[int]]):
        voltage_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedPowerFeedList]
    """

    kwargs = _get_kwargs(
        amperage=amperage,
        amperage_empty=amperage_empty,
        amperage_gt=amperage_gt,
        amperage_gte=amperage_gte,
        amperage_lt=amperage_lt,
        amperage_lte=amperage_lte,
        amperage_n=amperage_n,
        cable_end=cable_end,
        cable_end_n=cable_end_n,
        cabled=cabled,
        connected=connected,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        max_utilization=max_utilization,
        max_utilization_empty=max_utilization_empty,
        max_utilization_gt=max_utilization_gt,
        max_utilization_gte=max_utilization_gte,
        max_utilization_lt=max_utilization_lt,
        max_utilization_lte=max_utilization_lte,
        max_utilization_n=max_utilization_n,
        modified_by_request=modified_by_request,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        occupied=occupied,
        offset=offset,
        ordering=ordering,
        phase=phase,
        phase_n=phase_n,
        power_panel_id=power_panel_id,
        power_panel_id_n=power_panel_id_n,
        q=q,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        status=status,
        status_n=status_n,
        supply=supply,
        supply_n=supply_n,
        tag=tag,
        tag_n=tag_n,
        tenant=tenant,
        tenant_n=tenant_n,
        tenant_group=tenant_group,
        tenant_group_n=tenant_group_n,
        tenant_group_id=tenant_group_id,
        tenant_group_id_n=tenant_group_id_n,
        tenant_id=tenant_id,
        tenant_id_n=tenant_id_n,
        type=type,
        type_n=type_n,
        updated_by_request=updated_by_request,
        voltage=voltage,
        voltage_empty=voltage_empty,
        voltage_gt=voltage_gt,
        voltage_gte=voltage_gte,
        voltage_lt=voltage_lt,
        voltage_lte=voltage_lte,
        voltage_n=voltage_n,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    amperage: Union[Unset, List[int]] = UNSET,
    amperage_empty: Union[Unset, bool] = UNSET,
    amperage_gt: Union[Unset, List[int]] = UNSET,
    amperage_gte: Union[Unset, List[int]] = UNSET,
    amperage_lt: Union[Unset, List[int]] = UNSET,
    amperage_lte: Union[Unset, List[int]] = UNSET,
    amperage_n: Union[Unset, List[int]] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    cabled: Union[Unset, bool] = UNSET,
    connected: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    max_utilization: Union[Unset, List[int]] = UNSET,
    max_utilization_empty: Union[Unset, bool] = UNSET,
    max_utilization_gt: Union[Unset, List[int]] = UNSET,
    max_utilization_gte: Union[Unset, List[int]] = UNSET,
    max_utilization_lt: Union[Unset, List[int]] = UNSET,
    max_utilization_lte: Union[Unset, List[int]] = UNSET,
    max_utilization_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    occupied: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    phase: Union[Unset, str] = UNSET,
    phase_n: Union[Unset, str] = UNSET,
    power_panel_id: Union[Unset, List[int]] = UNSET,
    power_panel_id_n: Union[Unset, List[int]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    supply: Union[Unset, str] = UNSET,
    supply_n: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    type: Union[Unset, str] = UNSET,
    type_n: Union[Unset, str] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    voltage: Union[Unset, List[int]] = UNSET,
    voltage_empty: Union[Unset, bool] = UNSET,
    voltage_gt: Union[Unset, List[int]] = UNSET,
    voltage_gte: Union[Unset, List[int]] = UNSET,
    voltage_lt: Union[Unset, List[int]] = UNSET,
    voltage_lte: Union[Unset, List[int]] = UNSET,
    voltage_n: Union[Unset, List[int]] = UNSET,
) -> Optional[PaginatedPowerFeedList]:
    """Get a list of power feed objects.

    Args:
        amperage (Union[Unset, List[int]]):
        amperage_empty (Union[Unset, bool]):
        amperage_gt (Union[Unset, List[int]]):
        amperage_gte (Union[Unset, List[int]]):
        amperage_lt (Union[Unset, List[int]]):
        amperage_lte (Union[Unset, List[int]]):
        amperage_n (Union[Unset, List[int]]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        cabled (Union[Unset, bool]):
        connected (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        max_utilization (Union[Unset, List[int]]):
        max_utilization_empty (Union[Unset, bool]):
        max_utilization_gt (Union[Unset, List[int]]):
        max_utilization_gte (Union[Unset, List[int]]):
        max_utilization_lt (Union[Unset, List[int]]):
        max_utilization_lte (Union[Unset, List[int]]):
        max_utilization_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        occupied (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        phase (Union[Unset, str]):
        phase_n (Union[Unset, str]):
        power_panel_id (Union[Unset, List[int]]):
        power_panel_id_n (Union[Unset, List[int]]):
        q (Union[Unset, str]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        supply (Union[Unset, str]):
        supply_n (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        type (Union[Unset, str]):
        type_n (Union[Unset, str]):
        updated_by_request (Union[Unset, UUID]):
        voltage (Union[Unset, List[int]]):
        voltage_empty (Union[Unset, bool]):
        voltage_gt (Union[Unset, List[int]]):
        voltage_gte (Union[Unset, List[int]]):
        voltage_lt (Union[Unset, List[int]]):
        voltage_lte (Union[Unset, List[int]]):
        voltage_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedPowerFeedList
    """

    return sync_detailed(
        client=client,
        amperage=amperage,
        amperage_empty=amperage_empty,
        amperage_gt=amperage_gt,
        amperage_gte=amperage_gte,
        amperage_lt=amperage_lt,
        amperage_lte=amperage_lte,
        amperage_n=amperage_n,
        cable_end=cable_end,
        cable_end_n=cable_end_n,
        cabled=cabled,
        connected=connected,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        max_utilization=max_utilization,
        max_utilization_empty=max_utilization_empty,
        max_utilization_gt=max_utilization_gt,
        max_utilization_gte=max_utilization_gte,
        max_utilization_lt=max_utilization_lt,
        max_utilization_lte=max_utilization_lte,
        max_utilization_n=max_utilization_n,
        modified_by_request=modified_by_request,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        occupied=occupied,
        offset=offset,
        ordering=ordering,
        phase=phase,
        phase_n=phase_n,
        power_panel_id=power_panel_id,
        power_panel_id_n=power_panel_id_n,
        q=q,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        status=status,
        status_n=status_n,
        supply=supply,
        supply_n=supply_n,
        tag=tag,
        tag_n=tag_n,
        tenant=tenant,
        tenant_n=tenant_n,
        tenant_group=tenant_group,
        tenant_group_n=tenant_group_n,
        tenant_group_id=tenant_group_id,
        tenant_group_id_n=tenant_group_id_n,
        tenant_id=tenant_id,
        tenant_id_n=tenant_id_n,
        type=type,
        type_n=type_n,
        updated_by_request=updated_by_request,
        voltage=voltage,
        voltage_empty=voltage_empty,
        voltage_gt=voltage_gt,
        voltage_gte=voltage_gte,
        voltage_lt=voltage_lt,
        voltage_lte=voltage_lte,
        voltage_n=voltage_n,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    amperage: Union[Unset, List[int]] = UNSET,
    amperage_empty: Union[Unset, bool] = UNSET,
    amperage_gt: Union[Unset, List[int]] = UNSET,
    amperage_gte: Union[Unset, List[int]] = UNSET,
    amperage_lt: Union[Unset, List[int]] = UNSET,
    amperage_lte: Union[Unset, List[int]] = UNSET,
    amperage_n: Union[Unset, List[int]] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    cabled: Union[Unset, bool] = UNSET,
    connected: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    max_utilization: Union[Unset, List[int]] = UNSET,
    max_utilization_empty: Union[Unset, bool] = UNSET,
    max_utilization_gt: Union[Unset, List[int]] = UNSET,
    max_utilization_gte: Union[Unset, List[int]] = UNSET,
    max_utilization_lt: Union[Unset, List[int]] = UNSET,
    max_utilization_lte: Union[Unset, List[int]] = UNSET,
    max_utilization_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    occupied: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    phase: Union[Unset, str] = UNSET,
    phase_n: Union[Unset, str] = UNSET,
    power_panel_id: Union[Unset, List[int]] = UNSET,
    power_panel_id_n: Union[Unset, List[int]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    supply: Union[Unset, str] = UNSET,
    supply_n: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    type: Union[Unset, str] = UNSET,
    type_n: Union[Unset, str] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    voltage: Union[Unset, List[int]] = UNSET,
    voltage_empty: Union[Unset, bool] = UNSET,
    voltage_gt: Union[Unset, List[int]] = UNSET,
    voltage_gte: Union[Unset, List[int]] = UNSET,
    voltage_lt: Union[Unset, List[int]] = UNSET,
    voltage_lte: Union[Unset, List[int]] = UNSET,
    voltage_n: Union[Unset, List[int]] = UNSET,
) -> Response[PaginatedPowerFeedList]:
    """Get a list of power feed objects.

    Args:
        amperage (Union[Unset, List[int]]):
        amperage_empty (Union[Unset, bool]):
        amperage_gt (Union[Unset, List[int]]):
        amperage_gte (Union[Unset, List[int]]):
        amperage_lt (Union[Unset, List[int]]):
        amperage_lte (Union[Unset, List[int]]):
        amperage_n (Union[Unset, List[int]]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        cabled (Union[Unset, bool]):
        connected (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        max_utilization (Union[Unset, List[int]]):
        max_utilization_empty (Union[Unset, bool]):
        max_utilization_gt (Union[Unset, List[int]]):
        max_utilization_gte (Union[Unset, List[int]]):
        max_utilization_lt (Union[Unset, List[int]]):
        max_utilization_lte (Union[Unset, List[int]]):
        max_utilization_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        occupied (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        phase (Union[Unset, str]):
        phase_n (Union[Unset, str]):
        power_panel_id (Union[Unset, List[int]]):
        power_panel_id_n (Union[Unset, List[int]]):
        q (Union[Unset, str]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        supply (Union[Unset, str]):
        supply_n (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        type (Union[Unset, str]):
        type_n (Union[Unset, str]):
        updated_by_request (Union[Unset, UUID]):
        voltage (Union[Unset, List[int]]):
        voltage_empty (Union[Unset, bool]):
        voltage_gt (Union[Unset, List[int]]):
        voltage_gte (Union[Unset, List[int]]):
        voltage_lt (Union[Unset, List[int]]):
        voltage_lte (Union[Unset, List[int]]):
        voltage_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedPowerFeedList]
    """

    kwargs = _get_kwargs(
        amperage=amperage,
        amperage_empty=amperage_empty,
        amperage_gt=amperage_gt,
        amperage_gte=amperage_gte,
        amperage_lt=amperage_lt,
        amperage_lte=amperage_lte,
        amperage_n=amperage_n,
        cable_end=cable_end,
        cable_end_n=cable_end_n,
        cabled=cabled,
        connected=connected,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        max_utilization=max_utilization,
        max_utilization_empty=max_utilization_empty,
        max_utilization_gt=max_utilization_gt,
        max_utilization_gte=max_utilization_gte,
        max_utilization_lt=max_utilization_lt,
        max_utilization_lte=max_utilization_lte,
        max_utilization_n=max_utilization_n,
        modified_by_request=modified_by_request,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        occupied=occupied,
        offset=offset,
        ordering=ordering,
        phase=phase,
        phase_n=phase_n,
        power_panel_id=power_panel_id,
        power_panel_id_n=power_panel_id_n,
        q=q,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        status=status,
        status_n=status_n,
        supply=supply,
        supply_n=supply_n,
        tag=tag,
        tag_n=tag_n,
        tenant=tenant,
        tenant_n=tenant_n,
        tenant_group=tenant_group,
        tenant_group_n=tenant_group_n,
        tenant_group_id=tenant_group_id,
        tenant_group_id_n=tenant_group_id_n,
        tenant_id=tenant_id,
        tenant_id_n=tenant_id_n,
        type=type,
        type_n=type_n,
        updated_by_request=updated_by_request,
        voltage=voltage,
        voltage_empty=voltage_empty,
        voltage_gt=voltage_gt,
        voltage_gte=voltage_gte,
        voltage_lt=voltage_lt,
        voltage_lte=voltage_lte,
        voltage_n=voltage_n,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    amperage: Union[Unset, List[int]] = UNSET,
    amperage_empty: Union[Unset, bool] = UNSET,
    amperage_gt: Union[Unset, List[int]] = UNSET,
    amperage_gte: Union[Unset, List[int]] = UNSET,
    amperage_lt: Union[Unset, List[int]] = UNSET,
    amperage_lte: Union[Unset, List[int]] = UNSET,
    amperage_n: Union[Unset, List[int]] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    cabled: Union[Unset, bool] = UNSET,
    connected: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    max_utilization: Union[Unset, List[int]] = UNSET,
    max_utilization_empty: Union[Unset, bool] = UNSET,
    max_utilization_gt: Union[Unset, List[int]] = UNSET,
    max_utilization_gte: Union[Unset, List[int]] = UNSET,
    max_utilization_lt: Union[Unset, List[int]] = UNSET,
    max_utilization_lte: Union[Unset, List[int]] = UNSET,
    max_utilization_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    occupied: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    phase: Union[Unset, str] = UNSET,
    phase_n: Union[Unset, str] = UNSET,
    power_panel_id: Union[Unset, List[int]] = UNSET,
    power_panel_id_n: Union[Unset, List[int]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    supply: Union[Unset, str] = UNSET,
    supply_n: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    type: Union[Unset, str] = UNSET,
    type_n: Union[Unset, str] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    voltage: Union[Unset, List[int]] = UNSET,
    voltage_empty: Union[Unset, bool] = UNSET,
    voltage_gt: Union[Unset, List[int]] = UNSET,
    voltage_gte: Union[Unset, List[int]] = UNSET,
    voltage_lt: Union[Unset, List[int]] = UNSET,
    voltage_lte: Union[Unset, List[int]] = UNSET,
    voltage_n: Union[Unset, List[int]] = UNSET,
) -> Optional[PaginatedPowerFeedList]:
    """Get a list of power feed objects.

    Args:
        amperage (Union[Unset, List[int]]):
        amperage_empty (Union[Unset, bool]):
        amperage_gt (Union[Unset, List[int]]):
        amperage_gte (Union[Unset, List[int]]):
        amperage_lt (Union[Unset, List[int]]):
        amperage_lte (Union[Unset, List[int]]):
        amperage_n (Union[Unset, List[int]]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        cabled (Union[Unset, bool]):
        connected (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        max_utilization (Union[Unset, List[int]]):
        max_utilization_empty (Union[Unset, bool]):
        max_utilization_gt (Union[Unset, List[int]]):
        max_utilization_gte (Union[Unset, List[int]]):
        max_utilization_lt (Union[Unset, List[int]]):
        max_utilization_lte (Union[Unset, List[int]]):
        max_utilization_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        occupied (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        phase (Union[Unset, str]):
        phase_n (Union[Unset, str]):
        power_panel_id (Union[Unset, List[int]]):
        power_panel_id_n (Union[Unset, List[int]]):
        q (Union[Unset, str]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        supply (Union[Unset, str]):
        supply_n (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        type (Union[Unset, str]):
        type_n (Union[Unset, str]):
        updated_by_request (Union[Unset, UUID]):
        voltage (Union[Unset, List[int]]):
        voltage_empty (Union[Unset, bool]):
        voltage_gt (Union[Unset, List[int]]):
        voltage_gte (Union[Unset, List[int]]):
        voltage_lt (Union[Unset, List[int]]):
        voltage_lte (Union[Unset, List[int]]):
        voltage_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedPowerFeedList
    """

    return (
        await asyncio_detailed(
            client=client,
            amperage=amperage,
            amperage_empty=amperage_empty,
            amperage_gt=amperage_gt,
            amperage_gte=amperage_gte,
            amperage_lt=amperage_lt,
            amperage_lte=amperage_lte,
            amperage_n=amperage_n,
            cable_end=cable_end,
            cable_end_n=cable_end_n,
            cabled=cabled,
            connected=connected,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            description=description,
            description_empty=description_empty,
            description_ic=description_ic,
            description_ie=description_ie,
            description_iew=description_iew,
            description_isw=description_isw,
            description_n=description_n,
            description_nic=description_nic,
            description_nie=description_nie,
            description_niew=description_niew,
            description_nisw=description_nisw,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            limit=limit,
            max_utilization=max_utilization,
            max_utilization_empty=max_utilization_empty,
            max_utilization_gt=max_utilization_gt,
            max_utilization_gte=max_utilization_gte,
            max_utilization_lt=max_utilization_lt,
            max_utilization_lte=max_utilization_lte,
            max_utilization_n=max_utilization_n,
            modified_by_request=modified_by_request,
            name=name,
            name_empty=name_empty,
            name_ic=name_ic,
            name_ie=name_ie,
            name_iew=name_iew,
            name_isw=name_isw,
            name_n=name_n,
            name_nic=name_nic,
            name_nie=name_nie,
            name_niew=name_niew,
            name_nisw=name_nisw,
            occupied=occupied,
            offset=offset,
            ordering=ordering,
            phase=phase,
            phase_n=phase_n,
            power_panel_id=power_panel_id,
            power_panel_id_n=power_panel_id_n,
            q=q,
            rack_id=rack_id,
            rack_id_n=rack_id_n,
            region=region,
            region_n=region_n,
            region_id=region_id,
            region_id_n=region_id_n,
            site=site,
            site_n=site_n,
            site_group=site_group,
            site_group_n=site_group_n,
            site_group_id=site_group_id,
            site_group_id_n=site_group_id_n,
            site_id=site_id,
            site_id_n=site_id_n,
            status=status,
            status_n=status_n,
            supply=supply,
            supply_n=supply_n,
            tag=tag,
            tag_n=tag_n,
            tenant=tenant,
            tenant_n=tenant_n,
            tenant_group=tenant_group,
            tenant_group_n=tenant_group_n,
            tenant_group_id=tenant_group_id,
            tenant_group_id_n=tenant_group_id_n,
            tenant_id=tenant_id,
            tenant_id_n=tenant_id_n,
            type=type,
            type_n=type_n,
            updated_by_request=updated_by_request,
            voltage=voltage,
            voltage_empty=voltage_empty,
            voltage_gt=voltage_gt,
            voltage_gte=voltage_gte,
            voltage_lt=voltage_lt,
            voltage_lte=voltage_lte,
            voltage_n=voltage_n,
        )
    ).parsed
