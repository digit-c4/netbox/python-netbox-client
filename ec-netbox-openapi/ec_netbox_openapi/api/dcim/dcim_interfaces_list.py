import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_interface_list import PaginatedInterfaceList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    bridge_id: Union[Unset, List[int]] = UNSET,
    bridge_id_n: Union[Unset, List[int]] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    cabled: Union[Unset, bool] = UNSET,
    connected: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    duplex: Union[Unset, List[Union[None, str]]] = UNSET,
    duplex_n: Union[Unset, List[Union[None, str]]] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    kind: Union[Unset, str] = UNSET,
    l2vpn: Union[Unset, List[Union[None, int]]] = UNSET,
    l2vpn_n: Union[Unset, List[Union[None, int]]] = UNSET,
    l2vpn_id: Union[Unset, List[int]] = UNSET,
    l2vpn_id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    lag_id: Union[Unset, List[int]] = UNSET,
    lag_id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    mac_address: Union[Unset, List[str]] = UNSET,
    mac_address_ic: Union[Unset, List[str]] = UNSET,
    mac_address_ie: Union[Unset, List[str]] = UNSET,
    mac_address_iew: Union[Unset, List[str]] = UNSET,
    mac_address_isw: Union[Unset, List[str]] = UNSET,
    mac_address_n: Union[Unset, List[str]] = UNSET,
    mac_address_nic: Union[Unset, List[str]] = UNSET,
    mac_address_nie: Union[Unset, List[str]] = UNSET,
    mac_address_niew: Union[Unset, List[str]] = UNSET,
    mac_address_nisw: Union[Unset, List[str]] = UNSET,
    mgmt_only: Union[Unset, bool] = UNSET,
    mode: Union[Unset, str] = UNSET,
    mode_n: Union[Unset, str] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    module_id: Union[Unset, List[Union[None, int]]] = UNSET,
    module_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    mtu: Union[Unset, List[int]] = UNSET,
    mtu_empty: Union[Unset, bool] = UNSET,
    mtu_gt: Union[Unset, List[int]] = UNSET,
    mtu_gte: Union[Unset, List[int]] = UNSET,
    mtu_lt: Union[Unset, List[int]] = UNSET,
    mtu_lte: Union[Unset, List[int]] = UNSET,
    mtu_n: Union[Unset, List[int]] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    occupied: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent_id: Union[Unset, List[int]] = UNSET,
    parent_id_n: Union[Unset, List[int]] = UNSET,
    poe_mode: Union[Unset, List[str]] = UNSET,
    poe_mode_n: Union[Unset, List[str]] = UNSET,
    poe_type: Union[Unset, List[str]] = UNSET,
    poe_type_n: Union[Unset, List[str]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    rf_channel: Union[Unset, List[str]] = UNSET,
    rf_channel_n: Union[Unset, List[str]] = UNSET,
    rf_channel_frequency: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_empty: Union[Unset, bool] = UNSET,
    rf_channel_frequency_gt: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_gte: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_lt: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_lte: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_n: Union[Unset, List[float]] = UNSET,
    rf_channel_width: Union[Unset, List[float]] = UNSET,
    rf_channel_width_empty: Union[Unset, bool] = UNSET,
    rf_channel_width_gt: Union[Unset, List[float]] = UNSET,
    rf_channel_width_gte: Union[Unset, List[float]] = UNSET,
    rf_channel_width_lt: Union[Unset, List[float]] = UNSET,
    rf_channel_width_lte: Union[Unset, List[float]] = UNSET,
    rf_channel_width_n: Union[Unset, List[float]] = UNSET,
    rf_role: Union[Unset, List[str]] = UNSET,
    rf_role_n: Union[Unset, List[str]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[int]] = UNSET,
    role_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    speed: Union[Unset, List[int]] = UNSET,
    speed_empty: Union[Unset, List[int]] = UNSET,
    speed_gt: Union[Unset, List[int]] = UNSET,
    speed_gte: Union[Unset, List[int]] = UNSET,
    speed_lt: Union[Unset, List[int]] = UNSET,
    speed_lte: Union[Unset, List[int]] = UNSET,
    speed_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tx_power: Union[Unset, List[int]] = UNSET,
    tx_power_empty: Union[Unset, bool] = UNSET,
    tx_power_gt: Union[Unset, List[int]] = UNSET,
    tx_power_gte: Union[Unset, List[int]] = UNSET,
    tx_power_lt: Union[Unset, List[int]] = UNSET,
    tx_power_lte: Union[Unset, List[int]] = UNSET,
    tx_power_n: Union[Unset, List[int]] = UNSET,
    type: Union[Unset, List[str]] = UNSET,
    type_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    vdc: Union[Unset, List[str]] = UNSET,
    vdc_n: Union[Unset, List[str]] = UNSET,
    vdc_id: Union[Unset, List[int]] = UNSET,
    vdc_id_n: Union[Unset, List[int]] = UNSET,
    vdc_identifier: Union[Unset, List[Union[None, int]]] = UNSET,
    vdc_identifier_n: Union[Unset, List[Union[None, int]]] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
    virtual_chassis_member: Union[Unset, List[str]] = UNSET,
    virtual_chassis_member_id: Union[Unset, List[int]] = UNSET,
    vlan: Union[Unset, str] = UNSET,
    vlan_id: Union[Unset, str] = UNSET,
    vrf: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_n: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_id: Union[Unset, List[int]] = UNSET,
    vrf_id_n: Union[Unset, List[int]] = UNSET,
    wwn: Union[Unset, List[str]] = UNSET,
    wwn_ic: Union[Unset, List[str]] = UNSET,
    wwn_ie: Union[Unset, List[str]] = UNSET,
    wwn_iew: Union[Unset, List[str]] = UNSET,
    wwn_isw: Union[Unset, List[str]] = UNSET,
    wwn_n: Union[Unset, List[str]] = UNSET,
    wwn_nic: Union[Unset, List[str]] = UNSET,
    wwn_nie: Union[Unset, List[str]] = UNSET,
    wwn_niew: Union[Unset, List[str]] = UNSET,
    wwn_nisw: Union[Unset, List[str]] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_bridge_id: Union[Unset, List[int]] = UNSET
    if not isinstance(bridge_id, Unset):
        json_bridge_id = bridge_id

    params["bridge_id"] = json_bridge_id

    json_bridge_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(bridge_id_n, Unset):
        json_bridge_id_n = bridge_id_n

    params["bridge_id__n"] = json_bridge_id_n

    params["cable_end"] = cable_end

    params["cable_end__n"] = cable_end_n

    params["cabled"] = cabled

    params["connected"] = connected

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    json_description: Union[Unset, List[str]] = UNSET
    if not isinstance(description, Unset):
        json_description = description

    params["description"] = json_description

    params["description__empty"] = description_empty

    json_description_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ic, Unset):
        json_description_ic = description_ic

    params["description__ic"] = json_description_ic

    json_description_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ie, Unset):
        json_description_ie = description_ie

    params["description__ie"] = json_description_ie

    json_description_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_iew, Unset):
        json_description_iew = description_iew

    params["description__iew"] = json_description_iew

    json_description_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_isw, Unset):
        json_description_isw = description_isw

    params["description__isw"] = json_description_isw

    json_description_n: Union[Unset, List[str]] = UNSET
    if not isinstance(description_n, Unset):
        json_description_n = description_n

    params["description__n"] = json_description_n

    json_description_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nic, Unset):
        json_description_nic = description_nic

    params["description__nic"] = json_description_nic

    json_description_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nie, Unset):
        json_description_nie = description_nie

    params["description__nie"] = json_description_nie

    json_description_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_niew, Unset):
        json_description_niew = description_niew

    params["description__niew"] = json_description_niew

    json_description_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nisw, Unset):
        json_description_nisw = description_nisw

    params["description__nisw"] = json_description_nisw

    json_device: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(device, Unset):
        json_device = []
        for device_item_data in device:
            device_item: Union[None, str]
            device_item = device_item_data
            json_device.append(device_item)

    params["device"] = json_device

    json_device_n: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(device_n, Unset):
        json_device_n = []
        for device_n_item_data in device_n:
            device_n_item: Union[None, str]
            device_n_item = device_n_item_data
            json_device_n.append(device_n_item)

    params["device__n"] = json_device_n

    json_device_id: Union[Unset, List[int]] = UNSET
    if not isinstance(device_id, Unset):
        json_device_id = device_id

    params["device_id"] = json_device_id

    json_device_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(device_id_n, Unset):
        json_device_id_n = device_id_n

    params["device_id__n"] = json_device_id_n

    json_device_role: Union[Unset, List[str]] = UNSET
    if not isinstance(device_role, Unset):
        json_device_role = device_role

    params["device_role"] = json_device_role

    json_device_role_n: Union[Unset, List[str]] = UNSET
    if not isinstance(device_role_n, Unset):
        json_device_role_n = device_role_n

    params["device_role__n"] = json_device_role_n

    json_device_role_id: Union[Unset, List[int]] = UNSET
    if not isinstance(device_role_id, Unset):
        json_device_role_id = device_role_id

    params["device_role_id"] = json_device_role_id

    json_device_role_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(device_role_id_n, Unset):
        json_device_role_id_n = device_role_id_n

    params["device_role_id__n"] = json_device_role_id_n

    json_device_type: Union[Unset, List[str]] = UNSET
    if not isinstance(device_type, Unset):
        json_device_type = device_type

    params["device_type"] = json_device_type

    json_device_type_n: Union[Unset, List[str]] = UNSET
    if not isinstance(device_type_n, Unset):
        json_device_type_n = device_type_n

    params["device_type__n"] = json_device_type_n

    json_device_type_id: Union[Unset, List[int]] = UNSET
    if not isinstance(device_type_id, Unset):
        json_device_type_id = device_type_id

    params["device_type_id"] = json_device_type_id

    json_device_type_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(device_type_id_n, Unset):
        json_device_type_id_n = device_type_id_n

    params["device_type_id__n"] = json_device_type_id_n

    json_duplex: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(duplex, Unset):
        json_duplex = []
        for duplex_item_data in duplex:
            duplex_item: Union[None, str]
            duplex_item = duplex_item_data
            json_duplex.append(duplex_item)

    params["duplex"] = json_duplex

    json_duplex_n: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(duplex_n, Unset):
        json_duplex_n = []
        for duplex_n_item_data in duplex_n:
            duplex_n_item: Union[None, str]
            duplex_n_item = duplex_n_item_data
            json_duplex_n.append(duplex_n_item)

    params["duplex__n"] = json_duplex_n

    params["enabled"] = enabled

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    params["kind"] = kind

    json_l2vpn: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(l2vpn, Unset):
        json_l2vpn = []
        for l2vpn_item_data in l2vpn:
            l2vpn_item: Union[None, int]
            l2vpn_item = l2vpn_item_data
            json_l2vpn.append(l2vpn_item)

    params["l2vpn"] = json_l2vpn

    json_l2vpn_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(l2vpn_n, Unset):
        json_l2vpn_n = []
        for l2vpn_n_item_data in l2vpn_n:
            l2vpn_n_item: Union[None, int]
            l2vpn_n_item = l2vpn_n_item_data
            json_l2vpn_n.append(l2vpn_n_item)

    params["l2vpn__n"] = json_l2vpn_n

    json_l2vpn_id: Union[Unset, List[int]] = UNSET
    if not isinstance(l2vpn_id, Unset):
        json_l2vpn_id = l2vpn_id

    params["l2vpn_id"] = json_l2vpn_id

    json_l2vpn_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(l2vpn_id_n, Unset):
        json_l2vpn_id_n = l2vpn_id_n

    params["l2vpn_id__n"] = json_l2vpn_id_n

    json_label: Union[Unset, List[str]] = UNSET
    if not isinstance(label, Unset):
        json_label = label

    params["label"] = json_label

    params["label__empty"] = label_empty

    json_label_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(label_ic, Unset):
        json_label_ic = label_ic

    params["label__ic"] = json_label_ic

    json_label_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(label_ie, Unset):
        json_label_ie = label_ie

    params["label__ie"] = json_label_ie

    json_label_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(label_iew, Unset):
        json_label_iew = label_iew

    params["label__iew"] = json_label_iew

    json_label_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(label_isw, Unset):
        json_label_isw = label_isw

    params["label__isw"] = json_label_isw

    json_label_n: Union[Unset, List[str]] = UNSET
    if not isinstance(label_n, Unset):
        json_label_n = label_n

    params["label__n"] = json_label_n

    json_label_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nic, Unset):
        json_label_nic = label_nic

    params["label__nic"] = json_label_nic

    json_label_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nie, Unset):
        json_label_nie = label_nie

    params["label__nie"] = json_label_nie

    json_label_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(label_niew, Unset):
        json_label_niew = label_niew

    params["label__niew"] = json_label_niew

    json_label_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nisw, Unset):
        json_label_nisw = label_nisw

    params["label__nisw"] = json_label_nisw

    json_lag_id: Union[Unset, List[int]] = UNSET
    if not isinstance(lag_id, Unset):
        json_lag_id = lag_id

    params["lag_id"] = json_lag_id

    json_lag_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(lag_id_n, Unset):
        json_lag_id_n = lag_id_n

    params["lag_id__n"] = json_lag_id_n

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    params["limit"] = limit

    json_location: Union[Unset, List[str]] = UNSET
    if not isinstance(location, Unset):
        json_location = location

    params["location"] = json_location

    json_location_n: Union[Unset, List[str]] = UNSET
    if not isinstance(location_n, Unset):
        json_location_n = location_n

    params["location__n"] = json_location_n

    json_location_id: Union[Unset, List[int]] = UNSET
    if not isinstance(location_id, Unset):
        json_location_id = location_id

    params["location_id"] = json_location_id

    json_location_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(location_id_n, Unset):
        json_location_id_n = location_id_n

    params["location_id__n"] = json_location_id_n

    json_mac_address: Union[Unset, List[str]] = UNSET
    if not isinstance(mac_address, Unset):
        json_mac_address = mac_address

    params["mac_address"] = json_mac_address

    json_mac_address_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(mac_address_ic, Unset):
        json_mac_address_ic = mac_address_ic

    params["mac_address__ic"] = json_mac_address_ic

    json_mac_address_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(mac_address_ie, Unset):
        json_mac_address_ie = mac_address_ie

    params["mac_address__ie"] = json_mac_address_ie

    json_mac_address_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(mac_address_iew, Unset):
        json_mac_address_iew = mac_address_iew

    params["mac_address__iew"] = json_mac_address_iew

    json_mac_address_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(mac_address_isw, Unset):
        json_mac_address_isw = mac_address_isw

    params["mac_address__isw"] = json_mac_address_isw

    json_mac_address_n: Union[Unset, List[str]] = UNSET
    if not isinstance(mac_address_n, Unset):
        json_mac_address_n = mac_address_n

    params["mac_address__n"] = json_mac_address_n

    json_mac_address_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(mac_address_nic, Unset):
        json_mac_address_nic = mac_address_nic

    params["mac_address__nic"] = json_mac_address_nic

    json_mac_address_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(mac_address_nie, Unset):
        json_mac_address_nie = mac_address_nie

    params["mac_address__nie"] = json_mac_address_nie

    json_mac_address_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(mac_address_niew, Unset):
        json_mac_address_niew = mac_address_niew

    params["mac_address__niew"] = json_mac_address_niew

    json_mac_address_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(mac_address_nisw, Unset):
        json_mac_address_nisw = mac_address_nisw

    params["mac_address__nisw"] = json_mac_address_nisw

    params["mgmt_only"] = mgmt_only

    params["mode"] = mode

    params["mode__n"] = mode_n

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    json_module_id: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(module_id, Unset):
        json_module_id = []
        for module_id_item_data in module_id:
            module_id_item: Union[None, int]
            module_id_item = module_id_item_data
            json_module_id.append(module_id_item)

    params["module_id"] = json_module_id

    json_module_id_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(module_id_n, Unset):
        json_module_id_n = []
        for module_id_n_item_data in module_id_n:
            module_id_n_item: Union[None, int]
            module_id_n_item = module_id_n_item_data
            json_module_id_n.append(module_id_n_item)

    params["module_id__n"] = json_module_id_n

    json_mtu: Union[Unset, List[int]] = UNSET
    if not isinstance(mtu, Unset):
        json_mtu = mtu

    params["mtu"] = json_mtu

    params["mtu__empty"] = mtu_empty

    json_mtu_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(mtu_gt, Unset):
        json_mtu_gt = mtu_gt

    params["mtu__gt"] = json_mtu_gt

    json_mtu_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(mtu_gte, Unset):
        json_mtu_gte = mtu_gte

    params["mtu__gte"] = json_mtu_gte

    json_mtu_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(mtu_lt, Unset):
        json_mtu_lt = mtu_lt

    params["mtu__lt"] = json_mtu_lt

    json_mtu_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(mtu_lte, Unset):
        json_mtu_lte = mtu_lte

    params["mtu__lte"] = json_mtu_lte

    json_mtu_n: Union[Unset, List[int]] = UNSET
    if not isinstance(mtu_n, Unset):
        json_mtu_n = mtu_n

    params["mtu__n"] = json_mtu_n

    json_name: Union[Unset, List[str]] = UNSET
    if not isinstance(name, Unset):
        json_name = name

    params["name"] = json_name

    params["name__empty"] = name_empty

    json_name_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ic, Unset):
        json_name_ic = name_ic

    params["name__ic"] = json_name_ic

    json_name_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ie, Unset):
        json_name_ie = name_ie

    params["name__ie"] = json_name_ie

    json_name_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_iew, Unset):
        json_name_iew = name_iew

    params["name__iew"] = json_name_iew

    json_name_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_isw, Unset):
        json_name_isw = name_isw

    params["name__isw"] = json_name_isw

    json_name_n: Union[Unset, List[str]] = UNSET
    if not isinstance(name_n, Unset):
        json_name_n = name_n

    params["name__n"] = json_name_n

    json_name_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nic, Unset):
        json_name_nic = name_nic

    params["name__nic"] = json_name_nic

    json_name_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nie, Unset):
        json_name_nie = name_nie

    params["name__nie"] = json_name_nie

    json_name_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_niew, Unset):
        json_name_niew = name_niew

    params["name__niew"] = json_name_niew

    json_name_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nisw, Unset):
        json_name_nisw = name_nisw

    params["name__nisw"] = json_name_nisw

    params["occupied"] = occupied

    params["offset"] = offset

    params["ordering"] = ordering

    json_parent_id: Union[Unset, List[int]] = UNSET
    if not isinstance(parent_id, Unset):
        json_parent_id = parent_id

    params["parent_id"] = json_parent_id

    json_parent_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(parent_id_n, Unset):
        json_parent_id_n = parent_id_n

    params["parent_id__n"] = json_parent_id_n

    json_poe_mode: Union[Unset, List[str]] = UNSET
    if not isinstance(poe_mode, Unset):
        json_poe_mode = poe_mode

    params["poe_mode"] = json_poe_mode

    json_poe_mode_n: Union[Unset, List[str]] = UNSET
    if not isinstance(poe_mode_n, Unset):
        json_poe_mode_n = poe_mode_n

    params["poe_mode__n"] = json_poe_mode_n

    json_poe_type: Union[Unset, List[str]] = UNSET
    if not isinstance(poe_type, Unset):
        json_poe_type = poe_type

    params["poe_type"] = json_poe_type

    json_poe_type_n: Union[Unset, List[str]] = UNSET
    if not isinstance(poe_type_n, Unset):
        json_poe_type_n = poe_type_n

    params["poe_type__n"] = json_poe_type_n

    params["q"] = q

    json_rack: Union[Unset, List[str]] = UNSET
    if not isinstance(rack, Unset):
        json_rack = rack

    params["rack"] = json_rack

    json_rack_n: Union[Unset, List[str]] = UNSET
    if not isinstance(rack_n, Unset):
        json_rack_n = rack_n

    params["rack__n"] = json_rack_n

    json_rack_id: Union[Unset, List[int]] = UNSET
    if not isinstance(rack_id, Unset):
        json_rack_id = rack_id

    params["rack_id"] = json_rack_id

    json_rack_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(rack_id_n, Unset):
        json_rack_id_n = rack_id_n

    params["rack_id__n"] = json_rack_id_n

    json_region: Union[Unset, List[int]] = UNSET
    if not isinstance(region, Unset):
        json_region = region

    params["region"] = json_region

    json_region_n: Union[Unset, List[int]] = UNSET
    if not isinstance(region_n, Unset):
        json_region_n = region_n

    params["region__n"] = json_region_n

    json_region_id: Union[Unset, List[int]] = UNSET
    if not isinstance(region_id, Unset):
        json_region_id = region_id

    params["region_id"] = json_region_id

    json_region_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(region_id_n, Unset):
        json_region_id_n = region_id_n

    params["region_id__n"] = json_region_id_n

    json_rf_channel: Union[Unset, List[str]] = UNSET
    if not isinstance(rf_channel, Unset):
        json_rf_channel = rf_channel

    params["rf_channel"] = json_rf_channel

    json_rf_channel_n: Union[Unset, List[str]] = UNSET
    if not isinstance(rf_channel_n, Unset):
        json_rf_channel_n = rf_channel_n

    params["rf_channel__n"] = json_rf_channel_n

    json_rf_channel_frequency: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_frequency, Unset):
        json_rf_channel_frequency = rf_channel_frequency

    params["rf_channel_frequency"] = json_rf_channel_frequency

    params["rf_channel_frequency__empty"] = rf_channel_frequency_empty

    json_rf_channel_frequency_gt: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_frequency_gt, Unset):
        json_rf_channel_frequency_gt = rf_channel_frequency_gt

    params["rf_channel_frequency__gt"] = json_rf_channel_frequency_gt

    json_rf_channel_frequency_gte: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_frequency_gte, Unset):
        json_rf_channel_frequency_gte = rf_channel_frequency_gte

    params["rf_channel_frequency__gte"] = json_rf_channel_frequency_gte

    json_rf_channel_frequency_lt: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_frequency_lt, Unset):
        json_rf_channel_frequency_lt = rf_channel_frequency_lt

    params["rf_channel_frequency__lt"] = json_rf_channel_frequency_lt

    json_rf_channel_frequency_lte: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_frequency_lte, Unset):
        json_rf_channel_frequency_lte = rf_channel_frequency_lte

    params["rf_channel_frequency__lte"] = json_rf_channel_frequency_lte

    json_rf_channel_frequency_n: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_frequency_n, Unset):
        json_rf_channel_frequency_n = rf_channel_frequency_n

    params["rf_channel_frequency__n"] = json_rf_channel_frequency_n

    json_rf_channel_width: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_width, Unset):
        json_rf_channel_width = rf_channel_width

    params["rf_channel_width"] = json_rf_channel_width

    params["rf_channel_width__empty"] = rf_channel_width_empty

    json_rf_channel_width_gt: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_width_gt, Unset):
        json_rf_channel_width_gt = rf_channel_width_gt

    params["rf_channel_width__gt"] = json_rf_channel_width_gt

    json_rf_channel_width_gte: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_width_gte, Unset):
        json_rf_channel_width_gte = rf_channel_width_gte

    params["rf_channel_width__gte"] = json_rf_channel_width_gte

    json_rf_channel_width_lt: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_width_lt, Unset):
        json_rf_channel_width_lt = rf_channel_width_lt

    params["rf_channel_width__lt"] = json_rf_channel_width_lt

    json_rf_channel_width_lte: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_width_lte, Unset):
        json_rf_channel_width_lte = rf_channel_width_lte

    params["rf_channel_width__lte"] = json_rf_channel_width_lte

    json_rf_channel_width_n: Union[Unset, List[float]] = UNSET
    if not isinstance(rf_channel_width_n, Unset):
        json_rf_channel_width_n = rf_channel_width_n

    params["rf_channel_width__n"] = json_rf_channel_width_n

    json_rf_role: Union[Unset, List[str]] = UNSET
    if not isinstance(rf_role, Unset):
        json_rf_role = rf_role

    params["rf_role"] = json_rf_role

    json_rf_role_n: Union[Unset, List[str]] = UNSET
    if not isinstance(rf_role_n, Unset):
        json_rf_role_n = rf_role_n

    params["rf_role__n"] = json_rf_role_n

    json_role: Union[Unset, List[str]] = UNSET
    if not isinstance(role, Unset):
        json_role = role

    params["role"] = json_role

    json_role_n: Union[Unset, List[str]] = UNSET
    if not isinstance(role_n, Unset):
        json_role_n = role_n

    params["role__n"] = json_role_n

    json_role_id: Union[Unset, List[int]] = UNSET
    if not isinstance(role_id, Unset):
        json_role_id = role_id

    params["role_id"] = json_role_id

    json_role_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(role_id_n, Unset):
        json_role_id_n = role_id_n

    params["role_id__n"] = json_role_id_n

    json_site: Union[Unset, List[str]] = UNSET
    if not isinstance(site, Unset):
        json_site = site

    params["site"] = json_site

    json_site_n: Union[Unset, List[str]] = UNSET
    if not isinstance(site_n, Unset):
        json_site_n = site_n

    params["site__n"] = json_site_n

    json_site_group: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group, Unset):
        json_site_group = site_group

    params["site_group"] = json_site_group

    json_site_group_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_n, Unset):
        json_site_group_n = site_group_n

    params["site_group__n"] = json_site_group_n

    json_site_group_id: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_id, Unset):
        json_site_group_id = site_group_id

    params["site_group_id"] = json_site_group_id

    json_site_group_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_id_n, Unset):
        json_site_group_id_n = site_group_id_n

    params["site_group_id__n"] = json_site_group_id_n

    json_site_id: Union[Unset, List[int]] = UNSET
    if not isinstance(site_id, Unset):
        json_site_id = site_id

    params["site_id"] = json_site_id

    json_site_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_id_n, Unset):
        json_site_id_n = site_id_n

    params["site_id__n"] = json_site_id_n

    json_speed: Union[Unset, List[int]] = UNSET
    if not isinstance(speed, Unset):
        json_speed = speed

    params["speed"] = json_speed

    json_speed_empty: Union[Unset, List[int]] = UNSET
    if not isinstance(speed_empty, Unset):
        json_speed_empty = speed_empty

    params["speed__empty"] = json_speed_empty

    json_speed_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(speed_gt, Unset):
        json_speed_gt = speed_gt

    params["speed__gt"] = json_speed_gt

    json_speed_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(speed_gte, Unset):
        json_speed_gte = speed_gte

    params["speed__gte"] = json_speed_gte

    json_speed_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(speed_lt, Unset):
        json_speed_lt = speed_lt

    params["speed__lt"] = json_speed_lt

    json_speed_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(speed_lte, Unset):
        json_speed_lte = speed_lte

    params["speed__lte"] = json_speed_lte

    json_speed_n: Union[Unset, List[int]] = UNSET
    if not isinstance(speed_n, Unset):
        json_speed_n = speed_n

    params["speed__n"] = json_speed_n

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_tx_power: Union[Unset, List[int]] = UNSET
    if not isinstance(tx_power, Unset):
        json_tx_power = tx_power

    params["tx_power"] = json_tx_power

    params["tx_power__empty"] = tx_power_empty

    json_tx_power_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(tx_power_gt, Unset):
        json_tx_power_gt = tx_power_gt

    params["tx_power__gt"] = json_tx_power_gt

    json_tx_power_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(tx_power_gte, Unset):
        json_tx_power_gte = tx_power_gte

    params["tx_power__gte"] = json_tx_power_gte

    json_tx_power_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(tx_power_lt, Unset):
        json_tx_power_lt = tx_power_lt

    params["tx_power__lt"] = json_tx_power_lt

    json_tx_power_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(tx_power_lte, Unset):
        json_tx_power_lte = tx_power_lte

    params["tx_power__lte"] = json_tx_power_lte

    json_tx_power_n: Union[Unset, List[int]] = UNSET
    if not isinstance(tx_power_n, Unset):
        json_tx_power_n = tx_power_n

    params["tx_power__n"] = json_tx_power_n

    json_type: Union[Unset, List[str]] = UNSET
    if not isinstance(type, Unset):
        json_type = type

    params["type"] = json_type

    json_type_n: Union[Unset, List[str]] = UNSET
    if not isinstance(type_n, Unset):
        json_type_n = type_n

    params["type__n"] = json_type_n

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    json_vdc: Union[Unset, List[str]] = UNSET
    if not isinstance(vdc, Unset):
        json_vdc = vdc

    params["vdc"] = json_vdc

    json_vdc_n: Union[Unset, List[str]] = UNSET
    if not isinstance(vdc_n, Unset):
        json_vdc_n = vdc_n

    params["vdc__n"] = json_vdc_n

    json_vdc_id: Union[Unset, List[int]] = UNSET
    if not isinstance(vdc_id, Unset):
        json_vdc_id = vdc_id

    params["vdc_id"] = json_vdc_id

    json_vdc_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(vdc_id_n, Unset):
        json_vdc_id_n = vdc_id_n

    params["vdc_id__n"] = json_vdc_id_n

    json_vdc_identifier: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(vdc_identifier, Unset):
        json_vdc_identifier = []
        for vdc_identifier_item_data in vdc_identifier:
            vdc_identifier_item: Union[None, int]
            vdc_identifier_item = vdc_identifier_item_data
            json_vdc_identifier.append(vdc_identifier_item)

    params["vdc_identifier"] = json_vdc_identifier

    json_vdc_identifier_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(vdc_identifier_n, Unset):
        json_vdc_identifier_n = []
        for vdc_identifier_n_item_data in vdc_identifier_n:
            vdc_identifier_n_item: Union[None, int]
            vdc_identifier_n_item = vdc_identifier_n_item_data
            json_vdc_identifier_n.append(vdc_identifier_n_item)

    params["vdc_identifier__n"] = json_vdc_identifier_n

    json_virtual_chassis: Union[Unset, List[str]] = UNSET
    if not isinstance(virtual_chassis, Unset):
        json_virtual_chassis = virtual_chassis

    params["virtual_chassis"] = json_virtual_chassis

    json_virtual_chassis_n: Union[Unset, List[str]] = UNSET
    if not isinstance(virtual_chassis_n, Unset):
        json_virtual_chassis_n = virtual_chassis_n

    params["virtual_chassis__n"] = json_virtual_chassis_n

    json_virtual_chassis_id: Union[Unset, List[int]] = UNSET
    if not isinstance(virtual_chassis_id, Unset):
        json_virtual_chassis_id = virtual_chassis_id

    params["virtual_chassis_id"] = json_virtual_chassis_id

    json_virtual_chassis_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(virtual_chassis_id_n, Unset):
        json_virtual_chassis_id_n = virtual_chassis_id_n

    params["virtual_chassis_id__n"] = json_virtual_chassis_id_n

    json_virtual_chassis_member: Union[Unset, List[str]] = UNSET
    if not isinstance(virtual_chassis_member, Unset):
        json_virtual_chassis_member = virtual_chassis_member

    params["virtual_chassis_member"] = json_virtual_chassis_member

    json_virtual_chassis_member_id: Union[Unset, List[int]] = UNSET
    if not isinstance(virtual_chassis_member_id, Unset):
        json_virtual_chassis_member_id = virtual_chassis_member_id

    params["virtual_chassis_member_id"] = json_virtual_chassis_member_id

    params["vlan"] = vlan

    params["vlan_id"] = vlan_id

    json_vrf: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(vrf, Unset):
        json_vrf = []
        for vrf_item_data in vrf:
            vrf_item: Union[None, str]
            vrf_item = vrf_item_data
            json_vrf.append(vrf_item)

    params["vrf"] = json_vrf

    json_vrf_n: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(vrf_n, Unset):
        json_vrf_n = []
        for vrf_n_item_data in vrf_n:
            vrf_n_item: Union[None, str]
            vrf_n_item = vrf_n_item_data
            json_vrf_n.append(vrf_n_item)

    params["vrf__n"] = json_vrf_n

    json_vrf_id: Union[Unset, List[int]] = UNSET
    if not isinstance(vrf_id, Unset):
        json_vrf_id = vrf_id

    params["vrf_id"] = json_vrf_id

    json_vrf_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(vrf_id_n, Unset):
        json_vrf_id_n = vrf_id_n

    params["vrf_id__n"] = json_vrf_id_n

    json_wwn: Union[Unset, List[str]] = UNSET
    if not isinstance(wwn, Unset):
        json_wwn = wwn

    params["wwn"] = json_wwn

    json_wwn_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(wwn_ic, Unset):
        json_wwn_ic = wwn_ic

    params["wwn__ic"] = json_wwn_ic

    json_wwn_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(wwn_ie, Unset):
        json_wwn_ie = wwn_ie

    params["wwn__ie"] = json_wwn_ie

    json_wwn_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(wwn_iew, Unset):
        json_wwn_iew = wwn_iew

    params["wwn__iew"] = json_wwn_iew

    json_wwn_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(wwn_isw, Unset):
        json_wwn_isw = wwn_isw

    params["wwn__isw"] = json_wwn_isw

    json_wwn_n: Union[Unset, List[str]] = UNSET
    if not isinstance(wwn_n, Unset):
        json_wwn_n = wwn_n

    params["wwn__n"] = json_wwn_n

    json_wwn_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(wwn_nic, Unset):
        json_wwn_nic = wwn_nic

    params["wwn__nic"] = json_wwn_nic

    json_wwn_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(wwn_nie, Unset):
        json_wwn_nie = wwn_nie

    params["wwn__nie"] = json_wwn_nie

    json_wwn_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(wwn_niew, Unset):
        json_wwn_niew = wwn_niew

    params["wwn__niew"] = json_wwn_niew

    json_wwn_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(wwn_nisw, Unset):
        json_wwn_nisw = wwn_nisw

    params["wwn__nisw"] = json_wwn_nisw

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/dcim/interfaces/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedInterfaceList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedInterfaceList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedInterfaceList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    bridge_id: Union[Unset, List[int]] = UNSET,
    bridge_id_n: Union[Unset, List[int]] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    cabled: Union[Unset, bool] = UNSET,
    connected: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    duplex: Union[Unset, List[Union[None, str]]] = UNSET,
    duplex_n: Union[Unset, List[Union[None, str]]] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    kind: Union[Unset, str] = UNSET,
    l2vpn: Union[Unset, List[Union[None, int]]] = UNSET,
    l2vpn_n: Union[Unset, List[Union[None, int]]] = UNSET,
    l2vpn_id: Union[Unset, List[int]] = UNSET,
    l2vpn_id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    lag_id: Union[Unset, List[int]] = UNSET,
    lag_id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    mac_address: Union[Unset, List[str]] = UNSET,
    mac_address_ic: Union[Unset, List[str]] = UNSET,
    mac_address_ie: Union[Unset, List[str]] = UNSET,
    mac_address_iew: Union[Unset, List[str]] = UNSET,
    mac_address_isw: Union[Unset, List[str]] = UNSET,
    mac_address_n: Union[Unset, List[str]] = UNSET,
    mac_address_nic: Union[Unset, List[str]] = UNSET,
    mac_address_nie: Union[Unset, List[str]] = UNSET,
    mac_address_niew: Union[Unset, List[str]] = UNSET,
    mac_address_nisw: Union[Unset, List[str]] = UNSET,
    mgmt_only: Union[Unset, bool] = UNSET,
    mode: Union[Unset, str] = UNSET,
    mode_n: Union[Unset, str] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    module_id: Union[Unset, List[Union[None, int]]] = UNSET,
    module_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    mtu: Union[Unset, List[int]] = UNSET,
    mtu_empty: Union[Unset, bool] = UNSET,
    mtu_gt: Union[Unset, List[int]] = UNSET,
    mtu_gte: Union[Unset, List[int]] = UNSET,
    mtu_lt: Union[Unset, List[int]] = UNSET,
    mtu_lte: Union[Unset, List[int]] = UNSET,
    mtu_n: Union[Unset, List[int]] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    occupied: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent_id: Union[Unset, List[int]] = UNSET,
    parent_id_n: Union[Unset, List[int]] = UNSET,
    poe_mode: Union[Unset, List[str]] = UNSET,
    poe_mode_n: Union[Unset, List[str]] = UNSET,
    poe_type: Union[Unset, List[str]] = UNSET,
    poe_type_n: Union[Unset, List[str]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    rf_channel: Union[Unset, List[str]] = UNSET,
    rf_channel_n: Union[Unset, List[str]] = UNSET,
    rf_channel_frequency: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_empty: Union[Unset, bool] = UNSET,
    rf_channel_frequency_gt: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_gte: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_lt: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_lte: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_n: Union[Unset, List[float]] = UNSET,
    rf_channel_width: Union[Unset, List[float]] = UNSET,
    rf_channel_width_empty: Union[Unset, bool] = UNSET,
    rf_channel_width_gt: Union[Unset, List[float]] = UNSET,
    rf_channel_width_gte: Union[Unset, List[float]] = UNSET,
    rf_channel_width_lt: Union[Unset, List[float]] = UNSET,
    rf_channel_width_lte: Union[Unset, List[float]] = UNSET,
    rf_channel_width_n: Union[Unset, List[float]] = UNSET,
    rf_role: Union[Unset, List[str]] = UNSET,
    rf_role_n: Union[Unset, List[str]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[int]] = UNSET,
    role_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    speed: Union[Unset, List[int]] = UNSET,
    speed_empty: Union[Unset, List[int]] = UNSET,
    speed_gt: Union[Unset, List[int]] = UNSET,
    speed_gte: Union[Unset, List[int]] = UNSET,
    speed_lt: Union[Unset, List[int]] = UNSET,
    speed_lte: Union[Unset, List[int]] = UNSET,
    speed_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tx_power: Union[Unset, List[int]] = UNSET,
    tx_power_empty: Union[Unset, bool] = UNSET,
    tx_power_gt: Union[Unset, List[int]] = UNSET,
    tx_power_gte: Union[Unset, List[int]] = UNSET,
    tx_power_lt: Union[Unset, List[int]] = UNSET,
    tx_power_lte: Union[Unset, List[int]] = UNSET,
    tx_power_n: Union[Unset, List[int]] = UNSET,
    type: Union[Unset, List[str]] = UNSET,
    type_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    vdc: Union[Unset, List[str]] = UNSET,
    vdc_n: Union[Unset, List[str]] = UNSET,
    vdc_id: Union[Unset, List[int]] = UNSET,
    vdc_id_n: Union[Unset, List[int]] = UNSET,
    vdc_identifier: Union[Unset, List[Union[None, int]]] = UNSET,
    vdc_identifier_n: Union[Unset, List[Union[None, int]]] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
    virtual_chassis_member: Union[Unset, List[str]] = UNSET,
    virtual_chassis_member_id: Union[Unset, List[int]] = UNSET,
    vlan: Union[Unset, str] = UNSET,
    vlan_id: Union[Unset, str] = UNSET,
    vrf: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_n: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_id: Union[Unset, List[int]] = UNSET,
    vrf_id_n: Union[Unset, List[int]] = UNSET,
    wwn: Union[Unset, List[str]] = UNSET,
    wwn_ic: Union[Unset, List[str]] = UNSET,
    wwn_ie: Union[Unset, List[str]] = UNSET,
    wwn_iew: Union[Unset, List[str]] = UNSET,
    wwn_isw: Union[Unset, List[str]] = UNSET,
    wwn_n: Union[Unset, List[str]] = UNSET,
    wwn_nic: Union[Unset, List[str]] = UNSET,
    wwn_nie: Union[Unset, List[str]] = UNSET,
    wwn_niew: Union[Unset, List[str]] = UNSET,
    wwn_nisw: Union[Unset, List[str]] = UNSET,
) -> Response[PaginatedInterfaceList]:
    """Get a list of interface objects.

    Args:
        bridge_id (Union[Unset, List[int]]):
        bridge_id_n (Union[Unset, List[int]]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        cabled (Union[Unset, bool]):
        connected (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        duplex (Union[Unset, List[Union[None, str]]]):
        duplex_n (Union[Unset, List[Union[None, str]]]):
        enabled (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        kind (Union[Unset, str]):
        l2vpn (Union[Unset, List[Union[None, int]]]):
        l2vpn_n (Union[Unset, List[Union[None, int]]]):
        l2vpn_id (Union[Unset, List[int]]):
        l2vpn_id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        lag_id (Union[Unset, List[int]]):
        lag_id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        mac_address (Union[Unset, List[str]]):
        mac_address_ic (Union[Unset, List[str]]):
        mac_address_ie (Union[Unset, List[str]]):
        mac_address_iew (Union[Unset, List[str]]):
        mac_address_isw (Union[Unset, List[str]]):
        mac_address_n (Union[Unset, List[str]]):
        mac_address_nic (Union[Unset, List[str]]):
        mac_address_nie (Union[Unset, List[str]]):
        mac_address_niew (Union[Unset, List[str]]):
        mac_address_nisw (Union[Unset, List[str]]):
        mgmt_only (Union[Unset, bool]):
        mode (Union[Unset, str]):
        mode_n (Union[Unset, str]):
        modified_by_request (Union[Unset, UUID]):
        module_id (Union[Unset, List[Union[None, int]]]):
        module_id_n (Union[Unset, List[Union[None, int]]]):
        mtu (Union[Unset, List[int]]):
        mtu_empty (Union[Unset, bool]):
        mtu_gt (Union[Unset, List[int]]):
        mtu_gte (Union[Unset, List[int]]):
        mtu_lt (Union[Unset, List[int]]):
        mtu_lte (Union[Unset, List[int]]):
        mtu_n (Union[Unset, List[int]]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        occupied (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        parent_id (Union[Unset, List[int]]):
        parent_id_n (Union[Unset, List[int]]):
        poe_mode (Union[Unset, List[str]]):
        poe_mode_n (Union[Unset, List[str]]):
        poe_type (Union[Unset, List[str]]):
        poe_type_n (Union[Unset, List[str]]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        rf_channel (Union[Unset, List[str]]):
        rf_channel_n (Union[Unset, List[str]]):
        rf_channel_frequency (Union[Unset, List[float]]):
        rf_channel_frequency_empty (Union[Unset, bool]):
        rf_channel_frequency_gt (Union[Unset, List[float]]):
        rf_channel_frequency_gte (Union[Unset, List[float]]):
        rf_channel_frequency_lt (Union[Unset, List[float]]):
        rf_channel_frequency_lte (Union[Unset, List[float]]):
        rf_channel_frequency_n (Union[Unset, List[float]]):
        rf_channel_width (Union[Unset, List[float]]):
        rf_channel_width_empty (Union[Unset, bool]):
        rf_channel_width_gt (Union[Unset, List[float]]):
        rf_channel_width_gte (Union[Unset, List[float]]):
        rf_channel_width_lt (Union[Unset, List[float]]):
        rf_channel_width_lte (Union[Unset, List[float]]):
        rf_channel_width_n (Union[Unset, List[float]]):
        rf_role (Union[Unset, List[str]]):
        rf_role_n (Union[Unset, List[str]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[int]]):
        role_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        speed (Union[Unset, List[int]]):
        speed_empty (Union[Unset, List[int]]):
        speed_gt (Union[Unset, List[int]]):
        speed_gte (Union[Unset, List[int]]):
        speed_lt (Union[Unset, List[int]]):
        speed_lte (Union[Unset, List[int]]):
        speed_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tx_power (Union[Unset, List[int]]):
        tx_power_empty (Union[Unset, bool]):
        tx_power_gt (Union[Unset, List[int]]):
        tx_power_gte (Union[Unset, List[int]]):
        tx_power_lt (Union[Unset, List[int]]):
        tx_power_lte (Union[Unset, List[int]]):
        tx_power_n (Union[Unset, List[int]]):
        type (Union[Unset, List[str]]):
        type_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        vdc (Union[Unset, List[str]]):
        vdc_n (Union[Unset, List[str]]):
        vdc_id (Union[Unset, List[int]]):
        vdc_id_n (Union[Unset, List[int]]):
        vdc_identifier (Union[Unset, List[Union[None, int]]]):
        vdc_identifier_n (Union[Unset, List[Union[None, int]]]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):
        virtual_chassis_member (Union[Unset, List[str]]):
        virtual_chassis_member_id (Union[Unset, List[int]]):
        vlan (Union[Unset, str]):
        vlan_id (Union[Unset, str]):
        vrf (Union[Unset, List[Union[None, str]]]):
        vrf_n (Union[Unset, List[Union[None, str]]]):
        vrf_id (Union[Unset, List[int]]):
        vrf_id_n (Union[Unset, List[int]]):
        wwn (Union[Unset, List[str]]):
        wwn_ic (Union[Unset, List[str]]):
        wwn_ie (Union[Unset, List[str]]):
        wwn_iew (Union[Unset, List[str]]):
        wwn_isw (Union[Unset, List[str]]):
        wwn_n (Union[Unset, List[str]]):
        wwn_nic (Union[Unset, List[str]]):
        wwn_nie (Union[Unset, List[str]]):
        wwn_niew (Union[Unset, List[str]]):
        wwn_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedInterfaceList]
    """

    kwargs = _get_kwargs(
        bridge_id=bridge_id,
        bridge_id_n=bridge_id_n,
        cable_end=cable_end,
        cable_end_n=cable_end_n,
        cabled=cabled,
        connected=connected,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_n=device_n,
        device_id=device_id,
        device_id_n=device_id_n,
        device_role=device_role,
        device_role_n=device_role_n,
        device_role_id=device_role_id,
        device_role_id_n=device_role_id_n,
        device_type=device_type,
        device_type_n=device_type_n,
        device_type_id=device_type_id,
        device_type_id_n=device_type_id_n,
        duplex=duplex,
        duplex_n=duplex_n,
        enabled=enabled,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        kind=kind,
        l2vpn=l2vpn,
        l2vpn_n=l2vpn_n,
        l2vpn_id=l2vpn_id,
        l2vpn_id_n=l2vpn_id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        lag_id=lag_id,
        lag_id_n=lag_id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        location=location,
        location_n=location_n,
        location_id=location_id,
        location_id_n=location_id_n,
        mac_address=mac_address,
        mac_address_ic=mac_address_ic,
        mac_address_ie=mac_address_ie,
        mac_address_iew=mac_address_iew,
        mac_address_isw=mac_address_isw,
        mac_address_n=mac_address_n,
        mac_address_nic=mac_address_nic,
        mac_address_nie=mac_address_nie,
        mac_address_niew=mac_address_niew,
        mac_address_nisw=mac_address_nisw,
        mgmt_only=mgmt_only,
        mode=mode,
        mode_n=mode_n,
        modified_by_request=modified_by_request,
        module_id=module_id,
        module_id_n=module_id_n,
        mtu=mtu,
        mtu_empty=mtu_empty,
        mtu_gt=mtu_gt,
        mtu_gte=mtu_gte,
        mtu_lt=mtu_lt,
        mtu_lte=mtu_lte,
        mtu_n=mtu_n,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        occupied=occupied,
        offset=offset,
        ordering=ordering,
        parent_id=parent_id,
        parent_id_n=parent_id_n,
        poe_mode=poe_mode,
        poe_mode_n=poe_mode_n,
        poe_type=poe_type,
        poe_type_n=poe_type_n,
        q=q,
        rack=rack,
        rack_n=rack_n,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        rf_channel=rf_channel,
        rf_channel_n=rf_channel_n,
        rf_channel_frequency=rf_channel_frequency,
        rf_channel_frequency_empty=rf_channel_frequency_empty,
        rf_channel_frequency_gt=rf_channel_frequency_gt,
        rf_channel_frequency_gte=rf_channel_frequency_gte,
        rf_channel_frequency_lt=rf_channel_frequency_lt,
        rf_channel_frequency_lte=rf_channel_frequency_lte,
        rf_channel_frequency_n=rf_channel_frequency_n,
        rf_channel_width=rf_channel_width,
        rf_channel_width_empty=rf_channel_width_empty,
        rf_channel_width_gt=rf_channel_width_gt,
        rf_channel_width_gte=rf_channel_width_gte,
        rf_channel_width_lt=rf_channel_width_lt,
        rf_channel_width_lte=rf_channel_width_lte,
        rf_channel_width_n=rf_channel_width_n,
        rf_role=rf_role,
        rf_role_n=rf_role_n,
        role=role,
        role_n=role_n,
        role_id=role_id,
        role_id_n=role_id_n,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        speed=speed,
        speed_empty=speed_empty,
        speed_gt=speed_gt,
        speed_gte=speed_gte,
        speed_lt=speed_lt,
        speed_lte=speed_lte,
        speed_n=speed_n,
        tag=tag,
        tag_n=tag_n,
        tx_power=tx_power,
        tx_power_empty=tx_power_empty,
        tx_power_gt=tx_power_gt,
        tx_power_gte=tx_power_gte,
        tx_power_lt=tx_power_lt,
        tx_power_lte=tx_power_lte,
        tx_power_n=tx_power_n,
        type=type,
        type_n=type_n,
        updated_by_request=updated_by_request,
        vdc=vdc,
        vdc_n=vdc_n,
        vdc_id=vdc_id,
        vdc_id_n=vdc_id_n,
        vdc_identifier=vdc_identifier,
        vdc_identifier_n=vdc_identifier_n,
        virtual_chassis=virtual_chassis,
        virtual_chassis_n=virtual_chassis_n,
        virtual_chassis_id=virtual_chassis_id,
        virtual_chassis_id_n=virtual_chassis_id_n,
        virtual_chassis_member=virtual_chassis_member,
        virtual_chassis_member_id=virtual_chassis_member_id,
        vlan=vlan,
        vlan_id=vlan_id,
        vrf=vrf,
        vrf_n=vrf_n,
        vrf_id=vrf_id,
        vrf_id_n=vrf_id_n,
        wwn=wwn,
        wwn_ic=wwn_ic,
        wwn_ie=wwn_ie,
        wwn_iew=wwn_iew,
        wwn_isw=wwn_isw,
        wwn_n=wwn_n,
        wwn_nic=wwn_nic,
        wwn_nie=wwn_nie,
        wwn_niew=wwn_niew,
        wwn_nisw=wwn_nisw,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    bridge_id: Union[Unset, List[int]] = UNSET,
    bridge_id_n: Union[Unset, List[int]] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    cabled: Union[Unset, bool] = UNSET,
    connected: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    duplex: Union[Unset, List[Union[None, str]]] = UNSET,
    duplex_n: Union[Unset, List[Union[None, str]]] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    kind: Union[Unset, str] = UNSET,
    l2vpn: Union[Unset, List[Union[None, int]]] = UNSET,
    l2vpn_n: Union[Unset, List[Union[None, int]]] = UNSET,
    l2vpn_id: Union[Unset, List[int]] = UNSET,
    l2vpn_id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    lag_id: Union[Unset, List[int]] = UNSET,
    lag_id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    mac_address: Union[Unset, List[str]] = UNSET,
    mac_address_ic: Union[Unset, List[str]] = UNSET,
    mac_address_ie: Union[Unset, List[str]] = UNSET,
    mac_address_iew: Union[Unset, List[str]] = UNSET,
    mac_address_isw: Union[Unset, List[str]] = UNSET,
    mac_address_n: Union[Unset, List[str]] = UNSET,
    mac_address_nic: Union[Unset, List[str]] = UNSET,
    mac_address_nie: Union[Unset, List[str]] = UNSET,
    mac_address_niew: Union[Unset, List[str]] = UNSET,
    mac_address_nisw: Union[Unset, List[str]] = UNSET,
    mgmt_only: Union[Unset, bool] = UNSET,
    mode: Union[Unset, str] = UNSET,
    mode_n: Union[Unset, str] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    module_id: Union[Unset, List[Union[None, int]]] = UNSET,
    module_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    mtu: Union[Unset, List[int]] = UNSET,
    mtu_empty: Union[Unset, bool] = UNSET,
    mtu_gt: Union[Unset, List[int]] = UNSET,
    mtu_gte: Union[Unset, List[int]] = UNSET,
    mtu_lt: Union[Unset, List[int]] = UNSET,
    mtu_lte: Union[Unset, List[int]] = UNSET,
    mtu_n: Union[Unset, List[int]] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    occupied: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent_id: Union[Unset, List[int]] = UNSET,
    parent_id_n: Union[Unset, List[int]] = UNSET,
    poe_mode: Union[Unset, List[str]] = UNSET,
    poe_mode_n: Union[Unset, List[str]] = UNSET,
    poe_type: Union[Unset, List[str]] = UNSET,
    poe_type_n: Union[Unset, List[str]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    rf_channel: Union[Unset, List[str]] = UNSET,
    rf_channel_n: Union[Unset, List[str]] = UNSET,
    rf_channel_frequency: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_empty: Union[Unset, bool] = UNSET,
    rf_channel_frequency_gt: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_gte: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_lt: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_lte: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_n: Union[Unset, List[float]] = UNSET,
    rf_channel_width: Union[Unset, List[float]] = UNSET,
    rf_channel_width_empty: Union[Unset, bool] = UNSET,
    rf_channel_width_gt: Union[Unset, List[float]] = UNSET,
    rf_channel_width_gte: Union[Unset, List[float]] = UNSET,
    rf_channel_width_lt: Union[Unset, List[float]] = UNSET,
    rf_channel_width_lte: Union[Unset, List[float]] = UNSET,
    rf_channel_width_n: Union[Unset, List[float]] = UNSET,
    rf_role: Union[Unset, List[str]] = UNSET,
    rf_role_n: Union[Unset, List[str]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[int]] = UNSET,
    role_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    speed: Union[Unset, List[int]] = UNSET,
    speed_empty: Union[Unset, List[int]] = UNSET,
    speed_gt: Union[Unset, List[int]] = UNSET,
    speed_gte: Union[Unset, List[int]] = UNSET,
    speed_lt: Union[Unset, List[int]] = UNSET,
    speed_lte: Union[Unset, List[int]] = UNSET,
    speed_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tx_power: Union[Unset, List[int]] = UNSET,
    tx_power_empty: Union[Unset, bool] = UNSET,
    tx_power_gt: Union[Unset, List[int]] = UNSET,
    tx_power_gte: Union[Unset, List[int]] = UNSET,
    tx_power_lt: Union[Unset, List[int]] = UNSET,
    tx_power_lte: Union[Unset, List[int]] = UNSET,
    tx_power_n: Union[Unset, List[int]] = UNSET,
    type: Union[Unset, List[str]] = UNSET,
    type_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    vdc: Union[Unset, List[str]] = UNSET,
    vdc_n: Union[Unset, List[str]] = UNSET,
    vdc_id: Union[Unset, List[int]] = UNSET,
    vdc_id_n: Union[Unset, List[int]] = UNSET,
    vdc_identifier: Union[Unset, List[Union[None, int]]] = UNSET,
    vdc_identifier_n: Union[Unset, List[Union[None, int]]] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
    virtual_chassis_member: Union[Unset, List[str]] = UNSET,
    virtual_chassis_member_id: Union[Unset, List[int]] = UNSET,
    vlan: Union[Unset, str] = UNSET,
    vlan_id: Union[Unset, str] = UNSET,
    vrf: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_n: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_id: Union[Unset, List[int]] = UNSET,
    vrf_id_n: Union[Unset, List[int]] = UNSET,
    wwn: Union[Unset, List[str]] = UNSET,
    wwn_ic: Union[Unset, List[str]] = UNSET,
    wwn_ie: Union[Unset, List[str]] = UNSET,
    wwn_iew: Union[Unset, List[str]] = UNSET,
    wwn_isw: Union[Unset, List[str]] = UNSET,
    wwn_n: Union[Unset, List[str]] = UNSET,
    wwn_nic: Union[Unset, List[str]] = UNSET,
    wwn_nie: Union[Unset, List[str]] = UNSET,
    wwn_niew: Union[Unset, List[str]] = UNSET,
    wwn_nisw: Union[Unset, List[str]] = UNSET,
) -> Optional[PaginatedInterfaceList]:
    """Get a list of interface objects.

    Args:
        bridge_id (Union[Unset, List[int]]):
        bridge_id_n (Union[Unset, List[int]]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        cabled (Union[Unset, bool]):
        connected (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        duplex (Union[Unset, List[Union[None, str]]]):
        duplex_n (Union[Unset, List[Union[None, str]]]):
        enabled (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        kind (Union[Unset, str]):
        l2vpn (Union[Unset, List[Union[None, int]]]):
        l2vpn_n (Union[Unset, List[Union[None, int]]]):
        l2vpn_id (Union[Unset, List[int]]):
        l2vpn_id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        lag_id (Union[Unset, List[int]]):
        lag_id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        mac_address (Union[Unset, List[str]]):
        mac_address_ic (Union[Unset, List[str]]):
        mac_address_ie (Union[Unset, List[str]]):
        mac_address_iew (Union[Unset, List[str]]):
        mac_address_isw (Union[Unset, List[str]]):
        mac_address_n (Union[Unset, List[str]]):
        mac_address_nic (Union[Unset, List[str]]):
        mac_address_nie (Union[Unset, List[str]]):
        mac_address_niew (Union[Unset, List[str]]):
        mac_address_nisw (Union[Unset, List[str]]):
        mgmt_only (Union[Unset, bool]):
        mode (Union[Unset, str]):
        mode_n (Union[Unset, str]):
        modified_by_request (Union[Unset, UUID]):
        module_id (Union[Unset, List[Union[None, int]]]):
        module_id_n (Union[Unset, List[Union[None, int]]]):
        mtu (Union[Unset, List[int]]):
        mtu_empty (Union[Unset, bool]):
        mtu_gt (Union[Unset, List[int]]):
        mtu_gte (Union[Unset, List[int]]):
        mtu_lt (Union[Unset, List[int]]):
        mtu_lte (Union[Unset, List[int]]):
        mtu_n (Union[Unset, List[int]]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        occupied (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        parent_id (Union[Unset, List[int]]):
        parent_id_n (Union[Unset, List[int]]):
        poe_mode (Union[Unset, List[str]]):
        poe_mode_n (Union[Unset, List[str]]):
        poe_type (Union[Unset, List[str]]):
        poe_type_n (Union[Unset, List[str]]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        rf_channel (Union[Unset, List[str]]):
        rf_channel_n (Union[Unset, List[str]]):
        rf_channel_frequency (Union[Unset, List[float]]):
        rf_channel_frequency_empty (Union[Unset, bool]):
        rf_channel_frequency_gt (Union[Unset, List[float]]):
        rf_channel_frequency_gte (Union[Unset, List[float]]):
        rf_channel_frequency_lt (Union[Unset, List[float]]):
        rf_channel_frequency_lte (Union[Unset, List[float]]):
        rf_channel_frequency_n (Union[Unset, List[float]]):
        rf_channel_width (Union[Unset, List[float]]):
        rf_channel_width_empty (Union[Unset, bool]):
        rf_channel_width_gt (Union[Unset, List[float]]):
        rf_channel_width_gte (Union[Unset, List[float]]):
        rf_channel_width_lt (Union[Unset, List[float]]):
        rf_channel_width_lte (Union[Unset, List[float]]):
        rf_channel_width_n (Union[Unset, List[float]]):
        rf_role (Union[Unset, List[str]]):
        rf_role_n (Union[Unset, List[str]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[int]]):
        role_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        speed (Union[Unset, List[int]]):
        speed_empty (Union[Unset, List[int]]):
        speed_gt (Union[Unset, List[int]]):
        speed_gte (Union[Unset, List[int]]):
        speed_lt (Union[Unset, List[int]]):
        speed_lte (Union[Unset, List[int]]):
        speed_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tx_power (Union[Unset, List[int]]):
        tx_power_empty (Union[Unset, bool]):
        tx_power_gt (Union[Unset, List[int]]):
        tx_power_gte (Union[Unset, List[int]]):
        tx_power_lt (Union[Unset, List[int]]):
        tx_power_lte (Union[Unset, List[int]]):
        tx_power_n (Union[Unset, List[int]]):
        type (Union[Unset, List[str]]):
        type_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        vdc (Union[Unset, List[str]]):
        vdc_n (Union[Unset, List[str]]):
        vdc_id (Union[Unset, List[int]]):
        vdc_id_n (Union[Unset, List[int]]):
        vdc_identifier (Union[Unset, List[Union[None, int]]]):
        vdc_identifier_n (Union[Unset, List[Union[None, int]]]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):
        virtual_chassis_member (Union[Unset, List[str]]):
        virtual_chassis_member_id (Union[Unset, List[int]]):
        vlan (Union[Unset, str]):
        vlan_id (Union[Unset, str]):
        vrf (Union[Unset, List[Union[None, str]]]):
        vrf_n (Union[Unset, List[Union[None, str]]]):
        vrf_id (Union[Unset, List[int]]):
        vrf_id_n (Union[Unset, List[int]]):
        wwn (Union[Unset, List[str]]):
        wwn_ic (Union[Unset, List[str]]):
        wwn_ie (Union[Unset, List[str]]):
        wwn_iew (Union[Unset, List[str]]):
        wwn_isw (Union[Unset, List[str]]):
        wwn_n (Union[Unset, List[str]]):
        wwn_nic (Union[Unset, List[str]]):
        wwn_nie (Union[Unset, List[str]]):
        wwn_niew (Union[Unset, List[str]]):
        wwn_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedInterfaceList
    """

    return sync_detailed(
        client=client,
        bridge_id=bridge_id,
        bridge_id_n=bridge_id_n,
        cable_end=cable_end,
        cable_end_n=cable_end_n,
        cabled=cabled,
        connected=connected,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_n=device_n,
        device_id=device_id,
        device_id_n=device_id_n,
        device_role=device_role,
        device_role_n=device_role_n,
        device_role_id=device_role_id,
        device_role_id_n=device_role_id_n,
        device_type=device_type,
        device_type_n=device_type_n,
        device_type_id=device_type_id,
        device_type_id_n=device_type_id_n,
        duplex=duplex,
        duplex_n=duplex_n,
        enabled=enabled,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        kind=kind,
        l2vpn=l2vpn,
        l2vpn_n=l2vpn_n,
        l2vpn_id=l2vpn_id,
        l2vpn_id_n=l2vpn_id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        lag_id=lag_id,
        lag_id_n=lag_id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        location=location,
        location_n=location_n,
        location_id=location_id,
        location_id_n=location_id_n,
        mac_address=mac_address,
        mac_address_ic=mac_address_ic,
        mac_address_ie=mac_address_ie,
        mac_address_iew=mac_address_iew,
        mac_address_isw=mac_address_isw,
        mac_address_n=mac_address_n,
        mac_address_nic=mac_address_nic,
        mac_address_nie=mac_address_nie,
        mac_address_niew=mac_address_niew,
        mac_address_nisw=mac_address_nisw,
        mgmt_only=mgmt_only,
        mode=mode,
        mode_n=mode_n,
        modified_by_request=modified_by_request,
        module_id=module_id,
        module_id_n=module_id_n,
        mtu=mtu,
        mtu_empty=mtu_empty,
        mtu_gt=mtu_gt,
        mtu_gte=mtu_gte,
        mtu_lt=mtu_lt,
        mtu_lte=mtu_lte,
        mtu_n=mtu_n,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        occupied=occupied,
        offset=offset,
        ordering=ordering,
        parent_id=parent_id,
        parent_id_n=parent_id_n,
        poe_mode=poe_mode,
        poe_mode_n=poe_mode_n,
        poe_type=poe_type,
        poe_type_n=poe_type_n,
        q=q,
        rack=rack,
        rack_n=rack_n,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        rf_channel=rf_channel,
        rf_channel_n=rf_channel_n,
        rf_channel_frequency=rf_channel_frequency,
        rf_channel_frequency_empty=rf_channel_frequency_empty,
        rf_channel_frequency_gt=rf_channel_frequency_gt,
        rf_channel_frequency_gte=rf_channel_frequency_gte,
        rf_channel_frequency_lt=rf_channel_frequency_lt,
        rf_channel_frequency_lte=rf_channel_frequency_lte,
        rf_channel_frequency_n=rf_channel_frequency_n,
        rf_channel_width=rf_channel_width,
        rf_channel_width_empty=rf_channel_width_empty,
        rf_channel_width_gt=rf_channel_width_gt,
        rf_channel_width_gte=rf_channel_width_gte,
        rf_channel_width_lt=rf_channel_width_lt,
        rf_channel_width_lte=rf_channel_width_lte,
        rf_channel_width_n=rf_channel_width_n,
        rf_role=rf_role,
        rf_role_n=rf_role_n,
        role=role,
        role_n=role_n,
        role_id=role_id,
        role_id_n=role_id_n,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        speed=speed,
        speed_empty=speed_empty,
        speed_gt=speed_gt,
        speed_gte=speed_gte,
        speed_lt=speed_lt,
        speed_lte=speed_lte,
        speed_n=speed_n,
        tag=tag,
        tag_n=tag_n,
        tx_power=tx_power,
        tx_power_empty=tx_power_empty,
        tx_power_gt=tx_power_gt,
        tx_power_gte=tx_power_gte,
        tx_power_lt=tx_power_lt,
        tx_power_lte=tx_power_lte,
        tx_power_n=tx_power_n,
        type=type,
        type_n=type_n,
        updated_by_request=updated_by_request,
        vdc=vdc,
        vdc_n=vdc_n,
        vdc_id=vdc_id,
        vdc_id_n=vdc_id_n,
        vdc_identifier=vdc_identifier,
        vdc_identifier_n=vdc_identifier_n,
        virtual_chassis=virtual_chassis,
        virtual_chassis_n=virtual_chassis_n,
        virtual_chassis_id=virtual_chassis_id,
        virtual_chassis_id_n=virtual_chassis_id_n,
        virtual_chassis_member=virtual_chassis_member,
        virtual_chassis_member_id=virtual_chassis_member_id,
        vlan=vlan,
        vlan_id=vlan_id,
        vrf=vrf,
        vrf_n=vrf_n,
        vrf_id=vrf_id,
        vrf_id_n=vrf_id_n,
        wwn=wwn,
        wwn_ic=wwn_ic,
        wwn_ie=wwn_ie,
        wwn_iew=wwn_iew,
        wwn_isw=wwn_isw,
        wwn_n=wwn_n,
        wwn_nic=wwn_nic,
        wwn_nie=wwn_nie,
        wwn_niew=wwn_niew,
        wwn_nisw=wwn_nisw,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    bridge_id: Union[Unset, List[int]] = UNSET,
    bridge_id_n: Union[Unset, List[int]] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    cabled: Union[Unset, bool] = UNSET,
    connected: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    duplex: Union[Unset, List[Union[None, str]]] = UNSET,
    duplex_n: Union[Unset, List[Union[None, str]]] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    kind: Union[Unset, str] = UNSET,
    l2vpn: Union[Unset, List[Union[None, int]]] = UNSET,
    l2vpn_n: Union[Unset, List[Union[None, int]]] = UNSET,
    l2vpn_id: Union[Unset, List[int]] = UNSET,
    l2vpn_id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    lag_id: Union[Unset, List[int]] = UNSET,
    lag_id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    mac_address: Union[Unset, List[str]] = UNSET,
    mac_address_ic: Union[Unset, List[str]] = UNSET,
    mac_address_ie: Union[Unset, List[str]] = UNSET,
    mac_address_iew: Union[Unset, List[str]] = UNSET,
    mac_address_isw: Union[Unset, List[str]] = UNSET,
    mac_address_n: Union[Unset, List[str]] = UNSET,
    mac_address_nic: Union[Unset, List[str]] = UNSET,
    mac_address_nie: Union[Unset, List[str]] = UNSET,
    mac_address_niew: Union[Unset, List[str]] = UNSET,
    mac_address_nisw: Union[Unset, List[str]] = UNSET,
    mgmt_only: Union[Unset, bool] = UNSET,
    mode: Union[Unset, str] = UNSET,
    mode_n: Union[Unset, str] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    module_id: Union[Unset, List[Union[None, int]]] = UNSET,
    module_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    mtu: Union[Unset, List[int]] = UNSET,
    mtu_empty: Union[Unset, bool] = UNSET,
    mtu_gt: Union[Unset, List[int]] = UNSET,
    mtu_gte: Union[Unset, List[int]] = UNSET,
    mtu_lt: Union[Unset, List[int]] = UNSET,
    mtu_lte: Union[Unset, List[int]] = UNSET,
    mtu_n: Union[Unset, List[int]] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    occupied: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent_id: Union[Unset, List[int]] = UNSET,
    parent_id_n: Union[Unset, List[int]] = UNSET,
    poe_mode: Union[Unset, List[str]] = UNSET,
    poe_mode_n: Union[Unset, List[str]] = UNSET,
    poe_type: Union[Unset, List[str]] = UNSET,
    poe_type_n: Union[Unset, List[str]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    rf_channel: Union[Unset, List[str]] = UNSET,
    rf_channel_n: Union[Unset, List[str]] = UNSET,
    rf_channel_frequency: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_empty: Union[Unset, bool] = UNSET,
    rf_channel_frequency_gt: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_gte: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_lt: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_lte: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_n: Union[Unset, List[float]] = UNSET,
    rf_channel_width: Union[Unset, List[float]] = UNSET,
    rf_channel_width_empty: Union[Unset, bool] = UNSET,
    rf_channel_width_gt: Union[Unset, List[float]] = UNSET,
    rf_channel_width_gte: Union[Unset, List[float]] = UNSET,
    rf_channel_width_lt: Union[Unset, List[float]] = UNSET,
    rf_channel_width_lte: Union[Unset, List[float]] = UNSET,
    rf_channel_width_n: Union[Unset, List[float]] = UNSET,
    rf_role: Union[Unset, List[str]] = UNSET,
    rf_role_n: Union[Unset, List[str]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[int]] = UNSET,
    role_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    speed: Union[Unset, List[int]] = UNSET,
    speed_empty: Union[Unset, List[int]] = UNSET,
    speed_gt: Union[Unset, List[int]] = UNSET,
    speed_gte: Union[Unset, List[int]] = UNSET,
    speed_lt: Union[Unset, List[int]] = UNSET,
    speed_lte: Union[Unset, List[int]] = UNSET,
    speed_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tx_power: Union[Unset, List[int]] = UNSET,
    tx_power_empty: Union[Unset, bool] = UNSET,
    tx_power_gt: Union[Unset, List[int]] = UNSET,
    tx_power_gte: Union[Unset, List[int]] = UNSET,
    tx_power_lt: Union[Unset, List[int]] = UNSET,
    tx_power_lte: Union[Unset, List[int]] = UNSET,
    tx_power_n: Union[Unset, List[int]] = UNSET,
    type: Union[Unset, List[str]] = UNSET,
    type_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    vdc: Union[Unset, List[str]] = UNSET,
    vdc_n: Union[Unset, List[str]] = UNSET,
    vdc_id: Union[Unset, List[int]] = UNSET,
    vdc_id_n: Union[Unset, List[int]] = UNSET,
    vdc_identifier: Union[Unset, List[Union[None, int]]] = UNSET,
    vdc_identifier_n: Union[Unset, List[Union[None, int]]] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
    virtual_chassis_member: Union[Unset, List[str]] = UNSET,
    virtual_chassis_member_id: Union[Unset, List[int]] = UNSET,
    vlan: Union[Unset, str] = UNSET,
    vlan_id: Union[Unset, str] = UNSET,
    vrf: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_n: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_id: Union[Unset, List[int]] = UNSET,
    vrf_id_n: Union[Unset, List[int]] = UNSET,
    wwn: Union[Unset, List[str]] = UNSET,
    wwn_ic: Union[Unset, List[str]] = UNSET,
    wwn_ie: Union[Unset, List[str]] = UNSET,
    wwn_iew: Union[Unset, List[str]] = UNSET,
    wwn_isw: Union[Unset, List[str]] = UNSET,
    wwn_n: Union[Unset, List[str]] = UNSET,
    wwn_nic: Union[Unset, List[str]] = UNSET,
    wwn_nie: Union[Unset, List[str]] = UNSET,
    wwn_niew: Union[Unset, List[str]] = UNSET,
    wwn_nisw: Union[Unset, List[str]] = UNSET,
) -> Response[PaginatedInterfaceList]:
    """Get a list of interface objects.

    Args:
        bridge_id (Union[Unset, List[int]]):
        bridge_id_n (Union[Unset, List[int]]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        cabled (Union[Unset, bool]):
        connected (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        duplex (Union[Unset, List[Union[None, str]]]):
        duplex_n (Union[Unset, List[Union[None, str]]]):
        enabled (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        kind (Union[Unset, str]):
        l2vpn (Union[Unset, List[Union[None, int]]]):
        l2vpn_n (Union[Unset, List[Union[None, int]]]):
        l2vpn_id (Union[Unset, List[int]]):
        l2vpn_id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        lag_id (Union[Unset, List[int]]):
        lag_id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        mac_address (Union[Unset, List[str]]):
        mac_address_ic (Union[Unset, List[str]]):
        mac_address_ie (Union[Unset, List[str]]):
        mac_address_iew (Union[Unset, List[str]]):
        mac_address_isw (Union[Unset, List[str]]):
        mac_address_n (Union[Unset, List[str]]):
        mac_address_nic (Union[Unset, List[str]]):
        mac_address_nie (Union[Unset, List[str]]):
        mac_address_niew (Union[Unset, List[str]]):
        mac_address_nisw (Union[Unset, List[str]]):
        mgmt_only (Union[Unset, bool]):
        mode (Union[Unset, str]):
        mode_n (Union[Unset, str]):
        modified_by_request (Union[Unset, UUID]):
        module_id (Union[Unset, List[Union[None, int]]]):
        module_id_n (Union[Unset, List[Union[None, int]]]):
        mtu (Union[Unset, List[int]]):
        mtu_empty (Union[Unset, bool]):
        mtu_gt (Union[Unset, List[int]]):
        mtu_gte (Union[Unset, List[int]]):
        mtu_lt (Union[Unset, List[int]]):
        mtu_lte (Union[Unset, List[int]]):
        mtu_n (Union[Unset, List[int]]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        occupied (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        parent_id (Union[Unset, List[int]]):
        parent_id_n (Union[Unset, List[int]]):
        poe_mode (Union[Unset, List[str]]):
        poe_mode_n (Union[Unset, List[str]]):
        poe_type (Union[Unset, List[str]]):
        poe_type_n (Union[Unset, List[str]]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        rf_channel (Union[Unset, List[str]]):
        rf_channel_n (Union[Unset, List[str]]):
        rf_channel_frequency (Union[Unset, List[float]]):
        rf_channel_frequency_empty (Union[Unset, bool]):
        rf_channel_frequency_gt (Union[Unset, List[float]]):
        rf_channel_frequency_gte (Union[Unset, List[float]]):
        rf_channel_frequency_lt (Union[Unset, List[float]]):
        rf_channel_frequency_lte (Union[Unset, List[float]]):
        rf_channel_frequency_n (Union[Unset, List[float]]):
        rf_channel_width (Union[Unset, List[float]]):
        rf_channel_width_empty (Union[Unset, bool]):
        rf_channel_width_gt (Union[Unset, List[float]]):
        rf_channel_width_gte (Union[Unset, List[float]]):
        rf_channel_width_lt (Union[Unset, List[float]]):
        rf_channel_width_lte (Union[Unset, List[float]]):
        rf_channel_width_n (Union[Unset, List[float]]):
        rf_role (Union[Unset, List[str]]):
        rf_role_n (Union[Unset, List[str]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[int]]):
        role_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        speed (Union[Unset, List[int]]):
        speed_empty (Union[Unset, List[int]]):
        speed_gt (Union[Unset, List[int]]):
        speed_gte (Union[Unset, List[int]]):
        speed_lt (Union[Unset, List[int]]):
        speed_lte (Union[Unset, List[int]]):
        speed_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tx_power (Union[Unset, List[int]]):
        tx_power_empty (Union[Unset, bool]):
        tx_power_gt (Union[Unset, List[int]]):
        tx_power_gte (Union[Unset, List[int]]):
        tx_power_lt (Union[Unset, List[int]]):
        tx_power_lte (Union[Unset, List[int]]):
        tx_power_n (Union[Unset, List[int]]):
        type (Union[Unset, List[str]]):
        type_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        vdc (Union[Unset, List[str]]):
        vdc_n (Union[Unset, List[str]]):
        vdc_id (Union[Unset, List[int]]):
        vdc_id_n (Union[Unset, List[int]]):
        vdc_identifier (Union[Unset, List[Union[None, int]]]):
        vdc_identifier_n (Union[Unset, List[Union[None, int]]]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):
        virtual_chassis_member (Union[Unset, List[str]]):
        virtual_chassis_member_id (Union[Unset, List[int]]):
        vlan (Union[Unset, str]):
        vlan_id (Union[Unset, str]):
        vrf (Union[Unset, List[Union[None, str]]]):
        vrf_n (Union[Unset, List[Union[None, str]]]):
        vrf_id (Union[Unset, List[int]]):
        vrf_id_n (Union[Unset, List[int]]):
        wwn (Union[Unset, List[str]]):
        wwn_ic (Union[Unset, List[str]]):
        wwn_ie (Union[Unset, List[str]]):
        wwn_iew (Union[Unset, List[str]]):
        wwn_isw (Union[Unset, List[str]]):
        wwn_n (Union[Unset, List[str]]):
        wwn_nic (Union[Unset, List[str]]):
        wwn_nie (Union[Unset, List[str]]):
        wwn_niew (Union[Unset, List[str]]):
        wwn_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedInterfaceList]
    """

    kwargs = _get_kwargs(
        bridge_id=bridge_id,
        bridge_id_n=bridge_id_n,
        cable_end=cable_end,
        cable_end_n=cable_end_n,
        cabled=cabled,
        connected=connected,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_n=device_n,
        device_id=device_id,
        device_id_n=device_id_n,
        device_role=device_role,
        device_role_n=device_role_n,
        device_role_id=device_role_id,
        device_role_id_n=device_role_id_n,
        device_type=device_type,
        device_type_n=device_type_n,
        device_type_id=device_type_id,
        device_type_id_n=device_type_id_n,
        duplex=duplex,
        duplex_n=duplex_n,
        enabled=enabled,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        kind=kind,
        l2vpn=l2vpn,
        l2vpn_n=l2vpn_n,
        l2vpn_id=l2vpn_id,
        l2vpn_id_n=l2vpn_id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        lag_id=lag_id,
        lag_id_n=lag_id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        location=location,
        location_n=location_n,
        location_id=location_id,
        location_id_n=location_id_n,
        mac_address=mac_address,
        mac_address_ic=mac_address_ic,
        mac_address_ie=mac_address_ie,
        mac_address_iew=mac_address_iew,
        mac_address_isw=mac_address_isw,
        mac_address_n=mac_address_n,
        mac_address_nic=mac_address_nic,
        mac_address_nie=mac_address_nie,
        mac_address_niew=mac_address_niew,
        mac_address_nisw=mac_address_nisw,
        mgmt_only=mgmt_only,
        mode=mode,
        mode_n=mode_n,
        modified_by_request=modified_by_request,
        module_id=module_id,
        module_id_n=module_id_n,
        mtu=mtu,
        mtu_empty=mtu_empty,
        mtu_gt=mtu_gt,
        mtu_gte=mtu_gte,
        mtu_lt=mtu_lt,
        mtu_lte=mtu_lte,
        mtu_n=mtu_n,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        occupied=occupied,
        offset=offset,
        ordering=ordering,
        parent_id=parent_id,
        parent_id_n=parent_id_n,
        poe_mode=poe_mode,
        poe_mode_n=poe_mode_n,
        poe_type=poe_type,
        poe_type_n=poe_type_n,
        q=q,
        rack=rack,
        rack_n=rack_n,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        rf_channel=rf_channel,
        rf_channel_n=rf_channel_n,
        rf_channel_frequency=rf_channel_frequency,
        rf_channel_frequency_empty=rf_channel_frequency_empty,
        rf_channel_frequency_gt=rf_channel_frequency_gt,
        rf_channel_frequency_gte=rf_channel_frequency_gte,
        rf_channel_frequency_lt=rf_channel_frequency_lt,
        rf_channel_frequency_lte=rf_channel_frequency_lte,
        rf_channel_frequency_n=rf_channel_frequency_n,
        rf_channel_width=rf_channel_width,
        rf_channel_width_empty=rf_channel_width_empty,
        rf_channel_width_gt=rf_channel_width_gt,
        rf_channel_width_gte=rf_channel_width_gte,
        rf_channel_width_lt=rf_channel_width_lt,
        rf_channel_width_lte=rf_channel_width_lte,
        rf_channel_width_n=rf_channel_width_n,
        rf_role=rf_role,
        rf_role_n=rf_role_n,
        role=role,
        role_n=role_n,
        role_id=role_id,
        role_id_n=role_id_n,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        speed=speed,
        speed_empty=speed_empty,
        speed_gt=speed_gt,
        speed_gte=speed_gte,
        speed_lt=speed_lt,
        speed_lte=speed_lte,
        speed_n=speed_n,
        tag=tag,
        tag_n=tag_n,
        tx_power=tx_power,
        tx_power_empty=tx_power_empty,
        tx_power_gt=tx_power_gt,
        tx_power_gte=tx_power_gte,
        tx_power_lt=tx_power_lt,
        tx_power_lte=tx_power_lte,
        tx_power_n=tx_power_n,
        type=type,
        type_n=type_n,
        updated_by_request=updated_by_request,
        vdc=vdc,
        vdc_n=vdc_n,
        vdc_id=vdc_id,
        vdc_id_n=vdc_id_n,
        vdc_identifier=vdc_identifier,
        vdc_identifier_n=vdc_identifier_n,
        virtual_chassis=virtual_chassis,
        virtual_chassis_n=virtual_chassis_n,
        virtual_chassis_id=virtual_chassis_id,
        virtual_chassis_id_n=virtual_chassis_id_n,
        virtual_chassis_member=virtual_chassis_member,
        virtual_chassis_member_id=virtual_chassis_member_id,
        vlan=vlan,
        vlan_id=vlan_id,
        vrf=vrf,
        vrf_n=vrf_n,
        vrf_id=vrf_id,
        vrf_id_n=vrf_id_n,
        wwn=wwn,
        wwn_ic=wwn_ic,
        wwn_ie=wwn_ie,
        wwn_iew=wwn_iew,
        wwn_isw=wwn_isw,
        wwn_n=wwn_n,
        wwn_nic=wwn_nic,
        wwn_nie=wwn_nie,
        wwn_niew=wwn_niew,
        wwn_nisw=wwn_nisw,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    bridge_id: Union[Unset, List[int]] = UNSET,
    bridge_id_n: Union[Unset, List[int]] = UNSET,
    cable_end: Union[Unset, str] = UNSET,
    cable_end_n: Union[Unset, str] = UNSET,
    cabled: Union[Unset, bool] = UNSET,
    connected: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    duplex: Union[Unset, List[Union[None, str]]] = UNSET,
    duplex_n: Union[Unset, List[Union[None, str]]] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    kind: Union[Unset, str] = UNSET,
    l2vpn: Union[Unset, List[Union[None, int]]] = UNSET,
    l2vpn_n: Union[Unset, List[Union[None, int]]] = UNSET,
    l2vpn_id: Union[Unset, List[int]] = UNSET,
    l2vpn_id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    lag_id: Union[Unset, List[int]] = UNSET,
    lag_id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    mac_address: Union[Unset, List[str]] = UNSET,
    mac_address_ic: Union[Unset, List[str]] = UNSET,
    mac_address_ie: Union[Unset, List[str]] = UNSET,
    mac_address_iew: Union[Unset, List[str]] = UNSET,
    mac_address_isw: Union[Unset, List[str]] = UNSET,
    mac_address_n: Union[Unset, List[str]] = UNSET,
    mac_address_nic: Union[Unset, List[str]] = UNSET,
    mac_address_nie: Union[Unset, List[str]] = UNSET,
    mac_address_niew: Union[Unset, List[str]] = UNSET,
    mac_address_nisw: Union[Unset, List[str]] = UNSET,
    mgmt_only: Union[Unset, bool] = UNSET,
    mode: Union[Unset, str] = UNSET,
    mode_n: Union[Unset, str] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    module_id: Union[Unset, List[Union[None, int]]] = UNSET,
    module_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    mtu: Union[Unset, List[int]] = UNSET,
    mtu_empty: Union[Unset, bool] = UNSET,
    mtu_gt: Union[Unset, List[int]] = UNSET,
    mtu_gte: Union[Unset, List[int]] = UNSET,
    mtu_lt: Union[Unset, List[int]] = UNSET,
    mtu_lte: Union[Unset, List[int]] = UNSET,
    mtu_n: Union[Unset, List[int]] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    occupied: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent_id: Union[Unset, List[int]] = UNSET,
    parent_id_n: Union[Unset, List[int]] = UNSET,
    poe_mode: Union[Unset, List[str]] = UNSET,
    poe_mode_n: Union[Unset, List[str]] = UNSET,
    poe_type: Union[Unset, List[str]] = UNSET,
    poe_type_n: Union[Unset, List[str]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    rf_channel: Union[Unset, List[str]] = UNSET,
    rf_channel_n: Union[Unset, List[str]] = UNSET,
    rf_channel_frequency: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_empty: Union[Unset, bool] = UNSET,
    rf_channel_frequency_gt: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_gte: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_lt: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_lte: Union[Unset, List[float]] = UNSET,
    rf_channel_frequency_n: Union[Unset, List[float]] = UNSET,
    rf_channel_width: Union[Unset, List[float]] = UNSET,
    rf_channel_width_empty: Union[Unset, bool] = UNSET,
    rf_channel_width_gt: Union[Unset, List[float]] = UNSET,
    rf_channel_width_gte: Union[Unset, List[float]] = UNSET,
    rf_channel_width_lt: Union[Unset, List[float]] = UNSET,
    rf_channel_width_lte: Union[Unset, List[float]] = UNSET,
    rf_channel_width_n: Union[Unset, List[float]] = UNSET,
    rf_role: Union[Unset, List[str]] = UNSET,
    rf_role_n: Union[Unset, List[str]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[int]] = UNSET,
    role_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    speed: Union[Unset, List[int]] = UNSET,
    speed_empty: Union[Unset, List[int]] = UNSET,
    speed_gt: Union[Unset, List[int]] = UNSET,
    speed_gte: Union[Unset, List[int]] = UNSET,
    speed_lt: Union[Unset, List[int]] = UNSET,
    speed_lte: Union[Unset, List[int]] = UNSET,
    speed_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tx_power: Union[Unset, List[int]] = UNSET,
    tx_power_empty: Union[Unset, bool] = UNSET,
    tx_power_gt: Union[Unset, List[int]] = UNSET,
    tx_power_gte: Union[Unset, List[int]] = UNSET,
    tx_power_lt: Union[Unset, List[int]] = UNSET,
    tx_power_lte: Union[Unset, List[int]] = UNSET,
    tx_power_n: Union[Unset, List[int]] = UNSET,
    type: Union[Unset, List[str]] = UNSET,
    type_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    vdc: Union[Unset, List[str]] = UNSET,
    vdc_n: Union[Unset, List[str]] = UNSET,
    vdc_id: Union[Unset, List[int]] = UNSET,
    vdc_id_n: Union[Unset, List[int]] = UNSET,
    vdc_identifier: Union[Unset, List[Union[None, int]]] = UNSET,
    vdc_identifier_n: Union[Unset, List[Union[None, int]]] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
    virtual_chassis_member: Union[Unset, List[str]] = UNSET,
    virtual_chassis_member_id: Union[Unset, List[int]] = UNSET,
    vlan: Union[Unset, str] = UNSET,
    vlan_id: Union[Unset, str] = UNSET,
    vrf: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_n: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_id: Union[Unset, List[int]] = UNSET,
    vrf_id_n: Union[Unset, List[int]] = UNSET,
    wwn: Union[Unset, List[str]] = UNSET,
    wwn_ic: Union[Unset, List[str]] = UNSET,
    wwn_ie: Union[Unset, List[str]] = UNSET,
    wwn_iew: Union[Unset, List[str]] = UNSET,
    wwn_isw: Union[Unset, List[str]] = UNSET,
    wwn_n: Union[Unset, List[str]] = UNSET,
    wwn_nic: Union[Unset, List[str]] = UNSET,
    wwn_nie: Union[Unset, List[str]] = UNSET,
    wwn_niew: Union[Unset, List[str]] = UNSET,
    wwn_nisw: Union[Unset, List[str]] = UNSET,
) -> Optional[PaginatedInterfaceList]:
    """Get a list of interface objects.

    Args:
        bridge_id (Union[Unset, List[int]]):
        bridge_id_n (Union[Unset, List[int]]):
        cable_end (Union[Unset, str]):
        cable_end_n (Union[Unset, str]):
        cabled (Union[Unset, bool]):
        connected (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        duplex (Union[Unset, List[Union[None, str]]]):
        duplex_n (Union[Unset, List[Union[None, str]]]):
        enabled (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        kind (Union[Unset, str]):
        l2vpn (Union[Unset, List[Union[None, int]]]):
        l2vpn_n (Union[Unset, List[Union[None, int]]]):
        l2vpn_id (Union[Unset, List[int]]):
        l2vpn_id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        lag_id (Union[Unset, List[int]]):
        lag_id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        mac_address (Union[Unset, List[str]]):
        mac_address_ic (Union[Unset, List[str]]):
        mac_address_ie (Union[Unset, List[str]]):
        mac_address_iew (Union[Unset, List[str]]):
        mac_address_isw (Union[Unset, List[str]]):
        mac_address_n (Union[Unset, List[str]]):
        mac_address_nic (Union[Unset, List[str]]):
        mac_address_nie (Union[Unset, List[str]]):
        mac_address_niew (Union[Unset, List[str]]):
        mac_address_nisw (Union[Unset, List[str]]):
        mgmt_only (Union[Unset, bool]):
        mode (Union[Unset, str]):
        mode_n (Union[Unset, str]):
        modified_by_request (Union[Unset, UUID]):
        module_id (Union[Unset, List[Union[None, int]]]):
        module_id_n (Union[Unset, List[Union[None, int]]]):
        mtu (Union[Unset, List[int]]):
        mtu_empty (Union[Unset, bool]):
        mtu_gt (Union[Unset, List[int]]):
        mtu_gte (Union[Unset, List[int]]):
        mtu_lt (Union[Unset, List[int]]):
        mtu_lte (Union[Unset, List[int]]):
        mtu_n (Union[Unset, List[int]]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        occupied (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        parent_id (Union[Unset, List[int]]):
        parent_id_n (Union[Unset, List[int]]):
        poe_mode (Union[Unset, List[str]]):
        poe_mode_n (Union[Unset, List[str]]):
        poe_type (Union[Unset, List[str]]):
        poe_type_n (Union[Unset, List[str]]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        rf_channel (Union[Unset, List[str]]):
        rf_channel_n (Union[Unset, List[str]]):
        rf_channel_frequency (Union[Unset, List[float]]):
        rf_channel_frequency_empty (Union[Unset, bool]):
        rf_channel_frequency_gt (Union[Unset, List[float]]):
        rf_channel_frequency_gte (Union[Unset, List[float]]):
        rf_channel_frequency_lt (Union[Unset, List[float]]):
        rf_channel_frequency_lte (Union[Unset, List[float]]):
        rf_channel_frequency_n (Union[Unset, List[float]]):
        rf_channel_width (Union[Unset, List[float]]):
        rf_channel_width_empty (Union[Unset, bool]):
        rf_channel_width_gt (Union[Unset, List[float]]):
        rf_channel_width_gte (Union[Unset, List[float]]):
        rf_channel_width_lt (Union[Unset, List[float]]):
        rf_channel_width_lte (Union[Unset, List[float]]):
        rf_channel_width_n (Union[Unset, List[float]]):
        rf_role (Union[Unset, List[str]]):
        rf_role_n (Union[Unset, List[str]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[int]]):
        role_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        speed (Union[Unset, List[int]]):
        speed_empty (Union[Unset, List[int]]):
        speed_gt (Union[Unset, List[int]]):
        speed_gte (Union[Unset, List[int]]):
        speed_lt (Union[Unset, List[int]]):
        speed_lte (Union[Unset, List[int]]):
        speed_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tx_power (Union[Unset, List[int]]):
        tx_power_empty (Union[Unset, bool]):
        tx_power_gt (Union[Unset, List[int]]):
        tx_power_gte (Union[Unset, List[int]]):
        tx_power_lt (Union[Unset, List[int]]):
        tx_power_lte (Union[Unset, List[int]]):
        tx_power_n (Union[Unset, List[int]]):
        type (Union[Unset, List[str]]):
        type_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        vdc (Union[Unset, List[str]]):
        vdc_n (Union[Unset, List[str]]):
        vdc_id (Union[Unset, List[int]]):
        vdc_id_n (Union[Unset, List[int]]):
        vdc_identifier (Union[Unset, List[Union[None, int]]]):
        vdc_identifier_n (Union[Unset, List[Union[None, int]]]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):
        virtual_chassis_member (Union[Unset, List[str]]):
        virtual_chassis_member_id (Union[Unset, List[int]]):
        vlan (Union[Unset, str]):
        vlan_id (Union[Unset, str]):
        vrf (Union[Unset, List[Union[None, str]]]):
        vrf_n (Union[Unset, List[Union[None, str]]]):
        vrf_id (Union[Unset, List[int]]):
        vrf_id_n (Union[Unset, List[int]]):
        wwn (Union[Unset, List[str]]):
        wwn_ic (Union[Unset, List[str]]):
        wwn_ie (Union[Unset, List[str]]):
        wwn_iew (Union[Unset, List[str]]):
        wwn_isw (Union[Unset, List[str]]):
        wwn_n (Union[Unset, List[str]]):
        wwn_nic (Union[Unset, List[str]]):
        wwn_nie (Union[Unset, List[str]]):
        wwn_niew (Union[Unset, List[str]]):
        wwn_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedInterfaceList
    """

    return (
        await asyncio_detailed(
            client=client,
            bridge_id=bridge_id,
            bridge_id_n=bridge_id_n,
            cable_end=cable_end,
            cable_end_n=cable_end_n,
            cabled=cabled,
            connected=connected,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            description=description,
            description_empty=description_empty,
            description_ic=description_ic,
            description_ie=description_ie,
            description_iew=description_iew,
            description_isw=description_isw,
            description_n=description_n,
            description_nic=description_nic,
            description_nie=description_nie,
            description_niew=description_niew,
            description_nisw=description_nisw,
            device=device,
            device_n=device_n,
            device_id=device_id,
            device_id_n=device_id_n,
            device_role=device_role,
            device_role_n=device_role_n,
            device_role_id=device_role_id,
            device_role_id_n=device_role_id_n,
            device_type=device_type,
            device_type_n=device_type_n,
            device_type_id=device_type_id,
            device_type_id_n=device_type_id_n,
            duplex=duplex,
            duplex_n=duplex_n,
            enabled=enabled,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            kind=kind,
            l2vpn=l2vpn,
            l2vpn_n=l2vpn_n,
            l2vpn_id=l2vpn_id,
            l2vpn_id_n=l2vpn_id_n,
            label=label,
            label_empty=label_empty,
            label_ic=label_ic,
            label_ie=label_ie,
            label_iew=label_iew,
            label_isw=label_isw,
            label_n=label_n,
            label_nic=label_nic,
            label_nie=label_nie,
            label_niew=label_niew,
            label_nisw=label_nisw,
            lag_id=lag_id,
            lag_id_n=lag_id_n,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            limit=limit,
            location=location,
            location_n=location_n,
            location_id=location_id,
            location_id_n=location_id_n,
            mac_address=mac_address,
            mac_address_ic=mac_address_ic,
            mac_address_ie=mac_address_ie,
            mac_address_iew=mac_address_iew,
            mac_address_isw=mac_address_isw,
            mac_address_n=mac_address_n,
            mac_address_nic=mac_address_nic,
            mac_address_nie=mac_address_nie,
            mac_address_niew=mac_address_niew,
            mac_address_nisw=mac_address_nisw,
            mgmt_only=mgmt_only,
            mode=mode,
            mode_n=mode_n,
            modified_by_request=modified_by_request,
            module_id=module_id,
            module_id_n=module_id_n,
            mtu=mtu,
            mtu_empty=mtu_empty,
            mtu_gt=mtu_gt,
            mtu_gte=mtu_gte,
            mtu_lt=mtu_lt,
            mtu_lte=mtu_lte,
            mtu_n=mtu_n,
            name=name,
            name_empty=name_empty,
            name_ic=name_ic,
            name_ie=name_ie,
            name_iew=name_iew,
            name_isw=name_isw,
            name_n=name_n,
            name_nic=name_nic,
            name_nie=name_nie,
            name_niew=name_niew,
            name_nisw=name_nisw,
            occupied=occupied,
            offset=offset,
            ordering=ordering,
            parent_id=parent_id,
            parent_id_n=parent_id_n,
            poe_mode=poe_mode,
            poe_mode_n=poe_mode_n,
            poe_type=poe_type,
            poe_type_n=poe_type_n,
            q=q,
            rack=rack,
            rack_n=rack_n,
            rack_id=rack_id,
            rack_id_n=rack_id_n,
            region=region,
            region_n=region_n,
            region_id=region_id,
            region_id_n=region_id_n,
            rf_channel=rf_channel,
            rf_channel_n=rf_channel_n,
            rf_channel_frequency=rf_channel_frequency,
            rf_channel_frequency_empty=rf_channel_frequency_empty,
            rf_channel_frequency_gt=rf_channel_frequency_gt,
            rf_channel_frequency_gte=rf_channel_frequency_gte,
            rf_channel_frequency_lt=rf_channel_frequency_lt,
            rf_channel_frequency_lte=rf_channel_frequency_lte,
            rf_channel_frequency_n=rf_channel_frequency_n,
            rf_channel_width=rf_channel_width,
            rf_channel_width_empty=rf_channel_width_empty,
            rf_channel_width_gt=rf_channel_width_gt,
            rf_channel_width_gte=rf_channel_width_gte,
            rf_channel_width_lt=rf_channel_width_lt,
            rf_channel_width_lte=rf_channel_width_lte,
            rf_channel_width_n=rf_channel_width_n,
            rf_role=rf_role,
            rf_role_n=rf_role_n,
            role=role,
            role_n=role_n,
            role_id=role_id,
            role_id_n=role_id_n,
            site=site,
            site_n=site_n,
            site_group=site_group,
            site_group_n=site_group_n,
            site_group_id=site_group_id,
            site_group_id_n=site_group_id_n,
            site_id=site_id,
            site_id_n=site_id_n,
            speed=speed,
            speed_empty=speed_empty,
            speed_gt=speed_gt,
            speed_gte=speed_gte,
            speed_lt=speed_lt,
            speed_lte=speed_lte,
            speed_n=speed_n,
            tag=tag,
            tag_n=tag_n,
            tx_power=tx_power,
            tx_power_empty=tx_power_empty,
            tx_power_gt=tx_power_gt,
            tx_power_gte=tx_power_gte,
            tx_power_lt=tx_power_lt,
            tx_power_lte=tx_power_lte,
            tx_power_n=tx_power_n,
            type=type,
            type_n=type_n,
            updated_by_request=updated_by_request,
            vdc=vdc,
            vdc_n=vdc_n,
            vdc_id=vdc_id,
            vdc_id_n=vdc_id_n,
            vdc_identifier=vdc_identifier,
            vdc_identifier_n=vdc_identifier_n,
            virtual_chassis=virtual_chassis,
            virtual_chassis_n=virtual_chassis_n,
            virtual_chassis_id=virtual_chassis_id,
            virtual_chassis_id_n=virtual_chassis_id_n,
            virtual_chassis_member=virtual_chassis_member,
            virtual_chassis_member_id=virtual_chassis_member_id,
            vlan=vlan,
            vlan_id=vlan_id,
            vrf=vrf,
            vrf_n=vrf_n,
            vrf_id=vrf_id,
            vrf_id_n=vrf_id_n,
            wwn=wwn,
            wwn_ic=wwn_ic,
            wwn_ie=wwn_ie,
            wwn_iew=wwn_iew,
            wwn_isw=wwn_isw,
            wwn_n=wwn_n,
            wwn_nic=wwn_nic,
            wwn_nie=wwn_nie,
            wwn_niew=wwn_niew,
            wwn_nisw=wwn_nisw,
        )
    ).parsed
