from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.inventory_item_template import InventoryItemTemplate
from ...models.inventory_item_template_request import InventoryItemTemplateRequest
from ...types import Response


def _get_kwargs(
    *,
    body: Union[
        List["InventoryItemTemplateRequest"],
        List["InventoryItemTemplateRequest"],
    ],
    _request_content_type: Optional[str] = None,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}

    _kwargs: Dict[str, Any] = {
        "method": "patch",
        "url": "/api/dcim/inventory-item-templates/",
    }

    if _request_content_type is None:
        _body = []
        for body_item_data in body:
            body_item = body_item_data.to_dict()
            _body.append(body_item)

        _kwargs["json"] = _body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "application/json":
        _json_body = []
        for body_item_data in body:
            body_item = body_item_data.to_dict()
            _json_body.append(body_item)

        _kwargs["json"] = _json_body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "multipart/form-data":
        _files_body = []
        for body_item_data in body:
            body_item = body_item_data.to_dict()
            _files_body.append(body_item)

        _kwargs["files"] = _files_body
        headers["Content-Type"] = "multipart/form-data; boundary=+++"

    else:
        raise ValueError("Content type application/json not supported by the server")

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[List["InventoryItemTemplate"]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = []
        _response_200 = response.json()
        for response_200_item_data in _response_200:
            response_200_item = InventoryItemTemplate.from_dict(response_200_item_data)

            response_200.append(response_200_item)

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[List["InventoryItemTemplate"]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    body: Union[
        List["InventoryItemTemplateRequest"],
        List["InventoryItemTemplateRequest"],
    ],
    _request_content_type: Optional[str] = None,
) -> Response[List["InventoryItemTemplate"]]:
    """Patch a list of inventory item template objects.

    Args:
        body (List['InventoryItemTemplateRequest']):
        body (List['InventoryItemTemplateRequest']):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[List['InventoryItemTemplate']]
    """

    kwargs = _get_kwargs(
        body=body,
        _request_content_type=_request_content_type,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    body: Union[
        List["InventoryItemTemplateRequest"],
        List["InventoryItemTemplateRequest"],
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[List["InventoryItemTemplate"]]:
    """Patch a list of inventory item template objects.

    Args:
        body (List['InventoryItemTemplateRequest']):
        body (List['InventoryItemTemplateRequest']):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        List['InventoryItemTemplate']
    """

    return sync_detailed(
        client=client,
        body=body,
        _request_content_type=_request_content_type,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    body: Union[
        List["InventoryItemTemplateRequest"],
        List["InventoryItemTemplateRequest"],
    ],
    _request_content_type: Optional[str] = None,
) -> Response[List["InventoryItemTemplate"]]:
    """Patch a list of inventory item template objects.

    Args:
        body (List['InventoryItemTemplateRequest']):
        body (List['InventoryItemTemplateRequest']):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[List['InventoryItemTemplate']]
    """

    kwargs = _get_kwargs(
        body=body,
        _request_content_type=_request_content_type,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    body: Union[
        List["InventoryItemTemplateRequest"],
        List["InventoryItemTemplateRequest"],
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[List["InventoryItemTemplate"]]:
    """Patch a list of inventory item template objects.

    Args:
        body (List['InventoryItemTemplateRequest']):
        body (List['InventoryItemTemplateRequest']):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        List['InventoryItemTemplate']
    """

    return (
        await asyncio_detailed(
            client=client,
            body=body,
            _request_content_type=_request_content_type,
        )
    ).parsed
