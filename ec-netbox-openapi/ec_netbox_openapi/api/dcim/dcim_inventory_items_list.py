import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_inventory_item_list import PaginatedInventoryItemList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    asset_tag: Union[Unset, List[str]] = UNSET,
    asset_tag_empty: Union[Unset, bool] = UNSET,
    asset_tag_ic: Union[Unset, List[str]] = UNSET,
    asset_tag_ie: Union[Unset, List[str]] = UNSET,
    asset_tag_iew: Union[Unset, List[str]] = UNSET,
    asset_tag_isw: Union[Unset, List[str]] = UNSET,
    asset_tag_n: Union[Unset, List[str]] = UNSET,
    asset_tag_nic: Union[Unset, List[str]] = UNSET,
    asset_tag_nie: Union[Unset, List[str]] = UNSET,
    asset_tag_niew: Union[Unset, List[str]] = UNSET,
    asset_tag_nisw: Union[Unset, List[str]] = UNSET,
    component_id: Union[Unset, List[int]] = UNSET,
    component_id_empty: Union[Unset, List[int]] = UNSET,
    component_id_gt: Union[Unset, List[int]] = UNSET,
    component_id_gte: Union[Unset, List[int]] = UNSET,
    component_id_lt: Union[Unset, List[int]] = UNSET,
    component_id_lte: Union[Unset, List[int]] = UNSET,
    component_id_n: Union[Unset, List[int]] = UNSET,
    component_type: Union[Unset, str] = UNSET,
    component_type_n: Union[Unset, str] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    discovered: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    manufacturer: Union[Unset, List[str]] = UNSET,
    manufacturer_n: Union[Unset, List[str]] = UNSET,
    manufacturer_id: Union[Unset, List[Union[None, int]]] = UNSET,
    manufacturer_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent_id: Union[Unset, List[Union[None, int]]] = UNSET,
    parent_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    part_id: Union[Unset, List[str]] = UNSET,
    part_id_empty: Union[Unset, bool] = UNSET,
    part_id_ic: Union[Unset, List[str]] = UNSET,
    part_id_ie: Union[Unset, List[str]] = UNSET,
    part_id_iew: Union[Unset, List[str]] = UNSET,
    part_id_isw: Union[Unset, List[str]] = UNSET,
    part_id_n: Union[Unset, List[str]] = UNSET,
    part_id_nic: Union[Unset, List[str]] = UNSET,
    part_id_nie: Union[Unset, List[str]] = UNSET,
    part_id_niew: Union[Unset, List[str]] = UNSET,
    part_id_nisw: Union[Unset, List[str]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[Union[None, int]]] = UNSET,
    role_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    serial: Union[Unset, List[str]] = UNSET,
    serial_empty: Union[Unset, bool] = UNSET,
    serial_ic: Union[Unset, List[str]] = UNSET,
    serial_ie: Union[Unset, List[str]] = UNSET,
    serial_iew: Union[Unset, List[str]] = UNSET,
    serial_isw: Union[Unset, List[str]] = UNSET,
    serial_n: Union[Unset, List[str]] = UNSET,
    serial_nic: Union[Unset, List[str]] = UNSET,
    serial_nie: Union[Unset, List[str]] = UNSET,
    serial_niew: Union[Unset, List[str]] = UNSET,
    serial_nisw: Union[Unset, List[str]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_asset_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(asset_tag, Unset):
        json_asset_tag = asset_tag

    params["asset_tag"] = json_asset_tag

    params["asset_tag__empty"] = asset_tag_empty

    json_asset_tag_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(asset_tag_ic, Unset):
        json_asset_tag_ic = asset_tag_ic

    params["asset_tag__ic"] = json_asset_tag_ic

    json_asset_tag_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(asset_tag_ie, Unset):
        json_asset_tag_ie = asset_tag_ie

    params["asset_tag__ie"] = json_asset_tag_ie

    json_asset_tag_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(asset_tag_iew, Unset):
        json_asset_tag_iew = asset_tag_iew

    params["asset_tag__iew"] = json_asset_tag_iew

    json_asset_tag_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(asset_tag_isw, Unset):
        json_asset_tag_isw = asset_tag_isw

    params["asset_tag__isw"] = json_asset_tag_isw

    json_asset_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(asset_tag_n, Unset):
        json_asset_tag_n = asset_tag_n

    params["asset_tag__n"] = json_asset_tag_n

    json_asset_tag_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(asset_tag_nic, Unset):
        json_asset_tag_nic = asset_tag_nic

    params["asset_tag__nic"] = json_asset_tag_nic

    json_asset_tag_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(asset_tag_nie, Unset):
        json_asset_tag_nie = asset_tag_nie

    params["asset_tag__nie"] = json_asset_tag_nie

    json_asset_tag_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(asset_tag_niew, Unset):
        json_asset_tag_niew = asset_tag_niew

    params["asset_tag__niew"] = json_asset_tag_niew

    json_asset_tag_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(asset_tag_nisw, Unset):
        json_asset_tag_nisw = asset_tag_nisw

    params["asset_tag__nisw"] = json_asset_tag_nisw

    json_component_id: Union[Unset, List[int]] = UNSET
    if not isinstance(component_id, Unset):
        json_component_id = component_id

    params["component_id"] = json_component_id

    json_component_id_empty: Union[Unset, List[int]] = UNSET
    if not isinstance(component_id_empty, Unset):
        json_component_id_empty = component_id_empty

    params["component_id__empty"] = json_component_id_empty

    json_component_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(component_id_gt, Unset):
        json_component_id_gt = component_id_gt

    params["component_id__gt"] = json_component_id_gt

    json_component_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(component_id_gte, Unset):
        json_component_id_gte = component_id_gte

    params["component_id__gte"] = json_component_id_gte

    json_component_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(component_id_lt, Unset):
        json_component_id_lt = component_id_lt

    params["component_id__lt"] = json_component_id_lt

    json_component_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(component_id_lte, Unset):
        json_component_id_lte = component_id_lte

    params["component_id__lte"] = json_component_id_lte

    json_component_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(component_id_n, Unset):
        json_component_id_n = component_id_n

    params["component_id__n"] = json_component_id_n

    params["component_type"] = component_type

    params["component_type__n"] = component_type_n

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    json_device: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(device, Unset):
        json_device = []
        for device_item_data in device:
            device_item: Union[None, str]
            device_item = device_item_data
            json_device.append(device_item)

    params["device"] = json_device

    json_device_n: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(device_n, Unset):
        json_device_n = []
        for device_n_item_data in device_n:
            device_n_item: Union[None, str]
            device_n_item = device_n_item_data
            json_device_n.append(device_n_item)

    params["device__n"] = json_device_n

    json_device_id: Union[Unset, List[int]] = UNSET
    if not isinstance(device_id, Unset):
        json_device_id = device_id

    params["device_id"] = json_device_id

    json_device_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(device_id_n, Unset):
        json_device_id_n = device_id_n

    params["device_id__n"] = json_device_id_n

    json_device_role: Union[Unset, List[str]] = UNSET
    if not isinstance(device_role, Unset):
        json_device_role = device_role

    params["device_role"] = json_device_role

    json_device_role_n: Union[Unset, List[str]] = UNSET
    if not isinstance(device_role_n, Unset):
        json_device_role_n = device_role_n

    params["device_role__n"] = json_device_role_n

    json_device_role_id: Union[Unset, List[int]] = UNSET
    if not isinstance(device_role_id, Unset):
        json_device_role_id = device_role_id

    params["device_role_id"] = json_device_role_id

    json_device_role_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(device_role_id_n, Unset):
        json_device_role_id_n = device_role_id_n

    params["device_role_id__n"] = json_device_role_id_n

    json_device_type: Union[Unset, List[str]] = UNSET
    if not isinstance(device_type, Unset):
        json_device_type = device_type

    params["device_type"] = json_device_type

    json_device_type_n: Union[Unset, List[str]] = UNSET
    if not isinstance(device_type_n, Unset):
        json_device_type_n = device_type_n

    params["device_type__n"] = json_device_type_n

    json_device_type_id: Union[Unset, List[int]] = UNSET
    if not isinstance(device_type_id, Unset):
        json_device_type_id = device_type_id

    params["device_type_id"] = json_device_type_id

    json_device_type_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(device_type_id_n, Unset):
        json_device_type_id_n = device_type_id_n

    params["device_type_id__n"] = json_device_type_id_n

    params["discovered"] = discovered

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    json_label: Union[Unset, List[str]] = UNSET
    if not isinstance(label, Unset):
        json_label = label

    params["label"] = json_label

    params["label__empty"] = label_empty

    json_label_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(label_ic, Unset):
        json_label_ic = label_ic

    params["label__ic"] = json_label_ic

    json_label_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(label_ie, Unset):
        json_label_ie = label_ie

    params["label__ie"] = json_label_ie

    json_label_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(label_iew, Unset):
        json_label_iew = label_iew

    params["label__iew"] = json_label_iew

    json_label_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(label_isw, Unset):
        json_label_isw = label_isw

    params["label__isw"] = json_label_isw

    json_label_n: Union[Unset, List[str]] = UNSET
    if not isinstance(label_n, Unset):
        json_label_n = label_n

    params["label__n"] = json_label_n

    json_label_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nic, Unset):
        json_label_nic = label_nic

    params["label__nic"] = json_label_nic

    json_label_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nie, Unset):
        json_label_nie = label_nie

    params["label__nie"] = json_label_nie

    json_label_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(label_niew, Unset):
        json_label_niew = label_niew

    params["label__niew"] = json_label_niew

    json_label_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nisw, Unset):
        json_label_nisw = label_nisw

    params["label__nisw"] = json_label_nisw

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    params["limit"] = limit

    json_location: Union[Unset, List[str]] = UNSET
    if not isinstance(location, Unset):
        json_location = location

    params["location"] = json_location

    json_location_n: Union[Unset, List[str]] = UNSET
    if not isinstance(location_n, Unset):
        json_location_n = location_n

    params["location__n"] = json_location_n

    json_location_id: Union[Unset, List[int]] = UNSET
    if not isinstance(location_id, Unset):
        json_location_id = location_id

    params["location_id"] = json_location_id

    json_location_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(location_id_n, Unset):
        json_location_id_n = location_id_n

    params["location_id__n"] = json_location_id_n

    json_manufacturer: Union[Unset, List[str]] = UNSET
    if not isinstance(manufacturer, Unset):
        json_manufacturer = manufacturer

    params["manufacturer"] = json_manufacturer

    json_manufacturer_n: Union[Unset, List[str]] = UNSET
    if not isinstance(manufacturer_n, Unset):
        json_manufacturer_n = manufacturer_n

    params["manufacturer__n"] = json_manufacturer_n

    json_manufacturer_id: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(manufacturer_id, Unset):
        json_manufacturer_id = []
        for manufacturer_id_item_data in manufacturer_id:
            manufacturer_id_item: Union[None, int]
            manufacturer_id_item = manufacturer_id_item_data
            json_manufacturer_id.append(manufacturer_id_item)

    params["manufacturer_id"] = json_manufacturer_id

    json_manufacturer_id_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(manufacturer_id_n, Unset):
        json_manufacturer_id_n = []
        for manufacturer_id_n_item_data in manufacturer_id_n:
            manufacturer_id_n_item: Union[None, int]
            manufacturer_id_n_item = manufacturer_id_n_item_data
            json_manufacturer_id_n.append(manufacturer_id_n_item)

    params["manufacturer_id__n"] = json_manufacturer_id_n

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    json_name: Union[Unset, List[str]] = UNSET
    if not isinstance(name, Unset):
        json_name = name

    params["name"] = json_name

    params["name__empty"] = name_empty

    json_name_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ic, Unset):
        json_name_ic = name_ic

    params["name__ic"] = json_name_ic

    json_name_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ie, Unset):
        json_name_ie = name_ie

    params["name__ie"] = json_name_ie

    json_name_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_iew, Unset):
        json_name_iew = name_iew

    params["name__iew"] = json_name_iew

    json_name_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_isw, Unset):
        json_name_isw = name_isw

    params["name__isw"] = json_name_isw

    json_name_n: Union[Unset, List[str]] = UNSET
    if not isinstance(name_n, Unset):
        json_name_n = name_n

    params["name__n"] = json_name_n

    json_name_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nic, Unset):
        json_name_nic = name_nic

    params["name__nic"] = json_name_nic

    json_name_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nie, Unset):
        json_name_nie = name_nie

    params["name__nie"] = json_name_nie

    json_name_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_niew, Unset):
        json_name_niew = name_niew

    params["name__niew"] = json_name_niew

    json_name_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nisw, Unset):
        json_name_nisw = name_nisw

    params["name__nisw"] = json_name_nisw

    params["offset"] = offset

    params["ordering"] = ordering

    json_parent_id: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(parent_id, Unset):
        json_parent_id = []
        for parent_id_item_data in parent_id:
            parent_id_item: Union[None, int]
            parent_id_item = parent_id_item_data
            json_parent_id.append(parent_id_item)

    params["parent_id"] = json_parent_id

    json_parent_id_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(parent_id_n, Unset):
        json_parent_id_n = []
        for parent_id_n_item_data in parent_id_n:
            parent_id_n_item: Union[None, int]
            parent_id_n_item = parent_id_n_item_data
            json_parent_id_n.append(parent_id_n_item)

    params["parent_id__n"] = json_parent_id_n

    json_part_id: Union[Unset, List[str]] = UNSET
    if not isinstance(part_id, Unset):
        json_part_id = part_id

    params["part_id"] = json_part_id

    params["part_id__empty"] = part_id_empty

    json_part_id_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(part_id_ic, Unset):
        json_part_id_ic = part_id_ic

    params["part_id__ic"] = json_part_id_ic

    json_part_id_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(part_id_ie, Unset):
        json_part_id_ie = part_id_ie

    params["part_id__ie"] = json_part_id_ie

    json_part_id_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(part_id_iew, Unset):
        json_part_id_iew = part_id_iew

    params["part_id__iew"] = json_part_id_iew

    json_part_id_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(part_id_isw, Unset):
        json_part_id_isw = part_id_isw

    params["part_id__isw"] = json_part_id_isw

    json_part_id_n: Union[Unset, List[str]] = UNSET
    if not isinstance(part_id_n, Unset):
        json_part_id_n = part_id_n

    params["part_id__n"] = json_part_id_n

    json_part_id_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(part_id_nic, Unset):
        json_part_id_nic = part_id_nic

    params["part_id__nic"] = json_part_id_nic

    json_part_id_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(part_id_nie, Unset):
        json_part_id_nie = part_id_nie

    params["part_id__nie"] = json_part_id_nie

    json_part_id_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(part_id_niew, Unset):
        json_part_id_niew = part_id_niew

    params["part_id__niew"] = json_part_id_niew

    json_part_id_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(part_id_nisw, Unset):
        json_part_id_nisw = part_id_nisw

    params["part_id__nisw"] = json_part_id_nisw

    params["q"] = q

    json_rack: Union[Unset, List[str]] = UNSET
    if not isinstance(rack, Unset):
        json_rack = rack

    params["rack"] = json_rack

    json_rack_n: Union[Unset, List[str]] = UNSET
    if not isinstance(rack_n, Unset):
        json_rack_n = rack_n

    params["rack__n"] = json_rack_n

    json_rack_id: Union[Unset, List[int]] = UNSET
    if not isinstance(rack_id, Unset):
        json_rack_id = rack_id

    params["rack_id"] = json_rack_id

    json_rack_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(rack_id_n, Unset):
        json_rack_id_n = rack_id_n

    params["rack_id__n"] = json_rack_id_n

    json_region: Union[Unset, List[int]] = UNSET
    if not isinstance(region, Unset):
        json_region = region

    params["region"] = json_region

    json_region_n: Union[Unset, List[int]] = UNSET
    if not isinstance(region_n, Unset):
        json_region_n = region_n

    params["region__n"] = json_region_n

    json_region_id: Union[Unset, List[int]] = UNSET
    if not isinstance(region_id, Unset):
        json_region_id = region_id

    params["region_id"] = json_region_id

    json_region_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(region_id_n, Unset):
        json_region_id_n = region_id_n

    params["region_id__n"] = json_region_id_n

    json_role: Union[Unset, List[str]] = UNSET
    if not isinstance(role, Unset):
        json_role = role

    params["role"] = json_role

    json_role_n: Union[Unset, List[str]] = UNSET
    if not isinstance(role_n, Unset):
        json_role_n = role_n

    params["role__n"] = json_role_n

    json_role_id: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(role_id, Unset):
        json_role_id = []
        for role_id_item_data in role_id:
            role_id_item: Union[None, int]
            role_id_item = role_id_item_data
            json_role_id.append(role_id_item)

    params["role_id"] = json_role_id

    json_role_id_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(role_id_n, Unset):
        json_role_id_n = []
        for role_id_n_item_data in role_id_n:
            role_id_n_item: Union[None, int]
            role_id_n_item = role_id_n_item_data
            json_role_id_n.append(role_id_n_item)

    params["role_id__n"] = json_role_id_n

    json_serial: Union[Unset, List[str]] = UNSET
    if not isinstance(serial, Unset):
        json_serial = serial

    params["serial"] = json_serial

    params["serial__empty"] = serial_empty

    json_serial_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(serial_ic, Unset):
        json_serial_ic = serial_ic

    params["serial__ic"] = json_serial_ic

    json_serial_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(serial_ie, Unset):
        json_serial_ie = serial_ie

    params["serial__ie"] = json_serial_ie

    json_serial_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(serial_iew, Unset):
        json_serial_iew = serial_iew

    params["serial__iew"] = json_serial_iew

    json_serial_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(serial_isw, Unset):
        json_serial_isw = serial_isw

    params["serial__isw"] = json_serial_isw

    json_serial_n: Union[Unset, List[str]] = UNSET
    if not isinstance(serial_n, Unset):
        json_serial_n = serial_n

    params["serial__n"] = json_serial_n

    json_serial_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(serial_nic, Unset):
        json_serial_nic = serial_nic

    params["serial__nic"] = json_serial_nic

    json_serial_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(serial_nie, Unset):
        json_serial_nie = serial_nie

    params["serial__nie"] = json_serial_nie

    json_serial_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(serial_niew, Unset):
        json_serial_niew = serial_niew

    params["serial__niew"] = json_serial_niew

    json_serial_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(serial_nisw, Unset):
        json_serial_nisw = serial_nisw

    params["serial__nisw"] = json_serial_nisw

    json_site: Union[Unset, List[str]] = UNSET
    if not isinstance(site, Unset):
        json_site = site

    params["site"] = json_site

    json_site_n: Union[Unset, List[str]] = UNSET
    if not isinstance(site_n, Unset):
        json_site_n = site_n

    params["site__n"] = json_site_n

    json_site_group: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group, Unset):
        json_site_group = site_group

    params["site_group"] = json_site_group

    json_site_group_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_n, Unset):
        json_site_group_n = site_group_n

    params["site_group__n"] = json_site_group_n

    json_site_group_id: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_id, Unset):
        json_site_group_id = site_group_id

    params["site_group_id"] = json_site_group_id

    json_site_group_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_id_n, Unset):
        json_site_group_id_n = site_group_id_n

    params["site_group_id__n"] = json_site_group_id_n

    json_site_id: Union[Unset, List[int]] = UNSET
    if not isinstance(site_id, Unset):
        json_site_id = site_id

    params["site_id"] = json_site_id

    json_site_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_id_n, Unset):
        json_site_id_n = site_id_n

    params["site_id__n"] = json_site_id_n

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    json_virtual_chassis: Union[Unset, List[str]] = UNSET
    if not isinstance(virtual_chassis, Unset):
        json_virtual_chassis = virtual_chassis

    params["virtual_chassis"] = json_virtual_chassis

    json_virtual_chassis_n: Union[Unset, List[str]] = UNSET
    if not isinstance(virtual_chassis_n, Unset):
        json_virtual_chassis_n = virtual_chassis_n

    params["virtual_chassis__n"] = json_virtual_chassis_n

    json_virtual_chassis_id: Union[Unset, List[int]] = UNSET
    if not isinstance(virtual_chassis_id, Unset):
        json_virtual_chassis_id = virtual_chassis_id

    params["virtual_chassis_id"] = json_virtual_chassis_id

    json_virtual_chassis_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(virtual_chassis_id_n, Unset):
        json_virtual_chassis_id_n = virtual_chassis_id_n

    params["virtual_chassis_id__n"] = json_virtual_chassis_id_n

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/dcim/inventory-items/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedInventoryItemList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedInventoryItemList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedInventoryItemList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    asset_tag: Union[Unset, List[str]] = UNSET,
    asset_tag_empty: Union[Unset, bool] = UNSET,
    asset_tag_ic: Union[Unset, List[str]] = UNSET,
    asset_tag_ie: Union[Unset, List[str]] = UNSET,
    asset_tag_iew: Union[Unset, List[str]] = UNSET,
    asset_tag_isw: Union[Unset, List[str]] = UNSET,
    asset_tag_n: Union[Unset, List[str]] = UNSET,
    asset_tag_nic: Union[Unset, List[str]] = UNSET,
    asset_tag_nie: Union[Unset, List[str]] = UNSET,
    asset_tag_niew: Union[Unset, List[str]] = UNSET,
    asset_tag_nisw: Union[Unset, List[str]] = UNSET,
    component_id: Union[Unset, List[int]] = UNSET,
    component_id_empty: Union[Unset, List[int]] = UNSET,
    component_id_gt: Union[Unset, List[int]] = UNSET,
    component_id_gte: Union[Unset, List[int]] = UNSET,
    component_id_lt: Union[Unset, List[int]] = UNSET,
    component_id_lte: Union[Unset, List[int]] = UNSET,
    component_id_n: Union[Unset, List[int]] = UNSET,
    component_type: Union[Unset, str] = UNSET,
    component_type_n: Union[Unset, str] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    discovered: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    manufacturer: Union[Unset, List[str]] = UNSET,
    manufacturer_n: Union[Unset, List[str]] = UNSET,
    manufacturer_id: Union[Unset, List[Union[None, int]]] = UNSET,
    manufacturer_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent_id: Union[Unset, List[Union[None, int]]] = UNSET,
    parent_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    part_id: Union[Unset, List[str]] = UNSET,
    part_id_empty: Union[Unset, bool] = UNSET,
    part_id_ic: Union[Unset, List[str]] = UNSET,
    part_id_ie: Union[Unset, List[str]] = UNSET,
    part_id_iew: Union[Unset, List[str]] = UNSET,
    part_id_isw: Union[Unset, List[str]] = UNSET,
    part_id_n: Union[Unset, List[str]] = UNSET,
    part_id_nic: Union[Unset, List[str]] = UNSET,
    part_id_nie: Union[Unset, List[str]] = UNSET,
    part_id_niew: Union[Unset, List[str]] = UNSET,
    part_id_nisw: Union[Unset, List[str]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[Union[None, int]]] = UNSET,
    role_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    serial: Union[Unset, List[str]] = UNSET,
    serial_empty: Union[Unset, bool] = UNSET,
    serial_ic: Union[Unset, List[str]] = UNSET,
    serial_ie: Union[Unset, List[str]] = UNSET,
    serial_iew: Union[Unset, List[str]] = UNSET,
    serial_isw: Union[Unset, List[str]] = UNSET,
    serial_n: Union[Unset, List[str]] = UNSET,
    serial_nic: Union[Unset, List[str]] = UNSET,
    serial_nie: Union[Unset, List[str]] = UNSET,
    serial_niew: Union[Unset, List[str]] = UNSET,
    serial_nisw: Union[Unset, List[str]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
) -> Response[PaginatedInventoryItemList]:
    """Get a list of inventory item objects.

    Args:
        asset_tag (Union[Unset, List[str]]):
        asset_tag_empty (Union[Unset, bool]):
        asset_tag_ic (Union[Unset, List[str]]):
        asset_tag_ie (Union[Unset, List[str]]):
        asset_tag_iew (Union[Unset, List[str]]):
        asset_tag_isw (Union[Unset, List[str]]):
        asset_tag_n (Union[Unset, List[str]]):
        asset_tag_nic (Union[Unset, List[str]]):
        asset_tag_nie (Union[Unset, List[str]]):
        asset_tag_niew (Union[Unset, List[str]]):
        asset_tag_nisw (Union[Unset, List[str]]):
        component_id (Union[Unset, List[int]]):
        component_id_empty (Union[Unset, List[int]]):
        component_id_gt (Union[Unset, List[int]]):
        component_id_gte (Union[Unset, List[int]]):
        component_id_lt (Union[Unset, List[int]]):
        component_id_lte (Union[Unset, List[int]]):
        component_id_n (Union[Unset, List[int]]):
        component_type (Union[Unset, str]):
        component_type_n (Union[Unset, str]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        discovered (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        manufacturer (Union[Unset, List[str]]):
        manufacturer_n (Union[Unset, List[str]]):
        manufacturer_id (Union[Unset, List[Union[None, int]]]):
        manufacturer_id_n (Union[Unset, List[Union[None, int]]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        parent_id (Union[Unset, List[Union[None, int]]]):
        parent_id_n (Union[Unset, List[Union[None, int]]]):
        part_id (Union[Unset, List[str]]):
        part_id_empty (Union[Unset, bool]):
        part_id_ic (Union[Unset, List[str]]):
        part_id_ie (Union[Unset, List[str]]):
        part_id_iew (Union[Unset, List[str]]):
        part_id_isw (Union[Unset, List[str]]):
        part_id_n (Union[Unset, List[str]]):
        part_id_nic (Union[Unset, List[str]]):
        part_id_nie (Union[Unset, List[str]]):
        part_id_niew (Union[Unset, List[str]]):
        part_id_nisw (Union[Unset, List[str]]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[Union[None, int]]]):
        role_id_n (Union[Unset, List[Union[None, int]]]):
        serial (Union[Unset, List[str]]):
        serial_empty (Union[Unset, bool]):
        serial_ic (Union[Unset, List[str]]):
        serial_ie (Union[Unset, List[str]]):
        serial_iew (Union[Unset, List[str]]):
        serial_isw (Union[Unset, List[str]]):
        serial_n (Union[Unset, List[str]]):
        serial_nic (Union[Unset, List[str]]):
        serial_nie (Union[Unset, List[str]]):
        serial_niew (Union[Unset, List[str]]):
        serial_nisw (Union[Unset, List[str]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedInventoryItemList]
    """

    kwargs = _get_kwargs(
        asset_tag=asset_tag,
        asset_tag_empty=asset_tag_empty,
        asset_tag_ic=asset_tag_ic,
        asset_tag_ie=asset_tag_ie,
        asset_tag_iew=asset_tag_iew,
        asset_tag_isw=asset_tag_isw,
        asset_tag_n=asset_tag_n,
        asset_tag_nic=asset_tag_nic,
        asset_tag_nie=asset_tag_nie,
        asset_tag_niew=asset_tag_niew,
        asset_tag_nisw=asset_tag_nisw,
        component_id=component_id,
        component_id_empty=component_id_empty,
        component_id_gt=component_id_gt,
        component_id_gte=component_id_gte,
        component_id_lt=component_id_lt,
        component_id_lte=component_id_lte,
        component_id_n=component_id_n,
        component_type=component_type,
        component_type_n=component_type_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        device=device,
        device_n=device_n,
        device_id=device_id,
        device_id_n=device_id_n,
        device_role=device_role,
        device_role_n=device_role_n,
        device_role_id=device_role_id,
        device_role_id_n=device_role_id_n,
        device_type=device_type,
        device_type_n=device_type_n,
        device_type_id=device_type_id,
        device_type_id_n=device_type_id_n,
        discovered=discovered,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        location=location,
        location_n=location_n,
        location_id=location_id,
        location_id_n=location_id_n,
        manufacturer=manufacturer,
        manufacturer_n=manufacturer_n,
        manufacturer_id=manufacturer_id,
        manufacturer_id_n=manufacturer_id_n,
        modified_by_request=modified_by_request,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        parent_id=parent_id,
        parent_id_n=parent_id_n,
        part_id=part_id,
        part_id_empty=part_id_empty,
        part_id_ic=part_id_ic,
        part_id_ie=part_id_ie,
        part_id_iew=part_id_iew,
        part_id_isw=part_id_isw,
        part_id_n=part_id_n,
        part_id_nic=part_id_nic,
        part_id_nie=part_id_nie,
        part_id_niew=part_id_niew,
        part_id_nisw=part_id_nisw,
        q=q,
        rack=rack,
        rack_n=rack_n,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        role=role,
        role_n=role_n,
        role_id=role_id,
        role_id_n=role_id_n,
        serial=serial,
        serial_empty=serial_empty,
        serial_ic=serial_ic,
        serial_ie=serial_ie,
        serial_iew=serial_iew,
        serial_isw=serial_isw,
        serial_n=serial_n,
        serial_nic=serial_nic,
        serial_nie=serial_nie,
        serial_niew=serial_niew,
        serial_nisw=serial_nisw,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        virtual_chassis=virtual_chassis,
        virtual_chassis_n=virtual_chassis_n,
        virtual_chassis_id=virtual_chassis_id,
        virtual_chassis_id_n=virtual_chassis_id_n,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    asset_tag: Union[Unset, List[str]] = UNSET,
    asset_tag_empty: Union[Unset, bool] = UNSET,
    asset_tag_ic: Union[Unset, List[str]] = UNSET,
    asset_tag_ie: Union[Unset, List[str]] = UNSET,
    asset_tag_iew: Union[Unset, List[str]] = UNSET,
    asset_tag_isw: Union[Unset, List[str]] = UNSET,
    asset_tag_n: Union[Unset, List[str]] = UNSET,
    asset_tag_nic: Union[Unset, List[str]] = UNSET,
    asset_tag_nie: Union[Unset, List[str]] = UNSET,
    asset_tag_niew: Union[Unset, List[str]] = UNSET,
    asset_tag_nisw: Union[Unset, List[str]] = UNSET,
    component_id: Union[Unset, List[int]] = UNSET,
    component_id_empty: Union[Unset, List[int]] = UNSET,
    component_id_gt: Union[Unset, List[int]] = UNSET,
    component_id_gte: Union[Unset, List[int]] = UNSET,
    component_id_lt: Union[Unset, List[int]] = UNSET,
    component_id_lte: Union[Unset, List[int]] = UNSET,
    component_id_n: Union[Unset, List[int]] = UNSET,
    component_type: Union[Unset, str] = UNSET,
    component_type_n: Union[Unset, str] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    discovered: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    manufacturer: Union[Unset, List[str]] = UNSET,
    manufacturer_n: Union[Unset, List[str]] = UNSET,
    manufacturer_id: Union[Unset, List[Union[None, int]]] = UNSET,
    manufacturer_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent_id: Union[Unset, List[Union[None, int]]] = UNSET,
    parent_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    part_id: Union[Unset, List[str]] = UNSET,
    part_id_empty: Union[Unset, bool] = UNSET,
    part_id_ic: Union[Unset, List[str]] = UNSET,
    part_id_ie: Union[Unset, List[str]] = UNSET,
    part_id_iew: Union[Unset, List[str]] = UNSET,
    part_id_isw: Union[Unset, List[str]] = UNSET,
    part_id_n: Union[Unset, List[str]] = UNSET,
    part_id_nic: Union[Unset, List[str]] = UNSET,
    part_id_nie: Union[Unset, List[str]] = UNSET,
    part_id_niew: Union[Unset, List[str]] = UNSET,
    part_id_nisw: Union[Unset, List[str]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[Union[None, int]]] = UNSET,
    role_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    serial: Union[Unset, List[str]] = UNSET,
    serial_empty: Union[Unset, bool] = UNSET,
    serial_ic: Union[Unset, List[str]] = UNSET,
    serial_ie: Union[Unset, List[str]] = UNSET,
    serial_iew: Union[Unset, List[str]] = UNSET,
    serial_isw: Union[Unset, List[str]] = UNSET,
    serial_n: Union[Unset, List[str]] = UNSET,
    serial_nic: Union[Unset, List[str]] = UNSET,
    serial_nie: Union[Unset, List[str]] = UNSET,
    serial_niew: Union[Unset, List[str]] = UNSET,
    serial_nisw: Union[Unset, List[str]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
) -> Optional[PaginatedInventoryItemList]:
    """Get a list of inventory item objects.

    Args:
        asset_tag (Union[Unset, List[str]]):
        asset_tag_empty (Union[Unset, bool]):
        asset_tag_ic (Union[Unset, List[str]]):
        asset_tag_ie (Union[Unset, List[str]]):
        asset_tag_iew (Union[Unset, List[str]]):
        asset_tag_isw (Union[Unset, List[str]]):
        asset_tag_n (Union[Unset, List[str]]):
        asset_tag_nic (Union[Unset, List[str]]):
        asset_tag_nie (Union[Unset, List[str]]):
        asset_tag_niew (Union[Unset, List[str]]):
        asset_tag_nisw (Union[Unset, List[str]]):
        component_id (Union[Unset, List[int]]):
        component_id_empty (Union[Unset, List[int]]):
        component_id_gt (Union[Unset, List[int]]):
        component_id_gte (Union[Unset, List[int]]):
        component_id_lt (Union[Unset, List[int]]):
        component_id_lte (Union[Unset, List[int]]):
        component_id_n (Union[Unset, List[int]]):
        component_type (Union[Unset, str]):
        component_type_n (Union[Unset, str]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        discovered (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        manufacturer (Union[Unset, List[str]]):
        manufacturer_n (Union[Unset, List[str]]):
        manufacturer_id (Union[Unset, List[Union[None, int]]]):
        manufacturer_id_n (Union[Unset, List[Union[None, int]]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        parent_id (Union[Unset, List[Union[None, int]]]):
        parent_id_n (Union[Unset, List[Union[None, int]]]):
        part_id (Union[Unset, List[str]]):
        part_id_empty (Union[Unset, bool]):
        part_id_ic (Union[Unset, List[str]]):
        part_id_ie (Union[Unset, List[str]]):
        part_id_iew (Union[Unset, List[str]]):
        part_id_isw (Union[Unset, List[str]]):
        part_id_n (Union[Unset, List[str]]):
        part_id_nic (Union[Unset, List[str]]):
        part_id_nie (Union[Unset, List[str]]):
        part_id_niew (Union[Unset, List[str]]):
        part_id_nisw (Union[Unset, List[str]]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[Union[None, int]]]):
        role_id_n (Union[Unset, List[Union[None, int]]]):
        serial (Union[Unset, List[str]]):
        serial_empty (Union[Unset, bool]):
        serial_ic (Union[Unset, List[str]]):
        serial_ie (Union[Unset, List[str]]):
        serial_iew (Union[Unset, List[str]]):
        serial_isw (Union[Unset, List[str]]):
        serial_n (Union[Unset, List[str]]):
        serial_nic (Union[Unset, List[str]]):
        serial_nie (Union[Unset, List[str]]):
        serial_niew (Union[Unset, List[str]]):
        serial_nisw (Union[Unset, List[str]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedInventoryItemList
    """

    return sync_detailed(
        client=client,
        asset_tag=asset_tag,
        asset_tag_empty=asset_tag_empty,
        asset_tag_ic=asset_tag_ic,
        asset_tag_ie=asset_tag_ie,
        asset_tag_iew=asset_tag_iew,
        asset_tag_isw=asset_tag_isw,
        asset_tag_n=asset_tag_n,
        asset_tag_nic=asset_tag_nic,
        asset_tag_nie=asset_tag_nie,
        asset_tag_niew=asset_tag_niew,
        asset_tag_nisw=asset_tag_nisw,
        component_id=component_id,
        component_id_empty=component_id_empty,
        component_id_gt=component_id_gt,
        component_id_gte=component_id_gte,
        component_id_lt=component_id_lt,
        component_id_lte=component_id_lte,
        component_id_n=component_id_n,
        component_type=component_type,
        component_type_n=component_type_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        device=device,
        device_n=device_n,
        device_id=device_id,
        device_id_n=device_id_n,
        device_role=device_role,
        device_role_n=device_role_n,
        device_role_id=device_role_id,
        device_role_id_n=device_role_id_n,
        device_type=device_type,
        device_type_n=device_type_n,
        device_type_id=device_type_id,
        device_type_id_n=device_type_id_n,
        discovered=discovered,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        location=location,
        location_n=location_n,
        location_id=location_id,
        location_id_n=location_id_n,
        manufacturer=manufacturer,
        manufacturer_n=manufacturer_n,
        manufacturer_id=manufacturer_id,
        manufacturer_id_n=manufacturer_id_n,
        modified_by_request=modified_by_request,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        parent_id=parent_id,
        parent_id_n=parent_id_n,
        part_id=part_id,
        part_id_empty=part_id_empty,
        part_id_ic=part_id_ic,
        part_id_ie=part_id_ie,
        part_id_iew=part_id_iew,
        part_id_isw=part_id_isw,
        part_id_n=part_id_n,
        part_id_nic=part_id_nic,
        part_id_nie=part_id_nie,
        part_id_niew=part_id_niew,
        part_id_nisw=part_id_nisw,
        q=q,
        rack=rack,
        rack_n=rack_n,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        role=role,
        role_n=role_n,
        role_id=role_id,
        role_id_n=role_id_n,
        serial=serial,
        serial_empty=serial_empty,
        serial_ic=serial_ic,
        serial_ie=serial_ie,
        serial_iew=serial_iew,
        serial_isw=serial_isw,
        serial_n=serial_n,
        serial_nic=serial_nic,
        serial_nie=serial_nie,
        serial_niew=serial_niew,
        serial_nisw=serial_nisw,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        virtual_chassis=virtual_chassis,
        virtual_chassis_n=virtual_chassis_n,
        virtual_chassis_id=virtual_chassis_id,
        virtual_chassis_id_n=virtual_chassis_id_n,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    asset_tag: Union[Unset, List[str]] = UNSET,
    asset_tag_empty: Union[Unset, bool] = UNSET,
    asset_tag_ic: Union[Unset, List[str]] = UNSET,
    asset_tag_ie: Union[Unset, List[str]] = UNSET,
    asset_tag_iew: Union[Unset, List[str]] = UNSET,
    asset_tag_isw: Union[Unset, List[str]] = UNSET,
    asset_tag_n: Union[Unset, List[str]] = UNSET,
    asset_tag_nic: Union[Unset, List[str]] = UNSET,
    asset_tag_nie: Union[Unset, List[str]] = UNSET,
    asset_tag_niew: Union[Unset, List[str]] = UNSET,
    asset_tag_nisw: Union[Unset, List[str]] = UNSET,
    component_id: Union[Unset, List[int]] = UNSET,
    component_id_empty: Union[Unset, List[int]] = UNSET,
    component_id_gt: Union[Unset, List[int]] = UNSET,
    component_id_gte: Union[Unset, List[int]] = UNSET,
    component_id_lt: Union[Unset, List[int]] = UNSET,
    component_id_lte: Union[Unset, List[int]] = UNSET,
    component_id_n: Union[Unset, List[int]] = UNSET,
    component_type: Union[Unset, str] = UNSET,
    component_type_n: Union[Unset, str] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    discovered: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    manufacturer: Union[Unset, List[str]] = UNSET,
    manufacturer_n: Union[Unset, List[str]] = UNSET,
    manufacturer_id: Union[Unset, List[Union[None, int]]] = UNSET,
    manufacturer_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent_id: Union[Unset, List[Union[None, int]]] = UNSET,
    parent_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    part_id: Union[Unset, List[str]] = UNSET,
    part_id_empty: Union[Unset, bool] = UNSET,
    part_id_ic: Union[Unset, List[str]] = UNSET,
    part_id_ie: Union[Unset, List[str]] = UNSET,
    part_id_iew: Union[Unset, List[str]] = UNSET,
    part_id_isw: Union[Unset, List[str]] = UNSET,
    part_id_n: Union[Unset, List[str]] = UNSET,
    part_id_nic: Union[Unset, List[str]] = UNSET,
    part_id_nie: Union[Unset, List[str]] = UNSET,
    part_id_niew: Union[Unset, List[str]] = UNSET,
    part_id_nisw: Union[Unset, List[str]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[Union[None, int]]] = UNSET,
    role_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    serial: Union[Unset, List[str]] = UNSET,
    serial_empty: Union[Unset, bool] = UNSET,
    serial_ic: Union[Unset, List[str]] = UNSET,
    serial_ie: Union[Unset, List[str]] = UNSET,
    serial_iew: Union[Unset, List[str]] = UNSET,
    serial_isw: Union[Unset, List[str]] = UNSET,
    serial_n: Union[Unset, List[str]] = UNSET,
    serial_nic: Union[Unset, List[str]] = UNSET,
    serial_nie: Union[Unset, List[str]] = UNSET,
    serial_niew: Union[Unset, List[str]] = UNSET,
    serial_nisw: Union[Unset, List[str]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
) -> Response[PaginatedInventoryItemList]:
    """Get a list of inventory item objects.

    Args:
        asset_tag (Union[Unset, List[str]]):
        asset_tag_empty (Union[Unset, bool]):
        asset_tag_ic (Union[Unset, List[str]]):
        asset_tag_ie (Union[Unset, List[str]]):
        asset_tag_iew (Union[Unset, List[str]]):
        asset_tag_isw (Union[Unset, List[str]]):
        asset_tag_n (Union[Unset, List[str]]):
        asset_tag_nic (Union[Unset, List[str]]):
        asset_tag_nie (Union[Unset, List[str]]):
        asset_tag_niew (Union[Unset, List[str]]):
        asset_tag_nisw (Union[Unset, List[str]]):
        component_id (Union[Unset, List[int]]):
        component_id_empty (Union[Unset, List[int]]):
        component_id_gt (Union[Unset, List[int]]):
        component_id_gte (Union[Unset, List[int]]):
        component_id_lt (Union[Unset, List[int]]):
        component_id_lte (Union[Unset, List[int]]):
        component_id_n (Union[Unset, List[int]]):
        component_type (Union[Unset, str]):
        component_type_n (Union[Unset, str]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        discovered (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        manufacturer (Union[Unset, List[str]]):
        manufacturer_n (Union[Unset, List[str]]):
        manufacturer_id (Union[Unset, List[Union[None, int]]]):
        manufacturer_id_n (Union[Unset, List[Union[None, int]]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        parent_id (Union[Unset, List[Union[None, int]]]):
        parent_id_n (Union[Unset, List[Union[None, int]]]):
        part_id (Union[Unset, List[str]]):
        part_id_empty (Union[Unset, bool]):
        part_id_ic (Union[Unset, List[str]]):
        part_id_ie (Union[Unset, List[str]]):
        part_id_iew (Union[Unset, List[str]]):
        part_id_isw (Union[Unset, List[str]]):
        part_id_n (Union[Unset, List[str]]):
        part_id_nic (Union[Unset, List[str]]):
        part_id_nie (Union[Unset, List[str]]):
        part_id_niew (Union[Unset, List[str]]):
        part_id_nisw (Union[Unset, List[str]]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[Union[None, int]]]):
        role_id_n (Union[Unset, List[Union[None, int]]]):
        serial (Union[Unset, List[str]]):
        serial_empty (Union[Unset, bool]):
        serial_ic (Union[Unset, List[str]]):
        serial_ie (Union[Unset, List[str]]):
        serial_iew (Union[Unset, List[str]]):
        serial_isw (Union[Unset, List[str]]):
        serial_n (Union[Unset, List[str]]):
        serial_nic (Union[Unset, List[str]]):
        serial_nie (Union[Unset, List[str]]):
        serial_niew (Union[Unset, List[str]]):
        serial_nisw (Union[Unset, List[str]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedInventoryItemList]
    """

    kwargs = _get_kwargs(
        asset_tag=asset_tag,
        asset_tag_empty=asset_tag_empty,
        asset_tag_ic=asset_tag_ic,
        asset_tag_ie=asset_tag_ie,
        asset_tag_iew=asset_tag_iew,
        asset_tag_isw=asset_tag_isw,
        asset_tag_n=asset_tag_n,
        asset_tag_nic=asset_tag_nic,
        asset_tag_nie=asset_tag_nie,
        asset_tag_niew=asset_tag_niew,
        asset_tag_nisw=asset_tag_nisw,
        component_id=component_id,
        component_id_empty=component_id_empty,
        component_id_gt=component_id_gt,
        component_id_gte=component_id_gte,
        component_id_lt=component_id_lt,
        component_id_lte=component_id_lte,
        component_id_n=component_id_n,
        component_type=component_type,
        component_type_n=component_type_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        device=device,
        device_n=device_n,
        device_id=device_id,
        device_id_n=device_id_n,
        device_role=device_role,
        device_role_n=device_role_n,
        device_role_id=device_role_id,
        device_role_id_n=device_role_id_n,
        device_type=device_type,
        device_type_n=device_type_n,
        device_type_id=device_type_id,
        device_type_id_n=device_type_id_n,
        discovered=discovered,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        location=location,
        location_n=location_n,
        location_id=location_id,
        location_id_n=location_id_n,
        manufacturer=manufacturer,
        manufacturer_n=manufacturer_n,
        manufacturer_id=manufacturer_id,
        manufacturer_id_n=manufacturer_id_n,
        modified_by_request=modified_by_request,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        parent_id=parent_id,
        parent_id_n=parent_id_n,
        part_id=part_id,
        part_id_empty=part_id_empty,
        part_id_ic=part_id_ic,
        part_id_ie=part_id_ie,
        part_id_iew=part_id_iew,
        part_id_isw=part_id_isw,
        part_id_n=part_id_n,
        part_id_nic=part_id_nic,
        part_id_nie=part_id_nie,
        part_id_niew=part_id_niew,
        part_id_nisw=part_id_nisw,
        q=q,
        rack=rack,
        rack_n=rack_n,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        role=role,
        role_n=role_n,
        role_id=role_id,
        role_id_n=role_id_n,
        serial=serial,
        serial_empty=serial_empty,
        serial_ic=serial_ic,
        serial_ie=serial_ie,
        serial_iew=serial_iew,
        serial_isw=serial_isw,
        serial_n=serial_n,
        serial_nic=serial_nic,
        serial_nie=serial_nie,
        serial_niew=serial_niew,
        serial_nisw=serial_nisw,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        virtual_chassis=virtual_chassis,
        virtual_chassis_n=virtual_chassis_n,
        virtual_chassis_id=virtual_chassis_id,
        virtual_chassis_id_n=virtual_chassis_id_n,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    asset_tag: Union[Unset, List[str]] = UNSET,
    asset_tag_empty: Union[Unset, bool] = UNSET,
    asset_tag_ic: Union[Unset, List[str]] = UNSET,
    asset_tag_ie: Union[Unset, List[str]] = UNSET,
    asset_tag_iew: Union[Unset, List[str]] = UNSET,
    asset_tag_isw: Union[Unset, List[str]] = UNSET,
    asset_tag_n: Union[Unset, List[str]] = UNSET,
    asset_tag_nic: Union[Unset, List[str]] = UNSET,
    asset_tag_nie: Union[Unset, List[str]] = UNSET,
    asset_tag_niew: Union[Unset, List[str]] = UNSET,
    asset_tag_nisw: Union[Unset, List[str]] = UNSET,
    component_id: Union[Unset, List[int]] = UNSET,
    component_id_empty: Union[Unset, List[int]] = UNSET,
    component_id_gt: Union[Unset, List[int]] = UNSET,
    component_id_gte: Union[Unset, List[int]] = UNSET,
    component_id_lt: Union[Unset, List[int]] = UNSET,
    component_id_lte: Union[Unset, List[int]] = UNSET,
    component_id_n: Union[Unset, List[int]] = UNSET,
    component_type: Union[Unset, str] = UNSET,
    component_type_n: Union[Unset, str] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    discovered: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    manufacturer: Union[Unset, List[str]] = UNSET,
    manufacturer_n: Union[Unset, List[str]] = UNSET,
    manufacturer_id: Union[Unset, List[Union[None, int]]] = UNSET,
    manufacturer_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent_id: Union[Unset, List[Union[None, int]]] = UNSET,
    parent_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    part_id: Union[Unset, List[str]] = UNSET,
    part_id_empty: Union[Unset, bool] = UNSET,
    part_id_ic: Union[Unset, List[str]] = UNSET,
    part_id_ie: Union[Unset, List[str]] = UNSET,
    part_id_iew: Union[Unset, List[str]] = UNSET,
    part_id_isw: Union[Unset, List[str]] = UNSET,
    part_id_n: Union[Unset, List[str]] = UNSET,
    part_id_nic: Union[Unset, List[str]] = UNSET,
    part_id_nie: Union[Unset, List[str]] = UNSET,
    part_id_niew: Union[Unset, List[str]] = UNSET,
    part_id_nisw: Union[Unset, List[str]] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[Union[None, int]]] = UNSET,
    role_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    serial: Union[Unset, List[str]] = UNSET,
    serial_empty: Union[Unset, bool] = UNSET,
    serial_ic: Union[Unset, List[str]] = UNSET,
    serial_ie: Union[Unset, List[str]] = UNSET,
    serial_iew: Union[Unset, List[str]] = UNSET,
    serial_isw: Union[Unset, List[str]] = UNSET,
    serial_n: Union[Unset, List[str]] = UNSET,
    serial_nic: Union[Unset, List[str]] = UNSET,
    serial_nie: Union[Unset, List[str]] = UNSET,
    serial_niew: Union[Unset, List[str]] = UNSET,
    serial_nisw: Union[Unset, List[str]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
) -> Optional[PaginatedInventoryItemList]:
    """Get a list of inventory item objects.

    Args:
        asset_tag (Union[Unset, List[str]]):
        asset_tag_empty (Union[Unset, bool]):
        asset_tag_ic (Union[Unset, List[str]]):
        asset_tag_ie (Union[Unset, List[str]]):
        asset_tag_iew (Union[Unset, List[str]]):
        asset_tag_isw (Union[Unset, List[str]]):
        asset_tag_n (Union[Unset, List[str]]):
        asset_tag_nic (Union[Unset, List[str]]):
        asset_tag_nie (Union[Unset, List[str]]):
        asset_tag_niew (Union[Unset, List[str]]):
        asset_tag_nisw (Union[Unset, List[str]]):
        component_id (Union[Unset, List[int]]):
        component_id_empty (Union[Unset, List[int]]):
        component_id_gt (Union[Unset, List[int]]):
        component_id_gte (Union[Unset, List[int]]):
        component_id_lt (Union[Unset, List[int]]):
        component_id_lte (Union[Unset, List[int]]):
        component_id_n (Union[Unset, List[int]]):
        component_type (Union[Unset, str]):
        component_type_n (Union[Unset, str]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        discovered (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        manufacturer (Union[Unset, List[str]]):
        manufacturer_n (Union[Unset, List[str]]):
        manufacturer_id (Union[Unset, List[Union[None, int]]]):
        manufacturer_id_n (Union[Unset, List[Union[None, int]]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        parent_id (Union[Unset, List[Union[None, int]]]):
        parent_id_n (Union[Unset, List[Union[None, int]]]):
        part_id (Union[Unset, List[str]]):
        part_id_empty (Union[Unset, bool]):
        part_id_ic (Union[Unset, List[str]]):
        part_id_ie (Union[Unset, List[str]]):
        part_id_iew (Union[Unset, List[str]]):
        part_id_isw (Union[Unset, List[str]]):
        part_id_n (Union[Unset, List[str]]):
        part_id_nic (Union[Unset, List[str]]):
        part_id_nie (Union[Unset, List[str]]):
        part_id_niew (Union[Unset, List[str]]):
        part_id_nisw (Union[Unset, List[str]]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[Union[None, int]]]):
        role_id_n (Union[Unset, List[Union[None, int]]]):
        serial (Union[Unset, List[str]]):
        serial_empty (Union[Unset, bool]):
        serial_ic (Union[Unset, List[str]]):
        serial_ie (Union[Unset, List[str]]):
        serial_iew (Union[Unset, List[str]]):
        serial_isw (Union[Unset, List[str]]):
        serial_n (Union[Unset, List[str]]):
        serial_nic (Union[Unset, List[str]]):
        serial_nie (Union[Unset, List[str]]):
        serial_niew (Union[Unset, List[str]]):
        serial_nisw (Union[Unset, List[str]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedInventoryItemList
    """

    return (
        await asyncio_detailed(
            client=client,
            asset_tag=asset_tag,
            asset_tag_empty=asset_tag_empty,
            asset_tag_ic=asset_tag_ic,
            asset_tag_ie=asset_tag_ie,
            asset_tag_iew=asset_tag_iew,
            asset_tag_isw=asset_tag_isw,
            asset_tag_n=asset_tag_n,
            asset_tag_nic=asset_tag_nic,
            asset_tag_nie=asset_tag_nie,
            asset_tag_niew=asset_tag_niew,
            asset_tag_nisw=asset_tag_nisw,
            component_id=component_id,
            component_id_empty=component_id_empty,
            component_id_gt=component_id_gt,
            component_id_gte=component_id_gte,
            component_id_lt=component_id_lt,
            component_id_lte=component_id_lte,
            component_id_n=component_id_n,
            component_type=component_type,
            component_type_n=component_type_n,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            device=device,
            device_n=device_n,
            device_id=device_id,
            device_id_n=device_id_n,
            device_role=device_role,
            device_role_n=device_role_n,
            device_role_id=device_role_id,
            device_role_id_n=device_role_id_n,
            device_type=device_type,
            device_type_n=device_type_n,
            device_type_id=device_type_id,
            device_type_id_n=device_type_id_n,
            discovered=discovered,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            label=label,
            label_empty=label_empty,
            label_ic=label_ic,
            label_ie=label_ie,
            label_iew=label_iew,
            label_isw=label_isw,
            label_n=label_n,
            label_nic=label_nic,
            label_nie=label_nie,
            label_niew=label_niew,
            label_nisw=label_nisw,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            limit=limit,
            location=location,
            location_n=location_n,
            location_id=location_id,
            location_id_n=location_id_n,
            manufacturer=manufacturer,
            manufacturer_n=manufacturer_n,
            manufacturer_id=manufacturer_id,
            manufacturer_id_n=manufacturer_id_n,
            modified_by_request=modified_by_request,
            name=name,
            name_empty=name_empty,
            name_ic=name_ic,
            name_ie=name_ie,
            name_iew=name_iew,
            name_isw=name_isw,
            name_n=name_n,
            name_nic=name_nic,
            name_nie=name_nie,
            name_niew=name_niew,
            name_nisw=name_nisw,
            offset=offset,
            ordering=ordering,
            parent_id=parent_id,
            parent_id_n=parent_id_n,
            part_id=part_id,
            part_id_empty=part_id_empty,
            part_id_ic=part_id_ic,
            part_id_ie=part_id_ie,
            part_id_iew=part_id_iew,
            part_id_isw=part_id_isw,
            part_id_n=part_id_n,
            part_id_nic=part_id_nic,
            part_id_nie=part_id_nie,
            part_id_niew=part_id_niew,
            part_id_nisw=part_id_nisw,
            q=q,
            rack=rack,
            rack_n=rack_n,
            rack_id=rack_id,
            rack_id_n=rack_id_n,
            region=region,
            region_n=region_n,
            region_id=region_id,
            region_id_n=region_id_n,
            role=role,
            role_n=role_n,
            role_id=role_id,
            role_id_n=role_id_n,
            serial=serial,
            serial_empty=serial_empty,
            serial_ic=serial_ic,
            serial_ie=serial_ie,
            serial_iew=serial_iew,
            serial_isw=serial_isw,
            serial_n=serial_n,
            serial_nic=serial_nic,
            serial_nie=serial_nie,
            serial_niew=serial_niew,
            serial_nisw=serial_nisw,
            site=site,
            site_n=site_n,
            site_group=site_group,
            site_group_n=site_group_n,
            site_group_id=site_group_id,
            site_group_id_n=site_group_id_n,
            site_id=site_id,
            site_id_n=site_id_n,
            tag=tag,
            tag_n=tag_n,
            updated_by_request=updated_by_request,
            virtual_chassis=virtual_chassis,
            virtual_chassis_n=virtual_chassis_n,
            virtual_chassis_id=virtual_chassis_id,
            virtual_chassis_id_n=virtual_chassis_id_n,
        )
    ).parsed
