import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_cable_list import PaginatedCableList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    color: Union[Unset, List[str]] = UNSET,
    color_n: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[str]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    length: Union[Unset, List[float]] = UNSET,
    length_empty: Union[Unset, bool] = UNSET,
    length_gt: Union[Unset, List[float]] = UNSET,
    length_gte: Union[Unset, List[float]] = UNSET,
    length_lt: Union[Unset, List[float]] = UNSET,
    length_lte: Union[Unset, List[float]] = UNSET,
    length_n: Union[Unset, List[float]] = UNSET,
    length_unit: Union[Unset, str] = UNSET,
    length_unit_n: Union[Unset, str] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    termination_a_id: Union[Unset, List[int]] = UNSET,
    termination_a_type: Union[Unset, str] = UNSET,
    termination_a_type_n: Union[Unset, str] = UNSET,
    termination_b_id: Union[Unset, List[int]] = UNSET,
    termination_b_type: Union[Unset, str] = UNSET,
    termination_b_type_n: Union[Unset, str] = UNSET,
    type: Union[Unset, List[str]] = UNSET,
    type_n: Union[Unset, List[str]] = UNSET,
    unterminated: Union[Unset, bool] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_color: Union[Unset, List[str]] = UNSET
    if not isinstance(color, Unset):
        json_color = color

    params["color"] = json_color

    json_color_n: Union[Unset, List[str]] = UNSET
    if not isinstance(color_n, Unset):
        json_color_n = color_n

    params["color__n"] = json_color_n

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    json_description: Union[Unset, List[str]] = UNSET
    if not isinstance(description, Unset):
        json_description = description

    params["description"] = json_description

    params["description__empty"] = description_empty

    json_description_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ic, Unset):
        json_description_ic = description_ic

    params["description__ic"] = json_description_ic

    json_description_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ie, Unset):
        json_description_ie = description_ie

    params["description__ie"] = json_description_ie

    json_description_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_iew, Unset):
        json_description_iew = description_iew

    params["description__iew"] = json_description_iew

    json_description_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_isw, Unset):
        json_description_isw = description_isw

    params["description__isw"] = json_description_isw

    json_description_n: Union[Unset, List[str]] = UNSET
    if not isinstance(description_n, Unset):
        json_description_n = description_n

    params["description__n"] = json_description_n

    json_description_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nic, Unset):
        json_description_nic = description_nic

    params["description__nic"] = json_description_nic

    json_description_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nie, Unset):
        json_description_nie = description_nie

    params["description__nie"] = json_description_nie

    json_description_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_niew, Unset):
        json_description_niew = description_niew

    params["description__niew"] = json_description_niew

    json_description_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nisw, Unset):
        json_description_nisw = description_nisw

    params["description__nisw"] = json_description_nisw

    json_device: Union[Unset, List[str]] = UNSET
    if not isinstance(device, Unset):
        json_device = device

    params["device"] = json_device

    json_device_id: Union[Unset, List[int]] = UNSET
    if not isinstance(device_id, Unset):
        json_device_id = device_id

    params["device_id"] = json_device_id

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    json_label: Union[Unset, List[str]] = UNSET
    if not isinstance(label, Unset):
        json_label = label

    params["label"] = json_label

    params["label__empty"] = label_empty

    json_label_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(label_ic, Unset):
        json_label_ic = label_ic

    params["label__ic"] = json_label_ic

    json_label_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(label_ie, Unset):
        json_label_ie = label_ie

    params["label__ie"] = json_label_ie

    json_label_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(label_iew, Unset):
        json_label_iew = label_iew

    params["label__iew"] = json_label_iew

    json_label_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(label_isw, Unset):
        json_label_isw = label_isw

    params["label__isw"] = json_label_isw

    json_label_n: Union[Unset, List[str]] = UNSET
    if not isinstance(label_n, Unset):
        json_label_n = label_n

    params["label__n"] = json_label_n

    json_label_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nic, Unset):
        json_label_nic = label_nic

    params["label__nic"] = json_label_nic

    json_label_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nie, Unset):
        json_label_nie = label_nie

    params["label__nie"] = json_label_nie

    json_label_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(label_niew, Unset):
        json_label_niew = label_niew

    params["label__niew"] = json_label_niew

    json_label_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nisw, Unset):
        json_label_nisw = label_nisw

    params["label__nisw"] = json_label_nisw

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    json_length: Union[Unset, List[float]] = UNSET
    if not isinstance(length, Unset):
        json_length = length

    params["length"] = json_length

    params["length__empty"] = length_empty

    json_length_gt: Union[Unset, List[float]] = UNSET
    if not isinstance(length_gt, Unset):
        json_length_gt = length_gt

    params["length__gt"] = json_length_gt

    json_length_gte: Union[Unset, List[float]] = UNSET
    if not isinstance(length_gte, Unset):
        json_length_gte = length_gte

    params["length__gte"] = json_length_gte

    json_length_lt: Union[Unset, List[float]] = UNSET
    if not isinstance(length_lt, Unset):
        json_length_lt = length_lt

    params["length__lt"] = json_length_lt

    json_length_lte: Union[Unset, List[float]] = UNSET
    if not isinstance(length_lte, Unset):
        json_length_lte = length_lte

    params["length__lte"] = json_length_lte

    json_length_n: Union[Unset, List[float]] = UNSET
    if not isinstance(length_n, Unset):
        json_length_n = length_n

    params["length__n"] = json_length_n

    params["length_unit"] = length_unit

    params["length_unit__n"] = length_unit_n

    params["limit"] = limit

    json_location: Union[Unset, List[str]] = UNSET
    if not isinstance(location, Unset):
        json_location = location

    params["location"] = json_location

    json_location_id: Union[Unset, List[int]] = UNSET
    if not isinstance(location_id, Unset):
        json_location_id = location_id

    params["location_id"] = json_location_id

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    params["offset"] = offset

    params["ordering"] = ordering

    params["q"] = q

    json_rack: Union[Unset, List[str]] = UNSET
    if not isinstance(rack, Unset):
        json_rack = rack

    params["rack"] = json_rack

    json_rack_id: Union[Unset, List[int]] = UNSET
    if not isinstance(rack_id, Unset):
        json_rack_id = rack_id

    params["rack_id"] = json_rack_id

    json_site: Union[Unset, List[str]] = UNSET
    if not isinstance(site, Unset):
        json_site = site

    params["site"] = json_site

    json_site_id: Union[Unset, List[int]] = UNSET
    if not isinstance(site_id, Unset):
        json_site_id = site_id

    params["site_id"] = json_site_id

    json_status: Union[Unset, List[str]] = UNSET
    if not isinstance(status, Unset):
        json_status = status

    params["status"] = json_status

    json_status_n: Union[Unset, List[str]] = UNSET
    if not isinstance(status_n, Unset):
        json_status_n = status_n

    params["status__n"] = json_status_n

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_tenant: Union[Unset, List[str]] = UNSET
    if not isinstance(tenant, Unset):
        json_tenant = tenant

    params["tenant"] = json_tenant

    json_tenant_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tenant_n, Unset):
        json_tenant_n = tenant_n

    params["tenant__n"] = json_tenant_n

    json_tenant_group: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group, Unset):
        json_tenant_group = tenant_group

    params["tenant_group"] = json_tenant_group

    json_tenant_group_n: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group_n, Unset):
        json_tenant_group_n = tenant_group_n

    params["tenant_group__n"] = json_tenant_group_n

    json_tenant_group_id: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group_id, Unset):
        json_tenant_group_id = tenant_group_id

    params["tenant_group_id"] = json_tenant_group_id

    json_tenant_group_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group_id_n, Unset):
        json_tenant_group_id_n = tenant_group_id_n

    params["tenant_group_id__n"] = json_tenant_group_id_n

    json_tenant_id: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(tenant_id, Unset):
        json_tenant_id = []
        for tenant_id_item_data in tenant_id:
            tenant_id_item: Union[None, int]
            tenant_id_item = tenant_id_item_data
            json_tenant_id.append(tenant_id_item)

    params["tenant_id"] = json_tenant_id

    json_tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(tenant_id_n, Unset):
        json_tenant_id_n = []
        for tenant_id_n_item_data in tenant_id_n:
            tenant_id_n_item: Union[None, int]
            tenant_id_n_item = tenant_id_n_item_data
            json_tenant_id_n.append(tenant_id_n_item)

    params["tenant_id__n"] = json_tenant_id_n

    json_termination_a_id: Union[Unset, List[int]] = UNSET
    if not isinstance(termination_a_id, Unset):
        json_termination_a_id = termination_a_id

    params["termination_a_id"] = json_termination_a_id

    params["termination_a_type"] = termination_a_type

    params["termination_a_type__n"] = termination_a_type_n

    json_termination_b_id: Union[Unset, List[int]] = UNSET
    if not isinstance(termination_b_id, Unset):
        json_termination_b_id = termination_b_id

    params["termination_b_id"] = json_termination_b_id

    params["termination_b_type"] = termination_b_type

    params["termination_b_type__n"] = termination_b_type_n

    json_type: Union[Unset, List[str]] = UNSET
    if not isinstance(type, Unset):
        json_type = type

    params["type"] = json_type

    json_type_n: Union[Unset, List[str]] = UNSET
    if not isinstance(type_n, Unset):
        json_type_n = type_n

    params["type__n"] = json_type_n

    params["unterminated"] = unterminated

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/dcim/cables/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedCableList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedCableList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedCableList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    color: Union[Unset, List[str]] = UNSET,
    color_n: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[str]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    length: Union[Unset, List[float]] = UNSET,
    length_empty: Union[Unset, bool] = UNSET,
    length_gt: Union[Unset, List[float]] = UNSET,
    length_gte: Union[Unset, List[float]] = UNSET,
    length_lt: Union[Unset, List[float]] = UNSET,
    length_lte: Union[Unset, List[float]] = UNSET,
    length_n: Union[Unset, List[float]] = UNSET,
    length_unit: Union[Unset, str] = UNSET,
    length_unit_n: Union[Unset, str] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    termination_a_id: Union[Unset, List[int]] = UNSET,
    termination_a_type: Union[Unset, str] = UNSET,
    termination_a_type_n: Union[Unset, str] = UNSET,
    termination_b_id: Union[Unset, List[int]] = UNSET,
    termination_b_type: Union[Unset, str] = UNSET,
    termination_b_type_n: Union[Unset, str] = UNSET,
    type: Union[Unset, List[str]] = UNSET,
    type_n: Union[Unset, List[str]] = UNSET,
    unterminated: Union[Unset, bool] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Response[PaginatedCableList]:
    """Get a list of cable objects.

    Args:
        color (Union[Unset, List[str]]):
        color_n (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[str]]):
        device_id (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        length (Union[Unset, List[float]]):
        length_empty (Union[Unset, bool]):
        length_gt (Union[Unset, List[float]]):
        length_gte (Union[Unset, List[float]]):
        length_lt (Union[Unset, List[float]]):
        length_lte (Union[Unset, List[float]]):
        length_n (Union[Unset, List[float]]):
        length_unit (Union[Unset, str]):
        length_unit_n (Union[Unset, str]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_id (Union[Unset, List[int]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        termination_a_id (Union[Unset, List[int]]):
        termination_a_type (Union[Unset, str]):
        termination_a_type_n (Union[Unset, str]):
        termination_b_id (Union[Unset, List[int]]):
        termination_b_type (Union[Unset, str]):
        termination_b_type_n (Union[Unset, str]):
        type (Union[Unset, List[str]]):
        type_n (Union[Unset, List[str]]):
        unterminated (Union[Unset, bool]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCableList]
    """

    kwargs = _get_kwargs(
        color=color,
        color_n=color_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_id=device_id,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        length=length,
        length_empty=length_empty,
        length_gt=length_gt,
        length_gte=length_gte,
        length_lt=length_lt,
        length_lte=length_lte,
        length_n=length_n,
        length_unit=length_unit,
        length_unit_n=length_unit_n,
        limit=limit,
        location=location,
        location_id=location_id,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        rack=rack,
        rack_id=rack_id,
        site=site,
        site_id=site_id,
        status=status,
        status_n=status_n,
        tag=tag,
        tag_n=tag_n,
        tenant=tenant,
        tenant_n=tenant_n,
        tenant_group=tenant_group,
        tenant_group_n=tenant_group_n,
        tenant_group_id=tenant_group_id,
        tenant_group_id_n=tenant_group_id_n,
        tenant_id=tenant_id,
        tenant_id_n=tenant_id_n,
        termination_a_id=termination_a_id,
        termination_a_type=termination_a_type,
        termination_a_type_n=termination_a_type_n,
        termination_b_id=termination_b_id,
        termination_b_type=termination_b_type,
        termination_b_type_n=termination_b_type_n,
        type=type,
        type_n=type_n,
        unterminated=unterminated,
        updated_by_request=updated_by_request,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    color: Union[Unset, List[str]] = UNSET,
    color_n: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[str]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    length: Union[Unset, List[float]] = UNSET,
    length_empty: Union[Unset, bool] = UNSET,
    length_gt: Union[Unset, List[float]] = UNSET,
    length_gte: Union[Unset, List[float]] = UNSET,
    length_lt: Union[Unset, List[float]] = UNSET,
    length_lte: Union[Unset, List[float]] = UNSET,
    length_n: Union[Unset, List[float]] = UNSET,
    length_unit: Union[Unset, str] = UNSET,
    length_unit_n: Union[Unset, str] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    termination_a_id: Union[Unset, List[int]] = UNSET,
    termination_a_type: Union[Unset, str] = UNSET,
    termination_a_type_n: Union[Unset, str] = UNSET,
    termination_b_id: Union[Unset, List[int]] = UNSET,
    termination_b_type: Union[Unset, str] = UNSET,
    termination_b_type_n: Union[Unset, str] = UNSET,
    type: Union[Unset, List[str]] = UNSET,
    type_n: Union[Unset, List[str]] = UNSET,
    unterminated: Union[Unset, bool] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Optional[PaginatedCableList]:
    """Get a list of cable objects.

    Args:
        color (Union[Unset, List[str]]):
        color_n (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[str]]):
        device_id (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        length (Union[Unset, List[float]]):
        length_empty (Union[Unset, bool]):
        length_gt (Union[Unset, List[float]]):
        length_gte (Union[Unset, List[float]]):
        length_lt (Union[Unset, List[float]]):
        length_lte (Union[Unset, List[float]]):
        length_n (Union[Unset, List[float]]):
        length_unit (Union[Unset, str]):
        length_unit_n (Union[Unset, str]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_id (Union[Unset, List[int]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        termination_a_id (Union[Unset, List[int]]):
        termination_a_type (Union[Unset, str]):
        termination_a_type_n (Union[Unset, str]):
        termination_b_id (Union[Unset, List[int]]):
        termination_b_type (Union[Unset, str]):
        termination_b_type_n (Union[Unset, str]):
        type (Union[Unset, List[str]]):
        type_n (Union[Unset, List[str]]):
        unterminated (Union[Unset, bool]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCableList
    """

    return sync_detailed(
        client=client,
        color=color,
        color_n=color_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_id=device_id,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        length=length,
        length_empty=length_empty,
        length_gt=length_gt,
        length_gte=length_gte,
        length_lt=length_lt,
        length_lte=length_lte,
        length_n=length_n,
        length_unit=length_unit,
        length_unit_n=length_unit_n,
        limit=limit,
        location=location,
        location_id=location_id,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        rack=rack,
        rack_id=rack_id,
        site=site,
        site_id=site_id,
        status=status,
        status_n=status_n,
        tag=tag,
        tag_n=tag_n,
        tenant=tenant,
        tenant_n=tenant_n,
        tenant_group=tenant_group,
        tenant_group_n=tenant_group_n,
        tenant_group_id=tenant_group_id,
        tenant_group_id_n=tenant_group_id_n,
        tenant_id=tenant_id,
        tenant_id_n=tenant_id_n,
        termination_a_id=termination_a_id,
        termination_a_type=termination_a_type,
        termination_a_type_n=termination_a_type_n,
        termination_b_id=termination_b_id,
        termination_b_type=termination_b_type,
        termination_b_type_n=termination_b_type_n,
        type=type,
        type_n=type_n,
        unterminated=unterminated,
        updated_by_request=updated_by_request,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    color: Union[Unset, List[str]] = UNSET,
    color_n: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[str]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    length: Union[Unset, List[float]] = UNSET,
    length_empty: Union[Unset, bool] = UNSET,
    length_gt: Union[Unset, List[float]] = UNSET,
    length_gte: Union[Unset, List[float]] = UNSET,
    length_lt: Union[Unset, List[float]] = UNSET,
    length_lte: Union[Unset, List[float]] = UNSET,
    length_n: Union[Unset, List[float]] = UNSET,
    length_unit: Union[Unset, str] = UNSET,
    length_unit_n: Union[Unset, str] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    termination_a_id: Union[Unset, List[int]] = UNSET,
    termination_a_type: Union[Unset, str] = UNSET,
    termination_a_type_n: Union[Unset, str] = UNSET,
    termination_b_id: Union[Unset, List[int]] = UNSET,
    termination_b_type: Union[Unset, str] = UNSET,
    termination_b_type_n: Union[Unset, str] = UNSET,
    type: Union[Unset, List[str]] = UNSET,
    type_n: Union[Unset, List[str]] = UNSET,
    unterminated: Union[Unset, bool] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Response[PaginatedCableList]:
    """Get a list of cable objects.

    Args:
        color (Union[Unset, List[str]]):
        color_n (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[str]]):
        device_id (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        length (Union[Unset, List[float]]):
        length_empty (Union[Unset, bool]):
        length_gt (Union[Unset, List[float]]):
        length_gte (Union[Unset, List[float]]):
        length_lt (Union[Unset, List[float]]):
        length_lte (Union[Unset, List[float]]):
        length_n (Union[Unset, List[float]]):
        length_unit (Union[Unset, str]):
        length_unit_n (Union[Unset, str]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_id (Union[Unset, List[int]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        termination_a_id (Union[Unset, List[int]]):
        termination_a_type (Union[Unset, str]):
        termination_a_type_n (Union[Unset, str]):
        termination_b_id (Union[Unset, List[int]]):
        termination_b_type (Union[Unset, str]):
        termination_b_type_n (Union[Unset, str]):
        type (Union[Unset, List[str]]):
        type_n (Union[Unset, List[str]]):
        unterminated (Union[Unset, bool]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCableList]
    """

    kwargs = _get_kwargs(
        color=color,
        color_n=color_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_id=device_id,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        length=length,
        length_empty=length_empty,
        length_gt=length_gt,
        length_gte=length_gte,
        length_lt=length_lt,
        length_lte=length_lte,
        length_n=length_n,
        length_unit=length_unit,
        length_unit_n=length_unit_n,
        limit=limit,
        location=location,
        location_id=location_id,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        rack=rack,
        rack_id=rack_id,
        site=site,
        site_id=site_id,
        status=status,
        status_n=status_n,
        tag=tag,
        tag_n=tag_n,
        tenant=tenant,
        tenant_n=tenant_n,
        tenant_group=tenant_group,
        tenant_group_n=tenant_group_n,
        tenant_group_id=tenant_group_id,
        tenant_group_id_n=tenant_group_id_n,
        tenant_id=tenant_id,
        tenant_id_n=tenant_id_n,
        termination_a_id=termination_a_id,
        termination_a_type=termination_a_type,
        termination_a_type_n=termination_a_type_n,
        termination_b_id=termination_b_id,
        termination_b_type=termination_b_type,
        termination_b_type_n=termination_b_type_n,
        type=type,
        type_n=type_n,
        unterminated=unterminated,
        updated_by_request=updated_by_request,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    color: Union[Unset, List[str]] = UNSET,
    color_n: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[str]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    length: Union[Unset, List[float]] = UNSET,
    length_empty: Union[Unset, bool] = UNSET,
    length_gt: Union[Unset, List[float]] = UNSET,
    length_gte: Union[Unset, List[float]] = UNSET,
    length_lt: Union[Unset, List[float]] = UNSET,
    length_lte: Union[Unset, List[float]] = UNSET,
    length_n: Union[Unset, List[float]] = UNSET,
    length_unit: Union[Unset, str] = UNSET,
    length_unit_n: Union[Unset, str] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    termination_a_id: Union[Unset, List[int]] = UNSET,
    termination_a_type: Union[Unset, str] = UNSET,
    termination_a_type_n: Union[Unset, str] = UNSET,
    termination_b_id: Union[Unset, List[int]] = UNSET,
    termination_b_type: Union[Unset, str] = UNSET,
    termination_b_type_n: Union[Unset, str] = UNSET,
    type: Union[Unset, List[str]] = UNSET,
    type_n: Union[Unset, List[str]] = UNSET,
    unterminated: Union[Unset, bool] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Optional[PaginatedCableList]:
    """Get a list of cable objects.

    Args:
        color (Union[Unset, List[str]]):
        color_n (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[str]]):
        device_id (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        length (Union[Unset, List[float]]):
        length_empty (Union[Unset, bool]):
        length_gt (Union[Unset, List[float]]):
        length_gte (Union[Unset, List[float]]):
        length_lt (Union[Unset, List[float]]):
        length_lte (Union[Unset, List[float]]):
        length_n (Union[Unset, List[float]]):
        length_unit (Union[Unset, str]):
        length_unit_n (Union[Unset, str]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_id (Union[Unset, List[int]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        termination_a_id (Union[Unset, List[int]]):
        termination_a_type (Union[Unset, str]):
        termination_a_type_n (Union[Unset, str]):
        termination_b_id (Union[Unset, List[int]]):
        termination_b_type (Union[Unset, str]):
        termination_b_type_n (Union[Unset, str]):
        type (Union[Unset, List[str]]):
        type_n (Union[Unset, List[str]]):
        unterminated (Union[Unset, bool]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCableList
    """

    return (
        await asyncio_detailed(
            client=client,
            color=color,
            color_n=color_n,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            description=description,
            description_empty=description_empty,
            description_ic=description_ic,
            description_ie=description_ie,
            description_iew=description_iew,
            description_isw=description_isw,
            description_n=description_n,
            description_nic=description_nic,
            description_nie=description_nie,
            description_niew=description_niew,
            description_nisw=description_nisw,
            device=device,
            device_id=device_id,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            label=label,
            label_empty=label_empty,
            label_ic=label_ic,
            label_ie=label_ie,
            label_iew=label_iew,
            label_isw=label_isw,
            label_n=label_n,
            label_nic=label_nic,
            label_nie=label_nie,
            label_niew=label_niew,
            label_nisw=label_nisw,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            length=length,
            length_empty=length_empty,
            length_gt=length_gt,
            length_gte=length_gte,
            length_lt=length_lt,
            length_lte=length_lte,
            length_n=length_n,
            length_unit=length_unit,
            length_unit_n=length_unit_n,
            limit=limit,
            location=location,
            location_id=location_id,
            modified_by_request=modified_by_request,
            offset=offset,
            ordering=ordering,
            q=q,
            rack=rack,
            rack_id=rack_id,
            site=site,
            site_id=site_id,
            status=status,
            status_n=status_n,
            tag=tag,
            tag_n=tag_n,
            tenant=tenant,
            tenant_n=tenant_n,
            tenant_group=tenant_group,
            tenant_group_n=tenant_group_n,
            tenant_group_id=tenant_group_id,
            tenant_group_id_n=tenant_group_id_n,
            tenant_id=tenant_id,
            tenant_id_n=tenant_id_n,
            termination_a_id=termination_a_id,
            termination_a_type=termination_a_type,
            termination_a_type_n=termination_a_type_n,
            termination_b_id=termination_b_id,
            termination_b_type=termination_b_type,
            termination_b_type_n=termination_b_type_n,
            type=type,
            type_n=type_n,
            unterminated=unterminated,
            updated_by_request=updated_by_request,
        )
    ).parsed
