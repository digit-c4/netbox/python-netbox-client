import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_device_bay_list import PaginatedDeviceBayList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[int]] = UNSET,
    role_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    json_description: Union[Unset, List[str]] = UNSET
    if not isinstance(description, Unset):
        json_description = description

    params["description"] = json_description

    params["description__empty"] = description_empty

    json_description_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ic, Unset):
        json_description_ic = description_ic

    params["description__ic"] = json_description_ic

    json_description_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ie, Unset):
        json_description_ie = description_ie

    params["description__ie"] = json_description_ie

    json_description_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_iew, Unset):
        json_description_iew = description_iew

    params["description__iew"] = json_description_iew

    json_description_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_isw, Unset):
        json_description_isw = description_isw

    params["description__isw"] = json_description_isw

    json_description_n: Union[Unset, List[str]] = UNSET
    if not isinstance(description_n, Unset):
        json_description_n = description_n

    params["description__n"] = json_description_n

    json_description_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nic, Unset):
        json_description_nic = description_nic

    params["description__nic"] = json_description_nic

    json_description_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nie, Unset):
        json_description_nie = description_nie

    params["description__nie"] = json_description_nie

    json_description_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_niew, Unset):
        json_description_niew = description_niew

    params["description__niew"] = json_description_niew

    json_description_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nisw, Unset):
        json_description_nisw = description_nisw

    params["description__nisw"] = json_description_nisw

    json_device: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(device, Unset):
        json_device = []
        for device_item_data in device:
            device_item: Union[None, str]
            device_item = device_item_data
            json_device.append(device_item)

    params["device"] = json_device

    json_device_n: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(device_n, Unset):
        json_device_n = []
        for device_n_item_data in device_n:
            device_n_item: Union[None, str]
            device_n_item = device_n_item_data
            json_device_n.append(device_n_item)

    params["device__n"] = json_device_n

    json_device_id: Union[Unset, List[int]] = UNSET
    if not isinstance(device_id, Unset):
        json_device_id = device_id

    params["device_id"] = json_device_id

    json_device_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(device_id_n, Unset):
        json_device_id_n = device_id_n

    params["device_id__n"] = json_device_id_n

    json_device_role: Union[Unset, List[str]] = UNSET
    if not isinstance(device_role, Unset):
        json_device_role = device_role

    params["device_role"] = json_device_role

    json_device_role_n: Union[Unset, List[str]] = UNSET
    if not isinstance(device_role_n, Unset):
        json_device_role_n = device_role_n

    params["device_role__n"] = json_device_role_n

    json_device_role_id: Union[Unset, List[int]] = UNSET
    if not isinstance(device_role_id, Unset):
        json_device_role_id = device_role_id

    params["device_role_id"] = json_device_role_id

    json_device_role_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(device_role_id_n, Unset):
        json_device_role_id_n = device_role_id_n

    params["device_role_id__n"] = json_device_role_id_n

    json_device_type: Union[Unset, List[str]] = UNSET
    if not isinstance(device_type, Unset):
        json_device_type = device_type

    params["device_type"] = json_device_type

    json_device_type_n: Union[Unset, List[str]] = UNSET
    if not isinstance(device_type_n, Unset):
        json_device_type_n = device_type_n

    params["device_type__n"] = json_device_type_n

    json_device_type_id: Union[Unset, List[int]] = UNSET
    if not isinstance(device_type_id, Unset):
        json_device_type_id = device_type_id

    params["device_type_id"] = json_device_type_id

    json_device_type_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(device_type_id_n, Unset):
        json_device_type_id_n = device_type_id_n

    params["device_type_id__n"] = json_device_type_id_n

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    json_label: Union[Unset, List[str]] = UNSET
    if not isinstance(label, Unset):
        json_label = label

    params["label"] = json_label

    params["label__empty"] = label_empty

    json_label_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(label_ic, Unset):
        json_label_ic = label_ic

    params["label__ic"] = json_label_ic

    json_label_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(label_ie, Unset):
        json_label_ie = label_ie

    params["label__ie"] = json_label_ie

    json_label_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(label_iew, Unset):
        json_label_iew = label_iew

    params["label__iew"] = json_label_iew

    json_label_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(label_isw, Unset):
        json_label_isw = label_isw

    params["label__isw"] = json_label_isw

    json_label_n: Union[Unset, List[str]] = UNSET
    if not isinstance(label_n, Unset):
        json_label_n = label_n

    params["label__n"] = json_label_n

    json_label_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nic, Unset):
        json_label_nic = label_nic

    params["label__nic"] = json_label_nic

    json_label_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nie, Unset):
        json_label_nie = label_nie

    params["label__nie"] = json_label_nie

    json_label_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(label_niew, Unset):
        json_label_niew = label_niew

    params["label__niew"] = json_label_niew

    json_label_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(label_nisw, Unset):
        json_label_nisw = label_nisw

    params["label__nisw"] = json_label_nisw

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    params["limit"] = limit

    json_location: Union[Unset, List[str]] = UNSET
    if not isinstance(location, Unset):
        json_location = location

    params["location"] = json_location

    json_location_n: Union[Unset, List[str]] = UNSET
    if not isinstance(location_n, Unset):
        json_location_n = location_n

    params["location__n"] = json_location_n

    json_location_id: Union[Unset, List[int]] = UNSET
    if not isinstance(location_id, Unset):
        json_location_id = location_id

    params["location_id"] = json_location_id

    json_location_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(location_id_n, Unset):
        json_location_id_n = location_id_n

    params["location_id__n"] = json_location_id_n

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    json_name: Union[Unset, List[str]] = UNSET
    if not isinstance(name, Unset):
        json_name = name

    params["name"] = json_name

    params["name__empty"] = name_empty

    json_name_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ic, Unset):
        json_name_ic = name_ic

    params["name__ic"] = json_name_ic

    json_name_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ie, Unset):
        json_name_ie = name_ie

    params["name__ie"] = json_name_ie

    json_name_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_iew, Unset):
        json_name_iew = name_iew

    params["name__iew"] = json_name_iew

    json_name_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_isw, Unset):
        json_name_isw = name_isw

    params["name__isw"] = json_name_isw

    json_name_n: Union[Unset, List[str]] = UNSET
    if not isinstance(name_n, Unset):
        json_name_n = name_n

    params["name__n"] = json_name_n

    json_name_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nic, Unset):
        json_name_nic = name_nic

    params["name__nic"] = json_name_nic

    json_name_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nie, Unset):
        json_name_nie = name_nie

    params["name__nie"] = json_name_nie

    json_name_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_niew, Unset):
        json_name_niew = name_niew

    params["name__niew"] = json_name_niew

    json_name_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nisw, Unset):
        json_name_nisw = name_nisw

    params["name__nisw"] = json_name_nisw

    params["offset"] = offset

    params["ordering"] = ordering

    params["q"] = q

    json_rack: Union[Unset, List[str]] = UNSET
    if not isinstance(rack, Unset):
        json_rack = rack

    params["rack"] = json_rack

    json_rack_n: Union[Unset, List[str]] = UNSET
    if not isinstance(rack_n, Unset):
        json_rack_n = rack_n

    params["rack__n"] = json_rack_n

    json_rack_id: Union[Unset, List[int]] = UNSET
    if not isinstance(rack_id, Unset):
        json_rack_id = rack_id

    params["rack_id"] = json_rack_id

    json_rack_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(rack_id_n, Unset):
        json_rack_id_n = rack_id_n

    params["rack_id__n"] = json_rack_id_n

    json_region: Union[Unset, List[int]] = UNSET
    if not isinstance(region, Unset):
        json_region = region

    params["region"] = json_region

    json_region_n: Union[Unset, List[int]] = UNSET
    if not isinstance(region_n, Unset):
        json_region_n = region_n

    params["region__n"] = json_region_n

    json_region_id: Union[Unset, List[int]] = UNSET
    if not isinstance(region_id, Unset):
        json_region_id = region_id

    params["region_id"] = json_region_id

    json_region_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(region_id_n, Unset):
        json_region_id_n = region_id_n

    params["region_id__n"] = json_region_id_n

    json_role: Union[Unset, List[str]] = UNSET
    if not isinstance(role, Unset):
        json_role = role

    params["role"] = json_role

    json_role_n: Union[Unset, List[str]] = UNSET
    if not isinstance(role_n, Unset):
        json_role_n = role_n

    params["role__n"] = json_role_n

    json_role_id: Union[Unset, List[int]] = UNSET
    if not isinstance(role_id, Unset):
        json_role_id = role_id

    params["role_id"] = json_role_id

    json_role_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(role_id_n, Unset):
        json_role_id_n = role_id_n

    params["role_id__n"] = json_role_id_n

    json_site: Union[Unset, List[str]] = UNSET
    if not isinstance(site, Unset):
        json_site = site

    params["site"] = json_site

    json_site_n: Union[Unset, List[str]] = UNSET
    if not isinstance(site_n, Unset):
        json_site_n = site_n

    params["site__n"] = json_site_n

    json_site_group: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group, Unset):
        json_site_group = site_group

    params["site_group"] = json_site_group

    json_site_group_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_n, Unset):
        json_site_group_n = site_group_n

    params["site_group__n"] = json_site_group_n

    json_site_group_id: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_id, Unset):
        json_site_group_id = site_group_id

    params["site_group_id"] = json_site_group_id

    json_site_group_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_group_id_n, Unset):
        json_site_group_id_n = site_group_id_n

    params["site_group_id__n"] = json_site_group_id_n

    json_site_id: Union[Unset, List[int]] = UNSET
    if not isinstance(site_id, Unset):
        json_site_id = site_id

    params["site_id"] = json_site_id

    json_site_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(site_id_n, Unset):
        json_site_id_n = site_id_n

    params["site_id__n"] = json_site_id_n

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    json_virtual_chassis: Union[Unset, List[str]] = UNSET
    if not isinstance(virtual_chassis, Unset):
        json_virtual_chassis = virtual_chassis

    params["virtual_chassis"] = json_virtual_chassis

    json_virtual_chassis_n: Union[Unset, List[str]] = UNSET
    if not isinstance(virtual_chassis_n, Unset):
        json_virtual_chassis_n = virtual_chassis_n

    params["virtual_chassis__n"] = json_virtual_chassis_n

    json_virtual_chassis_id: Union[Unset, List[int]] = UNSET
    if not isinstance(virtual_chassis_id, Unset):
        json_virtual_chassis_id = virtual_chassis_id

    params["virtual_chassis_id"] = json_virtual_chassis_id

    json_virtual_chassis_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(virtual_chassis_id_n, Unset):
        json_virtual_chassis_id_n = virtual_chassis_id_n

    params["virtual_chassis_id__n"] = json_virtual_chassis_id_n

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/dcim/device-bays/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedDeviceBayList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedDeviceBayList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedDeviceBayList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[int]] = UNSET,
    role_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
) -> Response[PaginatedDeviceBayList]:
    """Get a list of device bay objects.

    Args:
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[int]]):
        role_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedDeviceBayList]
    """

    kwargs = _get_kwargs(
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_n=device_n,
        device_id=device_id,
        device_id_n=device_id_n,
        device_role=device_role,
        device_role_n=device_role_n,
        device_role_id=device_role_id,
        device_role_id_n=device_role_id_n,
        device_type=device_type,
        device_type_n=device_type_n,
        device_type_id=device_type_id,
        device_type_id_n=device_type_id_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        location=location,
        location_n=location_n,
        location_id=location_id,
        location_id_n=location_id_n,
        modified_by_request=modified_by_request,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        q=q,
        rack=rack,
        rack_n=rack_n,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        role=role,
        role_n=role_n,
        role_id=role_id,
        role_id_n=role_id_n,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        virtual_chassis=virtual_chassis,
        virtual_chassis_n=virtual_chassis_n,
        virtual_chassis_id=virtual_chassis_id,
        virtual_chassis_id_n=virtual_chassis_id_n,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[int]] = UNSET,
    role_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
) -> Optional[PaginatedDeviceBayList]:
    """Get a list of device bay objects.

    Args:
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[int]]):
        role_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedDeviceBayList
    """

    return sync_detailed(
        client=client,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_n=device_n,
        device_id=device_id,
        device_id_n=device_id_n,
        device_role=device_role,
        device_role_n=device_role_n,
        device_role_id=device_role_id,
        device_role_id_n=device_role_id_n,
        device_type=device_type,
        device_type_n=device_type_n,
        device_type_id=device_type_id,
        device_type_id_n=device_type_id_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        location=location,
        location_n=location_n,
        location_id=location_id,
        location_id_n=location_id_n,
        modified_by_request=modified_by_request,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        q=q,
        rack=rack,
        rack_n=rack_n,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        role=role,
        role_n=role_n,
        role_id=role_id,
        role_id_n=role_id_n,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        virtual_chassis=virtual_chassis,
        virtual_chassis_n=virtual_chassis_n,
        virtual_chassis_id=virtual_chassis_id,
        virtual_chassis_id_n=virtual_chassis_id_n,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[int]] = UNSET,
    role_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
) -> Response[PaginatedDeviceBayList]:
    """Get a list of device bay objects.

    Args:
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[int]]):
        role_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedDeviceBayList]
    """

    kwargs = _get_kwargs(
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_n=device_n,
        device_id=device_id,
        device_id_n=device_id_n,
        device_role=device_role,
        device_role_n=device_role_n,
        device_role_id=device_role_id,
        device_role_id_n=device_role_id_n,
        device_type=device_type,
        device_type_n=device_type_n,
        device_type_id=device_type_id,
        device_type_id_n=device_type_id_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        label=label,
        label_empty=label_empty,
        label_ic=label_ic,
        label_ie=label_ie,
        label_iew=label_iew,
        label_isw=label_isw,
        label_n=label_n,
        label_nic=label_nic,
        label_nie=label_nie,
        label_niew=label_niew,
        label_nisw=label_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        location=location,
        location_n=location_n,
        location_id=location_id,
        location_id_n=location_id_n,
        modified_by_request=modified_by_request,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        q=q,
        rack=rack,
        rack_n=rack_n,
        rack_id=rack_id,
        rack_id_n=rack_id_n,
        region=region,
        region_n=region_n,
        region_id=region_id,
        region_id_n=region_id_n,
        role=role,
        role_n=role_n,
        role_id=role_id,
        role_id_n=role_id_n,
        site=site,
        site_n=site_n,
        site_group=site_group,
        site_group_n=site_group_n,
        site_group_id=site_group_id,
        site_group_id_n=site_group_id_n,
        site_id=site_id,
        site_id_n=site_id_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        virtual_chassis=virtual_chassis,
        virtual_chassis_n=virtual_chassis_n,
        virtual_chassis_id=virtual_chassis_id,
        virtual_chassis_id_n=virtual_chassis_id_n,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[Union[None, str]]] = UNSET,
    device_n: Union[Unset, List[Union[None, str]]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    device_id_n: Union[Unset, List[int]] = UNSET,
    device_role: Union[Unset, List[str]] = UNSET,
    device_role_n: Union[Unset, List[str]] = UNSET,
    device_role_id: Union[Unset, List[int]] = UNSET,
    device_role_id_n: Union[Unset, List[int]] = UNSET,
    device_type: Union[Unset, List[str]] = UNSET,
    device_type_n: Union[Unset, List[str]] = UNSET,
    device_type_id: Union[Unset, List[int]] = UNSET,
    device_type_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    label: Union[Unset, List[str]] = UNSET,
    label_empty: Union[Unset, bool] = UNSET,
    label_ic: Union[Unset, List[str]] = UNSET,
    label_ie: Union[Unset, List[str]] = UNSET,
    label_iew: Union[Unset, List[str]] = UNSET,
    label_isw: Union[Unset, List[str]] = UNSET,
    label_n: Union[Unset, List[str]] = UNSET,
    label_nic: Union[Unset, List[str]] = UNSET,
    label_nie: Union[Unset, List[str]] = UNSET,
    label_niew: Union[Unset, List[str]] = UNSET,
    label_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    location: Union[Unset, List[str]] = UNSET,
    location_n: Union[Unset, List[str]] = UNSET,
    location_id: Union[Unset, List[int]] = UNSET,
    location_id_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    rack: Union[Unset, List[str]] = UNSET,
    rack_n: Union[Unset, List[str]] = UNSET,
    rack_id: Union[Unset, List[int]] = UNSET,
    rack_id_n: Union[Unset, List[int]] = UNSET,
    region: Union[Unset, List[int]] = UNSET,
    region_n: Union[Unset, List[int]] = UNSET,
    region_id: Union[Unset, List[int]] = UNSET,
    region_id_n: Union[Unset, List[int]] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    role_id: Union[Unset, List[int]] = UNSET,
    role_id_n: Union[Unset, List[int]] = UNSET,
    site: Union[Unset, List[str]] = UNSET,
    site_n: Union[Unset, List[str]] = UNSET,
    site_group: Union[Unset, List[int]] = UNSET,
    site_group_n: Union[Unset, List[int]] = UNSET,
    site_group_id: Union[Unset, List[int]] = UNSET,
    site_group_id_n: Union[Unset, List[int]] = UNSET,
    site_id: Union[Unset, List[int]] = UNSET,
    site_id_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_chassis: Union[Unset, List[str]] = UNSET,
    virtual_chassis_n: Union[Unset, List[str]] = UNSET,
    virtual_chassis_id: Union[Unset, List[int]] = UNSET,
    virtual_chassis_id_n: Union[Unset, List[int]] = UNSET,
) -> Optional[PaginatedDeviceBayList]:
    """Get a list of device bay objects.

    Args:
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[Union[None, str]]]):
        device_n (Union[Unset, List[Union[None, str]]]):
        device_id (Union[Unset, List[int]]):
        device_id_n (Union[Unset, List[int]]):
        device_role (Union[Unset, List[str]]):
        device_role_n (Union[Unset, List[str]]):
        device_role_id (Union[Unset, List[int]]):
        device_role_id_n (Union[Unset, List[int]]):
        device_type (Union[Unset, List[str]]):
        device_type_n (Union[Unset, List[str]]):
        device_type_id (Union[Unset, List[int]]):
        device_type_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        label (Union[Unset, List[str]]):
        label_empty (Union[Unset, bool]):
        label_ic (Union[Unset, List[str]]):
        label_ie (Union[Unset, List[str]]):
        label_iew (Union[Unset, List[str]]):
        label_isw (Union[Unset, List[str]]):
        label_n (Union[Unset, List[str]]):
        label_nic (Union[Unset, List[str]]):
        label_nie (Union[Unset, List[str]]):
        label_niew (Union[Unset, List[str]]):
        label_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        location (Union[Unset, List[str]]):
        location_n (Union[Unset, List[str]]):
        location_id (Union[Unset, List[int]]):
        location_id_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        rack (Union[Unset, List[str]]):
        rack_n (Union[Unset, List[str]]):
        rack_id (Union[Unset, List[int]]):
        rack_id_n (Union[Unset, List[int]]):
        region (Union[Unset, List[int]]):
        region_n (Union[Unset, List[int]]):
        region_id (Union[Unset, List[int]]):
        region_id_n (Union[Unset, List[int]]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        role_id (Union[Unset, List[int]]):
        role_id_n (Union[Unset, List[int]]):
        site (Union[Unset, List[str]]):
        site_n (Union[Unset, List[str]]):
        site_group (Union[Unset, List[int]]):
        site_group_n (Union[Unset, List[int]]):
        site_group_id (Union[Unset, List[int]]):
        site_group_id_n (Union[Unset, List[int]]):
        site_id (Union[Unset, List[int]]):
        site_id_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_chassis (Union[Unset, List[str]]):
        virtual_chassis_n (Union[Unset, List[str]]):
        virtual_chassis_id (Union[Unset, List[int]]):
        virtual_chassis_id_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedDeviceBayList
    """

    return (
        await asyncio_detailed(
            client=client,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            description=description,
            description_empty=description_empty,
            description_ic=description_ic,
            description_ie=description_ie,
            description_iew=description_iew,
            description_isw=description_isw,
            description_n=description_n,
            description_nic=description_nic,
            description_nie=description_nie,
            description_niew=description_niew,
            description_nisw=description_nisw,
            device=device,
            device_n=device_n,
            device_id=device_id,
            device_id_n=device_id_n,
            device_role=device_role,
            device_role_n=device_role_n,
            device_role_id=device_role_id,
            device_role_id_n=device_role_id_n,
            device_type=device_type,
            device_type_n=device_type_n,
            device_type_id=device_type_id,
            device_type_id_n=device_type_id_n,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            label=label,
            label_empty=label_empty,
            label_ic=label_ic,
            label_ie=label_ie,
            label_iew=label_iew,
            label_isw=label_isw,
            label_n=label_n,
            label_nic=label_nic,
            label_nie=label_nie,
            label_niew=label_niew,
            label_nisw=label_nisw,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            limit=limit,
            location=location,
            location_n=location_n,
            location_id=location_id,
            location_id_n=location_id_n,
            modified_by_request=modified_by_request,
            name=name,
            name_empty=name_empty,
            name_ic=name_ic,
            name_ie=name_ie,
            name_iew=name_iew,
            name_isw=name_isw,
            name_n=name_n,
            name_nic=name_nic,
            name_nie=name_nie,
            name_niew=name_niew,
            name_nisw=name_nisw,
            offset=offset,
            ordering=ordering,
            q=q,
            rack=rack,
            rack_n=rack_n,
            rack_id=rack_id,
            rack_id_n=rack_id_n,
            region=region,
            region_n=region_n,
            region_id=region_id,
            region_id_n=region_id_n,
            role=role,
            role_n=role_n,
            role_id=role_id,
            role_id_n=role_id_n,
            site=site,
            site_n=site_n,
            site_group=site_group,
            site_group_n=site_group_n,
            site_group_id=site_group_id,
            site_group_id_n=site_group_id_n,
            site_id=site_id,
            site_id_n=site_id_n,
            tag=tag,
            tag_n=tag_n,
            updated_by_request=updated_by_request,
            virtual_chassis=virtual_chassis,
            virtual_chassis_n=virtual_chassis_n,
            virtual_chassis_id=virtual_chassis_id,
            virtual_chassis_id_n=virtual_chassis_id_n,
        )
    ).parsed
