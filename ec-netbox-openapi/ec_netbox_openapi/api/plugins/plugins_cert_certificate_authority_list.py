import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_certificate_authority_list import (
    PaginatedCertificateAuthorityList,
)
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    acme_url: Union[Unset, List[str]] = UNSET,
    acme_url_empty: Union[Unset, bool] = UNSET,
    acme_url_ic: Union[Unset, List[str]] = UNSET,
    acme_url_ie: Union[Unset, List[str]] = UNSET,
    acme_url_iew: Union[Unset, List[str]] = UNSET,
    acme_url_isw: Union[Unset, List[str]] = UNSET,
    acme_url_n: Union[Unset, List[str]] = UNSET,
    acme_url_nic: Union[Unset, List[str]] = UNSET,
    acme_url_nie: Union[Unset, List[str]] = UNSET,
    acme_url_niew: Union[Unset, List[str]] = UNSET,
    acme_url_nisw: Union[Unset, List[str]] = UNSET,
    alt_name: Union[Unset, str] = UNSET,
    ca_name: Union[Unset, List[str]] = UNSET,
    ca_name_empty: Union[Unset, bool] = UNSET,
    ca_name_ic: Union[Unset, List[str]] = UNSET,
    ca_name_ie: Union[Unset, List[str]] = UNSET,
    ca_name_iew: Union[Unset, List[str]] = UNSET,
    ca_name_isw: Union[Unset, List[str]] = UNSET,
    ca_name_n: Union[Unset, List[str]] = UNSET,
    ca_name_nic: Union[Unset, List[str]] = UNSET,
    ca_name_nie: Union[Unset, List[str]] = UNSET,
    ca_name_niew: Union[Unset, List[str]] = UNSET,
    ca_name_nisw: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    default_validity: Union[Unset, List[int]] = UNSET,
    default_validity_empty: Union[Unset, bool] = UNSET,
    default_validity_gt: Union[Unset, List[int]] = UNSET,
    default_validity_gte: Union[Unset, List[int]] = UNSET,
    default_validity_lt: Union[Unset, List[int]] = UNSET,
    default_validity_lte: Union[Unset, List[int]] = UNSET,
    default_validity_n: Union[Unset, List[int]] = UNSET,
    end_date: Union[Unset, List[datetime.date]] = UNSET,
    end_date_empty: Union[Unset, bool] = UNSET,
    end_date_gt: Union[Unset, List[datetime.date]] = UNSET,
    end_date_gte: Union[Unset, List[datetime.date]] = UNSET,
    end_date_lt: Union[Unset, List[datetime.date]] = UNSET,
    end_date_lte: Union[Unset, List[datetime.date]] = UNSET,
    end_date_n: Union[Unset, List[datetime.date]] = UNSET,
    key_vault_url: Union[Unset, List[str]] = UNSET,
    key_vault_url_empty: Union[Unset, bool] = UNSET,
    key_vault_url_ic: Union[Unset, List[str]] = UNSET,
    key_vault_url_ie: Union[Unset, List[str]] = UNSET,
    key_vault_url_iew: Union[Unset, List[str]] = UNSET,
    key_vault_url_isw: Union[Unset, List[str]] = UNSET,
    key_vault_url_n: Union[Unset, List[str]] = UNSET,
    key_vault_url_nic: Union[Unset, List[str]] = UNSET,
    key_vault_url_nie: Union[Unset, List[str]] = UNSET,
    key_vault_url_niew: Union[Unset, List[str]] = UNSET,
    key_vault_url_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    start_date: Union[Unset, List[datetime.date]] = UNSET,
    start_date_empty: Union[Unset, bool] = UNSET,
    start_date_gt: Union[Unset, List[datetime.date]] = UNSET,
    start_date_gte: Union[Unset, List[datetime.date]] = UNSET,
    start_date_lt: Union[Unset, List[datetime.date]] = UNSET,
    start_date_lte: Union[Unset, List[datetime.date]] = UNSET,
    start_date_n: Union[Unset, List[datetime.date]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_acme_url: Union[Unset, List[str]] = UNSET
    if not isinstance(acme_url, Unset):
        json_acme_url = acme_url

    params["acme_url"] = json_acme_url

    params["acme_url__empty"] = acme_url_empty

    json_acme_url_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(acme_url_ic, Unset):
        json_acme_url_ic = acme_url_ic

    params["acme_url__ic"] = json_acme_url_ic

    json_acme_url_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(acme_url_ie, Unset):
        json_acme_url_ie = acme_url_ie

    params["acme_url__ie"] = json_acme_url_ie

    json_acme_url_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(acme_url_iew, Unset):
        json_acme_url_iew = acme_url_iew

    params["acme_url__iew"] = json_acme_url_iew

    json_acme_url_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(acme_url_isw, Unset):
        json_acme_url_isw = acme_url_isw

    params["acme_url__isw"] = json_acme_url_isw

    json_acme_url_n: Union[Unset, List[str]] = UNSET
    if not isinstance(acme_url_n, Unset):
        json_acme_url_n = acme_url_n

    params["acme_url__n"] = json_acme_url_n

    json_acme_url_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(acme_url_nic, Unset):
        json_acme_url_nic = acme_url_nic

    params["acme_url__nic"] = json_acme_url_nic

    json_acme_url_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(acme_url_nie, Unset):
        json_acme_url_nie = acme_url_nie

    params["acme_url__nie"] = json_acme_url_nie

    json_acme_url_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(acme_url_niew, Unset):
        json_acme_url_niew = acme_url_niew

    params["acme_url__niew"] = json_acme_url_niew

    json_acme_url_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(acme_url_nisw, Unset):
        json_acme_url_nisw = acme_url_nisw

    params["acme_url__nisw"] = json_acme_url_nisw

    params["alt_name"] = alt_name

    json_ca_name: Union[Unset, List[str]] = UNSET
    if not isinstance(ca_name, Unset):
        json_ca_name = ca_name

    params["ca_name"] = json_ca_name

    params["ca_name__empty"] = ca_name_empty

    json_ca_name_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(ca_name_ic, Unset):
        json_ca_name_ic = ca_name_ic

    params["ca_name__ic"] = json_ca_name_ic

    json_ca_name_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(ca_name_ie, Unset):
        json_ca_name_ie = ca_name_ie

    params["ca_name__ie"] = json_ca_name_ie

    json_ca_name_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(ca_name_iew, Unset):
        json_ca_name_iew = ca_name_iew

    params["ca_name__iew"] = json_ca_name_iew

    json_ca_name_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(ca_name_isw, Unset):
        json_ca_name_isw = ca_name_isw

    params["ca_name__isw"] = json_ca_name_isw

    json_ca_name_n: Union[Unset, List[str]] = UNSET
    if not isinstance(ca_name_n, Unset):
        json_ca_name_n = ca_name_n

    params["ca_name__n"] = json_ca_name_n

    json_ca_name_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(ca_name_nic, Unset):
        json_ca_name_nic = ca_name_nic

    params["ca_name__nic"] = json_ca_name_nic

    json_ca_name_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(ca_name_nie, Unset):
        json_ca_name_nie = ca_name_nie

    params["ca_name__nie"] = json_ca_name_nie

    json_ca_name_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(ca_name_niew, Unset):
        json_ca_name_niew = ca_name_niew

    params["ca_name__niew"] = json_ca_name_niew

    json_ca_name_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(ca_name_nisw, Unset):
        json_ca_name_nisw = ca_name_nisw

    params["ca_name__nisw"] = json_ca_name_nisw

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    json_default_validity: Union[Unset, List[int]] = UNSET
    if not isinstance(default_validity, Unset):
        json_default_validity = default_validity

    params["default_validity"] = json_default_validity

    params["default_validity__empty"] = default_validity_empty

    json_default_validity_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(default_validity_gt, Unset):
        json_default_validity_gt = default_validity_gt

    params["default_validity__gt"] = json_default_validity_gt

    json_default_validity_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(default_validity_gte, Unset):
        json_default_validity_gte = default_validity_gte

    params["default_validity__gte"] = json_default_validity_gte

    json_default_validity_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(default_validity_lt, Unset):
        json_default_validity_lt = default_validity_lt

    params["default_validity__lt"] = json_default_validity_lt

    json_default_validity_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(default_validity_lte, Unset):
        json_default_validity_lte = default_validity_lte

    params["default_validity__lte"] = json_default_validity_lte

    json_default_validity_n: Union[Unset, List[int]] = UNSET
    if not isinstance(default_validity_n, Unset):
        json_default_validity_n = default_validity_n

    params["default_validity__n"] = json_default_validity_n

    json_end_date: Union[Unset, List[str]] = UNSET
    if not isinstance(end_date, Unset):
        json_end_date = []
        for end_date_item_data in end_date:
            end_date_item = end_date_item_data.isoformat()
            json_end_date.append(end_date_item)

    params["end_date"] = json_end_date

    params["end_date__empty"] = end_date_empty

    json_end_date_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(end_date_gt, Unset):
        json_end_date_gt = []
        for end_date_gt_item_data in end_date_gt:
            end_date_gt_item = end_date_gt_item_data.isoformat()
            json_end_date_gt.append(end_date_gt_item)

    params["end_date__gt"] = json_end_date_gt

    json_end_date_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(end_date_gte, Unset):
        json_end_date_gte = []
        for end_date_gte_item_data in end_date_gte:
            end_date_gte_item = end_date_gte_item_data.isoformat()
            json_end_date_gte.append(end_date_gte_item)

    params["end_date__gte"] = json_end_date_gte

    json_end_date_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(end_date_lt, Unset):
        json_end_date_lt = []
        for end_date_lt_item_data in end_date_lt:
            end_date_lt_item = end_date_lt_item_data.isoformat()
            json_end_date_lt.append(end_date_lt_item)

    params["end_date__lt"] = json_end_date_lt

    json_end_date_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(end_date_lte, Unset):
        json_end_date_lte = []
        for end_date_lte_item_data in end_date_lte:
            end_date_lte_item = end_date_lte_item_data.isoformat()
            json_end_date_lte.append(end_date_lte_item)

    params["end_date__lte"] = json_end_date_lte

    json_end_date_n: Union[Unset, List[str]] = UNSET
    if not isinstance(end_date_n, Unset):
        json_end_date_n = []
        for end_date_n_item_data in end_date_n:
            end_date_n_item = end_date_n_item_data.isoformat()
            json_end_date_n.append(end_date_n_item)

    params["end_date__n"] = json_end_date_n

    json_key_vault_url: Union[Unset, List[str]] = UNSET
    if not isinstance(key_vault_url, Unset):
        json_key_vault_url = key_vault_url

    params["key_vault_url"] = json_key_vault_url

    params["key_vault_url__empty"] = key_vault_url_empty

    json_key_vault_url_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(key_vault_url_ic, Unset):
        json_key_vault_url_ic = key_vault_url_ic

    params["key_vault_url__ic"] = json_key_vault_url_ic

    json_key_vault_url_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(key_vault_url_ie, Unset):
        json_key_vault_url_ie = key_vault_url_ie

    params["key_vault_url__ie"] = json_key_vault_url_ie

    json_key_vault_url_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(key_vault_url_iew, Unset):
        json_key_vault_url_iew = key_vault_url_iew

    params["key_vault_url__iew"] = json_key_vault_url_iew

    json_key_vault_url_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(key_vault_url_isw, Unset):
        json_key_vault_url_isw = key_vault_url_isw

    params["key_vault_url__isw"] = json_key_vault_url_isw

    json_key_vault_url_n: Union[Unset, List[str]] = UNSET
    if not isinstance(key_vault_url_n, Unset):
        json_key_vault_url_n = key_vault_url_n

    params["key_vault_url__n"] = json_key_vault_url_n

    json_key_vault_url_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(key_vault_url_nic, Unset):
        json_key_vault_url_nic = key_vault_url_nic

    params["key_vault_url__nic"] = json_key_vault_url_nic

    json_key_vault_url_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(key_vault_url_nie, Unset):
        json_key_vault_url_nie = key_vault_url_nie

    params["key_vault_url__nie"] = json_key_vault_url_nie

    json_key_vault_url_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(key_vault_url_niew, Unset):
        json_key_vault_url_niew = key_vault_url_niew

    params["key_vault_url__niew"] = json_key_vault_url_niew

    json_key_vault_url_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(key_vault_url_nisw, Unset):
        json_key_vault_url_nisw = key_vault_url_nisw

    params["key_vault_url__nisw"] = json_key_vault_url_nisw

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    params["limit"] = limit

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    params["offset"] = offset

    params["ordering"] = ordering

    params["q"] = q

    json_start_date: Union[Unset, List[str]] = UNSET
    if not isinstance(start_date, Unset):
        json_start_date = []
        for start_date_item_data in start_date:
            start_date_item = start_date_item_data.isoformat()
            json_start_date.append(start_date_item)

    params["start_date"] = json_start_date

    params["start_date__empty"] = start_date_empty

    json_start_date_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(start_date_gt, Unset):
        json_start_date_gt = []
        for start_date_gt_item_data in start_date_gt:
            start_date_gt_item = start_date_gt_item_data.isoformat()
            json_start_date_gt.append(start_date_gt_item)

    params["start_date__gt"] = json_start_date_gt

    json_start_date_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(start_date_gte, Unset):
        json_start_date_gte = []
        for start_date_gte_item_data in start_date_gte:
            start_date_gte_item = start_date_gte_item_data.isoformat()
            json_start_date_gte.append(start_date_gte_item)

    params["start_date__gte"] = json_start_date_gte

    json_start_date_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(start_date_lt, Unset):
        json_start_date_lt = []
        for start_date_lt_item_data in start_date_lt:
            start_date_lt_item = start_date_lt_item_data.isoformat()
            json_start_date_lt.append(start_date_lt_item)

    params["start_date__lt"] = json_start_date_lt

    json_start_date_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(start_date_lte, Unset):
        json_start_date_lte = []
        for start_date_lte_item_data in start_date_lte:
            start_date_lte_item = start_date_lte_item_data.isoformat()
            json_start_date_lte.append(start_date_lte_item)

    params["start_date__lte"] = json_start_date_lte

    json_start_date_n: Union[Unset, List[str]] = UNSET
    if not isinstance(start_date_n, Unset):
        json_start_date_n = []
        for start_date_n_item_data in start_date_n:
            start_date_n_item = start_date_n_item_data.isoformat()
            json_start_date_n.append(start_date_n_item)

    params["start_date__n"] = json_start_date_n

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/plugins/cert/CertificateAuthority/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedCertificateAuthorityList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedCertificateAuthorityList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedCertificateAuthorityList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    acme_url: Union[Unset, List[str]] = UNSET,
    acme_url_empty: Union[Unset, bool] = UNSET,
    acme_url_ic: Union[Unset, List[str]] = UNSET,
    acme_url_ie: Union[Unset, List[str]] = UNSET,
    acme_url_iew: Union[Unset, List[str]] = UNSET,
    acme_url_isw: Union[Unset, List[str]] = UNSET,
    acme_url_n: Union[Unset, List[str]] = UNSET,
    acme_url_nic: Union[Unset, List[str]] = UNSET,
    acme_url_nie: Union[Unset, List[str]] = UNSET,
    acme_url_niew: Union[Unset, List[str]] = UNSET,
    acme_url_nisw: Union[Unset, List[str]] = UNSET,
    alt_name: Union[Unset, str] = UNSET,
    ca_name: Union[Unset, List[str]] = UNSET,
    ca_name_empty: Union[Unset, bool] = UNSET,
    ca_name_ic: Union[Unset, List[str]] = UNSET,
    ca_name_ie: Union[Unset, List[str]] = UNSET,
    ca_name_iew: Union[Unset, List[str]] = UNSET,
    ca_name_isw: Union[Unset, List[str]] = UNSET,
    ca_name_n: Union[Unset, List[str]] = UNSET,
    ca_name_nic: Union[Unset, List[str]] = UNSET,
    ca_name_nie: Union[Unset, List[str]] = UNSET,
    ca_name_niew: Union[Unset, List[str]] = UNSET,
    ca_name_nisw: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    default_validity: Union[Unset, List[int]] = UNSET,
    default_validity_empty: Union[Unset, bool] = UNSET,
    default_validity_gt: Union[Unset, List[int]] = UNSET,
    default_validity_gte: Union[Unset, List[int]] = UNSET,
    default_validity_lt: Union[Unset, List[int]] = UNSET,
    default_validity_lte: Union[Unset, List[int]] = UNSET,
    default_validity_n: Union[Unset, List[int]] = UNSET,
    end_date: Union[Unset, List[datetime.date]] = UNSET,
    end_date_empty: Union[Unset, bool] = UNSET,
    end_date_gt: Union[Unset, List[datetime.date]] = UNSET,
    end_date_gte: Union[Unset, List[datetime.date]] = UNSET,
    end_date_lt: Union[Unset, List[datetime.date]] = UNSET,
    end_date_lte: Union[Unset, List[datetime.date]] = UNSET,
    end_date_n: Union[Unset, List[datetime.date]] = UNSET,
    key_vault_url: Union[Unset, List[str]] = UNSET,
    key_vault_url_empty: Union[Unset, bool] = UNSET,
    key_vault_url_ic: Union[Unset, List[str]] = UNSET,
    key_vault_url_ie: Union[Unset, List[str]] = UNSET,
    key_vault_url_iew: Union[Unset, List[str]] = UNSET,
    key_vault_url_isw: Union[Unset, List[str]] = UNSET,
    key_vault_url_n: Union[Unset, List[str]] = UNSET,
    key_vault_url_nic: Union[Unset, List[str]] = UNSET,
    key_vault_url_nie: Union[Unset, List[str]] = UNSET,
    key_vault_url_niew: Union[Unset, List[str]] = UNSET,
    key_vault_url_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    start_date: Union[Unset, List[datetime.date]] = UNSET,
    start_date_empty: Union[Unset, bool] = UNSET,
    start_date_gt: Union[Unset, List[datetime.date]] = UNSET,
    start_date_gte: Union[Unset, List[datetime.date]] = UNSET,
    start_date_lt: Union[Unset, List[datetime.date]] = UNSET,
    start_date_lte: Union[Unset, List[datetime.date]] = UNSET,
    start_date_n: Union[Unset, List[datetime.date]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Response[PaginatedCertificateAuthorityList]:
    """Mapping view set class

    Args:
        acme_url (Union[Unset, List[str]]):
        acme_url_empty (Union[Unset, bool]):
        acme_url_ic (Union[Unset, List[str]]):
        acme_url_ie (Union[Unset, List[str]]):
        acme_url_iew (Union[Unset, List[str]]):
        acme_url_isw (Union[Unset, List[str]]):
        acme_url_n (Union[Unset, List[str]]):
        acme_url_nic (Union[Unset, List[str]]):
        acme_url_nie (Union[Unset, List[str]]):
        acme_url_niew (Union[Unset, List[str]]):
        acme_url_nisw (Union[Unset, List[str]]):
        alt_name (Union[Unset, str]):
        ca_name (Union[Unset, List[str]]):
        ca_name_empty (Union[Unset, bool]):
        ca_name_ic (Union[Unset, List[str]]):
        ca_name_ie (Union[Unset, List[str]]):
        ca_name_iew (Union[Unset, List[str]]):
        ca_name_isw (Union[Unset, List[str]]):
        ca_name_n (Union[Unset, List[str]]):
        ca_name_nic (Union[Unset, List[str]]):
        ca_name_nie (Union[Unset, List[str]]):
        ca_name_niew (Union[Unset, List[str]]):
        ca_name_nisw (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        default_validity (Union[Unset, List[int]]):
        default_validity_empty (Union[Unset, bool]):
        default_validity_gt (Union[Unset, List[int]]):
        default_validity_gte (Union[Unset, List[int]]):
        default_validity_lt (Union[Unset, List[int]]):
        default_validity_lte (Union[Unset, List[int]]):
        default_validity_n (Union[Unset, List[int]]):
        end_date (Union[Unset, List[datetime.date]]):
        end_date_empty (Union[Unset, bool]):
        end_date_gt (Union[Unset, List[datetime.date]]):
        end_date_gte (Union[Unset, List[datetime.date]]):
        end_date_lt (Union[Unset, List[datetime.date]]):
        end_date_lte (Union[Unset, List[datetime.date]]):
        end_date_n (Union[Unset, List[datetime.date]]):
        key_vault_url (Union[Unset, List[str]]):
        key_vault_url_empty (Union[Unset, bool]):
        key_vault_url_ic (Union[Unset, List[str]]):
        key_vault_url_ie (Union[Unset, List[str]]):
        key_vault_url_iew (Union[Unset, List[str]]):
        key_vault_url_isw (Union[Unset, List[str]]):
        key_vault_url_n (Union[Unset, List[str]]):
        key_vault_url_nic (Union[Unset, List[str]]):
        key_vault_url_nie (Union[Unset, List[str]]):
        key_vault_url_niew (Union[Unset, List[str]]):
        key_vault_url_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        start_date (Union[Unset, List[datetime.date]]):
        start_date_empty (Union[Unset, bool]):
        start_date_gt (Union[Unset, List[datetime.date]]):
        start_date_gte (Union[Unset, List[datetime.date]]):
        start_date_lt (Union[Unset, List[datetime.date]]):
        start_date_lte (Union[Unset, List[datetime.date]]):
        start_date_n (Union[Unset, List[datetime.date]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCertificateAuthorityList]
    """

    kwargs = _get_kwargs(
        acme_url=acme_url,
        acme_url_empty=acme_url_empty,
        acme_url_ic=acme_url_ic,
        acme_url_ie=acme_url_ie,
        acme_url_iew=acme_url_iew,
        acme_url_isw=acme_url_isw,
        acme_url_n=acme_url_n,
        acme_url_nic=acme_url_nic,
        acme_url_nie=acme_url_nie,
        acme_url_niew=acme_url_niew,
        acme_url_nisw=acme_url_nisw,
        alt_name=alt_name,
        ca_name=ca_name,
        ca_name_empty=ca_name_empty,
        ca_name_ic=ca_name_ic,
        ca_name_ie=ca_name_ie,
        ca_name_iew=ca_name_iew,
        ca_name_isw=ca_name_isw,
        ca_name_n=ca_name_n,
        ca_name_nic=ca_name_nic,
        ca_name_nie=ca_name_nie,
        ca_name_niew=ca_name_niew,
        ca_name_nisw=ca_name_nisw,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        default_validity=default_validity,
        default_validity_empty=default_validity_empty,
        default_validity_gt=default_validity_gt,
        default_validity_gte=default_validity_gte,
        default_validity_lt=default_validity_lt,
        default_validity_lte=default_validity_lte,
        default_validity_n=default_validity_n,
        end_date=end_date,
        end_date_empty=end_date_empty,
        end_date_gt=end_date_gt,
        end_date_gte=end_date_gte,
        end_date_lt=end_date_lt,
        end_date_lte=end_date_lte,
        end_date_n=end_date_n,
        key_vault_url=key_vault_url,
        key_vault_url_empty=key_vault_url_empty,
        key_vault_url_ic=key_vault_url_ic,
        key_vault_url_ie=key_vault_url_ie,
        key_vault_url_iew=key_vault_url_iew,
        key_vault_url_isw=key_vault_url_isw,
        key_vault_url_n=key_vault_url_n,
        key_vault_url_nic=key_vault_url_nic,
        key_vault_url_nie=key_vault_url_nie,
        key_vault_url_niew=key_vault_url_niew,
        key_vault_url_nisw=key_vault_url_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        start_date=start_date,
        start_date_empty=start_date_empty,
        start_date_gt=start_date_gt,
        start_date_gte=start_date_gte,
        start_date_lt=start_date_lt,
        start_date_lte=start_date_lte,
        start_date_n=start_date_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    acme_url: Union[Unset, List[str]] = UNSET,
    acme_url_empty: Union[Unset, bool] = UNSET,
    acme_url_ic: Union[Unset, List[str]] = UNSET,
    acme_url_ie: Union[Unset, List[str]] = UNSET,
    acme_url_iew: Union[Unset, List[str]] = UNSET,
    acme_url_isw: Union[Unset, List[str]] = UNSET,
    acme_url_n: Union[Unset, List[str]] = UNSET,
    acme_url_nic: Union[Unset, List[str]] = UNSET,
    acme_url_nie: Union[Unset, List[str]] = UNSET,
    acme_url_niew: Union[Unset, List[str]] = UNSET,
    acme_url_nisw: Union[Unset, List[str]] = UNSET,
    alt_name: Union[Unset, str] = UNSET,
    ca_name: Union[Unset, List[str]] = UNSET,
    ca_name_empty: Union[Unset, bool] = UNSET,
    ca_name_ic: Union[Unset, List[str]] = UNSET,
    ca_name_ie: Union[Unset, List[str]] = UNSET,
    ca_name_iew: Union[Unset, List[str]] = UNSET,
    ca_name_isw: Union[Unset, List[str]] = UNSET,
    ca_name_n: Union[Unset, List[str]] = UNSET,
    ca_name_nic: Union[Unset, List[str]] = UNSET,
    ca_name_nie: Union[Unset, List[str]] = UNSET,
    ca_name_niew: Union[Unset, List[str]] = UNSET,
    ca_name_nisw: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    default_validity: Union[Unset, List[int]] = UNSET,
    default_validity_empty: Union[Unset, bool] = UNSET,
    default_validity_gt: Union[Unset, List[int]] = UNSET,
    default_validity_gte: Union[Unset, List[int]] = UNSET,
    default_validity_lt: Union[Unset, List[int]] = UNSET,
    default_validity_lte: Union[Unset, List[int]] = UNSET,
    default_validity_n: Union[Unset, List[int]] = UNSET,
    end_date: Union[Unset, List[datetime.date]] = UNSET,
    end_date_empty: Union[Unset, bool] = UNSET,
    end_date_gt: Union[Unset, List[datetime.date]] = UNSET,
    end_date_gte: Union[Unset, List[datetime.date]] = UNSET,
    end_date_lt: Union[Unset, List[datetime.date]] = UNSET,
    end_date_lte: Union[Unset, List[datetime.date]] = UNSET,
    end_date_n: Union[Unset, List[datetime.date]] = UNSET,
    key_vault_url: Union[Unset, List[str]] = UNSET,
    key_vault_url_empty: Union[Unset, bool] = UNSET,
    key_vault_url_ic: Union[Unset, List[str]] = UNSET,
    key_vault_url_ie: Union[Unset, List[str]] = UNSET,
    key_vault_url_iew: Union[Unset, List[str]] = UNSET,
    key_vault_url_isw: Union[Unset, List[str]] = UNSET,
    key_vault_url_n: Union[Unset, List[str]] = UNSET,
    key_vault_url_nic: Union[Unset, List[str]] = UNSET,
    key_vault_url_nie: Union[Unset, List[str]] = UNSET,
    key_vault_url_niew: Union[Unset, List[str]] = UNSET,
    key_vault_url_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    start_date: Union[Unset, List[datetime.date]] = UNSET,
    start_date_empty: Union[Unset, bool] = UNSET,
    start_date_gt: Union[Unset, List[datetime.date]] = UNSET,
    start_date_gte: Union[Unset, List[datetime.date]] = UNSET,
    start_date_lt: Union[Unset, List[datetime.date]] = UNSET,
    start_date_lte: Union[Unset, List[datetime.date]] = UNSET,
    start_date_n: Union[Unset, List[datetime.date]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Optional[PaginatedCertificateAuthorityList]:
    """Mapping view set class

    Args:
        acme_url (Union[Unset, List[str]]):
        acme_url_empty (Union[Unset, bool]):
        acme_url_ic (Union[Unset, List[str]]):
        acme_url_ie (Union[Unset, List[str]]):
        acme_url_iew (Union[Unset, List[str]]):
        acme_url_isw (Union[Unset, List[str]]):
        acme_url_n (Union[Unset, List[str]]):
        acme_url_nic (Union[Unset, List[str]]):
        acme_url_nie (Union[Unset, List[str]]):
        acme_url_niew (Union[Unset, List[str]]):
        acme_url_nisw (Union[Unset, List[str]]):
        alt_name (Union[Unset, str]):
        ca_name (Union[Unset, List[str]]):
        ca_name_empty (Union[Unset, bool]):
        ca_name_ic (Union[Unset, List[str]]):
        ca_name_ie (Union[Unset, List[str]]):
        ca_name_iew (Union[Unset, List[str]]):
        ca_name_isw (Union[Unset, List[str]]):
        ca_name_n (Union[Unset, List[str]]):
        ca_name_nic (Union[Unset, List[str]]):
        ca_name_nie (Union[Unset, List[str]]):
        ca_name_niew (Union[Unset, List[str]]):
        ca_name_nisw (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        default_validity (Union[Unset, List[int]]):
        default_validity_empty (Union[Unset, bool]):
        default_validity_gt (Union[Unset, List[int]]):
        default_validity_gte (Union[Unset, List[int]]):
        default_validity_lt (Union[Unset, List[int]]):
        default_validity_lte (Union[Unset, List[int]]):
        default_validity_n (Union[Unset, List[int]]):
        end_date (Union[Unset, List[datetime.date]]):
        end_date_empty (Union[Unset, bool]):
        end_date_gt (Union[Unset, List[datetime.date]]):
        end_date_gte (Union[Unset, List[datetime.date]]):
        end_date_lt (Union[Unset, List[datetime.date]]):
        end_date_lte (Union[Unset, List[datetime.date]]):
        end_date_n (Union[Unset, List[datetime.date]]):
        key_vault_url (Union[Unset, List[str]]):
        key_vault_url_empty (Union[Unset, bool]):
        key_vault_url_ic (Union[Unset, List[str]]):
        key_vault_url_ie (Union[Unset, List[str]]):
        key_vault_url_iew (Union[Unset, List[str]]):
        key_vault_url_isw (Union[Unset, List[str]]):
        key_vault_url_n (Union[Unset, List[str]]):
        key_vault_url_nic (Union[Unset, List[str]]):
        key_vault_url_nie (Union[Unset, List[str]]):
        key_vault_url_niew (Union[Unset, List[str]]):
        key_vault_url_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        start_date (Union[Unset, List[datetime.date]]):
        start_date_empty (Union[Unset, bool]):
        start_date_gt (Union[Unset, List[datetime.date]]):
        start_date_gte (Union[Unset, List[datetime.date]]):
        start_date_lt (Union[Unset, List[datetime.date]]):
        start_date_lte (Union[Unset, List[datetime.date]]):
        start_date_n (Union[Unset, List[datetime.date]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCertificateAuthorityList
    """

    return sync_detailed(
        client=client,
        acme_url=acme_url,
        acme_url_empty=acme_url_empty,
        acme_url_ic=acme_url_ic,
        acme_url_ie=acme_url_ie,
        acme_url_iew=acme_url_iew,
        acme_url_isw=acme_url_isw,
        acme_url_n=acme_url_n,
        acme_url_nic=acme_url_nic,
        acme_url_nie=acme_url_nie,
        acme_url_niew=acme_url_niew,
        acme_url_nisw=acme_url_nisw,
        alt_name=alt_name,
        ca_name=ca_name,
        ca_name_empty=ca_name_empty,
        ca_name_ic=ca_name_ic,
        ca_name_ie=ca_name_ie,
        ca_name_iew=ca_name_iew,
        ca_name_isw=ca_name_isw,
        ca_name_n=ca_name_n,
        ca_name_nic=ca_name_nic,
        ca_name_nie=ca_name_nie,
        ca_name_niew=ca_name_niew,
        ca_name_nisw=ca_name_nisw,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        default_validity=default_validity,
        default_validity_empty=default_validity_empty,
        default_validity_gt=default_validity_gt,
        default_validity_gte=default_validity_gte,
        default_validity_lt=default_validity_lt,
        default_validity_lte=default_validity_lte,
        default_validity_n=default_validity_n,
        end_date=end_date,
        end_date_empty=end_date_empty,
        end_date_gt=end_date_gt,
        end_date_gte=end_date_gte,
        end_date_lt=end_date_lt,
        end_date_lte=end_date_lte,
        end_date_n=end_date_n,
        key_vault_url=key_vault_url,
        key_vault_url_empty=key_vault_url_empty,
        key_vault_url_ic=key_vault_url_ic,
        key_vault_url_ie=key_vault_url_ie,
        key_vault_url_iew=key_vault_url_iew,
        key_vault_url_isw=key_vault_url_isw,
        key_vault_url_n=key_vault_url_n,
        key_vault_url_nic=key_vault_url_nic,
        key_vault_url_nie=key_vault_url_nie,
        key_vault_url_niew=key_vault_url_niew,
        key_vault_url_nisw=key_vault_url_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        start_date=start_date,
        start_date_empty=start_date_empty,
        start_date_gt=start_date_gt,
        start_date_gte=start_date_gte,
        start_date_lt=start_date_lt,
        start_date_lte=start_date_lte,
        start_date_n=start_date_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    acme_url: Union[Unset, List[str]] = UNSET,
    acme_url_empty: Union[Unset, bool] = UNSET,
    acme_url_ic: Union[Unset, List[str]] = UNSET,
    acme_url_ie: Union[Unset, List[str]] = UNSET,
    acme_url_iew: Union[Unset, List[str]] = UNSET,
    acme_url_isw: Union[Unset, List[str]] = UNSET,
    acme_url_n: Union[Unset, List[str]] = UNSET,
    acme_url_nic: Union[Unset, List[str]] = UNSET,
    acme_url_nie: Union[Unset, List[str]] = UNSET,
    acme_url_niew: Union[Unset, List[str]] = UNSET,
    acme_url_nisw: Union[Unset, List[str]] = UNSET,
    alt_name: Union[Unset, str] = UNSET,
    ca_name: Union[Unset, List[str]] = UNSET,
    ca_name_empty: Union[Unset, bool] = UNSET,
    ca_name_ic: Union[Unset, List[str]] = UNSET,
    ca_name_ie: Union[Unset, List[str]] = UNSET,
    ca_name_iew: Union[Unset, List[str]] = UNSET,
    ca_name_isw: Union[Unset, List[str]] = UNSET,
    ca_name_n: Union[Unset, List[str]] = UNSET,
    ca_name_nic: Union[Unset, List[str]] = UNSET,
    ca_name_nie: Union[Unset, List[str]] = UNSET,
    ca_name_niew: Union[Unset, List[str]] = UNSET,
    ca_name_nisw: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    default_validity: Union[Unset, List[int]] = UNSET,
    default_validity_empty: Union[Unset, bool] = UNSET,
    default_validity_gt: Union[Unset, List[int]] = UNSET,
    default_validity_gte: Union[Unset, List[int]] = UNSET,
    default_validity_lt: Union[Unset, List[int]] = UNSET,
    default_validity_lte: Union[Unset, List[int]] = UNSET,
    default_validity_n: Union[Unset, List[int]] = UNSET,
    end_date: Union[Unset, List[datetime.date]] = UNSET,
    end_date_empty: Union[Unset, bool] = UNSET,
    end_date_gt: Union[Unset, List[datetime.date]] = UNSET,
    end_date_gte: Union[Unset, List[datetime.date]] = UNSET,
    end_date_lt: Union[Unset, List[datetime.date]] = UNSET,
    end_date_lte: Union[Unset, List[datetime.date]] = UNSET,
    end_date_n: Union[Unset, List[datetime.date]] = UNSET,
    key_vault_url: Union[Unset, List[str]] = UNSET,
    key_vault_url_empty: Union[Unset, bool] = UNSET,
    key_vault_url_ic: Union[Unset, List[str]] = UNSET,
    key_vault_url_ie: Union[Unset, List[str]] = UNSET,
    key_vault_url_iew: Union[Unset, List[str]] = UNSET,
    key_vault_url_isw: Union[Unset, List[str]] = UNSET,
    key_vault_url_n: Union[Unset, List[str]] = UNSET,
    key_vault_url_nic: Union[Unset, List[str]] = UNSET,
    key_vault_url_nie: Union[Unset, List[str]] = UNSET,
    key_vault_url_niew: Union[Unset, List[str]] = UNSET,
    key_vault_url_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    start_date: Union[Unset, List[datetime.date]] = UNSET,
    start_date_empty: Union[Unset, bool] = UNSET,
    start_date_gt: Union[Unset, List[datetime.date]] = UNSET,
    start_date_gte: Union[Unset, List[datetime.date]] = UNSET,
    start_date_lt: Union[Unset, List[datetime.date]] = UNSET,
    start_date_lte: Union[Unset, List[datetime.date]] = UNSET,
    start_date_n: Union[Unset, List[datetime.date]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Response[PaginatedCertificateAuthorityList]:
    """Mapping view set class

    Args:
        acme_url (Union[Unset, List[str]]):
        acme_url_empty (Union[Unset, bool]):
        acme_url_ic (Union[Unset, List[str]]):
        acme_url_ie (Union[Unset, List[str]]):
        acme_url_iew (Union[Unset, List[str]]):
        acme_url_isw (Union[Unset, List[str]]):
        acme_url_n (Union[Unset, List[str]]):
        acme_url_nic (Union[Unset, List[str]]):
        acme_url_nie (Union[Unset, List[str]]):
        acme_url_niew (Union[Unset, List[str]]):
        acme_url_nisw (Union[Unset, List[str]]):
        alt_name (Union[Unset, str]):
        ca_name (Union[Unset, List[str]]):
        ca_name_empty (Union[Unset, bool]):
        ca_name_ic (Union[Unset, List[str]]):
        ca_name_ie (Union[Unset, List[str]]):
        ca_name_iew (Union[Unset, List[str]]):
        ca_name_isw (Union[Unset, List[str]]):
        ca_name_n (Union[Unset, List[str]]):
        ca_name_nic (Union[Unset, List[str]]):
        ca_name_nie (Union[Unset, List[str]]):
        ca_name_niew (Union[Unset, List[str]]):
        ca_name_nisw (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        default_validity (Union[Unset, List[int]]):
        default_validity_empty (Union[Unset, bool]):
        default_validity_gt (Union[Unset, List[int]]):
        default_validity_gte (Union[Unset, List[int]]):
        default_validity_lt (Union[Unset, List[int]]):
        default_validity_lte (Union[Unset, List[int]]):
        default_validity_n (Union[Unset, List[int]]):
        end_date (Union[Unset, List[datetime.date]]):
        end_date_empty (Union[Unset, bool]):
        end_date_gt (Union[Unset, List[datetime.date]]):
        end_date_gte (Union[Unset, List[datetime.date]]):
        end_date_lt (Union[Unset, List[datetime.date]]):
        end_date_lte (Union[Unset, List[datetime.date]]):
        end_date_n (Union[Unset, List[datetime.date]]):
        key_vault_url (Union[Unset, List[str]]):
        key_vault_url_empty (Union[Unset, bool]):
        key_vault_url_ic (Union[Unset, List[str]]):
        key_vault_url_ie (Union[Unset, List[str]]):
        key_vault_url_iew (Union[Unset, List[str]]):
        key_vault_url_isw (Union[Unset, List[str]]):
        key_vault_url_n (Union[Unset, List[str]]):
        key_vault_url_nic (Union[Unset, List[str]]):
        key_vault_url_nie (Union[Unset, List[str]]):
        key_vault_url_niew (Union[Unset, List[str]]):
        key_vault_url_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        start_date (Union[Unset, List[datetime.date]]):
        start_date_empty (Union[Unset, bool]):
        start_date_gt (Union[Unset, List[datetime.date]]):
        start_date_gte (Union[Unset, List[datetime.date]]):
        start_date_lt (Union[Unset, List[datetime.date]]):
        start_date_lte (Union[Unset, List[datetime.date]]):
        start_date_n (Union[Unset, List[datetime.date]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCertificateAuthorityList]
    """

    kwargs = _get_kwargs(
        acme_url=acme_url,
        acme_url_empty=acme_url_empty,
        acme_url_ic=acme_url_ic,
        acme_url_ie=acme_url_ie,
        acme_url_iew=acme_url_iew,
        acme_url_isw=acme_url_isw,
        acme_url_n=acme_url_n,
        acme_url_nic=acme_url_nic,
        acme_url_nie=acme_url_nie,
        acme_url_niew=acme_url_niew,
        acme_url_nisw=acme_url_nisw,
        alt_name=alt_name,
        ca_name=ca_name,
        ca_name_empty=ca_name_empty,
        ca_name_ic=ca_name_ic,
        ca_name_ie=ca_name_ie,
        ca_name_iew=ca_name_iew,
        ca_name_isw=ca_name_isw,
        ca_name_n=ca_name_n,
        ca_name_nic=ca_name_nic,
        ca_name_nie=ca_name_nie,
        ca_name_niew=ca_name_niew,
        ca_name_nisw=ca_name_nisw,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        default_validity=default_validity,
        default_validity_empty=default_validity_empty,
        default_validity_gt=default_validity_gt,
        default_validity_gte=default_validity_gte,
        default_validity_lt=default_validity_lt,
        default_validity_lte=default_validity_lte,
        default_validity_n=default_validity_n,
        end_date=end_date,
        end_date_empty=end_date_empty,
        end_date_gt=end_date_gt,
        end_date_gte=end_date_gte,
        end_date_lt=end_date_lt,
        end_date_lte=end_date_lte,
        end_date_n=end_date_n,
        key_vault_url=key_vault_url,
        key_vault_url_empty=key_vault_url_empty,
        key_vault_url_ic=key_vault_url_ic,
        key_vault_url_ie=key_vault_url_ie,
        key_vault_url_iew=key_vault_url_iew,
        key_vault_url_isw=key_vault_url_isw,
        key_vault_url_n=key_vault_url_n,
        key_vault_url_nic=key_vault_url_nic,
        key_vault_url_nie=key_vault_url_nie,
        key_vault_url_niew=key_vault_url_niew,
        key_vault_url_nisw=key_vault_url_nisw,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        start_date=start_date,
        start_date_empty=start_date_empty,
        start_date_gt=start_date_gt,
        start_date_gte=start_date_gte,
        start_date_lt=start_date_lt,
        start_date_lte=start_date_lte,
        start_date_n=start_date_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    acme_url: Union[Unset, List[str]] = UNSET,
    acme_url_empty: Union[Unset, bool] = UNSET,
    acme_url_ic: Union[Unset, List[str]] = UNSET,
    acme_url_ie: Union[Unset, List[str]] = UNSET,
    acme_url_iew: Union[Unset, List[str]] = UNSET,
    acme_url_isw: Union[Unset, List[str]] = UNSET,
    acme_url_n: Union[Unset, List[str]] = UNSET,
    acme_url_nic: Union[Unset, List[str]] = UNSET,
    acme_url_nie: Union[Unset, List[str]] = UNSET,
    acme_url_niew: Union[Unset, List[str]] = UNSET,
    acme_url_nisw: Union[Unset, List[str]] = UNSET,
    alt_name: Union[Unset, str] = UNSET,
    ca_name: Union[Unset, List[str]] = UNSET,
    ca_name_empty: Union[Unset, bool] = UNSET,
    ca_name_ic: Union[Unset, List[str]] = UNSET,
    ca_name_ie: Union[Unset, List[str]] = UNSET,
    ca_name_iew: Union[Unset, List[str]] = UNSET,
    ca_name_isw: Union[Unset, List[str]] = UNSET,
    ca_name_n: Union[Unset, List[str]] = UNSET,
    ca_name_nic: Union[Unset, List[str]] = UNSET,
    ca_name_nie: Union[Unset, List[str]] = UNSET,
    ca_name_niew: Union[Unset, List[str]] = UNSET,
    ca_name_nisw: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    default_validity: Union[Unset, List[int]] = UNSET,
    default_validity_empty: Union[Unset, bool] = UNSET,
    default_validity_gt: Union[Unset, List[int]] = UNSET,
    default_validity_gte: Union[Unset, List[int]] = UNSET,
    default_validity_lt: Union[Unset, List[int]] = UNSET,
    default_validity_lte: Union[Unset, List[int]] = UNSET,
    default_validity_n: Union[Unset, List[int]] = UNSET,
    end_date: Union[Unset, List[datetime.date]] = UNSET,
    end_date_empty: Union[Unset, bool] = UNSET,
    end_date_gt: Union[Unset, List[datetime.date]] = UNSET,
    end_date_gte: Union[Unset, List[datetime.date]] = UNSET,
    end_date_lt: Union[Unset, List[datetime.date]] = UNSET,
    end_date_lte: Union[Unset, List[datetime.date]] = UNSET,
    end_date_n: Union[Unset, List[datetime.date]] = UNSET,
    key_vault_url: Union[Unset, List[str]] = UNSET,
    key_vault_url_empty: Union[Unset, bool] = UNSET,
    key_vault_url_ic: Union[Unset, List[str]] = UNSET,
    key_vault_url_ie: Union[Unset, List[str]] = UNSET,
    key_vault_url_iew: Union[Unset, List[str]] = UNSET,
    key_vault_url_isw: Union[Unset, List[str]] = UNSET,
    key_vault_url_n: Union[Unset, List[str]] = UNSET,
    key_vault_url_nic: Union[Unset, List[str]] = UNSET,
    key_vault_url_nie: Union[Unset, List[str]] = UNSET,
    key_vault_url_niew: Union[Unset, List[str]] = UNSET,
    key_vault_url_nisw: Union[Unset, List[str]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    start_date: Union[Unset, List[datetime.date]] = UNSET,
    start_date_empty: Union[Unset, bool] = UNSET,
    start_date_gt: Union[Unset, List[datetime.date]] = UNSET,
    start_date_gte: Union[Unset, List[datetime.date]] = UNSET,
    start_date_lt: Union[Unset, List[datetime.date]] = UNSET,
    start_date_lte: Union[Unset, List[datetime.date]] = UNSET,
    start_date_n: Union[Unset, List[datetime.date]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Optional[PaginatedCertificateAuthorityList]:
    """Mapping view set class

    Args:
        acme_url (Union[Unset, List[str]]):
        acme_url_empty (Union[Unset, bool]):
        acme_url_ic (Union[Unset, List[str]]):
        acme_url_ie (Union[Unset, List[str]]):
        acme_url_iew (Union[Unset, List[str]]):
        acme_url_isw (Union[Unset, List[str]]):
        acme_url_n (Union[Unset, List[str]]):
        acme_url_nic (Union[Unset, List[str]]):
        acme_url_nie (Union[Unset, List[str]]):
        acme_url_niew (Union[Unset, List[str]]):
        acme_url_nisw (Union[Unset, List[str]]):
        alt_name (Union[Unset, str]):
        ca_name (Union[Unset, List[str]]):
        ca_name_empty (Union[Unset, bool]):
        ca_name_ic (Union[Unset, List[str]]):
        ca_name_ie (Union[Unset, List[str]]):
        ca_name_iew (Union[Unset, List[str]]):
        ca_name_isw (Union[Unset, List[str]]):
        ca_name_n (Union[Unset, List[str]]):
        ca_name_nic (Union[Unset, List[str]]):
        ca_name_nie (Union[Unset, List[str]]):
        ca_name_niew (Union[Unset, List[str]]):
        ca_name_nisw (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        default_validity (Union[Unset, List[int]]):
        default_validity_empty (Union[Unset, bool]):
        default_validity_gt (Union[Unset, List[int]]):
        default_validity_gte (Union[Unset, List[int]]):
        default_validity_lt (Union[Unset, List[int]]):
        default_validity_lte (Union[Unset, List[int]]):
        default_validity_n (Union[Unset, List[int]]):
        end_date (Union[Unset, List[datetime.date]]):
        end_date_empty (Union[Unset, bool]):
        end_date_gt (Union[Unset, List[datetime.date]]):
        end_date_gte (Union[Unset, List[datetime.date]]):
        end_date_lt (Union[Unset, List[datetime.date]]):
        end_date_lte (Union[Unset, List[datetime.date]]):
        end_date_n (Union[Unset, List[datetime.date]]):
        key_vault_url (Union[Unset, List[str]]):
        key_vault_url_empty (Union[Unset, bool]):
        key_vault_url_ic (Union[Unset, List[str]]):
        key_vault_url_ie (Union[Unset, List[str]]):
        key_vault_url_iew (Union[Unset, List[str]]):
        key_vault_url_isw (Union[Unset, List[str]]):
        key_vault_url_n (Union[Unset, List[str]]):
        key_vault_url_nic (Union[Unset, List[str]]):
        key_vault_url_nie (Union[Unset, List[str]]):
        key_vault_url_niew (Union[Unset, List[str]]):
        key_vault_url_nisw (Union[Unset, List[str]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        start_date (Union[Unset, List[datetime.date]]):
        start_date_empty (Union[Unset, bool]):
        start_date_gt (Union[Unset, List[datetime.date]]):
        start_date_gte (Union[Unset, List[datetime.date]]):
        start_date_lt (Union[Unset, List[datetime.date]]):
        start_date_lte (Union[Unset, List[datetime.date]]):
        start_date_n (Union[Unset, List[datetime.date]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCertificateAuthorityList
    """

    return (
        await asyncio_detailed(
            client=client,
            acme_url=acme_url,
            acme_url_empty=acme_url_empty,
            acme_url_ic=acme_url_ic,
            acme_url_ie=acme_url_ie,
            acme_url_iew=acme_url_iew,
            acme_url_isw=acme_url_isw,
            acme_url_n=acme_url_n,
            acme_url_nic=acme_url_nic,
            acme_url_nie=acme_url_nie,
            acme_url_niew=acme_url_niew,
            acme_url_nisw=acme_url_nisw,
            alt_name=alt_name,
            ca_name=ca_name,
            ca_name_empty=ca_name_empty,
            ca_name_ic=ca_name_ic,
            ca_name_ie=ca_name_ie,
            ca_name_iew=ca_name_iew,
            ca_name_isw=ca_name_isw,
            ca_name_n=ca_name_n,
            ca_name_nic=ca_name_nic,
            ca_name_nie=ca_name_nie,
            ca_name_niew=ca_name_niew,
            ca_name_nisw=ca_name_nisw,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            default_validity=default_validity,
            default_validity_empty=default_validity_empty,
            default_validity_gt=default_validity_gt,
            default_validity_gte=default_validity_gte,
            default_validity_lt=default_validity_lt,
            default_validity_lte=default_validity_lte,
            default_validity_n=default_validity_n,
            end_date=end_date,
            end_date_empty=end_date_empty,
            end_date_gt=end_date_gt,
            end_date_gte=end_date_gte,
            end_date_lt=end_date_lt,
            end_date_lte=end_date_lte,
            end_date_n=end_date_n,
            key_vault_url=key_vault_url,
            key_vault_url_empty=key_vault_url_empty,
            key_vault_url_ic=key_vault_url_ic,
            key_vault_url_ie=key_vault_url_ie,
            key_vault_url_iew=key_vault_url_iew,
            key_vault_url_isw=key_vault_url_isw,
            key_vault_url_n=key_vault_url_n,
            key_vault_url_nic=key_vault_url_nic,
            key_vault_url_nie=key_vault_url_nie,
            key_vault_url_niew=key_vault_url_niew,
            key_vault_url_nisw=key_vault_url_nisw,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            limit=limit,
            modified_by_request=modified_by_request,
            offset=offset,
            ordering=ordering,
            q=q,
            start_date=start_date,
            start_date_empty=start_date_empty,
            start_date_gt=start_date_gt,
            start_date_gte=start_date_gte,
            start_date_lt=start_date_lt,
            start_date_lte=start_date_lte,
            start_date_n=start_date_n,
            tag=tag,
            tag_n=tag_n,
            updated_by_request=updated_by_request,
        )
    ).parsed
