import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_host_list import PaginatedHostList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    agent_version: Union[Unset, List[str]] = UNSET,
    agent_version_empty: Union[Unset, bool] = UNSET,
    agent_version_ic: Union[Unset, List[str]] = UNSET,
    agent_version_ie: Union[Unset, List[str]] = UNSET,
    agent_version_iew: Union[Unset, List[str]] = UNSET,
    agent_version_isw: Union[Unset, List[str]] = UNSET,
    agent_version_n: Union[Unset, List[str]] = UNSET,
    agent_version_nic: Union[Unset, List[str]] = UNSET,
    agent_version_nie: Union[Unset, List[str]] = UNSET,
    agent_version_niew: Union[Unset, List[str]] = UNSET,
    agent_version_nisw: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    docker_api_version: Union[Unset, List[str]] = UNSET,
    docker_api_version_empty: Union[Unset, bool] = UNSET,
    docker_api_version_ic: Union[Unset, List[str]] = UNSET,
    docker_api_version_ie: Union[Unset, List[str]] = UNSET,
    docker_api_version_iew: Union[Unset, List[str]] = UNSET,
    docker_api_version_isw: Union[Unset, List[str]] = UNSET,
    docker_api_version_n: Union[Unset, List[str]] = UNSET,
    docker_api_version_nic: Union[Unset, List[str]] = UNSET,
    docker_api_version_nie: Union[Unset, List[str]] = UNSET,
    docker_api_version_niew: Union[Unset, List[str]] = UNSET,
    docker_api_version_nisw: Union[Unset, List[str]] = UNSET,
    endpoint: Union[Unset, List[str]] = UNSET,
    endpoint_empty: Union[Unset, bool] = UNSET,
    endpoint_ic: Union[Unset, List[str]] = UNSET,
    endpoint_ie: Union[Unset, List[str]] = UNSET,
    endpoint_iew: Union[Unset, List[str]] = UNSET,
    endpoint_isw: Union[Unset, List[str]] = UNSET,
    endpoint_n: Union[Unset, List[str]] = UNSET,
    endpoint_nic: Union[Unset, List[str]] = UNSET,
    endpoint_nie: Union[Unset, List[str]] = UNSET,
    endpoint_niew: Union[Unset, List[str]] = UNSET,
    endpoint_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, str] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    state: Union[Unset, str] = UNSET,
    state_n: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_agent_version: Union[Unset, List[str]] = UNSET
    if not isinstance(agent_version, Unset):
        json_agent_version = agent_version

    params["agent_version"] = json_agent_version

    params["agent_version__empty"] = agent_version_empty

    json_agent_version_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(agent_version_ic, Unset):
        json_agent_version_ic = agent_version_ic

    params["agent_version__ic"] = json_agent_version_ic

    json_agent_version_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(agent_version_ie, Unset):
        json_agent_version_ie = agent_version_ie

    params["agent_version__ie"] = json_agent_version_ie

    json_agent_version_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(agent_version_iew, Unset):
        json_agent_version_iew = agent_version_iew

    params["agent_version__iew"] = json_agent_version_iew

    json_agent_version_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(agent_version_isw, Unset):
        json_agent_version_isw = agent_version_isw

    params["agent_version__isw"] = json_agent_version_isw

    json_agent_version_n: Union[Unset, List[str]] = UNSET
    if not isinstance(agent_version_n, Unset):
        json_agent_version_n = agent_version_n

    params["agent_version__n"] = json_agent_version_n

    json_agent_version_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(agent_version_nic, Unset):
        json_agent_version_nic = agent_version_nic

    params["agent_version__nic"] = json_agent_version_nic

    json_agent_version_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(agent_version_nie, Unset):
        json_agent_version_nie = agent_version_nie

    params["agent_version__nie"] = json_agent_version_nie

    json_agent_version_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(agent_version_niew, Unset):
        json_agent_version_niew = agent_version_niew

    params["agent_version__niew"] = json_agent_version_niew

    json_agent_version_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(agent_version_nisw, Unset):
        json_agent_version_nisw = agent_version_nisw

    params["agent_version__nisw"] = json_agent_version_nisw

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    json_docker_api_version: Union[Unset, List[str]] = UNSET
    if not isinstance(docker_api_version, Unset):
        json_docker_api_version = docker_api_version

    params["docker_api_version"] = json_docker_api_version

    params["docker_api_version__empty"] = docker_api_version_empty

    json_docker_api_version_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(docker_api_version_ic, Unset):
        json_docker_api_version_ic = docker_api_version_ic

    params["docker_api_version__ic"] = json_docker_api_version_ic

    json_docker_api_version_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(docker_api_version_ie, Unset):
        json_docker_api_version_ie = docker_api_version_ie

    params["docker_api_version__ie"] = json_docker_api_version_ie

    json_docker_api_version_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(docker_api_version_iew, Unset):
        json_docker_api_version_iew = docker_api_version_iew

    params["docker_api_version__iew"] = json_docker_api_version_iew

    json_docker_api_version_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(docker_api_version_isw, Unset):
        json_docker_api_version_isw = docker_api_version_isw

    params["docker_api_version__isw"] = json_docker_api_version_isw

    json_docker_api_version_n: Union[Unset, List[str]] = UNSET
    if not isinstance(docker_api_version_n, Unset):
        json_docker_api_version_n = docker_api_version_n

    params["docker_api_version__n"] = json_docker_api_version_n

    json_docker_api_version_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(docker_api_version_nic, Unset):
        json_docker_api_version_nic = docker_api_version_nic

    params["docker_api_version__nic"] = json_docker_api_version_nic

    json_docker_api_version_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(docker_api_version_nie, Unset):
        json_docker_api_version_nie = docker_api_version_nie

    params["docker_api_version__nie"] = json_docker_api_version_nie

    json_docker_api_version_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(docker_api_version_niew, Unset):
        json_docker_api_version_niew = docker_api_version_niew

    params["docker_api_version__niew"] = json_docker_api_version_niew

    json_docker_api_version_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(docker_api_version_nisw, Unset):
        json_docker_api_version_nisw = docker_api_version_nisw

    params["docker_api_version__nisw"] = json_docker_api_version_nisw

    json_endpoint: Union[Unset, List[str]] = UNSET
    if not isinstance(endpoint, Unset):
        json_endpoint = endpoint

    params["endpoint"] = json_endpoint

    params["endpoint__empty"] = endpoint_empty

    json_endpoint_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(endpoint_ic, Unset):
        json_endpoint_ic = endpoint_ic

    params["endpoint__ic"] = json_endpoint_ic

    json_endpoint_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(endpoint_ie, Unset):
        json_endpoint_ie = endpoint_ie

    params["endpoint__ie"] = json_endpoint_ie

    json_endpoint_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(endpoint_iew, Unset):
        json_endpoint_iew = endpoint_iew

    params["endpoint__iew"] = json_endpoint_iew

    json_endpoint_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(endpoint_isw, Unset):
        json_endpoint_isw = endpoint_isw

    params["endpoint__isw"] = json_endpoint_isw

    json_endpoint_n: Union[Unset, List[str]] = UNSET
    if not isinstance(endpoint_n, Unset):
        json_endpoint_n = endpoint_n

    params["endpoint__n"] = json_endpoint_n

    json_endpoint_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(endpoint_nic, Unset):
        json_endpoint_nic = endpoint_nic

    params["endpoint__nic"] = json_endpoint_nic

    json_endpoint_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(endpoint_nie, Unset):
        json_endpoint_nie = endpoint_nie

    params["endpoint__nie"] = json_endpoint_nie

    json_endpoint_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(endpoint_niew, Unset):
        json_endpoint_niew = endpoint_niew

    params["endpoint__niew"] = json_endpoint_niew

    json_endpoint_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(endpoint_nisw, Unset):
        json_endpoint_nisw = endpoint_nisw

    params["endpoint__nisw"] = json_endpoint_nisw

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    params["limit"] = limit

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    params["name"] = name

    params["offset"] = offset

    params["ordering"] = ordering

    params["q"] = q

    params["state"] = state

    params["state__n"] = state_n

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/plugins/docker/hosts/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedHostList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedHostList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedHostList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    agent_version: Union[Unset, List[str]] = UNSET,
    agent_version_empty: Union[Unset, bool] = UNSET,
    agent_version_ic: Union[Unset, List[str]] = UNSET,
    agent_version_ie: Union[Unset, List[str]] = UNSET,
    agent_version_iew: Union[Unset, List[str]] = UNSET,
    agent_version_isw: Union[Unset, List[str]] = UNSET,
    agent_version_n: Union[Unset, List[str]] = UNSET,
    agent_version_nic: Union[Unset, List[str]] = UNSET,
    agent_version_nie: Union[Unset, List[str]] = UNSET,
    agent_version_niew: Union[Unset, List[str]] = UNSET,
    agent_version_nisw: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    docker_api_version: Union[Unset, List[str]] = UNSET,
    docker_api_version_empty: Union[Unset, bool] = UNSET,
    docker_api_version_ic: Union[Unset, List[str]] = UNSET,
    docker_api_version_ie: Union[Unset, List[str]] = UNSET,
    docker_api_version_iew: Union[Unset, List[str]] = UNSET,
    docker_api_version_isw: Union[Unset, List[str]] = UNSET,
    docker_api_version_n: Union[Unset, List[str]] = UNSET,
    docker_api_version_nic: Union[Unset, List[str]] = UNSET,
    docker_api_version_nie: Union[Unset, List[str]] = UNSET,
    docker_api_version_niew: Union[Unset, List[str]] = UNSET,
    docker_api_version_nisw: Union[Unset, List[str]] = UNSET,
    endpoint: Union[Unset, List[str]] = UNSET,
    endpoint_empty: Union[Unset, bool] = UNSET,
    endpoint_ic: Union[Unset, List[str]] = UNSET,
    endpoint_ie: Union[Unset, List[str]] = UNSET,
    endpoint_iew: Union[Unset, List[str]] = UNSET,
    endpoint_isw: Union[Unset, List[str]] = UNSET,
    endpoint_n: Union[Unset, List[str]] = UNSET,
    endpoint_nic: Union[Unset, List[str]] = UNSET,
    endpoint_nie: Union[Unset, List[str]] = UNSET,
    endpoint_niew: Union[Unset, List[str]] = UNSET,
    endpoint_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, str] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    state: Union[Unset, str] = UNSET,
    state_n: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Response[PaginatedHostList]:
    """Host view set class

    Args:
        agent_version (Union[Unset, List[str]]):
        agent_version_empty (Union[Unset, bool]):
        agent_version_ic (Union[Unset, List[str]]):
        agent_version_ie (Union[Unset, List[str]]):
        agent_version_iew (Union[Unset, List[str]]):
        agent_version_isw (Union[Unset, List[str]]):
        agent_version_n (Union[Unset, List[str]]):
        agent_version_nic (Union[Unset, List[str]]):
        agent_version_nie (Union[Unset, List[str]]):
        agent_version_niew (Union[Unset, List[str]]):
        agent_version_nisw (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        docker_api_version (Union[Unset, List[str]]):
        docker_api_version_empty (Union[Unset, bool]):
        docker_api_version_ic (Union[Unset, List[str]]):
        docker_api_version_ie (Union[Unset, List[str]]):
        docker_api_version_iew (Union[Unset, List[str]]):
        docker_api_version_isw (Union[Unset, List[str]]):
        docker_api_version_n (Union[Unset, List[str]]):
        docker_api_version_nic (Union[Unset, List[str]]):
        docker_api_version_nie (Union[Unset, List[str]]):
        docker_api_version_niew (Union[Unset, List[str]]):
        docker_api_version_nisw (Union[Unset, List[str]]):
        endpoint (Union[Unset, List[str]]):
        endpoint_empty (Union[Unset, bool]):
        endpoint_ic (Union[Unset, List[str]]):
        endpoint_ie (Union[Unset, List[str]]):
        endpoint_iew (Union[Unset, List[str]]):
        endpoint_isw (Union[Unset, List[str]]):
        endpoint_n (Union[Unset, List[str]]):
        endpoint_nic (Union[Unset, List[str]]):
        endpoint_nie (Union[Unset, List[str]]):
        endpoint_niew (Union[Unset, List[str]]):
        endpoint_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, str]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        state (Union[Unset, str]):
        state_n (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedHostList]
    """

    kwargs = _get_kwargs(
        agent_version=agent_version,
        agent_version_empty=agent_version_empty,
        agent_version_ic=agent_version_ic,
        agent_version_ie=agent_version_ie,
        agent_version_iew=agent_version_iew,
        agent_version_isw=agent_version_isw,
        agent_version_n=agent_version_n,
        agent_version_nic=agent_version_nic,
        agent_version_nie=agent_version_nie,
        agent_version_niew=agent_version_niew,
        agent_version_nisw=agent_version_nisw,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        docker_api_version=docker_api_version,
        docker_api_version_empty=docker_api_version_empty,
        docker_api_version_ic=docker_api_version_ic,
        docker_api_version_ie=docker_api_version_ie,
        docker_api_version_iew=docker_api_version_iew,
        docker_api_version_isw=docker_api_version_isw,
        docker_api_version_n=docker_api_version_n,
        docker_api_version_nic=docker_api_version_nic,
        docker_api_version_nie=docker_api_version_nie,
        docker_api_version_niew=docker_api_version_niew,
        docker_api_version_nisw=docker_api_version_nisw,
        endpoint=endpoint,
        endpoint_empty=endpoint_empty,
        endpoint_ic=endpoint_ic,
        endpoint_ie=endpoint_ie,
        endpoint_iew=endpoint_iew,
        endpoint_isw=endpoint_isw,
        endpoint_n=endpoint_n,
        endpoint_nic=endpoint_nic,
        endpoint_nie=endpoint_nie,
        endpoint_niew=endpoint_niew,
        endpoint_nisw=endpoint_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        name=name,
        offset=offset,
        ordering=ordering,
        q=q,
        state=state,
        state_n=state_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    agent_version: Union[Unset, List[str]] = UNSET,
    agent_version_empty: Union[Unset, bool] = UNSET,
    agent_version_ic: Union[Unset, List[str]] = UNSET,
    agent_version_ie: Union[Unset, List[str]] = UNSET,
    agent_version_iew: Union[Unset, List[str]] = UNSET,
    agent_version_isw: Union[Unset, List[str]] = UNSET,
    agent_version_n: Union[Unset, List[str]] = UNSET,
    agent_version_nic: Union[Unset, List[str]] = UNSET,
    agent_version_nie: Union[Unset, List[str]] = UNSET,
    agent_version_niew: Union[Unset, List[str]] = UNSET,
    agent_version_nisw: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    docker_api_version: Union[Unset, List[str]] = UNSET,
    docker_api_version_empty: Union[Unset, bool] = UNSET,
    docker_api_version_ic: Union[Unset, List[str]] = UNSET,
    docker_api_version_ie: Union[Unset, List[str]] = UNSET,
    docker_api_version_iew: Union[Unset, List[str]] = UNSET,
    docker_api_version_isw: Union[Unset, List[str]] = UNSET,
    docker_api_version_n: Union[Unset, List[str]] = UNSET,
    docker_api_version_nic: Union[Unset, List[str]] = UNSET,
    docker_api_version_nie: Union[Unset, List[str]] = UNSET,
    docker_api_version_niew: Union[Unset, List[str]] = UNSET,
    docker_api_version_nisw: Union[Unset, List[str]] = UNSET,
    endpoint: Union[Unset, List[str]] = UNSET,
    endpoint_empty: Union[Unset, bool] = UNSET,
    endpoint_ic: Union[Unset, List[str]] = UNSET,
    endpoint_ie: Union[Unset, List[str]] = UNSET,
    endpoint_iew: Union[Unset, List[str]] = UNSET,
    endpoint_isw: Union[Unset, List[str]] = UNSET,
    endpoint_n: Union[Unset, List[str]] = UNSET,
    endpoint_nic: Union[Unset, List[str]] = UNSET,
    endpoint_nie: Union[Unset, List[str]] = UNSET,
    endpoint_niew: Union[Unset, List[str]] = UNSET,
    endpoint_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, str] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    state: Union[Unset, str] = UNSET,
    state_n: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Optional[PaginatedHostList]:
    """Host view set class

    Args:
        agent_version (Union[Unset, List[str]]):
        agent_version_empty (Union[Unset, bool]):
        agent_version_ic (Union[Unset, List[str]]):
        agent_version_ie (Union[Unset, List[str]]):
        agent_version_iew (Union[Unset, List[str]]):
        agent_version_isw (Union[Unset, List[str]]):
        agent_version_n (Union[Unset, List[str]]):
        agent_version_nic (Union[Unset, List[str]]):
        agent_version_nie (Union[Unset, List[str]]):
        agent_version_niew (Union[Unset, List[str]]):
        agent_version_nisw (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        docker_api_version (Union[Unset, List[str]]):
        docker_api_version_empty (Union[Unset, bool]):
        docker_api_version_ic (Union[Unset, List[str]]):
        docker_api_version_ie (Union[Unset, List[str]]):
        docker_api_version_iew (Union[Unset, List[str]]):
        docker_api_version_isw (Union[Unset, List[str]]):
        docker_api_version_n (Union[Unset, List[str]]):
        docker_api_version_nic (Union[Unset, List[str]]):
        docker_api_version_nie (Union[Unset, List[str]]):
        docker_api_version_niew (Union[Unset, List[str]]):
        docker_api_version_nisw (Union[Unset, List[str]]):
        endpoint (Union[Unset, List[str]]):
        endpoint_empty (Union[Unset, bool]):
        endpoint_ic (Union[Unset, List[str]]):
        endpoint_ie (Union[Unset, List[str]]):
        endpoint_iew (Union[Unset, List[str]]):
        endpoint_isw (Union[Unset, List[str]]):
        endpoint_n (Union[Unset, List[str]]):
        endpoint_nic (Union[Unset, List[str]]):
        endpoint_nie (Union[Unset, List[str]]):
        endpoint_niew (Union[Unset, List[str]]):
        endpoint_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, str]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        state (Union[Unset, str]):
        state_n (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedHostList
    """

    return sync_detailed(
        client=client,
        agent_version=agent_version,
        agent_version_empty=agent_version_empty,
        agent_version_ic=agent_version_ic,
        agent_version_ie=agent_version_ie,
        agent_version_iew=agent_version_iew,
        agent_version_isw=agent_version_isw,
        agent_version_n=agent_version_n,
        agent_version_nic=agent_version_nic,
        agent_version_nie=agent_version_nie,
        agent_version_niew=agent_version_niew,
        agent_version_nisw=agent_version_nisw,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        docker_api_version=docker_api_version,
        docker_api_version_empty=docker_api_version_empty,
        docker_api_version_ic=docker_api_version_ic,
        docker_api_version_ie=docker_api_version_ie,
        docker_api_version_iew=docker_api_version_iew,
        docker_api_version_isw=docker_api_version_isw,
        docker_api_version_n=docker_api_version_n,
        docker_api_version_nic=docker_api_version_nic,
        docker_api_version_nie=docker_api_version_nie,
        docker_api_version_niew=docker_api_version_niew,
        docker_api_version_nisw=docker_api_version_nisw,
        endpoint=endpoint,
        endpoint_empty=endpoint_empty,
        endpoint_ic=endpoint_ic,
        endpoint_ie=endpoint_ie,
        endpoint_iew=endpoint_iew,
        endpoint_isw=endpoint_isw,
        endpoint_n=endpoint_n,
        endpoint_nic=endpoint_nic,
        endpoint_nie=endpoint_nie,
        endpoint_niew=endpoint_niew,
        endpoint_nisw=endpoint_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        name=name,
        offset=offset,
        ordering=ordering,
        q=q,
        state=state,
        state_n=state_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    agent_version: Union[Unset, List[str]] = UNSET,
    agent_version_empty: Union[Unset, bool] = UNSET,
    agent_version_ic: Union[Unset, List[str]] = UNSET,
    agent_version_ie: Union[Unset, List[str]] = UNSET,
    agent_version_iew: Union[Unset, List[str]] = UNSET,
    agent_version_isw: Union[Unset, List[str]] = UNSET,
    agent_version_n: Union[Unset, List[str]] = UNSET,
    agent_version_nic: Union[Unset, List[str]] = UNSET,
    agent_version_nie: Union[Unset, List[str]] = UNSET,
    agent_version_niew: Union[Unset, List[str]] = UNSET,
    agent_version_nisw: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    docker_api_version: Union[Unset, List[str]] = UNSET,
    docker_api_version_empty: Union[Unset, bool] = UNSET,
    docker_api_version_ic: Union[Unset, List[str]] = UNSET,
    docker_api_version_ie: Union[Unset, List[str]] = UNSET,
    docker_api_version_iew: Union[Unset, List[str]] = UNSET,
    docker_api_version_isw: Union[Unset, List[str]] = UNSET,
    docker_api_version_n: Union[Unset, List[str]] = UNSET,
    docker_api_version_nic: Union[Unset, List[str]] = UNSET,
    docker_api_version_nie: Union[Unset, List[str]] = UNSET,
    docker_api_version_niew: Union[Unset, List[str]] = UNSET,
    docker_api_version_nisw: Union[Unset, List[str]] = UNSET,
    endpoint: Union[Unset, List[str]] = UNSET,
    endpoint_empty: Union[Unset, bool] = UNSET,
    endpoint_ic: Union[Unset, List[str]] = UNSET,
    endpoint_ie: Union[Unset, List[str]] = UNSET,
    endpoint_iew: Union[Unset, List[str]] = UNSET,
    endpoint_isw: Union[Unset, List[str]] = UNSET,
    endpoint_n: Union[Unset, List[str]] = UNSET,
    endpoint_nic: Union[Unset, List[str]] = UNSET,
    endpoint_nie: Union[Unset, List[str]] = UNSET,
    endpoint_niew: Union[Unset, List[str]] = UNSET,
    endpoint_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, str] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    state: Union[Unset, str] = UNSET,
    state_n: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Response[PaginatedHostList]:
    """Host view set class

    Args:
        agent_version (Union[Unset, List[str]]):
        agent_version_empty (Union[Unset, bool]):
        agent_version_ic (Union[Unset, List[str]]):
        agent_version_ie (Union[Unset, List[str]]):
        agent_version_iew (Union[Unset, List[str]]):
        agent_version_isw (Union[Unset, List[str]]):
        agent_version_n (Union[Unset, List[str]]):
        agent_version_nic (Union[Unset, List[str]]):
        agent_version_nie (Union[Unset, List[str]]):
        agent_version_niew (Union[Unset, List[str]]):
        agent_version_nisw (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        docker_api_version (Union[Unset, List[str]]):
        docker_api_version_empty (Union[Unset, bool]):
        docker_api_version_ic (Union[Unset, List[str]]):
        docker_api_version_ie (Union[Unset, List[str]]):
        docker_api_version_iew (Union[Unset, List[str]]):
        docker_api_version_isw (Union[Unset, List[str]]):
        docker_api_version_n (Union[Unset, List[str]]):
        docker_api_version_nic (Union[Unset, List[str]]):
        docker_api_version_nie (Union[Unset, List[str]]):
        docker_api_version_niew (Union[Unset, List[str]]):
        docker_api_version_nisw (Union[Unset, List[str]]):
        endpoint (Union[Unset, List[str]]):
        endpoint_empty (Union[Unset, bool]):
        endpoint_ic (Union[Unset, List[str]]):
        endpoint_ie (Union[Unset, List[str]]):
        endpoint_iew (Union[Unset, List[str]]):
        endpoint_isw (Union[Unset, List[str]]):
        endpoint_n (Union[Unset, List[str]]):
        endpoint_nic (Union[Unset, List[str]]):
        endpoint_nie (Union[Unset, List[str]]):
        endpoint_niew (Union[Unset, List[str]]):
        endpoint_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, str]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        state (Union[Unset, str]):
        state_n (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedHostList]
    """

    kwargs = _get_kwargs(
        agent_version=agent_version,
        agent_version_empty=agent_version_empty,
        agent_version_ic=agent_version_ic,
        agent_version_ie=agent_version_ie,
        agent_version_iew=agent_version_iew,
        agent_version_isw=agent_version_isw,
        agent_version_n=agent_version_n,
        agent_version_nic=agent_version_nic,
        agent_version_nie=agent_version_nie,
        agent_version_niew=agent_version_niew,
        agent_version_nisw=agent_version_nisw,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        docker_api_version=docker_api_version,
        docker_api_version_empty=docker_api_version_empty,
        docker_api_version_ic=docker_api_version_ic,
        docker_api_version_ie=docker_api_version_ie,
        docker_api_version_iew=docker_api_version_iew,
        docker_api_version_isw=docker_api_version_isw,
        docker_api_version_n=docker_api_version_n,
        docker_api_version_nic=docker_api_version_nic,
        docker_api_version_nie=docker_api_version_nie,
        docker_api_version_niew=docker_api_version_niew,
        docker_api_version_nisw=docker_api_version_nisw,
        endpoint=endpoint,
        endpoint_empty=endpoint_empty,
        endpoint_ic=endpoint_ic,
        endpoint_ie=endpoint_ie,
        endpoint_iew=endpoint_iew,
        endpoint_isw=endpoint_isw,
        endpoint_n=endpoint_n,
        endpoint_nic=endpoint_nic,
        endpoint_nie=endpoint_nie,
        endpoint_niew=endpoint_niew,
        endpoint_nisw=endpoint_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        name=name,
        offset=offset,
        ordering=ordering,
        q=q,
        state=state,
        state_n=state_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    agent_version: Union[Unset, List[str]] = UNSET,
    agent_version_empty: Union[Unset, bool] = UNSET,
    agent_version_ic: Union[Unset, List[str]] = UNSET,
    agent_version_ie: Union[Unset, List[str]] = UNSET,
    agent_version_iew: Union[Unset, List[str]] = UNSET,
    agent_version_isw: Union[Unset, List[str]] = UNSET,
    agent_version_n: Union[Unset, List[str]] = UNSET,
    agent_version_nic: Union[Unset, List[str]] = UNSET,
    agent_version_nie: Union[Unset, List[str]] = UNSET,
    agent_version_niew: Union[Unset, List[str]] = UNSET,
    agent_version_nisw: Union[Unset, List[str]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    docker_api_version: Union[Unset, List[str]] = UNSET,
    docker_api_version_empty: Union[Unset, bool] = UNSET,
    docker_api_version_ic: Union[Unset, List[str]] = UNSET,
    docker_api_version_ie: Union[Unset, List[str]] = UNSET,
    docker_api_version_iew: Union[Unset, List[str]] = UNSET,
    docker_api_version_isw: Union[Unset, List[str]] = UNSET,
    docker_api_version_n: Union[Unset, List[str]] = UNSET,
    docker_api_version_nic: Union[Unset, List[str]] = UNSET,
    docker_api_version_nie: Union[Unset, List[str]] = UNSET,
    docker_api_version_niew: Union[Unset, List[str]] = UNSET,
    docker_api_version_nisw: Union[Unset, List[str]] = UNSET,
    endpoint: Union[Unset, List[str]] = UNSET,
    endpoint_empty: Union[Unset, bool] = UNSET,
    endpoint_ic: Union[Unset, List[str]] = UNSET,
    endpoint_ie: Union[Unset, List[str]] = UNSET,
    endpoint_iew: Union[Unset, List[str]] = UNSET,
    endpoint_isw: Union[Unset, List[str]] = UNSET,
    endpoint_n: Union[Unset, List[str]] = UNSET,
    endpoint_nic: Union[Unset, List[str]] = UNSET,
    endpoint_nie: Union[Unset, List[str]] = UNSET,
    endpoint_niew: Union[Unset, List[str]] = UNSET,
    endpoint_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, str] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    state: Union[Unset, str] = UNSET,
    state_n: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Optional[PaginatedHostList]:
    """Host view set class

    Args:
        agent_version (Union[Unset, List[str]]):
        agent_version_empty (Union[Unset, bool]):
        agent_version_ic (Union[Unset, List[str]]):
        agent_version_ie (Union[Unset, List[str]]):
        agent_version_iew (Union[Unset, List[str]]):
        agent_version_isw (Union[Unset, List[str]]):
        agent_version_n (Union[Unset, List[str]]):
        agent_version_nic (Union[Unset, List[str]]):
        agent_version_nie (Union[Unset, List[str]]):
        agent_version_niew (Union[Unset, List[str]]):
        agent_version_nisw (Union[Unset, List[str]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        docker_api_version (Union[Unset, List[str]]):
        docker_api_version_empty (Union[Unset, bool]):
        docker_api_version_ic (Union[Unset, List[str]]):
        docker_api_version_ie (Union[Unset, List[str]]):
        docker_api_version_iew (Union[Unset, List[str]]):
        docker_api_version_isw (Union[Unset, List[str]]):
        docker_api_version_n (Union[Unset, List[str]]):
        docker_api_version_nic (Union[Unset, List[str]]):
        docker_api_version_nie (Union[Unset, List[str]]):
        docker_api_version_niew (Union[Unset, List[str]]):
        docker_api_version_nisw (Union[Unset, List[str]]):
        endpoint (Union[Unset, List[str]]):
        endpoint_empty (Union[Unset, bool]):
        endpoint_ic (Union[Unset, List[str]]):
        endpoint_ie (Union[Unset, List[str]]):
        endpoint_iew (Union[Unset, List[str]]):
        endpoint_isw (Union[Unset, List[str]]):
        endpoint_n (Union[Unset, List[str]]):
        endpoint_nic (Union[Unset, List[str]]):
        endpoint_nie (Union[Unset, List[str]]):
        endpoint_niew (Union[Unset, List[str]]):
        endpoint_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, str]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        state (Union[Unset, str]):
        state_n (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedHostList
    """

    return (
        await asyncio_detailed(
            client=client,
            agent_version=agent_version,
            agent_version_empty=agent_version_empty,
            agent_version_ic=agent_version_ic,
            agent_version_ie=agent_version_ie,
            agent_version_iew=agent_version_iew,
            agent_version_isw=agent_version_isw,
            agent_version_n=agent_version_n,
            agent_version_nic=agent_version_nic,
            agent_version_nie=agent_version_nie,
            agent_version_niew=agent_version_niew,
            agent_version_nisw=agent_version_nisw,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            docker_api_version=docker_api_version,
            docker_api_version_empty=docker_api_version_empty,
            docker_api_version_ic=docker_api_version_ic,
            docker_api_version_ie=docker_api_version_ie,
            docker_api_version_iew=docker_api_version_iew,
            docker_api_version_isw=docker_api_version_isw,
            docker_api_version_n=docker_api_version_n,
            docker_api_version_nic=docker_api_version_nic,
            docker_api_version_nie=docker_api_version_nie,
            docker_api_version_niew=docker_api_version_niew,
            docker_api_version_nisw=docker_api_version_nisw,
            endpoint=endpoint,
            endpoint_empty=endpoint_empty,
            endpoint_ic=endpoint_ic,
            endpoint_ie=endpoint_ie,
            endpoint_iew=endpoint_iew,
            endpoint_isw=endpoint_isw,
            endpoint_n=endpoint_n,
            endpoint_nic=endpoint_nic,
            endpoint_nie=endpoint_nie,
            endpoint_niew=endpoint_niew,
            endpoint_nisw=endpoint_nisw,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            limit=limit,
            modified_by_request=modified_by_request,
            name=name,
            offset=offset,
            ordering=ordering,
            q=q,
            state=state,
            state_n=state_n,
            tag=tag,
            tag_n=tag_n,
            updated_by_request=updated_by_request,
        )
    ).parsed
