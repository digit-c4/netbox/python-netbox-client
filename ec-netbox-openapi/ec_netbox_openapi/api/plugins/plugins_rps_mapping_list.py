import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_mapping_list import PaginatedMappingList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    comment: Union[Unset, List[str]] = UNSET,
    comment_empty: Union[Unset, bool] = UNSET,
    comment_ic: Union[Unset, List[str]] = UNSET,
    comment_ie: Union[Unset, List[str]] = UNSET,
    comment_iew: Union[Unset, List[str]] = UNSET,
    comment_isw: Union[Unset, List[str]] = UNSET,
    comment_n: Union[Unset, List[str]] = UNSET,
    comment_nic: Union[Unset, List[str]] = UNSET,
    comment_nie: Union[Unset, List[str]] = UNSET,
    comment_niew: Union[Unset, List[str]] = UNSET,
    comment_nisw: Union[Unset, List[str]] = UNSET,
    authentication: Union[Unset, str] = UNSET,
    authentication_n: Union[Unset, str] = UNSET,
    client_max_body_size: Union[Unset, int] = UNSET,
    client_max_body_size_n: Union[Unset, int] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    extra_protocols: Union[Unset, str] = UNSET,
    gzip_proxied: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    keepalive_requests: Union[Unset, List[int]] = UNSET,
    keepalive_requests_empty: Union[Unset, bool] = UNSET,
    keepalive_requests_gt: Union[Unset, List[int]] = UNSET,
    keepalive_requests_gte: Union[Unset, List[int]] = UNSET,
    keepalive_requests_lt: Union[Unset, List[int]] = UNSET,
    keepalive_requests_lte: Union[Unset, List[int]] = UNSET,
    keepalive_requests_n: Union[Unset, List[int]] = UNSET,
    keepalive_timeout: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_empty: Union[Unset, bool] = UNSET,
    keepalive_timeout_gt: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_gte: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_lt: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_lte: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    proxy_buffer_size: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_empty: Union[Unset, bool] = UNSET,
    proxy_buffer_size_gt: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_gte: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_lt: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_lte: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_n: Union[Unset, List[int]] = UNSET,
    proxy_cache: Union[Unset, bool] = UNSET,
    proxy_read_timeout: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_empty: Union[Unset, bool] = UNSET,
    proxy_read_timeout_gt: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_gte: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_lt: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_lte: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_n: Union[Unset, List[int]] = UNSET,
    q: Union[Unset, str] = UNSET,
    sorry_page: Union[Unset, List[str]] = UNSET,
    sorry_page_empty: Union[Unset, bool] = UNSET,
    sorry_page_ic: Union[Unset, List[str]] = UNSET,
    sorry_page_ie: Union[Unset, List[str]] = UNSET,
    sorry_page_iew: Union[Unset, List[str]] = UNSET,
    sorry_page_isw: Union[Unset, List[str]] = UNSET,
    sorry_page_n: Union[Unset, List[str]] = UNSET,
    sorry_page_nic: Union[Unset, List[str]] = UNSET,
    sorry_page_nie: Union[Unset, List[str]] = UNSET,
    sorry_page_niew: Union[Unset, List[str]] = UNSET,
    sorry_page_nisw: Union[Unset, List[str]] = UNSET,
    source: Union[Unset, List[str]] = UNSET,
    source_empty: Union[Unset, bool] = UNSET,
    source_ic: Union[Unset, List[str]] = UNSET,
    source_ie: Union[Unset, List[str]] = UNSET,
    source_iew: Union[Unset, List[str]] = UNSET,
    source_isw: Union[Unset, List[str]] = UNSET,
    source_n: Union[Unset, List[str]] = UNSET,
    source_nic: Union[Unset, List[str]] = UNSET,
    source_nie: Union[Unset, List[str]] = UNSET,
    source_niew: Union[Unset, List[str]] = UNSET,
    source_nisw: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    target: Union[Unset, List[str]] = UNSET,
    target_empty: Union[Unset, bool] = UNSET,
    target_ic: Union[Unset, List[str]] = UNSET,
    target_ie: Union[Unset, List[str]] = UNSET,
    target_iew: Union[Unset, List[str]] = UNSET,
    target_isw: Union[Unset, List[str]] = UNSET,
    target_n: Union[Unset, List[str]] = UNSET,
    target_nic: Union[Unset, List[str]] = UNSET,
    target_nie: Union[Unset, List[str]] = UNSET,
    target_niew: Union[Unset, List[str]] = UNSET,
    target_nisw: Union[Unset, List[str]] = UNSET,
    testingpage: Union[Unset, List[str]] = UNSET,
    testingpage_empty: Union[Unset, bool] = UNSET,
    testingpage_ic: Union[Unset, List[str]] = UNSET,
    testingpage_ie: Union[Unset, List[str]] = UNSET,
    testingpage_iew: Union[Unset, List[str]] = UNSET,
    testingpage_isw: Union[Unset, List[str]] = UNSET,
    testingpage_n: Union[Unset, List[str]] = UNSET,
    testingpage_nic: Union[Unset, List[str]] = UNSET,
    testingpage_nie: Union[Unset, List[str]] = UNSET,
    testingpage_niew: Union[Unset, List[str]] = UNSET,
    testingpage_nisw: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    webdav: Union[Unset, bool] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_comment: Union[Unset, List[str]] = UNSET
    if not isinstance(comment, Unset):
        json_comment = comment

    params["Comment"] = json_comment

    params["Comment__empty"] = comment_empty

    json_comment_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(comment_ic, Unset):
        json_comment_ic = comment_ic

    params["Comment__ic"] = json_comment_ic

    json_comment_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(comment_ie, Unset):
        json_comment_ie = comment_ie

    params["Comment__ie"] = json_comment_ie

    json_comment_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(comment_iew, Unset):
        json_comment_iew = comment_iew

    params["Comment__iew"] = json_comment_iew

    json_comment_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(comment_isw, Unset):
        json_comment_isw = comment_isw

    params["Comment__isw"] = json_comment_isw

    json_comment_n: Union[Unset, List[str]] = UNSET
    if not isinstance(comment_n, Unset):
        json_comment_n = comment_n

    params["Comment__n"] = json_comment_n

    json_comment_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(comment_nic, Unset):
        json_comment_nic = comment_nic

    params["Comment__nic"] = json_comment_nic

    json_comment_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(comment_nie, Unset):
        json_comment_nie = comment_nie

    params["Comment__nie"] = json_comment_nie

    json_comment_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(comment_niew, Unset):
        json_comment_niew = comment_niew

    params["Comment__niew"] = json_comment_niew

    json_comment_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(comment_nisw, Unset):
        json_comment_nisw = comment_nisw

    params["Comment__nisw"] = json_comment_nisw

    params["authentication"] = authentication

    params["authentication__n"] = authentication_n

    params["client_max_body_size"] = client_max_body_size

    params["client_max_body_size__n"] = client_max_body_size_n

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    params["extra_protocols"] = extra_protocols

    params["gzip_proxied"] = gzip_proxied

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    json_keepalive_requests: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_requests, Unset):
        json_keepalive_requests = keepalive_requests

    params["keepalive_requests"] = json_keepalive_requests

    params["keepalive_requests__empty"] = keepalive_requests_empty

    json_keepalive_requests_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_requests_gt, Unset):
        json_keepalive_requests_gt = keepalive_requests_gt

    params["keepalive_requests__gt"] = json_keepalive_requests_gt

    json_keepalive_requests_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_requests_gte, Unset):
        json_keepalive_requests_gte = keepalive_requests_gte

    params["keepalive_requests__gte"] = json_keepalive_requests_gte

    json_keepalive_requests_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_requests_lt, Unset):
        json_keepalive_requests_lt = keepalive_requests_lt

    params["keepalive_requests__lt"] = json_keepalive_requests_lt

    json_keepalive_requests_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_requests_lte, Unset):
        json_keepalive_requests_lte = keepalive_requests_lte

    params["keepalive_requests__lte"] = json_keepalive_requests_lte

    json_keepalive_requests_n: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_requests_n, Unset):
        json_keepalive_requests_n = keepalive_requests_n

    params["keepalive_requests__n"] = json_keepalive_requests_n

    json_keepalive_timeout: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_timeout, Unset):
        json_keepalive_timeout = keepalive_timeout

    params["keepalive_timeout"] = json_keepalive_timeout

    params["keepalive_timeout__empty"] = keepalive_timeout_empty

    json_keepalive_timeout_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_timeout_gt, Unset):
        json_keepalive_timeout_gt = keepalive_timeout_gt

    params["keepalive_timeout__gt"] = json_keepalive_timeout_gt

    json_keepalive_timeout_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_timeout_gte, Unset):
        json_keepalive_timeout_gte = keepalive_timeout_gte

    params["keepalive_timeout__gte"] = json_keepalive_timeout_gte

    json_keepalive_timeout_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_timeout_lt, Unset):
        json_keepalive_timeout_lt = keepalive_timeout_lt

    params["keepalive_timeout__lt"] = json_keepalive_timeout_lt

    json_keepalive_timeout_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_timeout_lte, Unset):
        json_keepalive_timeout_lte = keepalive_timeout_lte

    params["keepalive_timeout__lte"] = json_keepalive_timeout_lte

    json_keepalive_timeout_n: Union[Unset, List[int]] = UNSET
    if not isinstance(keepalive_timeout_n, Unset):
        json_keepalive_timeout_n = keepalive_timeout_n

    params["keepalive_timeout__n"] = json_keepalive_timeout_n

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    params["limit"] = limit

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    params["offset"] = offset

    params["ordering"] = ordering

    json_proxy_buffer_size: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_buffer_size, Unset):
        json_proxy_buffer_size = proxy_buffer_size

    params["proxy_buffer_size"] = json_proxy_buffer_size

    params["proxy_buffer_size__empty"] = proxy_buffer_size_empty

    json_proxy_buffer_size_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_buffer_size_gt, Unset):
        json_proxy_buffer_size_gt = proxy_buffer_size_gt

    params["proxy_buffer_size__gt"] = json_proxy_buffer_size_gt

    json_proxy_buffer_size_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_buffer_size_gte, Unset):
        json_proxy_buffer_size_gte = proxy_buffer_size_gte

    params["proxy_buffer_size__gte"] = json_proxy_buffer_size_gte

    json_proxy_buffer_size_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_buffer_size_lt, Unset):
        json_proxy_buffer_size_lt = proxy_buffer_size_lt

    params["proxy_buffer_size__lt"] = json_proxy_buffer_size_lt

    json_proxy_buffer_size_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_buffer_size_lte, Unset):
        json_proxy_buffer_size_lte = proxy_buffer_size_lte

    params["proxy_buffer_size__lte"] = json_proxy_buffer_size_lte

    json_proxy_buffer_size_n: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_buffer_size_n, Unset):
        json_proxy_buffer_size_n = proxy_buffer_size_n

    params["proxy_buffer_size__n"] = json_proxy_buffer_size_n

    params["proxy_cache"] = proxy_cache

    json_proxy_read_timeout: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_read_timeout, Unset):
        json_proxy_read_timeout = proxy_read_timeout

    params["proxy_read_timeout"] = json_proxy_read_timeout

    params["proxy_read_timeout__empty"] = proxy_read_timeout_empty

    json_proxy_read_timeout_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_read_timeout_gt, Unset):
        json_proxy_read_timeout_gt = proxy_read_timeout_gt

    params["proxy_read_timeout__gt"] = json_proxy_read_timeout_gt

    json_proxy_read_timeout_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_read_timeout_gte, Unset):
        json_proxy_read_timeout_gte = proxy_read_timeout_gte

    params["proxy_read_timeout__gte"] = json_proxy_read_timeout_gte

    json_proxy_read_timeout_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_read_timeout_lt, Unset):
        json_proxy_read_timeout_lt = proxy_read_timeout_lt

    params["proxy_read_timeout__lt"] = json_proxy_read_timeout_lt

    json_proxy_read_timeout_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_read_timeout_lte, Unset):
        json_proxy_read_timeout_lte = proxy_read_timeout_lte

    params["proxy_read_timeout__lte"] = json_proxy_read_timeout_lte

    json_proxy_read_timeout_n: Union[Unset, List[int]] = UNSET
    if not isinstance(proxy_read_timeout_n, Unset):
        json_proxy_read_timeout_n = proxy_read_timeout_n

    params["proxy_read_timeout__n"] = json_proxy_read_timeout_n

    params["q"] = q

    json_sorry_page: Union[Unset, List[str]] = UNSET
    if not isinstance(sorry_page, Unset):
        json_sorry_page = sorry_page

    params["sorry_page"] = json_sorry_page

    params["sorry_page__empty"] = sorry_page_empty

    json_sorry_page_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(sorry_page_ic, Unset):
        json_sorry_page_ic = sorry_page_ic

    params["sorry_page__ic"] = json_sorry_page_ic

    json_sorry_page_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(sorry_page_ie, Unset):
        json_sorry_page_ie = sorry_page_ie

    params["sorry_page__ie"] = json_sorry_page_ie

    json_sorry_page_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(sorry_page_iew, Unset):
        json_sorry_page_iew = sorry_page_iew

    params["sorry_page__iew"] = json_sorry_page_iew

    json_sorry_page_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(sorry_page_isw, Unset):
        json_sorry_page_isw = sorry_page_isw

    params["sorry_page__isw"] = json_sorry_page_isw

    json_sorry_page_n: Union[Unset, List[str]] = UNSET
    if not isinstance(sorry_page_n, Unset):
        json_sorry_page_n = sorry_page_n

    params["sorry_page__n"] = json_sorry_page_n

    json_sorry_page_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(sorry_page_nic, Unset):
        json_sorry_page_nic = sorry_page_nic

    params["sorry_page__nic"] = json_sorry_page_nic

    json_sorry_page_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(sorry_page_nie, Unset):
        json_sorry_page_nie = sorry_page_nie

    params["sorry_page__nie"] = json_sorry_page_nie

    json_sorry_page_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(sorry_page_niew, Unset):
        json_sorry_page_niew = sorry_page_niew

    params["sorry_page__niew"] = json_sorry_page_niew

    json_sorry_page_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(sorry_page_nisw, Unset):
        json_sorry_page_nisw = sorry_page_nisw

    params["sorry_page__nisw"] = json_sorry_page_nisw

    json_source: Union[Unset, List[str]] = UNSET
    if not isinstance(source, Unset):
        json_source = source

    params["source"] = json_source

    params["source__empty"] = source_empty

    json_source_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(source_ic, Unset):
        json_source_ic = source_ic

    params["source__ic"] = json_source_ic

    json_source_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(source_ie, Unset):
        json_source_ie = source_ie

    params["source__ie"] = json_source_ie

    json_source_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(source_iew, Unset):
        json_source_iew = source_iew

    params["source__iew"] = json_source_iew

    json_source_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(source_isw, Unset):
        json_source_isw = source_isw

    params["source__isw"] = json_source_isw

    json_source_n: Union[Unset, List[str]] = UNSET
    if not isinstance(source_n, Unset):
        json_source_n = source_n

    params["source__n"] = json_source_n

    json_source_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(source_nic, Unset):
        json_source_nic = source_nic

    params["source__nic"] = json_source_nic

    json_source_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(source_nie, Unset):
        json_source_nie = source_nie

    params["source__nie"] = json_source_nie

    json_source_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(source_niew, Unset):
        json_source_niew = source_niew

    params["source__niew"] = json_source_niew

    json_source_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(source_nisw, Unset):
        json_source_nisw = source_nisw

    params["source__nisw"] = json_source_nisw

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_target: Union[Unset, List[str]] = UNSET
    if not isinstance(target, Unset):
        json_target = target

    params["target"] = json_target

    params["target__empty"] = target_empty

    json_target_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(target_ic, Unset):
        json_target_ic = target_ic

    params["target__ic"] = json_target_ic

    json_target_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(target_ie, Unset):
        json_target_ie = target_ie

    params["target__ie"] = json_target_ie

    json_target_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(target_iew, Unset):
        json_target_iew = target_iew

    params["target__iew"] = json_target_iew

    json_target_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(target_isw, Unset):
        json_target_isw = target_isw

    params["target__isw"] = json_target_isw

    json_target_n: Union[Unset, List[str]] = UNSET
    if not isinstance(target_n, Unset):
        json_target_n = target_n

    params["target__n"] = json_target_n

    json_target_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(target_nic, Unset):
        json_target_nic = target_nic

    params["target__nic"] = json_target_nic

    json_target_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(target_nie, Unset):
        json_target_nie = target_nie

    params["target__nie"] = json_target_nie

    json_target_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(target_niew, Unset):
        json_target_niew = target_niew

    params["target__niew"] = json_target_niew

    json_target_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(target_nisw, Unset):
        json_target_nisw = target_nisw

    params["target__nisw"] = json_target_nisw

    json_testingpage: Union[Unset, List[str]] = UNSET
    if not isinstance(testingpage, Unset):
        json_testingpage = testingpage

    params["testingpage"] = json_testingpage

    params["testingpage__empty"] = testingpage_empty

    json_testingpage_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(testingpage_ic, Unset):
        json_testingpage_ic = testingpage_ic

    params["testingpage__ic"] = json_testingpage_ic

    json_testingpage_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(testingpage_ie, Unset):
        json_testingpage_ie = testingpage_ie

    params["testingpage__ie"] = json_testingpage_ie

    json_testingpage_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(testingpage_iew, Unset):
        json_testingpage_iew = testingpage_iew

    params["testingpage__iew"] = json_testingpage_iew

    json_testingpage_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(testingpage_isw, Unset):
        json_testingpage_isw = testingpage_isw

    params["testingpage__isw"] = json_testingpage_isw

    json_testingpage_n: Union[Unset, List[str]] = UNSET
    if not isinstance(testingpage_n, Unset):
        json_testingpage_n = testingpage_n

    params["testingpage__n"] = json_testingpage_n

    json_testingpage_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(testingpage_nic, Unset):
        json_testingpage_nic = testingpage_nic

    params["testingpage__nic"] = json_testingpage_nic

    json_testingpage_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(testingpage_nie, Unset):
        json_testingpage_nie = testingpage_nie

    params["testingpage__nie"] = json_testingpage_nie

    json_testingpage_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(testingpage_niew, Unset):
        json_testingpage_niew = testingpage_niew

    params["testingpage__niew"] = json_testingpage_niew

    json_testingpage_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(testingpage_nisw, Unset):
        json_testingpage_nisw = testingpage_nisw

    params["testingpage__nisw"] = json_testingpage_nisw

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    params["webdav"] = webdav

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/plugins/rps/mapping/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedMappingList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedMappingList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedMappingList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    comment: Union[Unset, List[str]] = UNSET,
    comment_empty: Union[Unset, bool] = UNSET,
    comment_ic: Union[Unset, List[str]] = UNSET,
    comment_ie: Union[Unset, List[str]] = UNSET,
    comment_iew: Union[Unset, List[str]] = UNSET,
    comment_isw: Union[Unset, List[str]] = UNSET,
    comment_n: Union[Unset, List[str]] = UNSET,
    comment_nic: Union[Unset, List[str]] = UNSET,
    comment_nie: Union[Unset, List[str]] = UNSET,
    comment_niew: Union[Unset, List[str]] = UNSET,
    comment_nisw: Union[Unset, List[str]] = UNSET,
    authentication: Union[Unset, str] = UNSET,
    authentication_n: Union[Unset, str] = UNSET,
    client_max_body_size: Union[Unset, int] = UNSET,
    client_max_body_size_n: Union[Unset, int] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    extra_protocols: Union[Unset, str] = UNSET,
    gzip_proxied: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    keepalive_requests: Union[Unset, List[int]] = UNSET,
    keepalive_requests_empty: Union[Unset, bool] = UNSET,
    keepalive_requests_gt: Union[Unset, List[int]] = UNSET,
    keepalive_requests_gte: Union[Unset, List[int]] = UNSET,
    keepalive_requests_lt: Union[Unset, List[int]] = UNSET,
    keepalive_requests_lte: Union[Unset, List[int]] = UNSET,
    keepalive_requests_n: Union[Unset, List[int]] = UNSET,
    keepalive_timeout: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_empty: Union[Unset, bool] = UNSET,
    keepalive_timeout_gt: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_gte: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_lt: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_lte: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    proxy_buffer_size: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_empty: Union[Unset, bool] = UNSET,
    proxy_buffer_size_gt: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_gte: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_lt: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_lte: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_n: Union[Unset, List[int]] = UNSET,
    proxy_cache: Union[Unset, bool] = UNSET,
    proxy_read_timeout: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_empty: Union[Unset, bool] = UNSET,
    proxy_read_timeout_gt: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_gte: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_lt: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_lte: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_n: Union[Unset, List[int]] = UNSET,
    q: Union[Unset, str] = UNSET,
    sorry_page: Union[Unset, List[str]] = UNSET,
    sorry_page_empty: Union[Unset, bool] = UNSET,
    sorry_page_ic: Union[Unset, List[str]] = UNSET,
    sorry_page_ie: Union[Unset, List[str]] = UNSET,
    sorry_page_iew: Union[Unset, List[str]] = UNSET,
    sorry_page_isw: Union[Unset, List[str]] = UNSET,
    sorry_page_n: Union[Unset, List[str]] = UNSET,
    sorry_page_nic: Union[Unset, List[str]] = UNSET,
    sorry_page_nie: Union[Unset, List[str]] = UNSET,
    sorry_page_niew: Union[Unset, List[str]] = UNSET,
    sorry_page_nisw: Union[Unset, List[str]] = UNSET,
    source: Union[Unset, List[str]] = UNSET,
    source_empty: Union[Unset, bool] = UNSET,
    source_ic: Union[Unset, List[str]] = UNSET,
    source_ie: Union[Unset, List[str]] = UNSET,
    source_iew: Union[Unset, List[str]] = UNSET,
    source_isw: Union[Unset, List[str]] = UNSET,
    source_n: Union[Unset, List[str]] = UNSET,
    source_nic: Union[Unset, List[str]] = UNSET,
    source_nie: Union[Unset, List[str]] = UNSET,
    source_niew: Union[Unset, List[str]] = UNSET,
    source_nisw: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    target: Union[Unset, List[str]] = UNSET,
    target_empty: Union[Unset, bool] = UNSET,
    target_ic: Union[Unset, List[str]] = UNSET,
    target_ie: Union[Unset, List[str]] = UNSET,
    target_iew: Union[Unset, List[str]] = UNSET,
    target_isw: Union[Unset, List[str]] = UNSET,
    target_n: Union[Unset, List[str]] = UNSET,
    target_nic: Union[Unset, List[str]] = UNSET,
    target_nie: Union[Unset, List[str]] = UNSET,
    target_niew: Union[Unset, List[str]] = UNSET,
    target_nisw: Union[Unset, List[str]] = UNSET,
    testingpage: Union[Unset, List[str]] = UNSET,
    testingpage_empty: Union[Unset, bool] = UNSET,
    testingpage_ic: Union[Unset, List[str]] = UNSET,
    testingpage_ie: Union[Unset, List[str]] = UNSET,
    testingpage_iew: Union[Unset, List[str]] = UNSET,
    testingpage_isw: Union[Unset, List[str]] = UNSET,
    testingpage_n: Union[Unset, List[str]] = UNSET,
    testingpage_nic: Union[Unset, List[str]] = UNSET,
    testingpage_nie: Union[Unset, List[str]] = UNSET,
    testingpage_niew: Union[Unset, List[str]] = UNSET,
    testingpage_nisw: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    webdav: Union[Unset, bool] = UNSET,
) -> Response[PaginatedMappingList]:
    """Mapping view set class

    Args:
        comment (Union[Unset, List[str]]):
        comment_empty (Union[Unset, bool]):
        comment_ic (Union[Unset, List[str]]):
        comment_ie (Union[Unset, List[str]]):
        comment_iew (Union[Unset, List[str]]):
        comment_isw (Union[Unset, List[str]]):
        comment_n (Union[Unset, List[str]]):
        comment_nic (Union[Unset, List[str]]):
        comment_nie (Union[Unset, List[str]]):
        comment_niew (Union[Unset, List[str]]):
        comment_nisw (Union[Unset, List[str]]):
        authentication (Union[Unset, str]):
        authentication_n (Union[Unset, str]):
        client_max_body_size (Union[Unset, int]):
        client_max_body_size_n (Union[Unset, int]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        extra_protocols (Union[Unset, str]):
        gzip_proxied (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        keepalive_requests (Union[Unset, List[int]]):
        keepalive_requests_empty (Union[Unset, bool]):
        keepalive_requests_gt (Union[Unset, List[int]]):
        keepalive_requests_gte (Union[Unset, List[int]]):
        keepalive_requests_lt (Union[Unset, List[int]]):
        keepalive_requests_lte (Union[Unset, List[int]]):
        keepalive_requests_n (Union[Unset, List[int]]):
        keepalive_timeout (Union[Unset, List[int]]):
        keepalive_timeout_empty (Union[Unset, bool]):
        keepalive_timeout_gt (Union[Unset, List[int]]):
        keepalive_timeout_gte (Union[Unset, List[int]]):
        keepalive_timeout_lt (Union[Unset, List[int]]):
        keepalive_timeout_lte (Union[Unset, List[int]]):
        keepalive_timeout_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        proxy_buffer_size (Union[Unset, List[int]]):
        proxy_buffer_size_empty (Union[Unset, bool]):
        proxy_buffer_size_gt (Union[Unset, List[int]]):
        proxy_buffer_size_gte (Union[Unset, List[int]]):
        proxy_buffer_size_lt (Union[Unset, List[int]]):
        proxy_buffer_size_lte (Union[Unset, List[int]]):
        proxy_buffer_size_n (Union[Unset, List[int]]):
        proxy_cache (Union[Unset, bool]):
        proxy_read_timeout (Union[Unset, List[int]]):
        proxy_read_timeout_empty (Union[Unset, bool]):
        proxy_read_timeout_gt (Union[Unset, List[int]]):
        proxy_read_timeout_gte (Union[Unset, List[int]]):
        proxy_read_timeout_lt (Union[Unset, List[int]]):
        proxy_read_timeout_lte (Union[Unset, List[int]]):
        proxy_read_timeout_n (Union[Unset, List[int]]):
        q (Union[Unset, str]):
        sorry_page (Union[Unset, List[str]]):
        sorry_page_empty (Union[Unset, bool]):
        sorry_page_ic (Union[Unset, List[str]]):
        sorry_page_ie (Union[Unset, List[str]]):
        sorry_page_iew (Union[Unset, List[str]]):
        sorry_page_isw (Union[Unset, List[str]]):
        sorry_page_n (Union[Unset, List[str]]):
        sorry_page_nic (Union[Unset, List[str]]):
        sorry_page_nie (Union[Unset, List[str]]):
        sorry_page_niew (Union[Unset, List[str]]):
        sorry_page_nisw (Union[Unset, List[str]]):
        source (Union[Unset, List[str]]):
        source_empty (Union[Unset, bool]):
        source_ic (Union[Unset, List[str]]):
        source_ie (Union[Unset, List[str]]):
        source_iew (Union[Unset, List[str]]):
        source_isw (Union[Unset, List[str]]):
        source_n (Union[Unset, List[str]]):
        source_nic (Union[Unset, List[str]]):
        source_nie (Union[Unset, List[str]]):
        source_niew (Union[Unset, List[str]]):
        source_nisw (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        target (Union[Unset, List[str]]):
        target_empty (Union[Unset, bool]):
        target_ic (Union[Unset, List[str]]):
        target_ie (Union[Unset, List[str]]):
        target_iew (Union[Unset, List[str]]):
        target_isw (Union[Unset, List[str]]):
        target_n (Union[Unset, List[str]]):
        target_nic (Union[Unset, List[str]]):
        target_nie (Union[Unset, List[str]]):
        target_niew (Union[Unset, List[str]]):
        target_nisw (Union[Unset, List[str]]):
        testingpage (Union[Unset, List[str]]):
        testingpage_empty (Union[Unset, bool]):
        testingpage_ic (Union[Unset, List[str]]):
        testingpage_ie (Union[Unset, List[str]]):
        testingpage_iew (Union[Unset, List[str]]):
        testingpage_isw (Union[Unset, List[str]]):
        testingpage_n (Union[Unset, List[str]]):
        testingpage_nic (Union[Unset, List[str]]):
        testingpage_nie (Union[Unset, List[str]]):
        testingpage_niew (Union[Unset, List[str]]):
        testingpage_nisw (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        webdav (Union[Unset, bool]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedMappingList]
    """

    kwargs = _get_kwargs(
        comment=comment,
        comment_empty=comment_empty,
        comment_ic=comment_ic,
        comment_ie=comment_ie,
        comment_iew=comment_iew,
        comment_isw=comment_isw,
        comment_n=comment_n,
        comment_nic=comment_nic,
        comment_nie=comment_nie,
        comment_niew=comment_niew,
        comment_nisw=comment_nisw,
        authentication=authentication,
        authentication_n=authentication_n,
        client_max_body_size=client_max_body_size,
        client_max_body_size_n=client_max_body_size_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        extra_protocols=extra_protocols,
        gzip_proxied=gzip_proxied,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        keepalive_requests=keepalive_requests,
        keepalive_requests_empty=keepalive_requests_empty,
        keepalive_requests_gt=keepalive_requests_gt,
        keepalive_requests_gte=keepalive_requests_gte,
        keepalive_requests_lt=keepalive_requests_lt,
        keepalive_requests_lte=keepalive_requests_lte,
        keepalive_requests_n=keepalive_requests_n,
        keepalive_timeout=keepalive_timeout,
        keepalive_timeout_empty=keepalive_timeout_empty,
        keepalive_timeout_gt=keepalive_timeout_gt,
        keepalive_timeout_gte=keepalive_timeout_gte,
        keepalive_timeout_lt=keepalive_timeout_lt,
        keepalive_timeout_lte=keepalive_timeout_lte,
        keepalive_timeout_n=keepalive_timeout_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        proxy_buffer_size=proxy_buffer_size,
        proxy_buffer_size_empty=proxy_buffer_size_empty,
        proxy_buffer_size_gt=proxy_buffer_size_gt,
        proxy_buffer_size_gte=proxy_buffer_size_gte,
        proxy_buffer_size_lt=proxy_buffer_size_lt,
        proxy_buffer_size_lte=proxy_buffer_size_lte,
        proxy_buffer_size_n=proxy_buffer_size_n,
        proxy_cache=proxy_cache,
        proxy_read_timeout=proxy_read_timeout,
        proxy_read_timeout_empty=proxy_read_timeout_empty,
        proxy_read_timeout_gt=proxy_read_timeout_gt,
        proxy_read_timeout_gte=proxy_read_timeout_gte,
        proxy_read_timeout_lt=proxy_read_timeout_lt,
        proxy_read_timeout_lte=proxy_read_timeout_lte,
        proxy_read_timeout_n=proxy_read_timeout_n,
        q=q,
        sorry_page=sorry_page,
        sorry_page_empty=sorry_page_empty,
        sorry_page_ic=sorry_page_ic,
        sorry_page_ie=sorry_page_ie,
        sorry_page_iew=sorry_page_iew,
        sorry_page_isw=sorry_page_isw,
        sorry_page_n=sorry_page_n,
        sorry_page_nic=sorry_page_nic,
        sorry_page_nie=sorry_page_nie,
        sorry_page_niew=sorry_page_niew,
        sorry_page_nisw=sorry_page_nisw,
        source=source,
        source_empty=source_empty,
        source_ic=source_ic,
        source_ie=source_ie,
        source_iew=source_iew,
        source_isw=source_isw,
        source_n=source_n,
        source_nic=source_nic,
        source_nie=source_nie,
        source_niew=source_niew,
        source_nisw=source_nisw,
        tag=tag,
        tag_n=tag_n,
        target=target,
        target_empty=target_empty,
        target_ic=target_ic,
        target_ie=target_ie,
        target_iew=target_iew,
        target_isw=target_isw,
        target_n=target_n,
        target_nic=target_nic,
        target_nie=target_nie,
        target_niew=target_niew,
        target_nisw=target_nisw,
        testingpage=testingpage,
        testingpage_empty=testingpage_empty,
        testingpage_ic=testingpage_ic,
        testingpage_ie=testingpage_ie,
        testingpage_iew=testingpage_iew,
        testingpage_isw=testingpage_isw,
        testingpage_n=testingpage_n,
        testingpage_nic=testingpage_nic,
        testingpage_nie=testingpage_nie,
        testingpage_niew=testingpage_niew,
        testingpage_nisw=testingpage_nisw,
        updated_by_request=updated_by_request,
        webdav=webdav,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    comment: Union[Unset, List[str]] = UNSET,
    comment_empty: Union[Unset, bool] = UNSET,
    comment_ic: Union[Unset, List[str]] = UNSET,
    comment_ie: Union[Unset, List[str]] = UNSET,
    comment_iew: Union[Unset, List[str]] = UNSET,
    comment_isw: Union[Unset, List[str]] = UNSET,
    comment_n: Union[Unset, List[str]] = UNSET,
    comment_nic: Union[Unset, List[str]] = UNSET,
    comment_nie: Union[Unset, List[str]] = UNSET,
    comment_niew: Union[Unset, List[str]] = UNSET,
    comment_nisw: Union[Unset, List[str]] = UNSET,
    authentication: Union[Unset, str] = UNSET,
    authentication_n: Union[Unset, str] = UNSET,
    client_max_body_size: Union[Unset, int] = UNSET,
    client_max_body_size_n: Union[Unset, int] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    extra_protocols: Union[Unset, str] = UNSET,
    gzip_proxied: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    keepalive_requests: Union[Unset, List[int]] = UNSET,
    keepalive_requests_empty: Union[Unset, bool] = UNSET,
    keepalive_requests_gt: Union[Unset, List[int]] = UNSET,
    keepalive_requests_gte: Union[Unset, List[int]] = UNSET,
    keepalive_requests_lt: Union[Unset, List[int]] = UNSET,
    keepalive_requests_lte: Union[Unset, List[int]] = UNSET,
    keepalive_requests_n: Union[Unset, List[int]] = UNSET,
    keepalive_timeout: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_empty: Union[Unset, bool] = UNSET,
    keepalive_timeout_gt: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_gte: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_lt: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_lte: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    proxy_buffer_size: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_empty: Union[Unset, bool] = UNSET,
    proxy_buffer_size_gt: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_gte: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_lt: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_lte: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_n: Union[Unset, List[int]] = UNSET,
    proxy_cache: Union[Unset, bool] = UNSET,
    proxy_read_timeout: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_empty: Union[Unset, bool] = UNSET,
    proxy_read_timeout_gt: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_gte: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_lt: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_lte: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_n: Union[Unset, List[int]] = UNSET,
    q: Union[Unset, str] = UNSET,
    sorry_page: Union[Unset, List[str]] = UNSET,
    sorry_page_empty: Union[Unset, bool] = UNSET,
    sorry_page_ic: Union[Unset, List[str]] = UNSET,
    sorry_page_ie: Union[Unset, List[str]] = UNSET,
    sorry_page_iew: Union[Unset, List[str]] = UNSET,
    sorry_page_isw: Union[Unset, List[str]] = UNSET,
    sorry_page_n: Union[Unset, List[str]] = UNSET,
    sorry_page_nic: Union[Unset, List[str]] = UNSET,
    sorry_page_nie: Union[Unset, List[str]] = UNSET,
    sorry_page_niew: Union[Unset, List[str]] = UNSET,
    sorry_page_nisw: Union[Unset, List[str]] = UNSET,
    source: Union[Unset, List[str]] = UNSET,
    source_empty: Union[Unset, bool] = UNSET,
    source_ic: Union[Unset, List[str]] = UNSET,
    source_ie: Union[Unset, List[str]] = UNSET,
    source_iew: Union[Unset, List[str]] = UNSET,
    source_isw: Union[Unset, List[str]] = UNSET,
    source_n: Union[Unset, List[str]] = UNSET,
    source_nic: Union[Unset, List[str]] = UNSET,
    source_nie: Union[Unset, List[str]] = UNSET,
    source_niew: Union[Unset, List[str]] = UNSET,
    source_nisw: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    target: Union[Unset, List[str]] = UNSET,
    target_empty: Union[Unset, bool] = UNSET,
    target_ic: Union[Unset, List[str]] = UNSET,
    target_ie: Union[Unset, List[str]] = UNSET,
    target_iew: Union[Unset, List[str]] = UNSET,
    target_isw: Union[Unset, List[str]] = UNSET,
    target_n: Union[Unset, List[str]] = UNSET,
    target_nic: Union[Unset, List[str]] = UNSET,
    target_nie: Union[Unset, List[str]] = UNSET,
    target_niew: Union[Unset, List[str]] = UNSET,
    target_nisw: Union[Unset, List[str]] = UNSET,
    testingpage: Union[Unset, List[str]] = UNSET,
    testingpage_empty: Union[Unset, bool] = UNSET,
    testingpage_ic: Union[Unset, List[str]] = UNSET,
    testingpage_ie: Union[Unset, List[str]] = UNSET,
    testingpage_iew: Union[Unset, List[str]] = UNSET,
    testingpage_isw: Union[Unset, List[str]] = UNSET,
    testingpage_n: Union[Unset, List[str]] = UNSET,
    testingpage_nic: Union[Unset, List[str]] = UNSET,
    testingpage_nie: Union[Unset, List[str]] = UNSET,
    testingpage_niew: Union[Unset, List[str]] = UNSET,
    testingpage_nisw: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    webdav: Union[Unset, bool] = UNSET,
) -> Optional[PaginatedMappingList]:
    """Mapping view set class

    Args:
        comment (Union[Unset, List[str]]):
        comment_empty (Union[Unset, bool]):
        comment_ic (Union[Unset, List[str]]):
        comment_ie (Union[Unset, List[str]]):
        comment_iew (Union[Unset, List[str]]):
        comment_isw (Union[Unset, List[str]]):
        comment_n (Union[Unset, List[str]]):
        comment_nic (Union[Unset, List[str]]):
        comment_nie (Union[Unset, List[str]]):
        comment_niew (Union[Unset, List[str]]):
        comment_nisw (Union[Unset, List[str]]):
        authentication (Union[Unset, str]):
        authentication_n (Union[Unset, str]):
        client_max_body_size (Union[Unset, int]):
        client_max_body_size_n (Union[Unset, int]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        extra_protocols (Union[Unset, str]):
        gzip_proxied (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        keepalive_requests (Union[Unset, List[int]]):
        keepalive_requests_empty (Union[Unset, bool]):
        keepalive_requests_gt (Union[Unset, List[int]]):
        keepalive_requests_gte (Union[Unset, List[int]]):
        keepalive_requests_lt (Union[Unset, List[int]]):
        keepalive_requests_lte (Union[Unset, List[int]]):
        keepalive_requests_n (Union[Unset, List[int]]):
        keepalive_timeout (Union[Unset, List[int]]):
        keepalive_timeout_empty (Union[Unset, bool]):
        keepalive_timeout_gt (Union[Unset, List[int]]):
        keepalive_timeout_gte (Union[Unset, List[int]]):
        keepalive_timeout_lt (Union[Unset, List[int]]):
        keepalive_timeout_lte (Union[Unset, List[int]]):
        keepalive_timeout_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        proxy_buffer_size (Union[Unset, List[int]]):
        proxy_buffer_size_empty (Union[Unset, bool]):
        proxy_buffer_size_gt (Union[Unset, List[int]]):
        proxy_buffer_size_gte (Union[Unset, List[int]]):
        proxy_buffer_size_lt (Union[Unset, List[int]]):
        proxy_buffer_size_lte (Union[Unset, List[int]]):
        proxy_buffer_size_n (Union[Unset, List[int]]):
        proxy_cache (Union[Unset, bool]):
        proxy_read_timeout (Union[Unset, List[int]]):
        proxy_read_timeout_empty (Union[Unset, bool]):
        proxy_read_timeout_gt (Union[Unset, List[int]]):
        proxy_read_timeout_gte (Union[Unset, List[int]]):
        proxy_read_timeout_lt (Union[Unset, List[int]]):
        proxy_read_timeout_lte (Union[Unset, List[int]]):
        proxy_read_timeout_n (Union[Unset, List[int]]):
        q (Union[Unset, str]):
        sorry_page (Union[Unset, List[str]]):
        sorry_page_empty (Union[Unset, bool]):
        sorry_page_ic (Union[Unset, List[str]]):
        sorry_page_ie (Union[Unset, List[str]]):
        sorry_page_iew (Union[Unset, List[str]]):
        sorry_page_isw (Union[Unset, List[str]]):
        sorry_page_n (Union[Unset, List[str]]):
        sorry_page_nic (Union[Unset, List[str]]):
        sorry_page_nie (Union[Unset, List[str]]):
        sorry_page_niew (Union[Unset, List[str]]):
        sorry_page_nisw (Union[Unset, List[str]]):
        source (Union[Unset, List[str]]):
        source_empty (Union[Unset, bool]):
        source_ic (Union[Unset, List[str]]):
        source_ie (Union[Unset, List[str]]):
        source_iew (Union[Unset, List[str]]):
        source_isw (Union[Unset, List[str]]):
        source_n (Union[Unset, List[str]]):
        source_nic (Union[Unset, List[str]]):
        source_nie (Union[Unset, List[str]]):
        source_niew (Union[Unset, List[str]]):
        source_nisw (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        target (Union[Unset, List[str]]):
        target_empty (Union[Unset, bool]):
        target_ic (Union[Unset, List[str]]):
        target_ie (Union[Unset, List[str]]):
        target_iew (Union[Unset, List[str]]):
        target_isw (Union[Unset, List[str]]):
        target_n (Union[Unset, List[str]]):
        target_nic (Union[Unset, List[str]]):
        target_nie (Union[Unset, List[str]]):
        target_niew (Union[Unset, List[str]]):
        target_nisw (Union[Unset, List[str]]):
        testingpage (Union[Unset, List[str]]):
        testingpage_empty (Union[Unset, bool]):
        testingpage_ic (Union[Unset, List[str]]):
        testingpage_ie (Union[Unset, List[str]]):
        testingpage_iew (Union[Unset, List[str]]):
        testingpage_isw (Union[Unset, List[str]]):
        testingpage_n (Union[Unset, List[str]]):
        testingpage_nic (Union[Unset, List[str]]):
        testingpage_nie (Union[Unset, List[str]]):
        testingpage_niew (Union[Unset, List[str]]):
        testingpage_nisw (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        webdav (Union[Unset, bool]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedMappingList
    """

    return sync_detailed(
        client=client,
        comment=comment,
        comment_empty=comment_empty,
        comment_ic=comment_ic,
        comment_ie=comment_ie,
        comment_iew=comment_iew,
        comment_isw=comment_isw,
        comment_n=comment_n,
        comment_nic=comment_nic,
        comment_nie=comment_nie,
        comment_niew=comment_niew,
        comment_nisw=comment_nisw,
        authentication=authentication,
        authentication_n=authentication_n,
        client_max_body_size=client_max_body_size,
        client_max_body_size_n=client_max_body_size_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        extra_protocols=extra_protocols,
        gzip_proxied=gzip_proxied,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        keepalive_requests=keepalive_requests,
        keepalive_requests_empty=keepalive_requests_empty,
        keepalive_requests_gt=keepalive_requests_gt,
        keepalive_requests_gte=keepalive_requests_gte,
        keepalive_requests_lt=keepalive_requests_lt,
        keepalive_requests_lte=keepalive_requests_lte,
        keepalive_requests_n=keepalive_requests_n,
        keepalive_timeout=keepalive_timeout,
        keepalive_timeout_empty=keepalive_timeout_empty,
        keepalive_timeout_gt=keepalive_timeout_gt,
        keepalive_timeout_gte=keepalive_timeout_gte,
        keepalive_timeout_lt=keepalive_timeout_lt,
        keepalive_timeout_lte=keepalive_timeout_lte,
        keepalive_timeout_n=keepalive_timeout_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        proxy_buffer_size=proxy_buffer_size,
        proxy_buffer_size_empty=proxy_buffer_size_empty,
        proxy_buffer_size_gt=proxy_buffer_size_gt,
        proxy_buffer_size_gte=proxy_buffer_size_gte,
        proxy_buffer_size_lt=proxy_buffer_size_lt,
        proxy_buffer_size_lte=proxy_buffer_size_lte,
        proxy_buffer_size_n=proxy_buffer_size_n,
        proxy_cache=proxy_cache,
        proxy_read_timeout=proxy_read_timeout,
        proxy_read_timeout_empty=proxy_read_timeout_empty,
        proxy_read_timeout_gt=proxy_read_timeout_gt,
        proxy_read_timeout_gte=proxy_read_timeout_gte,
        proxy_read_timeout_lt=proxy_read_timeout_lt,
        proxy_read_timeout_lte=proxy_read_timeout_lte,
        proxy_read_timeout_n=proxy_read_timeout_n,
        q=q,
        sorry_page=sorry_page,
        sorry_page_empty=sorry_page_empty,
        sorry_page_ic=sorry_page_ic,
        sorry_page_ie=sorry_page_ie,
        sorry_page_iew=sorry_page_iew,
        sorry_page_isw=sorry_page_isw,
        sorry_page_n=sorry_page_n,
        sorry_page_nic=sorry_page_nic,
        sorry_page_nie=sorry_page_nie,
        sorry_page_niew=sorry_page_niew,
        sorry_page_nisw=sorry_page_nisw,
        source=source,
        source_empty=source_empty,
        source_ic=source_ic,
        source_ie=source_ie,
        source_iew=source_iew,
        source_isw=source_isw,
        source_n=source_n,
        source_nic=source_nic,
        source_nie=source_nie,
        source_niew=source_niew,
        source_nisw=source_nisw,
        tag=tag,
        tag_n=tag_n,
        target=target,
        target_empty=target_empty,
        target_ic=target_ic,
        target_ie=target_ie,
        target_iew=target_iew,
        target_isw=target_isw,
        target_n=target_n,
        target_nic=target_nic,
        target_nie=target_nie,
        target_niew=target_niew,
        target_nisw=target_nisw,
        testingpage=testingpage,
        testingpage_empty=testingpage_empty,
        testingpage_ic=testingpage_ic,
        testingpage_ie=testingpage_ie,
        testingpage_iew=testingpage_iew,
        testingpage_isw=testingpage_isw,
        testingpage_n=testingpage_n,
        testingpage_nic=testingpage_nic,
        testingpage_nie=testingpage_nie,
        testingpage_niew=testingpage_niew,
        testingpage_nisw=testingpage_nisw,
        updated_by_request=updated_by_request,
        webdav=webdav,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    comment: Union[Unset, List[str]] = UNSET,
    comment_empty: Union[Unset, bool] = UNSET,
    comment_ic: Union[Unset, List[str]] = UNSET,
    comment_ie: Union[Unset, List[str]] = UNSET,
    comment_iew: Union[Unset, List[str]] = UNSET,
    comment_isw: Union[Unset, List[str]] = UNSET,
    comment_n: Union[Unset, List[str]] = UNSET,
    comment_nic: Union[Unset, List[str]] = UNSET,
    comment_nie: Union[Unset, List[str]] = UNSET,
    comment_niew: Union[Unset, List[str]] = UNSET,
    comment_nisw: Union[Unset, List[str]] = UNSET,
    authentication: Union[Unset, str] = UNSET,
    authentication_n: Union[Unset, str] = UNSET,
    client_max_body_size: Union[Unset, int] = UNSET,
    client_max_body_size_n: Union[Unset, int] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    extra_protocols: Union[Unset, str] = UNSET,
    gzip_proxied: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    keepalive_requests: Union[Unset, List[int]] = UNSET,
    keepalive_requests_empty: Union[Unset, bool] = UNSET,
    keepalive_requests_gt: Union[Unset, List[int]] = UNSET,
    keepalive_requests_gte: Union[Unset, List[int]] = UNSET,
    keepalive_requests_lt: Union[Unset, List[int]] = UNSET,
    keepalive_requests_lte: Union[Unset, List[int]] = UNSET,
    keepalive_requests_n: Union[Unset, List[int]] = UNSET,
    keepalive_timeout: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_empty: Union[Unset, bool] = UNSET,
    keepalive_timeout_gt: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_gte: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_lt: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_lte: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    proxy_buffer_size: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_empty: Union[Unset, bool] = UNSET,
    proxy_buffer_size_gt: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_gte: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_lt: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_lte: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_n: Union[Unset, List[int]] = UNSET,
    proxy_cache: Union[Unset, bool] = UNSET,
    proxy_read_timeout: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_empty: Union[Unset, bool] = UNSET,
    proxy_read_timeout_gt: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_gte: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_lt: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_lte: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_n: Union[Unset, List[int]] = UNSET,
    q: Union[Unset, str] = UNSET,
    sorry_page: Union[Unset, List[str]] = UNSET,
    sorry_page_empty: Union[Unset, bool] = UNSET,
    sorry_page_ic: Union[Unset, List[str]] = UNSET,
    sorry_page_ie: Union[Unset, List[str]] = UNSET,
    sorry_page_iew: Union[Unset, List[str]] = UNSET,
    sorry_page_isw: Union[Unset, List[str]] = UNSET,
    sorry_page_n: Union[Unset, List[str]] = UNSET,
    sorry_page_nic: Union[Unset, List[str]] = UNSET,
    sorry_page_nie: Union[Unset, List[str]] = UNSET,
    sorry_page_niew: Union[Unset, List[str]] = UNSET,
    sorry_page_nisw: Union[Unset, List[str]] = UNSET,
    source: Union[Unset, List[str]] = UNSET,
    source_empty: Union[Unset, bool] = UNSET,
    source_ic: Union[Unset, List[str]] = UNSET,
    source_ie: Union[Unset, List[str]] = UNSET,
    source_iew: Union[Unset, List[str]] = UNSET,
    source_isw: Union[Unset, List[str]] = UNSET,
    source_n: Union[Unset, List[str]] = UNSET,
    source_nic: Union[Unset, List[str]] = UNSET,
    source_nie: Union[Unset, List[str]] = UNSET,
    source_niew: Union[Unset, List[str]] = UNSET,
    source_nisw: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    target: Union[Unset, List[str]] = UNSET,
    target_empty: Union[Unset, bool] = UNSET,
    target_ic: Union[Unset, List[str]] = UNSET,
    target_ie: Union[Unset, List[str]] = UNSET,
    target_iew: Union[Unset, List[str]] = UNSET,
    target_isw: Union[Unset, List[str]] = UNSET,
    target_n: Union[Unset, List[str]] = UNSET,
    target_nic: Union[Unset, List[str]] = UNSET,
    target_nie: Union[Unset, List[str]] = UNSET,
    target_niew: Union[Unset, List[str]] = UNSET,
    target_nisw: Union[Unset, List[str]] = UNSET,
    testingpage: Union[Unset, List[str]] = UNSET,
    testingpage_empty: Union[Unset, bool] = UNSET,
    testingpage_ic: Union[Unset, List[str]] = UNSET,
    testingpage_ie: Union[Unset, List[str]] = UNSET,
    testingpage_iew: Union[Unset, List[str]] = UNSET,
    testingpage_isw: Union[Unset, List[str]] = UNSET,
    testingpage_n: Union[Unset, List[str]] = UNSET,
    testingpage_nic: Union[Unset, List[str]] = UNSET,
    testingpage_nie: Union[Unset, List[str]] = UNSET,
    testingpage_niew: Union[Unset, List[str]] = UNSET,
    testingpage_nisw: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    webdav: Union[Unset, bool] = UNSET,
) -> Response[PaginatedMappingList]:
    """Mapping view set class

    Args:
        comment (Union[Unset, List[str]]):
        comment_empty (Union[Unset, bool]):
        comment_ic (Union[Unset, List[str]]):
        comment_ie (Union[Unset, List[str]]):
        comment_iew (Union[Unset, List[str]]):
        comment_isw (Union[Unset, List[str]]):
        comment_n (Union[Unset, List[str]]):
        comment_nic (Union[Unset, List[str]]):
        comment_nie (Union[Unset, List[str]]):
        comment_niew (Union[Unset, List[str]]):
        comment_nisw (Union[Unset, List[str]]):
        authentication (Union[Unset, str]):
        authentication_n (Union[Unset, str]):
        client_max_body_size (Union[Unset, int]):
        client_max_body_size_n (Union[Unset, int]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        extra_protocols (Union[Unset, str]):
        gzip_proxied (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        keepalive_requests (Union[Unset, List[int]]):
        keepalive_requests_empty (Union[Unset, bool]):
        keepalive_requests_gt (Union[Unset, List[int]]):
        keepalive_requests_gte (Union[Unset, List[int]]):
        keepalive_requests_lt (Union[Unset, List[int]]):
        keepalive_requests_lte (Union[Unset, List[int]]):
        keepalive_requests_n (Union[Unset, List[int]]):
        keepalive_timeout (Union[Unset, List[int]]):
        keepalive_timeout_empty (Union[Unset, bool]):
        keepalive_timeout_gt (Union[Unset, List[int]]):
        keepalive_timeout_gte (Union[Unset, List[int]]):
        keepalive_timeout_lt (Union[Unset, List[int]]):
        keepalive_timeout_lte (Union[Unset, List[int]]):
        keepalive_timeout_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        proxy_buffer_size (Union[Unset, List[int]]):
        proxy_buffer_size_empty (Union[Unset, bool]):
        proxy_buffer_size_gt (Union[Unset, List[int]]):
        proxy_buffer_size_gte (Union[Unset, List[int]]):
        proxy_buffer_size_lt (Union[Unset, List[int]]):
        proxy_buffer_size_lte (Union[Unset, List[int]]):
        proxy_buffer_size_n (Union[Unset, List[int]]):
        proxy_cache (Union[Unset, bool]):
        proxy_read_timeout (Union[Unset, List[int]]):
        proxy_read_timeout_empty (Union[Unset, bool]):
        proxy_read_timeout_gt (Union[Unset, List[int]]):
        proxy_read_timeout_gte (Union[Unset, List[int]]):
        proxy_read_timeout_lt (Union[Unset, List[int]]):
        proxy_read_timeout_lte (Union[Unset, List[int]]):
        proxy_read_timeout_n (Union[Unset, List[int]]):
        q (Union[Unset, str]):
        sorry_page (Union[Unset, List[str]]):
        sorry_page_empty (Union[Unset, bool]):
        sorry_page_ic (Union[Unset, List[str]]):
        sorry_page_ie (Union[Unset, List[str]]):
        sorry_page_iew (Union[Unset, List[str]]):
        sorry_page_isw (Union[Unset, List[str]]):
        sorry_page_n (Union[Unset, List[str]]):
        sorry_page_nic (Union[Unset, List[str]]):
        sorry_page_nie (Union[Unset, List[str]]):
        sorry_page_niew (Union[Unset, List[str]]):
        sorry_page_nisw (Union[Unset, List[str]]):
        source (Union[Unset, List[str]]):
        source_empty (Union[Unset, bool]):
        source_ic (Union[Unset, List[str]]):
        source_ie (Union[Unset, List[str]]):
        source_iew (Union[Unset, List[str]]):
        source_isw (Union[Unset, List[str]]):
        source_n (Union[Unset, List[str]]):
        source_nic (Union[Unset, List[str]]):
        source_nie (Union[Unset, List[str]]):
        source_niew (Union[Unset, List[str]]):
        source_nisw (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        target (Union[Unset, List[str]]):
        target_empty (Union[Unset, bool]):
        target_ic (Union[Unset, List[str]]):
        target_ie (Union[Unset, List[str]]):
        target_iew (Union[Unset, List[str]]):
        target_isw (Union[Unset, List[str]]):
        target_n (Union[Unset, List[str]]):
        target_nic (Union[Unset, List[str]]):
        target_nie (Union[Unset, List[str]]):
        target_niew (Union[Unset, List[str]]):
        target_nisw (Union[Unset, List[str]]):
        testingpage (Union[Unset, List[str]]):
        testingpage_empty (Union[Unset, bool]):
        testingpage_ic (Union[Unset, List[str]]):
        testingpage_ie (Union[Unset, List[str]]):
        testingpage_iew (Union[Unset, List[str]]):
        testingpage_isw (Union[Unset, List[str]]):
        testingpage_n (Union[Unset, List[str]]):
        testingpage_nic (Union[Unset, List[str]]):
        testingpage_nie (Union[Unset, List[str]]):
        testingpage_niew (Union[Unset, List[str]]):
        testingpage_nisw (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        webdav (Union[Unset, bool]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedMappingList]
    """

    kwargs = _get_kwargs(
        comment=comment,
        comment_empty=comment_empty,
        comment_ic=comment_ic,
        comment_ie=comment_ie,
        comment_iew=comment_iew,
        comment_isw=comment_isw,
        comment_n=comment_n,
        comment_nic=comment_nic,
        comment_nie=comment_nie,
        comment_niew=comment_niew,
        comment_nisw=comment_nisw,
        authentication=authentication,
        authentication_n=authentication_n,
        client_max_body_size=client_max_body_size,
        client_max_body_size_n=client_max_body_size_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        extra_protocols=extra_protocols,
        gzip_proxied=gzip_proxied,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        keepalive_requests=keepalive_requests,
        keepalive_requests_empty=keepalive_requests_empty,
        keepalive_requests_gt=keepalive_requests_gt,
        keepalive_requests_gte=keepalive_requests_gte,
        keepalive_requests_lt=keepalive_requests_lt,
        keepalive_requests_lte=keepalive_requests_lte,
        keepalive_requests_n=keepalive_requests_n,
        keepalive_timeout=keepalive_timeout,
        keepalive_timeout_empty=keepalive_timeout_empty,
        keepalive_timeout_gt=keepalive_timeout_gt,
        keepalive_timeout_gte=keepalive_timeout_gte,
        keepalive_timeout_lt=keepalive_timeout_lt,
        keepalive_timeout_lte=keepalive_timeout_lte,
        keepalive_timeout_n=keepalive_timeout_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        proxy_buffer_size=proxy_buffer_size,
        proxy_buffer_size_empty=proxy_buffer_size_empty,
        proxy_buffer_size_gt=proxy_buffer_size_gt,
        proxy_buffer_size_gte=proxy_buffer_size_gte,
        proxy_buffer_size_lt=proxy_buffer_size_lt,
        proxy_buffer_size_lte=proxy_buffer_size_lte,
        proxy_buffer_size_n=proxy_buffer_size_n,
        proxy_cache=proxy_cache,
        proxy_read_timeout=proxy_read_timeout,
        proxy_read_timeout_empty=proxy_read_timeout_empty,
        proxy_read_timeout_gt=proxy_read_timeout_gt,
        proxy_read_timeout_gte=proxy_read_timeout_gte,
        proxy_read_timeout_lt=proxy_read_timeout_lt,
        proxy_read_timeout_lte=proxy_read_timeout_lte,
        proxy_read_timeout_n=proxy_read_timeout_n,
        q=q,
        sorry_page=sorry_page,
        sorry_page_empty=sorry_page_empty,
        sorry_page_ic=sorry_page_ic,
        sorry_page_ie=sorry_page_ie,
        sorry_page_iew=sorry_page_iew,
        sorry_page_isw=sorry_page_isw,
        sorry_page_n=sorry_page_n,
        sorry_page_nic=sorry_page_nic,
        sorry_page_nie=sorry_page_nie,
        sorry_page_niew=sorry_page_niew,
        sorry_page_nisw=sorry_page_nisw,
        source=source,
        source_empty=source_empty,
        source_ic=source_ic,
        source_ie=source_ie,
        source_iew=source_iew,
        source_isw=source_isw,
        source_n=source_n,
        source_nic=source_nic,
        source_nie=source_nie,
        source_niew=source_niew,
        source_nisw=source_nisw,
        tag=tag,
        tag_n=tag_n,
        target=target,
        target_empty=target_empty,
        target_ic=target_ic,
        target_ie=target_ie,
        target_iew=target_iew,
        target_isw=target_isw,
        target_n=target_n,
        target_nic=target_nic,
        target_nie=target_nie,
        target_niew=target_niew,
        target_nisw=target_nisw,
        testingpage=testingpage,
        testingpage_empty=testingpage_empty,
        testingpage_ic=testingpage_ic,
        testingpage_ie=testingpage_ie,
        testingpage_iew=testingpage_iew,
        testingpage_isw=testingpage_isw,
        testingpage_n=testingpage_n,
        testingpage_nic=testingpage_nic,
        testingpage_nie=testingpage_nie,
        testingpage_niew=testingpage_niew,
        testingpage_nisw=testingpage_nisw,
        updated_by_request=updated_by_request,
        webdav=webdav,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    comment: Union[Unset, List[str]] = UNSET,
    comment_empty: Union[Unset, bool] = UNSET,
    comment_ic: Union[Unset, List[str]] = UNSET,
    comment_ie: Union[Unset, List[str]] = UNSET,
    comment_iew: Union[Unset, List[str]] = UNSET,
    comment_isw: Union[Unset, List[str]] = UNSET,
    comment_n: Union[Unset, List[str]] = UNSET,
    comment_nic: Union[Unset, List[str]] = UNSET,
    comment_nie: Union[Unset, List[str]] = UNSET,
    comment_niew: Union[Unset, List[str]] = UNSET,
    comment_nisw: Union[Unset, List[str]] = UNSET,
    authentication: Union[Unset, str] = UNSET,
    authentication_n: Union[Unset, str] = UNSET,
    client_max_body_size: Union[Unset, int] = UNSET,
    client_max_body_size_n: Union[Unset, int] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    extra_protocols: Union[Unset, str] = UNSET,
    gzip_proxied: Union[Unset, bool] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    keepalive_requests: Union[Unset, List[int]] = UNSET,
    keepalive_requests_empty: Union[Unset, bool] = UNSET,
    keepalive_requests_gt: Union[Unset, List[int]] = UNSET,
    keepalive_requests_gte: Union[Unset, List[int]] = UNSET,
    keepalive_requests_lt: Union[Unset, List[int]] = UNSET,
    keepalive_requests_lte: Union[Unset, List[int]] = UNSET,
    keepalive_requests_n: Union[Unset, List[int]] = UNSET,
    keepalive_timeout: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_empty: Union[Unset, bool] = UNSET,
    keepalive_timeout_gt: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_gte: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_lt: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_lte: Union[Unset, List[int]] = UNSET,
    keepalive_timeout_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    proxy_buffer_size: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_empty: Union[Unset, bool] = UNSET,
    proxy_buffer_size_gt: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_gte: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_lt: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_lte: Union[Unset, List[int]] = UNSET,
    proxy_buffer_size_n: Union[Unset, List[int]] = UNSET,
    proxy_cache: Union[Unset, bool] = UNSET,
    proxy_read_timeout: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_empty: Union[Unset, bool] = UNSET,
    proxy_read_timeout_gt: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_gte: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_lt: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_lte: Union[Unset, List[int]] = UNSET,
    proxy_read_timeout_n: Union[Unset, List[int]] = UNSET,
    q: Union[Unset, str] = UNSET,
    sorry_page: Union[Unset, List[str]] = UNSET,
    sorry_page_empty: Union[Unset, bool] = UNSET,
    sorry_page_ic: Union[Unset, List[str]] = UNSET,
    sorry_page_ie: Union[Unset, List[str]] = UNSET,
    sorry_page_iew: Union[Unset, List[str]] = UNSET,
    sorry_page_isw: Union[Unset, List[str]] = UNSET,
    sorry_page_n: Union[Unset, List[str]] = UNSET,
    sorry_page_nic: Union[Unset, List[str]] = UNSET,
    sorry_page_nie: Union[Unset, List[str]] = UNSET,
    sorry_page_niew: Union[Unset, List[str]] = UNSET,
    sorry_page_nisw: Union[Unset, List[str]] = UNSET,
    source: Union[Unset, List[str]] = UNSET,
    source_empty: Union[Unset, bool] = UNSET,
    source_ic: Union[Unset, List[str]] = UNSET,
    source_ie: Union[Unset, List[str]] = UNSET,
    source_iew: Union[Unset, List[str]] = UNSET,
    source_isw: Union[Unset, List[str]] = UNSET,
    source_n: Union[Unset, List[str]] = UNSET,
    source_nic: Union[Unset, List[str]] = UNSET,
    source_nie: Union[Unset, List[str]] = UNSET,
    source_niew: Union[Unset, List[str]] = UNSET,
    source_nisw: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    target: Union[Unset, List[str]] = UNSET,
    target_empty: Union[Unset, bool] = UNSET,
    target_ic: Union[Unset, List[str]] = UNSET,
    target_ie: Union[Unset, List[str]] = UNSET,
    target_iew: Union[Unset, List[str]] = UNSET,
    target_isw: Union[Unset, List[str]] = UNSET,
    target_n: Union[Unset, List[str]] = UNSET,
    target_nic: Union[Unset, List[str]] = UNSET,
    target_nie: Union[Unset, List[str]] = UNSET,
    target_niew: Union[Unset, List[str]] = UNSET,
    target_nisw: Union[Unset, List[str]] = UNSET,
    testingpage: Union[Unset, List[str]] = UNSET,
    testingpage_empty: Union[Unset, bool] = UNSET,
    testingpage_ic: Union[Unset, List[str]] = UNSET,
    testingpage_ie: Union[Unset, List[str]] = UNSET,
    testingpage_iew: Union[Unset, List[str]] = UNSET,
    testingpage_isw: Union[Unset, List[str]] = UNSET,
    testingpage_n: Union[Unset, List[str]] = UNSET,
    testingpage_nic: Union[Unset, List[str]] = UNSET,
    testingpage_nie: Union[Unset, List[str]] = UNSET,
    testingpage_niew: Union[Unset, List[str]] = UNSET,
    testingpage_nisw: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    webdav: Union[Unset, bool] = UNSET,
) -> Optional[PaginatedMappingList]:
    """Mapping view set class

    Args:
        comment (Union[Unset, List[str]]):
        comment_empty (Union[Unset, bool]):
        comment_ic (Union[Unset, List[str]]):
        comment_ie (Union[Unset, List[str]]):
        comment_iew (Union[Unset, List[str]]):
        comment_isw (Union[Unset, List[str]]):
        comment_n (Union[Unset, List[str]]):
        comment_nic (Union[Unset, List[str]]):
        comment_nie (Union[Unset, List[str]]):
        comment_niew (Union[Unset, List[str]]):
        comment_nisw (Union[Unset, List[str]]):
        authentication (Union[Unset, str]):
        authentication_n (Union[Unset, str]):
        client_max_body_size (Union[Unset, int]):
        client_max_body_size_n (Union[Unset, int]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        extra_protocols (Union[Unset, str]):
        gzip_proxied (Union[Unset, bool]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        keepalive_requests (Union[Unset, List[int]]):
        keepalive_requests_empty (Union[Unset, bool]):
        keepalive_requests_gt (Union[Unset, List[int]]):
        keepalive_requests_gte (Union[Unset, List[int]]):
        keepalive_requests_lt (Union[Unset, List[int]]):
        keepalive_requests_lte (Union[Unset, List[int]]):
        keepalive_requests_n (Union[Unset, List[int]]):
        keepalive_timeout (Union[Unset, List[int]]):
        keepalive_timeout_empty (Union[Unset, bool]):
        keepalive_timeout_gt (Union[Unset, List[int]]):
        keepalive_timeout_gte (Union[Unset, List[int]]):
        keepalive_timeout_lt (Union[Unset, List[int]]):
        keepalive_timeout_lte (Union[Unset, List[int]]):
        keepalive_timeout_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        proxy_buffer_size (Union[Unset, List[int]]):
        proxy_buffer_size_empty (Union[Unset, bool]):
        proxy_buffer_size_gt (Union[Unset, List[int]]):
        proxy_buffer_size_gte (Union[Unset, List[int]]):
        proxy_buffer_size_lt (Union[Unset, List[int]]):
        proxy_buffer_size_lte (Union[Unset, List[int]]):
        proxy_buffer_size_n (Union[Unset, List[int]]):
        proxy_cache (Union[Unset, bool]):
        proxy_read_timeout (Union[Unset, List[int]]):
        proxy_read_timeout_empty (Union[Unset, bool]):
        proxy_read_timeout_gt (Union[Unset, List[int]]):
        proxy_read_timeout_gte (Union[Unset, List[int]]):
        proxy_read_timeout_lt (Union[Unset, List[int]]):
        proxy_read_timeout_lte (Union[Unset, List[int]]):
        proxy_read_timeout_n (Union[Unset, List[int]]):
        q (Union[Unset, str]):
        sorry_page (Union[Unset, List[str]]):
        sorry_page_empty (Union[Unset, bool]):
        sorry_page_ic (Union[Unset, List[str]]):
        sorry_page_ie (Union[Unset, List[str]]):
        sorry_page_iew (Union[Unset, List[str]]):
        sorry_page_isw (Union[Unset, List[str]]):
        sorry_page_n (Union[Unset, List[str]]):
        sorry_page_nic (Union[Unset, List[str]]):
        sorry_page_nie (Union[Unset, List[str]]):
        sorry_page_niew (Union[Unset, List[str]]):
        sorry_page_nisw (Union[Unset, List[str]]):
        source (Union[Unset, List[str]]):
        source_empty (Union[Unset, bool]):
        source_ic (Union[Unset, List[str]]):
        source_ie (Union[Unset, List[str]]):
        source_iew (Union[Unset, List[str]]):
        source_isw (Union[Unset, List[str]]):
        source_n (Union[Unset, List[str]]):
        source_nic (Union[Unset, List[str]]):
        source_nie (Union[Unset, List[str]]):
        source_niew (Union[Unset, List[str]]):
        source_nisw (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        target (Union[Unset, List[str]]):
        target_empty (Union[Unset, bool]):
        target_ic (Union[Unset, List[str]]):
        target_ie (Union[Unset, List[str]]):
        target_iew (Union[Unset, List[str]]):
        target_isw (Union[Unset, List[str]]):
        target_n (Union[Unset, List[str]]):
        target_nic (Union[Unset, List[str]]):
        target_nie (Union[Unset, List[str]]):
        target_niew (Union[Unset, List[str]]):
        target_nisw (Union[Unset, List[str]]):
        testingpage (Union[Unset, List[str]]):
        testingpage_empty (Union[Unset, bool]):
        testingpage_ic (Union[Unset, List[str]]):
        testingpage_ie (Union[Unset, List[str]]):
        testingpage_iew (Union[Unset, List[str]]):
        testingpage_isw (Union[Unset, List[str]]):
        testingpage_n (Union[Unset, List[str]]):
        testingpage_nic (Union[Unset, List[str]]):
        testingpage_nie (Union[Unset, List[str]]):
        testingpage_niew (Union[Unset, List[str]]):
        testingpage_nisw (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        webdav (Union[Unset, bool]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedMappingList
    """

    return (
        await asyncio_detailed(
            client=client,
            comment=comment,
            comment_empty=comment_empty,
            comment_ic=comment_ic,
            comment_ie=comment_ie,
            comment_iew=comment_iew,
            comment_isw=comment_isw,
            comment_n=comment_n,
            comment_nic=comment_nic,
            comment_nie=comment_nie,
            comment_niew=comment_niew,
            comment_nisw=comment_nisw,
            authentication=authentication,
            authentication_n=authentication_n,
            client_max_body_size=client_max_body_size,
            client_max_body_size_n=client_max_body_size_n,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            extra_protocols=extra_protocols,
            gzip_proxied=gzip_proxied,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            keepalive_requests=keepalive_requests,
            keepalive_requests_empty=keepalive_requests_empty,
            keepalive_requests_gt=keepalive_requests_gt,
            keepalive_requests_gte=keepalive_requests_gte,
            keepalive_requests_lt=keepalive_requests_lt,
            keepalive_requests_lte=keepalive_requests_lte,
            keepalive_requests_n=keepalive_requests_n,
            keepalive_timeout=keepalive_timeout,
            keepalive_timeout_empty=keepalive_timeout_empty,
            keepalive_timeout_gt=keepalive_timeout_gt,
            keepalive_timeout_gte=keepalive_timeout_gte,
            keepalive_timeout_lt=keepalive_timeout_lt,
            keepalive_timeout_lte=keepalive_timeout_lte,
            keepalive_timeout_n=keepalive_timeout_n,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            limit=limit,
            modified_by_request=modified_by_request,
            offset=offset,
            ordering=ordering,
            proxy_buffer_size=proxy_buffer_size,
            proxy_buffer_size_empty=proxy_buffer_size_empty,
            proxy_buffer_size_gt=proxy_buffer_size_gt,
            proxy_buffer_size_gte=proxy_buffer_size_gte,
            proxy_buffer_size_lt=proxy_buffer_size_lt,
            proxy_buffer_size_lte=proxy_buffer_size_lte,
            proxy_buffer_size_n=proxy_buffer_size_n,
            proxy_cache=proxy_cache,
            proxy_read_timeout=proxy_read_timeout,
            proxy_read_timeout_empty=proxy_read_timeout_empty,
            proxy_read_timeout_gt=proxy_read_timeout_gt,
            proxy_read_timeout_gte=proxy_read_timeout_gte,
            proxy_read_timeout_lt=proxy_read_timeout_lt,
            proxy_read_timeout_lte=proxy_read_timeout_lte,
            proxy_read_timeout_n=proxy_read_timeout_n,
            q=q,
            sorry_page=sorry_page,
            sorry_page_empty=sorry_page_empty,
            sorry_page_ic=sorry_page_ic,
            sorry_page_ie=sorry_page_ie,
            sorry_page_iew=sorry_page_iew,
            sorry_page_isw=sorry_page_isw,
            sorry_page_n=sorry_page_n,
            sorry_page_nic=sorry_page_nic,
            sorry_page_nie=sorry_page_nie,
            sorry_page_niew=sorry_page_niew,
            sorry_page_nisw=sorry_page_nisw,
            source=source,
            source_empty=source_empty,
            source_ic=source_ic,
            source_ie=source_ie,
            source_iew=source_iew,
            source_isw=source_isw,
            source_n=source_n,
            source_nic=source_nic,
            source_nie=source_nie,
            source_niew=source_niew,
            source_nisw=source_nisw,
            tag=tag,
            tag_n=tag_n,
            target=target,
            target_empty=target_empty,
            target_ic=target_ic,
            target_ie=target_ie,
            target_iew=target_iew,
            target_isw=target_isw,
            target_n=target_n,
            target_nic=target_nic,
            target_nie=target_nie,
            target_niew=target_niew,
            target_nisw=target_nisw,
            testingpage=testingpage,
            testingpage_empty=testingpage_empty,
            testingpage_ic=testingpage_ic,
            testingpage_ie=testingpage_ie,
            testingpage_iew=testingpage_iew,
            testingpage_isw=testingpage_isw,
            testingpage_n=testingpage_n,
            testingpage_nic=testingpage_nic,
            testingpage_nie=testingpage_nie,
            testingpage_niew=testingpage_niew,
            testingpage_nisw=testingpage_nisw,
            updated_by_request=updated_by_request,
            webdav=webdav,
        )
    ).parsed
