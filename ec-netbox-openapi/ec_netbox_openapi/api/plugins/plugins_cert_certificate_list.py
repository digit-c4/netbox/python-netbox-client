import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_certificate_list import PaginatedCertificateList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    alt_name: Union[Unset, str] = UNSET,
    ca: Union[Unset, str] = UNSET,
    ca_n: Union[Unset, str] = UNSET,
    cert_created_at: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_empty: Union[Unset, bool] = UNSET,
    cert_created_at_gt: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_gte: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_lt: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_lte: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_n: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_empty: Union[Unset, bool] = UNSET,
    cert_expired_at_gt: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_gte: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_lt: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_lte: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_n: Union[Unset, List[datetime.date]] = UNSET,
    cn: Union[Unset, List[str]] = UNSET,
    cn_empty: Union[Unset, bool] = UNSET,
    cn_ic: Union[Unset, List[str]] = UNSET,
    cn_ie: Union[Unset, List[str]] = UNSET,
    cn_iew: Union[Unset, List[str]] = UNSET,
    cn_isw: Union[Unset, List[str]] = UNSET,
    cn_n: Union[Unset, List[str]] = UNSET,
    cn_nic: Union[Unset, List[str]] = UNSET,
    cn_nie: Union[Unset, List[str]] = UNSET,
    cn_niew: Union[Unset, List[str]] = UNSET,
    cn_nisw: Union[Unset, List[str]] = UNSET,
    content: Union[Unset, str] = UNSET,
    content_ic: Union[Unset, str] = UNSET,
    content_ie: Union[Unset, str] = UNSET,
    content_iew: Union[Unset, str] = UNSET,
    content_isw: Union[Unset, str] = UNSET,
    content_n: Union[Unset, str] = UNSET,
    content_nic: Union[Unset, str] = UNSET,
    content_nie: Union[Unset, str] = UNSET,
    content_niew: Union[Unset, str] = UNSET,
    content_nisw: Union[Unset, str] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    expiration_time: Union[Unset, str] = UNSET,
    expiration_time_n: Union[Unset, str] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    service: Union[None, Unset, str] = UNSET,
    service_n: Union[None, Unset, str] = UNSET,
    state: Union[Unset, List[str]] = UNSET,
    state_empty: Union[Unset, bool] = UNSET,
    state_ic: Union[Unset, List[str]] = UNSET,
    state_ie: Union[Unset, List[str]] = UNSET,
    state_iew: Union[Unset, List[str]] = UNSET,
    state_isw: Union[Unset, List[str]] = UNSET,
    state_n: Union[Unset, List[str]] = UNSET,
    state_nic: Union[Unset, List[str]] = UNSET,
    state_nie: Union[Unset, List[str]] = UNSET,
    state_niew: Union[Unset, List[str]] = UNSET,
    state_nisw: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    vault_url: Union[Unset, List[str]] = UNSET,
    vault_url_empty: Union[Unset, bool] = UNSET,
    vault_url_ic: Union[Unset, List[str]] = UNSET,
    vault_url_ie: Union[Unset, List[str]] = UNSET,
    vault_url_iew: Union[Unset, List[str]] = UNSET,
    vault_url_isw: Union[Unset, List[str]] = UNSET,
    vault_url_n: Union[Unset, List[str]] = UNSET,
    vault_url_nic: Union[Unset, List[str]] = UNSET,
    vault_url_nie: Union[Unset, List[str]] = UNSET,
    vault_url_niew: Union[Unset, List[str]] = UNSET,
    vault_url_nisw: Union[Unset, List[str]] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    params["alt_name"] = alt_name

    params["ca"] = ca

    params["ca__n"] = ca_n

    json_cert_created_at: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_created_at, Unset):
        json_cert_created_at = []
        for cert_created_at_item_data in cert_created_at:
            cert_created_at_item = cert_created_at_item_data.isoformat()
            json_cert_created_at.append(cert_created_at_item)

    params["cert_created_at"] = json_cert_created_at

    params["cert_created_at__empty"] = cert_created_at_empty

    json_cert_created_at_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_created_at_gt, Unset):
        json_cert_created_at_gt = []
        for cert_created_at_gt_item_data in cert_created_at_gt:
            cert_created_at_gt_item = cert_created_at_gt_item_data.isoformat()
            json_cert_created_at_gt.append(cert_created_at_gt_item)

    params["cert_created_at__gt"] = json_cert_created_at_gt

    json_cert_created_at_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_created_at_gte, Unset):
        json_cert_created_at_gte = []
        for cert_created_at_gte_item_data in cert_created_at_gte:
            cert_created_at_gte_item = cert_created_at_gte_item_data.isoformat()
            json_cert_created_at_gte.append(cert_created_at_gte_item)

    params["cert_created_at__gte"] = json_cert_created_at_gte

    json_cert_created_at_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_created_at_lt, Unset):
        json_cert_created_at_lt = []
        for cert_created_at_lt_item_data in cert_created_at_lt:
            cert_created_at_lt_item = cert_created_at_lt_item_data.isoformat()
            json_cert_created_at_lt.append(cert_created_at_lt_item)

    params["cert_created_at__lt"] = json_cert_created_at_lt

    json_cert_created_at_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_created_at_lte, Unset):
        json_cert_created_at_lte = []
        for cert_created_at_lte_item_data in cert_created_at_lte:
            cert_created_at_lte_item = cert_created_at_lte_item_data.isoformat()
            json_cert_created_at_lte.append(cert_created_at_lte_item)

    params["cert_created_at__lte"] = json_cert_created_at_lte

    json_cert_created_at_n: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_created_at_n, Unset):
        json_cert_created_at_n = []
        for cert_created_at_n_item_data in cert_created_at_n:
            cert_created_at_n_item = cert_created_at_n_item_data.isoformat()
            json_cert_created_at_n.append(cert_created_at_n_item)

    params["cert_created_at__n"] = json_cert_created_at_n

    json_cert_expired_at: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_expired_at, Unset):
        json_cert_expired_at = []
        for cert_expired_at_item_data in cert_expired_at:
            cert_expired_at_item = cert_expired_at_item_data.isoformat()
            json_cert_expired_at.append(cert_expired_at_item)

    params["cert_expired_at"] = json_cert_expired_at

    params["cert_expired_at__empty"] = cert_expired_at_empty

    json_cert_expired_at_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_expired_at_gt, Unset):
        json_cert_expired_at_gt = []
        for cert_expired_at_gt_item_data in cert_expired_at_gt:
            cert_expired_at_gt_item = cert_expired_at_gt_item_data.isoformat()
            json_cert_expired_at_gt.append(cert_expired_at_gt_item)

    params["cert_expired_at__gt"] = json_cert_expired_at_gt

    json_cert_expired_at_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_expired_at_gte, Unset):
        json_cert_expired_at_gte = []
        for cert_expired_at_gte_item_data in cert_expired_at_gte:
            cert_expired_at_gte_item = cert_expired_at_gte_item_data.isoformat()
            json_cert_expired_at_gte.append(cert_expired_at_gte_item)

    params["cert_expired_at__gte"] = json_cert_expired_at_gte

    json_cert_expired_at_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_expired_at_lt, Unset):
        json_cert_expired_at_lt = []
        for cert_expired_at_lt_item_data in cert_expired_at_lt:
            cert_expired_at_lt_item = cert_expired_at_lt_item_data.isoformat()
            json_cert_expired_at_lt.append(cert_expired_at_lt_item)

    params["cert_expired_at__lt"] = json_cert_expired_at_lt

    json_cert_expired_at_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_expired_at_lte, Unset):
        json_cert_expired_at_lte = []
        for cert_expired_at_lte_item_data in cert_expired_at_lte:
            cert_expired_at_lte_item = cert_expired_at_lte_item_data.isoformat()
            json_cert_expired_at_lte.append(cert_expired_at_lte_item)

    params["cert_expired_at__lte"] = json_cert_expired_at_lte

    json_cert_expired_at_n: Union[Unset, List[str]] = UNSET
    if not isinstance(cert_expired_at_n, Unset):
        json_cert_expired_at_n = []
        for cert_expired_at_n_item_data in cert_expired_at_n:
            cert_expired_at_n_item = cert_expired_at_n_item_data.isoformat()
            json_cert_expired_at_n.append(cert_expired_at_n_item)

    params["cert_expired_at__n"] = json_cert_expired_at_n

    json_cn: Union[Unset, List[str]] = UNSET
    if not isinstance(cn, Unset):
        json_cn = cn

    params["cn"] = json_cn

    params["cn__empty"] = cn_empty

    json_cn_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(cn_ic, Unset):
        json_cn_ic = cn_ic

    params["cn__ic"] = json_cn_ic

    json_cn_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(cn_ie, Unset):
        json_cn_ie = cn_ie

    params["cn__ie"] = json_cn_ie

    json_cn_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(cn_iew, Unset):
        json_cn_iew = cn_iew

    params["cn__iew"] = json_cn_iew

    json_cn_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(cn_isw, Unset):
        json_cn_isw = cn_isw

    params["cn__isw"] = json_cn_isw

    json_cn_n: Union[Unset, List[str]] = UNSET
    if not isinstance(cn_n, Unset):
        json_cn_n = cn_n

    params["cn__n"] = json_cn_n

    json_cn_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(cn_nic, Unset):
        json_cn_nic = cn_nic

    params["cn__nic"] = json_cn_nic

    json_cn_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(cn_nie, Unset):
        json_cn_nie = cn_nie

    params["cn__nie"] = json_cn_nie

    json_cn_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(cn_niew, Unset):
        json_cn_niew = cn_niew

    params["cn__niew"] = json_cn_niew

    json_cn_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(cn_nisw, Unset):
        json_cn_nisw = cn_nisw

    params["cn__nisw"] = json_cn_nisw

    params["content"] = content

    params["content__ic"] = content_ic

    params["content__ie"] = content_ie

    params["content__iew"] = content_iew

    params["content__isw"] = content_isw

    params["content__n"] = content_n

    params["content__nic"] = content_nic

    params["content__nie"] = content_nie

    params["content__niew"] = content_niew

    params["content__nisw"] = content_nisw

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    params["expiration_time"] = expiration_time

    params["expiration_time__n"] = expiration_time_n

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    params["limit"] = limit

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    params["offset"] = offset

    params["ordering"] = ordering

    params["q"] = q

    json_service: Union[None, Unset, str]
    if isinstance(service, Unset):
        json_service = UNSET
    else:
        json_service = service
    params["service"] = json_service

    json_service_n: Union[None, Unset, str]
    if isinstance(service_n, Unset):
        json_service_n = UNSET
    else:
        json_service_n = service_n
    params["service__n"] = json_service_n

    json_state: Union[Unset, List[str]] = UNSET
    if not isinstance(state, Unset):
        json_state = state

    params["state"] = json_state

    params["state__empty"] = state_empty

    json_state_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(state_ic, Unset):
        json_state_ic = state_ic

    params["state__ic"] = json_state_ic

    json_state_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(state_ie, Unset):
        json_state_ie = state_ie

    params["state__ie"] = json_state_ie

    json_state_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(state_iew, Unset):
        json_state_iew = state_iew

    params["state__iew"] = json_state_iew

    json_state_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(state_isw, Unset):
        json_state_isw = state_isw

    params["state__isw"] = json_state_isw

    json_state_n: Union[Unset, List[str]] = UNSET
    if not isinstance(state_n, Unset):
        json_state_n = state_n

    params["state__n"] = json_state_n

    json_state_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(state_nic, Unset):
        json_state_nic = state_nic

    params["state__nic"] = json_state_nic

    json_state_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(state_nie, Unset):
        json_state_nie = state_nie

    params["state__nie"] = json_state_nie

    json_state_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(state_niew, Unset):
        json_state_niew = state_niew

    params["state__niew"] = json_state_niew

    json_state_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(state_nisw, Unset):
        json_state_nisw = state_nisw

    params["state__nisw"] = json_state_nisw

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    json_vault_url: Union[Unset, List[str]] = UNSET
    if not isinstance(vault_url, Unset):
        json_vault_url = vault_url

    params["vault_url"] = json_vault_url

    params["vault_url__empty"] = vault_url_empty

    json_vault_url_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(vault_url_ic, Unset):
        json_vault_url_ic = vault_url_ic

    params["vault_url__ic"] = json_vault_url_ic

    json_vault_url_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(vault_url_ie, Unset):
        json_vault_url_ie = vault_url_ie

    params["vault_url__ie"] = json_vault_url_ie

    json_vault_url_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(vault_url_iew, Unset):
        json_vault_url_iew = vault_url_iew

    params["vault_url__iew"] = json_vault_url_iew

    json_vault_url_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(vault_url_isw, Unset):
        json_vault_url_isw = vault_url_isw

    params["vault_url__isw"] = json_vault_url_isw

    json_vault_url_n: Union[Unset, List[str]] = UNSET
    if not isinstance(vault_url_n, Unset):
        json_vault_url_n = vault_url_n

    params["vault_url__n"] = json_vault_url_n

    json_vault_url_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(vault_url_nic, Unset):
        json_vault_url_nic = vault_url_nic

    params["vault_url__nic"] = json_vault_url_nic

    json_vault_url_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(vault_url_nie, Unset):
        json_vault_url_nie = vault_url_nie

    params["vault_url__nie"] = json_vault_url_nie

    json_vault_url_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(vault_url_niew, Unset):
        json_vault_url_niew = vault_url_niew

    params["vault_url__niew"] = json_vault_url_niew

    json_vault_url_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(vault_url_nisw, Unset):
        json_vault_url_nisw = vault_url_nisw

    params["vault_url__nisw"] = json_vault_url_nisw

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/plugins/cert/certificate/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedCertificateList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedCertificateList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedCertificateList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    alt_name: Union[Unset, str] = UNSET,
    ca: Union[Unset, str] = UNSET,
    ca_n: Union[Unset, str] = UNSET,
    cert_created_at: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_empty: Union[Unset, bool] = UNSET,
    cert_created_at_gt: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_gte: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_lt: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_lte: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_n: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_empty: Union[Unset, bool] = UNSET,
    cert_expired_at_gt: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_gte: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_lt: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_lte: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_n: Union[Unset, List[datetime.date]] = UNSET,
    cn: Union[Unset, List[str]] = UNSET,
    cn_empty: Union[Unset, bool] = UNSET,
    cn_ic: Union[Unset, List[str]] = UNSET,
    cn_ie: Union[Unset, List[str]] = UNSET,
    cn_iew: Union[Unset, List[str]] = UNSET,
    cn_isw: Union[Unset, List[str]] = UNSET,
    cn_n: Union[Unset, List[str]] = UNSET,
    cn_nic: Union[Unset, List[str]] = UNSET,
    cn_nie: Union[Unset, List[str]] = UNSET,
    cn_niew: Union[Unset, List[str]] = UNSET,
    cn_nisw: Union[Unset, List[str]] = UNSET,
    content: Union[Unset, str] = UNSET,
    content_ic: Union[Unset, str] = UNSET,
    content_ie: Union[Unset, str] = UNSET,
    content_iew: Union[Unset, str] = UNSET,
    content_isw: Union[Unset, str] = UNSET,
    content_n: Union[Unset, str] = UNSET,
    content_nic: Union[Unset, str] = UNSET,
    content_nie: Union[Unset, str] = UNSET,
    content_niew: Union[Unset, str] = UNSET,
    content_nisw: Union[Unset, str] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    expiration_time: Union[Unset, str] = UNSET,
    expiration_time_n: Union[Unset, str] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    service: Union[None, Unset, str] = UNSET,
    service_n: Union[None, Unset, str] = UNSET,
    state: Union[Unset, List[str]] = UNSET,
    state_empty: Union[Unset, bool] = UNSET,
    state_ic: Union[Unset, List[str]] = UNSET,
    state_ie: Union[Unset, List[str]] = UNSET,
    state_iew: Union[Unset, List[str]] = UNSET,
    state_isw: Union[Unset, List[str]] = UNSET,
    state_n: Union[Unset, List[str]] = UNSET,
    state_nic: Union[Unset, List[str]] = UNSET,
    state_nie: Union[Unset, List[str]] = UNSET,
    state_niew: Union[Unset, List[str]] = UNSET,
    state_nisw: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    vault_url: Union[Unset, List[str]] = UNSET,
    vault_url_empty: Union[Unset, bool] = UNSET,
    vault_url_ic: Union[Unset, List[str]] = UNSET,
    vault_url_ie: Union[Unset, List[str]] = UNSET,
    vault_url_iew: Union[Unset, List[str]] = UNSET,
    vault_url_isw: Union[Unset, List[str]] = UNSET,
    vault_url_n: Union[Unset, List[str]] = UNSET,
    vault_url_nic: Union[Unset, List[str]] = UNSET,
    vault_url_nie: Union[Unset, List[str]] = UNSET,
    vault_url_niew: Union[Unset, List[str]] = UNSET,
    vault_url_nisw: Union[Unset, List[str]] = UNSET,
) -> Response[PaginatedCertificateList]:
    """Mapping view set class

    Args:
        alt_name (Union[Unset, str]):
        ca (Union[Unset, str]):
        ca_n (Union[Unset, str]):
        cert_created_at (Union[Unset, List[datetime.date]]):
        cert_created_at_empty (Union[Unset, bool]):
        cert_created_at_gt (Union[Unset, List[datetime.date]]):
        cert_created_at_gte (Union[Unset, List[datetime.date]]):
        cert_created_at_lt (Union[Unset, List[datetime.date]]):
        cert_created_at_lte (Union[Unset, List[datetime.date]]):
        cert_created_at_n (Union[Unset, List[datetime.date]]):
        cert_expired_at (Union[Unset, List[datetime.date]]):
        cert_expired_at_empty (Union[Unset, bool]):
        cert_expired_at_gt (Union[Unset, List[datetime.date]]):
        cert_expired_at_gte (Union[Unset, List[datetime.date]]):
        cert_expired_at_lt (Union[Unset, List[datetime.date]]):
        cert_expired_at_lte (Union[Unset, List[datetime.date]]):
        cert_expired_at_n (Union[Unset, List[datetime.date]]):
        cn (Union[Unset, List[str]]):
        cn_empty (Union[Unset, bool]):
        cn_ic (Union[Unset, List[str]]):
        cn_ie (Union[Unset, List[str]]):
        cn_iew (Union[Unset, List[str]]):
        cn_isw (Union[Unset, List[str]]):
        cn_n (Union[Unset, List[str]]):
        cn_nic (Union[Unset, List[str]]):
        cn_nie (Union[Unset, List[str]]):
        cn_niew (Union[Unset, List[str]]):
        cn_nisw (Union[Unset, List[str]]):
        content (Union[Unset, str]):
        content_ic (Union[Unset, str]):
        content_ie (Union[Unset, str]):
        content_iew (Union[Unset, str]):
        content_isw (Union[Unset, str]):
        content_n (Union[Unset, str]):
        content_nic (Union[Unset, str]):
        content_nie (Union[Unset, str]):
        content_niew (Union[Unset, str]):
        content_nisw (Union[Unset, str]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        expiration_time (Union[Unset, str]):
        expiration_time_n (Union[Unset, str]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        service (Union[None, Unset, str]):
        service_n (Union[None, Unset, str]):
        state (Union[Unset, List[str]]):
        state_empty (Union[Unset, bool]):
        state_ic (Union[Unset, List[str]]):
        state_ie (Union[Unset, List[str]]):
        state_iew (Union[Unset, List[str]]):
        state_isw (Union[Unset, List[str]]):
        state_n (Union[Unset, List[str]]):
        state_nic (Union[Unset, List[str]]):
        state_nie (Union[Unset, List[str]]):
        state_niew (Union[Unset, List[str]]):
        state_nisw (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        vault_url (Union[Unset, List[str]]):
        vault_url_empty (Union[Unset, bool]):
        vault_url_ic (Union[Unset, List[str]]):
        vault_url_ie (Union[Unset, List[str]]):
        vault_url_iew (Union[Unset, List[str]]):
        vault_url_isw (Union[Unset, List[str]]):
        vault_url_n (Union[Unset, List[str]]):
        vault_url_nic (Union[Unset, List[str]]):
        vault_url_nie (Union[Unset, List[str]]):
        vault_url_niew (Union[Unset, List[str]]):
        vault_url_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCertificateList]
    """

    kwargs = _get_kwargs(
        alt_name=alt_name,
        ca=ca,
        ca_n=ca_n,
        cert_created_at=cert_created_at,
        cert_created_at_empty=cert_created_at_empty,
        cert_created_at_gt=cert_created_at_gt,
        cert_created_at_gte=cert_created_at_gte,
        cert_created_at_lt=cert_created_at_lt,
        cert_created_at_lte=cert_created_at_lte,
        cert_created_at_n=cert_created_at_n,
        cert_expired_at=cert_expired_at,
        cert_expired_at_empty=cert_expired_at_empty,
        cert_expired_at_gt=cert_expired_at_gt,
        cert_expired_at_gte=cert_expired_at_gte,
        cert_expired_at_lt=cert_expired_at_lt,
        cert_expired_at_lte=cert_expired_at_lte,
        cert_expired_at_n=cert_expired_at_n,
        cn=cn,
        cn_empty=cn_empty,
        cn_ic=cn_ic,
        cn_ie=cn_ie,
        cn_iew=cn_iew,
        cn_isw=cn_isw,
        cn_n=cn_n,
        cn_nic=cn_nic,
        cn_nie=cn_nie,
        cn_niew=cn_niew,
        cn_nisw=cn_nisw,
        content=content,
        content_ic=content_ic,
        content_ie=content_ie,
        content_iew=content_iew,
        content_isw=content_isw,
        content_n=content_n,
        content_nic=content_nic,
        content_nie=content_nie,
        content_niew=content_niew,
        content_nisw=content_nisw,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        expiration_time=expiration_time,
        expiration_time_n=expiration_time_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        service=service,
        service_n=service_n,
        state=state,
        state_empty=state_empty,
        state_ic=state_ic,
        state_ie=state_ie,
        state_iew=state_iew,
        state_isw=state_isw,
        state_n=state_n,
        state_nic=state_nic,
        state_nie=state_nie,
        state_niew=state_niew,
        state_nisw=state_nisw,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        vault_url=vault_url,
        vault_url_empty=vault_url_empty,
        vault_url_ic=vault_url_ic,
        vault_url_ie=vault_url_ie,
        vault_url_iew=vault_url_iew,
        vault_url_isw=vault_url_isw,
        vault_url_n=vault_url_n,
        vault_url_nic=vault_url_nic,
        vault_url_nie=vault_url_nie,
        vault_url_niew=vault_url_niew,
        vault_url_nisw=vault_url_nisw,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    alt_name: Union[Unset, str] = UNSET,
    ca: Union[Unset, str] = UNSET,
    ca_n: Union[Unset, str] = UNSET,
    cert_created_at: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_empty: Union[Unset, bool] = UNSET,
    cert_created_at_gt: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_gte: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_lt: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_lte: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_n: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_empty: Union[Unset, bool] = UNSET,
    cert_expired_at_gt: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_gte: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_lt: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_lte: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_n: Union[Unset, List[datetime.date]] = UNSET,
    cn: Union[Unset, List[str]] = UNSET,
    cn_empty: Union[Unset, bool] = UNSET,
    cn_ic: Union[Unset, List[str]] = UNSET,
    cn_ie: Union[Unset, List[str]] = UNSET,
    cn_iew: Union[Unset, List[str]] = UNSET,
    cn_isw: Union[Unset, List[str]] = UNSET,
    cn_n: Union[Unset, List[str]] = UNSET,
    cn_nic: Union[Unset, List[str]] = UNSET,
    cn_nie: Union[Unset, List[str]] = UNSET,
    cn_niew: Union[Unset, List[str]] = UNSET,
    cn_nisw: Union[Unset, List[str]] = UNSET,
    content: Union[Unset, str] = UNSET,
    content_ic: Union[Unset, str] = UNSET,
    content_ie: Union[Unset, str] = UNSET,
    content_iew: Union[Unset, str] = UNSET,
    content_isw: Union[Unset, str] = UNSET,
    content_n: Union[Unset, str] = UNSET,
    content_nic: Union[Unset, str] = UNSET,
    content_nie: Union[Unset, str] = UNSET,
    content_niew: Union[Unset, str] = UNSET,
    content_nisw: Union[Unset, str] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    expiration_time: Union[Unset, str] = UNSET,
    expiration_time_n: Union[Unset, str] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    service: Union[None, Unset, str] = UNSET,
    service_n: Union[None, Unset, str] = UNSET,
    state: Union[Unset, List[str]] = UNSET,
    state_empty: Union[Unset, bool] = UNSET,
    state_ic: Union[Unset, List[str]] = UNSET,
    state_ie: Union[Unset, List[str]] = UNSET,
    state_iew: Union[Unset, List[str]] = UNSET,
    state_isw: Union[Unset, List[str]] = UNSET,
    state_n: Union[Unset, List[str]] = UNSET,
    state_nic: Union[Unset, List[str]] = UNSET,
    state_nie: Union[Unset, List[str]] = UNSET,
    state_niew: Union[Unset, List[str]] = UNSET,
    state_nisw: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    vault_url: Union[Unset, List[str]] = UNSET,
    vault_url_empty: Union[Unset, bool] = UNSET,
    vault_url_ic: Union[Unset, List[str]] = UNSET,
    vault_url_ie: Union[Unset, List[str]] = UNSET,
    vault_url_iew: Union[Unset, List[str]] = UNSET,
    vault_url_isw: Union[Unset, List[str]] = UNSET,
    vault_url_n: Union[Unset, List[str]] = UNSET,
    vault_url_nic: Union[Unset, List[str]] = UNSET,
    vault_url_nie: Union[Unset, List[str]] = UNSET,
    vault_url_niew: Union[Unset, List[str]] = UNSET,
    vault_url_nisw: Union[Unset, List[str]] = UNSET,
) -> Optional[PaginatedCertificateList]:
    """Mapping view set class

    Args:
        alt_name (Union[Unset, str]):
        ca (Union[Unset, str]):
        ca_n (Union[Unset, str]):
        cert_created_at (Union[Unset, List[datetime.date]]):
        cert_created_at_empty (Union[Unset, bool]):
        cert_created_at_gt (Union[Unset, List[datetime.date]]):
        cert_created_at_gte (Union[Unset, List[datetime.date]]):
        cert_created_at_lt (Union[Unset, List[datetime.date]]):
        cert_created_at_lte (Union[Unset, List[datetime.date]]):
        cert_created_at_n (Union[Unset, List[datetime.date]]):
        cert_expired_at (Union[Unset, List[datetime.date]]):
        cert_expired_at_empty (Union[Unset, bool]):
        cert_expired_at_gt (Union[Unset, List[datetime.date]]):
        cert_expired_at_gte (Union[Unset, List[datetime.date]]):
        cert_expired_at_lt (Union[Unset, List[datetime.date]]):
        cert_expired_at_lte (Union[Unset, List[datetime.date]]):
        cert_expired_at_n (Union[Unset, List[datetime.date]]):
        cn (Union[Unset, List[str]]):
        cn_empty (Union[Unset, bool]):
        cn_ic (Union[Unset, List[str]]):
        cn_ie (Union[Unset, List[str]]):
        cn_iew (Union[Unset, List[str]]):
        cn_isw (Union[Unset, List[str]]):
        cn_n (Union[Unset, List[str]]):
        cn_nic (Union[Unset, List[str]]):
        cn_nie (Union[Unset, List[str]]):
        cn_niew (Union[Unset, List[str]]):
        cn_nisw (Union[Unset, List[str]]):
        content (Union[Unset, str]):
        content_ic (Union[Unset, str]):
        content_ie (Union[Unset, str]):
        content_iew (Union[Unset, str]):
        content_isw (Union[Unset, str]):
        content_n (Union[Unset, str]):
        content_nic (Union[Unset, str]):
        content_nie (Union[Unset, str]):
        content_niew (Union[Unset, str]):
        content_nisw (Union[Unset, str]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        expiration_time (Union[Unset, str]):
        expiration_time_n (Union[Unset, str]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        service (Union[None, Unset, str]):
        service_n (Union[None, Unset, str]):
        state (Union[Unset, List[str]]):
        state_empty (Union[Unset, bool]):
        state_ic (Union[Unset, List[str]]):
        state_ie (Union[Unset, List[str]]):
        state_iew (Union[Unset, List[str]]):
        state_isw (Union[Unset, List[str]]):
        state_n (Union[Unset, List[str]]):
        state_nic (Union[Unset, List[str]]):
        state_nie (Union[Unset, List[str]]):
        state_niew (Union[Unset, List[str]]):
        state_nisw (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        vault_url (Union[Unset, List[str]]):
        vault_url_empty (Union[Unset, bool]):
        vault_url_ic (Union[Unset, List[str]]):
        vault_url_ie (Union[Unset, List[str]]):
        vault_url_iew (Union[Unset, List[str]]):
        vault_url_isw (Union[Unset, List[str]]):
        vault_url_n (Union[Unset, List[str]]):
        vault_url_nic (Union[Unset, List[str]]):
        vault_url_nie (Union[Unset, List[str]]):
        vault_url_niew (Union[Unset, List[str]]):
        vault_url_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCertificateList
    """

    return sync_detailed(
        client=client,
        alt_name=alt_name,
        ca=ca,
        ca_n=ca_n,
        cert_created_at=cert_created_at,
        cert_created_at_empty=cert_created_at_empty,
        cert_created_at_gt=cert_created_at_gt,
        cert_created_at_gte=cert_created_at_gte,
        cert_created_at_lt=cert_created_at_lt,
        cert_created_at_lte=cert_created_at_lte,
        cert_created_at_n=cert_created_at_n,
        cert_expired_at=cert_expired_at,
        cert_expired_at_empty=cert_expired_at_empty,
        cert_expired_at_gt=cert_expired_at_gt,
        cert_expired_at_gte=cert_expired_at_gte,
        cert_expired_at_lt=cert_expired_at_lt,
        cert_expired_at_lte=cert_expired_at_lte,
        cert_expired_at_n=cert_expired_at_n,
        cn=cn,
        cn_empty=cn_empty,
        cn_ic=cn_ic,
        cn_ie=cn_ie,
        cn_iew=cn_iew,
        cn_isw=cn_isw,
        cn_n=cn_n,
        cn_nic=cn_nic,
        cn_nie=cn_nie,
        cn_niew=cn_niew,
        cn_nisw=cn_nisw,
        content=content,
        content_ic=content_ic,
        content_ie=content_ie,
        content_iew=content_iew,
        content_isw=content_isw,
        content_n=content_n,
        content_nic=content_nic,
        content_nie=content_nie,
        content_niew=content_niew,
        content_nisw=content_nisw,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        expiration_time=expiration_time,
        expiration_time_n=expiration_time_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        service=service,
        service_n=service_n,
        state=state,
        state_empty=state_empty,
        state_ic=state_ic,
        state_ie=state_ie,
        state_iew=state_iew,
        state_isw=state_isw,
        state_n=state_n,
        state_nic=state_nic,
        state_nie=state_nie,
        state_niew=state_niew,
        state_nisw=state_nisw,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        vault_url=vault_url,
        vault_url_empty=vault_url_empty,
        vault_url_ic=vault_url_ic,
        vault_url_ie=vault_url_ie,
        vault_url_iew=vault_url_iew,
        vault_url_isw=vault_url_isw,
        vault_url_n=vault_url_n,
        vault_url_nic=vault_url_nic,
        vault_url_nie=vault_url_nie,
        vault_url_niew=vault_url_niew,
        vault_url_nisw=vault_url_nisw,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    alt_name: Union[Unset, str] = UNSET,
    ca: Union[Unset, str] = UNSET,
    ca_n: Union[Unset, str] = UNSET,
    cert_created_at: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_empty: Union[Unset, bool] = UNSET,
    cert_created_at_gt: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_gte: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_lt: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_lte: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_n: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_empty: Union[Unset, bool] = UNSET,
    cert_expired_at_gt: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_gte: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_lt: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_lte: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_n: Union[Unset, List[datetime.date]] = UNSET,
    cn: Union[Unset, List[str]] = UNSET,
    cn_empty: Union[Unset, bool] = UNSET,
    cn_ic: Union[Unset, List[str]] = UNSET,
    cn_ie: Union[Unset, List[str]] = UNSET,
    cn_iew: Union[Unset, List[str]] = UNSET,
    cn_isw: Union[Unset, List[str]] = UNSET,
    cn_n: Union[Unset, List[str]] = UNSET,
    cn_nic: Union[Unset, List[str]] = UNSET,
    cn_nie: Union[Unset, List[str]] = UNSET,
    cn_niew: Union[Unset, List[str]] = UNSET,
    cn_nisw: Union[Unset, List[str]] = UNSET,
    content: Union[Unset, str] = UNSET,
    content_ic: Union[Unset, str] = UNSET,
    content_ie: Union[Unset, str] = UNSET,
    content_iew: Union[Unset, str] = UNSET,
    content_isw: Union[Unset, str] = UNSET,
    content_n: Union[Unset, str] = UNSET,
    content_nic: Union[Unset, str] = UNSET,
    content_nie: Union[Unset, str] = UNSET,
    content_niew: Union[Unset, str] = UNSET,
    content_nisw: Union[Unset, str] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    expiration_time: Union[Unset, str] = UNSET,
    expiration_time_n: Union[Unset, str] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    service: Union[None, Unset, str] = UNSET,
    service_n: Union[None, Unset, str] = UNSET,
    state: Union[Unset, List[str]] = UNSET,
    state_empty: Union[Unset, bool] = UNSET,
    state_ic: Union[Unset, List[str]] = UNSET,
    state_ie: Union[Unset, List[str]] = UNSET,
    state_iew: Union[Unset, List[str]] = UNSET,
    state_isw: Union[Unset, List[str]] = UNSET,
    state_n: Union[Unset, List[str]] = UNSET,
    state_nic: Union[Unset, List[str]] = UNSET,
    state_nie: Union[Unset, List[str]] = UNSET,
    state_niew: Union[Unset, List[str]] = UNSET,
    state_nisw: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    vault_url: Union[Unset, List[str]] = UNSET,
    vault_url_empty: Union[Unset, bool] = UNSET,
    vault_url_ic: Union[Unset, List[str]] = UNSET,
    vault_url_ie: Union[Unset, List[str]] = UNSET,
    vault_url_iew: Union[Unset, List[str]] = UNSET,
    vault_url_isw: Union[Unset, List[str]] = UNSET,
    vault_url_n: Union[Unset, List[str]] = UNSET,
    vault_url_nic: Union[Unset, List[str]] = UNSET,
    vault_url_nie: Union[Unset, List[str]] = UNSET,
    vault_url_niew: Union[Unset, List[str]] = UNSET,
    vault_url_nisw: Union[Unset, List[str]] = UNSET,
) -> Response[PaginatedCertificateList]:
    """Mapping view set class

    Args:
        alt_name (Union[Unset, str]):
        ca (Union[Unset, str]):
        ca_n (Union[Unset, str]):
        cert_created_at (Union[Unset, List[datetime.date]]):
        cert_created_at_empty (Union[Unset, bool]):
        cert_created_at_gt (Union[Unset, List[datetime.date]]):
        cert_created_at_gte (Union[Unset, List[datetime.date]]):
        cert_created_at_lt (Union[Unset, List[datetime.date]]):
        cert_created_at_lte (Union[Unset, List[datetime.date]]):
        cert_created_at_n (Union[Unset, List[datetime.date]]):
        cert_expired_at (Union[Unset, List[datetime.date]]):
        cert_expired_at_empty (Union[Unset, bool]):
        cert_expired_at_gt (Union[Unset, List[datetime.date]]):
        cert_expired_at_gte (Union[Unset, List[datetime.date]]):
        cert_expired_at_lt (Union[Unset, List[datetime.date]]):
        cert_expired_at_lte (Union[Unset, List[datetime.date]]):
        cert_expired_at_n (Union[Unset, List[datetime.date]]):
        cn (Union[Unset, List[str]]):
        cn_empty (Union[Unset, bool]):
        cn_ic (Union[Unset, List[str]]):
        cn_ie (Union[Unset, List[str]]):
        cn_iew (Union[Unset, List[str]]):
        cn_isw (Union[Unset, List[str]]):
        cn_n (Union[Unset, List[str]]):
        cn_nic (Union[Unset, List[str]]):
        cn_nie (Union[Unset, List[str]]):
        cn_niew (Union[Unset, List[str]]):
        cn_nisw (Union[Unset, List[str]]):
        content (Union[Unset, str]):
        content_ic (Union[Unset, str]):
        content_ie (Union[Unset, str]):
        content_iew (Union[Unset, str]):
        content_isw (Union[Unset, str]):
        content_n (Union[Unset, str]):
        content_nic (Union[Unset, str]):
        content_nie (Union[Unset, str]):
        content_niew (Union[Unset, str]):
        content_nisw (Union[Unset, str]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        expiration_time (Union[Unset, str]):
        expiration_time_n (Union[Unset, str]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        service (Union[None, Unset, str]):
        service_n (Union[None, Unset, str]):
        state (Union[Unset, List[str]]):
        state_empty (Union[Unset, bool]):
        state_ic (Union[Unset, List[str]]):
        state_ie (Union[Unset, List[str]]):
        state_iew (Union[Unset, List[str]]):
        state_isw (Union[Unset, List[str]]):
        state_n (Union[Unset, List[str]]):
        state_nic (Union[Unset, List[str]]):
        state_nie (Union[Unset, List[str]]):
        state_niew (Union[Unset, List[str]]):
        state_nisw (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        vault_url (Union[Unset, List[str]]):
        vault_url_empty (Union[Unset, bool]):
        vault_url_ic (Union[Unset, List[str]]):
        vault_url_ie (Union[Unset, List[str]]):
        vault_url_iew (Union[Unset, List[str]]):
        vault_url_isw (Union[Unset, List[str]]):
        vault_url_n (Union[Unset, List[str]]):
        vault_url_nic (Union[Unset, List[str]]):
        vault_url_nie (Union[Unset, List[str]]):
        vault_url_niew (Union[Unset, List[str]]):
        vault_url_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCertificateList]
    """

    kwargs = _get_kwargs(
        alt_name=alt_name,
        ca=ca,
        ca_n=ca_n,
        cert_created_at=cert_created_at,
        cert_created_at_empty=cert_created_at_empty,
        cert_created_at_gt=cert_created_at_gt,
        cert_created_at_gte=cert_created_at_gte,
        cert_created_at_lt=cert_created_at_lt,
        cert_created_at_lte=cert_created_at_lte,
        cert_created_at_n=cert_created_at_n,
        cert_expired_at=cert_expired_at,
        cert_expired_at_empty=cert_expired_at_empty,
        cert_expired_at_gt=cert_expired_at_gt,
        cert_expired_at_gte=cert_expired_at_gte,
        cert_expired_at_lt=cert_expired_at_lt,
        cert_expired_at_lte=cert_expired_at_lte,
        cert_expired_at_n=cert_expired_at_n,
        cn=cn,
        cn_empty=cn_empty,
        cn_ic=cn_ic,
        cn_ie=cn_ie,
        cn_iew=cn_iew,
        cn_isw=cn_isw,
        cn_n=cn_n,
        cn_nic=cn_nic,
        cn_nie=cn_nie,
        cn_niew=cn_niew,
        cn_nisw=cn_nisw,
        content=content,
        content_ic=content_ic,
        content_ie=content_ie,
        content_iew=content_iew,
        content_isw=content_isw,
        content_n=content_n,
        content_nic=content_nic,
        content_nie=content_nie,
        content_niew=content_niew,
        content_nisw=content_nisw,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        expiration_time=expiration_time,
        expiration_time_n=expiration_time_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        service=service,
        service_n=service_n,
        state=state,
        state_empty=state_empty,
        state_ic=state_ic,
        state_ie=state_ie,
        state_iew=state_iew,
        state_isw=state_isw,
        state_n=state_n,
        state_nic=state_nic,
        state_nie=state_nie,
        state_niew=state_niew,
        state_nisw=state_nisw,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        vault_url=vault_url,
        vault_url_empty=vault_url_empty,
        vault_url_ic=vault_url_ic,
        vault_url_ie=vault_url_ie,
        vault_url_iew=vault_url_iew,
        vault_url_isw=vault_url_isw,
        vault_url_n=vault_url_n,
        vault_url_nic=vault_url_nic,
        vault_url_nie=vault_url_nie,
        vault_url_niew=vault_url_niew,
        vault_url_nisw=vault_url_nisw,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    alt_name: Union[Unset, str] = UNSET,
    ca: Union[Unset, str] = UNSET,
    ca_n: Union[Unset, str] = UNSET,
    cert_created_at: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_empty: Union[Unset, bool] = UNSET,
    cert_created_at_gt: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_gte: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_lt: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_lte: Union[Unset, List[datetime.date]] = UNSET,
    cert_created_at_n: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_empty: Union[Unset, bool] = UNSET,
    cert_expired_at_gt: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_gte: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_lt: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_lte: Union[Unset, List[datetime.date]] = UNSET,
    cert_expired_at_n: Union[Unset, List[datetime.date]] = UNSET,
    cn: Union[Unset, List[str]] = UNSET,
    cn_empty: Union[Unset, bool] = UNSET,
    cn_ic: Union[Unset, List[str]] = UNSET,
    cn_ie: Union[Unset, List[str]] = UNSET,
    cn_iew: Union[Unset, List[str]] = UNSET,
    cn_isw: Union[Unset, List[str]] = UNSET,
    cn_n: Union[Unset, List[str]] = UNSET,
    cn_nic: Union[Unset, List[str]] = UNSET,
    cn_nie: Union[Unset, List[str]] = UNSET,
    cn_niew: Union[Unset, List[str]] = UNSET,
    cn_nisw: Union[Unset, List[str]] = UNSET,
    content: Union[Unset, str] = UNSET,
    content_ic: Union[Unset, str] = UNSET,
    content_ie: Union[Unset, str] = UNSET,
    content_iew: Union[Unset, str] = UNSET,
    content_isw: Union[Unset, str] = UNSET,
    content_n: Union[Unset, str] = UNSET,
    content_nic: Union[Unset, str] = UNSET,
    content_nie: Union[Unset, str] = UNSET,
    content_niew: Union[Unset, str] = UNSET,
    content_nisw: Union[Unset, str] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    expiration_time: Union[Unset, str] = UNSET,
    expiration_time_n: Union[Unset, str] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    service: Union[None, Unset, str] = UNSET,
    service_n: Union[None, Unset, str] = UNSET,
    state: Union[Unset, List[str]] = UNSET,
    state_empty: Union[Unset, bool] = UNSET,
    state_ic: Union[Unset, List[str]] = UNSET,
    state_ie: Union[Unset, List[str]] = UNSET,
    state_iew: Union[Unset, List[str]] = UNSET,
    state_isw: Union[Unset, List[str]] = UNSET,
    state_n: Union[Unset, List[str]] = UNSET,
    state_nic: Union[Unset, List[str]] = UNSET,
    state_nie: Union[Unset, List[str]] = UNSET,
    state_niew: Union[Unset, List[str]] = UNSET,
    state_nisw: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    vault_url: Union[Unset, List[str]] = UNSET,
    vault_url_empty: Union[Unset, bool] = UNSET,
    vault_url_ic: Union[Unset, List[str]] = UNSET,
    vault_url_ie: Union[Unset, List[str]] = UNSET,
    vault_url_iew: Union[Unset, List[str]] = UNSET,
    vault_url_isw: Union[Unset, List[str]] = UNSET,
    vault_url_n: Union[Unset, List[str]] = UNSET,
    vault_url_nic: Union[Unset, List[str]] = UNSET,
    vault_url_nie: Union[Unset, List[str]] = UNSET,
    vault_url_niew: Union[Unset, List[str]] = UNSET,
    vault_url_nisw: Union[Unset, List[str]] = UNSET,
) -> Optional[PaginatedCertificateList]:
    """Mapping view set class

    Args:
        alt_name (Union[Unset, str]):
        ca (Union[Unset, str]):
        ca_n (Union[Unset, str]):
        cert_created_at (Union[Unset, List[datetime.date]]):
        cert_created_at_empty (Union[Unset, bool]):
        cert_created_at_gt (Union[Unset, List[datetime.date]]):
        cert_created_at_gte (Union[Unset, List[datetime.date]]):
        cert_created_at_lt (Union[Unset, List[datetime.date]]):
        cert_created_at_lte (Union[Unset, List[datetime.date]]):
        cert_created_at_n (Union[Unset, List[datetime.date]]):
        cert_expired_at (Union[Unset, List[datetime.date]]):
        cert_expired_at_empty (Union[Unset, bool]):
        cert_expired_at_gt (Union[Unset, List[datetime.date]]):
        cert_expired_at_gte (Union[Unset, List[datetime.date]]):
        cert_expired_at_lt (Union[Unset, List[datetime.date]]):
        cert_expired_at_lte (Union[Unset, List[datetime.date]]):
        cert_expired_at_n (Union[Unset, List[datetime.date]]):
        cn (Union[Unset, List[str]]):
        cn_empty (Union[Unset, bool]):
        cn_ic (Union[Unset, List[str]]):
        cn_ie (Union[Unset, List[str]]):
        cn_iew (Union[Unset, List[str]]):
        cn_isw (Union[Unset, List[str]]):
        cn_n (Union[Unset, List[str]]):
        cn_nic (Union[Unset, List[str]]):
        cn_nie (Union[Unset, List[str]]):
        cn_niew (Union[Unset, List[str]]):
        cn_nisw (Union[Unset, List[str]]):
        content (Union[Unset, str]):
        content_ic (Union[Unset, str]):
        content_ie (Union[Unset, str]):
        content_iew (Union[Unset, str]):
        content_isw (Union[Unset, str]):
        content_n (Union[Unset, str]):
        content_nic (Union[Unset, str]):
        content_nie (Union[Unset, str]):
        content_niew (Union[Unset, str]):
        content_nisw (Union[Unset, str]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        expiration_time (Union[Unset, str]):
        expiration_time_n (Union[Unset, str]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        service (Union[None, Unset, str]):
        service_n (Union[None, Unset, str]):
        state (Union[Unset, List[str]]):
        state_empty (Union[Unset, bool]):
        state_ic (Union[Unset, List[str]]):
        state_ie (Union[Unset, List[str]]):
        state_iew (Union[Unset, List[str]]):
        state_isw (Union[Unset, List[str]]):
        state_n (Union[Unset, List[str]]):
        state_nic (Union[Unset, List[str]]):
        state_nie (Union[Unset, List[str]]):
        state_niew (Union[Unset, List[str]]):
        state_nisw (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        vault_url (Union[Unset, List[str]]):
        vault_url_empty (Union[Unset, bool]):
        vault_url_ic (Union[Unset, List[str]]):
        vault_url_ie (Union[Unset, List[str]]):
        vault_url_iew (Union[Unset, List[str]]):
        vault_url_isw (Union[Unset, List[str]]):
        vault_url_n (Union[Unset, List[str]]):
        vault_url_nic (Union[Unset, List[str]]):
        vault_url_nie (Union[Unset, List[str]]):
        vault_url_niew (Union[Unset, List[str]]):
        vault_url_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCertificateList
    """

    return (
        await asyncio_detailed(
            client=client,
            alt_name=alt_name,
            ca=ca,
            ca_n=ca_n,
            cert_created_at=cert_created_at,
            cert_created_at_empty=cert_created_at_empty,
            cert_created_at_gt=cert_created_at_gt,
            cert_created_at_gte=cert_created_at_gte,
            cert_created_at_lt=cert_created_at_lt,
            cert_created_at_lte=cert_created_at_lte,
            cert_created_at_n=cert_created_at_n,
            cert_expired_at=cert_expired_at,
            cert_expired_at_empty=cert_expired_at_empty,
            cert_expired_at_gt=cert_expired_at_gt,
            cert_expired_at_gte=cert_expired_at_gte,
            cert_expired_at_lt=cert_expired_at_lt,
            cert_expired_at_lte=cert_expired_at_lte,
            cert_expired_at_n=cert_expired_at_n,
            cn=cn,
            cn_empty=cn_empty,
            cn_ic=cn_ic,
            cn_ie=cn_ie,
            cn_iew=cn_iew,
            cn_isw=cn_isw,
            cn_n=cn_n,
            cn_nic=cn_nic,
            cn_nie=cn_nie,
            cn_niew=cn_niew,
            cn_nisw=cn_nisw,
            content=content,
            content_ic=content_ic,
            content_ie=content_ie,
            content_iew=content_iew,
            content_isw=content_isw,
            content_n=content_n,
            content_nic=content_nic,
            content_nie=content_nie,
            content_niew=content_niew,
            content_nisw=content_nisw,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            expiration_time=expiration_time,
            expiration_time_n=expiration_time_n,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            limit=limit,
            modified_by_request=modified_by_request,
            offset=offset,
            ordering=ordering,
            q=q,
            service=service,
            service_n=service_n,
            state=state,
            state_empty=state_empty,
            state_ic=state_ic,
            state_ie=state_ie,
            state_iew=state_iew,
            state_isw=state_isw,
            state_n=state_n,
            state_nic=state_nic,
            state_nie=state_nie,
            state_niew=state_niew,
            state_nisw=state_nisw,
            tag=tag,
            tag_n=tag_n,
            updated_by_request=updated_by_request,
            vault_url=vault_url,
            vault_url_empty=vault_url_empty,
            vault_url_ic=vault_url_ic,
            vault_url_ie=vault_url_ie,
            vault_url_iew=vault_url_iew,
            vault_url_isw=vault_url_isw,
            vault_url_n=vault_url_n,
            vault_url_nic=vault_url_nic,
            vault_url_nie=vault_url_nie,
            vault_url_niew=vault_url_niew,
            vault_url_nisw=vault_url_nisw,
        )
    ).parsed
