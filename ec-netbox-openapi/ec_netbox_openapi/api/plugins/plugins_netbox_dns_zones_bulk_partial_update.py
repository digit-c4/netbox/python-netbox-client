from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.zone import Zone
from ...models.zone_request import ZoneRequest
from ...types import Response


def _get_kwargs(
    *,
    body: Union[
        List["ZoneRequest"],
        List["ZoneRequest"],
    ],
    _request_content_type: Optional[str] = None,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}

    _kwargs: Dict[str, Any] = {
        "method": "patch",
        "url": "/api/plugins/netbox-dns/zones/",
    }

    if _request_content_type is None:
        _body = []
        for body_item_data in body:
            body_item = body_item_data.to_dict()
            _body.append(body_item)

        _kwargs["json"] = _body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "application/json":
        _json_body = []
        for body_item_data in body:
            body_item = body_item_data.to_dict()
            _json_body.append(body_item)

        _kwargs["json"] = _json_body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "multipart/form-data":
        _files_body = []
        for body_item_data in body:
            body_item = body_item_data.to_dict()
            _files_body.append(body_item)

        _kwargs["files"] = _files_body
        headers["Content-Type"] = "multipart/form-data; boundary=+++"

    else:
        raise ValueError("Content type application/json not supported by the server")

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[List["Zone"]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = []
        _response_200 = response.json()
        for response_200_item_data in _response_200:
            response_200_item = Zone.from_dict(response_200_item_data)

            response_200.append(response_200_item)

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[List["Zone"]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    body: Union[
        List["ZoneRequest"],
        List["ZoneRequest"],
    ],
    _request_content_type: Optional[str] = None,
) -> Response[List["Zone"]]:
    """Patch a list of zone objects.

    Args:
        body (List['ZoneRequest']):
        body (List['ZoneRequest']):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[List['Zone']]
    """

    kwargs = _get_kwargs(
        body=body,
        _request_content_type=_request_content_type,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    body: Union[
        List["ZoneRequest"],
        List["ZoneRequest"],
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[List["Zone"]]:
    """Patch a list of zone objects.

    Args:
        body (List['ZoneRequest']):
        body (List['ZoneRequest']):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        List['Zone']
    """

    return sync_detailed(
        client=client,
        body=body,
        _request_content_type=_request_content_type,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    body: Union[
        List["ZoneRequest"],
        List["ZoneRequest"],
    ],
    _request_content_type: Optional[str] = None,
) -> Response[List["Zone"]]:
    """Patch a list of zone objects.

    Args:
        body (List['ZoneRequest']):
        body (List['ZoneRequest']):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[List['Zone']]
    """

    kwargs = _get_kwargs(
        body=body,
        _request_content_type=_request_content_type,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    body: Union[
        List["ZoneRequest"],
        List["ZoneRequest"],
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[List["Zone"]]:
    """Patch a list of zone objects.

    Args:
        body (List['ZoneRequest']):
        body (List['ZoneRequest']):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        List['Zone']
    """

    return (
        await asyncio_detailed(
            client=client,
            body=body,
            _request_content_type=_request_content_type,
        )
    ).parsed
