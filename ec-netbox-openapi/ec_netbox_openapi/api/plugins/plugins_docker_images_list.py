import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_image_list import PaginatedImageList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    image_id: Union[Unset, List[str]] = UNSET,
    image_id_empty: Union[Unset, bool] = UNSET,
    image_id_ic: Union[Unset, List[str]] = UNSET,
    image_id_ie: Union[Unset, List[str]] = UNSET,
    image_id_iew: Union[Unset, List[str]] = UNSET,
    image_id_isw: Union[Unset, List[str]] = UNSET,
    image_id_n: Union[Unset, List[str]] = UNSET,
    image_id_nic: Union[Unset, List[str]] = UNSET,
    image_id_nie: Union[Unset, List[str]] = UNSET,
    image_id_niew: Union[Unset, List[str]] = UNSET,
    image_id_nisw: Union[Unset, List[str]] = UNSET,
    containers: Union[Unset, List[int]] = UNSET,
    containers_n: Union[Unset, List[int]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    host_id: Union[Unset, List[int]] = UNSET,
    host_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, str] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    registry_id: Union[Unset, List[int]] = UNSET,
    registry_id_n: Union[Unset, List[int]] = UNSET,
    size: Union[Unset, List[int]] = UNSET,
    size_empty: Union[Unset, bool] = UNSET,
    size_gt: Union[Unset, List[int]] = UNSET,
    size_gte: Union[Unset, List[int]] = UNSET,
    size_lt: Union[Unset, List[int]] = UNSET,
    size_lte: Union[Unset, List[int]] = UNSET,
    size_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    version: Union[Unset, List[str]] = UNSET,
    version_empty: Union[Unset, bool] = UNSET,
    version_ic: Union[Unset, List[str]] = UNSET,
    version_ie: Union[Unset, List[str]] = UNSET,
    version_iew: Union[Unset, List[str]] = UNSET,
    version_isw: Union[Unset, List[str]] = UNSET,
    version_n: Union[Unset, List[str]] = UNSET,
    version_nic: Union[Unset, List[str]] = UNSET,
    version_nie: Union[Unset, List[str]] = UNSET,
    version_niew: Union[Unset, List[str]] = UNSET,
    version_nisw: Union[Unset, List[str]] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_image_id: Union[Unset, List[str]] = UNSET
    if not isinstance(image_id, Unset):
        json_image_id = image_id

    params["ImageID"] = json_image_id

    params["ImageID__empty"] = image_id_empty

    json_image_id_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(image_id_ic, Unset):
        json_image_id_ic = image_id_ic

    params["ImageID__ic"] = json_image_id_ic

    json_image_id_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(image_id_ie, Unset):
        json_image_id_ie = image_id_ie

    params["ImageID__ie"] = json_image_id_ie

    json_image_id_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(image_id_iew, Unset):
        json_image_id_iew = image_id_iew

    params["ImageID__iew"] = json_image_id_iew

    json_image_id_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(image_id_isw, Unset):
        json_image_id_isw = image_id_isw

    params["ImageID__isw"] = json_image_id_isw

    json_image_id_n: Union[Unset, List[str]] = UNSET
    if not isinstance(image_id_n, Unset):
        json_image_id_n = image_id_n

    params["ImageID__n"] = json_image_id_n

    json_image_id_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(image_id_nic, Unset):
        json_image_id_nic = image_id_nic

    params["ImageID__nic"] = json_image_id_nic

    json_image_id_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(image_id_nie, Unset):
        json_image_id_nie = image_id_nie

    params["ImageID__nie"] = json_image_id_nie

    json_image_id_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(image_id_niew, Unset):
        json_image_id_niew = image_id_niew

    params["ImageID__niew"] = json_image_id_niew

    json_image_id_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(image_id_nisw, Unset):
        json_image_id_nisw = image_id_nisw

    params["ImageID__nisw"] = json_image_id_nisw

    json_containers: Union[Unset, List[int]] = UNSET
    if not isinstance(containers, Unset):
        json_containers = containers

    params["containers"] = json_containers

    json_containers_n: Union[Unset, List[int]] = UNSET
    if not isinstance(containers_n, Unset):
        json_containers_n = containers_n

    params["containers__n"] = json_containers_n

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    json_host_id: Union[Unset, List[int]] = UNSET
    if not isinstance(host_id, Unset):
        json_host_id = host_id

    params["host_id"] = json_host_id

    json_host_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(host_id_n, Unset):
        json_host_id_n = host_id_n

    params["host_id__n"] = json_host_id_n

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    params["limit"] = limit

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    params["name"] = name

    params["offset"] = offset

    params["ordering"] = ordering

    params["q"] = q

    json_registry_id: Union[Unset, List[int]] = UNSET
    if not isinstance(registry_id, Unset):
        json_registry_id = registry_id

    params["registry_id"] = json_registry_id

    json_registry_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(registry_id_n, Unset):
        json_registry_id_n = registry_id_n

    params["registry_id__n"] = json_registry_id_n

    json_size: Union[Unset, List[int]] = UNSET
    if not isinstance(size, Unset):
        json_size = size

    params["size"] = json_size

    params["size__empty"] = size_empty

    json_size_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(size_gt, Unset):
        json_size_gt = size_gt

    params["size__gt"] = json_size_gt

    json_size_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(size_gte, Unset):
        json_size_gte = size_gte

    params["size__gte"] = json_size_gte

    json_size_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(size_lt, Unset):
        json_size_lt = size_lt

    params["size__lt"] = json_size_lt

    json_size_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(size_lte, Unset):
        json_size_lte = size_lte

    params["size__lte"] = json_size_lte

    json_size_n: Union[Unset, List[int]] = UNSET
    if not isinstance(size_n, Unset):
        json_size_n = size_n

    params["size__n"] = json_size_n

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    json_version: Union[Unset, List[str]] = UNSET
    if not isinstance(version, Unset):
        json_version = version

    params["version"] = json_version

    params["version__empty"] = version_empty

    json_version_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(version_ic, Unset):
        json_version_ic = version_ic

    params["version__ic"] = json_version_ic

    json_version_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(version_ie, Unset):
        json_version_ie = version_ie

    params["version__ie"] = json_version_ie

    json_version_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(version_iew, Unset):
        json_version_iew = version_iew

    params["version__iew"] = json_version_iew

    json_version_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(version_isw, Unset):
        json_version_isw = version_isw

    params["version__isw"] = json_version_isw

    json_version_n: Union[Unset, List[str]] = UNSET
    if not isinstance(version_n, Unset):
        json_version_n = version_n

    params["version__n"] = json_version_n

    json_version_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(version_nic, Unset):
        json_version_nic = version_nic

    params["version__nic"] = json_version_nic

    json_version_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(version_nie, Unset):
        json_version_nie = version_nie

    params["version__nie"] = json_version_nie

    json_version_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(version_niew, Unset):
        json_version_niew = version_niew

    params["version__niew"] = json_version_niew

    json_version_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(version_nisw, Unset):
        json_version_nisw = version_nisw

    params["version__nisw"] = json_version_nisw

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/plugins/docker/images/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedImageList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedImageList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedImageList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    image_id: Union[Unset, List[str]] = UNSET,
    image_id_empty: Union[Unset, bool] = UNSET,
    image_id_ic: Union[Unset, List[str]] = UNSET,
    image_id_ie: Union[Unset, List[str]] = UNSET,
    image_id_iew: Union[Unset, List[str]] = UNSET,
    image_id_isw: Union[Unset, List[str]] = UNSET,
    image_id_n: Union[Unset, List[str]] = UNSET,
    image_id_nic: Union[Unset, List[str]] = UNSET,
    image_id_nie: Union[Unset, List[str]] = UNSET,
    image_id_niew: Union[Unset, List[str]] = UNSET,
    image_id_nisw: Union[Unset, List[str]] = UNSET,
    containers: Union[Unset, List[int]] = UNSET,
    containers_n: Union[Unset, List[int]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    host_id: Union[Unset, List[int]] = UNSET,
    host_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, str] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    registry_id: Union[Unset, List[int]] = UNSET,
    registry_id_n: Union[Unset, List[int]] = UNSET,
    size: Union[Unset, List[int]] = UNSET,
    size_empty: Union[Unset, bool] = UNSET,
    size_gt: Union[Unset, List[int]] = UNSET,
    size_gte: Union[Unset, List[int]] = UNSET,
    size_lt: Union[Unset, List[int]] = UNSET,
    size_lte: Union[Unset, List[int]] = UNSET,
    size_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    version: Union[Unset, List[str]] = UNSET,
    version_empty: Union[Unset, bool] = UNSET,
    version_ic: Union[Unset, List[str]] = UNSET,
    version_ie: Union[Unset, List[str]] = UNSET,
    version_iew: Union[Unset, List[str]] = UNSET,
    version_isw: Union[Unset, List[str]] = UNSET,
    version_n: Union[Unset, List[str]] = UNSET,
    version_nic: Union[Unset, List[str]] = UNSET,
    version_nie: Union[Unset, List[str]] = UNSET,
    version_niew: Union[Unset, List[str]] = UNSET,
    version_nisw: Union[Unset, List[str]] = UNSET,
) -> Response[PaginatedImageList]:
    """Image view set class

    Args:
        image_id (Union[Unset, List[str]]):
        image_id_empty (Union[Unset, bool]):
        image_id_ic (Union[Unset, List[str]]):
        image_id_ie (Union[Unset, List[str]]):
        image_id_iew (Union[Unset, List[str]]):
        image_id_isw (Union[Unset, List[str]]):
        image_id_n (Union[Unset, List[str]]):
        image_id_nic (Union[Unset, List[str]]):
        image_id_nie (Union[Unset, List[str]]):
        image_id_niew (Union[Unset, List[str]]):
        image_id_nisw (Union[Unset, List[str]]):
        containers (Union[Unset, List[int]]):
        containers_n (Union[Unset, List[int]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        host_id (Union[Unset, List[int]]):
        host_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, str]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        registry_id (Union[Unset, List[int]]):
        registry_id_n (Union[Unset, List[int]]):
        size (Union[Unset, List[int]]):
        size_empty (Union[Unset, bool]):
        size_gt (Union[Unset, List[int]]):
        size_gte (Union[Unset, List[int]]):
        size_lt (Union[Unset, List[int]]):
        size_lte (Union[Unset, List[int]]):
        size_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        version (Union[Unset, List[str]]):
        version_empty (Union[Unset, bool]):
        version_ic (Union[Unset, List[str]]):
        version_ie (Union[Unset, List[str]]):
        version_iew (Union[Unset, List[str]]):
        version_isw (Union[Unset, List[str]]):
        version_n (Union[Unset, List[str]]):
        version_nic (Union[Unset, List[str]]):
        version_nie (Union[Unset, List[str]]):
        version_niew (Union[Unset, List[str]]):
        version_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedImageList]
    """

    kwargs = _get_kwargs(
        image_id=image_id,
        image_id_empty=image_id_empty,
        image_id_ic=image_id_ic,
        image_id_ie=image_id_ie,
        image_id_iew=image_id_iew,
        image_id_isw=image_id_isw,
        image_id_n=image_id_n,
        image_id_nic=image_id_nic,
        image_id_nie=image_id_nie,
        image_id_niew=image_id_niew,
        image_id_nisw=image_id_nisw,
        containers=containers,
        containers_n=containers_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        host_id=host_id,
        host_id_n=host_id_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        name=name,
        offset=offset,
        ordering=ordering,
        q=q,
        registry_id=registry_id,
        registry_id_n=registry_id_n,
        size=size,
        size_empty=size_empty,
        size_gt=size_gt,
        size_gte=size_gte,
        size_lt=size_lt,
        size_lte=size_lte,
        size_n=size_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        version=version,
        version_empty=version_empty,
        version_ic=version_ic,
        version_ie=version_ie,
        version_iew=version_iew,
        version_isw=version_isw,
        version_n=version_n,
        version_nic=version_nic,
        version_nie=version_nie,
        version_niew=version_niew,
        version_nisw=version_nisw,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    image_id: Union[Unset, List[str]] = UNSET,
    image_id_empty: Union[Unset, bool] = UNSET,
    image_id_ic: Union[Unset, List[str]] = UNSET,
    image_id_ie: Union[Unset, List[str]] = UNSET,
    image_id_iew: Union[Unset, List[str]] = UNSET,
    image_id_isw: Union[Unset, List[str]] = UNSET,
    image_id_n: Union[Unset, List[str]] = UNSET,
    image_id_nic: Union[Unset, List[str]] = UNSET,
    image_id_nie: Union[Unset, List[str]] = UNSET,
    image_id_niew: Union[Unset, List[str]] = UNSET,
    image_id_nisw: Union[Unset, List[str]] = UNSET,
    containers: Union[Unset, List[int]] = UNSET,
    containers_n: Union[Unset, List[int]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    host_id: Union[Unset, List[int]] = UNSET,
    host_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, str] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    registry_id: Union[Unset, List[int]] = UNSET,
    registry_id_n: Union[Unset, List[int]] = UNSET,
    size: Union[Unset, List[int]] = UNSET,
    size_empty: Union[Unset, bool] = UNSET,
    size_gt: Union[Unset, List[int]] = UNSET,
    size_gte: Union[Unset, List[int]] = UNSET,
    size_lt: Union[Unset, List[int]] = UNSET,
    size_lte: Union[Unset, List[int]] = UNSET,
    size_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    version: Union[Unset, List[str]] = UNSET,
    version_empty: Union[Unset, bool] = UNSET,
    version_ic: Union[Unset, List[str]] = UNSET,
    version_ie: Union[Unset, List[str]] = UNSET,
    version_iew: Union[Unset, List[str]] = UNSET,
    version_isw: Union[Unset, List[str]] = UNSET,
    version_n: Union[Unset, List[str]] = UNSET,
    version_nic: Union[Unset, List[str]] = UNSET,
    version_nie: Union[Unset, List[str]] = UNSET,
    version_niew: Union[Unset, List[str]] = UNSET,
    version_nisw: Union[Unset, List[str]] = UNSET,
) -> Optional[PaginatedImageList]:
    """Image view set class

    Args:
        image_id (Union[Unset, List[str]]):
        image_id_empty (Union[Unset, bool]):
        image_id_ic (Union[Unset, List[str]]):
        image_id_ie (Union[Unset, List[str]]):
        image_id_iew (Union[Unset, List[str]]):
        image_id_isw (Union[Unset, List[str]]):
        image_id_n (Union[Unset, List[str]]):
        image_id_nic (Union[Unset, List[str]]):
        image_id_nie (Union[Unset, List[str]]):
        image_id_niew (Union[Unset, List[str]]):
        image_id_nisw (Union[Unset, List[str]]):
        containers (Union[Unset, List[int]]):
        containers_n (Union[Unset, List[int]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        host_id (Union[Unset, List[int]]):
        host_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, str]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        registry_id (Union[Unset, List[int]]):
        registry_id_n (Union[Unset, List[int]]):
        size (Union[Unset, List[int]]):
        size_empty (Union[Unset, bool]):
        size_gt (Union[Unset, List[int]]):
        size_gte (Union[Unset, List[int]]):
        size_lt (Union[Unset, List[int]]):
        size_lte (Union[Unset, List[int]]):
        size_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        version (Union[Unset, List[str]]):
        version_empty (Union[Unset, bool]):
        version_ic (Union[Unset, List[str]]):
        version_ie (Union[Unset, List[str]]):
        version_iew (Union[Unset, List[str]]):
        version_isw (Union[Unset, List[str]]):
        version_n (Union[Unset, List[str]]):
        version_nic (Union[Unset, List[str]]):
        version_nie (Union[Unset, List[str]]):
        version_niew (Union[Unset, List[str]]):
        version_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedImageList
    """

    return sync_detailed(
        client=client,
        image_id=image_id,
        image_id_empty=image_id_empty,
        image_id_ic=image_id_ic,
        image_id_ie=image_id_ie,
        image_id_iew=image_id_iew,
        image_id_isw=image_id_isw,
        image_id_n=image_id_n,
        image_id_nic=image_id_nic,
        image_id_nie=image_id_nie,
        image_id_niew=image_id_niew,
        image_id_nisw=image_id_nisw,
        containers=containers,
        containers_n=containers_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        host_id=host_id,
        host_id_n=host_id_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        name=name,
        offset=offset,
        ordering=ordering,
        q=q,
        registry_id=registry_id,
        registry_id_n=registry_id_n,
        size=size,
        size_empty=size_empty,
        size_gt=size_gt,
        size_gte=size_gte,
        size_lt=size_lt,
        size_lte=size_lte,
        size_n=size_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        version=version,
        version_empty=version_empty,
        version_ic=version_ic,
        version_ie=version_ie,
        version_iew=version_iew,
        version_isw=version_isw,
        version_n=version_n,
        version_nic=version_nic,
        version_nie=version_nie,
        version_niew=version_niew,
        version_nisw=version_nisw,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    image_id: Union[Unset, List[str]] = UNSET,
    image_id_empty: Union[Unset, bool] = UNSET,
    image_id_ic: Union[Unset, List[str]] = UNSET,
    image_id_ie: Union[Unset, List[str]] = UNSET,
    image_id_iew: Union[Unset, List[str]] = UNSET,
    image_id_isw: Union[Unset, List[str]] = UNSET,
    image_id_n: Union[Unset, List[str]] = UNSET,
    image_id_nic: Union[Unset, List[str]] = UNSET,
    image_id_nie: Union[Unset, List[str]] = UNSET,
    image_id_niew: Union[Unset, List[str]] = UNSET,
    image_id_nisw: Union[Unset, List[str]] = UNSET,
    containers: Union[Unset, List[int]] = UNSET,
    containers_n: Union[Unset, List[int]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    host_id: Union[Unset, List[int]] = UNSET,
    host_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, str] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    registry_id: Union[Unset, List[int]] = UNSET,
    registry_id_n: Union[Unset, List[int]] = UNSET,
    size: Union[Unset, List[int]] = UNSET,
    size_empty: Union[Unset, bool] = UNSET,
    size_gt: Union[Unset, List[int]] = UNSET,
    size_gte: Union[Unset, List[int]] = UNSET,
    size_lt: Union[Unset, List[int]] = UNSET,
    size_lte: Union[Unset, List[int]] = UNSET,
    size_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    version: Union[Unset, List[str]] = UNSET,
    version_empty: Union[Unset, bool] = UNSET,
    version_ic: Union[Unset, List[str]] = UNSET,
    version_ie: Union[Unset, List[str]] = UNSET,
    version_iew: Union[Unset, List[str]] = UNSET,
    version_isw: Union[Unset, List[str]] = UNSET,
    version_n: Union[Unset, List[str]] = UNSET,
    version_nic: Union[Unset, List[str]] = UNSET,
    version_nie: Union[Unset, List[str]] = UNSET,
    version_niew: Union[Unset, List[str]] = UNSET,
    version_nisw: Union[Unset, List[str]] = UNSET,
) -> Response[PaginatedImageList]:
    """Image view set class

    Args:
        image_id (Union[Unset, List[str]]):
        image_id_empty (Union[Unset, bool]):
        image_id_ic (Union[Unset, List[str]]):
        image_id_ie (Union[Unset, List[str]]):
        image_id_iew (Union[Unset, List[str]]):
        image_id_isw (Union[Unset, List[str]]):
        image_id_n (Union[Unset, List[str]]):
        image_id_nic (Union[Unset, List[str]]):
        image_id_nie (Union[Unset, List[str]]):
        image_id_niew (Union[Unset, List[str]]):
        image_id_nisw (Union[Unset, List[str]]):
        containers (Union[Unset, List[int]]):
        containers_n (Union[Unset, List[int]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        host_id (Union[Unset, List[int]]):
        host_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, str]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        registry_id (Union[Unset, List[int]]):
        registry_id_n (Union[Unset, List[int]]):
        size (Union[Unset, List[int]]):
        size_empty (Union[Unset, bool]):
        size_gt (Union[Unset, List[int]]):
        size_gte (Union[Unset, List[int]]):
        size_lt (Union[Unset, List[int]]):
        size_lte (Union[Unset, List[int]]):
        size_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        version (Union[Unset, List[str]]):
        version_empty (Union[Unset, bool]):
        version_ic (Union[Unset, List[str]]):
        version_ie (Union[Unset, List[str]]):
        version_iew (Union[Unset, List[str]]):
        version_isw (Union[Unset, List[str]]):
        version_n (Union[Unset, List[str]]):
        version_nic (Union[Unset, List[str]]):
        version_nie (Union[Unset, List[str]]):
        version_niew (Union[Unset, List[str]]):
        version_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedImageList]
    """

    kwargs = _get_kwargs(
        image_id=image_id,
        image_id_empty=image_id_empty,
        image_id_ic=image_id_ic,
        image_id_ie=image_id_ie,
        image_id_iew=image_id_iew,
        image_id_isw=image_id_isw,
        image_id_n=image_id_n,
        image_id_nic=image_id_nic,
        image_id_nie=image_id_nie,
        image_id_niew=image_id_niew,
        image_id_nisw=image_id_nisw,
        containers=containers,
        containers_n=containers_n,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        host_id=host_id,
        host_id_n=host_id_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        modified_by_request=modified_by_request,
        name=name,
        offset=offset,
        ordering=ordering,
        q=q,
        registry_id=registry_id,
        registry_id_n=registry_id_n,
        size=size,
        size_empty=size_empty,
        size_gt=size_gt,
        size_gte=size_gte,
        size_lt=size_lt,
        size_lte=size_lte,
        size_n=size_n,
        tag=tag,
        tag_n=tag_n,
        updated_by_request=updated_by_request,
        version=version,
        version_empty=version_empty,
        version_ic=version_ic,
        version_ie=version_ie,
        version_iew=version_iew,
        version_isw=version_isw,
        version_n=version_n,
        version_nic=version_nic,
        version_nie=version_nie,
        version_niew=version_niew,
        version_nisw=version_nisw,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    image_id: Union[Unset, List[str]] = UNSET,
    image_id_empty: Union[Unset, bool] = UNSET,
    image_id_ic: Union[Unset, List[str]] = UNSET,
    image_id_ie: Union[Unset, List[str]] = UNSET,
    image_id_iew: Union[Unset, List[str]] = UNSET,
    image_id_isw: Union[Unset, List[str]] = UNSET,
    image_id_n: Union[Unset, List[str]] = UNSET,
    image_id_nic: Union[Unset, List[str]] = UNSET,
    image_id_nie: Union[Unset, List[str]] = UNSET,
    image_id_niew: Union[Unset, List[str]] = UNSET,
    image_id_nisw: Union[Unset, List[str]] = UNSET,
    containers: Union[Unset, List[int]] = UNSET,
    containers_n: Union[Unset, List[int]] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    host_id: Union[Unset, List[int]] = UNSET,
    host_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    name: Union[Unset, str] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    registry_id: Union[Unset, List[int]] = UNSET,
    registry_id_n: Union[Unset, List[int]] = UNSET,
    size: Union[Unset, List[int]] = UNSET,
    size_empty: Union[Unset, bool] = UNSET,
    size_gt: Union[Unset, List[int]] = UNSET,
    size_gte: Union[Unset, List[int]] = UNSET,
    size_lt: Union[Unset, List[int]] = UNSET,
    size_lte: Union[Unset, List[int]] = UNSET,
    size_n: Union[Unset, List[int]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    version: Union[Unset, List[str]] = UNSET,
    version_empty: Union[Unset, bool] = UNSET,
    version_ic: Union[Unset, List[str]] = UNSET,
    version_ie: Union[Unset, List[str]] = UNSET,
    version_iew: Union[Unset, List[str]] = UNSET,
    version_isw: Union[Unset, List[str]] = UNSET,
    version_n: Union[Unset, List[str]] = UNSET,
    version_nic: Union[Unset, List[str]] = UNSET,
    version_nie: Union[Unset, List[str]] = UNSET,
    version_niew: Union[Unset, List[str]] = UNSET,
    version_nisw: Union[Unset, List[str]] = UNSET,
) -> Optional[PaginatedImageList]:
    """Image view set class

    Args:
        image_id (Union[Unset, List[str]]):
        image_id_empty (Union[Unset, bool]):
        image_id_ic (Union[Unset, List[str]]):
        image_id_ie (Union[Unset, List[str]]):
        image_id_iew (Union[Unset, List[str]]):
        image_id_isw (Union[Unset, List[str]]):
        image_id_n (Union[Unset, List[str]]):
        image_id_nic (Union[Unset, List[str]]):
        image_id_nie (Union[Unset, List[str]]):
        image_id_niew (Union[Unset, List[str]]):
        image_id_nisw (Union[Unset, List[str]]):
        containers (Union[Unset, List[int]]):
        containers_n (Union[Unset, List[int]]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        host_id (Union[Unset, List[int]]):
        host_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        modified_by_request (Union[Unset, UUID]):
        name (Union[Unset, str]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        registry_id (Union[Unset, List[int]]):
        registry_id_n (Union[Unset, List[int]]):
        size (Union[Unset, List[int]]):
        size_empty (Union[Unset, bool]):
        size_gt (Union[Unset, List[int]]):
        size_gte (Union[Unset, List[int]]):
        size_lt (Union[Unset, List[int]]):
        size_lte (Union[Unset, List[int]]):
        size_n (Union[Unset, List[int]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        updated_by_request (Union[Unset, UUID]):
        version (Union[Unset, List[str]]):
        version_empty (Union[Unset, bool]):
        version_ic (Union[Unset, List[str]]):
        version_ie (Union[Unset, List[str]]):
        version_iew (Union[Unset, List[str]]):
        version_isw (Union[Unset, List[str]]):
        version_n (Union[Unset, List[str]]):
        version_nic (Union[Unset, List[str]]):
        version_nie (Union[Unset, List[str]]):
        version_niew (Union[Unset, List[str]]):
        version_nisw (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedImageList
    """

    return (
        await asyncio_detailed(
            client=client,
            image_id=image_id,
            image_id_empty=image_id_empty,
            image_id_ic=image_id_ic,
            image_id_ie=image_id_ie,
            image_id_iew=image_id_iew,
            image_id_isw=image_id_isw,
            image_id_n=image_id_n,
            image_id_nic=image_id_nic,
            image_id_nie=image_id_nie,
            image_id_niew=image_id_niew,
            image_id_nisw=image_id_nisw,
            containers=containers,
            containers_n=containers_n,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            host_id=host_id,
            host_id_n=host_id_n,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            limit=limit,
            modified_by_request=modified_by_request,
            name=name,
            offset=offset,
            ordering=ordering,
            q=q,
            registry_id=registry_id,
            registry_id_n=registry_id_n,
            size=size,
            size_empty=size_empty,
            size_gt=size_gt,
            size_gte=size_gte,
            size_lt=size_lt,
            size_lte=size_lte,
            size_n=size_n,
            tag=tag,
            tag_n=tag_n,
            updated_by_request=updated_by_request,
            version=version,
            version_empty=version_empty,
            version_ic=version_ic,
            version_ie=version_ie,
            version_iew=version_iew,
            version_isw=version_isw,
            version_n=version_n,
            version_nic=version_nic,
            version_nie=version_nie,
            version_niew=version_niew,
            version_nisw=version_nisw,
        )
    ).parsed
