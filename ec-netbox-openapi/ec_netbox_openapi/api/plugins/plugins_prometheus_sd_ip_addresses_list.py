import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.prometheus_ip_address import PrometheusIPAddress
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    address: Union[Unset, List[str]] = UNSET,
    assigned: Union[Unset, bool] = UNSET,
    assigned_to_interface: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[str]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    dns_name: Union[Unset, List[str]] = UNSET,
    dns_name_empty: Union[Unset, bool] = UNSET,
    dns_name_ic: Union[Unset, List[str]] = UNSET,
    dns_name_ie: Union[Unset, List[str]] = UNSET,
    dns_name_iew: Union[Unset, List[str]] = UNSET,
    dns_name_isw: Union[Unset, List[str]] = UNSET,
    dns_name_n: Union[Unset, List[str]] = UNSET,
    dns_name_nic: Union[Unset, List[str]] = UNSET,
    dns_name_nie: Union[Unset, List[str]] = UNSET,
    dns_name_niew: Union[Unset, List[str]] = UNSET,
    dns_name_nisw: Union[Unset, List[str]] = UNSET,
    family: Union[Unset, float] = UNSET,
    fhrpgroup_id: Union[Unset, List[int]] = UNSET,
    fhrpgroup_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    interface: Union[Unset, List[str]] = UNSET,
    interface_n: Union[Unset, List[str]] = UNSET,
    interface_id: Union[Unset, List[int]] = UNSET,
    interface_id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    mask_length: Union[Unset, List[int]] = UNSET,
    mask_length_gte: Union[Unset, float] = UNSET,
    mask_length_lte: Union[Unset, float] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent: Union[Unset, List[str]] = UNSET,
    present_in_vrf: Union[Unset, str] = UNSET,
    present_in_vrf_id: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_machine: Union[Unset, List[str]] = UNSET,
    virtual_machine_id: Union[Unset, List[int]] = UNSET,
    vminterface: Union[Unset, List[str]] = UNSET,
    vminterface_n: Union[Unset, List[str]] = UNSET,
    vminterface_id: Union[Unset, List[int]] = UNSET,
    vminterface_id_n: Union[Unset, List[int]] = UNSET,
    vrf: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_n: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_id: Union[Unset, List[Union[None, int]]] = UNSET,
    vrf_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_address: Union[Unset, List[str]] = UNSET
    if not isinstance(address, Unset):
        json_address = address

    params["address"] = json_address

    params["assigned"] = assigned

    params["assigned_to_interface"] = assigned_to_interface

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    json_description: Union[Unset, List[str]] = UNSET
    if not isinstance(description, Unset):
        json_description = description

    params["description"] = json_description

    params["description__empty"] = description_empty

    json_description_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ic, Unset):
        json_description_ic = description_ic

    params["description__ic"] = json_description_ic

    json_description_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ie, Unset):
        json_description_ie = description_ie

    params["description__ie"] = json_description_ie

    json_description_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_iew, Unset):
        json_description_iew = description_iew

    params["description__iew"] = json_description_iew

    json_description_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_isw, Unset):
        json_description_isw = description_isw

    params["description__isw"] = json_description_isw

    json_description_n: Union[Unset, List[str]] = UNSET
    if not isinstance(description_n, Unset):
        json_description_n = description_n

    params["description__n"] = json_description_n

    json_description_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nic, Unset):
        json_description_nic = description_nic

    params["description__nic"] = json_description_nic

    json_description_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nie, Unset):
        json_description_nie = description_nie

    params["description__nie"] = json_description_nie

    json_description_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_niew, Unset):
        json_description_niew = description_niew

    params["description__niew"] = json_description_niew

    json_description_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nisw, Unset):
        json_description_nisw = description_nisw

    params["description__nisw"] = json_description_nisw

    json_device: Union[Unset, List[str]] = UNSET
    if not isinstance(device, Unset):
        json_device = device

    params["device"] = json_device

    json_device_id: Union[Unset, List[int]] = UNSET
    if not isinstance(device_id, Unset):
        json_device_id = device_id

    params["device_id"] = json_device_id

    json_dns_name: Union[Unset, List[str]] = UNSET
    if not isinstance(dns_name, Unset):
        json_dns_name = dns_name

    params["dns_name"] = json_dns_name

    params["dns_name__empty"] = dns_name_empty

    json_dns_name_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(dns_name_ic, Unset):
        json_dns_name_ic = dns_name_ic

    params["dns_name__ic"] = json_dns_name_ic

    json_dns_name_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(dns_name_ie, Unset):
        json_dns_name_ie = dns_name_ie

    params["dns_name__ie"] = json_dns_name_ie

    json_dns_name_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(dns_name_iew, Unset):
        json_dns_name_iew = dns_name_iew

    params["dns_name__iew"] = json_dns_name_iew

    json_dns_name_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(dns_name_isw, Unset):
        json_dns_name_isw = dns_name_isw

    params["dns_name__isw"] = json_dns_name_isw

    json_dns_name_n: Union[Unset, List[str]] = UNSET
    if not isinstance(dns_name_n, Unset):
        json_dns_name_n = dns_name_n

    params["dns_name__n"] = json_dns_name_n

    json_dns_name_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(dns_name_nic, Unset):
        json_dns_name_nic = dns_name_nic

    params["dns_name__nic"] = json_dns_name_nic

    json_dns_name_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(dns_name_nie, Unset):
        json_dns_name_nie = dns_name_nie

    params["dns_name__nie"] = json_dns_name_nie

    json_dns_name_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(dns_name_niew, Unset):
        json_dns_name_niew = dns_name_niew

    params["dns_name__niew"] = json_dns_name_niew

    json_dns_name_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(dns_name_nisw, Unset):
        json_dns_name_nisw = dns_name_nisw

    params["dns_name__nisw"] = json_dns_name_nisw

    params["family"] = family

    json_fhrpgroup_id: Union[Unset, List[int]] = UNSET
    if not isinstance(fhrpgroup_id, Unset):
        json_fhrpgroup_id = fhrpgroup_id

    params["fhrpgroup_id"] = json_fhrpgroup_id

    json_fhrpgroup_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(fhrpgroup_id_n, Unset):
        json_fhrpgroup_id_n = fhrpgroup_id_n

    params["fhrpgroup_id__n"] = json_fhrpgroup_id_n

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    json_interface: Union[Unset, List[str]] = UNSET
    if not isinstance(interface, Unset):
        json_interface = interface

    params["interface"] = json_interface

    json_interface_n: Union[Unset, List[str]] = UNSET
    if not isinstance(interface_n, Unset):
        json_interface_n = interface_n

    params["interface__n"] = json_interface_n

    json_interface_id: Union[Unset, List[int]] = UNSET
    if not isinstance(interface_id, Unset):
        json_interface_id = interface_id

    params["interface_id"] = json_interface_id

    json_interface_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(interface_id_n, Unset):
        json_interface_id_n = interface_id_n

    params["interface_id__n"] = json_interface_id_n

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    json_mask_length: Union[Unset, List[int]] = UNSET
    if not isinstance(mask_length, Unset):
        json_mask_length = mask_length

    params["mask_length"] = json_mask_length

    params["mask_length__gte"] = mask_length_gte

    params["mask_length__lte"] = mask_length_lte

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    params["ordering"] = ordering

    json_parent: Union[Unset, List[str]] = UNSET
    if not isinstance(parent, Unset):
        json_parent = parent

    params["parent"] = json_parent

    params["present_in_vrf"] = present_in_vrf

    params["present_in_vrf_id"] = present_in_vrf_id

    params["q"] = q

    json_role: Union[Unset, List[str]] = UNSET
    if not isinstance(role, Unset):
        json_role = role

    params["role"] = json_role

    json_role_n: Union[Unset, List[str]] = UNSET
    if not isinstance(role_n, Unset):
        json_role_n = role_n

    params["role__n"] = json_role_n

    json_status: Union[Unset, List[str]] = UNSET
    if not isinstance(status, Unset):
        json_status = status

    params["status"] = json_status

    json_status_n: Union[Unset, List[str]] = UNSET
    if not isinstance(status_n, Unset):
        json_status_n = status_n

    params["status__n"] = json_status_n

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_tenant: Union[Unset, List[str]] = UNSET
    if not isinstance(tenant, Unset):
        json_tenant = tenant

    params["tenant"] = json_tenant

    json_tenant_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tenant_n, Unset):
        json_tenant_n = tenant_n

    params["tenant__n"] = json_tenant_n

    json_tenant_group: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group, Unset):
        json_tenant_group = tenant_group

    params["tenant_group"] = json_tenant_group

    json_tenant_group_n: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group_n, Unset):
        json_tenant_group_n = tenant_group_n

    params["tenant_group__n"] = json_tenant_group_n

    json_tenant_group_id: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group_id, Unset):
        json_tenant_group_id = tenant_group_id

    params["tenant_group_id"] = json_tenant_group_id

    json_tenant_group_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(tenant_group_id_n, Unset):
        json_tenant_group_id_n = tenant_group_id_n

    params["tenant_group_id__n"] = json_tenant_group_id_n

    json_tenant_id: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(tenant_id, Unset):
        json_tenant_id = []
        for tenant_id_item_data in tenant_id:
            tenant_id_item: Union[None, int]
            tenant_id_item = tenant_id_item_data
            json_tenant_id.append(tenant_id_item)

    params["tenant_id"] = json_tenant_id

    json_tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(tenant_id_n, Unset):
        json_tenant_id_n = []
        for tenant_id_n_item_data in tenant_id_n:
            tenant_id_n_item: Union[None, int]
            tenant_id_n_item = tenant_id_n_item_data
            json_tenant_id_n.append(tenant_id_n_item)

    params["tenant_id__n"] = json_tenant_id_n

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    json_virtual_machine: Union[Unset, List[str]] = UNSET
    if not isinstance(virtual_machine, Unset):
        json_virtual_machine = virtual_machine

    params["virtual_machine"] = json_virtual_machine

    json_virtual_machine_id: Union[Unset, List[int]] = UNSET
    if not isinstance(virtual_machine_id, Unset):
        json_virtual_machine_id = virtual_machine_id

    params["virtual_machine_id"] = json_virtual_machine_id

    json_vminterface: Union[Unset, List[str]] = UNSET
    if not isinstance(vminterface, Unset):
        json_vminterface = vminterface

    params["vminterface"] = json_vminterface

    json_vminterface_n: Union[Unset, List[str]] = UNSET
    if not isinstance(vminterface_n, Unset):
        json_vminterface_n = vminterface_n

    params["vminterface__n"] = json_vminterface_n

    json_vminterface_id: Union[Unset, List[int]] = UNSET
    if not isinstance(vminterface_id, Unset):
        json_vminterface_id = vminterface_id

    params["vminterface_id"] = json_vminterface_id

    json_vminterface_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(vminterface_id_n, Unset):
        json_vminterface_id_n = vminterface_id_n

    params["vminterface_id__n"] = json_vminterface_id_n

    json_vrf: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(vrf, Unset):
        json_vrf = []
        for vrf_item_data in vrf:
            vrf_item: Union[None, str]
            vrf_item = vrf_item_data
            json_vrf.append(vrf_item)

    params["vrf"] = json_vrf

    json_vrf_n: Union[Unset, List[Union[None, str]]] = UNSET
    if not isinstance(vrf_n, Unset):
        json_vrf_n = []
        for vrf_n_item_data in vrf_n:
            vrf_n_item: Union[None, str]
            vrf_n_item = vrf_n_item_data
            json_vrf_n.append(vrf_n_item)

    params["vrf__n"] = json_vrf_n

    json_vrf_id: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(vrf_id, Unset):
        json_vrf_id = []
        for vrf_id_item_data in vrf_id:
            vrf_id_item: Union[None, int]
            vrf_id_item = vrf_id_item_data
            json_vrf_id.append(vrf_id_item)

    params["vrf_id"] = json_vrf_id

    json_vrf_id_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(vrf_id_n, Unset):
        json_vrf_id_n = []
        for vrf_id_n_item_data in vrf_id_n:
            vrf_id_n_item: Union[None, int]
            vrf_id_n_item = vrf_id_n_item_data
            json_vrf_id_n.append(vrf_id_n_item)

    params["vrf_id__n"] = json_vrf_id_n

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/plugins/prometheus-sd/ip-addresses/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[List["PrometheusIPAddress"]]:
    if response.status_code == HTTPStatus.OK:
        response_200 = []
        _response_200 = response.json()
        for response_200_item_data in _response_200:
            response_200_item = PrometheusIPAddress.from_dict(response_200_item_data)

            response_200.append(response_200_item)

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[List["PrometheusIPAddress"]]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    address: Union[Unset, List[str]] = UNSET,
    assigned: Union[Unset, bool] = UNSET,
    assigned_to_interface: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[str]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    dns_name: Union[Unset, List[str]] = UNSET,
    dns_name_empty: Union[Unset, bool] = UNSET,
    dns_name_ic: Union[Unset, List[str]] = UNSET,
    dns_name_ie: Union[Unset, List[str]] = UNSET,
    dns_name_iew: Union[Unset, List[str]] = UNSET,
    dns_name_isw: Union[Unset, List[str]] = UNSET,
    dns_name_n: Union[Unset, List[str]] = UNSET,
    dns_name_nic: Union[Unset, List[str]] = UNSET,
    dns_name_nie: Union[Unset, List[str]] = UNSET,
    dns_name_niew: Union[Unset, List[str]] = UNSET,
    dns_name_nisw: Union[Unset, List[str]] = UNSET,
    family: Union[Unset, float] = UNSET,
    fhrpgroup_id: Union[Unset, List[int]] = UNSET,
    fhrpgroup_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    interface: Union[Unset, List[str]] = UNSET,
    interface_n: Union[Unset, List[str]] = UNSET,
    interface_id: Union[Unset, List[int]] = UNSET,
    interface_id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    mask_length: Union[Unset, List[int]] = UNSET,
    mask_length_gte: Union[Unset, float] = UNSET,
    mask_length_lte: Union[Unset, float] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent: Union[Unset, List[str]] = UNSET,
    present_in_vrf: Union[Unset, str] = UNSET,
    present_in_vrf_id: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_machine: Union[Unset, List[str]] = UNSET,
    virtual_machine_id: Union[Unset, List[int]] = UNSET,
    vminterface: Union[Unset, List[str]] = UNSET,
    vminterface_n: Union[Unset, List[str]] = UNSET,
    vminterface_id: Union[Unset, List[int]] = UNSET,
    vminterface_id_n: Union[Unset, List[int]] = UNSET,
    vrf: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_n: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_id: Union[Unset, List[Union[None, int]]] = UNSET,
    vrf_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
) -> Response[List["PrometheusIPAddress"]]:
    """Get a list of IP address objects.

    Args:
        address (Union[Unset, List[str]]):
        assigned (Union[Unset, bool]):
        assigned_to_interface (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[str]]):
        device_id (Union[Unset, List[int]]):
        dns_name (Union[Unset, List[str]]):
        dns_name_empty (Union[Unset, bool]):
        dns_name_ic (Union[Unset, List[str]]):
        dns_name_ie (Union[Unset, List[str]]):
        dns_name_iew (Union[Unset, List[str]]):
        dns_name_isw (Union[Unset, List[str]]):
        dns_name_n (Union[Unset, List[str]]):
        dns_name_nic (Union[Unset, List[str]]):
        dns_name_nie (Union[Unset, List[str]]):
        dns_name_niew (Union[Unset, List[str]]):
        dns_name_nisw (Union[Unset, List[str]]):
        family (Union[Unset, float]):
        fhrpgroup_id (Union[Unset, List[int]]):
        fhrpgroup_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        interface (Union[Unset, List[str]]):
        interface_n (Union[Unset, List[str]]):
        interface_id (Union[Unset, List[int]]):
        interface_id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        mask_length (Union[Unset, List[int]]):
        mask_length_gte (Union[Unset, float]):
        mask_length_lte (Union[Unset, float]):
        modified_by_request (Union[Unset, UUID]):
        ordering (Union[Unset, str]):
        parent (Union[Unset, List[str]]):
        present_in_vrf (Union[Unset, str]):
        present_in_vrf_id (Union[Unset, str]):
        q (Union[Unset, str]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_machine (Union[Unset, List[str]]):
        virtual_machine_id (Union[Unset, List[int]]):
        vminterface (Union[Unset, List[str]]):
        vminterface_n (Union[Unset, List[str]]):
        vminterface_id (Union[Unset, List[int]]):
        vminterface_id_n (Union[Unset, List[int]]):
        vrf (Union[Unset, List[Union[None, str]]]):
        vrf_n (Union[Unset, List[Union[None, str]]]):
        vrf_id (Union[Unset, List[Union[None, int]]]):
        vrf_id_n (Union[Unset, List[Union[None, int]]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[List['PrometheusIPAddress']]
    """

    kwargs = _get_kwargs(
        address=address,
        assigned=assigned,
        assigned_to_interface=assigned_to_interface,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_id=device_id,
        dns_name=dns_name,
        dns_name_empty=dns_name_empty,
        dns_name_ic=dns_name_ic,
        dns_name_ie=dns_name_ie,
        dns_name_iew=dns_name_iew,
        dns_name_isw=dns_name_isw,
        dns_name_n=dns_name_n,
        dns_name_nic=dns_name_nic,
        dns_name_nie=dns_name_nie,
        dns_name_niew=dns_name_niew,
        dns_name_nisw=dns_name_nisw,
        family=family,
        fhrpgroup_id=fhrpgroup_id,
        fhrpgroup_id_n=fhrpgroup_id_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        interface=interface,
        interface_n=interface_n,
        interface_id=interface_id,
        interface_id_n=interface_id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        mask_length=mask_length,
        mask_length_gte=mask_length_gte,
        mask_length_lte=mask_length_lte,
        modified_by_request=modified_by_request,
        ordering=ordering,
        parent=parent,
        present_in_vrf=present_in_vrf,
        present_in_vrf_id=present_in_vrf_id,
        q=q,
        role=role,
        role_n=role_n,
        status=status,
        status_n=status_n,
        tag=tag,
        tag_n=tag_n,
        tenant=tenant,
        tenant_n=tenant_n,
        tenant_group=tenant_group,
        tenant_group_n=tenant_group_n,
        tenant_group_id=tenant_group_id,
        tenant_group_id_n=tenant_group_id_n,
        tenant_id=tenant_id,
        tenant_id_n=tenant_id_n,
        updated_by_request=updated_by_request,
        virtual_machine=virtual_machine,
        virtual_machine_id=virtual_machine_id,
        vminterface=vminterface,
        vminterface_n=vminterface_n,
        vminterface_id=vminterface_id,
        vminterface_id_n=vminterface_id_n,
        vrf=vrf,
        vrf_n=vrf_n,
        vrf_id=vrf_id,
        vrf_id_n=vrf_id_n,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    address: Union[Unset, List[str]] = UNSET,
    assigned: Union[Unset, bool] = UNSET,
    assigned_to_interface: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[str]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    dns_name: Union[Unset, List[str]] = UNSET,
    dns_name_empty: Union[Unset, bool] = UNSET,
    dns_name_ic: Union[Unset, List[str]] = UNSET,
    dns_name_ie: Union[Unset, List[str]] = UNSET,
    dns_name_iew: Union[Unset, List[str]] = UNSET,
    dns_name_isw: Union[Unset, List[str]] = UNSET,
    dns_name_n: Union[Unset, List[str]] = UNSET,
    dns_name_nic: Union[Unset, List[str]] = UNSET,
    dns_name_nie: Union[Unset, List[str]] = UNSET,
    dns_name_niew: Union[Unset, List[str]] = UNSET,
    dns_name_nisw: Union[Unset, List[str]] = UNSET,
    family: Union[Unset, float] = UNSET,
    fhrpgroup_id: Union[Unset, List[int]] = UNSET,
    fhrpgroup_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    interface: Union[Unset, List[str]] = UNSET,
    interface_n: Union[Unset, List[str]] = UNSET,
    interface_id: Union[Unset, List[int]] = UNSET,
    interface_id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    mask_length: Union[Unset, List[int]] = UNSET,
    mask_length_gte: Union[Unset, float] = UNSET,
    mask_length_lte: Union[Unset, float] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent: Union[Unset, List[str]] = UNSET,
    present_in_vrf: Union[Unset, str] = UNSET,
    present_in_vrf_id: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_machine: Union[Unset, List[str]] = UNSET,
    virtual_machine_id: Union[Unset, List[int]] = UNSET,
    vminterface: Union[Unset, List[str]] = UNSET,
    vminterface_n: Union[Unset, List[str]] = UNSET,
    vminterface_id: Union[Unset, List[int]] = UNSET,
    vminterface_id_n: Union[Unset, List[int]] = UNSET,
    vrf: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_n: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_id: Union[Unset, List[Union[None, int]]] = UNSET,
    vrf_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
) -> Optional[List["PrometheusIPAddress"]]:
    """Get a list of IP address objects.

    Args:
        address (Union[Unset, List[str]]):
        assigned (Union[Unset, bool]):
        assigned_to_interface (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[str]]):
        device_id (Union[Unset, List[int]]):
        dns_name (Union[Unset, List[str]]):
        dns_name_empty (Union[Unset, bool]):
        dns_name_ic (Union[Unset, List[str]]):
        dns_name_ie (Union[Unset, List[str]]):
        dns_name_iew (Union[Unset, List[str]]):
        dns_name_isw (Union[Unset, List[str]]):
        dns_name_n (Union[Unset, List[str]]):
        dns_name_nic (Union[Unset, List[str]]):
        dns_name_nie (Union[Unset, List[str]]):
        dns_name_niew (Union[Unset, List[str]]):
        dns_name_nisw (Union[Unset, List[str]]):
        family (Union[Unset, float]):
        fhrpgroup_id (Union[Unset, List[int]]):
        fhrpgroup_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        interface (Union[Unset, List[str]]):
        interface_n (Union[Unset, List[str]]):
        interface_id (Union[Unset, List[int]]):
        interface_id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        mask_length (Union[Unset, List[int]]):
        mask_length_gte (Union[Unset, float]):
        mask_length_lte (Union[Unset, float]):
        modified_by_request (Union[Unset, UUID]):
        ordering (Union[Unset, str]):
        parent (Union[Unset, List[str]]):
        present_in_vrf (Union[Unset, str]):
        present_in_vrf_id (Union[Unset, str]):
        q (Union[Unset, str]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_machine (Union[Unset, List[str]]):
        virtual_machine_id (Union[Unset, List[int]]):
        vminterface (Union[Unset, List[str]]):
        vminterface_n (Union[Unset, List[str]]):
        vminterface_id (Union[Unset, List[int]]):
        vminterface_id_n (Union[Unset, List[int]]):
        vrf (Union[Unset, List[Union[None, str]]]):
        vrf_n (Union[Unset, List[Union[None, str]]]):
        vrf_id (Union[Unset, List[Union[None, int]]]):
        vrf_id_n (Union[Unset, List[Union[None, int]]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        List['PrometheusIPAddress']
    """

    return sync_detailed(
        client=client,
        address=address,
        assigned=assigned,
        assigned_to_interface=assigned_to_interface,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_id=device_id,
        dns_name=dns_name,
        dns_name_empty=dns_name_empty,
        dns_name_ic=dns_name_ic,
        dns_name_ie=dns_name_ie,
        dns_name_iew=dns_name_iew,
        dns_name_isw=dns_name_isw,
        dns_name_n=dns_name_n,
        dns_name_nic=dns_name_nic,
        dns_name_nie=dns_name_nie,
        dns_name_niew=dns_name_niew,
        dns_name_nisw=dns_name_nisw,
        family=family,
        fhrpgroup_id=fhrpgroup_id,
        fhrpgroup_id_n=fhrpgroup_id_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        interface=interface,
        interface_n=interface_n,
        interface_id=interface_id,
        interface_id_n=interface_id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        mask_length=mask_length,
        mask_length_gte=mask_length_gte,
        mask_length_lte=mask_length_lte,
        modified_by_request=modified_by_request,
        ordering=ordering,
        parent=parent,
        present_in_vrf=present_in_vrf,
        present_in_vrf_id=present_in_vrf_id,
        q=q,
        role=role,
        role_n=role_n,
        status=status,
        status_n=status_n,
        tag=tag,
        tag_n=tag_n,
        tenant=tenant,
        tenant_n=tenant_n,
        tenant_group=tenant_group,
        tenant_group_n=tenant_group_n,
        tenant_group_id=tenant_group_id,
        tenant_group_id_n=tenant_group_id_n,
        tenant_id=tenant_id,
        tenant_id_n=tenant_id_n,
        updated_by_request=updated_by_request,
        virtual_machine=virtual_machine,
        virtual_machine_id=virtual_machine_id,
        vminterface=vminterface,
        vminterface_n=vminterface_n,
        vminterface_id=vminterface_id,
        vminterface_id_n=vminterface_id_n,
        vrf=vrf,
        vrf_n=vrf_n,
        vrf_id=vrf_id,
        vrf_id_n=vrf_id_n,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    address: Union[Unset, List[str]] = UNSET,
    assigned: Union[Unset, bool] = UNSET,
    assigned_to_interface: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[str]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    dns_name: Union[Unset, List[str]] = UNSET,
    dns_name_empty: Union[Unset, bool] = UNSET,
    dns_name_ic: Union[Unset, List[str]] = UNSET,
    dns_name_ie: Union[Unset, List[str]] = UNSET,
    dns_name_iew: Union[Unset, List[str]] = UNSET,
    dns_name_isw: Union[Unset, List[str]] = UNSET,
    dns_name_n: Union[Unset, List[str]] = UNSET,
    dns_name_nic: Union[Unset, List[str]] = UNSET,
    dns_name_nie: Union[Unset, List[str]] = UNSET,
    dns_name_niew: Union[Unset, List[str]] = UNSET,
    dns_name_nisw: Union[Unset, List[str]] = UNSET,
    family: Union[Unset, float] = UNSET,
    fhrpgroup_id: Union[Unset, List[int]] = UNSET,
    fhrpgroup_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    interface: Union[Unset, List[str]] = UNSET,
    interface_n: Union[Unset, List[str]] = UNSET,
    interface_id: Union[Unset, List[int]] = UNSET,
    interface_id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    mask_length: Union[Unset, List[int]] = UNSET,
    mask_length_gte: Union[Unset, float] = UNSET,
    mask_length_lte: Union[Unset, float] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent: Union[Unset, List[str]] = UNSET,
    present_in_vrf: Union[Unset, str] = UNSET,
    present_in_vrf_id: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_machine: Union[Unset, List[str]] = UNSET,
    virtual_machine_id: Union[Unset, List[int]] = UNSET,
    vminterface: Union[Unset, List[str]] = UNSET,
    vminterface_n: Union[Unset, List[str]] = UNSET,
    vminterface_id: Union[Unset, List[int]] = UNSET,
    vminterface_id_n: Union[Unset, List[int]] = UNSET,
    vrf: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_n: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_id: Union[Unset, List[Union[None, int]]] = UNSET,
    vrf_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
) -> Response[List["PrometheusIPAddress"]]:
    """Get a list of IP address objects.

    Args:
        address (Union[Unset, List[str]]):
        assigned (Union[Unset, bool]):
        assigned_to_interface (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[str]]):
        device_id (Union[Unset, List[int]]):
        dns_name (Union[Unset, List[str]]):
        dns_name_empty (Union[Unset, bool]):
        dns_name_ic (Union[Unset, List[str]]):
        dns_name_ie (Union[Unset, List[str]]):
        dns_name_iew (Union[Unset, List[str]]):
        dns_name_isw (Union[Unset, List[str]]):
        dns_name_n (Union[Unset, List[str]]):
        dns_name_nic (Union[Unset, List[str]]):
        dns_name_nie (Union[Unset, List[str]]):
        dns_name_niew (Union[Unset, List[str]]):
        dns_name_nisw (Union[Unset, List[str]]):
        family (Union[Unset, float]):
        fhrpgroup_id (Union[Unset, List[int]]):
        fhrpgroup_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        interface (Union[Unset, List[str]]):
        interface_n (Union[Unset, List[str]]):
        interface_id (Union[Unset, List[int]]):
        interface_id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        mask_length (Union[Unset, List[int]]):
        mask_length_gte (Union[Unset, float]):
        mask_length_lte (Union[Unset, float]):
        modified_by_request (Union[Unset, UUID]):
        ordering (Union[Unset, str]):
        parent (Union[Unset, List[str]]):
        present_in_vrf (Union[Unset, str]):
        present_in_vrf_id (Union[Unset, str]):
        q (Union[Unset, str]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_machine (Union[Unset, List[str]]):
        virtual_machine_id (Union[Unset, List[int]]):
        vminterface (Union[Unset, List[str]]):
        vminterface_n (Union[Unset, List[str]]):
        vminterface_id (Union[Unset, List[int]]):
        vminterface_id_n (Union[Unset, List[int]]):
        vrf (Union[Unset, List[Union[None, str]]]):
        vrf_n (Union[Unset, List[Union[None, str]]]):
        vrf_id (Union[Unset, List[Union[None, int]]]):
        vrf_id_n (Union[Unset, List[Union[None, int]]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[List['PrometheusIPAddress']]
    """

    kwargs = _get_kwargs(
        address=address,
        assigned=assigned,
        assigned_to_interface=assigned_to_interface,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        device=device,
        device_id=device_id,
        dns_name=dns_name,
        dns_name_empty=dns_name_empty,
        dns_name_ic=dns_name_ic,
        dns_name_ie=dns_name_ie,
        dns_name_iew=dns_name_iew,
        dns_name_isw=dns_name_isw,
        dns_name_n=dns_name_n,
        dns_name_nic=dns_name_nic,
        dns_name_nie=dns_name_nie,
        dns_name_niew=dns_name_niew,
        dns_name_nisw=dns_name_nisw,
        family=family,
        fhrpgroup_id=fhrpgroup_id,
        fhrpgroup_id_n=fhrpgroup_id_n,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        interface=interface,
        interface_n=interface_n,
        interface_id=interface_id,
        interface_id_n=interface_id_n,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        mask_length=mask_length,
        mask_length_gte=mask_length_gte,
        mask_length_lte=mask_length_lte,
        modified_by_request=modified_by_request,
        ordering=ordering,
        parent=parent,
        present_in_vrf=present_in_vrf,
        present_in_vrf_id=present_in_vrf_id,
        q=q,
        role=role,
        role_n=role_n,
        status=status,
        status_n=status_n,
        tag=tag,
        tag_n=tag_n,
        tenant=tenant,
        tenant_n=tenant_n,
        tenant_group=tenant_group,
        tenant_group_n=tenant_group_n,
        tenant_group_id=tenant_group_id,
        tenant_group_id_n=tenant_group_id_n,
        tenant_id=tenant_id,
        tenant_id_n=tenant_id_n,
        updated_by_request=updated_by_request,
        virtual_machine=virtual_machine,
        virtual_machine_id=virtual_machine_id,
        vminterface=vminterface,
        vminterface_n=vminterface_n,
        vminterface_id=vminterface_id,
        vminterface_id_n=vminterface_id_n,
        vrf=vrf,
        vrf_n=vrf_n,
        vrf_id=vrf_id,
        vrf_id_n=vrf_id_n,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    address: Union[Unset, List[str]] = UNSET,
    assigned: Union[Unset, bool] = UNSET,
    assigned_to_interface: Union[Unset, bool] = UNSET,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    device: Union[Unset, List[str]] = UNSET,
    device_id: Union[Unset, List[int]] = UNSET,
    dns_name: Union[Unset, List[str]] = UNSET,
    dns_name_empty: Union[Unset, bool] = UNSET,
    dns_name_ic: Union[Unset, List[str]] = UNSET,
    dns_name_ie: Union[Unset, List[str]] = UNSET,
    dns_name_iew: Union[Unset, List[str]] = UNSET,
    dns_name_isw: Union[Unset, List[str]] = UNSET,
    dns_name_n: Union[Unset, List[str]] = UNSET,
    dns_name_nic: Union[Unset, List[str]] = UNSET,
    dns_name_nie: Union[Unset, List[str]] = UNSET,
    dns_name_niew: Union[Unset, List[str]] = UNSET,
    dns_name_nisw: Union[Unset, List[str]] = UNSET,
    family: Union[Unset, float] = UNSET,
    fhrpgroup_id: Union[Unset, List[int]] = UNSET,
    fhrpgroup_id_n: Union[Unset, List[int]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    interface: Union[Unset, List[str]] = UNSET,
    interface_n: Union[Unset, List[str]] = UNSET,
    interface_id: Union[Unset, List[int]] = UNSET,
    interface_id_n: Union[Unset, List[int]] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    mask_length: Union[Unset, List[int]] = UNSET,
    mask_length_gte: Union[Unset, float] = UNSET,
    mask_length_lte: Union[Unset, float] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    parent: Union[Unset, List[str]] = UNSET,
    present_in_vrf: Union[Unset, str] = UNSET,
    present_in_vrf_id: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    role: Union[Unset, List[str]] = UNSET,
    role_n: Union[Unset, List[str]] = UNSET,
    status: Union[Unset, List[str]] = UNSET,
    status_n: Union[Unset, List[str]] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    tenant: Union[Unset, List[str]] = UNSET,
    tenant_n: Union[Unset, List[str]] = UNSET,
    tenant_group: Union[Unset, List[int]] = UNSET,
    tenant_group_n: Union[Unset, List[int]] = UNSET,
    tenant_group_id: Union[Unset, List[int]] = UNSET,
    tenant_group_id_n: Union[Unset, List[int]] = UNSET,
    tenant_id: Union[Unset, List[Union[None, int]]] = UNSET,
    tenant_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
    virtual_machine: Union[Unset, List[str]] = UNSET,
    virtual_machine_id: Union[Unset, List[int]] = UNSET,
    vminterface: Union[Unset, List[str]] = UNSET,
    vminterface_n: Union[Unset, List[str]] = UNSET,
    vminterface_id: Union[Unset, List[int]] = UNSET,
    vminterface_id_n: Union[Unset, List[int]] = UNSET,
    vrf: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_n: Union[Unset, List[Union[None, str]]] = UNSET,
    vrf_id: Union[Unset, List[Union[None, int]]] = UNSET,
    vrf_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
) -> Optional[List["PrometheusIPAddress"]]:
    """Get a list of IP address objects.

    Args:
        address (Union[Unset, List[str]]):
        assigned (Union[Unset, bool]):
        assigned_to_interface (Union[Unset, bool]):
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        device (Union[Unset, List[str]]):
        device_id (Union[Unset, List[int]]):
        dns_name (Union[Unset, List[str]]):
        dns_name_empty (Union[Unset, bool]):
        dns_name_ic (Union[Unset, List[str]]):
        dns_name_ie (Union[Unset, List[str]]):
        dns_name_iew (Union[Unset, List[str]]):
        dns_name_isw (Union[Unset, List[str]]):
        dns_name_n (Union[Unset, List[str]]):
        dns_name_nic (Union[Unset, List[str]]):
        dns_name_nie (Union[Unset, List[str]]):
        dns_name_niew (Union[Unset, List[str]]):
        dns_name_nisw (Union[Unset, List[str]]):
        family (Union[Unset, float]):
        fhrpgroup_id (Union[Unset, List[int]]):
        fhrpgroup_id_n (Union[Unset, List[int]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        interface (Union[Unset, List[str]]):
        interface_n (Union[Unset, List[str]]):
        interface_id (Union[Unset, List[int]]):
        interface_id_n (Union[Unset, List[int]]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        mask_length (Union[Unset, List[int]]):
        mask_length_gte (Union[Unset, float]):
        mask_length_lte (Union[Unset, float]):
        modified_by_request (Union[Unset, UUID]):
        ordering (Union[Unset, str]):
        parent (Union[Unset, List[str]]):
        present_in_vrf (Union[Unset, str]):
        present_in_vrf_id (Union[Unset, str]):
        q (Union[Unset, str]):
        role (Union[Unset, List[str]]):
        role_n (Union[Unset, List[str]]):
        status (Union[Unset, List[str]]):
        status_n (Union[Unset, List[str]]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        tenant (Union[Unset, List[str]]):
        tenant_n (Union[Unset, List[str]]):
        tenant_group (Union[Unset, List[int]]):
        tenant_group_n (Union[Unset, List[int]]):
        tenant_group_id (Union[Unset, List[int]]):
        tenant_group_id_n (Union[Unset, List[int]]):
        tenant_id (Union[Unset, List[Union[None, int]]]):
        tenant_id_n (Union[Unset, List[Union[None, int]]]):
        updated_by_request (Union[Unset, UUID]):
        virtual_machine (Union[Unset, List[str]]):
        virtual_machine_id (Union[Unset, List[int]]):
        vminterface (Union[Unset, List[str]]):
        vminterface_n (Union[Unset, List[str]]):
        vminterface_id (Union[Unset, List[int]]):
        vminterface_id_n (Union[Unset, List[int]]):
        vrf (Union[Unset, List[Union[None, str]]]):
        vrf_n (Union[Unset, List[Union[None, str]]]):
        vrf_id (Union[Unset, List[Union[None, int]]]):
        vrf_id_n (Union[Unset, List[Union[None, int]]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        List['PrometheusIPAddress']
    """

    return (
        await asyncio_detailed(
            client=client,
            address=address,
            assigned=assigned,
            assigned_to_interface=assigned_to_interface,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            description=description,
            description_empty=description_empty,
            description_ic=description_ic,
            description_ie=description_ie,
            description_iew=description_iew,
            description_isw=description_isw,
            description_n=description_n,
            description_nic=description_nic,
            description_nie=description_nie,
            description_niew=description_niew,
            description_nisw=description_nisw,
            device=device,
            device_id=device_id,
            dns_name=dns_name,
            dns_name_empty=dns_name_empty,
            dns_name_ic=dns_name_ic,
            dns_name_ie=dns_name_ie,
            dns_name_iew=dns_name_iew,
            dns_name_isw=dns_name_isw,
            dns_name_n=dns_name_n,
            dns_name_nic=dns_name_nic,
            dns_name_nie=dns_name_nie,
            dns_name_niew=dns_name_niew,
            dns_name_nisw=dns_name_nisw,
            family=family,
            fhrpgroup_id=fhrpgroup_id,
            fhrpgroup_id_n=fhrpgroup_id_n,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            interface=interface,
            interface_n=interface_n,
            interface_id=interface_id,
            interface_id_n=interface_id_n,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            mask_length=mask_length,
            mask_length_gte=mask_length_gte,
            mask_length_lte=mask_length_lte,
            modified_by_request=modified_by_request,
            ordering=ordering,
            parent=parent,
            present_in_vrf=present_in_vrf,
            present_in_vrf_id=present_in_vrf_id,
            q=q,
            role=role,
            role_n=role_n,
            status=status,
            status_n=status_n,
            tag=tag,
            tag_n=tag_n,
            tenant=tenant,
            tenant_n=tenant_n,
            tenant_group=tenant_group,
            tenant_group_n=tenant_group_n,
            tenant_group_id=tenant_group_id,
            tenant_group_id_n=tenant_group_id_n,
            tenant_id=tenant_id,
            tenant_id_n=tenant_id_n,
            updated_by_request=updated_by_request,
            virtual_machine=virtual_machine,
            virtual_machine_id=virtual_machine_id,
            vminterface=vminterface,
            vminterface_n=vminterface_n,
            vminterface_id=vminterface_id,
            vminterface_id_n=vminterface_id_n,
            vrf=vrf,
            vrf_n=vrf_n,
            vrf_id=vrf_id,
            vrf_id_n=vrf_id_n,
        )
    ).parsed
