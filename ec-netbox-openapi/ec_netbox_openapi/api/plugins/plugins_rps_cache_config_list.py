import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union
from uuid import UUID

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_cache_config_list import PaginatedCacheConfigList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    mapping: Union[Unset, int] = UNSET,
    mapping_n: Union[Unset, int] = UNSET,
    max_size_limit: Union[Unset, List[int]] = UNSET,
    max_size_limit_empty: Union[Unset, bool] = UNSET,
    max_size_limit_gt: Union[Unset, List[int]] = UNSET,
    max_size_limit_gte: Union[Unset, List[int]] = UNSET,
    max_size_limit_lt: Union[Unset, List[int]] = UNSET,
    max_size_limit_lte: Union[Unset, List[int]] = UNSET,
    max_size_limit_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    ttl: Union[Unset, List[int]] = UNSET,
    ttl_empty: Union[Unset, bool] = UNSET,
    ttl_gt: Union[Unset, List[int]] = UNSET,
    ttl_gte: Union[Unset, List[int]] = UNSET,
    ttl_lt: Union[Unset, List[int]] = UNSET,
    ttl_lte: Union[Unset, List[int]] = UNSET,
    ttl_n: Union[Unset, List[int]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_created: Union[Unset, List[str]] = UNSET
    if not isinstance(created, Unset):
        json_created = []
        for created_item_data in created:
            created_item = created_item_data.isoformat()
            json_created.append(created_item)

    params["created"] = json_created

    json_created_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(created_empty, Unset):
        json_created_empty = []
        for created_empty_item_data in created_empty:
            created_empty_item = created_empty_item_data.isoformat()
            json_created_empty.append(created_empty_item)

    params["created__empty"] = json_created_empty

    json_created_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gt, Unset):
        json_created_gt = []
        for created_gt_item_data in created_gt:
            created_gt_item = created_gt_item_data.isoformat()
            json_created_gt.append(created_gt_item)

    params["created__gt"] = json_created_gt

    json_created_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_gte, Unset):
        json_created_gte = []
        for created_gte_item_data in created_gte:
            created_gte_item = created_gte_item_data.isoformat()
            json_created_gte.append(created_gte_item)

    params["created__gte"] = json_created_gte

    json_created_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lt, Unset):
        json_created_lt = []
        for created_lt_item_data in created_lt:
            created_lt_item = created_lt_item_data.isoformat()
            json_created_lt.append(created_lt_item)

    params["created__lt"] = json_created_lt

    json_created_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(created_lte, Unset):
        json_created_lte = []
        for created_lte_item_data in created_lte:
            created_lte_item = created_lte_item_data.isoformat()
            json_created_lte.append(created_lte_item)

    params["created__lte"] = json_created_lte

    json_created_n: Union[Unset, List[str]] = UNSET
    if not isinstance(created_n, Unset):
        json_created_n = []
        for created_n_item_data in created_n:
            created_n_item = created_n_item_data.isoformat()
            json_created_n.append(created_n_item)

    params["created__n"] = json_created_n

    json_created_by_request: Union[Unset, str] = UNSET
    if not isinstance(created_by_request, Unset):
        json_created_by_request = str(created_by_request)
    params["created_by_request"] = json_created_by_request

    json_last_updated: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated, Unset):
        json_last_updated = []
        for last_updated_item_data in last_updated:
            last_updated_item = last_updated_item_data.isoformat()
            json_last_updated.append(last_updated_item)

    params["last_updated"] = json_last_updated

    json_last_updated_empty: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_empty, Unset):
        json_last_updated_empty = []
        for last_updated_empty_item_data in last_updated_empty:
            last_updated_empty_item = last_updated_empty_item_data.isoformat()
            json_last_updated_empty.append(last_updated_empty_item)

    params["last_updated__empty"] = json_last_updated_empty

    json_last_updated_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gt, Unset):
        json_last_updated_gt = []
        for last_updated_gt_item_data in last_updated_gt:
            last_updated_gt_item = last_updated_gt_item_data.isoformat()
            json_last_updated_gt.append(last_updated_gt_item)

    params["last_updated__gt"] = json_last_updated_gt

    json_last_updated_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_gte, Unset):
        json_last_updated_gte = []
        for last_updated_gte_item_data in last_updated_gte:
            last_updated_gte_item = last_updated_gte_item_data.isoformat()
            json_last_updated_gte.append(last_updated_gte_item)

    params["last_updated__gte"] = json_last_updated_gte

    json_last_updated_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lt, Unset):
        json_last_updated_lt = []
        for last_updated_lt_item_data in last_updated_lt:
            last_updated_lt_item = last_updated_lt_item_data.isoformat()
            json_last_updated_lt.append(last_updated_lt_item)

    params["last_updated__lt"] = json_last_updated_lt

    json_last_updated_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_lte, Unset):
        json_last_updated_lte = []
        for last_updated_lte_item_data in last_updated_lte:
            last_updated_lte_item = last_updated_lte_item_data.isoformat()
            json_last_updated_lte.append(last_updated_lte_item)

    params["last_updated__lte"] = json_last_updated_lte

    json_last_updated_n: Union[Unset, List[str]] = UNSET
    if not isinstance(last_updated_n, Unset):
        json_last_updated_n = []
        for last_updated_n_item_data in last_updated_n:
            last_updated_n_item = last_updated_n_item_data.isoformat()
            json_last_updated_n.append(last_updated_n_item)

    params["last_updated__n"] = json_last_updated_n

    params["limit"] = limit

    params["mapping"] = mapping

    params["mapping__n"] = mapping_n

    json_max_size_limit: Union[Unset, List[int]] = UNSET
    if not isinstance(max_size_limit, Unset):
        json_max_size_limit = max_size_limit

    params["max_size_limit"] = json_max_size_limit

    params["max_size_limit__empty"] = max_size_limit_empty

    json_max_size_limit_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(max_size_limit_gt, Unset):
        json_max_size_limit_gt = max_size_limit_gt

    params["max_size_limit__gt"] = json_max_size_limit_gt

    json_max_size_limit_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(max_size_limit_gte, Unset):
        json_max_size_limit_gte = max_size_limit_gte

    params["max_size_limit__gte"] = json_max_size_limit_gte

    json_max_size_limit_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(max_size_limit_lt, Unset):
        json_max_size_limit_lt = max_size_limit_lt

    params["max_size_limit__lt"] = json_max_size_limit_lt

    json_max_size_limit_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(max_size_limit_lte, Unset):
        json_max_size_limit_lte = max_size_limit_lte

    params["max_size_limit__lte"] = json_max_size_limit_lte

    json_max_size_limit_n: Union[Unset, List[int]] = UNSET
    if not isinstance(max_size_limit_n, Unset):
        json_max_size_limit_n = max_size_limit_n

    params["max_size_limit__n"] = json_max_size_limit_n

    json_modified_by_request: Union[Unset, str] = UNSET
    if not isinstance(modified_by_request, Unset):
        json_modified_by_request = str(modified_by_request)
    params["modified_by_request"] = json_modified_by_request

    params["offset"] = offset

    params["ordering"] = ordering

    params["q"] = q

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    json_ttl: Union[Unset, List[int]] = UNSET
    if not isinstance(ttl, Unset):
        json_ttl = ttl

    params["ttl"] = json_ttl

    params["ttl__empty"] = ttl_empty

    json_ttl_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(ttl_gt, Unset):
        json_ttl_gt = ttl_gt

    params["ttl__gt"] = json_ttl_gt

    json_ttl_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(ttl_gte, Unset):
        json_ttl_gte = ttl_gte

    params["ttl__gte"] = json_ttl_gte

    json_ttl_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(ttl_lt, Unset):
        json_ttl_lt = ttl_lt

    params["ttl__lt"] = json_ttl_lt

    json_ttl_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(ttl_lte, Unset):
        json_ttl_lte = ttl_lte

    params["ttl__lte"] = json_ttl_lte

    json_ttl_n: Union[Unset, List[int]] = UNSET
    if not isinstance(ttl_n, Unset):
        json_ttl_n = ttl_n

    params["ttl__n"] = json_ttl_n

    json_updated_by_request: Union[Unset, str] = UNSET
    if not isinstance(updated_by_request, Unset):
        json_updated_by_request = str(updated_by_request)
    params["updated_by_request"] = json_updated_by_request

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/plugins/rps/cache_config/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedCacheConfigList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedCacheConfigList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedCacheConfigList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    mapping: Union[Unset, int] = UNSET,
    mapping_n: Union[Unset, int] = UNSET,
    max_size_limit: Union[Unset, List[int]] = UNSET,
    max_size_limit_empty: Union[Unset, bool] = UNSET,
    max_size_limit_gt: Union[Unset, List[int]] = UNSET,
    max_size_limit_gte: Union[Unset, List[int]] = UNSET,
    max_size_limit_lt: Union[Unset, List[int]] = UNSET,
    max_size_limit_lte: Union[Unset, List[int]] = UNSET,
    max_size_limit_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    ttl: Union[Unset, List[int]] = UNSET,
    ttl_empty: Union[Unset, bool] = UNSET,
    ttl_gt: Union[Unset, List[int]] = UNSET,
    ttl_gte: Union[Unset, List[int]] = UNSET,
    ttl_lt: Union[Unset, List[int]] = UNSET,
    ttl_lte: Union[Unset, List[int]] = UNSET,
    ttl_n: Union[Unset, List[int]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Response[PaginatedCacheConfigList]:
    """Cache config view set class

    Args:
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        mapping (Union[Unset, int]):
        mapping_n (Union[Unset, int]):
        max_size_limit (Union[Unset, List[int]]):
        max_size_limit_empty (Union[Unset, bool]):
        max_size_limit_gt (Union[Unset, List[int]]):
        max_size_limit_gte (Union[Unset, List[int]]):
        max_size_limit_lt (Union[Unset, List[int]]):
        max_size_limit_lte (Union[Unset, List[int]]):
        max_size_limit_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        ttl (Union[Unset, List[int]]):
        ttl_empty (Union[Unset, bool]):
        ttl_gt (Union[Unset, List[int]]):
        ttl_gte (Union[Unset, List[int]]):
        ttl_lt (Union[Unset, List[int]]):
        ttl_lte (Union[Unset, List[int]]):
        ttl_n (Union[Unset, List[int]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCacheConfigList]
    """

    kwargs = _get_kwargs(
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        mapping=mapping,
        mapping_n=mapping_n,
        max_size_limit=max_size_limit,
        max_size_limit_empty=max_size_limit_empty,
        max_size_limit_gt=max_size_limit_gt,
        max_size_limit_gte=max_size_limit_gte,
        max_size_limit_lt=max_size_limit_lt,
        max_size_limit_lte=max_size_limit_lte,
        max_size_limit_n=max_size_limit_n,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        tag=tag,
        tag_n=tag_n,
        ttl=ttl,
        ttl_empty=ttl_empty,
        ttl_gt=ttl_gt,
        ttl_gte=ttl_gte,
        ttl_lt=ttl_lt,
        ttl_lte=ttl_lte,
        ttl_n=ttl_n,
        updated_by_request=updated_by_request,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    mapping: Union[Unset, int] = UNSET,
    mapping_n: Union[Unset, int] = UNSET,
    max_size_limit: Union[Unset, List[int]] = UNSET,
    max_size_limit_empty: Union[Unset, bool] = UNSET,
    max_size_limit_gt: Union[Unset, List[int]] = UNSET,
    max_size_limit_gte: Union[Unset, List[int]] = UNSET,
    max_size_limit_lt: Union[Unset, List[int]] = UNSET,
    max_size_limit_lte: Union[Unset, List[int]] = UNSET,
    max_size_limit_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    ttl: Union[Unset, List[int]] = UNSET,
    ttl_empty: Union[Unset, bool] = UNSET,
    ttl_gt: Union[Unset, List[int]] = UNSET,
    ttl_gte: Union[Unset, List[int]] = UNSET,
    ttl_lt: Union[Unset, List[int]] = UNSET,
    ttl_lte: Union[Unset, List[int]] = UNSET,
    ttl_n: Union[Unset, List[int]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Optional[PaginatedCacheConfigList]:
    """Cache config view set class

    Args:
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        mapping (Union[Unset, int]):
        mapping_n (Union[Unset, int]):
        max_size_limit (Union[Unset, List[int]]):
        max_size_limit_empty (Union[Unset, bool]):
        max_size_limit_gt (Union[Unset, List[int]]):
        max_size_limit_gte (Union[Unset, List[int]]):
        max_size_limit_lt (Union[Unset, List[int]]):
        max_size_limit_lte (Union[Unset, List[int]]):
        max_size_limit_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        ttl (Union[Unset, List[int]]):
        ttl_empty (Union[Unset, bool]):
        ttl_gt (Union[Unset, List[int]]):
        ttl_gte (Union[Unset, List[int]]):
        ttl_lt (Union[Unset, List[int]]):
        ttl_lte (Union[Unset, List[int]]):
        ttl_n (Union[Unset, List[int]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCacheConfigList
    """

    return sync_detailed(
        client=client,
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        mapping=mapping,
        mapping_n=mapping_n,
        max_size_limit=max_size_limit,
        max_size_limit_empty=max_size_limit_empty,
        max_size_limit_gt=max_size_limit_gt,
        max_size_limit_gte=max_size_limit_gte,
        max_size_limit_lt=max_size_limit_lt,
        max_size_limit_lte=max_size_limit_lte,
        max_size_limit_n=max_size_limit_n,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        tag=tag,
        tag_n=tag_n,
        ttl=ttl,
        ttl_empty=ttl_empty,
        ttl_gt=ttl_gt,
        ttl_gte=ttl_gte,
        ttl_lt=ttl_lt,
        ttl_lte=ttl_lte,
        ttl_n=ttl_n,
        updated_by_request=updated_by_request,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    mapping: Union[Unset, int] = UNSET,
    mapping_n: Union[Unset, int] = UNSET,
    max_size_limit: Union[Unset, List[int]] = UNSET,
    max_size_limit_empty: Union[Unset, bool] = UNSET,
    max_size_limit_gt: Union[Unset, List[int]] = UNSET,
    max_size_limit_gte: Union[Unset, List[int]] = UNSET,
    max_size_limit_lt: Union[Unset, List[int]] = UNSET,
    max_size_limit_lte: Union[Unset, List[int]] = UNSET,
    max_size_limit_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    ttl: Union[Unset, List[int]] = UNSET,
    ttl_empty: Union[Unset, bool] = UNSET,
    ttl_gt: Union[Unset, List[int]] = UNSET,
    ttl_gte: Union[Unset, List[int]] = UNSET,
    ttl_lt: Union[Unset, List[int]] = UNSET,
    ttl_lte: Union[Unset, List[int]] = UNSET,
    ttl_n: Union[Unset, List[int]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Response[PaginatedCacheConfigList]:
    """Cache config view set class

    Args:
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        mapping (Union[Unset, int]):
        mapping_n (Union[Unset, int]):
        max_size_limit (Union[Unset, List[int]]):
        max_size_limit_empty (Union[Unset, bool]):
        max_size_limit_gt (Union[Unset, List[int]]):
        max_size_limit_gte (Union[Unset, List[int]]):
        max_size_limit_lt (Union[Unset, List[int]]):
        max_size_limit_lte (Union[Unset, List[int]]):
        max_size_limit_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        ttl (Union[Unset, List[int]]):
        ttl_empty (Union[Unset, bool]):
        ttl_gt (Union[Unset, List[int]]):
        ttl_gte (Union[Unset, List[int]]):
        ttl_lt (Union[Unset, List[int]]):
        ttl_lte (Union[Unset, List[int]]):
        ttl_n (Union[Unset, List[int]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCacheConfigList]
    """

    kwargs = _get_kwargs(
        created=created,
        created_empty=created_empty,
        created_gt=created_gt,
        created_gte=created_gte,
        created_lt=created_lt,
        created_lte=created_lte,
        created_n=created_n,
        created_by_request=created_by_request,
        last_updated=last_updated,
        last_updated_empty=last_updated_empty,
        last_updated_gt=last_updated_gt,
        last_updated_gte=last_updated_gte,
        last_updated_lt=last_updated_lt,
        last_updated_lte=last_updated_lte,
        last_updated_n=last_updated_n,
        limit=limit,
        mapping=mapping,
        mapping_n=mapping_n,
        max_size_limit=max_size_limit,
        max_size_limit_empty=max_size_limit_empty,
        max_size_limit_gt=max_size_limit_gt,
        max_size_limit_gte=max_size_limit_gte,
        max_size_limit_lt=max_size_limit_lt,
        max_size_limit_lte=max_size_limit_lte,
        max_size_limit_n=max_size_limit_n,
        modified_by_request=modified_by_request,
        offset=offset,
        ordering=ordering,
        q=q,
        tag=tag,
        tag_n=tag_n,
        ttl=ttl,
        ttl_empty=ttl_empty,
        ttl_gt=ttl_gt,
        ttl_gte=ttl_gte,
        ttl_lt=ttl_lt,
        ttl_lte=ttl_lte,
        ttl_n=ttl_n,
        updated_by_request=updated_by_request,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    created: Union[Unset, List[datetime.datetime]] = UNSET,
    created_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    created_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    created_n: Union[Unset, List[datetime.datetime]] = UNSET,
    created_by_request: Union[Unset, UUID] = UNSET,
    last_updated: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_empty: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    last_updated_n: Union[Unset, List[datetime.datetime]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    mapping: Union[Unset, int] = UNSET,
    mapping_n: Union[Unset, int] = UNSET,
    max_size_limit: Union[Unset, List[int]] = UNSET,
    max_size_limit_empty: Union[Unset, bool] = UNSET,
    max_size_limit_gt: Union[Unset, List[int]] = UNSET,
    max_size_limit_gte: Union[Unset, List[int]] = UNSET,
    max_size_limit_lt: Union[Unset, List[int]] = UNSET,
    max_size_limit_lte: Union[Unset, List[int]] = UNSET,
    max_size_limit_n: Union[Unset, List[int]] = UNSET,
    modified_by_request: Union[Unset, UUID] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
    ttl: Union[Unset, List[int]] = UNSET,
    ttl_empty: Union[Unset, bool] = UNSET,
    ttl_gt: Union[Unset, List[int]] = UNSET,
    ttl_gte: Union[Unset, List[int]] = UNSET,
    ttl_lt: Union[Unset, List[int]] = UNSET,
    ttl_lte: Union[Unset, List[int]] = UNSET,
    ttl_n: Union[Unset, List[int]] = UNSET,
    updated_by_request: Union[Unset, UUID] = UNSET,
) -> Optional[PaginatedCacheConfigList]:
    """Cache config view set class

    Args:
        created (Union[Unset, List[datetime.datetime]]):
        created_empty (Union[Unset, List[datetime.datetime]]):
        created_gt (Union[Unset, List[datetime.datetime]]):
        created_gte (Union[Unset, List[datetime.datetime]]):
        created_lt (Union[Unset, List[datetime.datetime]]):
        created_lte (Union[Unset, List[datetime.datetime]]):
        created_n (Union[Unset, List[datetime.datetime]]):
        created_by_request (Union[Unset, UUID]):
        last_updated (Union[Unset, List[datetime.datetime]]):
        last_updated_empty (Union[Unset, List[datetime.datetime]]):
        last_updated_gt (Union[Unset, List[datetime.datetime]]):
        last_updated_gte (Union[Unset, List[datetime.datetime]]):
        last_updated_lt (Union[Unset, List[datetime.datetime]]):
        last_updated_lte (Union[Unset, List[datetime.datetime]]):
        last_updated_n (Union[Unset, List[datetime.datetime]]):
        limit (Union[Unset, int]):
        mapping (Union[Unset, int]):
        mapping_n (Union[Unset, int]):
        max_size_limit (Union[Unset, List[int]]):
        max_size_limit_empty (Union[Unset, bool]):
        max_size_limit_gt (Union[Unset, List[int]]):
        max_size_limit_gte (Union[Unset, List[int]]):
        max_size_limit_lt (Union[Unset, List[int]]):
        max_size_limit_lte (Union[Unset, List[int]]):
        max_size_limit_n (Union[Unset, List[int]]):
        modified_by_request (Union[Unset, UUID]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):
        ttl (Union[Unset, List[int]]):
        ttl_empty (Union[Unset, bool]):
        ttl_gt (Union[Unset, List[int]]):
        ttl_gte (Union[Unset, List[int]]):
        ttl_lt (Union[Unset, List[int]]):
        ttl_lte (Union[Unset, List[int]]):
        ttl_n (Union[Unset, List[int]]):
        updated_by_request (Union[Unset, UUID]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCacheConfigList
    """

    return (
        await asyncio_detailed(
            client=client,
            created=created,
            created_empty=created_empty,
            created_gt=created_gt,
            created_gte=created_gte,
            created_lt=created_lt,
            created_lte=created_lte,
            created_n=created_n,
            created_by_request=created_by_request,
            last_updated=last_updated,
            last_updated_empty=last_updated_empty,
            last_updated_gt=last_updated_gt,
            last_updated_gte=last_updated_gte,
            last_updated_lt=last_updated_lt,
            last_updated_lte=last_updated_lte,
            last_updated_n=last_updated_n,
            limit=limit,
            mapping=mapping,
            mapping_n=mapping_n,
            max_size_limit=max_size_limit,
            max_size_limit_empty=max_size_limit_empty,
            max_size_limit_gt=max_size_limit_gt,
            max_size_limit_gte=max_size_limit_gte,
            max_size_limit_lt=max_size_limit_lt,
            max_size_limit_lte=max_size_limit_lte,
            max_size_limit_n=max_size_limit_n,
            modified_by_request=modified_by_request,
            offset=offset,
            ordering=ordering,
            q=q,
            tag=tag,
            tag_n=tag_n,
            ttl=ttl,
            ttl_empty=ttl_empty,
            ttl_gt=ttl_gt,
            ttl_gte=ttl_gte,
            ttl_lt=ttl_lt,
            ttl_lte=ttl_lte,
            ttl_n=ttl_n,
            updated_by_request=updated_by_request,
        )
    ).parsed
