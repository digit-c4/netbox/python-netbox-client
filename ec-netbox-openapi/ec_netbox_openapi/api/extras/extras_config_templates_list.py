import datetime
from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_config_template_list import PaginatedConfigTemplateList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    data_file_id: Union[Unset, List[Union[None, int]]] = UNSET,
    data_file_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    data_source_id: Union[Unset, List[Union[None, int]]] = UNSET,
    data_source_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    data_synced: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_empty: Union[Unset, bool] = UNSET,
    data_synced_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_n: Union[Unset, List[datetime.datetime]] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_data_file_id: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(data_file_id, Unset):
        json_data_file_id = []
        for data_file_id_item_data in data_file_id:
            data_file_id_item: Union[None, int]
            data_file_id_item = data_file_id_item_data
            json_data_file_id.append(data_file_id_item)

    params["data_file_id"] = json_data_file_id

    json_data_file_id_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(data_file_id_n, Unset):
        json_data_file_id_n = []
        for data_file_id_n_item_data in data_file_id_n:
            data_file_id_n_item: Union[None, int]
            data_file_id_n_item = data_file_id_n_item_data
            json_data_file_id_n.append(data_file_id_n_item)

    params["data_file_id__n"] = json_data_file_id_n

    json_data_source_id: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(data_source_id, Unset):
        json_data_source_id = []
        for data_source_id_item_data in data_source_id:
            data_source_id_item: Union[None, int]
            data_source_id_item = data_source_id_item_data
            json_data_source_id.append(data_source_id_item)

    params["data_source_id"] = json_data_source_id

    json_data_source_id_n: Union[Unset, List[Union[None, int]]] = UNSET
    if not isinstance(data_source_id_n, Unset):
        json_data_source_id_n = []
        for data_source_id_n_item_data in data_source_id_n:
            data_source_id_n_item: Union[None, int]
            data_source_id_n_item = data_source_id_n_item_data
            json_data_source_id_n.append(data_source_id_n_item)

    params["data_source_id__n"] = json_data_source_id_n

    json_data_synced: Union[Unset, List[str]] = UNSET
    if not isinstance(data_synced, Unset):
        json_data_synced = []
        for data_synced_item_data in data_synced:
            data_synced_item = data_synced_item_data.isoformat()
            json_data_synced.append(data_synced_item)

    params["data_synced"] = json_data_synced

    params["data_synced__empty"] = data_synced_empty

    json_data_synced_gt: Union[Unset, List[str]] = UNSET
    if not isinstance(data_synced_gt, Unset):
        json_data_synced_gt = []
        for data_synced_gt_item_data in data_synced_gt:
            data_synced_gt_item = data_synced_gt_item_data.isoformat()
            json_data_synced_gt.append(data_synced_gt_item)

    params["data_synced__gt"] = json_data_synced_gt

    json_data_synced_gte: Union[Unset, List[str]] = UNSET
    if not isinstance(data_synced_gte, Unset):
        json_data_synced_gte = []
        for data_synced_gte_item_data in data_synced_gte:
            data_synced_gte_item = data_synced_gte_item_data.isoformat()
            json_data_synced_gte.append(data_synced_gte_item)

    params["data_synced__gte"] = json_data_synced_gte

    json_data_synced_lt: Union[Unset, List[str]] = UNSET
    if not isinstance(data_synced_lt, Unset):
        json_data_synced_lt = []
        for data_synced_lt_item_data in data_synced_lt:
            data_synced_lt_item = data_synced_lt_item_data.isoformat()
            json_data_synced_lt.append(data_synced_lt_item)

    params["data_synced__lt"] = json_data_synced_lt

    json_data_synced_lte: Union[Unset, List[str]] = UNSET
    if not isinstance(data_synced_lte, Unset):
        json_data_synced_lte = []
        for data_synced_lte_item_data in data_synced_lte:
            data_synced_lte_item = data_synced_lte_item_data.isoformat()
            json_data_synced_lte.append(data_synced_lte_item)

    params["data_synced__lte"] = json_data_synced_lte

    json_data_synced_n: Union[Unset, List[str]] = UNSET
    if not isinstance(data_synced_n, Unset):
        json_data_synced_n = []
        for data_synced_n_item_data in data_synced_n:
            data_synced_n_item = data_synced_n_item_data.isoformat()
            json_data_synced_n.append(data_synced_n_item)

    params["data_synced__n"] = json_data_synced_n

    json_description: Union[Unset, List[str]] = UNSET
    if not isinstance(description, Unset):
        json_description = description

    params["description"] = json_description

    params["description__empty"] = description_empty

    json_description_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ic, Unset):
        json_description_ic = description_ic

    params["description__ic"] = json_description_ic

    json_description_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_ie, Unset):
        json_description_ie = description_ie

    params["description__ie"] = json_description_ie

    json_description_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_iew, Unset):
        json_description_iew = description_iew

    params["description__iew"] = json_description_iew

    json_description_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_isw, Unset):
        json_description_isw = description_isw

    params["description__isw"] = json_description_isw

    json_description_n: Union[Unset, List[str]] = UNSET
    if not isinstance(description_n, Unset):
        json_description_n = description_n

    params["description__n"] = json_description_n

    json_description_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nic, Unset):
        json_description_nic = description_nic

    params["description__nic"] = json_description_nic

    json_description_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nie, Unset):
        json_description_nie = description_nie

    params["description__nie"] = json_description_nie

    json_description_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(description_niew, Unset):
        json_description_niew = description_niew

    params["description__niew"] = json_description_niew

    json_description_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(description_nisw, Unset):
        json_description_nisw = description_nisw

    params["description__nisw"] = json_description_nisw

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    params["limit"] = limit

    json_name: Union[Unset, List[str]] = UNSET
    if not isinstance(name, Unset):
        json_name = name

    params["name"] = json_name

    params["name__empty"] = name_empty

    json_name_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ic, Unset):
        json_name_ic = name_ic

    params["name__ic"] = json_name_ic

    json_name_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ie, Unset):
        json_name_ie = name_ie

    params["name__ie"] = json_name_ie

    json_name_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_iew, Unset):
        json_name_iew = name_iew

    params["name__iew"] = json_name_iew

    json_name_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_isw, Unset):
        json_name_isw = name_isw

    params["name__isw"] = json_name_isw

    json_name_n: Union[Unset, List[str]] = UNSET
    if not isinstance(name_n, Unset):
        json_name_n = name_n

    params["name__n"] = json_name_n

    json_name_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nic, Unset):
        json_name_nic = name_nic

    params["name__nic"] = json_name_nic

    json_name_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nie, Unset):
        json_name_nie = name_nie

    params["name__nie"] = json_name_nie

    json_name_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_niew, Unset):
        json_name_niew = name_niew

    params["name__niew"] = json_name_niew

    json_name_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nisw, Unset):
        json_name_nisw = name_nisw

    params["name__nisw"] = json_name_nisw

    params["offset"] = offset

    params["ordering"] = ordering

    params["q"] = q

    json_tag: Union[Unset, List[str]] = UNSET
    if not isinstance(tag, Unset):
        json_tag = tag

    params["tag"] = json_tag

    json_tag_n: Union[Unset, List[str]] = UNSET
    if not isinstance(tag_n, Unset):
        json_tag_n = tag_n

    params["tag__n"] = json_tag_n

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/extras/config-templates/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedConfigTemplateList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedConfigTemplateList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedConfigTemplateList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    data_file_id: Union[Unset, List[Union[None, int]]] = UNSET,
    data_file_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    data_source_id: Union[Unset, List[Union[None, int]]] = UNSET,
    data_source_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    data_synced: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_empty: Union[Unset, bool] = UNSET,
    data_synced_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_n: Union[Unset, List[datetime.datetime]] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
) -> Response[PaginatedConfigTemplateList]:
    """Get a list of config template objects.

    Args:
        data_file_id (Union[Unset, List[Union[None, int]]]):
        data_file_id_n (Union[Unset, List[Union[None, int]]]):
        data_source_id (Union[Unset, List[Union[None, int]]]):
        data_source_id_n (Union[Unset, List[Union[None, int]]]):
        data_synced (Union[Unset, List[datetime.datetime]]):
        data_synced_empty (Union[Unset, bool]):
        data_synced_gt (Union[Unset, List[datetime.datetime]]):
        data_synced_gte (Union[Unset, List[datetime.datetime]]):
        data_synced_lt (Union[Unset, List[datetime.datetime]]):
        data_synced_lte (Union[Unset, List[datetime.datetime]]):
        data_synced_n (Union[Unset, List[datetime.datetime]]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedConfigTemplateList]
    """

    kwargs = _get_kwargs(
        data_file_id=data_file_id,
        data_file_id_n=data_file_id_n,
        data_source_id=data_source_id,
        data_source_id_n=data_source_id_n,
        data_synced=data_synced,
        data_synced_empty=data_synced_empty,
        data_synced_gt=data_synced_gt,
        data_synced_gte=data_synced_gte,
        data_synced_lt=data_synced_lt,
        data_synced_lte=data_synced_lte,
        data_synced_n=data_synced_n,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        q=q,
        tag=tag,
        tag_n=tag_n,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    data_file_id: Union[Unset, List[Union[None, int]]] = UNSET,
    data_file_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    data_source_id: Union[Unset, List[Union[None, int]]] = UNSET,
    data_source_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    data_synced: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_empty: Union[Unset, bool] = UNSET,
    data_synced_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_n: Union[Unset, List[datetime.datetime]] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
) -> Optional[PaginatedConfigTemplateList]:
    """Get a list of config template objects.

    Args:
        data_file_id (Union[Unset, List[Union[None, int]]]):
        data_file_id_n (Union[Unset, List[Union[None, int]]]):
        data_source_id (Union[Unset, List[Union[None, int]]]):
        data_source_id_n (Union[Unset, List[Union[None, int]]]):
        data_synced (Union[Unset, List[datetime.datetime]]):
        data_synced_empty (Union[Unset, bool]):
        data_synced_gt (Union[Unset, List[datetime.datetime]]):
        data_synced_gte (Union[Unset, List[datetime.datetime]]):
        data_synced_lt (Union[Unset, List[datetime.datetime]]):
        data_synced_lte (Union[Unset, List[datetime.datetime]]):
        data_synced_n (Union[Unset, List[datetime.datetime]]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedConfigTemplateList
    """

    return sync_detailed(
        client=client,
        data_file_id=data_file_id,
        data_file_id_n=data_file_id_n,
        data_source_id=data_source_id,
        data_source_id_n=data_source_id_n,
        data_synced=data_synced,
        data_synced_empty=data_synced_empty,
        data_synced_gt=data_synced_gt,
        data_synced_gte=data_synced_gte,
        data_synced_lt=data_synced_lt,
        data_synced_lte=data_synced_lte,
        data_synced_n=data_synced_n,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        q=q,
        tag=tag,
        tag_n=tag_n,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    data_file_id: Union[Unset, List[Union[None, int]]] = UNSET,
    data_file_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    data_source_id: Union[Unset, List[Union[None, int]]] = UNSET,
    data_source_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    data_synced: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_empty: Union[Unset, bool] = UNSET,
    data_synced_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_n: Union[Unset, List[datetime.datetime]] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
) -> Response[PaginatedConfigTemplateList]:
    """Get a list of config template objects.

    Args:
        data_file_id (Union[Unset, List[Union[None, int]]]):
        data_file_id_n (Union[Unset, List[Union[None, int]]]):
        data_source_id (Union[Unset, List[Union[None, int]]]):
        data_source_id_n (Union[Unset, List[Union[None, int]]]):
        data_synced (Union[Unset, List[datetime.datetime]]):
        data_synced_empty (Union[Unset, bool]):
        data_synced_gt (Union[Unset, List[datetime.datetime]]):
        data_synced_gte (Union[Unset, List[datetime.datetime]]):
        data_synced_lt (Union[Unset, List[datetime.datetime]]):
        data_synced_lte (Union[Unset, List[datetime.datetime]]):
        data_synced_n (Union[Unset, List[datetime.datetime]]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedConfigTemplateList]
    """

    kwargs = _get_kwargs(
        data_file_id=data_file_id,
        data_file_id_n=data_file_id_n,
        data_source_id=data_source_id,
        data_source_id_n=data_source_id_n,
        data_synced=data_synced,
        data_synced_empty=data_synced_empty,
        data_synced_gt=data_synced_gt,
        data_synced_gte=data_synced_gte,
        data_synced_lt=data_synced_lt,
        data_synced_lte=data_synced_lte,
        data_synced_n=data_synced_n,
        description=description,
        description_empty=description_empty,
        description_ic=description_ic,
        description_ie=description_ie,
        description_iew=description_iew,
        description_isw=description_isw,
        description_n=description_n,
        description_nic=description_nic,
        description_nie=description_nie,
        description_niew=description_niew,
        description_nisw=description_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        q=q,
        tag=tag,
        tag_n=tag_n,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    data_file_id: Union[Unset, List[Union[None, int]]] = UNSET,
    data_file_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    data_source_id: Union[Unset, List[Union[None, int]]] = UNSET,
    data_source_id_n: Union[Unset, List[Union[None, int]]] = UNSET,
    data_synced: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_empty: Union[Unset, bool] = UNSET,
    data_synced_gt: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_gte: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_lt: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_lte: Union[Unset, List[datetime.datetime]] = UNSET,
    data_synced_n: Union[Unset, List[datetime.datetime]] = UNSET,
    description: Union[Unset, List[str]] = UNSET,
    description_empty: Union[Unset, bool] = UNSET,
    description_ic: Union[Unset, List[str]] = UNSET,
    description_ie: Union[Unset, List[str]] = UNSET,
    description_iew: Union[Unset, List[str]] = UNSET,
    description_isw: Union[Unset, List[str]] = UNSET,
    description_n: Union[Unset, List[str]] = UNSET,
    description_nic: Union[Unset, List[str]] = UNSET,
    description_nie: Union[Unset, List[str]] = UNSET,
    description_niew: Union[Unset, List[str]] = UNSET,
    description_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    tag: Union[Unset, List[str]] = UNSET,
    tag_n: Union[Unset, List[str]] = UNSET,
) -> Optional[PaginatedConfigTemplateList]:
    """Get a list of config template objects.

    Args:
        data_file_id (Union[Unset, List[Union[None, int]]]):
        data_file_id_n (Union[Unset, List[Union[None, int]]]):
        data_source_id (Union[Unset, List[Union[None, int]]]):
        data_source_id_n (Union[Unset, List[Union[None, int]]]):
        data_synced (Union[Unset, List[datetime.datetime]]):
        data_synced_empty (Union[Unset, bool]):
        data_synced_gt (Union[Unset, List[datetime.datetime]]):
        data_synced_gte (Union[Unset, List[datetime.datetime]]):
        data_synced_lt (Union[Unset, List[datetime.datetime]]):
        data_synced_lte (Union[Unset, List[datetime.datetime]]):
        data_synced_n (Union[Unset, List[datetime.datetime]]):
        description (Union[Unset, List[str]]):
        description_empty (Union[Unset, bool]):
        description_ic (Union[Unset, List[str]]):
        description_ie (Union[Unset, List[str]]):
        description_iew (Union[Unset, List[str]]):
        description_isw (Union[Unset, List[str]]):
        description_n (Union[Unset, List[str]]):
        description_nic (Union[Unset, List[str]]):
        description_nie (Union[Unset, List[str]]):
        description_niew (Union[Unset, List[str]]):
        description_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        tag (Union[Unset, List[str]]):
        tag_n (Union[Unset, List[str]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedConfigTemplateList
    """

    return (
        await asyncio_detailed(
            client=client,
            data_file_id=data_file_id,
            data_file_id_n=data_file_id_n,
            data_source_id=data_source_id,
            data_source_id_n=data_source_id_n,
            data_synced=data_synced,
            data_synced_empty=data_synced_empty,
            data_synced_gt=data_synced_gt,
            data_synced_gte=data_synced_gte,
            data_synced_lt=data_synced_lt,
            data_synced_lte=data_synced_lte,
            data_synced_n=data_synced_n,
            description=description,
            description_empty=description_empty,
            description_ic=description_ic,
            description_ie=description_ie,
            description_iew=description_iew,
            description_isw=description_isw,
            description_n=description_n,
            description_nic=description_nic,
            description_nie=description_nie,
            description_niew=description_niew,
            description_nisw=description_nisw,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            limit=limit,
            name=name,
            name_empty=name_empty,
            name_ic=name_ic,
            name_ie=name_ie,
            name_iew=name_iew,
            name_isw=name_isw,
            name_n=name_n,
            name_nic=name_nic,
            name_nie=name_nie,
            name_niew=name_niew,
            name_nisw=name_nisw,
            offset=offset,
            ordering=ordering,
            q=q,
            tag=tag,
            tag_n=tag_n,
        )
    ).parsed
