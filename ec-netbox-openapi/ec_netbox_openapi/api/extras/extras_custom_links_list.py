from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_custom_link_list import PaginatedCustomLinkList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    content_type_id: Union[Unset, List[int]] = UNSET,
    content_type_id_empty: Union[Unset, List[int]] = UNSET,
    content_type_id_gt: Union[Unset, List[int]] = UNSET,
    content_type_id_gte: Union[Unset, List[int]] = UNSET,
    content_type_id_lt: Union[Unset, List[int]] = UNSET,
    content_type_id_lte: Union[Unset, List[int]] = UNSET,
    content_type_id_n: Union[Unset, List[int]] = UNSET,
    content_types: Union[Unset, str] = UNSET,
    content_types_ic: Union[Unset, str] = UNSET,
    content_types_ie: Union[Unset, str] = UNSET,
    content_types_iew: Union[Unset, str] = UNSET,
    content_types_isw: Union[Unset, str] = UNSET,
    content_types_n: Union[Unset, str] = UNSET,
    content_types_nic: Union[Unset, str] = UNSET,
    content_types_nie: Union[Unset, str] = UNSET,
    content_types_niew: Union[Unset, str] = UNSET,
    content_types_nisw: Union[Unset, str] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    group_name: Union[Unset, List[str]] = UNSET,
    group_name_empty: Union[Unset, bool] = UNSET,
    group_name_ic: Union[Unset, List[str]] = UNSET,
    group_name_ie: Union[Unset, List[str]] = UNSET,
    group_name_iew: Union[Unset, List[str]] = UNSET,
    group_name_isw: Union[Unset, List[str]] = UNSET,
    group_name_n: Union[Unset, List[str]] = UNSET,
    group_name_nic: Union[Unset, List[str]] = UNSET,
    group_name_nie: Union[Unset, List[str]] = UNSET,
    group_name_niew: Union[Unset, List[str]] = UNSET,
    group_name_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    link_text: Union[Unset, str] = UNSET,
    link_text_ic: Union[Unset, str] = UNSET,
    link_text_ie: Union[Unset, str] = UNSET,
    link_text_iew: Union[Unset, str] = UNSET,
    link_text_isw: Union[Unset, str] = UNSET,
    link_text_n: Union[Unset, str] = UNSET,
    link_text_nic: Union[Unset, str] = UNSET,
    link_text_nie: Union[Unset, str] = UNSET,
    link_text_niew: Union[Unset, str] = UNSET,
    link_text_nisw: Union[Unset, str] = UNSET,
    link_url: Union[Unset, str] = UNSET,
    link_url_ic: Union[Unset, str] = UNSET,
    link_url_ie: Union[Unset, str] = UNSET,
    link_url_iew: Union[Unset, str] = UNSET,
    link_url_isw: Union[Unset, str] = UNSET,
    link_url_n: Union[Unset, str] = UNSET,
    link_url_nic: Union[Unset, str] = UNSET,
    link_url_nie: Union[Unset, str] = UNSET,
    link_url_niew: Union[Unset, str] = UNSET,
    link_url_nisw: Union[Unset, str] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    new_window: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    weight: Union[Unset, List[int]] = UNSET,
    weight_empty: Union[Unset, bool] = UNSET,
    weight_gt: Union[Unset, List[int]] = UNSET,
    weight_gte: Union[Unset, List[int]] = UNSET,
    weight_lt: Union[Unset, List[int]] = UNSET,
    weight_lte: Union[Unset, List[int]] = UNSET,
    weight_n: Union[Unset, List[int]] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_content_type_id: Union[Unset, List[int]] = UNSET
    if not isinstance(content_type_id, Unset):
        json_content_type_id = content_type_id

    params["content_type_id"] = json_content_type_id

    json_content_type_id_empty: Union[Unset, List[int]] = UNSET
    if not isinstance(content_type_id_empty, Unset):
        json_content_type_id_empty = content_type_id_empty

    params["content_type_id__empty"] = json_content_type_id_empty

    json_content_type_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(content_type_id_gt, Unset):
        json_content_type_id_gt = content_type_id_gt

    params["content_type_id__gt"] = json_content_type_id_gt

    json_content_type_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(content_type_id_gte, Unset):
        json_content_type_id_gte = content_type_id_gte

    params["content_type_id__gte"] = json_content_type_id_gte

    json_content_type_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(content_type_id_lt, Unset):
        json_content_type_id_lt = content_type_id_lt

    params["content_type_id__lt"] = json_content_type_id_lt

    json_content_type_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(content_type_id_lte, Unset):
        json_content_type_id_lte = content_type_id_lte

    params["content_type_id__lte"] = json_content_type_id_lte

    json_content_type_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(content_type_id_n, Unset):
        json_content_type_id_n = content_type_id_n

    params["content_type_id__n"] = json_content_type_id_n

    params["content_types"] = content_types

    params["content_types__ic"] = content_types_ic

    params["content_types__ie"] = content_types_ie

    params["content_types__iew"] = content_types_iew

    params["content_types__isw"] = content_types_isw

    params["content_types__n"] = content_types_n

    params["content_types__nic"] = content_types_nic

    params["content_types__nie"] = content_types_nie

    params["content_types__niew"] = content_types_niew

    params["content_types__nisw"] = content_types_nisw

    params["enabled"] = enabled

    json_group_name: Union[Unset, List[str]] = UNSET
    if not isinstance(group_name, Unset):
        json_group_name = group_name

    params["group_name"] = json_group_name

    params["group_name__empty"] = group_name_empty

    json_group_name_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(group_name_ic, Unset):
        json_group_name_ic = group_name_ic

    params["group_name__ic"] = json_group_name_ic

    json_group_name_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(group_name_ie, Unset):
        json_group_name_ie = group_name_ie

    params["group_name__ie"] = json_group_name_ie

    json_group_name_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(group_name_iew, Unset):
        json_group_name_iew = group_name_iew

    params["group_name__iew"] = json_group_name_iew

    json_group_name_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(group_name_isw, Unset):
        json_group_name_isw = group_name_isw

    params["group_name__isw"] = json_group_name_isw

    json_group_name_n: Union[Unset, List[str]] = UNSET
    if not isinstance(group_name_n, Unset):
        json_group_name_n = group_name_n

    params["group_name__n"] = json_group_name_n

    json_group_name_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(group_name_nic, Unset):
        json_group_name_nic = group_name_nic

    params["group_name__nic"] = json_group_name_nic

    json_group_name_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(group_name_nie, Unset):
        json_group_name_nie = group_name_nie

    params["group_name__nie"] = json_group_name_nie

    json_group_name_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(group_name_niew, Unset):
        json_group_name_niew = group_name_niew

    params["group_name__niew"] = json_group_name_niew

    json_group_name_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(group_name_nisw, Unset):
        json_group_name_nisw = group_name_nisw

    params["group_name__nisw"] = json_group_name_nisw

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    params["limit"] = limit

    params["link_text"] = link_text

    params["link_text__ic"] = link_text_ic

    params["link_text__ie"] = link_text_ie

    params["link_text__iew"] = link_text_iew

    params["link_text__isw"] = link_text_isw

    params["link_text__n"] = link_text_n

    params["link_text__nic"] = link_text_nic

    params["link_text__nie"] = link_text_nie

    params["link_text__niew"] = link_text_niew

    params["link_text__nisw"] = link_text_nisw

    params["link_url"] = link_url

    params["link_url__ic"] = link_url_ic

    params["link_url__ie"] = link_url_ie

    params["link_url__iew"] = link_url_iew

    params["link_url__isw"] = link_url_isw

    params["link_url__n"] = link_url_n

    params["link_url__nic"] = link_url_nic

    params["link_url__nie"] = link_url_nie

    params["link_url__niew"] = link_url_niew

    params["link_url__nisw"] = link_url_nisw

    json_name: Union[Unset, List[str]] = UNSET
    if not isinstance(name, Unset):
        json_name = name

    params["name"] = json_name

    params["name__empty"] = name_empty

    json_name_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ic, Unset):
        json_name_ic = name_ic

    params["name__ic"] = json_name_ic

    json_name_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ie, Unset):
        json_name_ie = name_ie

    params["name__ie"] = json_name_ie

    json_name_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_iew, Unset):
        json_name_iew = name_iew

    params["name__iew"] = json_name_iew

    json_name_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_isw, Unset):
        json_name_isw = name_isw

    params["name__isw"] = json_name_isw

    json_name_n: Union[Unset, List[str]] = UNSET
    if not isinstance(name_n, Unset):
        json_name_n = name_n

    params["name__n"] = json_name_n

    json_name_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nic, Unset):
        json_name_nic = name_nic

    params["name__nic"] = json_name_nic

    json_name_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nie, Unset):
        json_name_nie = name_nie

    params["name__nie"] = json_name_nie

    json_name_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_niew, Unset):
        json_name_niew = name_niew

    params["name__niew"] = json_name_niew

    json_name_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nisw, Unset):
        json_name_nisw = name_nisw

    params["name__nisw"] = json_name_nisw

    params["new_window"] = new_window

    params["offset"] = offset

    params["ordering"] = ordering

    params["q"] = q

    json_weight: Union[Unset, List[int]] = UNSET
    if not isinstance(weight, Unset):
        json_weight = weight

    params["weight"] = json_weight

    params["weight__empty"] = weight_empty

    json_weight_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(weight_gt, Unset):
        json_weight_gt = weight_gt

    params["weight__gt"] = json_weight_gt

    json_weight_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(weight_gte, Unset):
        json_weight_gte = weight_gte

    params["weight__gte"] = json_weight_gte

    json_weight_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(weight_lt, Unset):
        json_weight_lt = weight_lt

    params["weight__lt"] = json_weight_lt

    json_weight_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(weight_lte, Unset):
        json_weight_lte = weight_lte

    params["weight__lte"] = json_weight_lte

    json_weight_n: Union[Unset, List[int]] = UNSET
    if not isinstance(weight_n, Unset):
        json_weight_n = weight_n

    params["weight__n"] = json_weight_n

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/extras/custom-links/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedCustomLinkList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedCustomLinkList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedCustomLinkList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    content_type_id: Union[Unset, List[int]] = UNSET,
    content_type_id_empty: Union[Unset, List[int]] = UNSET,
    content_type_id_gt: Union[Unset, List[int]] = UNSET,
    content_type_id_gte: Union[Unset, List[int]] = UNSET,
    content_type_id_lt: Union[Unset, List[int]] = UNSET,
    content_type_id_lte: Union[Unset, List[int]] = UNSET,
    content_type_id_n: Union[Unset, List[int]] = UNSET,
    content_types: Union[Unset, str] = UNSET,
    content_types_ic: Union[Unset, str] = UNSET,
    content_types_ie: Union[Unset, str] = UNSET,
    content_types_iew: Union[Unset, str] = UNSET,
    content_types_isw: Union[Unset, str] = UNSET,
    content_types_n: Union[Unset, str] = UNSET,
    content_types_nic: Union[Unset, str] = UNSET,
    content_types_nie: Union[Unset, str] = UNSET,
    content_types_niew: Union[Unset, str] = UNSET,
    content_types_nisw: Union[Unset, str] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    group_name: Union[Unset, List[str]] = UNSET,
    group_name_empty: Union[Unset, bool] = UNSET,
    group_name_ic: Union[Unset, List[str]] = UNSET,
    group_name_ie: Union[Unset, List[str]] = UNSET,
    group_name_iew: Union[Unset, List[str]] = UNSET,
    group_name_isw: Union[Unset, List[str]] = UNSET,
    group_name_n: Union[Unset, List[str]] = UNSET,
    group_name_nic: Union[Unset, List[str]] = UNSET,
    group_name_nie: Union[Unset, List[str]] = UNSET,
    group_name_niew: Union[Unset, List[str]] = UNSET,
    group_name_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    link_text: Union[Unset, str] = UNSET,
    link_text_ic: Union[Unset, str] = UNSET,
    link_text_ie: Union[Unset, str] = UNSET,
    link_text_iew: Union[Unset, str] = UNSET,
    link_text_isw: Union[Unset, str] = UNSET,
    link_text_n: Union[Unset, str] = UNSET,
    link_text_nic: Union[Unset, str] = UNSET,
    link_text_nie: Union[Unset, str] = UNSET,
    link_text_niew: Union[Unset, str] = UNSET,
    link_text_nisw: Union[Unset, str] = UNSET,
    link_url: Union[Unset, str] = UNSET,
    link_url_ic: Union[Unset, str] = UNSET,
    link_url_ie: Union[Unset, str] = UNSET,
    link_url_iew: Union[Unset, str] = UNSET,
    link_url_isw: Union[Unset, str] = UNSET,
    link_url_n: Union[Unset, str] = UNSET,
    link_url_nic: Union[Unset, str] = UNSET,
    link_url_nie: Union[Unset, str] = UNSET,
    link_url_niew: Union[Unset, str] = UNSET,
    link_url_nisw: Union[Unset, str] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    new_window: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    weight: Union[Unset, List[int]] = UNSET,
    weight_empty: Union[Unset, bool] = UNSET,
    weight_gt: Union[Unset, List[int]] = UNSET,
    weight_gte: Union[Unset, List[int]] = UNSET,
    weight_lt: Union[Unset, List[int]] = UNSET,
    weight_lte: Union[Unset, List[int]] = UNSET,
    weight_n: Union[Unset, List[int]] = UNSET,
) -> Response[PaginatedCustomLinkList]:
    """Get a list of custom link objects.

    Args:
        content_type_id (Union[Unset, List[int]]):
        content_type_id_empty (Union[Unset, List[int]]):
        content_type_id_gt (Union[Unset, List[int]]):
        content_type_id_gte (Union[Unset, List[int]]):
        content_type_id_lt (Union[Unset, List[int]]):
        content_type_id_lte (Union[Unset, List[int]]):
        content_type_id_n (Union[Unset, List[int]]):
        content_types (Union[Unset, str]):
        content_types_ic (Union[Unset, str]):
        content_types_ie (Union[Unset, str]):
        content_types_iew (Union[Unset, str]):
        content_types_isw (Union[Unset, str]):
        content_types_n (Union[Unset, str]):
        content_types_nic (Union[Unset, str]):
        content_types_nie (Union[Unset, str]):
        content_types_niew (Union[Unset, str]):
        content_types_nisw (Union[Unset, str]):
        enabled (Union[Unset, bool]):
        group_name (Union[Unset, List[str]]):
        group_name_empty (Union[Unset, bool]):
        group_name_ic (Union[Unset, List[str]]):
        group_name_ie (Union[Unset, List[str]]):
        group_name_iew (Union[Unset, List[str]]):
        group_name_isw (Union[Unset, List[str]]):
        group_name_n (Union[Unset, List[str]]):
        group_name_nic (Union[Unset, List[str]]):
        group_name_nie (Union[Unset, List[str]]):
        group_name_niew (Union[Unset, List[str]]):
        group_name_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        link_text (Union[Unset, str]):
        link_text_ic (Union[Unset, str]):
        link_text_ie (Union[Unset, str]):
        link_text_iew (Union[Unset, str]):
        link_text_isw (Union[Unset, str]):
        link_text_n (Union[Unset, str]):
        link_text_nic (Union[Unset, str]):
        link_text_nie (Union[Unset, str]):
        link_text_niew (Union[Unset, str]):
        link_text_nisw (Union[Unset, str]):
        link_url (Union[Unset, str]):
        link_url_ic (Union[Unset, str]):
        link_url_ie (Union[Unset, str]):
        link_url_iew (Union[Unset, str]):
        link_url_isw (Union[Unset, str]):
        link_url_n (Union[Unset, str]):
        link_url_nic (Union[Unset, str]):
        link_url_nie (Union[Unset, str]):
        link_url_niew (Union[Unset, str]):
        link_url_nisw (Union[Unset, str]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        new_window (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        weight (Union[Unset, List[int]]):
        weight_empty (Union[Unset, bool]):
        weight_gt (Union[Unset, List[int]]):
        weight_gte (Union[Unset, List[int]]):
        weight_lt (Union[Unset, List[int]]):
        weight_lte (Union[Unset, List[int]]):
        weight_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCustomLinkList]
    """

    kwargs = _get_kwargs(
        content_type_id=content_type_id,
        content_type_id_empty=content_type_id_empty,
        content_type_id_gt=content_type_id_gt,
        content_type_id_gte=content_type_id_gte,
        content_type_id_lt=content_type_id_lt,
        content_type_id_lte=content_type_id_lte,
        content_type_id_n=content_type_id_n,
        content_types=content_types,
        content_types_ic=content_types_ic,
        content_types_ie=content_types_ie,
        content_types_iew=content_types_iew,
        content_types_isw=content_types_isw,
        content_types_n=content_types_n,
        content_types_nic=content_types_nic,
        content_types_nie=content_types_nie,
        content_types_niew=content_types_niew,
        content_types_nisw=content_types_nisw,
        enabled=enabled,
        group_name=group_name,
        group_name_empty=group_name_empty,
        group_name_ic=group_name_ic,
        group_name_ie=group_name_ie,
        group_name_iew=group_name_iew,
        group_name_isw=group_name_isw,
        group_name_n=group_name_n,
        group_name_nic=group_name_nic,
        group_name_nie=group_name_nie,
        group_name_niew=group_name_niew,
        group_name_nisw=group_name_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        link_text=link_text,
        link_text_ic=link_text_ic,
        link_text_ie=link_text_ie,
        link_text_iew=link_text_iew,
        link_text_isw=link_text_isw,
        link_text_n=link_text_n,
        link_text_nic=link_text_nic,
        link_text_nie=link_text_nie,
        link_text_niew=link_text_niew,
        link_text_nisw=link_text_nisw,
        link_url=link_url,
        link_url_ic=link_url_ic,
        link_url_ie=link_url_ie,
        link_url_iew=link_url_iew,
        link_url_isw=link_url_isw,
        link_url_n=link_url_n,
        link_url_nic=link_url_nic,
        link_url_nie=link_url_nie,
        link_url_niew=link_url_niew,
        link_url_nisw=link_url_nisw,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        new_window=new_window,
        offset=offset,
        ordering=ordering,
        q=q,
        weight=weight,
        weight_empty=weight_empty,
        weight_gt=weight_gt,
        weight_gte=weight_gte,
        weight_lt=weight_lt,
        weight_lte=weight_lte,
        weight_n=weight_n,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    content_type_id: Union[Unset, List[int]] = UNSET,
    content_type_id_empty: Union[Unset, List[int]] = UNSET,
    content_type_id_gt: Union[Unset, List[int]] = UNSET,
    content_type_id_gte: Union[Unset, List[int]] = UNSET,
    content_type_id_lt: Union[Unset, List[int]] = UNSET,
    content_type_id_lte: Union[Unset, List[int]] = UNSET,
    content_type_id_n: Union[Unset, List[int]] = UNSET,
    content_types: Union[Unset, str] = UNSET,
    content_types_ic: Union[Unset, str] = UNSET,
    content_types_ie: Union[Unset, str] = UNSET,
    content_types_iew: Union[Unset, str] = UNSET,
    content_types_isw: Union[Unset, str] = UNSET,
    content_types_n: Union[Unset, str] = UNSET,
    content_types_nic: Union[Unset, str] = UNSET,
    content_types_nie: Union[Unset, str] = UNSET,
    content_types_niew: Union[Unset, str] = UNSET,
    content_types_nisw: Union[Unset, str] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    group_name: Union[Unset, List[str]] = UNSET,
    group_name_empty: Union[Unset, bool] = UNSET,
    group_name_ic: Union[Unset, List[str]] = UNSET,
    group_name_ie: Union[Unset, List[str]] = UNSET,
    group_name_iew: Union[Unset, List[str]] = UNSET,
    group_name_isw: Union[Unset, List[str]] = UNSET,
    group_name_n: Union[Unset, List[str]] = UNSET,
    group_name_nic: Union[Unset, List[str]] = UNSET,
    group_name_nie: Union[Unset, List[str]] = UNSET,
    group_name_niew: Union[Unset, List[str]] = UNSET,
    group_name_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    link_text: Union[Unset, str] = UNSET,
    link_text_ic: Union[Unset, str] = UNSET,
    link_text_ie: Union[Unset, str] = UNSET,
    link_text_iew: Union[Unset, str] = UNSET,
    link_text_isw: Union[Unset, str] = UNSET,
    link_text_n: Union[Unset, str] = UNSET,
    link_text_nic: Union[Unset, str] = UNSET,
    link_text_nie: Union[Unset, str] = UNSET,
    link_text_niew: Union[Unset, str] = UNSET,
    link_text_nisw: Union[Unset, str] = UNSET,
    link_url: Union[Unset, str] = UNSET,
    link_url_ic: Union[Unset, str] = UNSET,
    link_url_ie: Union[Unset, str] = UNSET,
    link_url_iew: Union[Unset, str] = UNSET,
    link_url_isw: Union[Unset, str] = UNSET,
    link_url_n: Union[Unset, str] = UNSET,
    link_url_nic: Union[Unset, str] = UNSET,
    link_url_nie: Union[Unset, str] = UNSET,
    link_url_niew: Union[Unset, str] = UNSET,
    link_url_nisw: Union[Unset, str] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    new_window: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    weight: Union[Unset, List[int]] = UNSET,
    weight_empty: Union[Unset, bool] = UNSET,
    weight_gt: Union[Unset, List[int]] = UNSET,
    weight_gte: Union[Unset, List[int]] = UNSET,
    weight_lt: Union[Unset, List[int]] = UNSET,
    weight_lte: Union[Unset, List[int]] = UNSET,
    weight_n: Union[Unset, List[int]] = UNSET,
) -> Optional[PaginatedCustomLinkList]:
    """Get a list of custom link objects.

    Args:
        content_type_id (Union[Unset, List[int]]):
        content_type_id_empty (Union[Unset, List[int]]):
        content_type_id_gt (Union[Unset, List[int]]):
        content_type_id_gte (Union[Unset, List[int]]):
        content_type_id_lt (Union[Unset, List[int]]):
        content_type_id_lte (Union[Unset, List[int]]):
        content_type_id_n (Union[Unset, List[int]]):
        content_types (Union[Unset, str]):
        content_types_ic (Union[Unset, str]):
        content_types_ie (Union[Unset, str]):
        content_types_iew (Union[Unset, str]):
        content_types_isw (Union[Unset, str]):
        content_types_n (Union[Unset, str]):
        content_types_nic (Union[Unset, str]):
        content_types_nie (Union[Unset, str]):
        content_types_niew (Union[Unset, str]):
        content_types_nisw (Union[Unset, str]):
        enabled (Union[Unset, bool]):
        group_name (Union[Unset, List[str]]):
        group_name_empty (Union[Unset, bool]):
        group_name_ic (Union[Unset, List[str]]):
        group_name_ie (Union[Unset, List[str]]):
        group_name_iew (Union[Unset, List[str]]):
        group_name_isw (Union[Unset, List[str]]):
        group_name_n (Union[Unset, List[str]]):
        group_name_nic (Union[Unset, List[str]]):
        group_name_nie (Union[Unset, List[str]]):
        group_name_niew (Union[Unset, List[str]]):
        group_name_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        link_text (Union[Unset, str]):
        link_text_ic (Union[Unset, str]):
        link_text_ie (Union[Unset, str]):
        link_text_iew (Union[Unset, str]):
        link_text_isw (Union[Unset, str]):
        link_text_n (Union[Unset, str]):
        link_text_nic (Union[Unset, str]):
        link_text_nie (Union[Unset, str]):
        link_text_niew (Union[Unset, str]):
        link_text_nisw (Union[Unset, str]):
        link_url (Union[Unset, str]):
        link_url_ic (Union[Unset, str]):
        link_url_ie (Union[Unset, str]):
        link_url_iew (Union[Unset, str]):
        link_url_isw (Union[Unset, str]):
        link_url_n (Union[Unset, str]):
        link_url_nic (Union[Unset, str]):
        link_url_nie (Union[Unset, str]):
        link_url_niew (Union[Unset, str]):
        link_url_nisw (Union[Unset, str]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        new_window (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        weight (Union[Unset, List[int]]):
        weight_empty (Union[Unset, bool]):
        weight_gt (Union[Unset, List[int]]):
        weight_gte (Union[Unset, List[int]]):
        weight_lt (Union[Unset, List[int]]):
        weight_lte (Union[Unset, List[int]]):
        weight_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCustomLinkList
    """

    return sync_detailed(
        client=client,
        content_type_id=content_type_id,
        content_type_id_empty=content_type_id_empty,
        content_type_id_gt=content_type_id_gt,
        content_type_id_gte=content_type_id_gte,
        content_type_id_lt=content_type_id_lt,
        content_type_id_lte=content_type_id_lte,
        content_type_id_n=content_type_id_n,
        content_types=content_types,
        content_types_ic=content_types_ic,
        content_types_ie=content_types_ie,
        content_types_iew=content_types_iew,
        content_types_isw=content_types_isw,
        content_types_n=content_types_n,
        content_types_nic=content_types_nic,
        content_types_nie=content_types_nie,
        content_types_niew=content_types_niew,
        content_types_nisw=content_types_nisw,
        enabled=enabled,
        group_name=group_name,
        group_name_empty=group_name_empty,
        group_name_ic=group_name_ic,
        group_name_ie=group_name_ie,
        group_name_iew=group_name_iew,
        group_name_isw=group_name_isw,
        group_name_n=group_name_n,
        group_name_nic=group_name_nic,
        group_name_nie=group_name_nie,
        group_name_niew=group_name_niew,
        group_name_nisw=group_name_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        link_text=link_text,
        link_text_ic=link_text_ic,
        link_text_ie=link_text_ie,
        link_text_iew=link_text_iew,
        link_text_isw=link_text_isw,
        link_text_n=link_text_n,
        link_text_nic=link_text_nic,
        link_text_nie=link_text_nie,
        link_text_niew=link_text_niew,
        link_text_nisw=link_text_nisw,
        link_url=link_url,
        link_url_ic=link_url_ic,
        link_url_ie=link_url_ie,
        link_url_iew=link_url_iew,
        link_url_isw=link_url_isw,
        link_url_n=link_url_n,
        link_url_nic=link_url_nic,
        link_url_nie=link_url_nie,
        link_url_niew=link_url_niew,
        link_url_nisw=link_url_nisw,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        new_window=new_window,
        offset=offset,
        ordering=ordering,
        q=q,
        weight=weight,
        weight_empty=weight_empty,
        weight_gt=weight_gt,
        weight_gte=weight_gte,
        weight_lt=weight_lt,
        weight_lte=weight_lte,
        weight_n=weight_n,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    content_type_id: Union[Unset, List[int]] = UNSET,
    content_type_id_empty: Union[Unset, List[int]] = UNSET,
    content_type_id_gt: Union[Unset, List[int]] = UNSET,
    content_type_id_gte: Union[Unset, List[int]] = UNSET,
    content_type_id_lt: Union[Unset, List[int]] = UNSET,
    content_type_id_lte: Union[Unset, List[int]] = UNSET,
    content_type_id_n: Union[Unset, List[int]] = UNSET,
    content_types: Union[Unset, str] = UNSET,
    content_types_ic: Union[Unset, str] = UNSET,
    content_types_ie: Union[Unset, str] = UNSET,
    content_types_iew: Union[Unset, str] = UNSET,
    content_types_isw: Union[Unset, str] = UNSET,
    content_types_n: Union[Unset, str] = UNSET,
    content_types_nic: Union[Unset, str] = UNSET,
    content_types_nie: Union[Unset, str] = UNSET,
    content_types_niew: Union[Unset, str] = UNSET,
    content_types_nisw: Union[Unset, str] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    group_name: Union[Unset, List[str]] = UNSET,
    group_name_empty: Union[Unset, bool] = UNSET,
    group_name_ic: Union[Unset, List[str]] = UNSET,
    group_name_ie: Union[Unset, List[str]] = UNSET,
    group_name_iew: Union[Unset, List[str]] = UNSET,
    group_name_isw: Union[Unset, List[str]] = UNSET,
    group_name_n: Union[Unset, List[str]] = UNSET,
    group_name_nic: Union[Unset, List[str]] = UNSET,
    group_name_nie: Union[Unset, List[str]] = UNSET,
    group_name_niew: Union[Unset, List[str]] = UNSET,
    group_name_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    link_text: Union[Unset, str] = UNSET,
    link_text_ic: Union[Unset, str] = UNSET,
    link_text_ie: Union[Unset, str] = UNSET,
    link_text_iew: Union[Unset, str] = UNSET,
    link_text_isw: Union[Unset, str] = UNSET,
    link_text_n: Union[Unset, str] = UNSET,
    link_text_nic: Union[Unset, str] = UNSET,
    link_text_nie: Union[Unset, str] = UNSET,
    link_text_niew: Union[Unset, str] = UNSET,
    link_text_nisw: Union[Unset, str] = UNSET,
    link_url: Union[Unset, str] = UNSET,
    link_url_ic: Union[Unset, str] = UNSET,
    link_url_ie: Union[Unset, str] = UNSET,
    link_url_iew: Union[Unset, str] = UNSET,
    link_url_isw: Union[Unset, str] = UNSET,
    link_url_n: Union[Unset, str] = UNSET,
    link_url_nic: Union[Unset, str] = UNSET,
    link_url_nie: Union[Unset, str] = UNSET,
    link_url_niew: Union[Unset, str] = UNSET,
    link_url_nisw: Union[Unset, str] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    new_window: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    weight: Union[Unset, List[int]] = UNSET,
    weight_empty: Union[Unset, bool] = UNSET,
    weight_gt: Union[Unset, List[int]] = UNSET,
    weight_gte: Union[Unset, List[int]] = UNSET,
    weight_lt: Union[Unset, List[int]] = UNSET,
    weight_lte: Union[Unset, List[int]] = UNSET,
    weight_n: Union[Unset, List[int]] = UNSET,
) -> Response[PaginatedCustomLinkList]:
    """Get a list of custom link objects.

    Args:
        content_type_id (Union[Unset, List[int]]):
        content_type_id_empty (Union[Unset, List[int]]):
        content_type_id_gt (Union[Unset, List[int]]):
        content_type_id_gte (Union[Unset, List[int]]):
        content_type_id_lt (Union[Unset, List[int]]):
        content_type_id_lte (Union[Unset, List[int]]):
        content_type_id_n (Union[Unset, List[int]]):
        content_types (Union[Unset, str]):
        content_types_ic (Union[Unset, str]):
        content_types_ie (Union[Unset, str]):
        content_types_iew (Union[Unset, str]):
        content_types_isw (Union[Unset, str]):
        content_types_n (Union[Unset, str]):
        content_types_nic (Union[Unset, str]):
        content_types_nie (Union[Unset, str]):
        content_types_niew (Union[Unset, str]):
        content_types_nisw (Union[Unset, str]):
        enabled (Union[Unset, bool]):
        group_name (Union[Unset, List[str]]):
        group_name_empty (Union[Unset, bool]):
        group_name_ic (Union[Unset, List[str]]):
        group_name_ie (Union[Unset, List[str]]):
        group_name_iew (Union[Unset, List[str]]):
        group_name_isw (Union[Unset, List[str]]):
        group_name_n (Union[Unset, List[str]]):
        group_name_nic (Union[Unset, List[str]]):
        group_name_nie (Union[Unset, List[str]]):
        group_name_niew (Union[Unset, List[str]]):
        group_name_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        link_text (Union[Unset, str]):
        link_text_ic (Union[Unset, str]):
        link_text_ie (Union[Unset, str]):
        link_text_iew (Union[Unset, str]):
        link_text_isw (Union[Unset, str]):
        link_text_n (Union[Unset, str]):
        link_text_nic (Union[Unset, str]):
        link_text_nie (Union[Unset, str]):
        link_text_niew (Union[Unset, str]):
        link_text_nisw (Union[Unset, str]):
        link_url (Union[Unset, str]):
        link_url_ic (Union[Unset, str]):
        link_url_ie (Union[Unset, str]):
        link_url_iew (Union[Unset, str]):
        link_url_isw (Union[Unset, str]):
        link_url_n (Union[Unset, str]):
        link_url_nic (Union[Unset, str]):
        link_url_nie (Union[Unset, str]):
        link_url_niew (Union[Unset, str]):
        link_url_nisw (Union[Unset, str]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        new_window (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        weight (Union[Unset, List[int]]):
        weight_empty (Union[Unset, bool]):
        weight_gt (Union[Unset, List[int]]):
        weight_gte (Union[Unset, List[int]]):
        weight_lt (Union[Unset, List[int]]):
        weight_lte (Union[Unset, List[int]]):
        weight_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedCustomLinkList]
    """

    kwargs = _get_kwargs(
        content_type_id=content_type_id,
        content_type_id_empty=content_type_id_empty,
        content_type_id_gt=content_type_id_gt,
        content_type_id_gte=content_type_id_gte,
        content_type_id_lt=content_type_id_lt,
        content_type_id_lte=content_type_id_lte,
        content_type_id_n=content_type_id_n,
        content_types=content_types,
        content_types_ic=content_types_ic,
        content_types_ie=content_types_ie,
        content_types_iew=content_types_iew,
        content_types_isw=content_types_isw,
        content_types_n=content_types_n,
        content_types_nic=content_types_nic,
        content_types_nie=content_types_nie,
        content_types_niew=content_types_niew,
        content_types_nisw=content_types_nisw,
        enabled=enabled,
        group_name=group_name,
        group_name_empty=group_name_empty,
        group_name_ic=group_name_ic,
        group_name_ie=group_name_ie,
        group_name_iew=group_name_iew,
        group_name_isw=group_name_isw,
        group_name_n=group_name_n,
        group_name_nic=group_name_nic,
        group_name_nie=group_name_nie,
        group_name_niew=group_name_niew,
        group_name_nisw=group_name_nisw,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        link_text=link_text,
        link_text_ic=link_text_ic,
        link_text_ie=link_text_ie,
        link_text_iew=link_text_iew,
        link_text_isw=link_text_isw,
        link_text_n=link_text_n,
        link_text_nic=link_text_nic,
        link_text_nie=link_text_nie,
        link_text_niew=link_text_niew,
        link_text_nisw=link_text_nisw,
        link_url=link_url,
        link_url_ic=link_url_ic,
        link_url_ie=link_url_ie,
        link_url_iew=link_url_iew,
        link_url_isw=link_url_isw,
        link_url_n=link_url_n,
        link_url_nic=link_url_nic,
        link_url_nie=link_url_nie,
        link_url_niew=link_url_niew,
        link_url_nisw=link_url_nisw,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        new_window=new_window,
        offset=offset,
        ordering=ordering,
        q=q,
        weight=weight,
        weight_empty=weight_empty,
        weight_gt=weight_gt,
        weight_gte=weight_gte,
        weight_lt=weight_lt,
        weight_lte=weight_lte,
        weight_n=weight_n,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    content_type_id: Union[Unset, List[int]] = UNSET,
    content_type_id_empty: Union[Unset, List[int]] = UNSET,
    content_type_id_gt: Union[Unset, List[int]] = UNSET,
    content_type_id_gte: Union[Unset, List[int]] = UNSET,
    content_type_id_lt: Union[Unset, List[int]] = UNSET,
    content_type_id_lte: Union[Unset, List[int]] = UNSET,
    content_type_id_n: Union[Unset, List[int]] = UNSET,
    content_types: Union[Unset, str] = UNSET,
    content_types_ic: Union[Unset, str] = UNSET,
    content_types_ie: Union[Unset, str] = UNSET,
    content_types_iew: Union[Unset, str] = UNSET,
    content_types_isw: Union[Unset, str] = UNSET,
    content_types_n: Union[Unset, str] = UNSET,
    content_types_nic: Union[Unset, str] = UNSET,
    content_types_nie: Union[Unset, str] = UNSET,
    content_types_niew: Union[Unset, str] = UNSET,
    content_types_nisw: Union[Unset, str] = UNSET,
    enabled: Union[Unset, bool] = UNSET,
    group_name: Union[Unset, List[str]] = UNSET,
    group_name_empty: Union[Unset, bool] = UNSET,
    group_name_ic: Union[Unset, List[str]] = UNSET,
    group_name_ie: Union[Unset, List[str]] = UNSET,
    group_name_iew: Union[Unset, List[str]] = UNSET,
    group_name_isw: Union[Unset, List[str]] = UNSET,
    group_name_n: Union[Unset, List[str]] = UNSET,
    group_name_nic: Union[Unset, List[str]] = UNSET,
    group_name_nie: Union[Unset, List[str]] = UNSET,
    group_name_niew: Union[Unset, List[str]] = UNSET,
    group_name_nisw: Union[Unset, List[str]] = UNSET,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    link_text: Union[Unset, str] = UNSET,
    link_text_ic: Union[Unset, str] = UNSET,
    link_text_ie: Union[Unset, str] = UNSET,
    link_text_iew: Union[Unset, str] = UNSET,
    link_text_isw: Union[Unset, str] = UNSET,
    link_text_n: Union[Unset, str] = UNSET,
    link_text_nic: Union[Unset, str] = UNSET,
    link_text_nie: Union[Unset, str] = UNSET,
    link_text_niew: Union[Unset, str] = UNSET,
    link_text_nisw: Union[Unset, str] = UNSET,
    link_url: Union[Unset, str] = UNSET,
    link_url_ic: Union[Unset, str] = UNSET,
    link_url_ie: Union[Unset, str] = UNSET,
    link_url_iew: Union[Unset, str] = UNSET,
    link_url_isw: Union[Unset, str] = UNSET,
    link_url_n: Union[Unset, str] = UNSET,
    link_url_nic: Union[Unset, str] = UNSET,
    link_url_nie: Union[Unset, str] = UNSET,
    link_url_niew: Union[Unset, str] = UNSET,
    link_url_nisw: Union[Unset, str] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    new_window: Union[Unset, bool] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
    weight: Union[Unset, List[int]] = UNSET,
    weight_empty: Union[Unset, bool] = UNSET,
    weight_gt: Union[Unset, List[int]] = UNSET,
    weight_gte: Union[Unset, List[int]] = UNSET,
    weight_lt: Union[Unset, List[int]] = UNSET,
    weight_lte: Union[Unset, List[int]] = UNSET,
    weight_n: Union[Unset, List[int]] = UNSET,
) -> Optional[PaginatedCustomLinkList]:
    """Get a list of custom link objects.

    Args:
        content_type_id (Union[Unset, List[int]]):
        content_type_id_empty (Union[Unset, List[int]]):
        content_type_id_gt (Union[Unset, List[int]]):
        content_type_id_gte (Union[Unset, List[int]]):
        content_type_id_lt (Union[Unset, List[int]]):
        content_type_id_lte (Union[Unset, List[int]]):
        content_type_id_n (Union[Unset, List[int]]):
        content_types (Union[Unset, str]):
        content_types_ic (Union[Unset, str]):
        content_types_ie (Union[Unset, str]):
        content_types_iew (Union[Unset, str]):
        content_types_isw (Union[Unset, str]):
        content_types_n (Union[Unset, str]):
        content_types_nic (Union[Unset, str]):
        content_types_nie (Union[Unset, str]):
        content_types_niew (Union[Unset, str]):
        content_types_nisw (Union[Unset, str]):
        enabled (Union[Unset, bool]):
        group_name (Union[Unset, List[str]]):
        group_name_empty (Union[Unset, bool]):
        group_name_ic (Union[Unset, List[str]]):
        group_name_ie (Union[Unset, List[str]]):
        group_name_iew (Union[Unset, List[str]]):
        group_name_isw (Union[Unset, List[str]]):
        group_name_n (Union[Unset, List[str]]):
        group_name_nic (Union[Unset, List[str]]):
        group_name_nie (Union[Unset, List[str]]):
        group_name_niew (Union[Unset, List[str]]):
        group_name_nisw (Union[Unset, List[str]]):
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        link_text (Union[Unset, str]):
        link_text_ic (Union[Unset, str]):
        link_text_ie (Union[Unset, str]):
        link_text_iew (Union[Unset, str]):
        link_text_isw (Union[Unset, str]):
        link_text_n (Union[Unset, str]):
        link_text_nic (Union[Unset, str]):
        link_text_nie (Union[Unset, str]):
        link_text_niew (Union[Unset, str]):
        link_text_nisw (Union[Unset, str]):
        link_url (Union[Unset, str]):
        link_url_ic (Union[Unset, str]):
        link_url_ie (Union[Unset, str]):
        link_url_iew (Union[Unset, str]):
        link_url_isw (Union[Unset, str]):
        link_url_n (Union[Unset, str]):
        link_url_nic (Union[Unset, str]):
        link_url_nie (Union[Unset, str]):
        link_url_niew (Union[Unset, str]):
        link_url_nisw (Union[Unset, str]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        new_window (Union[Unset, bool]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):
        weight (Union[Unset, List[int]]):
        weight_empty (Union[Unset, bool]):
        weight_gt (Union[Unset, List[int]]):
        weight_gte (Union[Unset, List[int]]):
        weight_lt (Union[Unset, List[int]]):
        weight_lte (Union[Unset, List[int]]):
        weight_n (Union[Unset, List[int]]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedCustomLinkList
    """

    return (
        await asyncio_detailed(
            client=client,
            content_type_id=content_type_id,
            content_type_id_empty=content_type_id_empty,
            content_type_id_gt=content_type_id_gt,
            content_type_id_gte=content_type_id_gte,
            content_type_id_lt=content_type_id_lt,
            content_type_id_lte=content_type_id_lte,
            content_type_id_n=content_type_id_n,
            content_types=content_types,
            content_types_ic=content_types_ic,
            content_types_ie=content_types_ie,
            content_types_iew=content_types_iew,
            content_types_isw=content_types_isw,
            content_types_n=content_types_n,
            content_types_nic=content_types_nic,
            content_types_nie=content_types_nie,
            content_types_niew=content_types_niew,
            content_types_nisw=content_types_nisw,
            enabled=enabled,
            group_name=group_name,
            group_name_empty=group_name_empty,
            group_name_ic=group_name_ic,
            group_name_ie=group_name_ie,
            group_name_iew=group_name_iew,
            group_name_isw=group_name_isw,
            group_name_n=group_name_n,
            group_name_nic=group_name_nic,
            group_name_nie=group_name_nie,
            group_name_niew=group_name_niew,
            group_name_nisw=group_name_nisw,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            limit=limit,
            link_text=link_text,
            link_text_ic=link_text_ic,
            link_text_ie=link_text_ie,
            link_text_iew=link_text_iew,
            link_text_isw=link_text_isw,
            link_text_n=link_text_n,
            link_text_nic=link_text_nic,
            link_text_nie=link_text_nie,
            link_text_niew=link_text_niew,
            link_text_nisw=link_text_nisw,
            link_url=link_url,
            link_url_ic=link_url_ic,
            link_url_ie=link_url_ie,
            link_url_iew=link_url_iew,
            link_url_isw=link_url_isw,
            link_url_n=link_url_n,
            link_url_nic=link_url_nic,
            link_url_nie=link_url_nie,
            link_url_niew=link_url_niew,
            link_url_nisw=link_url_nisw,
            name=name,
            name_empty=name_empty,
            name_ic=name_ic,
            name_ie=name_ie,
            name_iew=name_iew,
            name_isw=name_isw,
            name_n=name_n,
            name_nic=name_nic,
            name_nie=name_nie,
            name_niew=name_niew,
            name_nisw=name_nisw,
            new_window=new_window,
            offset=offset,
            ordering=ordering,
            q=q,
            weight=weight,
            weight_empty=weight_empty,
            weight_gt=weight_gt,
            weight_gte=weight_gte,
            weight_lt=weight_lt,
            weight_lte=weight_lte,
            weight_n=weight_n,
        )
    ).parsed
