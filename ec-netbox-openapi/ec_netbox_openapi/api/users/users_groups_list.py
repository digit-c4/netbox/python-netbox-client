from http import HTTPStatus
from typing import Any, Dict, List, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.paginated_group_list import PaginatedGroupList
from ...types import UNSET, Response, Unset


def _get_kwargs(
    *,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
) -> Dict[str, Any]:
    params: Dict[str, Any] = {}

    json_id: Union[Unset, List[int]] = UNSET
    if not isinstance(id, Unset):
        json_id = id

    params["id"] = json_id

    params["id__empty"] = id_empty

    json_id_gt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gt, Unset):
        json_id_gt = id_gt

    params["id__gt"] = json_id_gt

    json_id_gte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_gte, Unset):
        json_id_gte = id_gte

    params["id__gte"] = json_id_gte

    json_id_lt: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lt, Unset):
        json_id_lt = id_lt

    params["id__lt"] = json_id_lt

    json_id_lte: Union[Unset, List[int]] = UNSET
    if not isinstance(id_lte, Unset):
        json_id_lte = id_lte

    params["id__lte"] = json_id_lte

    json_id_n: Union[Unset, List[int]] = UNSET
    if not isinstance(id_n, Unset):
        json_id_n = id_n

    params["id__n"] = json_id_n

    params["limit"] = limit

    json_name: Union[Unset, List[str]] = UNSET
    if not isinstance(name, Unset):
        json_name = name

    params["name"] = json_name

    params["name__empty"] = name_empty

    json_name_ic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ic, Unset):
        json_name_ic = name_ic

    params["name__ic"] = json_name_ic

    json_name_ie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_ie, Unset):
        json_name_ie = name_ie

    params["name__ie"] = json_name_ie

    json_name_iew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_iew, Unset):
        json_name_iew = name_iew

    params["name__iew"] = json_name_iew

    json_name_isw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_isw, Unset):
        json_name_isw = name_isw

    params["name__isw"] = json_name_isw

    json_name_n: Union[Unset, List[str]] = UNSET
    if not isinstance(name_n, Unset):
        json_name_n = name_n

    params["name__n"] = json_name_n

    json_name_nic: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nic, Unset):
        json_name_nic = name_nic

    params["name__nic"] = json_name_nic

    json_name_nie: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nie, Unset):
        json_name_nie = name_nie

    params["name__nie"] = json_name_nie

    json_name_niew: Union[Unset, List[str]] = UNSET
    if not isinstance(name_niew, Unset):
        json_name_niew = name_niew

    params["name__niew"] = json_name_niew

    json_name_nisw: Union[Unset, List[str]] = UNSET
    if not isinstance(name_nisw, Unset):
        json_name_nisw = name_nisw

    params["name__nisw"] = json_name_nisw

    params["offset"] = offset

    params["ordering"] = ordering

    params["q"] = q

    params = {k: v for k, v in params.items() if v is not UNSET and v is not None}

    _kwargs: Dict[str, Any] = {
        "method": "get",
        "url": "/api/users/groups/",
        "params": params,
    }

    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[PaginatedGroupList]:
    if response.status_code == HTTPStatus.OK:
        response_200 = PaginatedGroupList.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[PaginatedGroupList]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    *,
    client: AuthenticatedClient,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
) -> Response[PaginatedGroupList]:
    """Get a list of group objects.

    Args:
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedGroupList]
    """

    kwargs = _get_kwargs(
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        q=q,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    *,
    client: AuthenticatedClient,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
) -> Optional[PaginatedGroupList]:
    """Get a list of group objects.

    Args:
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedGroupList
    """

    return sync_detailed(
        client=client,
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        q=q,
    ).parsed


async def asyncio_detailed(
    *,
    client: AuthenticatedClient,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
) -> Response[PaginatedGroupList]:
    """Get a list of group objects.

    Args:
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[PaginatedGroupList]
    """

    kwargs = _get_kwargs(
        id=id,
        id_empty=id_empty,
        id_gt=id_gt,
        id_gte=id_gte,
        id_lt=id_lt,
        id_lte=id_lte,
        id_n=id_n,
        limit=limit,
        name=name,
        name_empty=name_empty,
        name_ic=name_ic,
        name_ie=name_ie,
        name_iew=name_iew,
        name_isw=name_isw,
        name_n=name_n,
        name_nic=name_nic,
        name_nie=name_nie,
        name_niew=name_niew,
        name_nisw=name_nisw,
        offset=offset,
        ordering=ordering,
        q=q,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    *,
    client: AuthenticatedClient,
    id: Union[Unset, List[int]] = UNSET,
    id_empty: Union[Unset, bool] = UNSET,
    id_gt: Union[Unset, List[int]] = UNSET,
    id_gte: Union[Unset, List[int]] = UNSET,
    id_lt: Union[Unset, List[int]] = UNSET,
    id_lte: Union[Unset, List[int]] = UNSET,
    id_n: Union[Unset, List[int]] = UNSET,
    limit: Union[Unset, int] = UNSET,
    name: Union[Unset, List[str]] = UNSET,
    name_empty: Union[Unset, bool] = UNSET,
    name_ic: Union[Unset, List[str]] = UNSET,
    name_ie: Union[Unset, List[str]] = UNSET,
    name_iew: Union[Unset, List[str]] = UNSET,
    name_isw: Union[Unset, List[str]] = UNSET,
    name_n: Union[Unset, List[str]] = UNSET,
    name_nic: Union[Unset, List[str]] = UNSET,
    name_nie: Union[Unset, List[str]] = UNSET,
    name_niew: Union[Unset, List[str]] = UNSET,
    name_nisw: Union[Unset, List[str]] = UNSET,
    offset: Union[Unset, int] = UNSET,
    ordering: Union[Unset, str] = UNSET,
    q: Union[Unset, str] = UNSET,
) -> Optional[PaginatedGroupList]:
    """Get a list of group objects.

    Args:
        id (Union[Unset, List[int]]):
        id_empty (Union[Unset, bool]):
        id_gt (Union[Unset, List[int]]):
        id_gte (Union[Unset, List[int]]):
        id_lt (Union[Unset, List[int]]):
        id_lte (Union[Unset, List[int]]):
        id_n (Union[Unset, List[int]]):
        limit (Union[Unset, int]):
        name (Union[Unset, List[str]]):
        name_empty (Union[Unset, bool]):
        name_ic (Union[Unset, List[str]]):
        name_ie (Union[Unset, List[str]]):
        name_iew (Union[Unset, List[str]]):
        name_isw (Union[Unset, List[str]]):
        name_n (Union[Unset, List[str]]):
        name_nic (Union[Unset, List[str]]):
        name_nie (Union[Unset, List[str]]):
        name_niew (Union[Unset, List[str]]):
        name_nisw (Union[Unset, List[str]]):
        offset (Union[Unset, int]):
        ordering (Union[Unset, str]):
        q (Union[Unset, str]):

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        PaginatedGroupList
    """

    return (
        await asyncio_detailed(
            client=client,
            id=id,
            id_empty=id_empty,
            id_gt=id_gt,
            id_gte=id_gte,
            id_lt=id_lt,
            id_lte=id_lte,
            id_n=id_n,
            limit=limit,
            name=name,
            name_empty=name_empty,
            name_ic=name_ic,
            name_ie=name_ie,
            name_iew=name_iew,
            name_isw=name_isw,
            name_n=name_n,
            name_nic=name_nic,
            name_nie=name_nie,
            name_niew=name_niew,
            name_nisw=name_nisw,
            offset=offset,
            ordering=ordering,
            q=q,
        )
    ).parsed
