from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.patched_writable_vlan_request import PatchedWritableVLANRequest
from ...models.vlan import VLAN
from ...types import Response


def _get_kwargs(
    id: int,
    *,
    body: Union[
        PatchedWritableVLANRequest,
        PatchedWritableVLANRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}

    _kwargs: Dict[str, Any] = {
        "method": "patch",
        "url": "/api/ipam/vlans/{id}/".format(
            id=id,
        ),
    }

    if _request_content_type is None:
        _body = body.to_dict()

        _kwargs["json"] = _body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "application/json":
        _json_body = body.to_dict()

        _kwargs["json"] = _json_body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "multipart/form-data":
        _files_body = body.to_multipart()

        _kwargs["files"] = _files_body
        headers["Content-Type"] = "multipart/form-data; boundary=+++"

    else:
        raise ValueError("Content type application/json not supported by the server")

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[VLAN]:
    if response.status_code == HTTPStatus.OK:
        response_200 = VLAN.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[VLAN]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        PatchedWritableVLANRequest,
        PatchedWritableVLANRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Response[VLAN]:
    """Patch a VLAN object.

    Args:
        id (int):
        body (PatchedWritableVLANRequest): Adds support for custom fields and tags.
        body (PatchedWritableVLANRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[VLAN]
    """

    kwargs = _get_kwargs(
        id=id,
        body=body,
        _request_content_type=_request_content_type,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        PatchedWritableVLANRequest,
        PatchedWritableVLANRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[VLAN]:
    """Patch a VLAN object.

    Args:
        id (int):
        body (PatchedWritableVLANRequest): Adds support for custom fields and tags.
        body (PatchedWritableVLANRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        VLAN
    """

    return sync_detailed(
        id=id,
        client=client,
        body=body,
        _request_content_type=_request_content_type,
    ).parsed


async def asyncio_detailed(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        PatchedWritableVLANRequest,
        PatchedWritableVLANRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Response[VLAN]:
    """Patch a VLAN object.

    Args:
        id (int):
        body (PatchedWritableVLANRequest): Adds support for custom fields and tags.
        body (PatchedWritableVLANRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[VLAN]
    """

    kwargs = _get_kwargs(
        id=id,
        body=body,
        _request_content_type=_request_content_type,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        PatchedWritableVLANRequest,
        PatchedWritableVLANRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[VLAN]:
    """Patch a VLAN object.

    Args:
        id (int):
        body (PatchedWritableVLANRequest): Adds support for custom fields and tags.
        body (PatchedWritableVLANRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        VLAN
    """

    return (
        await asyncio_detailed(
            id=id,
            client=client,
            body=body,
            _request_content_type=_request_content_type,
        )
    ).parsed
