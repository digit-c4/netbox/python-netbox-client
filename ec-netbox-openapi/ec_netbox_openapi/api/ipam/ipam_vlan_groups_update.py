from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.vlan_group import VLANGroup
from ...models.vlan_group_request import VLANGroupRequest
from ...types import Response


def _get_kwargs(
    id: int,
    *,
    body: Union[
        VLANGroupRequest,
        VLANGroupRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}

    _kwargs: Dict[str, Any] = {
        "method": "put",
        "url": "/api/ipam/vlan-groups/{id}/".format(
            id=id,
        ),
    }

    if _request_content_type is None:
        _body = body.to_dict()

        _kwargs["json"] = _body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "application/json":
        _json_body = body.to_dict()

        _kwargs["json"] = _json_body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "multipart/form-data":
        _files_body = body.to_multipart()

        _kwargs["files"] = _files_body
        headers["Content-Type"] = "multipart/form-data; boundary=+++"

    else:
        raise ValueError("Content type application/json not supported by the server")

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[VLANGroup]:
    if response.status_code == HTTPStatus.OK:
        response_200 = VLANGroup.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[VLANGroup]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        VLANGroupRequest,
        VLANGroupRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Response[VLANGroup]:
    """Put a VLAN group object.

    Args:
        id (int):
        body (VLANGroupRequest): Adds support for custom fields and tags.
        body (VLANGroupRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[VLANGroup]
    """

    kwargs = _get_kwargs(
        id=id,
        body=body,
        _request_content_type=_request_content_type,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        VLANGroupRequest,
        VLANGroupRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[VLANGroup]:
    """Put a VLAN group object.

    Args:
        id (int):
        body (VLANGroupRequest): Adds support for custom fields and tags.
        body (VLANGroupRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        VLANGroup
    """

    return sync_detailed(
        id=id,
        client=client,
        body=body,
        _request_content_type=_request_content_type,
    ).parsed


async def asyncio_detailed(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        VLANGroupRequest,
        VLANGroupRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Response[VLANGroup]:
    """Put a VLAN group object.

    Args:
        id (int):
        body (VLANGroupRequest): Adds support for custom fields and tags.
        body (VLANGroupRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[VLANGroup]
    """

    kwargs = _get_kwargs(
        id=id,
        body=body,
        _request_content_type=_request_content_type,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        VLANGroupRequest,
        VLANGroupRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[VLANGroup]:
    """Put a VLAN group object.

    Args:
        id (int):
        body (VLANGroupRequest): Adds support for custom fields and tags.
        body (VLANGroupRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        VLANGroup
    """

    return (
        await asyncio_detailed(
            id=id,
            client=client,
            body=body,
            _request_content_type=_request_content_type,
        )
    ).parsed
