from http import HTTPStatus
from typing import Any, Dict, Optional, Union

import httpx

from ... import errors
from ...client import AuthenticatedClient, Client
from ...models.provider_network import ProviderNetwork
from ...models.writable_provider_network_request import WritableProviderNetworkRequest
from ...types import Response


def _get_kwargs(
    id: int,
    *,
    body: Union[
        WritableProviderNetworkRequest,
        WritableProviderNetworkRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Dict[str, Any]:
    headers: Dict[str, Any] = {}

    _kwargs: Dict[str, Any] = {
        "method": "put",
        "url": "/api/circuits/provider-networks/{id}/".format(
            id=id,
        ),
    }

    if _request_content_type is None:
        _body = body.to_dict()

        _kwargs["json"] = _body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "application/json":
        _json_body = body.to_dict()

        _kwargs["json"] = _json_body
        headers["Content-Type"] = "application/json"

    elif _request_content_type == "multipart/form-data":
        _files_body = body.to_multipart()

        _kwargs["files"] = _files_body
        headers["Content-Type"] = "multipart/form-data; boundary=+++"

    else:
        raise ValueError("Content type application/json not supported by the server")

    _kwargs["headers"] = headers
    return _kwargs


def _parse_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Optional[ProviderNetwork]:
    if response.status_code == HTTPStatus.OK:
        response_200 = ProviderNetwork.from_dict(response.json())

        return response_200
    if client.raise_on_unexpected_status:
        raise errors.UnexpectedStatus(response.status_code, response.content)
    else:
        return None


def _build_response(
    *, client: Union[AuthenticatedClient, Client], response: httpx.Response
) -> Response[ProviderNetwork]:
    return Response(
        status_code=HTTPStatus(response.status_code),
        content=response.content,
        headers=response.headers,
        parsed=_parse_response(client=client, response=response),
    )


def sync_detailed(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        WritableProviderNetworkRequest,
        WritableProviderNetworkRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Response[ProviderNetwork]:
    """Put a provider network object.

    Args:
        id (int):
        body (WritableProviderNetworkRequest): Adds support for custom fields and tags.
        body (WritableProviderNetworkRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[ProviderNetwork]
    """

    kwargs = _get_kwargs(
        id=id,
        body=body,
        _request_content_type=_request_content_type,
    )

    response = client.get_httpx_client().request(
        **kwargs,
    )

    return _build_response(client=client, response=response)


def sync(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        WritableProviderNetworkRequest,
        WritableProviderNetworkRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[ProviderNetwork]:
    """Put a provider network object.

    Args:
        id (int):
        body (WritableProviderNetworkRequest): Adds support for custom fields and tags.
        body (WritableProviderNetworkRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        ProviderNetwork
    """

    return sync_detailed(
        id=id,
        client=client,
        body=body,
        _request_content_type=_request_content_type,
    ).parsed


async def asyncio_detailed(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        WritableProviderNetworkRequest,
        WritableProviderNetworkRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Response[ProviderNetwork]:
    """Put a provider network object.

    Args:
        id (int):
        body (WritableProviderNetworkRequest): Adds support for custom fields and tags.
        body (WritableProviderNetworkRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        Response[ProviderNetwork]
    """

    kwargs = _get_kwargs(
        id=id,
        body=body,
        _request_content_type=_request_content_type,
    )

    response = await client.get_async_httpx_client().request(**kwargs)

    return _build_response(client=client, response=response)


async def asyncio(
    id: int,
    *,
    client: AuthenticatedClient,
    body: Union[
        WritableProviderNetworkRequest,
        WritableProviderNetworkRequest,
    ],
    _request_content_type: Optional[str] = None,
) -> Optional[ProviderNetwork]:
    """Put a provider network object.

    Args:
        id (int):
        body (WritableProviderNetworkRequest): Adds support for custom fields and tags.
        body (WritableProviderNetworkRequest): Adds support for custom fields and tags.

    Raises:
        errors.UnexpectedStatus: If the server returns an undocumented status code and Client.raise_on_unexpected_status is True.
        httpx.TimeoutException: If the request takes longer than Client.timeout.

    Returns:
        ProviderNetwork
    """

    return (
        await asyncio_detailed(
            id=id,
            client=client,
            body=body,
            _request_content_type=_request_content_type,
        )
    ).parsed
