import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_device_type import NestedDeviceType


T = TypeVar("T", bound="ModuleBayTemplate")


@_attrs_define
class ModuleBayTemplate:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            id (int):
            url (str):
            display (str):
            device_type (NestedDeviceType): Represents an object related through a ForeignKey field. On write, it accepts a
                primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            name (str): {module} is accepted as a substitution for the module bay position when attached to a module type.
            created (Union[None, datetime.datetime]):
            last_updated (Union[None, datetime.datetime]):
            label (Union[Unset, str]): Physical label
            position (Union[Unset, str]): Identifier to reference when renaming installed components
            description (Union[Unset, str]):
    """

    id: int
    url: str
    display: str
    device_type: "NestedDeviceType"
    name: str
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    label: Union[Unset, str] = UNSET
    position: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        device_type = self.device_type.to_dict()

        name = self.name

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        label = self.label

        position = self.position

        description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "device_type": device_type,
                "name": name,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if label is not UNSET:
            field_dict["label"] = label
        if position is not UNSET:
            field_dict["position"] = position
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_device_type import NestedDeviceType

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        device_type = NestedDeviceType.from_dict(d.pop("device_type"))

        name = d.pop("name")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        label = d.pop("label", UNSET)

        position = d.pop("position", UNSET)

        description = d.pop("description", UNSET)

        module_bay_template = cls(
            id=id,
            url=url,
            display=display,
            device_type=device_type,
            name=name,
            created=created,
            last_updated=last_updated,
            label=label,
            position=position,
            description=description,
        )

        module_bay_template.additional_properties = d
        return module_bay_template

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
