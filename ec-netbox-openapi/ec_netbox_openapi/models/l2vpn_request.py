from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.l2vpn_request_type import L2VPNRequestType
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.l2vpn_request_custom_fields import L2VPNRequestCustomFields
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_tenant_request import NestedTenantRequest


T = TypeVar("T", bound="L2VPNRequest")


@_attrs_define
class L2VPNRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        slug (str):
        identifier (Union[None, Unset, int]):
        type (Union[Unset, L2VPNRequestType]): * `vpws` - VPWS
            * `vpls` - VPLS
            * `vxlan` - VXLAN
            * `vxlan-evpn` - VXLAN-EVPN
            * `mpls-evpn` - MPLS EVPN
            * `pbb-evpn` - PBB EVPN
            * `epl` - EPL
            * `evpl` - EVPL
            * `ep-lan` - Ethernet Private LAN
            * `evp-lan` - Ethernet Virtual Private LAN
            * `ep-tree` - Ethernet Private Tree
            * `evp-tree` - Ethernet Virtual Private Tree
        import_targets (Union[Unset, List[int]]):
        export_targets (Union[Unset, List[int]]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tenant (Union['NestedTenantRequest', None, Unset]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, L2VPNRequestCustomFields]):
    """

    name: str
    slug: str
    identifier: Union[None, Unset, int] = UNSET
    type: Union[Unset, L2VPNRequestType] = UNSET
    import_targets: Union[Unset, List[int]] = UNSET
    export_targets: Union[Unset, List[int]] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tenant: Union["NestedTenantRequest", None, Unset] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "L2VPNRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_tenant_request import NestedTenantRequest

        name = self.name

        slug = self.slug

        identifier: Union[None, Unset, int]
        if isinstance(self.identifier, Unset):
            identifier = UNSET
        else:
            identifier = self.identifier

        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        import_targets: Union[Unset, List[int]] = UNSET
        if not isinstance(self.import_targets, Unset):
            import_targets = self.import_targets

        export_targets: Union[Unset, List[int]] = UNSET
        if not isinstance(self.export_targets, Unset):
            export_targets = self.export_targets

        description = self.description

        comments = self.comments

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenantRequest):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "slug": slug,
            }
        )
        if identifier is not UNSET:
            field_dict["identifier"] = identifier
        if type is not UNSET:
            field_dict["type"] = type
        if import_targets is not UNSET:
            field_dict["import_targets"] = import_targets
        if export_targets is not UNSET:
            field_dict["export_targets"] = export_targets
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.l2vpn_request_custom_fields import L2VPNRequestCustomFields
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_tenant_request import NestedTenantRequest

        d = src_dict.copy()
        name = d.pop("name")

        slug = d.pop("slug")

        def _parse_identifier(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        identifier = _parse_identifier(d.pop("identifier", UNSET))

        _type = d.pop("type", UNSET)
        type: Union[Unset, L2VPNRequestType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = L2VPNRequestType(_type)

        import_targets = cast(List[int], d.pop("import_targets", UNSET))

        export_targets = cast(List[int], d.pop("export_targets", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        def _parse_tenant(data: object) -> Union["NestedTenantRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenantRequest.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenantRequest", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, L2VPNRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = L2VPNRequestCustomFields.from_dict(_custom_fields)

        l2vpn_request = cls(
            name=name,
            slug=slug,
            identifier=identifier,
            type=type,
            import_targets=import_targets,
            export_targets=export_targets,
            description=description,
            comments=comments,
            tenant=tenant,
            tags=tags,
            custom_fields=custom_fields,
        )

        l2vpn_request.additional_properties = d
        return l2vpn_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
