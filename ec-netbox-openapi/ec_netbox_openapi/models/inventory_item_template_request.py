from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_device_type_request import NestedDeviceTypeRequest
    from ..models.nested_inventory_item_role_request import (
        NestedInventoryItemRoleRequest,
    )
    from ..models.nested_manufacturer_request import NestedManufacturerRequest


T = TypeVar("T", bound="InventoryItemTemplateRequest")


@_attrs_define
class InventoryItemTemplateRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            device_type (NestedDeviceTypeRequest): Represents an object related through a ForeignKey field. On write, it
                accepts a primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            name (str): {module} is accepted as a substitution for the module bay position when attached to a module type.
            parent (Union[None, Unset, int]):
            label (Union[Unset, str]): Physical label
            role (Union['NestedInventoryItemRoleRequest', None, Unset]):
            manufacturer (Union['NestedManufacturerRequest', None, Unset]):
            part_id (Union[Unset, str]): Manufacturer-assigned part identifier
            description (Union[Unset, str]):
            component_type (Union[None, Unset, str]):
            component_id (Union[None, Unset, int]):
    """

    device_type: "NestedDeviceTypeRequest"
    name: str
    parent: Union[None, Unset, int] = UNSET
    label: Union[Unset, str] = UNSET
    role: Union["NestedInventoryItemRoleRequest", None, Unset] = UNSET
    manufacturer: Union["NestedManufacturerRequest", None, Unset] = UNSET
    part_id: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    component_type: Union[None, Unset, str] = UNSET
    component_id: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_inventory_item_role_request import (
            NestedInventoryItemRoleRequest,
        )
        from ..models.nested_manufacturer_request import NestedManufacturerRequest

        device_type = self.device_type.to_dict()

        name = self.name

        parent: Union[None, Unset, int]
        if isinstance(self.parent, Unset):
            parent = UNSET
        else:
            parent = self.parent

        label = self.label

        role: Union[Dict[str, Any], None, Unset]
        if isinstance(self.role, Unset):
            role = UNSET
        elif isinstance(self.role, NestedInventoryItemRoleRequest):
            role = self.role.to_dict()
        else:
            role = self.role

        manufacturer: Union[Dict[str, Any], None, Unset]
        if isinstance(self.manufacturer, Unset):
            manufacturer = UNSET
        elif isinstance(self.manufacturer, NestedManufacturerRequest):
            manufacturer = self.manufacturer.to_dict()
        else:
            manufacturer = self.manufacturer

        part_id = self.part_id

        description = self.description

        component_type: Union[None, Unset, str]
        if isinstance(self.component_type, Unset):
            component_type = UNSET
        else:
            component_type = self.component_type

        component_id: Union[None, Unset, int]
        if isinstance(self.component_id, Unset):
            component_id = UNSET
        else:
            component_id = self.component_id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "device_type": device_type,
                "name": name,
            }
        )
        if parent is not UNSET:
            field_dict["parent"] = parent
        if label is not UNSET:
            field_dict["label"] = label
        if role is not UNSET:
            field_dict["role"] = role
        if manufacturer is not UNSET:
            field_dict["manufacturer"] = manufacturer
        if part_id is not UNSET:
            field_dict["part_id"] = part_id
        if description is not UNSET:
            field_dict["description"] = description
        if component_type is not UNSET:
            field_dict["component_type"] = component_type
        if component_id is not UNSET:
            field_dict["component_id"] = component_id

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_device_type_request import NestedDeviceTypeRequest
        from ..models.nested_inventory_item_role_request import (
            NestedInventoryItemRoleRequest,
        )
        from ..models.nested_manufacturer_request import NestedManufacturerRequest

        d = src_dict.copy()
        device_type = NestedDeviceTypeRequest.from_dict(d.pop("device_type"))

        name = d.pop("name")

        def _parse_parent(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        parent = _parse_parent(d.pop("parent", UNSET))

        label = d.pop("label", UNSET)

        def _parse_role(
            data: object,
        ) -> Union["NestedInventoryItemRoleRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                role_type_1 = NestedInventoryItemRoleRequest.from_dict(data)

                return role_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedInventoryItemRoleRequest", None, Unset], data)

        role = _parse_role(d.pop("role", UNSET))

        def _parse_manufacturer(
            data: object,
        ) -> Union["NestedManufacturerRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                manufacturer_type_1 = NestedManufacturerRequest.from_dict(data)

                return manufacturer_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedManufacturerRequest", None, Unset], data)

        manufacturer = _parse_manufacturer(d.pop("manufacturer", UNSET))

        part_id = d.pop("part_id", UNSET)

        description = d.pop("description", UNSET)

        def _parse_component_type(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        component_type = _parse_component_type(d.pop("component_type", UNSET))

        def _parse_component_id(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        component_id = _parse_component_id(d.pop("component_id", UNSET))

        inventory_item_template_request = cls(
            device_type=device_type,
            name=name,
            parent=parent,
            label=label,
            role=role,
            manufacturer=manufacturer,
            part_id=part_id,
            description=description,
            component_type=component_type,
            component_id=component_id,
        )

        inventory_item_template_request.additional_properties = d
        return inventory_item_template_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
