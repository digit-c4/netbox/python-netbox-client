from enum import Enum


class WritableMappingRequestExtraProtocolsItem(str, Enum):
    WEBSOCKET = "websocket"

    def __str__(self) -> str:
        return str(self.value)
