from enum import Enum


class NetworkState(str, Enum):
    CREATED = "created"
    CREATING = "creating"

    def __str__(self) -> str:
        return str(self.value)
