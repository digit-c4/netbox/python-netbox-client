import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_ip_address_request_role import (
    PatchedWritableIPAddressRequestRole,
)
from ..models.patched_writable_ip_address_request_status import (
    PatchedWritableIPAddressRequestStatus,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_writable_ip_address_request_custom_fields import (
        PatchedWritableIPAddressRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedWritableIPAddressRequest")


@_attrs_define
class PatchedWritableIPAddressRequest:
    """Adds support for custom fields and tags.

    Attributes:
        address (Union[Unset, str]):
        vrf (Union[None, Unset, int]):
        tenant (Union[None, Unset, int]):
        status (Union[Unset, PatchedWritableIPAddressRequestStatus]): The operational status of this IP

            * `active` - Active
            * `reserved` - Reserved
            * `deprecated` - Deprecated
            * `dhcp` - DHCP
            * `slaac` - SLAAC
        role (Union[Unset, PatchedWritableIPAddressRequestRole]): The functional role of this IP

            * `loopback` - Loopback
            * `secondary` - Secondary
            * `anycast` - Anycast
            * `vip` - VIP
            * `vrrp` - VRRP
            * `hsrp` - HSRP
            * `glbp` - GLBP
            * `carp` - CARP
        assigned_object_type (Union[None, Unset, str]):
        assigned_object_id (Union[None, Unset, int]):
        nat_inside (Union[None, Unset, int]): The IP for which this address is the "outside" IP
        dns_name (Union[Unset, str]): Hostname or FQDN (not case-sensitive)
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, PatchedWritableIPAddressRequestCustomFields]):
    """

    address: Union[Unset, str] = UNSET
    vrf: Union[None, Unset, int] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    status: Union[Unset, PatchedWritableIPAddressRequestStatus] = UNSET
    role: Union[Unset, PatchedWritableIPAddressRequestRole] = UNSET
    assigned_object_type: Union[None, Unset, str] = UNSET
    assigned_object_id: Union[None, Unset, int] = UNSET
    nat_inside: Union[None, Unset, int] = UNSET
    dns_name: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "PatchedWritableIPAddressRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        address = self.address

        vrf: Union[None, Unset, int]
        if isinstance(self.vrf, Unset):
            vrf = UNSET
        else:
            vrf = self.vrf

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        role: Union[Unset, str] = UNSET
        if not isinstance(self.role, Unset):
            role = self.role.value

        assigned_object_type: Union[None, Unset, str]
        if isinstance(self.assigned_object_type, Unset):
            assigned_object_type = UNSET
        else:
            assigned_object_type = self.assigned_object_type

        assigned_object_id: Union[None, Unset, int]
        if isinstance(self.assigned_object_id, Unset):
            assigned_object_id = UNSET
        else:
            assigned_object_id = self.assigned_object_id

        nat_inside: Union[None, Unset, int]
        if isinstance(self.nat_inside, Unset):
            nat_inside = UNSET
        else:
            nat_inside = self.nat_inside

        dns_name = self.dns_name

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if address is not UNSET:
            field_dict["address"] = address
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if assigned_object_type is not UNSET:
            field_dict["assigned_object_type"] = assigned_object_type
        if assigned_object_id is not UNSET:
            field_dict["assigned_object_id"] = assigned_object_id
        if nat_inside is not UNSET:
            field_dict["nat_inside"] = nat_inside
        if dns_name is not UNSET:
            field_dict["dns_name"] = dns_name
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        address = (
            self.address
            if isinstance(self.address, Unset)
            else (None, str(self.address).encode(), "text/plain")
        )

        vrf: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.vrf, Unset):
            vrf = UNSET
        elif isinstance(self.vrf, int):
            vrf = (None, str(self.vrf).encode(), "text/plain")
        else:
            vrf = (None, str(self.vrf).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        role: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.role, Unset):
            role = (None, str(self.role.value).encode(), "text/plain")

        assigned_object_type: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.assigned_object_type, Unset):
            assigned_object_type = UNSET
        elif isinstance(self.assigned_object_type, str):
            assigned_object_type = (
                None,
                str(self.assigned_object_type).encode(),
                "text/plain",
            )
        else:
            assigned_object_type = (
                None,
                str(self.assigned_object_type).encode(),
                "text/plain",
            )

        assigned_object_id: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.assigned_object_id, Unset):
            assigned_object_id = UNSET
        elif isinstance(self.assigned_object_id, int):
            assigned_object_id = (
                None,
                str(self.assigned_object_id).encode(),
                "text/plain",
            )
        else:
            assigned_object_id = (
                None,
                str(self.assigned_object_id).encode(),
                "text/plain",
            )

        nat_inside: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.nat_inside, Unset):
            nat_inside = UNSET
        elif isinstance(self.nat_inside, int):
            nat_inside = (None, str(self.nat_inside).encode(), "text/plain")
        else:
            nat_inside = (None, str(self.nat_inside).encode(), "text/plain")

        dns_name = (
            self.dns_name
            if isinstance(self.dns_name, Unset)
            else (None, str(self.dns_name).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if address is not UNSET:
            field_dict["address"] = address
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if assigned_object_type is not UNSET:
            field_dict["assigned_object_type"] = assigned_object_type
        if assigned_object_id is not UNSET:
            field_dict["assigned_object_id"] = assigned_object_id
        if nat_inside is not UNSET:
            field_dict["nat_inside"] = nat_inside
        if dns_name is not UNSET:
            field_dict["dns_name"] = dns_name
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_writable_ip_address_request_custom_fields import (
            PatchedWritableIPAddressRequestCustomFields,
        )

        d = src_dict.copy()
        address = d.pop("address", UNSET)

        def _parse_vrf(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vrf = _parse_vrf(d.pop("vrf", UNSET))

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, PatchedWritableIPAddressRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = PatchedWritableIPAddressRequestStatus(_status)

        _role = d.pop("role", UNSET)
        role: Union[Unset, PatchedWritableIPAddressRequestRole]
        if isinstance(_role, Unset):
            role = UNSET
        else:
            role = PatchedWritableIPAddressRequestRole(_role)

        def _parse_assigned_object_type(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        assigned_object_type = _parse_assigned_object_type(
            d.pop("assigned_object_type", UNSET)
        )

        def _parse_assigned_object_id(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        assigned_object_id = _parse_assigned_object_id(
            d.pop("assigned_object_id", UNSET)
        )

        def _parse_nat_inside(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        nat_inside = _parse_nat_inside(d.pop("nat_inside", UNSET))

        dns_name = d.pop("dns_name", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedWritableIPAddressRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedWritableIPAddressRequestCustomFields.from_dict(
                _custom_fields
            )

        patched_writable_ip_address_request = cls(
            address=address,
            vrf=vrf,
            tenant=tenant,
            status=status,
            role=role,
            assigned_object_type=assigned_object_type,
            assigned_object_id=assigned_object_id,
            nat_inside=nat_inside,
            dns_name=dns_name,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        patched_writable_ip_address_request.additional_properties = d
        return patched_writable_ip_address_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
