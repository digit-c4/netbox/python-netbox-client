import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_saml_config_request_custom_fields import (
        WritableSamlConfigRequestCustomFields,
    )


T = TypeVar("T", bound="WritableSamlConfigRequest")


@_attrs_define
class WritableSamlConfigRequest:
    """SAML Config Serializer class

    Attributes:
        acs_url (str):
        logout_url (str):
        mapping (int):
        force_nauth (Union[Unset, bool]):
        custom_fields (Union[Unset, WritableSamlConfigRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    acs_url: str
    logout_url: str
    mapping: int
    force_nauth: Union[Unset, bool] = UNSET
    custom_fields: Union[Unset, "WritableSamlConfigRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        acs_url = self.acs_url

        logout_url = self.logout_url

        mapping = self.mapping

        force_nauth = self.force_nauth

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "acs_url": acs_url,
                "logout_url": logout_url,
                "mapping": mapping,
            }
        )
        if force_nauth is not UNSET:
            field_dict["force_nauth"] = force_nauth
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        acs_url = (None, str(self.acs_url).encode(), "text/plain")

        logout_url = (None, str(self.logout_url).encode(), "text/plain")

        mapping = (None, str(self.mapping).encode(), "text/plain")

        force_nauth = (
            self.force_nauth
            if isinstance(self.force_nauth, Unset)
            else (None, str(self.force_nauth).encode(), "text/plain")
        )

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "acs_url": acs_url,
                "logout_url": logout_url,
                "mapping": mapping,
            }
        )
        if force_nauth is not UNSET:
            field_dict["force_nauth"] = force_nauth
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_saml_config_request_custom_fields import (
            WritableSamlConfigRequestCustomFields,
        )

        d = src_dict.copy()
        acs_url = d.pop("acs_url")

        logout_url = d.pop("logout_url")

        mapping = d.pop("mapping")

        force_nauth = d.pop("force_nauth", UNSET)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableSamlConfigRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableSamlConfigRequestCustomFields.from_dict(
                _custom_fields
            )

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        writable_saml_config_request = cls(
            acs_url=acs_url,
            logout_url=logout_url,
            mapping=mapping,
            force_nauth=force_nauth,
            custom_fields=custom_fields,
            tags=tags,
        )

        writable_saml_config_request.additional_properties = d
        return writable_saml_config_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
