from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_device_type_request import NestedDeviceTypeRequest


T = TypeVar("T", bound="ModuleBayTemplateRequest")


@_attrs_define
class ModuleBayTemplateRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            device_type (NestedDeviceTypeRequest): Represents an object related through a ForeignKey field. On write, it
                accepts a primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            name (str): {module} is accepted as a substitution for the module bay position when attached to a module type.
            label (Union[Unset, str]): Physical label
            position (Union[Unset, str]): Identifier to reference when renaming installed components
            description (Union[Unset, str]):
    """

    device_type: "NestedDeviceTypeRequest"
    name: str
    label: Union[Unset, str] = UNSET
    position: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        device_type = self.device_type.to_dict()

        name = self.name

        label = self.label

        position = self.position

        description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "device_type": device_type,
                "name": name,
            }
        )
        if label is not UNSET:
            field_dict["label"] = label
        if position is not UNSET:
            field_dict["position"] = position
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_device_type_request import NestedDeviceTypeRequest

        d = src_dict.copy()
        device_type = NestedDeviceTypeRequest.from_dict(d.pop("device_type"))

        name = d.pop("name")

        label = d.pop("label", UNSET)

        position = d.pop("position", UNSET)

        description = d.pop("description", UNSET)

        module_bay_template_request = cls(
            device_type=device_type,
            name=name,
            label=label,
            position=position,
            description=description,
        )

        module_bay_template_request.additional_properties = d
        return module_bay_template_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
