import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.mac_address_status_type_1 import MacAddressStatusType1
from ..models.mac_address_status_type_2_type_1 import MacAddressStatusType2Type1
from ..models.mac_address_status_type_3_type_1 import MacAddressStatusType3Type1
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.vlan import Vlan
    from ..models.vlan_group import VlanGroup


T = TypeVar("T", bound="MacAddress")


@_attrs_define
class MacAddress:
    """
    Attributes:
        id (int):
        mac_address (str):
        assigned_object_type_name (str):
        vlans (Vlan):
        vlan_groups (VlanGroup):
        status (Union[MacAddressStatusType1, MacAddressStatusType2Type1, MacAddressStatusType3Type1, None, Unset]): *
            `active` - Active
            * `inactive` - Inactive
            * `quarantined` - Quarantined Default: MacAddressStatusType1.INACTIVE.
        end_date (Union[None, Unset, datetime.date]):
        description (Union[None, Unset, str]):
    """

    id: int
    mac_address: str
    assigned_object_type_name: str
    vlans: "Vlan"
    vlan_groups: "VlanGroup"
    status: Union[
        MacAddressStatusType1,
        MacAddressStatusType2Type1,
        MacAddressStatusType3Type1,
        None,
        Unset,
    ] = MacAddressStatusType1.INACTIVE
    end_date: Union[None, Unset, datetime.date] = UNSET
    description: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        mac_address = self.mac_address

        assigned_object_type_name = self.assigned_object_type_name

        vlans = self.vlans.to_dict()

        vlan_groups = self.vlan_groups.to_dict()

        status: Union[None, Unset, str]
        if isinstance(self.status, Unset):
            status = UNSET
        elif isinstance(self.status, MacAddressStatusType1):
            status = self.status.value
        elif isinstance(self.status, MacAddressStatusType2Type1):
            status = self.status.value
        elif isinstance(self.status, MacAddressStatusType3Type1):
            status = self.status.value
        else:
            status = self.status

        end_date: Union[None, Unset, str]
        if isinstance(self.end_date, Unset):
            end_date = UNSET
        elif isinstance(self.end_date, datetime.date):
            end_date = self.end_date.isoformat()
        else:
            end_date = self.end_date

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "mac_address": mac_address,
                "assigned_object_type_name": assigned_object_type_name,
                "vlans": vlans,
                "vlan_groups": vlan_groups,
            }
        )
        if status is not UNSET:
            field_dict["status"] = status
        if end_date is not UNSET:
            field_dict["end_date"] = end_date
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.vlan import Vlan
        from ..models.vlan_group import VlanGroup

        d = src_dict.copy()
        id = d.pop("id")

        mac_address = d.pop("mac_address")

        assigned_object_type_name = d.pop("assigned_object_type_name")

        vlans = Vlan.from_dict(d.pop("vlans"))

        vlan_groups = VlanGroup.from_dict(d.pop("vlan_groups"))

        def _parse_status(
            data: object,
        ) -> Union[
            MacAddressStatusType1,
            MacAddressStatusType2Type1,
            MacAddressStatusType3Type1,
            None,
            Unset,
        ]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                status_type_1 = MacAddressStatusType1(data)

                return status_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                status_type_2_type_1 = MacAddressStatusType2Type1(data)

                return status_type_2_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                status_type_3_type_1 = MacAddressStatusType3Type1(data)

                return status_type_3_type_1
            except:  # noqa: E722
                pass
            return cast(
                Union[
                    MacAddressStatusType1,
                    MacAddressStatusType2Type1,
                    MacAddressStatusType3Type1,
                    None,
                    Unset,
                ],
                data,
            )

        status = _parse_status(d.pop("status", UNSET))

        def _parse_end_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                end_date_type_0 = isoparse(data).date()

                return end_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        end_date = _parse_end_date(d.pop("end_date", UNSET))

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        mac_address = cls(
            id=id,
            mac_address=mac_address,
            assigned_object_type_name=assigned_object_type_name,
            vlans=vlans,
            vlan_groups=vlan_groups,
            status=status,
            end_date=end_date,
            description=description,
        )

        mac_address.additional_properties = d
        return mac_address

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
