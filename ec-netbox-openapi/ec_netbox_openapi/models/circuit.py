import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.circuit_circuit_termination import CircuitCircuitTermination
    from ..models.circuit_custom_fields import CircuitCustomFields
    from ..models.circuit_status import CircuitStatus
    from ..models.nested_circuit_type import NestedCircuitType
    from ..models.nested_provider import NestedProvider
    from ..models.nested_provider_account import NestedProviderAccount
    from ..models.nested_tag import NestedTag
    from ..models.nested_tenant import NestedTenant


T = TypeVar("T", bound="Circuit")


@_attrs_define
class Circuit:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        cid (str): Unique circuit ID
        provider (NestedProvider): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        type (NestedCircuitType): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        termination_a (Union['CircuitCircuitTermination', None]):
        termination_z (Union['CircuitCircuitTermination', None]):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        provider_account (Union['NestedProviderAccount', None, Unset]):
        status (Union[Unset, CircuitStatus]):
        tenant (Union['NestedTenant', None, Unset]):
        install_date (Union[None, Unset, datetime.date]):
        termination_date (Union[None, Unset, datetime.date]):
        commit_rate (Union[None, Unset, int]): Committed rate
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, CircuitCustomFields]):
    """

    id: int
    url: str
    display: str
    cid: str
    provider: "NestedProvider"
    type: "NestedCircuitType"
    termination_a: Union["CircuitCircuitTermination", None]
    termination_z: Union["CircuitCircuitTermination", None]
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    provider_account: Union["NestedProviderAccount", None, Unset] = UNSET
    status: Union[Unset, "CircuitStatus"] = UNSET
    tenant: Union["NestedTenant", None, Unset] = UNSET
    install_date: Union[None, Unset, datetime.date] = UNSET
    termination_date: Union[None, Unset, datetime.date] = UNSET
    commit_rate: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "CircuitCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.circuit_circuit_termination import CircuitCircuitTermination
        from ..models.nested_provider_account import NestedProviderAccount
        from ..models.nested_tenant import NestedTenant

        id = self.id

        url = self.url

        display = self.display

        cid = self.cid

        provider = self.provider.to_dict()

        type = self.type.to_dict()

        termination_a: Union[Dict[str, Any], None]
        if isinstance(self.termination_a, CircuitCircuitTermination):
            termination_a = self.termination_a.to_dict()
        else:
            termination_a = self.termination_a

        termination_z: Union[Dict[str, Any], None]
        if isinstance(self.termination_z, CircuitCircuitTermination):
            termination_z = self.termination_z.to_dict()
        else:
            termination_z = self.termination_z

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        provider_account: Union[Dict[str, Any], None, Unset]
        if isinstance(self.provider_account, Unset):
            provider_account = UNSET
        elif isinstance(self.provider_account, NestedProviderAccount):
            provider_account = self.provider_account.to_dict()
        else:
            provider_account = self.provider_account

        status: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.to_dict()

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenant):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        install_date: Union[None, Unset, str]
        if isinstance(self.install_date, Unset):
            install_date = UNSET
        elif isinstance(self.install_date, datetime.date):
            install_date = self.install_date.isoformat()
        else:
            install_date = self.install_date

        termination_date: Union[None, Unset, str]
        if isinstance(self.termination_date, Unset):
            termination_date = UNSET
        elif isinstance(self.termination_date, datetime.date):
            termination_date = self.termination_date.isoformat()
        else:
            termination_date = self.termination_date

        commit_rate: Union[None, Unset, int]
        if isinstance(self.commit_rate, Unset):
            commit_rate = UNSET
        else:
            commit_rate = self.commit_rate

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "cid": cid,
                "provider": provider,
                "type": type,
                "termination_a": termination_a,
                "termination_z": termination_z,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if provider_account is not UNSET:
            field_dict["provider_account"] = provider_account
        if status is not UNSET:
            field_dict["status"] = status
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if install_date is not UNSET:
            field_dict["install_date"] = install_date
        if termination_date is not UNSET:
            field_dict["termination_date"] = termination_date
        if commit_rate is not UNSET:
            field_dict["commit_rate"] = commit_rate
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.circuit_circuit_termination import CircuitCircuitTermination
        from ..models.circuit_custom_fields import CircuitCustomFields
        from ..models.circuit_status import CircuitStatus
        from ..models.nested_circuit_type import NestedCircuitType
        from ..models.nested_provider import NestedProvider
        from ..models.nested_provider_account import NestedProviderAccount
        from ..models.nested_tag import NestedTag
        from ..models.nested_tenant import NestedTenant

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        cid = d.pop("cid")

        provider = NestedProvider.from_dict(d.pop("provider"))

        type = NestedCircuitType.from_dict(d.pop("type"))

        def _parse_termination_a(
            data: object,
        ) -> Union["CircuitCircuitTermination", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                termination_a_type_1 = CircuitCircuitTermination.from_dict(data)

                return termination_a_type_1
            except:  # noqa: E722
                pass
            return cast(Union["CircuitCircuitTermination", None], data)

        termination_a = _parse_termination_a(d.pop("termination_a"))

        def _parse_termination_z(
            data: object,
        ) -> Union["CircuitCircuitTermination", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                termination_z_type_1 = CircuitCircuitTermination.from_dict(data)

                return termination_z_type_1
            except:  # noqa: E722
                pass
            return cast(Union["CircuitCircuitTermination", None], data)

        termination_z = _parse_termination_z(d.pop("termination_z"))

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        def _parse_provider_account(
            data: object,
        ) -> Union["NestedProviderAccount", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                provider_account_type_1 = NestedProviderAccount.from_dict(data)

                return provider_account_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedProviderAccount", None, Unset], data)

        provider_account = _parse_provider_account(d.pop("provider_account", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, CircuitStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = CircuitStatus.from_dict(_status)

        def _parse_tenant(data: object) -> Union["NestedTenant", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenant.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenant", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        def _parse_install_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                install_date_type_0 = isoparse(data).date()

                return install_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        install_date = _parse_install_date(d.pop("install_date", UNSET))

        def _parse_termination_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                termination_date_type_0 = isoparse(data).date()

                return termination_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        termination_date = _parse_termination_date(d.pop("termination_date", UNSET))

        def _parse_commit_rate(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        commit_rate = _parse_commit_rate(d.pop("commit_rate", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, CircuitCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = CircuitCustomFields.from_dict(_custom_fields)

        circuit = cls(
            id=id,
            url=url,
            display=display,
            cid=cid,
            provider=provider,
            type=type,
            termination_a=termination_a,
            termination_z=termination_z,
            created=created,
            last_updated=last_updated,
            provider_account=provider_account,
            status=status,
            tenant=tenant,
            install_date=install_date,
            termination_date=termination_date,
            commit_rate=commit_rate,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        circuit.additional_properties = d
        return circuit

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
