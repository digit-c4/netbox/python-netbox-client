from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.network_request_driver_type_1 import NetworkRequestDriverType1
from ..models.network_request_state import NetworkRequestState
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_host_request import NestedHostRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.network_request_custom_fields import NetworkRequestCustomFields


T = TypeVar("T", bound="NetworkRequest")


@_attrs_define
class NetworkRequest:
    """Network Serializer class

    Attributes:
        host (NestedHostRequest): Nested Host Serializer class
        name (str):
        driver (Union[NetworkRequestDriverType1, None, Unset]): * `None` - null
            * `bridge` - Bridge
            * `host` - Host
        network_id (Union[None, Unset, str]):
        state (Union[Unset, NetworkRequestState]): * `creating` - Creating
            * `created` - Created
        custom_fields (Union[Unset, NetworkRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    host: "NestedHostRequest"
    name: str
    driver: Union[NetworkRequestDriverType1, None, Unset] = UNSET
    network_id: Union[None, Unset, str] = UNSET
    state: Union[Unset, NetworkRequestState] = UNSET
    custom_fields: Union[Unset, "NetworkRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        host = self.host.to_dict()

        name = self.name

        driver: Union[None, Unset, str]
        if isinstance(self.driver, Unset):
            driver = UNSET
        elif isinstance(self.driver, NetworkRequestDriverType1):
            driver = self.driver.value
        else:
            driver = self.driver

        network_id: Union[None, Unset, str]
        if isinstance(self.network_id, Unset):
            network_id = UNSET
        else:
            network_id = self.network_id

        state: Union[Unset, str] = UNSET
        if not isinstance(self.state, Unset):
            state = self.state.value

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "host": host,
                "name": name,
            }
        )
        if driver is not UNSET:
            field_dict["driver"] = driver
        if network_id is not UNSET:
            field_dict["NetworkID"] = network_id
        if state is not UNSET:
            field_dict["state"] = state
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_host_request import NestedHostRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.network_request_custom_fields import NetworkRequestCustomFields

        d = src_dict.copy()
        host = NestedHostRequest.from_dict(d.pop("host"))

        name = d.pop("name")

        def _parse_driver(
            data: object,
        ) -> Union[NetworkRequestDriverType1, None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                driver_type_1 = NetworkRequestDriverType1(data)

                return driver_type_1
            except:  # noqa: E722
                pass
            return cast(Union[NetworkRequestDriverType1, None, Unset], data)

        driver = _parse_driver(d.pop("driver", UNSET))

        def _parse_network_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        network_id = _parse_network_id(d.pop("NetworkID", UNSET))

        _state = d.pop("state", UNSET)
        state: Union[Unset, NetworkRequestState]
        if isinstance(_state, Unset):
            state = UNSET
        else:
            state = NetworkRequestState(_state)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, NetworkRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = NetworkRequestCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        network_request = cls(
            host=host,
            name=name,
            driver=driver,
            network_id=network_id,
            state=state,
            custom_fields=custom_fields,
            tags=tags,
        )

        network_request.additional_properties = d
        return network_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
