from enum import Enum


class MappingRequestExtraProtocolsItem(str, Enum):
    WEBSOCKET = "websocket"

    def __str__(self) -> str:
        return str(self.value)
