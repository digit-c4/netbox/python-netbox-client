from enum import Enum


class NetworkRequestState(str, Enum):
    CREATED = "created"
    CREATING = "creating"

    def __str__(self) -> str:
        return str(self.value)
