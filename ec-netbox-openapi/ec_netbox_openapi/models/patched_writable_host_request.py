import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_host_request_operation import (
    PatchedWritableHostRequestOperation,
)
from ..models.patched_writable_host_request_state import PatchedWritableHostRequestState
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_writable_host_request_custom_fields import (
        PatchedWritableHostRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedWritableHostRequest")


@_attrs_define
class PatchedWritableHostRequest:
    """Host Serializer class

    Attributes:
        endpoint (Union[Unset, str]):
        name (Union[Unset, str]):
        state (Union[Unset, PatchedWritableHostRequestState]): * `created` - Created
            * `running` - Running
            * `deleted` - Deleted
            * `refreshing` - Refreshing
        netbox_base_url (Union[None, Unset, str]):
        agent_version (Union[None, Unset, str]):
        docker_api_version (Union[None, Unset, str]):
        operation (Union[Unset, PatchedWritableHostRequestOperation]): * `refresh` - Refresh
            * `none` - None
        custom_fields (Union[Unset, PatchedWritableHostRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    endpoint: Union[Unset, str] = UNSET
    name: Union[Unset, str] = UNSET
    state: Union[Unset, PatchedWritableHostRequestState] = UNSET
    netbox_base_url: Union[None, Unset, str] = UNSET
    agent_version: Union[None, Unset, str] = UNSET
    docker_api_version: Union[None, Unset, str] = UNSET
    operation: Union[Unset, PatchedWritableHostRequestOperation] = UNSET
    custom_fields: Union[Unset, "PatchedWritableHostRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        endpoint = self.endpoint

        name = self.name

        state: Union[Unset, str] = UNSET
        if not isinstance(self.state, Unset):
            state = self.state.value

        netbox_base_url: Union[None, Unset, str]
        if isinstance(self.netbox_base_url, Unset):
            netbox_base_url = UNSET
        else:
            netbox_base_url = self.netbox_base_url

        agent_version: Union[None, Unset, str]
        if isinstance(self.agent_version, Unset):
            agent_version = UNSET
        else:
            agent_version = self.agent_version

        docker_api_version: Union[None, Unset, str]
        if isinstance(self.docker_api_version, Unset):
            docker_api_version = UNSET
        else:
            docker_api_version = self.docker_api_version

        operation: Union[Unset, str] = UNSET
        if not isinstance(self.operation, Unset):
            operation = self.operation.value

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if endpoint is not UNSET:
            field_dict["endpoint"] = endpoint
        if name is not UNSET:
            field_dict["name"] = name
        if state is not UNSET:
            field_dict["state"] = state
        if netbox_base_url is not UNSET:
            field_dict["netbox_base_url"] = netbox_base_url
        if agent_version is not UNSET:
            field_dict["agent_version"] = agent_version
        if docker_api_version is not UNSET:
            field_dict["docker_api_version"] = docker_api_version
        if operation is not UNSET:
            field_dict["operation"] = operation
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        endpoint = (
            self.endpoint
            if isinstance(self.endpoint, Unset)
            else (None, str(self.endpoint).encode(), "text/plain")
        )

        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        state: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.state, Unset):
            state = (None, str(self.state.value).encode(), "text/plain")

        netbox_base_url: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.netbox_base_url, Unset):
            netbox_base_url = UNSET
        elif isinstance(self.netbox_base_url, str):
            netbox_base_url = (None, str(self.netbox_base_url).encode(), "text/plain")
        else:
            netbox_base_url = (None, str(self.netbox_base_url).encode(), "text/plain")

        agent_version: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.agent_version, Unset):
            agent_version = UNSET
        elif isinstance(self.agent_version, str):
            agent_version = (None, str(self.agent_version).encode(), "text/plain")
        else:
            agent_version = (None, str(self.agent_version).encode(), "text/plain")

        docker_api_version: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.docker_api_version, Unset):
            docker_api_version = UNSET
        elif isinstance(self.docker_api_version, str):
            docker_api_version = (
                None,
                str(self.docker_api_version).encode(),
                "text/plain",
            )
        else:
            docker_api_version = (
                None,
                str(self.docker_api_version).encode(),
                "text/plain",
            )

        operation: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.operation, Unset):
            operation = (None, str(self.operation.value).encode(), "text/plain")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if endpoint is not UNSET:
            field_dict["endpoint"] = endpoint
        if name is not UNSET:
            field_dict["name"] = name
        if state is not UNSET:
            field_dict["state"] = state
        if netbox_base_url is not UNSET:
            field_dict["netbox_base_url"] = netbox_base_url
        if agent_version is not UNSET:
            field_dict["agent_version"] = agent_version
        if docker_api_version is not UNSET:
            field_dict["docker_api_version"] = docker_api_version
        if operation is not UNSET:
            field_dict["operation"] = operation
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_writable_host_request_custom_fields import (
            PatchedWritableHostRequestCustomFields,
        )

        d = src_dict.copy()
        endpoint = d.pop("endpoint", UNSET)

        name = d.pop("name", UNSET)

        _state = d.pop("state", UNSET)
        state: Union[Unset, PatchedWritableHostRequestState]
        if isinstance(_state, Unset):
            state = UNSET
        else:
            state = PatchedWritableHostRequestState(_state)

        def _parse_netbox_base_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        netbox_base_url = _parse_netbox_base_url(d.pop("netbox_base_url", UNSET))

        def _parse_agent_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        agent_version = _parse_agent_version(d.pop("agent_version", UNSET))

        def _parse_docker_api_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        docker_api_version = _parse_docker_api_version(
            d.pop("docker_api_version", UNSET)
        )

        _operation = d.pop("operation", UNSET)
        operation: Union[Unset, PatchedWritableHostRequestOperation]
        if isinstance(_operation, Unset):
            operation = UNSET
        else:
            operation = PatchedWritableHostRequestOperation(_operation)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedWritableHostRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedWritableHostRequestCustomFields.from_dict(
                _custom_fields
            )

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        patched_writable_host_request = cls(
            endpoint=endpoint,
            name=name,
            state=state,
            netbox_base_url=netbox_base_url,
            agent_version=agent_version,
            docker_api_version=docker_api_version,
            operation=operation,
            custom_fields=custom_fields,
            tags=tags,
        )

        patched_writable_host_request.additional_properties = d
        return patched_writable_host_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
