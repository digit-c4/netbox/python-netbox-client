import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.data_source_status import DataSourceStatus
    from ..models.data_source_type import DataSourceType


T = TypeVar("T", bound="DataSource")


@_attrs_define
class DataSource:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        name (str):
        type (DataSourceType):
        source_url (str):
        status (DataSourceStatus):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        file_count (int):
        enabled (Union[Unset, bool]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        parameters (Union[Unset, Any]):
        ignore_rules (Union[Unset, str]): Patterns (one per line) matching files to ignore when syncing
    """

    id: int
    url: str
    display: str
    name: str
    type: "DataSourceType"
    source_url: str
    status: "DataSourceStatus"
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    file_count: int
    enabled: Union[Unset, bool] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    parameters: Union[Unset, Any] = UNSET
    ignore_rules: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        name = self.name

        type = self.type.to_dict()

        source_url = self.source_url

        status = self.status.to_dict()

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        file_count = self.file_count

        enabled = self.enabled

        description = self.description

        comments = self.comments

        parameters = self.parameters

        ignore_rules = self.ignore_rules

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "name": name,
                "type": type,
                "source_url": source_url,
                "status": status,
                "created": created,
                "last_updated": last_updated,
                "file_count": file_count,
            }
        )
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if parameters is not UNSET:
            field_dict["parameters"] = parameters
        if ignore_rules is not UNSET:
            field_dict["ignore_rules"] = ignore_rules

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.data_source_status import DataSourceStatus
        from ..models.data_source_type import DataSourceType

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        name = d.pop("name")

        type = DataSourceType.from_dict(d.pop("type"))

        source_url = d.pop("source_url")

        status = DataSourceStatus.from_dict(d.pop("status"))

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        file_count = d.pop("file_count")

        enabled = d.pop("enabled", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        parameters = d.pop("parameters", UNSET)

        ignore_rules = d.pop("ignore_rules", UNSET)

        data_source = cls(
            id=id,
            url=url,
            display=display,
            name=name,
            type=type,
            source_url=source_url,
            status=status,
            created=created,
            last_updated=last_updated,
            file_count=file_count,
            enabled=enabled,
            description=description,
            comments=comments,
            parameters=parameters,
            ignore_rules=ignore_rules,
        )

        data_source.additional_properties = d
        return data_source

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
