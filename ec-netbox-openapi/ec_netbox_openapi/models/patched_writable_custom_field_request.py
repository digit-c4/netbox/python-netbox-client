import json
from typing import Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_custom_field_request_filter_logic import (
    PatchedWritableCustomFieldRequestFilterLogic,
)
from ..models.patched_writable_custom_field_request_type import (
    PatchedWritableCustomFieldRequestType,
)
from ..models.patched_writable_custom_field_request_ui_visibility import (
    PatchedWritableCustomFieldRequestUiVisibility,
)
from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedWritableCustomFieldRequest")


@_attrs_define
class PatchedWritableCustomFieldRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            content_types (Union[Unset, List[str]]):
            type (Union[Unset, PatchedWritableCustomFieldRequestType]): The type of data this custom field holds

                * `text` - Text
                * `longtext` - Text (long)
                * `integer` - Integer
                * `decimal` - Decimal
                * `boolean` - Boolean (true/false)
                * `date` - Date
                * `datetime` - Date & time
                * `url` - URL
                * `json` - JSON
                * `select` - Selection
                * `multiselect` - Multiple selection
                * `object` - Object
                * `multiobject` - Multiple objects
            object_type (Union[Unset, str]):
            name (Union[Unset, str]): Internal field name
            label (Union[Unset, str]): Name of the field as displayed to users (if not provided, 'the field's name will be
                used)
            group_name (Union[Unset, str]): Custom fields within the same group will be displayed together
            description (Union[Unset, str]):
            required (Union[Unset, bool]): If true, this field is required when creating new objects or editing an existing
                object.
            search_weight (Union[Unset, int]): Weighting for search. Lower values are considered more important. Fields with
                a search weight of zero will be ignored.
            filter_logic (Union[Unset, PatchedWritableCustomFieldRequestFilterLogic]): Loose matches any instance of a given
                string; exact matches the entire field.

                * `disabled` - Disabled
                * `loose` - Loose
                * `exact` - Exact
            ui_visibility (Union[Unset, PatchedWritableCustomFieldRequestUiVisibility]): Specifies the visibility of custom
                field in the UI

                * `read-write` - Read/write
                * `read-only` - Read-only
                * `hidden` - Hidden
                * `hidden-ifunset` - Hidden (if unset)
            is_cloneable (Union[Unset, bool]): Replicate this value when cloning objects
            default (Union[Unset, Any]): Default value for the field (must be a JSON value). Encapsulate strings with double
                quotes (e.g. "Foo").
            weight (Union[Unset, int]): Fields with higher weights appear lower in a form.
            validation_minimum (Union[None, Unset, int]): Minimum allowed value (for numeric fields)
            validation_maximum (Union[None, Unset, int]): Maximum allowed value (for numeric fields)
            validation_regex (Union[Unset, str]): Regular expression to enforce on text field values. Use ^ and $ to force
                matching of entire string. For example, <code>^[A-Z]{3}$</code> will limit values to exactly three uppercase
                letters.
            choice_set (Union[None, Unset, int]):
    """

    content_types: Union[Unset, List[str]] = UNSET
    type: Union[Unset, PatchedWritableCustomFieldRequestType] = UNSET
    object_type: Union[Unset, str] = UNSET
    name: Union[Unset, str] = UNSET
    label: Union[Unset, str] = UNSET
    group_name: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    required: Union[Unset, bool] = UNSET
    search_weight: Union[Unset, int] = UNSET
    filter_logic: Union[Unset, PatchedWritableCustomFieldRequestFilterLogic] = UNSET
    ui_visibility: Union[Unset, PatchedWritableCustomFieldRequestUiVisibility] = UNSET
    is_cloneable: Union[Unset, bool] = UNSET
    default: Union[Unset, Any] = UNSET
    weight: Union[Unset, int] = UNSET
    validation_minimum: Union[None, Unset, int] = UNSET
    validation_maximum: Union[None, Unset, int] = UNSET
    validation_regex: Union[Unset, str] = UNSET
    choice_set: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        content_types: Union[Unset, List[str]] = UNSET
        if not isinstance(self.content_types, Unset):
            content_types = self.content_types

        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        object_type = self.object_type

        name = self.name

        label = self.label

        group_name = self.group_name

        description = self.description

        required = self.required

        search_weight = self.search_weight

        filter_logic: Union[Unset, str] = UNSET
        if not isinstance(self.filter_logic, Unset):
            filter_logic = self.filter_logic.value

        ui_visibility: Union[Unset, str] = UNSET
        if not isinstance(self.ui_visibility, Unset):
            ui_visibility = self.ui_visibility.value

        is_cloneable = self.is_cloneable

        default = self.default

        weight = self.weight

        validation_minimum: Union[None, Unset, int]
        if isinstance(self.validation_minimum, Unset):
            validation_minimum = UNSET
        else:
            validation_minimum = self.validation_minimum

        validation_maximum: Union[None, Unset, int]
        if isinstance(self.validation_maximum, Unset):
            validation_maximum = UNSET
        else:
            validation_maximum = self.validation_maximum

        validation_regex = self.validation_regex

        choice_set: Union[None, Unset, int]
        if isinstance(self.choice_set, Unset):
            choice_set = UNSET
        else:
            choice_set = self.choice_set

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if content_types is not UNSET:
            field_dict["content_types"] = content_types
        if type is not UNSET:
            field_dict["type"] = type
        if object_type is not UNSET:
            field_dict["object_type"] = object_type
        if name is not UNSET:
            field_dict["name"] = name
        if label is not UNSET:
            field_dict["label"] = label
        if group_name is not UNSET:
            field_dict["group_name"] = group_name
        if description is not UNSET:
            field_dict["description"] = description
        if required is not UNSET:
            field_dict["required"] = required
        if search_weight is not UNSET:
            field_dict["search_weight"] = search_weight
        if filter_logic is not UNSET:
            field_dict["filter_logic"] = filter_logic
        if ui_visibility is not UNSET:
            field_dict["ui_visibility"] = ui_visibility
        if is_cloneable is not UNSET:
            field_dict["is_cloneable"] = is_cloneable
        if default is not UNSET:
            field_dict["default"] = default
        if weight is not UNSET:
            field_dict["weight"] = weight
        if validation_minimum is not UNSET:
            field_dict["validation_minimum"] = validation_minimum
        if validation_maximum is not UNSET:
            field_dict["validation_maximum"] = validation_maximum
        if validation_regex is not UNSET:
            field_dict["validation_regex"] = validation_regex
        if choice_set is not UNSET:
            field_dict["choice_set"] = choice_set

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        content_types: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.content_types, Unset):
            _temp_content_types = self.content_types
            content_types = (
                None,
                json.dumps(_temp_content_types).encode(),
                "application/json",
            )

        type: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.type, Unset):
            type = (None, str(self.type.value).encode(), "text/plain")

        object_type = (
            self.object_type
            if isinstance(self.object_type, Unset)
            else (None, str(self.object_type).encode(), "text/plain")
        )

        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        label = (
            self.label
            if isinstance(self.label, Unset)
            else (None, str(self.label).encode(), "text/plain")
        )

        group_name = (
            self.group_name
            if isinstance(self.group_name, Unset)
            else (None, str(self.group_name).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        required = (
            self.required
            if isinstance(self.required, Unset)
            else (None, str(self.required).encode(), "text/plain")
        )

        search_weight = (
            self.search_weight
            if isinstance(self.search_weight, Unset)
            else (None, str(self.search_weight).encode(), "text/plain")
        )

        filter_logic: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.filter_logic, Unset):
            filter_logic = (None, str(self.filter_logic.value).encode(), "text/plain")

        ui_visibility: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.ui_visibility, Unset):
            ui_visibility = (None, str(self.ui_visibility.value).encode(), "text/plain")

        is_cloneable = (
            self.is_cloneable
            if isinstance(self.is_cloneable, Unset)
            else (None, str(self.is_cloneable).encode(), "text/plain")
        )

        default = (
            self.default
            if isinstance(self.default, Unset)
            else (None, str(self.default).encode(), "text/plain")
        )

        weight = (
            self.weight
            if isinstance(self.weight, Unset)
            else (None, str(self.weight).encode(), "text/plain")
        )

        validation_minimum: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.validation_minimum, Unset):
            validation_minimum = UNSET
        elif isinstance(self.validation_minimum, int):
            validation_minimum = (
                None,
                str(self.validation_minimum).encode(),
                "text/plain",
            )
        else:
            validation_minimum = (
                None,
                str(self.validation_minimum).encode(),
                "text/plain",
            )

        validation_maximum: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.validation_maximum, Unset):
            validation_maximum = UNSET
        elif isinstance(self.validation_maximum, int):
            validation_maximum = (
                None,
                str(self.validation_maximum).encode(),
                "text/plain",
            )
        else:
            validation_maximum = (
                None,
                str(self.validation_maximum).encode(),
                "text/plain",
            )

        validation_regex = (
            self.validation_regex
            if isinstance(self.validation_regex, Unset)
            else (None, str(self.validation_regex).encode(), "text/plain")
        )

        choice_set: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.choice_set, Unset):
            choice_set = UNSET
        elif isinstance(self.choice_set, int):
            choice_set = (None, str(self.choice_set).encode(), "text/plain")
        else:
            choice_set = (None, str(self.choice_set).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if content_types is not UNSET:
            field_dict["content_types"] = content_types
        if type is not UNSET:
            field_dict["type"] = type
        if object_type is not UNSET:
            field_dict["object_type"] = object_type
        if name is not UNSET:
            field_dict["name"] = name
        if label is not UNSET:
            field_dict["label"] = label
        if group_name is not UNSET:
            field_dict["group_name"] = group_name
        if description is not UNSET:
            field_dict["description"] = description
        if required is not UNSET:
            field_dict["required"] = required
        if search_weight is not UNSET:
            field_dict["search_weight"] = search_weight
        if filter_logic is not UNSET:
            field_dict["filter_logic"] = filter_logic
        if ui_visibility is not UNSET:
            field_dict["ui_visibility"] = ui_visibility
        if is_cloneable is not UNSET:
            field_dict["is_cloneable"] = is_cloneable
        if default is not UNSET:
            field_dict["default"] = default
        if weight is not UNSET:
            field_dict["weight"] = weight
        if validation_minimum is not UNSET:
            field_dict["validation_minimum"] = validation_minimum
        if validation_maximum is not UNSET:
            field_dict["validation_maximum"] = validation_maximum
        if validation_regex is not UNSET:
            field_dict["validation_regex"] = validation_regex
        if choice_set is not UNSET:
            field_dict["choice_set"] = choice_set

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        content_types = cast(List[str], d.pop("content_types", UNSET))

        _type = d.pop("type", UNSET)
        type: Union[Unset, PatchedWritableCustomFieldRequestType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = PatchedWritableCustomFieldRequestType(_type)

        object_type = d.pop("object_type", UNSET)

        name = d.pop("name", UNSET)

        label = d.pop("label", UNSET)

        group_name = d.pop("group_name", UNSET)

        description = d.pop("description", UNSET)

        required = d.pop("required", UNSET)

        search_weight = d.pop("search_weight", UNSET)

        _filter_logic = d.pop("filter_logic", UNSET)
        filter_logic: Union[Unset, PatchedWritableCustomFieldRequestFilterLogic]
        if isinstance(_filter_logic, Unset):
            filter_logic = UNSET
        else:
            filter_logic = PatchedWritableCustomFieldRequestFilterLogic(_filter_logic)

        _ui_visibility = d.pop("ui_visibility", UNSET)
        ui_visibility: Union[Unset, PatchedWritableCustomFieldRequestUiVisibility]
        if isinstance(_ui_visibility, Unset):
            ui_visibility = UNSET
        else:
            ui_visibility = PatchedWritableCustomFieldRequestUiVisibility(
                _ui_visibility
            )

        is_cloneable = d.pop("is_cloneable", UNSET)

        default = d.pop("default", UNSET)

        weight = d.pop("weight", UNSET)

        def _parse_validation_minimum(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        validation_minimum = _parse_validation_minimum(
            d.pop("validation_minimum", UNSET)
        )

        def _parse_validation_maximum(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        validation_maximum = _parse_validation_maximum(
            d.pop("validation_maximum", UNSET)
        )

        validation_regex = d.pop("validation_regex", UNSET)

        def _parse_choice_set(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        choice_set = _parse_choice_set(d.pop("choice_set", UNSET))

        patched_writable_custom_field_request = cls(
            content_types=content_types,
            type=type,
            object_type=object_type,
            name=name,
            label=label,
            group_name=group_name,
            description=description,
            required=required,
            search_weight=search_weight,
            filter_logic=filter_logic,
            ui_visibility=ui_visibility,
            is_cloneable=is_cloneable,
            default=default,
            weight=weight,
            validation_minimum=validation_minimum,
            validation_maximum=validation_maximum,
            validation_regex=validation_regex,
            choice_set=choice_set,
        )

        patched_writable_custom_field_request.additional_properties = d
        return patched_writable_custom_field_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
