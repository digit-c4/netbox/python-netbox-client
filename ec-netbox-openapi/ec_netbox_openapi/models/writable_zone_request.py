import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_zone_request_status import WritableZoneRequestStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_name_server_request import NestedNameServerRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_zone_request_custom_fields import (
        WritableZoneRequestCustomFields,
    )


T = TypeVar("T", bound="WritableZoneRequest")


@_attrs_define
class WritableZoneRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        view (Union[None, int]):
        soa_ttl (int):
        soa_mname (int):
        soa_rname (str):
        soa_refresh (int):
        soa_retry (int):
        soa_expire (int):
        soa_minimum (int):
        nameservers (Union[Unset, List['NestedNameServerRequest']]): Nameservers for the zone
        status (Union[Unset, WritableZoneRequestStatus]): * `active` - Active
            * `reserved` - Reserved
            * `deprecated` - Deprecated
            * `parked` - Parked
        description (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        default_ttl (Union[Unset, int]):
        soa_serial (Union[None, Unset, int]):
        soa_serial_auto (Union[Unset, bool]): Automatically generate the SOA Serial field
        custom_fields (Union[Unset, WritableZoneRequestCustomFields]):
        tenant (Union[None, Unset, int]):
    """

    name: str
    view: Union[None, int]
    soa_ttl: int
    soa_mname: int
    soa_rname: str
    soa_refresh: int
    soa_retry: int
    soa_expire: int
    soa_minimum: int
    nameservers: Union[Unset, List["NestedNameServerRequest"]] = UNSET
    status: Union[Unset, WritableZoneRequestStatus] = UNSET
    description: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    default_ttl: Union[Unset, int] = UNSET
    soa_serial: Union[None, Unset, int] = UNSET
    soa_serial_auto: Union[Unset, bool] = UNSET
    custom_fields: Union[Unset, "WritableZoneRequestCustomFields"] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        view: Union[None, int]
        view = self.view

        soa_ttl = self.soa_ttl

        soa_mname = self.soa_mname

        soa_rname = self.soa_rname

        soa_refresh = self.soa_refresh

        soa_retry = self.soa_retry

        soa_expire = self.soa_expire

        soa_minimum = self.soa_minimum

        nameservers: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.nameservers, Unset):
            nameservers = []
            for nameservers_item_data in self.nameservers:
                nameservers_item = nameservers_item_data.to_dict()
                nameservers.append(nameservers_item)

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        description = self.description

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        default_ttl = self.default_ttl

        soa_serial: Union[None, Unset, int]
        if isinstance(self.soa_serial, Unset):
            soa_serial = UNSET
        else:
            soa_serial = self.soa_serial

        soa_serial_auto = self.soa_serial_auto

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "view": view,
                "soa_ttl": soa_ttl,
                "soa_mname": soa_mname,
                "soa_rname": soa_rname,
                "soa_refresh": soa_refresh,
                "soa_retry": soa_retry,
                "soa_expire": soa_expire,
                "soa_minimum": soa_minimum,
            }
        )
        if nameservers is not UNSET:
            field_dict["nameservers"] = nameservers
        if status is not UNSET:
            field_dict["status"] = status
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if default_ttl is not UNSET:
            field_dict["default_ttl"] = default_ttl
        if soa_serial is not UNSET:
            field_dict["soa_serial"] = soa_serial
        if soa_serial_auto is not UNSET:
            field_dict["soa_serial_auto"] = soa_serial_auto
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tenant is not UNSET:
            field_dict["tenant"] = tenant

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (None, str(self.name).encode(), "text/plain")

        view: Tuple[None, bytes, str]

        if isinstance(self.view, int):
            view = (None, str(self.view).encode(), "text/plain")
        else:
            view = (None, str(self.view).encode(), "text/plain")

        soa_ttl = (None, str(self.soa_ttl).encode(), "text/plain")

        soa_mname = (None, str(self.soa_mname).encode(), "text/plain")

        soa_rname = (None, str(self.soa_rname).encode(), "text/plain")

        soa_refresh = (None, str(self.soa_refresh).encode(), "text/plain")

        soa_retry = (None, str(self.soa_retry).encode(), "text/plain")

        soa_expire = (None, str(self.soa_expire).encode(), "text/plain")

        soa_minimum = (None, str(self.soa_minimum).encode(), "text/plain")

        nameservers: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.nameservers, Unset):
            _temp_nameservers = []
            for nameservers_item_data in self.nameservers:
                nameservers_item = nameservers_item_data.to_dict()
                _temp_nameservers.append(nameservers_item)
            nameservers = (
                None,
                json.dumps(_temp_nameservers).encode(),
                "application/json",
            )

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        default_ttl = (
            self.default_ttl
            if isinstance(self.default_ttl, Unset)
            else (None, str(self.default_ttl).encode(), "text/plain")
        )

        soa_serial: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.soa_serial, Unset):
            soa_serial = UNSET
        elif isinstance(self.soa_serial, int):
            soa_serial = (None, str(self.soa_serial).encode(), "text/plain")
        else:
            soa_serial = (None, str(self.soa_serial).encode(), "text/plain")

        soa_serial_auto = (
            self.soa_serial_auto
            if isinstance(self.soa_serial_auto, Unset)
            else (None, str(self.soa_serial_auto).encode(), "text/plain")
        )

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "name": name,
                "view": view,
                "soa_ttl": soa_ttl,
                "soa_mname": soa_mname,
                "soa_rname": soa_rname,
                "soa_refresh": soa_refresh,
                "soa_retry": soa_retry,
                "soa_expire": soa_expire,
                "soa_minimum": soa_minimum,
            }
        )
        if nameservers is not UNSET:
            field_dict["nameservers"] = nameservers
        if status is not UNSET:
            field_dict["status"] = status
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if default_ttl is not UNSET:
            field_dict["default_ttl"] = default_ttl
        if soa_serial is not UNSET:
            field_dict["soa_serial"] = soa_serial
        if soa_serial_auto is not UNSET:
            field_dict["soa_serial_auto"] = soa_serial_auto
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tenant is not UNSET:
            field_dict["tenant"] = tenant

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_name_server_request import NestedNameServerRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_zone_request_custom_fields import (
            WritableZoneRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name")

        def _parse_view(data: object) -> Union[None, int]:
            if data is None:
                return data
            return cast(Union[None, int], data)

        view = _parse_view(d.pop("view"))

        soa_ttl = d.pop("soa_ttl")

        soa_mname = d.pop("soa_mname")

        soa_rname = d.pop("soa_rname")

        soa_refresh = d.pop("soa_refresh")

        soa_retry = d.pop("soa_retry")

        soa_expire = d.pop("soa_expire")

        soa_minimum = d.pop("soa_minimum")

        nameservers = []
        _nameservers = d.pop("nameservers", UNSET)
        for nameservers_item_data in _nameservers or []:
            nameservers_item = NestedNameServerRequest.from_dict(nameservers_item_data)

            nameservers.append(nameservers_item)

        _status = d.pop("status", UNSET)
        status: Union[Unset, WritableZoneRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = WritableZoneRequestStatus(_status)

        description = d.pop("description", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        default_ttl = d.pop("default_ttl", UNSET)

        def _parse_soa_serial(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        soa_serial = _parse_soa_serial(d.pop("soa_serial", UNSET))

        soa_serial_auto = d.pop("soa_serial_auto", UNSET)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableZoneRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableZoneRequestCustomFields.from_dict(_custom_fields)

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        writable_zone_request = cls(
            name=name,
            view=view,
            soa_ttl=soa_ttl,
            soa_mname=soa_mname,
            soa_rname=soa_rname,
            soa_refresh=soa_refresh,
            soa_retry=soa_retry,
            soa_expire=soa_expire,
            soa_minimum=soa_minimum,
            nameservers=nameservers,
            status=status,
            description=description,
            tags=tags,
            default_ttl=default_ttl,
            soa_serial=soa_serial,
            soa_serial_auto=soa_serial_auto,
            custom_fields=custom_fields,
            tenant=tenant,
        )

        writable_zone_request.additional_properties = d
        return writable_zone_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
