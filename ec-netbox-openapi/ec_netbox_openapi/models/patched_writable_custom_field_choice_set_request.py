import json
from typing import (
    Any,
    Dict,
    List,
    Tuple,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_custom_field_choice_set_request_base_choices import (
    PatchedWritableCustomFieldChoiceSetRequestBaseChoices,
)
from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedWritableCustomFieldChoiceSetRequest")


@_attrs_define
class PatchedWritableCustomFieldChoiceSetRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            name (Union[Unset, str]):
            description (Union[Unset, str]):
            base_choices (Union[Unset, PatchedWritableCustomFieldChoiceSetRequestBaseChoices]): Base set of predefined
                choices (optional)

                * `IATA` - IATA (Airport codes)
                * `ISO_3166` - ISO 3166 (Country codes)
                * `UN_LOCODE` - UN/LOCODE (Location codes)
            extra_choices (Union[List[List[str]], None, Unset]):
            order_alphabetically (Union[Unset, bool]): Choices are automatically ordered alphabetically
    """

    name: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    base_choices: Union[
        Unset, PatchedWritableCustomFieldChoiceSetRequestBaseChoices
    ] = UNSET
    extra_choices: Union[List[List[str]], None, Unset] = UNSET
    order_alphabetically: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        description = self.description

        base_choices: Union[Unset, str] = UNSET
        if not isinstance(self.base_choices, Unset):
            base_choices = self.base_choices.value

        extra_choices: Union[List[List[str]], None, Unset]
        if isinstance(self.extra_choices, Unset):
            extra_choices = UNSET
        elif isinstance(self.extra_choices, list):
            extra_choices = []
            for extra_choices_type_0_item_data in self.extra_choices:
                extra_choices_type_0_item = extra_choices_type_0_item_data

                extra_choices.append(extra_choices_type_0_item)

        else:
            extra_choices = self.extra_choices

        order_alphabetically = self.order_alphabetically

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if description is not UNSET:
            field_dict["description"] = description
        if base_choices is not UNSET:
            field_dict["base_choices"] = base_choices
        if extra_choices is not UNSET:
            field_dict["extra_choices"] = extra_choices
        if order_alphabetically is not UNSET:
            field_dict["order_alphabetically"] = order_alphabetically

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        base_choices: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.base_choices, Unset):
            base_choices = (None, str(self.base_choices.value).encode(), "text/plain")

        extra_choices: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.extra_choices, Unset):
            extra_choices = UNSET
        elif isinstance(self.extra_choices, list):
            _temp_extra_choices = []
            for extra_choices_type_0_item_data in self.extra_choices:
                extra_choices_type_0_item = extra_choices_type_0_item_data

                _temp_extra_choices.append(extra_choices_type_0_item)
            extra_choices = (
                None,
                json.dumps(_temp_extra_choices).encode(),
                "application/json",
            )
        else:
            extra_choices = (None, str(self.extra_choices).encode(), "text/plain")

        order_alphabetically = (
            self.order_alphabetically
            if isinstance(self.order_alphabetically, Unset)
            else (None, str(self.order_alphabetically).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if description is not UNSET:
            field_dict["description"] = description
        if base_choices is not UNSET:
            field_dict["base_choices"] = base_choices
        if extra_choices is not UNSET:
            field_dict["extra_choices"] = extra_choices
        if order_alphabetically is not UNSET:
            field_dict["order_alphabetically"] = order_alphabetically

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name", UNSET)

        description = d.pop("description", UNSET)

        _base_choices = d.pop("base_choices", UNSET)
        base_choices: Union[
            Unset, PatchedWritableCustomFieldChoiceSetRequestBaseChoices
        ]
        if isinstance(_base_choices, Unset):
            base_choices = UNSET
        else:
            base_choices = PatchedWritableCustomFieldChoiceSetRequestBaseChoices(
                _base_choices
            )

        def _parse_extra_choices(data: object) -> Union[List[List[str]], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                extra_choices_type_0 = []
                _extra_choices_type_0 = data
                for extra_choices_type_0_item_data in _extra_choices_type_0:
                    extra_choices_type_0_item = cast(
                        List[str], extra_choices_type_0_item_data
                    )

                    extra_choices_type_0.append(extra_choices_type_0_item)

                return extra_choices_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[List[str]], None, Unset], data)

        extra_choices = _parse_extra_choices(d.pop("extra_choices", UNSET))

        order_alphabetically = d.pop("order_alphabetically", UNSET)

        patched_writable_custom_field_choice_set_request = cls(
            name=name,
            description=description,
            base_choices=base_choices,
            extra_choices=extra_choices,
            order_alphabetically=order_alphabetically,
        )

        patched_writable_custom_field_choice_set_request.additional_properties = d
        return patched_writable_custom_field_choice_set_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
