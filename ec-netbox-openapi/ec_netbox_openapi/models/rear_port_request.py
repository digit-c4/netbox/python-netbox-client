from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.rear_port_request_type import RearPortRequestType
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.component_nested_module_request import ComponentNestedModuleRequest
    from ..models.nested_device_request import NestedDeviceRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.rear_port_request_custom_fields import RearPortRequestCustomFields


T = TypeVar("T", bound="RearPortRequest")


@_attrs_define
class RearPortRequest:
    """Adds support for custom fields and tags.

    Attributes:
        device (NestedDeviceRequest): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (str):
        type (RearPortRequestType): * `8p8c` - 8P8C
            * `8p6c` - 8P6C
            * `8p4c` - 8P4C
            * `8p2c` - 8P2C
            * `6p6c` - 6P6C
            * `6p4c` - 6P4C
            * `6p2c` - 6P2C
            * `4p4c` - 4P4C
            * `4p2c` - 4P2C
            * `gg45` - GG45
            * `tera-4p` - TERA 4P
            * `tera-2p` - TERA 2P
            * `tera-1p` - TERA 1P
            * `110-punch` - 110 Punch
            * `bnc` - BNC
            * `f` - F Connector
            * `n` - N Connector
            * `mrj21` - MRJ21
            * `fc` - FC
            * `lc` - LC
            * `lc-pc` - LC/PC
            * `lc-upc` - LC/UPC
            * `lc-apc` - LC/APC
            * `lsh` - LSH
            * `lsh-pc` - LSH/PC
            * `lsh-upc` - LSH/UPC
            * `lsh-apc` - LSH/APC
            * `lx5` - LX.5
            * `lx5-pc` - LX.5/PC
            * `lx5-upc` - LX.5/UPC
            * `lx5-apc` - LX.5/APC
            * `mpo` - MPO
            * `mtrj` - MTRJ
            * `sc` - SC
            * `sc-pc` - SC/PC
            * `sc-upc` - SC/UPC
            * `sc-apc` - SC/APC
            * `st` - ST
            * `cs` - CS
            * `sn` - SN
            * `sma-905` - SMA 905
            * `sma-906` - SMA 906
            * `urm-p2` - URM-P2
            * `urm-p4` - URM-P4
            * `urm-p8` - URM-P8
            * `splice` - Splice
            * `other` - Other
        module (Union['ComponentNestedModuleRequest', None, Unset]):
        label (Union[Unset, str]): Physical label
        color (Union[Unset, str]):
        positions (Union[Unset, int]): Number of front ports which may be mapped
        description (Union[Unset, str]):
        mark_connected (Union[Unset, bool]): Treat as if a cable is connected
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, RearPortRequestCustomFields]):
    """

    device: "NestedDeviceRequest"
    name: str
    type: RearPortRequestType
    module: Union["ComponentNestedModuleRequest", None, Unset] = UNSET
    label: Union[Unset, str] = UNSET
    color: Union[Unset, str] = UNSET
    positions: Union[Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    mark_connected: Union[Unset, bool] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "RearPortRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.component_nested_module_request import (
            ComponentNestedModuleRequest,
        )

        device = self.device.to_dict()

        name = self.name

        type = self.type.value

        module: Union[Dict[str, Any], None, Unset]
        if isinstance(self.module, Unset):
            module = UNSET
        elif isinstance(self.module, ComponentNestedModuleRequest):
            module = self.module.to_dict()
        else:
            module = self.module

        label = self.label

        color = self.color

        positions = self.positions

        description = self.description

        mark_connected = self.mark_connected

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "device": device,
                "name": name,
                "type": type,
            }
        )
        if module is not UNSET:
            field_dict["module"] = module
        if label is not UNSET:
            field_dict["label"] = label
        if color is not UNSET:
            field_dict["color"] = color
        if positions is not UNSET:
            field_dict["positions"] = positions
        if description is not UNSET:
            field_dict["description"] = description
        if mark_connected is not UNSET:
            field_dict["mark_connected"] = mark_connected
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.component_nested_module_request import (
            ComponentNestedModuleRequest,
        )
        from ..models.nested_device_request import NestedDeviceRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.rear_port_request_custom_fields import RearPortRequestCustomFields

        d = src_dict.copy()
        device = NestedDeviceRequest.from_dict(d.pop("device"))

        name = d.pop("name")

        type = RearPortRequestType(d.pop("type"))

        def _parse_module(
            data: object,
        ) -> Union["ComponentNestedModuleRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                module_type_1 = ComponentNestedModuleRequest.from_dict(data)

                return module_type_1
            except:  # noqa: E722
                pass
            return cast(Union["ComponentNestedModuleRequest", None, Unset], data)

        module = _parse_module(d.pop("module", UNSET))

        label = d.pop("label", UNSET)

        color = d.pop("color", UNSET)

        positions = d.pop("positions", UNSET)

        description = d.pop("description", UNSET)

        mark_connected = d.pop("mark_connected", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, RearPortRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = RearPortRequestCustomFields.from_dict(_custom_fields)

        rear_port_request = cls(
            device=device,
            name=name,
            type=type,
            module=module,
            label=label,
            color=color,
            positions=positions,
            description=description,
            mark_connected=mark_connected,
            tags=tags,
            custom_fields=custom_fields,
        )

        rear_port_request.additional_properties = d
        return rear_port_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
