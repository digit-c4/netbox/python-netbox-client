from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.device_with_config_context_request_airflow import (
    DeviceWithConfigContextRequestAirflow,
)
from ..models.device_with_config_context_request_face import (
    DeviceWithConfigContextRequestFace,
)
from ..models.device_with_config_context_request_status import (
    DeviceWithConfigContextRequestStatus,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.device_with_config_context_request_custom_fields import (
        DeviceWithConfigContextRequestCustomFields,
    )
    from ..models.nested_cluster_request import NestedClusterRequest
    from ..models.nested_config_template_request import NestedConfigTemplateRequest
    from ..models.nested_device_role_request import NestedDeviceRoleRequest
    from ..models.nested_device_type_request import NestedDeviceTypeRequest
    from ..models.nested_ip_address_request import NestedIPAddressRequest
    from ..models.nested_location_request import NestedLocationRequest
    from ..models.nested_platform_request import NestedPlatformRequest
    from ..models.nested_rack_request import NestedRackRequest
    from ..models.nested_site_request import NestedSiteRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_tenant_request import NestedTenantRequest
    from ..models.nested_virtual_chassis_request import NestedVirtualChassisRequest


T = TypeVar("T", bound="DeviceWithConfigContextRequest")


@_attrs_define
class DeviceWithConfigContextRequest:
    """Adds support for custom fields and tags.

    Attributes:
        device_type (NestedDeviceTypeRequest): Represents an object related through a ForeignKey field. On write, it
            accepts a primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        role (NestedDeviceRoleRequest): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        site (NestedSiteRequest): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (Union[None, Unset, str]):
        tenant (Union['NestedTenantRequest', None, Unset]):
        platform (Union['NestedPlatformRequest', None, Unset]):
        serial (Union[Unset, str]): Chassis serial number, assigned by the manufacturer
        asset_tag (Union[None, Unset, str]): A unique tag used to identify this device
        location (Union['NestedLocationRequest', None, Unset]):
        rack (Union['NestedRackRequest', None, Unset]):
        position (Union[None, Unset, float]):
        face (Union[Unset, DeviceWithConfigContextRequestFace]): * `front` - Front
            * `rear` - Rear
        latitude (Union[None, Unset, float]): GPS coordinate in decimal format (xx.yyyyyy)
        longitude (Union[None, Unset, float]): GPS coordinate in decimal format (xx.yyyyyy)
        status (Union[Unset, DeviceWithConfigContextRequestStatus]): * `offline` - Offline
            * `active` - Active
            * `planned` - Planned
            * `staged` - Staged
            * `failed` - Failed
            * `inventory` - Inventory
            * `decommissioning` - Decommissioning
        airflow (Union[Unset, DeviceWithConfigContextRequestAirflow]): * `front-to-rear` - Front to rear
            * `rear-to-front` - Rear to front
            * `left-to-right` - Left to right
            * `right-to-left` - Right to left
            * `side-to-rear` - Side to rear
            * `passive` - Passive
            * `mixed` - Mixed
        primary_ip4 (Union['NestedIPAddressRequest', None, Unset]):
        primary_ip6 (Union['NestedIPAddressRequest', None, Unset]):
        oob_ip (Union['NestedIPAddressRequest', None, Unset]):
        cluster (Union['NestedClusterRequest', None, Unset]):
        virtual_chassis (Union['NestedVirtualChassisRequest', None, Unset]):
        vc_position (Union[None, Unset, int]):
        vc_priority (Union[None, Unset, int]): Virtual chassis master election priority
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        config_template (Union['NestedConfigTemplateRequest', None, Unset]):
        local_context_data (Union[Unset, Any]): Local config context data takes precedence over source contexts in the
            final rendered config context
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, DeviceWithConfigContextRequestCustomFields]):
    """

    device_type: "NestedDeviceTypeRequest"
    role: "NestedDeviceRoleRequest"
    site: "NestedSiteRequest"
    name: Union[None, Unset, str] = UNSET
    tenant: Union["NestedTenantRequest", None, Unset] = UNSET
    platform: Union["NestedPlatformRequest", None, Unset] = UNSET
    serial: Union[Unset, str] = UNSET
    asset_tag: Union[None, Unset, str] = UNSET
    location: Union["NestedLocationRequest", None, Unset] = UNSET
    rack: Union["NestedRackRequest", None, Unset] = UNSET
    position: Union[None, Unset, float] = UNSET
    face: Union[Unset, DeviceWithConfigContextRequestFace] = UNSET
    latitude: Union[None, Unset, float] = UNSET
    longitude: Union[None, Unset, float] = UNSET
    status: Union[Unset, DeviceWithConfigContextRequestStatus] = UNSET
    airflow: Union[Unset, DeviceWithConfigContextRequestAirflow] = UNSET
    primary_ip4: Union["NestedIPAddressRequest", None, Unset] = UNSET
    primary_ip6: Union["NestedIPAddressRequest", None, Unset] = UNSET
    oob_ip: Union["NestedIPAddressRequest", None, Unset] = UNSET
    cluster: Union["NestedClusterRequest", None, Unset] = UNSET
    virtual_chassis: Union["NestedVirtualChassisRequest", None, Unset] = UNSET
    vc_position: Union[None, Unset, int] = UNSET
    vc_priority: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    config_template: Union["NestedConfigTemplateRequest", None, Unset] = UNSET
    local_context_data: Union[Unset, Any] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "DeviceWithConfigContextRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_cluster_request import NestedClusterRequest
        from ..models.nested_config_template_request import NestedConfigTemplateRequest
        from ..models.nested_ip_address_request import NestedIPAddressRequest
        from ..models.nested_location_request import NestedLocationRequest
        from ..models.nested_platform_request import NestedPlatformRequest
        from ..models.nested_rack_request import NestedRackRequest
        from ..models.nested_tenant_request import NestedTenantRequest
        from ..models.nested_virtual_chassis_request import NestedVirtualChassisRequest

        device_type = self.device_type.to_dict()

        role = self.role.to_dict()

        site = self.site.to_dict()

        name: Union[None, Unset, str]
        if isinstance(self.name, Unset):
            name = UNSET
        else:
            name = self.name

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenantRequest):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        platform: Union[Dict[str, Any], None, Unset]
        if isinstance(self.platform, Unset):
            platform = UNSET
        elif isinstance(self.platform, NestedPlatformRequest):
            platform = self.platform.to_dict()
        else:
            platform = self.platform

        serial = self.serial

        asset_tag: Union[None, Unset, str]
        if isinstance(self.asset_tag, Unset):
            asset_tag = UNSET
        else:
            asset_tag = self.asset_tag

        location: Union[Dict[str, Any], None, Unset]
        if isinstance(self.location, Unset):
            location = UNSET
        elif isinstance(self.location, NestedLocationRequest):
            location = self.location.to_dict()
        else:
            location = self.location

        rack: Union[Dict[str, Any], None, Unset]
        if isinstance(self.rack, Unset):
            rack = UNSET
        elif isinstance(self.rack, NestedRackRequest):
            rack = self.rack.to_dict()
        else:
            rack = self.rack

        position: Union[None, Unset, float]
        if isinstance(self.position, Unset):
            position = UNSET
        else:
            position = self.position

        face: Union[Unset, str] = UNSET
        if not isinstance(self.face, Unset):
            face = self.face.value

        latitude: Union[None, Unset, float]
        if isinstance(self.latitude, Unset):
            latitude = UNSET
        else:
            latitude = self.latitude

        longitude: Union[None, Unset, float]
        if isinstance(self.longitude, Unset):
            longitude = UNSET
        else:
            longitude = self.longitude

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        airflow: Union[Unset, str] = UNSET
        if not isinstance(self.airflow, Unset):
            airflow = self.airflow.value

        primary_ip4: Union[Dict[str, Any], None, Unset]
        if isinstance(self.primary_ip4, Unset):
            primary_ip4 = UNSET
        elif isinstance(self.primary_ip4, NestedIPAddressRequest):
            primary_ip4 = self.primary_ip4.to_dict()
        else:
            primary_ip4 = self.primary_ip4

        primary_ip6: Union[Dict[str, Any], None, Unset]
        if isinstance(self.primary_ip6, Unset):
            primary_ip6 = UNSET
        elif isinstance(self.primary_ip6, NestedIPAddressRequest):
            primary_ip6 = self.primary_ip6.to_dict()
        else:
            primary_ip6 = self.primary_ip6

        oob_ip: Union[Dict[str, Any], None, Unset]
        if isinstance(self.oob_ip, Unset):
            oob_ip = UNSET
        elif isinstance(self.oob_ip, NestedIPAddressRequest):
            oob_ip = self.oob_ip.to_dict()
        else:
            oob_ip = self.oob_ip

        cluster: Union[Dict[str, Any], None, Unset]
        if isinstance(self.cluster, Unset):
            cluster = UNSET
        elif isinstance(self.cluster, NestedClusterRequest):
            cluster = self.cluster.to_dict()
        else:
            cluster = self.cluster

        virtual_chassis: Union[Dict[str, Any], None, Unset]
        if isinstance(self.virtual_chassis, Unset):
            virtual_chassis = UNSET
        elif isinstance(self.virtual_chassis, NestedVirtualChassisRequest):
            virtual_chassis = self.virtual_chassis.to_dict()
        else:
            virtual_chassis = self.virtual_chassis

        vc_position: Union[None, Unset, int]
        if isinstance(self.vc_position, Unset):
            vc_position = UNSET
        else:
            vc_position = self.vc_position

        vc_priority: Union[None, Unset, int]
        if isinstance(self.vc_priority, Unset):
            vc_priority = UNSET
        else:
            vc_priority = self.vc_priority

        description = self.description

        comments = self.comments

        config_template: Union[Dict[str, Any], None, Unset]
        if isinstance(self.config_template, Unset):
            config_template = UNSET
        elif isinstance(self.config_template, NestedConfigTemplateRequest):
            config_template = self.config_template.to_dict()
        else:
            config_template = self.config_template

        local_context_data = self.local_context_data

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "device_type": device_type,
                "role": role,
                "site": site,
            }
        )
        if name is not UNSET:
            field_dict["name"] = name
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if platform is not UNSET:
            field_dict["platform"] = platform
        if serial is not UNSET:
            field_dict["serial"] = serial
        if asset_tag is not UNSET:
            field_dict["asset_tag"] = asset_tag
        if location is not UNSET:
            field_dict["location"] = location
        if rack is not UNSET:
            field_dict["rack"] = rack
        if position is not UNSET:
            field_dict["position"] = position
        if face is not UNSET:
            field_dict["face"] = face
        if latitude is not UNSET:
            field_dict["latitude"] = latitude
        if longitude is not UNSET:
            field_dict["longitude"] = longitude
        if status is not UNSET:
            field_dict["status"] = status
        if airflow is not UNSET:
            field_dict["airflow"] = airflow
        if primary_ip4 is not UNSET:
            field_dict["primary_ip4"] = primary_ip4
        if primary_ip6 is not UNSET:
            field_dict["primary_ip6"] = primary_ip6
        if oob_ip is not UNSET:
            field_dict["oob_ip"] = oob_ip
        if cluster is not UNSET:
            field_dict["cluster"] = cluster
        if virtual_chassis is not UNSET:
            field_dict["virtual_chassis"] = virtual_chassis
        if vc_position is not UNSET:
            field_dict["vc_position"] = vc_position
        if vc_priority is not UNSET:
            field_dict["vc_priority"] = vc_priority
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if config_template is not UNSET:
            field_dict["config_template"] = config_template
        if local_context_data is not UNSET:
            field_dict["local_context_data"] = local_context_data
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.device_with_config_context_request_custom_fields import (
            DeviceWithConfigContextRequestCustomFields,
        )
        from ..models.nested_cluster_request import NestedClusterRequest
        from ..models.nested_config_template_request import NestedConfigTemplateRequest
        from ..models.nested_device_role_request import NestedDeviceRoleRequest
        from ..models.nested_device_type_request import NestedDeviceTypeRequest
        from ..models.nested_ip_address_request import NestedIPAddressRequest
        from ..models.nested_location_request import NestedLocationRequest
        from ..models.nested_platform_request import NestedPlatformRequest
        from ..models.nested_rack_request import NestedRackRequest
        from ..models.nested_site_request import NestedSiteRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_tenant_request import NestedTenantRequest
        from ..models.nested_virtual_chassis_request import NestedVirtualChassisRequest

        d = src_dict.copy()
        device_type = NestedDeviceTypeRequest.from_dict(d.pop("device_type"))

        role = NestedDeviceRoleRequest.from_dict(d.pop("role"))

        site = NestedSiteRequest.from_dict(d.pop("site"))

        def _parse_name(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        name = _parse_name(d.pop("name", UNSET))

        def _parse_tenant(data: object) -> Union["NestedTenantRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenantRequest.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenantRequest", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        def _parse_platform(
            data: object,
        ) -> Union["NestedPlatformRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                platform_type_1 = NestedPlatformRequest.from_dict(data)

                return platform_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedPlatformRequest", None, Unset], data)

        platform = _parse_platform(d.pop("platform", UNSET))

        serial = d.pop("serial", UNSET)

        def _parse_asset_tag(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        asset_tag = _parse_asset_tag(d.pop("asset_tag", UNSET))

        def _parse_location(
            data: object,
        ) -> Union["NestedLocationRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                location_type_1 = NestedLocationRequest.from_dict(data)

                return location_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedLocationRequest", None, Unset], data)

        location = _parse_location(d.pop("location", UNSET))

        def _parse_rack(data: object) -> Union["NestedRackRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                rack_type_1 = NestedRackRequest.from_dict(data)

                return rack_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedRackRequest", None, Unset], data)

        rack = _parse_rack(d.pop("rack", UNSET))

        def _parse_position(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        position = _parse_position(d.pop("position", UNSET))

        _face = d.pop("face", UNSET)
        face: Union[Unset, DeviceWithConfigContextRequestFace]
        if isinstance(_face, Unset):
            face = UNSET
        else:
            face = DeviceWithConfigContextRequestFace(_face)

        def _parse_latitude(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        latitude = _parse_latitude(d.pop("latitude", UNSET))

        def _parse_longitude(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        longitude = _parse_longitude(d.pop("longitude", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, DeviceWithConfigContextRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = DeviceWithConfigContextRequestStatus(_status)

        _airflow = d.pop("airflow", UNSET)
        airflow: Union[Unset, DeviceWithConfigContextRequestAirflow]
        if isinstance(_airflow, Unset):
            airflow = UNSET
        else:
            airflow = DeviceWithConfigContextRequestAirflow(_airflow)

        def _parse_primary_ip4(
            data: object,
        ) -> Union["NestedIPAddressRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                primary_ip4_type_1 = NestedIPAddressRequest.from_dict(data)

                return primary_ip4_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedIPAddressRequest", None, Unset], data)

        primary_ip4 = _parse_primary_ip4(d.pop("primary_ip4", UNSET))

        def _parse_primary_ip6(
            data: object,
        ) -> Union["NestedIPAddressRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                primary_ip6_type_1 = NestedIPAddressRequest.from_dict(data)

                return primary_ip6_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedIPAddressRequest", None, Unset], data)

        primary_ip6 = _parse_primary_ip6(d.pop("primary_ip6", UNSET))

        def _parse_oob_ip(data: object) -> Union["NestedIPAddressRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                oob_ip_type_1 = NestedIPAddressRequest.from_dict(data)

                return oob_ip_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedIPAddressRequest", None, Unset], data)

        oob_ip = _parse_oob_ip(d.pop("oob_ip", UNSET))

        def _parse_cluster(data: object) -> Union["NestedClusterRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                cluster_type_1 = NestedClusterRequest.from_dict(data)

                return cluster_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedClusterRequest", None, Unset], data)

        cluster = _parse_cluster(d.pop("cluster", UNSET))

        def _parse_virtual_chassis(
            data: object,
        ) -> Union["NestedVirtualChassisRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                virtual_chassis_type_1 = NestedVirtualChassisRequest.from_dict(data)

                return virtual_chassis_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVirtualChassisRequest", None, Unset], data)

        virtual_chassis = _parse_virtual_chassis(d.pop("virtual_chassis", UNSET))

        def _parse_vc_position(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vc_position = _parse_vc_position(d.pop("vc_position", UNSET))

        def _parse_vc_priority(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vc_priority = _parse_vc_priority(d.pop("vc_priority", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        def _parse_config_template(
            data: object,
        ) -> Union["NestedConfigTemplateRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                config_template_type_1 = NestedConfigTemplateRequest.from_dict(data)

                return config_template_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedConfigTemplateRequest", None, Unset], data)

        config_template = _parse_config_template(d.pop("config_template", UNSET))

        local_context_data = d.pop("local_context_data", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, DeviceWithConfigContextRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = DeviceWithConfigContextRequestCustomFields.from_dict(
                _custom_fields
            )

        device_with_config_context_request = cls(
            device_type=device_type,
            role=role,
            site=site,
            name=name,
            tenant=tenant,
            platform=platform,
            serial=serial,
            asset_tag=asset_tag,
            location=location,
            rack=rack,
            position=position,
            face=face,
            latitude=latitude,
            longitude=longitude,
            status=status,
            airflow=airflow,
            primary_ip4=primary_ip4,
            primary_ip6=primary_ip6,
            oob_ip=oob_ip,
            cluster=cluster,
            virtual_chassis=virtual_chassis,
            vc_position=vc_position,
            vc_priority=vc_priority,
            description=description,
            comments=comments,
            config_template=config_template,
            local_context_data=local_context_data,
            tags=tags,
            custom_fields=custom_fields,
        )

        device_with_config_context_request.additional_properties = d
        return device_with_config_context_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
