import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_virtual_machine_with_config_context_request_status import (
    PatchedWritableVirtualMachineWithConfigContextRequestStatus,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_writable_virtual_machine_with_config_context_request_custom_fields import (
        PatchedWritableVirtualMachineWithConfigContextRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedWritableVirtualMachineWithConfigContextRequest")


@_attrs_define
class PatchedWritableVirtualMachineWithConfigContextRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (Union[Unset, str]):
        status (Union[Unset, PatchedWritableVirtualMachineWithConfigContextRequestStatus]): * `offline` - Offline
            * `active` - Active
            * `planned` - Planned
            * `staged` - Staged
            * `failed` - Failed
            * `decommissioning` - Decommissioning
        site (Union[None, Unset, int]):
        cluster (Union[None, Unset, int]):
        device (Union[None, Unset, int]):
        role (Union[None, Unset, int]):
        tenant (Union[None, Unset, int]):
        platform (Union[None, Unset, int]):
        primary_ip4 (Union[None, Unset, int]):
        primary_ip6 (Union[None, Unset, int]):
        vcpus (Union[None, Unset, float]):
        memory (Union[None, Unset, int]):
        disk (Union[None, Unset, int]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        local_context_data (Union[Unset, Any]): Local config context data takes precedence over source contexts in the
            final rendered config context
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, PatchedWritableVirtualMachineWithConfigContextRequestCustomFields]):
    """

    name: Union[Unset, str] = UNSET
    status: Union[
        Unset, PatchedWritableVirtualMachineWithConfigContextRequestStatus
    ] = UNSET
    site: Union[None, Unset, int] = UNSET
    cluster: Union[None, Unset, int] = UNSET
    device: Union[None, Unset, int] = UNSET
    role: Union[None, Unset, int] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    platform: Union[None, Unset, int] = UNSET
    primary_ip4: Union[None, Unset, int] = UNSET
    primary_ip6: Union[None, Unset, int] = UNSET
    vcpus: Union[None, Unset, float] = UNSET
    memory: Union[None, Unset, int] = UNSET
    disk: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    local_context_data: Union[Unset, Any] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[
        Unset, "PatchedWritableVirtualMachineWithConfigContextRequestCustomFields"
    ] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        site: Union[None, Unset, int]
        if isinstance(self.site, Unset):
            site = UNSET
        else:
            site = self.site

        cluster: Union[None, Unset, int]
        if isinstance(self.cluster, Unset):
            cluster = UNSET
        else:
            cluster = self.cluster

        device: Union[None, Unset, int]
        if isinstance(self.device, Unset):
            device = UNSET
        else:
            device = self.device

        role: Union[None, Unset, int]
        if isinstance(self.role, Unset):
            role = UNSET
        else:
            role = self.role

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        platform: Union[None, Unset, int]
        if isinstance(self.platform, Unset):
            platform = UNSET
        else:
            platform = self.platform

        primary_ip4: Union[None, Unset, int]
        if isinstance(self.primary_ip4, Unset):
            primary_ip4 = UNSET
        else:
            primary_ip4 = self.primary_ip4

        primary_ip6: Union[None, Unset, int]
        if isinstance(self.primary_ip6, Unset):
            primary_ip6 = UNSET
        else:
            primary_ip6 = self.primary_ip6

        vcpus: Union[None, Unset, float]
        if isinstance(self.vcpus, Unset):
            vcpus = UNSET
        else:
            vcpus = self.vcpus

        memory: Union[None, Unset, int]
        if isinstance(self.memory, Unset):
            memory = UNSET
        else:
            memory = self.memory

        disk: Union[None, Unset, int]
        if isinstance(self.disk, Unset):
            disk = UNSET
        else:
            disk = self.disk

        description = self.description

        comments = self.comments

        local_context_data = self.local_context_data

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if status is not UNSET:
            field_dict["status"] = status
        if site is not UNSET:
            field_dict["site"] = site
        if cluster is not UNSET:
            field_dict["cluster"] = cluster
        if device is not UNSET:
            field_dict["device"] = device
        if role is not UNSET:
            field_dict["role"] = role
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if platform is not UNSET:
            field_dict["platform"] = platform
        if primary_ip4 is not UNSET:
            field_dict["primary_ip4"] = primary_ip4
        if primary_ip6 is not UNSET:
            field_dict["primary_ip6"] = primary_ip6
        if vcpus is not UNSET:
            field_dict["vcpus"] = vcpus
        if memory is not UNSET:
            field_dict["memory"] = memory
        if disk is not UNSET:
            field_dict["disk"] = disk
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if local_context_data is not UNSET:
            field_dict["local_context_data"] = local_context_data
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        site: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.site, Unset):
            site = UNSET
        elif isinstance(self.site, int):
            site = (None, str(self.site).encode(), "text/plain")
        else:
            site = (None, str(self.site).encode(), "text/plain")

        cluster: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.cluster, Unset):
            cluster = UNSET
        elif isinstance(self.cluster, int):
            cluster = (None, str(self.cluster).encode(), "text/plain")
        else:
            cluster = (None, str(self.cluster).encode(), "text/plain")

        device: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.device, Unset):
            device = UNSET
        elif isinstance(self.device, int):
            device = (None, str(self.device).encode(), "text/plain")
        else:
            device = (None, str(self.device).encode(), "text/plain")

        role: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.role, Unset):
            role = UNSET
        elif isinstance(self.role, int):
            role = (None, str(self.role).encode(), "text/plain")
        else:
            role = (None, str(self.role).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        platform: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.platform, Unset):
            platform = UNSET
        elif isinstance(self.platform, int):
            platform = (None, str(self.platform).encode(), "text/plain")
        else:
            platform = (None, str(self.platform).encode(), "text/plain")

        primary_ip4: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.primary_ip4, Unset):
            primary_ip4 = UNSET
        elif isinstance(self.primary_ip4, int):
            primary_ip4 = (None, str(self.primary_ip4).encode(), "text/plain")
        else:
            primary_ip4 = (None, str(self.primary_ip4).encode(), "text/plain")

        primary_ip6: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.primary_ip6, Unset):
            primary_ip6 = UNSET
        elif isinstance(self.primary_ip6, int):
            primary_ip6 = (None, str(self.primary_ip6).encode(), "text/plain")
        else:
            primary_ip6 = (None, str(self.primary_ip6).encode(), "text/plain")

        vcpus: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.vcpus, Unset):
            vcpus = UNSET
        elif isinstance(self.vcpus, float):
            vcpus = (None, str(self.vcpus).encode(), "text/plain")
        else:
            vcpus = (None, str(self.vcpus).encode(), "text/plain")

        memory: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.memory, Unset):
            memory = UNSET
        elif isinstance(self.memory, int):
            memory = (None, str(self.memory).encode(), "text/plain")
        else:
            memory = (None, str(self.memory).encode(), "text/plain")

        disk: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.disk, Unset):
            disk = UNSET
        elif isinstance(self.disk, int):
            disk = (None, str(self.disk).encode(), "text/plain")
        else:
            disk = (None, str(self.disk).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        local_context_data = (
            self.local_context_data
            if isinstance(self.local_context_data, Unset)
            else (None, str(self.local_context_data).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if status is not UNSET:
            field_dict["status"] = status
        if site is not UNSET:
            field_dict["site"] = site
        if cluster is not UNSET:
            field_dict["cluster"] = cluster
        if device is not UNSET:
            field_dict["device"] = device
        if role is not UNSET:
            field_dict["role"] = role
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if platform is not UNSET:
            field_dict["platform"] = platform
        if primary_ip4 is not UNSET:
            field_dict["primary_ip4"] = primary_ip4
        if primary_ip6 is not UNSET:
            field_dict["primary_ip6"] = primary_ip6
        if vcpus is not UNSET:
            field_dict["vcpus"] = vcpus
        if memory is not UNSET:
            field_dict["memory"] = memory
        if disk is not UNSET:
            field_dict["disk"] = disk
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if local_context_data is not UNSET:
            field_dict["local_context_data"] = local_context_data
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_writable_virtual_machine_with_config_context_request_custom_fields import (
            PatchedWritableVirtualMachineWithConfigContextRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name", UNSET)

        _status = d.pop("status", UNSET)
        status: Union[
            Unset, PatchedWritableVirtualMachineWithConfigContextRequestStatus
        ]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = PatchedWritableVirtualMachineWithConfigContextRequestStatus(
                _status
            )

        def _parse_site(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        site = _parse_site(d.pop("site", UNSET))

        def _parse_cluster(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        cluster = _parse_cluster(d.pop("cluster", UNSET))

        def _parse_device(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        device = _parse_device(d.pop("device", UNSET))

        def _parse_role(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        role = _parse_role(d.pop("role", UNSET))

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        def _parse_platform(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        platform = _parse_platform(d.pop("platform", UNSET))

        def _parse_primary_ip4(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        primary_ip4 = _parse_primary_ip4(d.pop("primary_ip4", UNSET))

        def _parse_primary_ip6(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        primary_ip6 = _parse_primary_ip6(d.pop("primary_ip6", UNSET))

        def _parse_vcpus(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        vcpus = _parse_vcpus(d.pop("vcpus", UNSET))

        def _parse_memory(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        memory = _parse_memory(d.pop("memory", UNSET))

        def _parse_disk(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        disk = _parse_disk(d.pop("disk", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        local_context_data = d.pop("local_context_data", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[
            Unset, PatchedWritableVirtualMachineWithConfigContextRequestCustomFields
        ]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedWritableVirtualMachineWithConfigContextRequestCustomFields.from_dict(
                _custom_fields
            )

        patched_writable_virtual_machine_with_config_context_request = cls(
            name=name,
            status=status,
            site=site,
            cluster=cluster,
            device=device,
            role=role,
            tenant=tenant,
            platform=platform,
            primary_ip4=primary_ip4,
            primary_ip6=primary_ip6,
            vcpus=vcpus,
            memory=memory,
            disk=disk,
            description=description,
            comments=comments,
            local_context_data=local_context_data,
            tags=tags,
            custom_fields=custom_fields,
        )

        patched_writable_virtual_machine_with_config_context_request.additional_properties = d
        return patched_writable_virtual_machine_with_config_context_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
