from enum import IntEnum


class PatchedWritableMappingRequestClientMaxBodySize(IntEnum):
    VALUE_1 = 1
    VALUE_128 = 128
    VALUE_512 = 512
    VALUE_1024 = 1024
    VALUE_2048 = 2048

    def __str__(self) -> str:
        return str(self.value)
