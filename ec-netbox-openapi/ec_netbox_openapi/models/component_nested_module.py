from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

if TYPE_CHECKING:
    from ..models.module_nested_module_bay import ModuleNestedModuleBay


T = TypeVar("T", bound="ComponentNestedModule")


@_attrs_define
class ComponentNestedModule:
    """Used by device component serializers.

    Attributes:
        id (int):
        url (str):
        display (str):
        device (int):
        module_bay (ModuleNestedModuleBay): Represents an object related through a ForeignKey field. On write, it
            accepts a primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
    """

    id: int
    url: str
    display: str
    device: int
    module_bay: "ModuleNestedModuleBay"
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        device = self.device

        module_bay = self.module_bay.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "device": device,
                "module_bay": module_bay,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.module_nested_module_bay import ModuleNestedModuleBay

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        device = d.pop("device")

        module_bay = ModuleNestedModuleBay.from_dict(d.pop("module_bay"))

        component_nested_module = cls(
            id=id,
            url=url,
            display=display,
            device=device,
            module_bay=module_bay,
        )

        component_nested_module.additional_properties = d
        return component_nested_module

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
