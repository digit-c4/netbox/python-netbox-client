import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest


T = TypeVar("T", bound="WritableConfigTemplateRequest")


@_attrs_define
class WritableConfigTemplateRequest:
    """Introduces support for Tag assignment. Adds `tags` serialization, and handles tag assignment
    on create() and update().

        Attributes:
            name (str):
            template_code (str): Jinja2 template code.
            description (Union[Unset, str]):
            environment_params (Union[Unset, Any]): Any <a
                href="https://jinja.palletsprojects.com/en/3.1.x/api/#jinja2.Environment">additional parameters</a> to pass when
                constructing the Jinja2 environment.
            data_source (Union[None, Unset, int]): Remote data source
            data_file (Union[None, Unset, int]):
            tags (Union[Unset, List['NestedTagRequest']]):
    """

    name: str
    template_code: str
    description: Union[Unset, str] = UNSET
    environment_params: Union[Unset, Any] = UNSET
    data_source: Union[None, Unset, int] = UNSET
    data_file: Union[None, Unset, int] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        template_code = self.template_code

        description = self.description

        environment_params = self.environment_params

        data_source: Union[None, Unset, int]
        if isinstance(self.data_source, Unset):
            data_source = UNSET
        else:
            data_source = self.data_source

        data_file: Union[None, Unset, int]
        if isinstance(self.data_file, Unset):
            data_file = UNSET
        else:
            data_file = self.data_file

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "template_code": template_code,
            }
        )
        if description is not UNSET:
            field_dict["description"] = description
        if environment_params is not UNSET:
            field_dict["environment_params"] = environment_params
        if data_source is not UNSET:
            field_dict["data_source"] = data_source
        if data_file is not UNSET:
            field_dict["data_file"] = data_file
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (None, str(self.name).encode(), "text/plain")

        template_code = (None, str(self.template_code).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        environment_params = (
            self.environment_params
            if isinstance(self.environment_params, Unset)
            else (None, str(self.environment_params).encode(), "text/plain")
        )

        data_source: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.data_source, Unset):
            data_source = UNSET
        elif isinstance(self.data_source, int):
            data_source = (None, str(self.data_source).encode(), "text/plain")
        else:
            data_source = (None, str(self.data_source).encode(), "text/plain")

        data_file: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.data_file, Unset):
            data_file = UNSET
        elif isinstance(self.data_file, int):
            data_file = (None, str(self.data_file).encode(), "text/plain")
        else:
            data_file = (None, str(self.data_file).encode(), "text/plain")

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "name": name,
                "template_code": template_code,
            }
        )
        if description is not UNSET:
            field_dict["description"] = description
        if environment_params is not UNSET:
            field_dict["environment_params"] = environment_params
        if data_source is not UNSET:
            field_dict["data_source"] = data_source
        if data_file is not UNSET:
            field_dict["data_file"] = data_file
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest

        d = src_dict.copy()
        name = d.pop("name")

        template_code = d.pop("template_code")

        description = d.pop("description", UNSET)

        environment_params = d.pop("environment_params", UNSET)

        def _parse_data_source(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        data_source = _parse_data_source(d.pop("data_source", UNSET))

        def _parse_data_file(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        data_file = _parse_data_file(d.pop("data_file", UNSET))

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        writable_config_template_request = cls(
            name=name,
            template_code=template_code,
            description=description,
            environment_params=environment_params,
            data_source=data_source,
            data_file=data_file,
            tags=tags,
        )

        writable_config_template_request.additional_properties = d
        return writable_config_template_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
