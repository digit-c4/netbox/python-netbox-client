from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.image_request_custom_fields import ImageRequestCustomFields
    from ..models.nested_host_request import NestedHostRequest
    from ..models.nested_registry_request import NestedRegistryRequest
    from ..models.nested_tag_request import NestedTagRequest


T = TypeVar("T", bound="ImageRequest")


@_attrs_define
class ImageRequest:
    """Image Serializer class

    Attributes:
        host (NestedHostRequest): Nested Host Serializer class
        name (str):
        registry (NestedRegistryRequest): Nested Registry Serializer class
        version (Union[Unset, str]):
        size (Union[Unset, int]):
        image_id (Union[None, Unset, str]):
        digest (Union[None, Unset, str]):
        custom_fields (Union[Unset, ImageRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    host: "NestedHostRequest"
    name: str
    registry: "NestedRegistryRequest"
    version: Union[Unset, str] = UNSET
    size: Union[Unset, int] = UNSET
    image_id: Union[None, Unset, str] = UNSET
    digest: Union[None, Unset, str] = UNSET
    custom_fields: Union[Unset, "ImageRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        host = self.host.to_dict()

        name = self.name

        registry = self.registry.to_dict()

        version = self.version

        size = self.size

        image_id: Union[None, Unset, str]
        if isinstance(self.image_id, Unset):
            image_id = UNSET
        else:
            image_id = self.image_id

        digest: Union[None, Unset, str]
        if isinstance(self.digest, Unset):
            digest = UNSET
        else:
            digest = self.digest

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "host": host,
                "name": name,
                "registry": registry,
            }
        )
        if version is not UNSET:
            field_dict["version"] = version
        if size is not UNSET:
            field_dict["size"] = size
        if image_id is not UNSET:
            field_dict["ImageID"] = image_id
        if digest is not UNSET:
            field_dict["Digest"] = digest
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.image_request_custom_fields import ImageRequestCustomFields
        from ..models.nested_host_request import NestedHostRequest
        from ..models.nested_registry_request import NestedRegistryRequest
        from ..models.nested_tag_request import NestedTagRequest

        d = src_dict.copy()
        host = NestedHostRequest.from_dict(d.pop("host"))

        name = d.pop("name")

        registry = NestedRegistryRequest.from_dict(d.pop("registry"))

        version = d.pop("version", UNSET)

        size = d.pop("size", UNSET)

        def _parse_image_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        image_id = _parse_image_id(d.pop("ImageID", UNSET))

        def _parse_digest(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        digest = _parse_digest(d.pop("Digest", UNSET))

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ImageRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ImageRequestCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        image_request = cls(
            host=host,
            name=name,
            registry=registry,
            version=version,
            size=size,
            image_id=image_id,
            digest=digest,
            custom_fields=custom_fields,
            tags=tags,
        )

        image_request.additional_properties = d
        return image_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
