import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.image_custom_fields import ImageCustomFields
    from ..models.nested_container import NestedContainer
    from ..models.nested_host import NestedHost
    from ..models.nested_registry import NestedRegistry
    from ..models.nested_tag import NestedTag


T = TypeVar("T", bound="Image")


@_attrs_define
class Image:
    """Image Serializer class

    Attributes:
        id (int):
        url (str):
        display (str):
        host (NestedHost): Nested Host Serializer class
        name (str):
        registry (NestedRegistry): Nested Registry Serializer class
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        containers (List['NestedContainer']):
        version (Union[Unset, str]):
        size (Union[Unset, int]):
        image_id (Union[None, Unset, str]):
        digest (Union[None, Unset, str]):
        custom_fields (Union[Unset, ImageCustomFields]):
        tags (Union[Unset, List['NestedTag']]):
    """

    id: int
    url: str
    display: str
    host: "NestedHost"
    name: str
    registry: "NestedRegistry"
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    containers: List["NestedContainer"]
    version: Union[Unset, str] = UNSET
    size: Union[Unset, int] = UNSET
    image_id: Union[None, Unset, str] = UNSET
    digest: Union[None, Unset, str] = UNSET
    custom_fields: Union[Unset, "ImageCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        host = self.host.to_dict()

        name = self.name

        registry = self.registry.to_dict()

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        containers = []
        for containers_item_data in self.containers:
            containers_item = containers_item_data.to_dict()
            containers.append(containers_item)

        version = self.version

        size = self.size

        image_id: Union[None, Unset, str]
        if isinstance(self.image_id, Unset):
            image_id = UNSET
        else:
            image_id = self.image_id

        digest: Union[None, Unset, str]
        if isinstance(self.digest, Unset):
            digest = UNSET
        else:
            digest = self.digest

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "host": host,
                "name": name,
                "registry": registry,
                "created": created,
                "last_updated": last_updated,
                "containers": containers,
            }
        )
        if version is not UNSET:
            field_dict["version"] = version
        if size is not UNSET:
            field_dict["size"] = size
        if image_id is not UNSET:
            field_dict["ImageID"] = image_id
        if digest is not UNSET:
            field_dict["Digest"] = digest
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.image_custom_fields import ImageCustomFields
        from ..models.nested_container import NestedContainer
        from ..models.nested_host import NestedHost
        from ..models.nested_registry import NestedRegistry
        from ..models.nested_tag import NestedTag

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        host = NestedHost.from_dict(d.pop("host"))

        name = d.pop("name")

        registry = NestedRegistry.from_dict(d.pop("registry"))

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        containers = []
        _containers = d.pop("containers")
        for containers_item_data in _containers:
            containers_item = NestedContainer.from_dict(containers_item_data)

            containers.append(containers_item)

        version = d.pop("version", UNSET)

        size = d.pop("size", UNSET)

        def _parse_image_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        image_id = _parse_image_id(d.pop("ImageID", UNSET))

        def _parse_digest(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        digest = _parse_digest(d.pop("Digest", UNSET))

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ImageCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ImageCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        image = cls(
            id=id,
            url=url,
            display=display,
            host=host,
            name=name,
            registry=registry,
            created=created,
            last_updated=last_updated,
            containers=containers,
            version=version,
            size=size,
            image_id=image_id,
            digest=digest,
            custom_fields=custom_fields,
            tags=tags,
        )

        image.additional_properties = d
        return image

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
