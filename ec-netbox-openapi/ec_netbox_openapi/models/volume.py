import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.volume_driver import VolumeDriver
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_host import NestedHost
    from ..models.nested_mount import NestedMount
    from ..models.nested_tag import NestedTag
    from ..models.volume_custom_fields import VolumeCustomFields


T = TypeVar("T", bound="Volume")


@_attrs_define
class Volume:
    """Volume Serializer class

    Attributes:
        id (int):
        url (str):
        display (str):
        host (NestedHost): Nested Host Serializer class
        name (str):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        mounts (List['NestedMount']):
        driver (Union[Unset, VolumeDriver]): * `local` - local
        custom_fields (Union[Unset, VolumeCustomFields]):
        tags (Union[Unset, List['NestedTag']]):
    """

    id: int
    url: str
    display: str
    host: "NestedHost"
    name: str
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    mounts: List["NestedMount"]
    driver: Union[Unset, VolumeDriver] = UNSET
    custom_fields: Union[Unset, "VolumeCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        host = self.host.to_dict()

        name = self.name

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        mounts = []
        for mounts_item_data in self.mounts:
            mounts_item = mounts_item_data.to_dict()
            mounts.append(mounts_item)

        driver: Union[Unset, str] = UNSET
        if not isinstance(self.driver, Unset):
            driver = self.driver.value

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "host": host,
                "name": name,
                "created": created,
                "last_updated": last_updated,
                "mounts": mounts,
            }
        )
        if driver is not UNSET:
            field_dict["driver"] = driver
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_host import NestedHost
        from ..models.nested_mount import NestedMount
        from ..models.nested_tag import NestedTag
        from ..models.volume_custom_fields import VolumeCustomFields

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        host = NestedHost.from_dict(d.pop("host"))

        name = d.pop("name")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        mounts = []
        _mounts = d.pop("mounts")
        for mounts_item_data in _mounts:
            mounts_item = NestedMount.from_dict(mounts_item_data)

            mounts.append(mounts_item)

        _driver = d.pop("driver", UNSET)
        driver: Union[Unset, VolumeDriver]
        if isinstance(_driver, Unset):
            driver = UNSET
        else:
            driver = VolumeDriver(_driver)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, VolumeCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = VolumeCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        volume = cls(
            id=id,
            url=url,
            display=display,
            host=host,
            name=name,
            created=created,
            last_updated=last_updated,
            mounts=mounts,
            driver=driver,
            custom_fields=custom_fields,
            tags=tags,
        )

        volume.additional_properties = d
        return volume

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
