import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

if TYPE_CHECKING:
    from ..models.nested_mapping import NestedMapping


T = TypeVar("T", bound="AclDeniedSource")


@_attrs_define
class AclDeniedSource:
    """AclDeniedSource Serializer class

    Attributes:
        id (int):
        url (str):
        mapping (NestedMapping): Nested Mapping Serializer class
        acl_source (str):
        last_updated (datetime.datetime):
        created (datetime.datetime):
    """

    id: int
    url: str
    mapping: "NestedMapping"
    acl_source: str
    last_updated: datetime.datetime
    created: datetime.datetime
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        mapping = self.mapping.to_dict()

        acl_source = self.acl_source

        last_updated = self.last_updated.isoformat()

        created = self.created.isoformat()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "mapping": mapping,
                "acl_source": acl_source,
                "last_updated": last_updated,
                "created": created,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_mapping import NestedMapping

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        mapping = NestedMapping.from_dict(d.pop("mapping"))

        acl_source = d.pop("acl_source")

        last_updated = isoparse(d.pop("last_updated"))

        created = isoparse(d.pop("created"))

        acl_denied_source = cls(
            id=id,
            url=url,
            mapping=mapping,
            acl_source=acl_source,
            last_updated=last_updated,
            created=created,
        )

        acl_denied_source.additional_properties = d
        return acl_denied_source

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
