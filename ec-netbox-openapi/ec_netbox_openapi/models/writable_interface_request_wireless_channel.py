from enum import Enum


class WritableInterfaceRequestWirelessChannel(str, Enum):
    VALUE_0 = "2.4g-1-2412-22"
    VALUE_1 = "2.4g-2-2417-22"
    VALUE_10 = "2.4g-11-2462-22"
    VALUE_100 = "6g-69-6295-20"
    VALUE_101 = "6g-71-6305-80"
    VALUE_102 = "6g-73-6315-20"
    VALUE_103 = "6g-75-6325-40"
    VALUE_104 = "6g-77-6335-20"
    VALUE_105 = "6g-79-6345-160"
    VALUE_106 = "6g-81-6355-20"
    VALUE_107 = "6g-83-6365-40"
    VALUE_108 = "6g-85-6375-20"
    VALUE_109 = "6g-87-6385-80"
    VALUE_11 = "2.4g-12-2467-22"
    VALUE_110 = "6g-89-6395-20"
    VALUE_111 = "6g-91-6405-40"
    VALUE_112 = "6g-93-6415-20"
    VALUE_113 = "6g-95-6425-320"
    VALUE_114 = "6g-97-6435-20"
    VALUE_115 = "6g-99-6445-40"
    VALUE_116 = "6g-101-6455-20"
    VALUE_117 = "6g-103-6465-80"
    VALUE_118 = "6g-105-6475-20"
    VALUE_119 = "6g-107-6485-40"
    VALUE_12 = "2.4g-13-2472-22"
    VALUE_120 = "6g-109-6495-20"
    VALUE_121 = "6g-111-6505-160"
    VALUE_122 = "6g-113-6515-20"
    VALUE_123 = "6g-115-6525-40"
    VALUE_124 = "6g-117-6535-20"
    VALUE_125 = "6g-119-6545-80"
    VALUE_126 = "6g-121-6555-20"
    VALUE_127 = "6g-123-6565-40"
    VALUE_128 = "6g-125-6575-20"
    VALUE_129 = "6g-129-6595-20"
    VALUE_13 = "5g-32-5160-20"
    VALUE_130 = "6g-131-6605-40"
    VALUE_131 = "6g-133-6615-20"
    VALUE_132 = "6g-135-6625-80"
    VALUE_133 = "6g-137-6635-20"
    VALUE_134 = "6g-139-6645-40"
    VALUE_135 = "6g-141-6655-20"
    VALUE_136 = "6g-143-6665-160"
    VALUE_137 = "6g-145-6675-20"
    VALUE_138 = "6g-147-6685-40"
    VALUE_139 = "6g-149-6695-20"
    VALUE_14 = "5g-34-5170-40"
    VALUE_140 = "6g-151-6705-80"
    VALUE_141 = "6g-153-6715-20"
    VALUE_142 = "6g-155-6725-40"
    VALUE_143 = "6g-157-6735-20"
    VALUE_144 = "6g-159-6745-320"
    VALUE_145 = "6g-161-6755-20"
    VALUE_146 = "6g-163-6765-40"
    VALUE_147 = "6g-165-6775-20"
    VALUE_148 = "6g-167-6785-80"
    VALUE_149 = "6g-169-6795-20"
    VALUE_15 = "5g-36-5180-20"
    VALUE_150 = "6g-171-6805-40"
    VALUE_151 = "6g-173-6815-20"
    VALUE_152 = "6g-175-6825-160"
    VALUE_153 = "6g-177-6835-20"
    VALUE_154 = "6g-179-6845-40"
    VALUE_155 = "6g-181-6855-20"
    VALUE_156 = "6g-183-6865-80"
    VALUE_157 = "6g-185-6875-20"
    VALUE_158 = "6g-187-6885-40"
    VALUE_159 = "6g-189-6895-20"
    VALUE_16 = "5g-38-5190-40"
    VALUE_160 = "6g-193-6915-20"
    VALUE_161 = "6g-195-6925-40"
    VALUE_162 = "6g-197-6935-20"
    VALUE_163 = "6g-199-6945-80"
    VALUE_164 = "6g-201-6955-20"
    VALUE_165 = "6g-203-6965-40"
    VALUE_166 = "6g-205-6975-20"
    VALUE_167 = "6g-207-6985-160"
    VALUE_168 = "6g-209-6995-20"
    VALUE_169 = "6g-211-7005-40"
    VALUE_17 = "5g-40-5200-20"
    VALUE_170 = "6g-213-7015-20"
    VALUE_171 = "6g-215-7025-80"
    VALUE_172 = "6g-217-7035-20"
    VALUE_173 = "6g-219-7045-40"
    VALUE_174 = "6g-221-7055-20"
    VALUE_175 = "6g-225-7075-20"
    VALUE_176 = "6g-227-7085-40"
    VALUE_177 = "6g-229-7095-20"
    VALUE_178 = "6g-233-7115-20"
    VALUE_179 = "60g-1-58320-2160"
    VALUE_18 = "5g-42-5210-80"
    VALUE_180 = "60g-2-60480-2160"
    VALUE_181 = "60g-3-62640-2160"
    VALUE_182 = "60g-4-64800-2160"
    VALUE_183 = "60g-5-66960-2160"
    VALUE_184 = "60g-6-69120-2160"
    VALUE_185 = "60g-9-59400-4320"
    VALUE_186 = "60g-10-61560-4320"
    VALUE_187 = "60g-11-63720-4320"
    VALUE_188 = "60g-12-65880-4320"
    VALUE_189 = "60g-13-68040-4320"
    VALUE_19 = "5g-44-5220-20"
    VALUE_190 = "60g-17-60480-6480"
    VALUE_191 = "60g-18-62640-6480"
    VALUE_192 = "60g-19-64800-6480"
    VALUE_193 = "60g-20-66960-6480"
    VALUE_194 = "60g-25-61560-6480"
    VALUE_195 = "60g-26-63720-6480"
    VALUE_196 = "60g-27-65880-6480"
    VALUE_197 = ""
    VALUE_2 = "2.4g-3-2422-22"
    VALUE_20 = "5g-46-5230-40"
    VALUE_21 = "5g-48-5240-20"
    VALUE_22 = "5g-50-5250-160"
    VALUE_23 = "5g-52-5260-20"
    VALUE_24 = "5g-54-5270-40"
    VALUE_25 = "5g-56-5280-20"
    VALUE_26 = "5g-58-5290-80"
    VALUE_27 = "5g-60-5300-20"
    VALUE_28 = "5g-62-5310-40"
    VALUE_29 = "5g-64-5320-20"
    VALUE_3 = "2.4g-4-2427-22"
    VALUE_30 = "5g-100-5500-20"
    VALUE_31 = "5g-102-5510-40"
    VALUE_32 = "5g-104-5520-20"
    VALUE_33 = "5g-106-5530-80"
    VALUE_34 = "5g-108-5540-20"
    VALUE_35 = "5g-110-5550-40"
    VALUE_36 = "5g-112-5560-20"
    VALUE_37 = "5g-114-5570-160"
    VALUE_38 = "5g-116-5580-20"
    VALUE_39 = "5g-118-5590-40"
    VALUE_4 = "2.4g-5-2432-22"
    VALUE_40 = "5g-120-5600-20"
    VALUE_41 = "5g-122-5610-80"
    VALUE_42 = "5g-124-5620-20"
    VALUE_43 = "5g-126-5630-40"
    VALUE_44 = "5g-128-5640-20"
    VALUE_45 = "5g-132-5660-20"
    VALUE_46 = "5g-134-5670-40"
    VALUE_47 = "5g-136-5680-20"
    VALUE_48 = "5g-138-5690-80"
    VALUE_49 = "5g-140-5700-20"
    VALUE_5 = "2.4g-6-2437-22"
    VALUE_50 = "5g-142-5710-40"
    VALUE_51 = "5g-144-5720-20"
    VALUE_52 = "5g-149-5745-20"
    VALUE_53 = "5g-151-5755-40"
    VALUE_54 = "5g-153-5765-20"
    VALUE_55 = "5g-155-5775-80"
    VALUE_56 = "5g-157-5785-20"
    VALUE_57 = "5g-159-5795-40"
    VALUE_58 = "5g-161-5805-20"
    VALUE_59 = "5g-163-5815-160"
    VALUE_6 = "2.4g-7-2442-22"
    VALUE_60 = "5g-165-5825-20"
    VALUE_61 = "5g-167-5835-40"
    VALUE_62 = "5g-169-5845-20"
    VALUE_63 = "5g-171-5855-80"
    VALUE_64 = "5g-173-5865-20"
    VALUE_65 = "5g-175-5875-40"
    VALUE_66 = "5g-177-5885-20"
    VALUE_67 = "6g-1-5955-20"
    VALUE_68 = "6g-3-5965-40"
    VALUE_69 = "6g-5-5975-20"
    VALUE_7 = "2.4g-8-2447-22"
    VALUE_70 = "6g-7-5985-80"
    VALUE_71 = "6g-9-5995-20"
    VALUE_72 = "6g-11-6005-40"
    VALUE_73 = "6g-13-6015-20"
    VALUE_74 = "6g-15-6025-160"
    VALUE_75 = "6g-17-6035-20"
    VALUE_76 = "6g-19-6045-40"
    VALUE_77 = "6g-21-6055-20"
    VALUE_78 = "6g-23-6065-80"
    VALUE_79 = "6g-25-6075-20"
    VALUE_8 = "2.4g-9-2452-22"
    VALUE_80 = "6g-27-6085-40"
    VALUE_81 = "6g-29-6095-20"
    VALUE_82 = "6g-31-6105-320"
    VALUE_83 = "6g-33-6115-20"
    VALUE_84 = "6g-35-6125-40"
    VALUE_85 = "6g-37-6135-20"
    VALUE_86 = "6g-39-6145-80"
    VALUE_87 = "6g-41-6155-20"
    VALUE_88 = "6g-43-6165-40"
    VALUE_89 = "6g-45-6175-20"
    VALUE_9 = "2.4g-10-2457-22"
    VALUE_90 = "6g-47-6185-160"
    VALUE_91 = "6g-49-6195-20"
    VALUE_92 = "6g-51-6205-40"
    VALUE_93 = "6g-53-6215-20"
    VALUE_94 = "6g-55-6225-80"
    VALUE_95 = "6g-57-6235-20"
    VALUE_96 = "6g-59-6245-40"
    VALUE_97 = "6g-61-6255-20"
    VALUE_98 = "6g-65-6275-20"
    VALUE_99 = "6g-67-6285-40"

    def __str__(self) -> str:
        return str(self.value)
