from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.location_request_status import LocationRequestStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.location_request_custom_fields import LocationRequestCustomFields
    from ..models.nested_location_request import NestedLocationRequest
    from ..models.nested_site_request import NestedSiteRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_tenant_request import NestedTenantRequest


T = TypeVar("T", bound="LocationRequest")


@_attrs_define
class LocationRequest:
    """Extends PrimaryModelSerializer to include MPTT support.

    Attributes:
        name (str):
        slug (str):
        site (NestedSiteRequest): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        parent (Union['NestedLocationRequest', None, Unset]):
        status (Union[Unset, LocationRequestStatus]): * `planned` - Planned
            * `staging` - Staging
            * `active` - Active
            * `decommissioning` - Decommissioning
            * `retired` - Retired
        tenant (Union['NestedTenantRequest', None, Unset]):
        description (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, LocationRequestCustomFields]):
    """

    name: str
    slug: str
    site: "NestedSiteRequest"
    parent: Union["NestedLocationRequest", None, Unset] = UNSET
    status: Union[Unset, LocationRequestStatus] = UNSET
    tenant: Union["NestedTenantRequest", None, Unset] = UNSET
    description: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "LocationRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_location_request import NestedLocationRequest
        from ..models.nested_tenant_request import NestedTenantRequest

        name = self.name

        slug = self.slug

        site = self.site.to_dict()

        parent: Union[Dict[str, Any], None, Unset]
        if isinstance(self.parent, Unset):
            parent = UNSET
        elif isinstance(self.parent, NestedLocationRequest):
            parent = self.parent.to_dict()
        else:
            parent = self.parent

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenantRequest):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        description = self.description

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "slug": slug,
                "site": site,
            }
        )
        if parent is not UNSET:
            field_dict["parent"] = parent
        if status is not UNSET:
            field_dict["status"] = status
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.location_request_custom_fields import LocationRequestCustomFields
        from ..models.nested_location_request import NestedLocationRequest
        from ..models.nested_site_request import NestedSiteRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_tenant_request import NestedTenantRequest

        d = src_dict.copy()
        name = d.pop("name")

        slug = d.pop("slug")

        site = NestedSiteRequest.from_dict(d.pop("site"))

        def _parse_parent(data: object) -> Union["NestedLocationRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                parent_type_1 = NestedLocationRequest.from_dict(data)

                return parent_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedLocationRequest", None, Unset], data)

        parent = _parse_parent(d.pop("parent", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, LocationRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = LocationRequestStatus(_status)

        def _parse_tenant(data: object) -> Union["NestedTenantRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenantRequest.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenantRequest", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        description = d.pop("description", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, LocationRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = LocationRequestCustomFields.from_dict(_custom_fields)

        location_request = cls(
            name=name,
            slug=slug,
            site=site,
            parent=parent,
            status=status,
            tenant=tenant,
            description=description,
            tags=tags,
            custom_fields=custom_fields,
        )

        location_request.additional_properties = d
        return location_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
