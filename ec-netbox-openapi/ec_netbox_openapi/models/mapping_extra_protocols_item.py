from enum import Enum


class MappingExtraProtocolsItem(str, Enum):
    WEBSOCKET = "websocket"

    def __str__(self) -> str:
        return str(self.value)
