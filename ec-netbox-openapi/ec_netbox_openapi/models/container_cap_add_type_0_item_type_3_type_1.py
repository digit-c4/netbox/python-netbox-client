from enum import Enum


class ContainerCapAddType0ItemType3Type1(str, Enum):
    NET_ADMIN = "NET_ADMIN"
    VALUE_1 = ""

    def __str__(self) -> str:
        return str(self.value)
