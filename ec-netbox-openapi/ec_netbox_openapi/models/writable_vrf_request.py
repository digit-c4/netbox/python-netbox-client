import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_vrf_request_custom_fields import (
        WritableVRFRequestCustomFields,
    )


T = TypeVar("T", bound="WritableVRFRequest")


@_attrs_define
class WritableVRFRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        rd (Union[None, Unset, str]): Unique route distinguisher (as defined in RFC 4364)
        tenant (Union[None, Unset, int]):
        enforce_unique (Union[Unset, bool]): Prevent duplicate prefixes/IP addresses within this VRF
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        import_targets (Union[Unset, List[int]]):
        export_targets (Union[Unset, List[int]]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritableVRFRequestCustomFields]):
    """

    name: str
    rd: Union[None, Unset, str] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    enforce_unique: Union[Unset, bool] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    import_targets: Union[Unset, List[int]] = UNSET
    export_targets: Union[Unset, List[int]] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WritableVRFRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        rd: Union[None, Unset, str]
        if isinstance(self.rd, Unset):
            rd = UNSET
        else:
            rd = self.rd

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        enforce_unique = self.enforce_unique

        description = self.description

        comments = self.comments

        import_targets: Union[Unset, List[int]] = UNSET
        if not isinstance(self.import_targets, Unset):
            import_targets = self.import_targets

        export_targets: Union[Unset, List[int]] = UNSET
        if not isinstance(self.export_targets, Unset):
            export_targets = self.export_targets

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
            }
        )
        if rd is not UNSET:
            field_dict["rd"] = rd
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if enforce_unique is not UNSET:
            field_dict["enforce_unique"] = enforce_unique
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if import_targets is not UNSET:
            field_dict["import_targets"] = import_targets
        if export_targets is not UNSET:
            field_dict["export_targets"] = export_targets
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (None, str(self.name).encode(), "text/plain")

        rd: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.rd, Unset):
            rd = UNSET
        elif isinstance(self.rd, str):
            rd = (None, str(self.rd).encode(), "text/plain")
        else:
            rd = (None, str(self.rd).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        enforce_unique = (
            self.enforce_unique
            if isinstance(self.enforce_unique, Unset)
            else (None, str(self.enforce_unique).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        import_targets: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.import_targets, Unset):
            _temp_import_targets = self.import_targets
            import_targets = (
                None,
                json.dumps(_temp_import_targets).encode(),
                "application/json",
            )

        export_targets: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.export_targets, Unset):
            _temp_export_targets = self.export_targets
            export_targets = (
                None,
                json.dumps(_temp_export_targets).encode(),
                "application/json",
            )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "name": name,
            }
        )
        if rd is not UNSET:
            field_dict["rd"] = rd
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if enforce_unique is not UNSET:
            field_dict["enforce_unique"] = enforce_unique
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if import_targets is not UNSET:
            field_dict["import_targets"] = import_targets
        if export_targets is not UNSET:
            field_dict["export_targets"] = export_targets
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_vrf_request_custom_fields import (
            WritableVRFRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name")

        def _parse_rd(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        rd = _parse_rd(d.pop("rd", UNSET))

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        enforce_unique = d.pop("enforce_unique", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        import_targets = cast(List[int], d.pop("import_targets", UNSET))

        export_targets = cast(List[int], d.pop("export_targets", UNSET))

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableVRFRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableVRFRequestCustomFields.from_dict(_custom_fields)

        writable_vrf_request = cls(
            name=name,
            rd=rd,
            tenant=tenant,
            enforce_unique=enforce_unique,
            description=description,
            comments=comments,
            import_targets=import_targets,
            export_targets=export_targets,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_vrf_request.additional_properties = d
        return writable_vrf_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
