from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.contact_assignment_request_priority import (
    ContactAssignmentRequestPriority,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_contact_request import NestedContactRequest
    from ..models.nested_contact_role_request import NestedContactRoleRequest
    from ..models.nested_tag_request import NestedTagRequest


T = TypeVar("T", bound="ContactAssignmentRequest")


@_attrs_define
class ContactAssignmentRequest:
    """Adds support for custom fields and tags.

    Attributes:
        content_type (str):
        object_id (int):
        contact (NestedContactRequest): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        role (Union['NestedContactRoleRequest', None, Unset]):
        priority (Union[Unset, ContactAssignmentRequestPriority]): * `primary` - Primary
            * `secondary` - Secondary
            * `tertiary` - Tertiary
            * `inactive` - Inactive
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    content_type: str
    object_id: int
    contact: "NestedContactRequest"
    role: Union["NestedContactRoleRequest", None, Unset] = UNSET
    priority: Union[Unset, ContactAssignmentRequestPriority] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_contact_role_request import NestedContactRoleRequest

        content_type = self.content_type

        object_id = self.object_id

        contact = self.contact.to_dict()

        role: Union[Dict[str, Any], None, Unset]
        if isinstance(self.role, Unset):
            role = UNSET
        elif isinstance(self.role, NestedContactRoleRequest):
            role = self.role.to_dict()
        else:
            role = self.role

        priority: Union[Unset, str] = UNSET
        if not isinstance(self.priority, Unset):
            priority = self.priority.value

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "content_type": content_type,
                "object_id": object_id,
                "contact": contact,
            }
        )
        if role is not UNSET:
            field_dict["role"] = role
        if priority is not UNSET:
            field_dict["priority"] = priority
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_contact_request import NestedContactRequest
        from ..models.nested_contact_role_request import NestedContactRoleRequest
        from ..models.nested_tag_request import NestedTagRequest

        d = src_dict.copy()
        content_type = d.pop("content_type")

        object_id = d.pop("object_id")

        contact = NestedContactRequest.from_dict(d.pop("contact"))

        def _parse_role(data: object) -> Union["NestedContactRoleRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                role_type_1 = NestedContactRoleRequest.from_dict(data)

                return role_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedContactRoleRequest", None, Unset], data)

        role = _parse_role(d.pop("role", UNSET))

        _priority = d.pop("priority", UNSET)
        priority: Union[Unset, ContactAssignmentRequestPriority]
        if isinstance(_priority, Unset):
            priority = UNSET
        else:
            priority = ContactAssignmentRequestPriority(_priority)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        contact_assignment_request = cls(
            content_type=content_type,
            object_id=object_id,
            contact=contact,
            role=role,
            priority=priority,
            tags=tags,
        )

        contact_assignment_request.additional_properties = d
        return contact_assignment_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
