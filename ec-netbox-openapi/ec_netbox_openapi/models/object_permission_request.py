from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="ObjectPermissionRequest")


@_attrs_define
class ObjectPermissionRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            name (str):
            object_types (List[str]):
            actions (List[str]): The list of actions granted by this permission
            description (Union[Unset, str]):
            enabled (Union[Unset, bool]):
            groups (Union[Unset, List[int]]):
            users (Union[Unset, List[int]]):
            constraints (Union[Unset, Any]): Queryset filter matching the applicable objects of the selected type(s)
    """

    name: str
    object_types: List[str]
    actions: List[str]
    description: Union[Unset, str] = UNSET
    enabled: Union[Unset, bool] = UNSET
    groups: Union[Unset, List[int]] = UNSET
    users: Union[Unset, List[int]] = UNSET
    constraints: Union[Unset, Any] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        object_types = self.object_types

        actions = self.actions

        description = self.description

        enabled = self.enabled

        groups: Union[Unset, List[int]] = UNSET
        if not isinstance(self.groups, Unset):
            groups = self.groups

        users: Union[Unset, List[int]] = UNSET
        if not isinstance(self.users, Unset):
            users = self.users

        constraints = self.constraints

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "object_types": object_types,
                "actions": actions,
            }
        )
        if description is not UNSET:
            field_dict["description"] = description
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if groups is not UNSET:
            field_dict["groups"] = groups
        if users is not UNSET:
            field_dict["users"] = users
        if constraints is not UNSET:
            field_dict["constraints"] = constraints

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name")

        object_types = cast(List[str], d.pop("object_types"))

        actions = cast(List[str], d.pop("actions"))

        description = d.pop("description", UNSET)

        enabled = d.pop("enabled", UNSET)

        groups = cast(List[int], d.pop("groups", UNSET))

        users = cast(List[int], d.pop("users", UNSET))

        constraints = d.pop("constraints", UNSET)

        object_permission_request = cls(
            name=name,
            object_types=object_types,
            actions=actions,
            description=description,
            enabled=enabled,
            groups=groups,
            users=users,
            constraints=constraints,
        )

        object_permission_request.additional_properties = d
        return object_permission_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
