from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.nested_http_header_request_apply_to import NestedHttpHeaderRequestApplyTo
from ..types import UNSET, Unset

T = TypeVar("T", bound="NestedHttpHeaderRequest")


@_attrs_define
class NestedHttpHeaderRequest:
    """Nested HTTP Header Serializer class

    Attributes:
        name (str):
        value (Union[None, Unset, str]):
        apply_to (Union[Unset, NestedHttpHeaderRequestApplyTo]): * `request` - Request
            * `response` - Response
    """

    name: str
    value: Union[None, Unset, str] = UNSET
    apply_to: Union[Unset, NestedHttpHeaderRequestApplyTo] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        value: Union[None, Unset, str]
        if isinstance(self.value, Unset):
            value = UNSET
        else:
            value = self.value

        apply_to: Union[Unset, str] = UNSET
        if not isinstance(self.apply_to, Unset):
            apply_to = self.apply_to.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
            }
        )
        if value is not UNSET:
            field_dict["value"] = value
        if apply_to is not UNSET:
            field_dict["apply_to"] = apply_to

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name")

        def _parse_value(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        value = _parse_value(d.pop("value", UNSET))

        _apply_to = d.pop("apply_to", UNSET)
        apply_to: Union[Unset, NestedHttpHeaderRequestApplyTo]
        if isinstance(_apply_to, Unset):
            apply_to = UNSET
        else:
            apply_to = NestedHttpHeaderRequestApplyTo(_apply_to)

        nested_http_header_request = cls(
            name=name,
            value=value,
            apply_to=apply_to,
        )

        nested_http_header_request.additional_properties = d
        return nested_http_header_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
