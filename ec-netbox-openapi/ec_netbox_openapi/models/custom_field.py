import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.custom_field_filter_logic import CustomFieldFilterLogic
    from ..models.custom_field_type import CustomFieldType
    from ..models.custom_field_ui_visibility import CustomFieldUiVisibility
    from ..models.nested_custom_field_choice_set import NestedCustomFieldChoiceSet


T = TypeVar("T", bound="CustomField")


@_attrs_define
class CustomField:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            id (int):
            url (str):
            display (str):
            content_types (List[str]):
            type (CustomFieldType):
            data_type (str):
            name (str): Internal field name
            created (Union[None, datetime.datetime]):
            last_updated (Union[None, datetime.datetime]):
            object_type (Union[Unset, str]):
            label (Union[Unset, str]): Name of the field as displayed to users (if not provided, 'the field's name will be
                used)
            group_name (Union[Unset, str]): Custom fields within the same group will be displayed together
            description (Union[Unset, str]):
            required (Union[Unset, bool]): If true, this field is required when creating new objects or editing an existing
                object.
            search_weight (Union[Unset, int]): Weighting for search. Lower values are considered more important. Fields with
                a search weight of zero will be ignored.
            filter_logic (Union[Unset, CustomFieldFilterLogic]):
            ui_visibility (Union[Unset, CustomFieldUiVisibility]):
            is_cloneable (Union[Unset, bool]): Replicate this value when cloning objects
            default (Union[Unset, Any]): Default value for the field (must be a JSON value). Encapsulate strings with double
                quotes (e.g. "Foo").
            weight (Union[Unset, int]): Fields with higher weights appear lower in a form.
            validation_minimum (Union[None, Unset, int]): Minimum allowed value (for numeric fields)
            validation_maximum (Union[None, Unset, int]): Maximum allowed value (for numeric fields)
            validation_regex (Union[Unset, str]): Regular expression to enforce on text field values. Use ^ and $ to force
                matching of entire string. For example, <code>^[A-Z]{3}$</code> will limit values to exactly three uppercase
                letters.
            choice_set (Union[Unset, NestedCustomFieldChoiceSet]): Represents an object related through a ForeignKey field.
                On write, it accepts a primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
    """

    id: int
    url: str
    display: str
    content_types: List[str]
    type: "CustomFieldType"
    data_type: str
    name: str
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    object_type: Union[Unset, str] = UNSET
    label: Union[Unset, str] = UNSET
    group_name: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    required: Union[Unset, bool] = UNSET
    search_weight: Union[Unset, int] = UNSET
    filter_logic: Union[Unset, "CustomFieldFilterLogic"] = UNSET
    ui_visibility: Union[Unset, "CustomFieldUiVisibility"] = UNSET
    is_cloneable: Union[Unset, bool] = UNSET
    default: Union[Unset, Any] = UNSET
    weight: Union[Unset, int] = UNSET
    validation_minimum: Union[None, Unset, int] = UNSET
    validation_maximum: Union[None, Unset, int] = UNSET
    validation_regex: Union[Unset, str] = UNSET
    choice_set: Union[Unset, "NestedCustomFieldChoiceSet"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        content_types = self.content_types

        type = self.type.to_dict()

        data_type = self.data_type

        name = self.name

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        object_type = self.object_type

        label = self.label

        group_name = self.group_name

        description = self.description

        required = self.required

        search_weight = self.search_weight

        filter_logic: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.filter_logic, Unset):
            filter_logic = self.filter_logic.to_dict()

        ui_visibility: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.ui_visibility, Unset):
            ui_visibility = self.ui_visibility.to_dict()

        is_cloneable = self.is_cloneable

        default = self.default

        weight = self.weight

        validation_minimum: Union[None, Unset, int]
        if isinstance(self.validation_minimum, Unset):
            validation_minimum = UNSET
        else:
            validation_minimum = self.validation_minimum

        validation_maximum: Union[None, Unset, int]
        if isinstance(self.validation_maximum, Unset):
            validation_maximum = UNSET
        else:
            validation_maximum = self.validation_maximum

        validation_regex = self.validation_regex

        choice_set: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.choice_set, Unset):
            choice_set = self.choice_set.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "content_types": content_types,
                "type": type,
                "data_type": data_type,
                "name": name,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if object_type is not UNSET:
            field_dict["object_type"] = object_type
        if label is not UNSET:
            field_dict["label"] = label
        if group_name is not UNSET:
            field_dict["group_name"] = group_name
        if description is not UNSET:
            field_dict["description"] = description
        if required is not UNSET:
            field_dict["required"] = required
        if search_weight is not UNSET:
            field_dict["search_weight"] = search_weight
        if filter_logic is not UNSET:
            field_dict["filter_logic"] = filter_logic
        if ui_visibility is not UNSET:
            field_dict["ui_visibility"] = ui_visibility
        if is_cloneable is not UNSET:
            field_dict["is_cloneable"] = is_cloneable
        if default is not UNSET:
            field_dict["default"] = default
        if weight is not UNSET:
            field_dict["weight"] = weight
        if validation_minimum is not UNSET:
            field_dict["validation_minimum"] = validation_minimum
        if validation_maximum is not UNSET:
            field_dict["validation_maximum"] = validation_maximum
        if validation_regex is not UNSET:
            field_dict["validation_regex"] = validation_regex
        if choice_set is not UNSET:
            field_dict["choice_set"] = choice_set

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.custom_field_filter_logic import CustomFieldFilterLogic
        from ..models.custom_field_type import CustomFieldType
        from ..models.custom_field_ui_visibility import CustomFieldUiVisibility
        from ..models.nested_custom_field_choice_set import NestedCustomFieldChoiceSet

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        content_types = cast(List[str], d.pop("content_types"))

        type = CustomFieldType.from_dict(d.pop("type"))

        data_type = d.pop("data_type")

        name = d.pop("name")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        object_type = d.pop("object_type", UNSET)

        label = d.pop("label", UNSET)

        group_name = d.pop("group_name", UNSET)

        description = d.pop("description", UNSET)

        required = d.pop("required", UNSET)

        search_weight = d.pop("search_weight", UNSET)

        _filter_logic = d.pop("filter_logic", UNSET)
        filter_logic: Union[Unset, CustomFieldFilterLogic]
        if isinstance(_filter_logic, Unset):
            filter_logic = UNSET
        else:
            filter_logic = CustomFieldFilterLogic.from_dict(_filter_logic)

        _ui_visibility = d.pop("ui_visibility", UNSET)
        ui_visibility: Union[Unset, CustomFieldUiVisibility]
        if isinstance(_ui_visibility, Unset):
            ui_visibility = UNSET
        else:
            ui_visibility = CustomFieldUiVisibility.from_dict(_ui_visibility)

        is_cloneable = d.pop("is_cloneable", UNSET)

        default = d.pop("default", UNSET)

        weight = d.pop("weight", UNSET)

        def _parse_validation_minimum(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        validation_minimum = _parse_validation_minimum(
            d.pop("validation_minimum", UNSET)
        )

        def _parse_validation_maximum(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        validation_maximum = _parse_validation_maximum(
            d.pop("validation_maximum", UNSET)
        )

        validation_regex = d.pop("validation_regex", UNSET)

        _choice_set = d.pop("choice_set", UNSET)
        choice_set: Union[Unset, NestedCustomFieldChoiceSet]
        if isinstance(_choice_set, Unset):
            choice_set = UNSET
        else:
            choice_set = NestedCustomFieldChoiceSet.from_dict(_choice_set)

        custom_field = cls(
            id=id,
            url=url,
            display=display,
            content_types=content_types,
            type=type,
            data_type=data_type,
            name=name,
            created=created,
            last_updated=last_updated,
            object_type=object_type,
            label=label,
            group_name=group_name,
            description=description,
            required=required,
            search_weight=search_weight,
            filter_logic=filter_logic,
            ui_visibility=ui_visibility,
            is_cloneable=is_cloneable,
            default=default,
            weight=weight,
            validation_minimum=validation_minimum,
            validation_maximum=validation_maximum,
            validation_regex=validation_regex,
            choice_set=choice_set,
        )

        custom_field.additional_properties = d
        return custom_field

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
