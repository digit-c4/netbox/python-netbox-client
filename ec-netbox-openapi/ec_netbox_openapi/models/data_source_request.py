from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.data_source_request_type import DataSourceRequestType
from ..types import UNSET, Unset

T = TypeVar("T", bound="DataSourceRequest")


@_attrs_define
class DataSourceRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        type (DataSourceRequestType): * `local` - Local
            * `git` - Git
            * `amazon-s3` - Amazon S3
        source_url (str):
        enabled (Union[Unset, bool]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        parameters (Union[Unset, Any]):
        ignore_rules (Union[Unset, str]): Patterns (one per line) matching files to ignore when syncing
    """

    name: str
    type: DataSourceRequestType
    source_url: str
    enabled: Union[Unset, bool] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    parameters: Union[Unset, Any] = UNSET
    ignore_rules: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        type = self.type.value

        source_url = self.source_url

        enabled = self.enabled

        description = self.description

        comments = self.comments

        parameters = self.parameters

        ignore_rules = self.ignore_rules

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "type": type,
                "source_url": source_url,
            }
        )
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if parameters is not UNSET:
            field_dict["parameters"] = parameters
        if ignore_rules is not UNSET:
            field_dict["ignore_rules"] = ignore_rules

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name")

        type = DataSourceRequestType(d.pop("type"))

        source_url = d.pop("source_url")

        enabled = d.pop("enabled", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        parameters = d.pop("parameters", UNSET)

        ignore_rules = d.pop("ignore_rules", UNSET)

        data_source_request = cls(
            name=name,
            type=type,
            source_url=source_url,
            enabled=enabled,
            description=description,
            comments=comments,
            parameters=parameters,
            ignore_rules=ignore_rules,
        )

        data_source_request.additional_properties = d
        return data_source_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
