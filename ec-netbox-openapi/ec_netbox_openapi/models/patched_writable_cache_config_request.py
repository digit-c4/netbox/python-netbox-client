import json
from typing import Any, Dict, List, Tuple, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_cache_config_request_list_extensions_item import (
    PatchedWritableCacheConfigRequestListExtensionsItem,
)
from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedWritableCacheConfigRequest")


@_attrs_define
class PatchedWritableCacheConfigRequest:
    """Cache Configuration Serializer class

    Attributes:
        mapping (Union[Unset, int]):
        list_extensions (Union[Unset, List[PatchedWritableCacheConfigRequestListExtensionsItem]]):
        ttl (Union[Unset, int]):
        max_size_limit (Union[Unset, int]):
    """

    mapping: Union[Unset, int] = UNSET
    list_extensions: Union[
        Unset, List[PatchedWritableCacheConfigRequestListExtensionsItem]
    ] = UNSET
    ttl: Union[Unset, int] = UNSET
    max_size_limit: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        mapping = self.mapping

        list_extensions: Union[Unset, List[str]] = UNSET
        if not isinstance(self.list_extensions, Unset):
            list_extensions = []
            for list_extensions_item_data in self.list_extensions:
                list_extensions_item = list_extensions_item_data.value
                list_extensions.append(list_extensions_item)

        ttl = self.ttl

        max_size_limit = self.max_size_limit

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if mapping is not UNSET:
            field_dict["mapping"] = mapping
        if list_extensions is not UNSET:
            field_dict["list_extensions"] = list_extensions
        if ttl is not UNSET:
            field_dict["ttl"] = ttl
        if max_size_limit is not UNSET:
            field_dict["max_size_limit"] = max_size_limit

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        mapping = (
            self.mapping
            if isinstance(self.mapping, Unset)
            else (None, str(self.mapping).encode(), "text/plain")
        )

        list_extensions: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.list_extensions, Unset):
            _temp_list_extensions = []
            for list_extensions_item_data in self.list_extensions:
                list_extensions_item = list_extensions_item_data.value
                _temp_list_extensions.append(list_extensions_item)
            list_extensions = (
                None,
                json.dumps(_temp_list_extensions).encode(),
                "application/json",
            )

        ttl = (
            self.ttl
            if isinstance(self.ttl, Unset)
            else (None, str(self.ttl).encode(), "text/plain")
        )

        max_size_limit = (
            self.max_size_limit
            if isinstance(self.max_size_limit, Unset)
            else (None, str(self.max_size_limit).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if mapping is not UNSET:
            field_dict["mapping"] = mapping
        if list_extensions is not UNSET:
            field_dict["list_extensions"] = list_extensions
        if ttl is not UNSET:
            field_dict["ttl"] = ttl
        if max_size_limit is not UNSET:
            field_dict["max_size_limit"] = max_size_limit

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        mapping = d.pop("mapping", UNSET)

        list_extensions = []
        _list_extensions = d.pop("list_extensions", UNSET)
        for list_extensions_item_data in _list_extensions or []:
            list_extensions_item = PatchedWritableCacheConfigRequestListExtensionsItem(
                list_extensions_item_data
            )

            list_extensions.append(list_extensions_item)

        ttl = d.pop("ttl", UNSET)

        max_size_limit = d.pop("max_size_limit", UNSET)

        patched_writable_cache_config_request = cls(
            mapping=mapping,
            list_extensions=list_extensions,
            ttl=ttl,
            max_size_limit=max_size_limit,
        )

        patched_writable_cache_config_request.additional_properties = d
        return patched_writable_cache_config_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
