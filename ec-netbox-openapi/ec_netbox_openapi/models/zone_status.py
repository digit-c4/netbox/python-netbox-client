from enum import Enum


class ZoneStatus(str, Enum):
    ACTIVE = "active"
    DEPRECATED = "deprecated"
    PARKED = "parked"
    RESERVED = "reserved"
    VALUE_4 = ""

    def __str__(self) -> str:
        return str(self.value)
