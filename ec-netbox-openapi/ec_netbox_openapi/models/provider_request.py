from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.provider_request_custom_fields import ProviderRequestCustomFields


T = TypeVar("T", bound="ProviderRequest")


@_attrs_define
class ProviderRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str): Full name of the provider
        slug (str):
        accounts (Union[Unset, List[int]]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        asns (Union[Unset, List[int]]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, ProviderRequestCustomFields]):
    """

    name: str
    slug: str
    accounts: Union[Unset, List[int]] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    asns: Union[Unset, List[int]] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "ProviderRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        slug = self.slug

        accounts: Union[Unset, List[int]] = UNSET
        if not isinstance(self.accounts, Unset):
            accounts = self.accounts

        description = self.description

        comments = self.comments

        asns: Union[Unset, List[int]] = UNSET
        if not isinstance(self.asns, Unset):
            asns = self.asns

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "slug": slug,
            }
        )
        if accounts is not UNSET:
            field_dict["accounts"] = accounts
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if asns is not UNSET:
            field_dict["asns"] = asns
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.provider_request_custom_fields import ProviderRequestCustomFields

        d = src_dict.copy()
        name = d.pop("name")

        slug = d.pop("slug")

        accounts = cast(List[int], d.pop("accounts", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        asns = cast(List[int], d.pop("asns", UNSET))

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ProviderRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ProviderRequestCustomFields.from_dict(_custom_fields)

        provider_request = cls(
            name=name,
            slug=slug,
            accounts=accounts,
            description=description,
            comments=comments,
            asns=asns,
            tags=tags,
            custom_fields=custom_fields,
        )

        provider_request.additional_properties = d
        return provider_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
