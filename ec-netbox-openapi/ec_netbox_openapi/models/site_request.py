from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.site_request_status import SiteRequestStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_region_request import NestedRegionRequest
    from ..models.nested_site_group_request import NestedSiteGroupRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_tenant_request import NestedTenantRequest
    from ..models.site_request_custom_fields import SiteRequestCustomFields


T = TypeVar("T", bound="SiteRequest")


@_attrs_define
class SiteRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str): Full name of the site
        slug (str):
        status (Union[Unset, SiteRequestStatus]): * `planned` - Planned
            * `staging` - Staging
            * `active` - Active
            * `decommissioning` - Decommissioning
            * `retired` - Retired
        region (Union['NestedRegionRequest', None, Unset]):
        group (Union['NestedSiteGroupRequest', None, Unset]):
        tenant (Union['NestedTenantRequest', None, Unset]):
        facility (Union[Unset, str]): Local facility ID or description
        time_zone (Union[None, Unset, str]):
        description (Union[Unset, str]):
        physical_address (Union[Unset, str]): Physical location of the building
        shipping_address (Union[Unset, str]): If different from the physical address
        latitude (Union[None, Unset, float]): GPS coordinate in decimal format (xx.yyyyyy)
        longitude (Union[None, Unset, float]): GPS coordinate in decimal format (xx.yyyyyy)
        comments (Union[Unset, str]):
        asns (Union[Unset, List[int]]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, SiteRequestCustomFields]):
    """

    name: str
    slug: str
    status: Union[Unset, SiteRequestStatus] = UNSET
    region: Union["NestedRegionRequest", None, Unset] = UNSET
    group: Union["NestedSiteGroupRequest", None, Unset] = UNSET
    tenant: Union["NestedTenantRequest", None, Unset] = UNSET
    facility: Union[Unset, str] = UNSET
    time_zone: Union[None, Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    physical_address: Union[Unset, str] = UNSET
    shipping_address: Union[Unset, str] = UNSET
    latitude: Union[None, Unset, float] = UNSET
    longitude: Union[None, Unset, float] = UNSET
    comments: Union[Unset, str] = UNSET
    asns: Union[Unset, List[int]] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "SiteRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_region_request import NestedRegionRequest
        from ..models.nested_site_group_request import NestedSiteGroupRequest
        from ..models.nested_tenant_request import NestedTenantRequest

        name = self.name

        slug = self.slug

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        region: Union[Dict[str, Any], None, Unset]
        if isinstance(self.region, Unset):
            region = UNSET
        elif isinstance(self.region, NestedRegionRequest):
            region = self.region.to_dict()
        else:
            region = self.region

        group: Union[Dict[str, Any], None, Unset]
        if isinstance(self.group, Unset):
            group = UNSET
        elif isinstance(self.group, NestedSiteGroupRequest):
            group = self.group.to_dict()
        else:
            group = self.group

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenantRequest):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        facility = self.facility

        time_zone: Union[None, Unset, str]
        if isinstance(self.time_zone, Unset):
            time_zone = UNSET
        else:
            time_zone = self.time_zone

        description = self.description

        physical_address = self.physical_address

        shipping_address = self.shipping_address

        latitude: Union[None, Unset, float]
        if isinstance(self.latitude, Unset):
            latitude = UNSET
        else:
            latitude = self.latitude

        longitude: Union[None, Unset, float]
        if isinstance(self.longitude, Unset):
            longitude = UNSET
        else:
            longitude = self.longitude

        comments = self.comments

        asns: Union[Unset, List[int]] = UNSET
        if not isinstance(self.asns, Unset):
            asns = self.asns

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "slug": slug,
            }
        )
        if status is not UNSET:
            field_dict["status"] = status
        if region is not UNSET:
            field_dict["region"] = region
        if group is not UNSET:
            field_dict["group"] = group
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if facility is not UNSET:
            field_dict["facility"] = facility
        if time_zone is not UNSET:
            field_dict["time_zone"] = time_zone
        if description is not UNSET:
            field_dict["description"] = description
        if physical_address is not UNSET:
            field_dict["physical_address"] = physical_address
        if shipping_address is not UNSET:
            field_dict["shipping_address"] = shipping_address
        if latitude is not UNSET:
            field_dict["latitude"] = latitude
        if longitude is not UNSET:
            field_dict["longitude"] = longitude
        if comments is not UNSET:
            field_dict["comments"] = comments
        if asns is not UNSET:
            field_dict["asns"] = asns
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_region_request import NestedRegionRequest
        from ..models.nested_site_group_request import NestedSiteGroupRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_tenant_request import NestedTenantRequest
        from ..models.site_request_custom_fields import SiteRequestCustomFields

        d = src_dict.copy()
        name = d.pop("name")

        slug = d.pop("slug")

        _status = d.pop("status", UNSET)
        status: Union[Unset, SiteRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = SiteRequestStatus(_status)

        def _parse_region(data: object) -> Union["NestedRegionRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                region_type_1 = NestedRegionRequest.from_dict(data)

                return region_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedRegionRequest", None, Unset], data)

        region = _parse_region(d.pop("region", UNSET))

        def _parse_group(data: object) -> Union["NestedSiteGroupRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                group_type_1 = NestedSiteGroupRequest.from_dict(data)

                return group_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedSiteGroupRequest", None, Unset], data)

        group = _parse_group(d.pop("group", UNSET))

        def _parse_tenant(data: object) -> Union["NestedTenantRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenantRequest.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenantRequest", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        facility = d.pop("facility", UNSET)

        def _parse_time_zone(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        time_zone = _parse_time_zone(d.pop("time_zone", UNSET))

        description = d.pop("description", UNSET)

        physical_address = d.pop("physical_address", UNSET)

        shipping_address = d.pop("shipping_address", UNSET)

        def _parse_latitude(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        latitude = _parse_latitude(d.pop("latitude", UNSET))

        def _parse_longitude(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        longitude = _parse_longitude(d.pop("longitude", UNSET))

        comments = d.pop("comments", UNSET)

        asns = cast(List[int], d.pop("asns", UNSET))

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, SiteRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = SiteRequestCustomFields.from_dict(_custom_fields)

        site_request = cls(
            name=name,
            slug=slug,
            status=status,
            region=region,
            group=group,
            tenant=tenant,
            facility=facility,
            time_zone=time_zone,
            description=description,
            physical_address=physical_address,
            shipping_address=shipping_address,
            latitude=latitude,
            longitude=longitude,
            comments=comments,
            asns=asns,
            tags=tags,
            custom_fields=custom_fields,
        )

        site_request.additional_properties = d
        return site_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
