from typing import Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

T = TypeVar("T", bound="WritableAclDeniedSourceRequest")


@_attrs_define
class WritableAclDeniedSourceRequest:
    """AclDeniedSource Serializer class

    Attributes:
        mapping (int):
        acl_source (str):
    """

    mapping: int
    acl_source: str
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        mapping = self.mapping

        acl_source = self.acl_source

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "mapping": mapping,
                "acl_source": acl_source,
            }
        )

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        mapping = (None, str(self.mapping).encode(), "text/plain")

        acl_source = (None, str(self.acl_source).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "mapping": mapping,
                "acl_source": acl_source,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        mapping = d.pop("mapping")

        acl_source = d.pop("acl_source")

        writable_acl_denied_source_request = cls(
            mapping=mapping,
            acl_source=acl_source,
        )

        writable_acl_denied_source_request.additional_properties = d
        return writable_acl_denied_source_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
