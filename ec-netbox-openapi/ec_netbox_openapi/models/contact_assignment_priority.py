from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.contact_assignment_priority_label import ContactAssignmentPriorityLabel
from ..models.contact_assignment_priority_value import ContactAssignmentPriorityValue
from ..types import UNSET, Unset

T = TypeVar("T", bound="ContactAssignmentPriority")


@_attrs_define
class ContactAssignmentPriority:
    """
    Attributes:
        value (Union[Unset, ContactAssignmentPriorityValue]): * `primary` - Primary
            * `secondary` - Secondary
            * `tertiary` - Tertiary
            * `inactive` - Inactive
        label (Union[Unset, ContactAssignmentPriorityLabel]):
    """

    value: Union[Unset, ContactAssignmentPriorityValue] = UNSET
    label: Union[Unset, ContactAssignmentPriorityLabel] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        value: Union[Unset, str] = UNSET
        if not isinstance(self.value, Unset):
            value = self.value.value

        label: Union[Unset, str] = UNSET
        if not isinstance(self.label, Unset):
            label = self.label.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if value is not UNSET:
            field_dict["value"] = value
        if label is not UNSET:
            field_dict["label"] = label

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        _value = d.pop("value", UNSET)
        value: Union[Unset, ContactAssignmentPriorityValue]
        if isinstance(_value, Unset):
            value = UNSET
        else:
            value = ContactAssignmentPriorityValue(_value)

        _label = d.pop("label", UNSET)
        label: Union[Unset, ContactAssignmentPriorityLabel]
        if isinstance(_label, Unset):
            label = UNSET
        else:
            label = ContactAssignmentPriorityLabel(_label)

        contact_assignment_priority = cls(
            value=value,
            label=label,
        )

        contact_assignment_priority.additional_properties = d
        return contact_assignment_priority

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
