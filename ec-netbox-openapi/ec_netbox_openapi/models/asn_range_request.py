from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.asn_range_request_custom_fields import ASNRangeRequestCustomFields
    from ..models.nested_rir_request import NestedRIRRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_tenant_request import NestedTenantRequest


T = TypeVar("T", bound="ASNRangeRequest")


@_attrs_define
class ASNRangeRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        slug (str):
        rir (NestedRIRRequest): Represents an object related through a ForeignKey field. On write, it accepts a primary
            key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        start (int):
        end (int):
        tenant (Union['NestedTenantRequest', None, Unset]):
        description (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, ASNRangeRequestCustomFields]):
    """

    name: str
    slug: str
    rir: "NestedRIRRequest"
    start: int
    end: int
    tenant: Union["NestedTenantRequest", None, Unset] = UNSET
    description: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "ASNRangeRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_tenant_request import NestedTenantRequest

        name = self.name

        slug = self.slug

        rir = self.rir.to_dict()

        start = self.start

        end = self.end

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenantRequest):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        description = self.description

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "slug": slug,
                "rir": rir,
                "start": start,
                "end": end,
            }
        )
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.asn_range_request_custom_fields import ASNRangeRequestCustomFields
        from ..models.nested_rir_request import NestedRIRRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_tenant_request import NestedTenantRequest

        d = src_dict.copy()
        name = d.pop("name")

        slug = d.pop("slug")

        rir = NestedRIRRequest.from_dict(d.pop("rir"))

        start = d.pop("start")

        end = d.pop("end")

        def _parse_tenant(data: object) -> Union["NestedTenantRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenantRequest.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenantRequest", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        description = d.pop("description", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ASNRangeRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ASNRangeRequestCustomFields.from_dict(_custom_fields)

        asn_range_request = cls(
            name=name,
            slug=slug,
            rir=rir,
            start=start,
            end=end,
            tenant=tenant,
            description=description,
            tags=tags,
            custom_fields=custom_fields,
        )

        asn_range_request.additional_properties = d
        return asn_range_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
