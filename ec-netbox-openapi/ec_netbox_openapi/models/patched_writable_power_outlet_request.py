import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_power_outlet_request_feed_leg import (
    PatchedWritablePowerOutletRequestFeedLeg,
)
from ..models.patched_writable_power_outlet_request_type import (
    PatchedWritablePowerOutletRequestType,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_writable_power_outlet_request_custom_fields import (
        PatchedWritablePowerOutletRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedWritablePowerOutletRequest")


@_attrs_define
class PatchedWritablePowerOutletRequest:
    """Adds support for custom fields and tags.

    Attributes:
        device (Union[Unset, int]):
        module (Union[None, Unset, int]):
        name (Union[Unset, str]):
        label (Union[Unset, str]): Physical label
        type (Union[Unset, PatchedWritablePowerOutletRequestType]): Physical port type

            * `iec-60320-c5` - C5
            * `iec-60320-c7` - C7
            * `iec-60320-c13` - C13
            * `iec-60320-c15` - C15
            * `iec-60320-c19` - C19
            * `iec-60320-c21` - C21
            * `iec-60309-p-n-e-4h` - P+N+E 4H
            * `iec-60309-p-n-e-6h` - P+N+E 6H
            * `iec-60309-p-n-e-9h` - P+N+E 9H
            * `iec-60309-2p-e-4h` - 2P+E 4H
            * `iec-60309-2p-e-6h` - 2P+E 6H
            * `iec-60309-2p-e-9h` - 2P+E 9H
            * `iec-60309-3p-e-4h` - 3P+E 4H
            * `iec-60309-3p-e-6h` - 3P+E 6H
            * `iec-60309-3p-e-9h` - 3P+E 9H
            * `iec-60309-3p-n-e-4h` - 3P+N+E 4H
            * `iec-60309-3p-n-e-6h` - 3P+N+E 6H
            * `iec-60309-3p-n-e-9h` - 3P+N+E 9H
            * `iec-60906-1` - IEC 60906-1
            * `nbr-14136-10a` - 2P+T 10A (NBR 14136)
            * `nbr-14136-20a` - 2P+T 20A (NBR 14136)
            * `nema-1-15r` - NEMA 1-15R
            * `nema-5-15r` - NEMA 5-15R
            * `nema-5-20r` - NEMA 5-20R
            * `nema-5-30r` - NEMA 5-30R
            * `nema-5-50r` - NEMA 5-50R
            * `nema-6-15r` - NEMA 6-15R
            * `nema-6-20r` - NEMA 6-20R
            * `nema-6-30r` - NEMA 6-30R
            * `nema-6-50r` - NEMA 6-50R
            * `nema-10-30r` - NEMA 10-30R
            * `nema-10-50r` - NEMA 10-50R
            * `nema-14-20r` - NEMA 14-20R
            * `nema-14-30r` - NEMA 14-30R
            * `nema-14-50r` - NEMA 14-50R
            * `nema-14-60r` - NEMA 14-60R
            * `nema-15-15r` - NEMA 15-15R
            * `nema-15-20r` - NEMA 15-20R
            * `nema-15-30r` - NEMA 15-30R
            * `nema-15-50r` - NEMA 15-50R
            * `nema-15-60r` - NEMA 15-60R
            * `nema-l1-15r` - NEMA L1-15R
            * `nema-l5-15r` - NEMA L5-15R
            * `nema-l5-20r` - NEMA L5-20R
            * `nema-l5-30r` - NEMA L5-30R
            * `nema-l5-50r` - NEMA L5-50R
            * `nema-l6-15r` - NEMA L6-15R
            * `nema-l6-20r` - NEMA L6-20R
            * `nema-l6-30r` - NEMA L6-30R
            * `nema-l6-50r` - NEMA L6-50R
            * `nema-l10-30r` - NEMA L10-30R
            * `nema-l14-20r` - NEMA L14-20R
            * `nema-l14-30r` - NEMA L14-30R
            * `nema-l14-50r` - NEMA L14-50R
            * `nema-l14-60r` - NEMA L14-60R
            * `nema-l15-20r` - NEMA L15-20R
            * `nema-l15-30r` - NEMA L15-30R
            * `nema-l15-50r` - NEMA L15-50R
            * `nema-l15-60r` - NEMA L15-60R
            * `nema-l21-20r` - NEMA L21-20R
            * `nema-l21-30r` - NEMA L21-30R
            * `nema-l22-30r` - NEMA L22-30R
            * `CS6360C` - CS6360C
            * `CS6364C` - CS6364C
            * `CS8164C` - CS8164C
            * `CS8264C` - CS8264C
            * `CS8364C` - CS8364C
            * `CS8464C` - CS8464C
            * `ita-e` - ITA Type E (CEE 7/5)
            * `ita-f` - ITA Type F (CEE 7/3)
            * `ita-g` - ITA Type G (BS 1363)
            * `ita-h` - ITA Type H
            * `ita-i` - ITA Type I
            * `ita-j` - ITA Type J
            * `ita-k` - ITA Type K
            * `ita-l` - ITA Type L (CEI 23-50)
            * `ita-m` - ITA Type M (BS 546)
            * `ita-n` - ITA Type N
            * `ita-o` - ITA Type O
            * `ita-multistandard` - ITA Multistandard
            * `usb-a` - USB Type A
            * `usb-micro-b` - USB Micro B
            * `usb-c` - USB Type C
            * `dc-terminal` - DC Terminal
            * `hdot-cx` - HDOT Cx
            * `saf-d-grid` - Saf-D-Grid
            * `neutrik-powercon-20a` - Neutrik powerCON (20A)
            * `neutrik-powercon-32a` - Neutrik powerCON (32A)
            * `neutrik-powercon-true1` - Neutrik powerCON TRUE1
            * `neutrik-powercon-true1-top` - Neutrik powerCON TRUE1 TOP
            * `ubiquiti-smartpower` - Ubiquiti SmartPower
            * `hardwired` - Hardwired
            * `other` - Other
        power_port (Union[None, Unset, int]):
        feed_leg (Union[Unset, PatchedWritablePowerOutletRequestFeedLeg]): Phase (for three-phase feeds)

            * `A` - A
            * `B` - B
            * `C` - C
        description (Union[Unset, str]):
        mark_connected (Union[Unset, bool]): Treat as if a cable is connected
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, PatchedWritablePowerOutletRequestCustomFields]):
    """

    device: Union[Unset, int] = UNSET
    module: Union[None, Unset, int] = UNSET
    name: Union[Unset, str] = UNSET
    label: Union[Unset, str] = UNSET
    type: Union[Unset, PatchedWritablePowerOutletRequestType] = UNSET
    power_port: Union[None, Unset, int] = UNSET
    feed_leg: Union[Unset, PatchedWritablePowerOutletRequestFeedLeg] = UNSET
    description: Union[Unset, str] = UNSET
    mark_connected: Union[Unset, bool] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "PatchedWritablePowerOutletRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        device = self.device

        module: Union[None, Unset, int]
        if isinstance(self.module, Unset):
            module = UNSET
        else:
            module = self.module

        name = self.name

        label = self.label

        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        power_port: Union[None, Unset, int]
        if isinstance(self.power_port, Unset):
            power_port = UNSET
        else:
            power_port = self.power_port

        feed_leg: Union[Unset, str] = UNSET
        if not isinstance(self.feed_leg, Unset):
            feed_leg = self.feed_leg.value

        description = self.description

        mark_connected = self.mark_connected

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if device is not UNSET:
            field_dict["device"] = device
        if module is not UNSET:
            field_dict["module"] = module
        if name is not UNSET:
            field_dict["name"] = name
        if label is not UNSET:
            field_dict["label"] = label
        if type is not UNSET:
            field_dict["type"] = type
        if power_port is not UNSET:
            field_dict["power_port"] = power_port
        if feed_leg is not UNSET:
            field_dict["feed_leg"] = feed_leg
        if description is not UNSET:
            field_dict["description"] = description
        if mark_connected is not UNSET:
            field_dict["mark_connected"] = mark_connected
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        device = (
            self.device
            if isinstance(self.device, Unset)
            else (None, str(self.device).encode(), "text/plain")
        )

        module: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.module, Unset):
            module = UNSET
        elif isinstance(self.module, int):
            module = (None, str(self.module).encode(), "text/plain")
        else:
            module = (None, str(self.module).encode(), "text/plain")

        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        label = (
            self.label
            if isinstance(self.label, Unset)
            else (None, str(self.label).encode(), "text/plain")
        )

        type: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.type, Unset):
            type = (None, str(self.type.value).encode(), "text/plain")

        power_port: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.power_port, Unset):
            power_port = UNSET
        elif isinstance(self.power_port, int):
            power_port = (None, str(self.power_port).encode(), "text/plain")
        else:
            power_port = (None, str(self.power_port).encode(), "text/plain")

        feed_leg: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.feed_leg, Unset):
            feed_leg = (None, str(self.feed_leg.value).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        mark_connected = (
            self.mark_connected
            if isinstance(self.mark_connected, Unset)
            else (None, str(self.mark_connected).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if device is not UNSET:
            field_dict["device"] = device
        if module is not UNSET:
            field_dict["module"] = module
        if name is not UNSET:
            field_dict["name"] = name
        if label is not UNSET:
            field_dict["label"] = label
        if type is not UNSET:
            field_dict["type"] = type
        if power_port is not UNSET:
            field_dict["power_port"] = power_port
        if feed_leg is not UNSET:
            field_dict["feed_leg"] = feed_leg
        if description is not UNSET:
            field_dict["description"] = description
        if mark_connected is not UNSET:
            field_dict["mark_connected"] = mark_connected
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_writable_power_outlet_request_custom_fields import (
            PatchedWritablePowerOutletRequestCustomFields,
        )

        d = src_dict.copy()
        device = d.pop("device", UNSET)

        def _parse_module(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        module = _parse_module(d.pop("module", UNSET))

        name = d.pop("name", UNSET)

        label = d.pop("label", UNSET)

        _type = d.pop("type", UNSET)
        type: Union[Unset, PatchedWritablePowerOutletRequestType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = PatchedWritablePowerOutletRequestType(_type)

        def _parse_power_port(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        power_port = _parse_power_port(d.pop("power_port", UNSET))

        _feed_leg = d.pop("feed_leg", UNSET)
        feed_leg: Union[Unset, PatchedWritablePowerOutletRequestFeedLeg]
        if isinstance(_feed_leg, Unset):
            feed_leg = UNSET
        else:
            feed_leg = PatchedWritablePowerOutletRequestFeedLeg(_feed_leg)

        description = d.pop("description", UNSET)

        mark_connected = d.pop("mark_connected", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedWritablePowerOutletRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedWritablePowerOutletRequestCustomFields.from_dict(
                _custom_fields
            )

        patched_writable_power_outlet_request = cls(
            device=device,
            module=module,
            name=name,
            label=label,
            type=type,
            power_port=power_port,
            feed_leg=feed_leg,
            description=description,
            mark_connected=mark_connected,
            tags=tags,
            custom_fields=custom_fields,
        )

        patched_writable_power_outlet_request.additional_properties = d
        return patched_writable_power_outlet_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
