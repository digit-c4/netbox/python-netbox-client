import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.ip_range_custom_fields import IPRangeCustomFields
    from ..models.ip_range_family import IPRangeFamily
    from ..models.ip_range_status import IPRangeStatus
    from ..models.nested_role import NestedRole
    from ..models.nested_tag import NestedTag
    from ..models.nested_tenant import NestedTenant
    from ..models.nested_vrf import NestedVRF


T = TypeVar("T", bound="IPRange")


@_attrs_define
class IPRange:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        family (IPRangeFamily):
        start_address (str):
        end_address (str):
        size (int):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        vrf (Union['NestedVRF', None, Unset]):
        tenant (Union['NestedTenant', None, Unset]):
        status (Union[Unset, IPRangeStatus]):
        role (Union['NestedRole', None, Unset]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, IPRangeCustomFields]):
        mark_utilized (Union[Unset, bool]): Treat as 100% utilized
    """

    id: int
    url: str
    display: str
    family: "IPRangeFamily"
    start_address: str
    end_address: str
    size: int
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    vrf: Union["NestedVRF", None, Unset] = UNSET
    tenant: Union["NestedTenant", None, Unset] = UNSET
    status: Union[Unset, "IPRangeStatus"] = UNSET
    role: Union["NestedRole", None, Unset] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "IPRangeCustomFields"] = UNSET
    mark_utilized: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_role import NestedRole
        from ..models.nested_tenant import NestedTenant
        from ..models.nested_vrf import NestedVRF

        id = self.id

        url = self.url

        display = self.display

        family = self.family.to_dict()

        start_address = self.start_address

        end_address = self.end_address

        size = self.size

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        vrf: Union[Dict[str, Any], None, Unset]
        if isinstance(self.vrf, Unset):
            vrf = UNSET
        elif isinstance(self.vrf, NestedVRF):
            vrf = self.vrf.to_dict()
        else:
            vrf = self.vrf

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenant):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        status: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.to_dict()

        role: Union[Dict[str, Any], None, Unset]
        if isinstance(self.role, Unset):
            role = UNSET
        elif isinstance(self.role, NestedRole):
            role = self.role.to_dict()
        else:
            role = self.role

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        mark_utilized = self.mark_utilized

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "family": family,
                "start_address": start_address,
                "end_address": end_address,
                "size": size,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if mark_utilized is not UNSET:
            field_dict["mark_utilized"] = mark_utilized

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.ip_range_custom_fields import IPRangeCustomFields
        from ..models.ip_range_family import IPRangeFamily
        from ..models.ip_range_status import IPRangeStatus
        from ..models.nested_role import NestedRole
        from ..models.nested_tag import NestedTag
        from ..models.nested_tenant import NestedTenant
        from ..models.nested_vrf import NestedVRF

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        family = IPRangeFamily.from_dict(d.pop("family"))

        start_address = d.pop("start_address")

        end_address = d.pop("end_address")

        size = d.pop("size")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        def _parse_vrf(data: object) -> Union["NestedVRF", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                vrf_type_1 = NestedVRF.from_dict(data)

                return vrf_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVRF", None, Unset], data)

        vrf = _parse_vrf(d.pop("vrf", UNSET))

        def _parse_tenant(data: object) -> Union["NestedTenant", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenant.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenant", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, IPRangeStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = IPRangeStatus.from_dict(_status)

        def _parse_role(data: object) -> Union["NestedRole", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                role_type_1 = NestedRole.from_dict(data)

                return role_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedRole", None, Unset], data)

        role = _parse_role(d.pop("role", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, IPRangeCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = IPRangeCustomFields.from_dict(_custom_fields)

        mark_utilized = d.pop("mark_utilized", UNSET)

        ip_range = cls(
            id=id,
            url=url,
            display=display,
            family=family,
            start_address=start_address,
            end_address=end_address,
            size=size,
            created=created,
            last_updated=last_updated,
            vrf=vrf,
            tenant=tenant,
            status=status,
            role=role,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
            mark_utilized=mark_utilized,
        )

        ip_range.additional_properties = d
        return ip_range

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
