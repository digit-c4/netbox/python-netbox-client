from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.nested_container_cap_add_type_0_item_type_1 import (
    NestedContainerCapAddType0ItemType1,
)
from ..models.nested_container_cap_add_type_0_item_type_2_type_1 import (
    NestedContainerCapAddType0ItemType2Type1,
)
from ..models.nested_container_cap_add_type_0_item_type_3_type_1 import (
    NestedContainerCapAddType0ItemType3Type1,
)
from ..models.nested_container_operation import NestedContainerOperation
from ..models.nested_container_restart_policy import NestedContainerRestartPolicy
from ..models.nested_container_state import NestedContainerState
from ..types import UNSET, Unset

T = TypeVar("T", bound="NestedContainer")


@_attrs_define
class NestedContainer:
    """Nested Container Serializer class

    Attributes:
        id (int):
        url (str):
        display (str):
        name (str):
        container_id (Union[None, Unset, str]):
        state (Union[Unset, NestedContainerState]): * `created` - Created
            * `restarting` - Restarting
            * `running` - Running
            * `paused` - Paused
            * `exited` - Exited
            * `dead` - Dead
            * `none` - None
        status (Union[None, Unset, str]):
        restart_policy (Union[Unset, NestedContainerRestartPolicy]): * `no` - no
            * `on-failure` - on-failure
            * `always` - always
            * `unless-stopped` - unless-stopped
        operation (Union[Unset, NestedContainerOperation]): * `create` - Create
            * `start` - Start
            * `restart` - Restart
            * `stop` - Stop
            * `recreate` - Recreate
            * `kill` - Kill
            * `none` - None
        hostname (Union[None, Unset, str]):
        cap_add (Union[List[Union[NestedContainerCapAddType0ItemType1, NestedContainerCapAddType0ItemType2Type1,
            NestedContainerCapAddType0ItemType3Type1, None]], None, Unset]):
    """

    id: int
    url: str
    display: str
    name: str
    container_id: Union[None, Unset, str] = UNSET
    state: Union[Unset, NestedContainerState] = UNSET
    status: Union[None, Unset, str] = UNSET
    restart_policy: Union[Unset, NestedContainerRestartPolicy] = UNSET
    operation: Union[Unset, NestedContainerOperation] = UNSET
    hostname: Union[None, Unset, str] = UNSET
    cap_add: Union[
        List[
            Union[
                NestedContainerCapAddType0ItemType1,
                NestedContainerCapAddType0ItemType2Type1,
                NestedContainerCapAddType0ItemType3Type1,
                None,
            ]
        ],
        None,
        Unset,
    ] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        name = self.name

        container_id: Union[None, Unset, str]
        if isinstance(self.container_id, Unset):
            container_id = UNSET
        else:
            container_id = self.container_id

        state: Union[Unset, str] = UNSET
        if not isinstance(self.state, Unset):
            state = self.state.value

        status: Union[None, Unset, str]
        if isinstance(self.status, Unset):
            status = UNSET
        else:
            status = self.status

        restart_policy: Union[Unset, str] = UNSET
        if not isinstance(self.restart_policy, Unset):
            restart_policy = self.restart_policy.value

        operation: Union[Unset, str] = UNSET
        if not isinstance(self.operation, Unset):
            operation = self.operation.value

        hostname: Union[None, Unset, str]
        if isinstance(self.hostname, Unset):
            hostname = UNSET
        else:
            hostname = self.hostname

        cap_add: Union[List[Union[None, str]], None, Unset]
        if isinstance(self.cap_add, Unset):
            cap_add = UNSET
        elif isinstance(self.cap_add, list):
            cap_add = []
            for cap_add_type_0_item_data in self.cap_add:
                cap_add_type_0_item: Union[None, str]
                if isinstance(
                    cap_add_type_0_item_data, NestedContainerCapAddType0ItemType1
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                elif isinstance(
                    cap_add_type_0_item_data, NestedContainerCapAddType0ItemType2Type1
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                elif isinstance(
                    cap_add_type_0_item_data, NestedContainerCapAddType0ItemType3Type1
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                else:
                    cap_add_type_0_item = cap_add_type_0_item_data
                cap_add.append(cap_add_type_0_item)

        else:
            cap_add = self.cap_add

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "name": name,
            }
        )
        if container_id is not UNSET:
            field_dict["ContainerID"] = container_id
        if state is not UNSET:
            field_dict["state"] = state
        if status is not UNSET:
            field_dict["status"] = status
        if restart_policy is not UNSET:
            field_dict["restart_policy"] = restart_policy
        if operation is not UNSET:
            field_dict["operation"] = operation
        if hostname is not UNSET:
            field_dict["hostname"] = hostname
        if cap_add is not UNSET:
            field_dict["cap_add"] = cap_add

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        name = d.pop("name")

        def _parse_container_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        container_id = _parse_container_id(d.pop("ContainerID", UNSET))

        _state = d.pop("state", UNSET)
        state: Union[Unset, NestedContainerState]
        if isinstance(_state, Unset):
            state = UNSET
        else:
            state = NestedContainerState(_state)

        def _parse_status(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        status = _parse_status(d.pop("status", UNSET))

        _restart_policy = d.pop("restart_policy", UNSET)
        restart_policy: Union[Unset, NestedContainerRestartPolicy]
        if isinstance(_restart_policy, Unset):
            restart_policy = UNSET
        else:
            restart_policy = NestedContainerRestartPolicy(_restart_policy)

        _operation = d.pop("operation", UNSET)
        operation: Union[Unset, NestedContainerOperation]
        if isinstance(_operation, Unset):
            operation = UNSET
        else:
            operation = NestedContainerOperation(_operation)

        def _parse_hostname(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        hostname = _parse_hostname(d.pop("hostname", UNSET))

        def _parse_cap_add(
            data: object,
        ) -> Union[
            List[
                Union[
                    NestedContainerCapAddType0ItemType1,
                    NestedContainerCapAddType0ItemType2Type1,
                    NestedContainerCapAddType0ItemType3Type1,
                    None,
                ]
            ],
            None,
            Unset,
        ]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                cap_add_type_0 = []
                _cap_add_type_0 = data
                for cap_add_type_0_item_data in _cap_add_type_0:

                    def _parse_cap_add_type_0_item(
                        data: object,
                    ) -> Union[
                        NestedContainerCapAddType0ItemType1,
                        NestedContainerCapAddType0ItemType2Type1,
                        NestedContainerCapAddType0ItemType3Type1,
                        None,
                    ]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_1 = (
                                NestedContainerCapAddType0ItemType1(data)
                            )

                            return cap_add_type_0_item_type_1
                        except:  # noqa: E722
                            pass
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_2_type_1 = (
                                NestedContainerCapAddType0ItemType2Type1(data)
                            )

                            return cap_add_type_0_item_type_2_type_1
                        except:  # noqa: E722
                            pass
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_3_type_1 = (
                                NestedContainerCapAddType0ItemType3Type1(data)
                            )

                            return cap_add_type_0_item_type_3_type_1
                        except:  # noqa: E722
                            pass
                        return cast(
                            Union[
                                NestedContainerCapAddType0ItemType1,
                                NestedContainerCapAddType0ItemType2Type1,
                                NestedContainerCapAddType0ItemType3Type1,
                                None,
                            ],
                            data,
                        )

                    cap_add_type_0_item = _parse_cap_add_type_0_item(
                        cap_add_type_0_item_data
                    )

                    cap_add_type_0.append(cap_add_type_0_item)

                return cap_add_type_0
            except:  # noqa: E722
                pass
            return cast(
                Union[
                    List[
                        Union[
                            NestedContainerCapAddType0ItemType1,
                            NestedContainerCapAddType0ItemType2Type1,
                            NestedContainerCapAddType0ItemType3Type1,
                            None,
                        ]
                    ],
                    None,
                    Unset,
                ],
                data,
            )

        cap_add = _parse_cap_add(d.pop("cap_add", UNSET))

        nested_container = cls(
            id=id,
            url=url,
            display=display,
            name=name,
            container_id=container_id,
            state=state,
            status=status,
            restart_policy=restart_policy,
            operation=operation,
            hostname=hostname,
            cap_add=cap_add,
        )

        nested_container.additional_properties = d
        return nested_container

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
