import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_config_template import NestedConfigTemplate
    from ..models.nested_manufacturer import NestedManufacturer
    from ..models.nested_tag import NestedTag
    from ..models.platform_custom_fields import PlatformCustomFields


T = TypeVar("T", bound="Platform")


@_attrs_define
class Platform:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        name (str):
        slug (str):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        device_count (int):
        virtualmachine_count (int):
        manufacturer (Union['NestedManufacturer', None, Unset]):
        config_template (Union['NestedConfigTemplate', None, Unset]):
        description (Union[Unset, str]):
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, PlatformCustomFields]):
    """

    id: int
    url: str
    display: str
    name: str
    slug: str
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    device_count: int
    virtualmachine_count: int
    manufacturer: Union["NestedManufacturer", None, Unset] = UNSET
    config_template: Union["NestedConfigTemplate", None, Unset] = UNSET
    description: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "PlatformCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_config_template import NestedConfigTemplate
        from ..models.nested_manufacturer import NestedManufacturer

        id = self.id

        url = self.url

        display = self.display

        name = self.name

        slug = self.slug

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        device_count = self.device_count

        virtualmachine_count = self.virtualmachine_count

        manufacturer: Union[Dict[str, Any], None, Unset]
        if isinstance(self.manufacturer, Unset):
            manufacturer = UNSET
        elif isinstance(self.manufacturer, NestedManufacturer):
            manufacturer = self.manufacturer.to_dict()
        else:
            manufacturer = self.manufacturer

        config_template: Union[Dict[str, Any], None, Unset]
        if isinstance(self.config_template, Unset):
            config_template = UNSET
        elif isinstance(self.config_template, NestedConfigTemplate):
            config_template = self.config_template.to_dict()
        else:
            config_template = self.config_template

        description = self.description

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "name": name,
                "slug": slug,
                "created": created,
                "last_updated": last_updated,
                "device_count": device_count,
                "virtualmachine_count": virtualmachine_count,
            }
        )
        if manufacturer is not UNSET:
            field_dict["manufacturer"] = manufacturer
        if config_template is not UNSET:
            field_dict["config_template"] = config_template
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_config_template import NestedConfigTemplate
        from ..models.nested_manufacturer import NestedManufacturer
        from ..models.nested_tag import NestedTag
        from ..models.platform_custom_fields import PlatformCustomFields

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        name = d.pop("name")

        slug = d.pop("slug")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        device_count = d.pop("device_count")

        virtualmachine_count = d.pop("virtualmachine_count")

        def _parse_manufacturer(
            data: object,
        ) -> Union["NestedManufacturer", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                manufacturer_type_1 = NestedManufacturer.from_dict(data)

                return manufacturer_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedManufacturer", None, Unset], data)

        manufacturer = _parse_manufacturer(d.pop("manufacturer", UNSET))

        def _parse_config_template(
            data: object,
        ) -> Union["NestedConfigTemplate", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                config_template_type_1 = NestedConfigTemplate.from_dict(data)

                return config_template_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedConfigTemplate", None, Unset], data)

        config_template = _parse_config_template(d.pop("config_template", UNSET))

        description = d.pop("description", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PlatformCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PlatformCustomFields.from_dict(_custom_fields)

        platform = cls(
            id=id,
            url=url,
            display=display,
            name=name,
            slug=slug,
            created=created,
            last_updated=last_updated,
            device_count=device_count,
            virtualmachine_count=virtualmachine_count,
            manufacturer=manufacturer,
            config_template=config_template,
            description=description,
            tags=tags,
            custom_fields=custom_fields,
        )

        platform.additional_properties = d
        return platform

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
