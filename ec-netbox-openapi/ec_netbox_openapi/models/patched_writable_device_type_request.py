import json
from io import BytesIO
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_device_type_request_airflow import (
    PatchedWritableDeviceTypeRequestAirflow,
)
from ..models.patched_writable_device_type_request_parentchild_status import (
    PatchedWritableDeviceTypeRequestParentchildStatus,
)
from ..models.patched_writable_device_type_request_weight_unit import (
    PatchedWritableDeviceTypeRequestWeightUnit,
)
from ..types import UNSET, File, FileJsonType, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_writable_device_type_request_custom_fields import (
        PatchedWritableDeviceTypeRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedWritableDeviceTypeRequest")


@_attrs_define
class PatchedWritableDeviceTypeRequest:
    """Adds support for custom fields and tags.

    Attributes:
        manufacturer (Union[Unset, int]):
        default_platform (Union[None, Unset, int]):
        model (Union[Unset, str]):
        slug (Union[Unset, str]):
        part_number (Union[Unset, str]): Discrete part number (optional)
        u_height (Union[Unset, float]):  Default: 1.0.
        is_full_depth (Union[Unset, bool]): Device consumes both front and rear rack faces
        subdevice_role (Union[Unset, PatchedWritableDeviceTypeRequestParentchildStatus]): Parent devices house child
            devices in device bays. Leave blank if this device type is neither a parent nor a child.

            * `parent` - Parent
            * `child` - Child
        airflow (Union[Unset, PatchedWritableDeviceTypeRequestAirflow]): * `front-to-rear` - Front to rear
            * `rear-to-front` - Rear to front
            * `left-to-right` - Left to right
            * `right-to-left` - Right to left
            * `side-to-rear` - Side to rear
            * `passive` - Passive
            * `mixed` - Mixed
        weight (Union[None, Unset, float]):
        weight_unit (Union[Unset, PatchedWritableDeviceTypeRequestWeightUnit]): * `kg` - Kilograms
            * `g` - Grams
            * `lb` - Pounds
            * `oz` - Ounces
        front_image (Union[Unset, File]):
        rear_image (Union[Unset, File]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, PatchedWritableDeviceTypeRequestCustomFields]):
    """

    manufacturer: Union[Unset, int] = UNSET
    default_platform: Union[None, Unset, int] = UNSET
    model: Union[Unset, str] = UNSET
    slug: Union[Unset, str] = UNSET
    part_number: Union[Unset, str] = UNSET
    u_height: Union[Unset, float] = 1.0
    is_full_depth: Union[Unset, bool] = UNSET
    subdevice_role: Union[Unset, PatchedWritableDeviceTypeRequestParentchildStatus] = (
        UNSET
    )
    airflow: Union[Unset, PatchedWritableDeviceTypeRequestAirflow] = UNSET
    weight: Union[None, Unset, float] = UNSET
    weight_unit: Union[Unset, PatchedWritableDeviceTypeRequestWeightUnit] = UNSET
    front_image: Union[Unset, File] = UNSET
    rear_image: Union[Unset, File] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "PatchedWritableDeviceTypeRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        manufacturer = self.manufacturer

        default_platform: Union[None, Unset, int]
        if isinstance(self.default_platform, Unset):
            default_platform = UNSET
        else:
            default_platform = self.default_platform

        model = self.model

        slug = self.slug

        part_number = self.part_number

        u_height = self.u_height

        is_full_depth = self.is_full_depth

        subdevice_role: Union[Unset, str] = UNSET
        if not isinstance(self.subdevice_role, Unset):
            subdevice_role = self.subdevice_role.value

        airflow: Union[Unset, str] = UNSET
        if not isinstance(self.airflow, Unset):
            airflow = self.airflow.value

        weight: Union[None, Unset, float]
        if isinstance(self.weight, Unset):
            weight = UNSET
        else:
            weight = self.weight

        weight_unit: Union[Unset, str] = UNSET
        if not isinstance(self.weight_unit, Unset):
            weight_unit = self.weight_unit.value

        front_image: Union[Unset, FileJsonType] = UNSET
        if not isinstance(self.front_image, Unset):
            front_image = self.front_image.to_tuple()

        rear_image: Union[Unset, FileJsonType] = UNSET
        if not isinstance(self.rear_image, Unset):
            rear_image = self.rear_image.to_tuple()

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if manufacturer is not UNSET:
            field_dict["manufacturer"] = manufacturer
        if default_platform is not UNSET:
            field_dict["default_platform"] = default_platform
        if model is not UNSET:
            field_dict["model"] = model
        if slug is not UNSET:
            field_dict["slug"] = slug
        if part_number is not UNSET:
            field_dict["part_number"] = part_number
        if u_height is not UNSET:
            field_dict["u_height"] = u_height
        if is_full_depth is not UNSET:
            field_dict["is_full_depth"] = is_full_depth
        if subdevice_role is not UNSET:
            field_dict["subdevice_role"] = subdevice_role
        if airflow is not UNSET:
            field_dict["airflow"] = airflow
        if weight is not UNSET:
            field_dict["weight"] = weight
        if weight_unit is not UNSET:
            field_dict["weight_unit"] = weight_unit
        if front_image is not UNSET:
            field_dict["front_image"] = front_image
        if rear_image is not UNSET:
            field_dict["rear_image"] = rear_image
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        manufacturer = (
            self.manufacturer
            if isinstance(self.manufacturer, Unset)
            else (None, str(self.manufacturer).encode(), "text/plain")
        )

        default_platform: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.default_platform, Unset):
            default_platform = UNSET
        elif isinstance(self.default_platform, int):
            default_platform = (None, str(self.default_platform).encode(), "text/plain")
        else:
            default_platform = (None, str(self.default_platform).encode(), "text/plain")

        model = (
            self.model
            if isinstance(self.model, Unset)
            else (None, str(self.model).encode(), "text/plain")
        )

        slug = (
            self.slug
            if isinstance(self.slug, Unset)
            else (None, str(self.slug).encode(), "text/plain")
        )

        part_number = (
            self.part_number
            if isinstance(self.part_number, Unset)
            else (None, str(self.part_number).encode(), "text/plain")
        )

        u_height = (
            self.u_height
            if isinstance(self.u_height, Unset)
            else (None, str(self.u_height).encode(), "text/plain")
        )

        is_full_depth = (
            self.is_full_depth
            if isinstance(self.is_full_depth, Unset)
            else (None, str(self.is_full_depth).encode(), "text/plain")
        )

        subdevice_role: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.subdevice_role, Unset):
            subdevice_role = (
                None,
                str(self.subdevice_role.value).encode(),
                "text/plain",
            )

        airflow: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.airflow, Unset):
            airflow = (None, str(self.airflow.value).encode(), "text/plain")

        weight: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.weight, Unset):
            weight = UNSET
        elif isinstance(self.weight, float):
            weight = (None, str(self.weight).encode(), "text/plain")
        else:
            weight = (None, str(self.weight).encode(), "text/plain")

        weight_unit: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.weight_unit, Unset):
            weight_unit = (None, str(self.weight_unit.value).encode(), "text/plain")

        front_image: Union[Unset, FileJsonType] = UNSET
        if not isinstance(self.front_image, Unset):
            front_image = self.front_image.to_tuple()

        rear_image: Union[Unset, FileJsonType] = UNSET
        if not isinstance(self.rear_image, Unset):
            rear_image = self.rear_image.to_tuple()

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if manufacturer is not UNSET:
            field_dict["manufacturer"] = manufacturer
        if default_platform is not UNSET:
            field_dict["default_platform"] = default_platform
        if model is not UNSET:
            field_dict["model"] = model
        if slug is not UNSET:
            field_dict["slug"] = slug
        if part_number is not UNSET:
            field_dict["part_number"] = part_number
        if u_height is not UNSET:
            field_dict["u_height"] = u_height
        if is_full_depth is not UNSET:
            field_dict["is_full_depth"] = is_full_depth
        if subdevice_role is not UNSET:
            field_dict["subdevice_role"] = subdevice_role
        if airflow is not UNSET:
            field_dict["airflow"] = airflow
        if weight is not UNSET:
            field_dict["weight"] = weight
        if weight_unit is not UNSET:
            field_dict["weight_unit"] = weight_unit
        if front_image is not UNSET:
            field_dict["front_image"] = front_image
        if rear_image is not UNSET:
            field_dict["rear_image"] = rear_image
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_writable_device_type_request_custom_fields import (
            PatchedWritableDeviceTypeRequestCustomFields,
        )

        d = src_dict.copy()
        manufacturer = d.pop("manufacturer", UNSET)

        def _parse_default_platform(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        default_platform = _parse_default_platform(d.pop("default_platform", UNSET))

        model = d.pop("model", UNSET)

        slug = d.pop("slug", UNSET)

        part_number = d.pop("part_number", UNSET)

        u_height = d.pop("u_height", UNSET)

        is_full_depth = d.pop("is_full_depth", UNSET)

        _subdevice_role = d.pop("subdevice_role", UNSET)
        subdevice_role: Union[Unset, PatchedWritableDeviceTypeRequestParentchildStatus]
        if isinstance(_subdevice_role, Unset):
            subdevice_role = UNSET
        else:
            subdevice_role = PatchedWritableDeviceTypeRequestParentchildStatus(
                _subdevice_role
            )

        _airflow = d.pop("airflow", UNSET)
        airflow: Union[Unset, PatchedWritableDeviceTypeRequestAirflow]
        if isinstance(_airflow, Unset):
            airflow = UNSET
        else:
            airflow = PatchedWritableDeviceTypeRequestAirflow(_airflow)

        def _parse_weight(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        weight = _parse_weight(d.pop("weight", UNSET))

        _weight_unit = d.pop("weight_unit", UNSET)
        weight_unit: Union[Unset, PatchedWritableDeviceTypeRequestWeightUnit]
        if isinstance(_weight_unit, Unset):
            weight_unit = UNSET
        else:
            weight_unit = PatchedWritableDeviceTypeRequestWeightUnit(_weight_unit)

        _front_image = d.pop("front_image", UNSET)
        front_image: Union[Unset, File]
        if isinstance(_front_image, Unset):
            front_image = UNSET
        else:
            front_image = File(payload=BytesIO(_front_image))

        _rear_image = d.pop("rear_image", UNSET)
        rear_image: Union[Unset, File]
        if isinstance(_rear_image, Unset):
            rear_image = UNSET
        else:
            rear_image = File(payload=BytesIO(_rear_image))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedWritableDeviceTypeRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedWritableDeviceTypeRequestCustomFields.from_dict(
                _custom_fields
            )

        patched_writable_device_type_request = cls(
            manufacturer=manufacturer,
            default_platform=default_platform,
            model=model,
            slug=slug,
            part_number=part_number,
            u_height=u_height,
            is_full_depth=is_full_depth,
            subdevice_role=subdevice_role,
            airflow=airflow,
            weight=weight,
            weight_unit=weight_unit,
            front_image=front_image,
            rear_image=rear_image,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        patched_writable_device_type_request.additional_properties = d
        return patched_writable_device_type_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
