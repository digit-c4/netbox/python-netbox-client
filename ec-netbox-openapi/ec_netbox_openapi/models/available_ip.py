from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_vrf import NestedVRF


T = TypeVar("T", bound="AvailableIP")


@_attrs_define
class AvailableIP:
    """Representation of an IP address which does not exist in the database.

    Attributes:
        family (int):
        address (str):
        vrf (NestedVRF): Represents an object related through a ForeignKey field. On write, it accepts a primary key
            (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        description (Union[Unset, str]):
    """

    family: int
    address: str
    vrf: "NestedVRF"
    description: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        family = self.family

        address = self.address

        vrf = self.vrf.to_dict()

        description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "family": family,
                "address": address,
                "vrf": vrf,
            }
        )
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_vrf import NestedVRF

        d = src_dict.copy()
        family = d.pop("family")

        address = d.pop("address")

        vrf = NestedVRF.from_dict(d.pop("vrf"))

        description = d.pop("description", UNSET)

        available_ip = cls(
            family=family,
            address=address,
            vrf=vrf,
            description=description,
        )

        available_ip.additional_properties = d
        return available_ip

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
