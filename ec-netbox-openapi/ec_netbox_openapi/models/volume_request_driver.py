from enum import Enum


class VolumeRequestDriver(str, Enum):
    LOCAL = "local"

    def __str__(self) -> str:
        return str(self.value)
