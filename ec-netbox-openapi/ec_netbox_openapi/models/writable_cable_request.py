import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_cable_request_length_unit import WritableCableRequestLengthUnit
from ..models.writable_cable_request_status import WritableCableRequestStatus
from ..models.writable_cable_request_type import WritableCableRequestType
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.generic_object_request import GenericObjectRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_cable_request_custom_fields import (
        WritableCableRequestCustomFields,
    )


T = TypeVar("T", bound="WritableCableRequest")


@_attrs_define
class WritableCableRequest:
    """Adds support for custom fields and tags.

    Attributes:
        type (Union[Unset, WritableCableRequestType]): * `cat3` - CAT3
            * `cat5` - CAT5
            * `cat5e` - CAT5e
            * `cat6` - CAT6
            * `cat6a` - CAT6a
            * `cat7` - CAT7
            * `cat7a` - CAT7a
            * `cat8` - CAT8
            * `dac-active` - Direct Attach Copper (Active)
            * `dac-passive` - Direct Attach Copper (Passive)
            * `mrj21-trunk` - MRJ21 Trunk
            * `coaxial` - Coaxial
            * `mmf` - Multimode Fiber
            * `mmf-om1` - Multimode Fiber (OM1)
            * `mmf-om2` - Multimode Fiber (OM2)
            * `mmf-om3` - Multimode Fiber (OM3)
            * `mmf-om4` - Multimode Fiber (OM4)
            * `mmf-om5` - Multimode Fiber (OM5)
            * `smf` - Singlemode Fiber
            * `smf-os1` - Singlemode Fiber (OS1)
            * `smf-os2` - Singlemode Fiber (OS2)
            * `aoc` - Active Optical Cabling (AOC)
            * `power` - Power
        a_terminations (Union[Unset, List['GenericObjectRequest']]):
        b_terminations (Union[Unset, List['GenericObjectRequest']]):
        status (Union[Unset, WritableCableRequestStatus]): * `connected` - Connected
            * `planned` - Planned
            * `decommissioning` - Decommissioning
        tenant (Union[None, Unset, int]):
        label (Union[Unset, str]):
        color (Union[Unset, str]):
        length (Union[None, Unset, float]):
        length_unit (Union[Unset, WritableCableRequestLengthUnit]): * `km` - Kilometers
            * `m` - Meters
            * `cm` - Centimeters
            * `mi` - Miles
            * `ft` - Feet
            * `in` - Inches
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritableCableRequestCustomFields]):
    """

    type: Union[Unset, WritableCableRequestType] = UNSET
    a_terminations: Union[Unset, List["GenericObjectRequest"]] = UNSET
    b_terminations: Union[Unset, List["GenericObjectRequest"]] = UNSET
    status: Union[Unset, WritableCableRequestStatus] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    label: Union[Unset, str] = UNSET
    color: Union[Unset, str] = UNSET
    length: Union[None, Unset, float] = UNSET
    length_unit: Union[Unset, WritableCableRequestLengthUnit] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WritableCableRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        a_terminations: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.a_terminations, Unset):
            a_terminations = []
            for a_terminations_item_data in self.a_terminations:
                a_terminations_item = a_terminations_item_data.to_dict()
                a_terminations.append(a_terminations_item)

        b_terminations: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.b_terminations, Unset):
            b_terminations = []
            for b_terminations_item_data in self.b_terminations:
                b_terminations_item = b_terminations_item_data.to_dict()
                b_terminations.append(b_terminations_item)

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        label = self.label

        color = self.color

        length: Union[None, Unset, float]
        if isinstance(self.length, Unset):
            length = UNSET
        else:
            length = self.length

        length_unit: Union[Unset, str] = UNSET
        if not isinstance(self.length_unit, Unset):
            length_unit = self.length_unit.value

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if type is not UNSET:
            field_dict["type"] = type
        if a_terminations is not UNSET:
            field_dict["a_terminations"] = a_terminations
        if b_terminations is not UNSET:
            field_dict["b_terminations"] = b_terminations
        if status is not UNSET:
            field_dict["status"] = status
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if label is not UNSET:
            field_dict["label"] = label
        if color is not UNSET:
            field_dict["color"] = color
        if length is not UNSET:
            field_dict["length"] = length
        if length_unit is not UNSET:
            field_dict["length_unit"] = length_unit
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        type: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.type, Unset):
            type = (None, str(self.type.value).encode(), "text/plain")

        a_terminations: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.a_terminations, Unset):
            _temp_a_terminations = []
            for a_terminations_item_data in self.a_terminations:
                a_terminations_item = a_terminations_item_data.to_dict()
                _temp_a_terminations.append(a_terminations_item)
            a_terminations = (
                None,
                json.dumps(_temp_a_terminations).encode(),
                "application/json",
            )

        b_terminations: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.b_terminations, Unset):
            _temp_b_terminations = []
            for b_terminations_item_data in self.b_terminations:
                b_terminations_item = b_terminations_item_data.to_dict()
                _temp_b_terminations.append(b_terminations_item)
            b_terminations = (
                None,
                json.dumps(_temp_b_terminations).encode(),
                "application/json",
            )

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        label = (
            self.label
            if isinstance(self.label, Unset)
            else (None, str(self.label).encode(), "text/plain")
        )

        color = (
            self.color
            if isinstance(self.color, Unset)
            else (None, str(self.color).encode(), "text/plain")
        )

        length: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.length, Unset):
            length = UNSET
        elif isinstance(self.length, float):
            length = (None, str(self.length).encode(), "text/plain")
        else:
            length = (None, str(self.length).encode(), "text/plain")

        length_unit: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.length_unit, Unset):
            length_unit = (None, str(self.length_unit.value).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if type is not UNSET:
            field_dict["type"] = type
        if a_terminations is not UNSET:
            field_dict["a_terminations"] = a_terminations
        if b_terminations is not UNSET:
            field_dict["b_terminations"] = b_terminations
        if status is not UNSET:
            field_dict["status"] = status
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if label is not UNSET:
            field_dict["label"] = label
        if color is not UNSET:
            field_dict["color"] = color
        if length is not UNSET:
            field_dict["length"] = length
        if length_unit is not UNSET:
            field_dict["length_unit"] = length_unit
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.generic_object_request import GenericObjectRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_cable_request_custom_fields import (
            WritableCableRequestCustomFields,
        )

        d = src_dict.copy()
        _type = d.pop("type", UNSET)
        type: Union[Unset, WritableCableRequestType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = WritableCableRequestType(_type)

        a_terminations = []
        _a_terminations = d.pop("a_terminations", UNSET)
        for a_terminations_item_data in _a_terminations or []:
            a_terminations_item = GenericObjectRequest.from_dict(
                a_terminations_item_data
            )

            a_terminations.append(a_terminations_item)

        b_terminations = []
        _b_terminations = d.pop("b_terminations", UNSET)
        for b_terminations_item_data in _b_terminations or []:
            b_terminations_item = GenericObjectRequest.from_dict(
                b_terminations_item_data
            )

            b_terminations.append(b_terminations_item)

        _status = d.pop("status", UNSET)
        status: Union[Unset, WritableCableRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = WritableCableRequestStatus(_status)

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        label = d.pop("label", UNSET)

        color = d.pop("color", UNSET)

        def _parse_length(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        length = _parse_length(d.pop("length", UNSET))

        _length_unit = d.pop("length_unit", UNSET)
        length_unit: Union[Unset, WritableCableRequestLengthUnit]
        if isinstance(_length_unit, Unset):
            length_unit = UNSET
        else:
            length_unit = WritableCableRequestLengthUnit(_length_unit)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableCableRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableCableRequestCustomFields.from_dict(_custom_fields)

        writable_cable_request = cls(
            type=type,
            a_terminations=a_terminations,
            b_terminations=b_terminations,
            status=status,
            tenant=tenant,
            label=label,
            color=color,
            length=length,
            length_unit=length_unit,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_cable_request.additional_properties = d
        return writable_cable_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
