from enum import Enum


class PatchedCertificateRequestCertificateService(str, Enum):
    LB = "LB"
    RAS = "RAS"
    RPS = "RPS"
    VALUE_4 = ""
    WIFI = "WIFI"

    def __str__(self) -> str:
        return str(self.value)
