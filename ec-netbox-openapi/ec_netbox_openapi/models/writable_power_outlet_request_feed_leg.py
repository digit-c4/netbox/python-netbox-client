from enum import Enum


class WritablePowerOutletRequestFeedLeg(str, Enum):
    A = "A"
    B = "B"
    C = "C"
    VALUE_3 = ""

    def __str__(self) -> str:
        return str(self.value)
