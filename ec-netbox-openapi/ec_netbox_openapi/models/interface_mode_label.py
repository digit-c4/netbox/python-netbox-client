from enum import Enum


class InterfaceModeLabel(str, Enum):
    ACCESS = "Access"
    TAGGED = "Tagged"
    TAGGED_ALL = "Tagged (All)"

    def __str__(self) -> str:
        return str(self.value)
