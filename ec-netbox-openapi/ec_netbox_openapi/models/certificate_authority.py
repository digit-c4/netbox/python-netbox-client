import datetime
from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

T = TypeVar("T", bound="CertificateAuthority")


@_attrs_define
class CertificateAuthority:
    """Certificate Serializer class

    Attributes:
        id (int):
        ca_name (str):
        url (str):
        acme_url (Union[None, Unset, str]):
        key_vault_url (Union[None, Unset, str]):
        start_date (Union[None, Unset, datetime.date]):
        end_date (Union[None, Unset, datetime.date]):
        default_validity (Union[None, Unset, int]):
    """

    id: int
    ca_name: str
    url: str
    acme_url: Union[None, Unset, str] = UNSET
    key_vault_url: Union[None, Unset, str] = UNSET
    start_date: Union[None, Unset, datetime.date] = UNSET
    end_date: Union[None, Unset, datetime.date] = UNSET
    default_validity: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        ca_name = self.ca_name

        url = self.url

        acme_url: Union[None, Unset, str]
        if isinstance(self.acme_url, Unset):
            acme_url = UNSET
        else:
            acme_url = self.acme_url

        key_vault_url: Union[None, Unset, str]
        if isinstance(self.key_vault_url, Unset):
            key_vault_url = UNSET
        else:
            key_vault_url = self.key_vault_url

        start_date: Union[None, Unset, str]
        if isinstance(self.start_date, Unset):
            start_date = UNSET
        elif isinstance(self.start_date, datetime.date):
            start_date = self.start_date.isoformat()
        else:
            start_date = self.start_date

        end_date: Union[None, Unset, str]
        if isinstance(self.end_date, Unset):
            end_date = UNSET
        elif isinstance(self.end_date, datetime.date):
            end_date = self.end_date.isoformat()
        else:
            end_date = self.end_date

        default_validity: Union[None, Unset, int]
        if isinstance(self.default_validity, Unset):
            default_validity = UNSET
        else:
            default_validity = self.default_validity

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "ca_name": ca_name,
                "url": url,
            }
        )
        if acme_url is not UNSET:
            field_dict["acme_url"] = acme_url
        if key_vault_url is not UNSET:
            field_dict["key_vault_url"] = key_vault_url
        if start_date is not UNSET:
            field_dict["start_date"] = start_date
        if end_date is not UNSET:
            field_dict["end_date"] = end_date
        if default_validity is not UNSET:
            field_dict["default_validity"] = default_validity

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id")

        ca_name = d.pop("ca_name")

        url = d.pop("url")

        def _parse_acme_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        acme_url = _parse_acme_url(d.pop("acme_url", UNSET))

        def _parse_key_vault_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        key_vault_url = _parse_key_vault_url(d.pop("key_vault_url", UNSET))

        def _parse_start_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                start_date_type_0 = isoparse(data).date()

                return start_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        start_date = _parse_start_date(d.pop("start_date", UNSET))

        def _parse_end_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                end_date_type_0 = isoparse(data).date()

                return end_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        end_date = _parse_end_date(d.pop("end_date", UNSET))

        def _parse_default_validity(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        default_validity = _parse_default_validity(d.pop("default_validity", UNSET))

        certificate_authority = cls(
            id=id,
            ca_name=ca_name,
            url=url,
            acme_url=acme_url,
            key_vault_url=key_vault_url,
            start_date=start_date,
            end_date=end_date,
            default_validity=default_validity,
        )

        certificate_authority.additional_properties = d
        return certificate_authority

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
