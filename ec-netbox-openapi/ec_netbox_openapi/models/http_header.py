import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.http_header_apply_to import HttpHeaderApplyTo
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.http_header_custom_fields import HttpHeaderCustomFields
    from ..models.nested_mapping import NestedMapping
    from ..models.nested_tag import NestedTag


T = TypeVar("T", bound="HttpHeader")


@_attrs_define
class HttpHeader:
    """HTTP Header Serializer class

    Attributes:
        id (int):
        url (str):
        name (str):
        mapping (NestedMapping): Nested Mapping Serializer class
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        value (Union[None, Unset, str]):
        apply_to (Union[Unset, HttpHeaderApplyTo]): * `request` - Request
            * `response` - Response Default: HttpHeaderApplyTo.REQUEST.
        custom_fields (Union[Unset, HttpHeaderCustomFields]):
        tags (Union[Unset, List['NestedTag']]):
    """

    id: int
    url: str
    name: str
    mapping: "NestedMapping"
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    value: Union[None, Unset, str] = UNSET
    apply_to: Union[Unset, HttpHeaderApplyTo] = HttpHeaderApplyTo.REQUEST
    custom_fields: Union[Unset, "HttpHeaderCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        name = self.name

        mapping = self.mapping.to_dict()

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        value: Union[None, Unset, str]
        if isinstance(self.value, Unset):
            value = UNSET
        else:
            value = self.value

        apply_to: Union[Unset, str] = UNSET
        if not isinstance(self.apply_to, Unset):
            apply_to = self.apply_to.value

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "name": name,
                "mapping": mapping,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if value is not UNSET:
            field_dict["value"] = value
        if apply_to is not UNSET:
            field_dict["apply_to"] = apply_to
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.http_header_custom_fields import HttpHeaderCustomFields
        from ..models.nested_mapping import NestedMapping
        from ..models.nested_tag import NestedTag

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        name = d.pop("name")

        mapping = NestedMapping.from_dict(d.pop("mapping"))

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        def _parse_value(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        value = _parse_value(d.pop("value", UNSET))

        _apply_to = d.pop("apply_to", UNSET)
        apply_to: Union[Unset, HttpHeaderApplyTo]
        if isinstance(_apply_to, Unset):
            apply_to = UNSET
        else:
            apply_to = HttpHeaderApplyTo(_apply_to)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, HttpHeaderCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = HttpHeaderCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        http_header = cls(
            id=id,
            url=url,
            name=name,
            mapping=mapping,
            created=created,
            last_updated=last_updated,
            value=value,
            apply_to=apply_to,
            custom_fields=custom_fields,
            tags=tags,
        )

        http_header.additional_properties = d
        return http_header

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
