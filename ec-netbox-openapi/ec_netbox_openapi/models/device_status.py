from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.device_status_label import DeviceStatusLabel
from ..models.device_status_value import DeviceStatusValue
from ..types import UNSET, Unset

T = TypeVar("T", bound="DeviceStatus")


@_attrs_define
class DeviceStatus:
    """
    Attributes:
        value (Union[Unset, DeviceStatusValue]): * `offline` - Offline
            * `active` - Active
            * `planned` - Planned
            * `staged` - Staged
            * `failed` - Failed
            * `inventory` - Inventory
            * `decommissioning` - Decommissioning
        label (Union[Unset, DeviceStatusLabel]):
    """

    value: Union[Unset, DeviceStatusValue] = UNSET
    label: Union[Unset, DeviceStatusLabel] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        value: Union[Unset, str] = UNSET
        if not isinstance(self.value, Unset):
            value = self.value.value

        label: Union[Unset, str] = UNSET
        if not isinstance(self.label, Unset):
            label = self.label.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if value is not UNSET:
            field_dict["value"] = value
        if label is not UNSET:
            field_dict["label"] = label

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        _value = d.pop("value", UNSET)
        value: Union[Unset, DeviceStatusValue]
        if isinstance(_value, Unset):
            value = UNSET
        else:
            value = DeviceStatusValue(_value)

        _label = d.pop("label", UNSET)
        label: Union[Unset, DeviceStatusLabel]
        if isinstance(_label, Unset):
            label = UNSET
        else:
            label = DeviceStatusLabel(_label)

        device_status = cls(
            value=value,
            label=label,
        )

        device_status.additional_properties = d
        return device_status

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
