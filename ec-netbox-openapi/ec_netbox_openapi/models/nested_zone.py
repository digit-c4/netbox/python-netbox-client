from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.nested_zone_status import NestedZoneStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_view import NestedView


T = TypeVar("T", bound="NestedZone")


@_attrs_define
class NestedZone:
    """Represents an object related through a ForeignKey field. On write, it accepts a primary key (PK) value or a
    dictionary of attributes which can be used to uniquely identify the related object. This class should be
    subclassed to return a full representation of the related object on read.

        Attributes:
            id (int):
            url (str):
            display (str):
            name (str):
            view (NestedView): Represents an object related through a ForeignKey field. On write, it accepts a primary key
                (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            active (Union[None, bool]):
            status (Union[Unset, NestedZoneStatus]): * `active` - Active
                * `reserved` - Reserved
                * `deprecated` - Deprecated
                * `parked` - Parked
    """

    id: int
    url: str
    display: str
    name: str
    view: "NestedView"
    active: Union[None, bool]
    status: Union[Unset, NestedZoneStatus] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        name = self.name

        view = self.view.to_dict()

        active: Union[None, bool]
        active = self.active

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "name": name,
                "view": view,
                "active": active,
            }
        )
        if status is not UNSET:
            field_dict["status"] = status

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_view import NestedView

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        name = d.pop("name")

        view = NestedView.from_dict(d.pop("view"))

        def _parse_active(data: object) -> Union[None, bool]:
            if data is None:
                return data
            return cast(Union[None, bool], data)

        active = _parse_active(d.pop("active"))

        _status = d.pop("status", UNSET)
        status: Union[Unset, NestedZoneStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = NestedZoneStatus(_status)

        nested_zone = cls(
            id=id,
            url=url,
            display=display,
            name=name,
            view=view,
            active=active,
            status=status,
        )

        nested_zone.additional_properties = d
        return nested_zone

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
