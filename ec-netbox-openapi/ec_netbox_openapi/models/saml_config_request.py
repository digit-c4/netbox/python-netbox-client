from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.saml_config_request_custom_fields import SamlConfigRequestCustomFields


T = TypeVar("T", bound="SamlConfigRequest")


@_attrs_define
class SamlConfigRequest:
    """SAML Config Serializer class

    Attributes:
        acs_url (str):
        logout_url (str):
        force_nauth (Union[Unset, bool]):
        custom_fields (Union[Unset, SamlConfigRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    acs_url: str
    logout_url: str
    force_nauth: Union[Unset, bool] = UNSET
    custom_fields: Union[Unset, "SamlConfigRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        acs_url = self.acs_url

        logout_url = self.logout_url

        force_nauth = self.force_nauth

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "acs_url": acs_url,
                "logout_url": logout_url,
            }
        )
        if force_nauth is not UNSET:
            field_dict["force_nauth"] = force_nauth
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.saml_config_request_custom_fields import (
            SamlConfigRequestCustomFields,
        )

        d = src_dict.copy()
        acs_url = d.pop("acs_url")

        logout_url = d.pop("logout_url")

        force_nauth = d.pop("force_nauth", UNSET)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, SamlConfigRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = SamlConfigRequestCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        saml_config_request = cls(
            acs_url=acs_url,
            logout_url=logout_url,
            force_nauth=force_nauth,
            custom_fields=custom_fields,
            tags=tags,
        )

        saml_config_request.additional_properties = d
        return saml_config_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
