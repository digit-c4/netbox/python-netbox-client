import datetime
import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.patched_certificate_request_certificate_authority import (
    PatchedCertificateRequestCertificateAuthority,
)
from ..models.patched_certificate_request_certificate_service import (
    PatchedCertificateRequestCertificateService,
)
from ..models.patched_certificate_request_expiration_time_needed import (
    PatchedCertificateRequestExpirationTimeNeeded,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_certificate_request_custom_fields import (
        PatchedCertificateRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedCertificateRequest")


@_attrs_define
class PatchedCertificateRequest:
    """Certificate Serializer class

    Attributes:
        cn (Union[Unset, str]): Unique Common Name
        alt_name (Union[List[str], None, Unset]): Alt Name is a list of host separated by commas (e.g. alt1,alt2,alt3)
        ca (Union[Unset, PatchedCertificateRequestCertificateAuthority]): * `letsencrypt` - Let's Encrypt
            * `commissign` - CommisSign
            * `globalsign` - GlobalSign
            * `other` - Other
        expiration_time (Union[Unset, PatchedCertificateRequestExpirationTimeNeeded]): * `1m` - 1 month
            * `3m` - 3 months
            * `6m` - 6 months
            * `1y` - 1 year
            * `3y` - 3 year
        cert_created_at (Union[None, Unset, datetime.date]):
        cert_expired_at (Union[None, Unset, datetime.date]):
        state (Union[Unset, str]):
        custom_fields (Union[Unset, PatchedCertificateRequestCustomFields]):
        content (Union[None, Unset, str]):
        vault_url (Union[None, Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        service (Union[None, PatchedCertificateRequestCertificateService, Unset]): * `WIFI` - WIFI
            * `RPS` - RPS
            * `RAS` - RAS
            * `LB` - LB
    """

    cn: Union[Unset, str] = UNSET
    alt_name: Union[List[str], None, Unset] = UNSET
    ca: Union[Unset, PatchedCertificateRequestCertificateAuthority] = UNSET
    expiration_time: Union[Unset, PatchedCertificateRequestExpirationTimeNeeded] = UNSET
    cert_created_at: Union[None, Unset, datetime.date] = UNSET
    cert_expired_at: Union[None, Unset, datetime.date] = UNSET
    state: Union[Unset, str] = UNSET
    custom_fields: Union[Unset, "PatchedCertificateRequestCustomFields"] = UNSET
    content: Union[None, Unset, str] = UNSET
    vault_url: Union[None, Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    service: Union[None, PatchedCertificateRequestCertificateService, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        cn = self.cn

        alt_name: Union[List[str], None, Unset]
        if isinstance(self.alt_name, Unset):
            alt_name = UNSET
        elif isinstance(self.alt_name, list):
            alt_name = self.alt_name

        else:
            alt_name = self.alt_name

        ca: Union[Unset, str] = UNSET
        if not isinstance(self.ca, Unset):
            ca = self.ca.value

        expiration_time: Union[Unset, str] = UNSET
        if not isinstance(self.expiration_time, Unset):
            expiration_time = self.expiration_time.value

        cert_created_at: Union[None, Unset, str]
        if isinstance(self.cert_created_at, Unset):
            cert_created_at = UNSET
        elif isinstance(self.cert_created_at, datetime.date):
            cert_created_at = self.cert_created_at.isoformat()
        else:
            cert_created_at = self.cert_created_at

        cert_expired_at: Union[None, Unset, str]
        if isinstance(self.cert_expired_at, Unset):
            cert_expired_at = UNSET
        elif isinstance(self.cert_expired_at, datetime.date):
            cert_expired_at = self.cert_expired_at.isoformat()
        else:
            cert_expired_at = self.cert_expired_at

        state = self.state

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        content: Union[None, Unset, str]
        if isinstance(self.content, Unset):
            content = UNSET
        else:
            content = self.content

        vault_url: Union[None, Unset, str]
        if isinstance(self.vault_url, Unset):
            vault_url = UNSET
        else:
            vault_url = self.vault_url

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        service: Union[None, Unset, str]
        if isinstance(self.service, Unset):
            service = UNSET
        elif isinstance(self.service, PatchedCertificateRequestCertificateService):
            service = self.service.value
        elif isinstance(self.service, PatchedCertificateRequestCertificateService):
            service = self.service.value
        elif isinstance(self.service, PatchedCertificateRequestCertificateService):
            service = self.service.value
        else:
            service = self.service

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if cn is not UNSET:
            field_dict["cn"] = cn
        if alt_name is not UNSET:
            field_dict["alt_name"] = alt_name
        if ca is not UNSET:
            field_dict["ca"] = ca
        if expiration_time is not UNSET:
            field_dict["expiration_time"] = expiration_time
        if cert_created_at is not UNSET:
            field_dict["cert_created_at"] = cert_created_at
        if cert_expired_at is not UNSET:
            field_dict["cert_expired_at"] = cert_expired_at
        if state is not UNSET:
            field_dict["state"] = state
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if content is not UNSET:
            field_dict["content"] = content
        if vault_url is not UNSET:
            field_dict["vault_url"] = vault_url
        if tags is not UNSET:
            field_dict["tags"] = tags
        if service is not UNSET:
            field_dict["service"] = service

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        cn = (
            self.cn
            if isinstance(self.cn, Unset)
            else (None, str(self.cn).encode(), "text/plain")
        )

        alt_name: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.alt_name, Unset):
            alt_name = UNSET
        elif isinstance(self.alt_name, list):
            _temp_alt_name = self.alt_name
            alt_name = (None, json.dumps(_temp_alt_name).encode(), "application/json")
        else:
            alt_name = (None, str(self.alt_name).encode(), "text/plain")

        ca: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.ca, Unset):
            ca = (None, str(self.ca.value).encode(), "text/plain")

        expiration_time: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.expiration_time, Unset):
            expiration_time = (
                None,
                str(self.expiration_time.value).encode(),
                "text/plain",
            )

        cert_created_at: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.cert_created_at, Unset):
            cert_created_at = UNSET
        elif isinstance(self.cert_created_at, datetime.date):
            cert_created_at = self.cert_created_at.isoformat().encode()
        else:
            cert_created_at = (None, str(self.cert_created_at).encode(), "text/plain")

        cert_expired_at: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.cert_expired_at, Unset):
            cert_expired_at = UNSET
        elif isinstance(self.cert_expired_at, datetime.date):
            cert_expired_at = self.cert_expired_at.isoformat().encode()
        else:
            cert_expired_at = (None, str(self.cert_expired_at).encode(), "text/plain")

        state = (
            self.state
            if isinstance(self.state, Unset)
            else (None, str(self.state).encode(), "text/plain")
        )

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        content: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.content, Unset):
            content = UNSET
        elif isinstance(self.content, str):
            content = (None, str(self.content).encode(), "text/plain")
        else:
            content = (None, str(self.content).encode(), "text/plain")

        vault_url: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.vault_url, Unset):
            vault_url = UNSET
        elif isinstance(self.vault_url, str):
            vault_url = (None, str(self.vault_url).encode(), "text/plain")
        else:
            vault_url = (None, str(self.vault_url).encode(), "text/plain")

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        service: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.service, Unset):
            service = UNSET
        elif isinstance(self.service, None):
            service = (None, str(self.service).encode(), "text/plain")
        elif isinstance(self.service, PatchedCertificateRequestCertificateService):
            service = (None, str(self.service.value).encode(), "text/plain")
        elif isinstance(self.service, None):
            service = (None, str(self.service).encode(), "text/plain")
        elif isinstance(self.service, PatchedCertificateRequestCertificateService):
            service = (None, str(self.service.value).encode(), "text/plain")
        elif isinstance(self.service, None):
            service = (None, str(self.service).encode(), "text/plain")
        else:
            service = (None, str(self.service.value).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if cn is not UNSET:
            field_dict["cn"] = cn
        if alt_name is not UNSET:
            field_dict["alt_name"] = alt_name
        if ca is not UNSET:
            field_dict["ca"] = ca
        if expiration_time is not UNSET:
            field_dict["expiration_time"] = expiration_time
        if cert_created_at is not UNSET:
            field_dict["cert_created_at"] = cert_created_at
        if cert_expired_at is not UNSET:
            field_dict["cert_expired_at"] = cert_expired_at
        if state is not UNSET:
            field_dict["state"] = state
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if content is not UNSET:
            field_dict["content"] = content
        if vault_url is not UNSET:
            field_dict["vault_url"] = vault_url
        if tags is not UNSET:
            field_dict["tags"] = tags
        if service is not UNSET:
            field_dict["service"] = service

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_certificate_request_custom_fields import (
            PatchedCertificateRequestCustomFields,
        )

        d = src_dict.copy()
        cn = d.pop("cn", UNSET)

        def _parse_alt_name(data: object) -> Union[List[str], None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                alt_name_type_0 = cast(List[str], data)

                return alt_name_type_0
            except:  # noqa: E722
                pass
            return cast(Union[List[str], None, Unset], data)

        alt_name = _parse_alt_name(d.pop("alt_name", UNSET))

        _ca = d.pop("ca", UNSET)
        ca: Union[Unset, PatchedCertificateRequestCertificateAuthority]
        if isinstance(_ca, Unset):
            ca = UNSET
        else:
            ca = PatchedCertificateRequestCertificateAuthority(_ca)

        _expiration_time = d.pop("expiration_time", UNSET)
        expiration_time: Union[Unset, PatchedCertificateRequestExpirationTimeNeeded]
        if isinstance(_expiration_time, Unset):
            expiration_time = UNSET
        else:
            expiration_time = PatchedCertificateRequestExpirationTimeNeeded(
                _expiration_time
            )

        def _parse_cert_created_at(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                cert_created_at_type_0 = isoparse(data).date()

                return cert_created_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        cert_created_at = _parse_cert_created_at(d.pop("cert_created_at", UNSET))

        def _parse_cert_expired_at(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                cert_expired_at_type_0 = isoparse(data).date()

                return cert_expired_at_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        cert_expired_at = _parse_cert_expired_at(d.pop("cert_expired_at", UNSET))

        state = d.pop("state", UNSET)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedCertificateRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedCertificateRequestCustomFields.from_dict(
                _custom_fields
            )

        def _parse_content(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        content = _parse_content(d.pop("content", UNSET))

        def _parse_vault_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        vault_url = _parse_vault_url(d.pop("vault_url", UNSET))

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        def _parse_service(
            data: object,
        ) -> Union[None, PatchedCertificateRequestCertificateService, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                service_type_1 = PatchedCertificateRequestCertificateService(data)

                return service_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                service_type_2_type_1 = PatchedCertificateRequestCertificateService(
                    data
                )

                return service_type_2_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                service_type_3_type_1 = PatchedCertificateRequestCertificateService(
                    data
                )

                return service_type_3_type_1
            except:  # noqa: E722
                pass
            return cast(
                Union[None, PatchedCertificateRequestCertificateService, Unset], data
            )

        service = _parse_service(d.pop("service", UNSET))

        patched_certificate_request = cls(
            cn=cn,
            alt_name=alt_name,
            ca=ca,
            expiration_time=expiration_time,
            cert_created_at=cert_created_at,
            cert_expired_at=cert_expired_at,
            state=state,
            custom_fields=custom_fields,
            content=content,
            vault_url=vault_url,
            tags=tags,
            service=service,
        )

        patched_certificate_request.additional_properties = d
        return patched_certificate_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
