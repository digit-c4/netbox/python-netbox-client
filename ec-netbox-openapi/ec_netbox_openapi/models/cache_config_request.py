from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.cache_config_request_list_extensions_item import (
    CacheConfigRequestListExtensionsItem,
)
from ..types import UNSET, Unset

T = TypeVar("T", bound="CacheConfigRequest")


@_attrs_define
class CacheConfigRequest:
    """Cache Configuration Serializer class

    Attributes:
        list_extensions (List[CacheConfigRequestListExtensionsItem]):
        ttl (Union[Unset, int]):
        max_size_limit (Union[Unset, int]):
    """

    list_extensions: List[CacheConfigRequestListExtensionsItem]
    ttl: Union[Unset, int] = UNSET
    max_size_limit: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        list_extensions = []
        for list_extensions_item_data in self.list_extensions:
            list_extensions_item = list_extensions_item_data.value
            list_extensions.append(list_extensions_item)

        ttl = self.ttl

        max_size_limit = self.max_size_limit

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "list_extensions": list_extensions,
            }
        )
        if ttl is not UNSET:
            field_dict["ttl"] = ttl
        if max_size_limit is not UNSET:
            field_dict["max_size_limit"] = max_size_limit

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        list_extensions = []
        _list_extensions = d.pop("list_extensions")
        for list_extensions_item_data in _list_extensions:
            list_extensions_item = CacheConfigRequestListExtensionsItem(
                list_extensions_item_data
            )

            list_extensions.append(list_extensions_item)

        ttl = d.pop("ttl", UNSET)

        max_size_limit = d.pop("max_size_limit", UNSET)

        cache_config_request = cls(
            list_extensions=list_extensions,
            ttl=ttl,
            max_size_limit=max_size_limit,
        )

        cache_config_request.additional_properties = d
        return cache_config_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
