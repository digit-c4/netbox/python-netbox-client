from enum import Enum


class NestedHttpHeaderRequestApplyTo(str, Enum):
    REQUEST = "request"
    RESPONSE = "response"

    def __str__(self) -> str:
        return str(self.value)
