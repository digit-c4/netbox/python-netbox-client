from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_location_request import NestedLocationRequest
    from ..models.nested_site_request import NestedSiteRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.power_panel_request_custom_fields import PowerPanelRequestCustomFields


T = TypeVar("T", bound="PowerPanelRequest")


@_attrs_define
class PowerPanelRequest:
    """Adds support for custom fields and tags.

    Attributes:
        site (NestedSiteRequest): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (str):
        location (Union['NestedLocationRequest', None, Unset]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, PowerPanelRequestCustomFields]):
    """

    site: "NestedSiteRequest"
    name: str
    location: Union["NestedLocationRequest", None, Unset] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "PowerPanelRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_location_request import NestedLocationRequest

        site = self.site.to_dict()

        name = self.name

        location: Union[Dict[str, Any], None, Unset]
        if isinstance(self.location, Unset):
            location = UNSET
        elif isinstance(self.location, NestedLocationRequest):
            location = self.location.to_dict()
        else:
            location = self.location

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "site": site,
                "name": name,
            }
        )
        if location is not UNSET:
            field_dict["location"] = location
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_location_request import NestedLocationRequest
        from ..models.nested_site_request import NestedSiteRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.power_panel_request_custom_fields import (
            PowerPanelRequestCustomFields,
        )

        d = src_dict.copy()
        site = NestedSiteRequest.from_dict(d.pop("site"))

        name = d.pop("name")

        def _parse_location(
            data: object,
        ) -> Union["NestedLocationRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                location_type_1 = NestedLocationRequest.from_dict(data)

                return location_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedLocationRequest", None, Unset], data)

        location = _parse_location(d.pop("location", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PowerPanelRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PowerPanelRequestCustomFields.from_dict(_custom_fields)

        power_panel_request = cls(
            site=site,
            name=name,
            location=location,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        power_panel_request.additional_properties = d
        return power_panel_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
