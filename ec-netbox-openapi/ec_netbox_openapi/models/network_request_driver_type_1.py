from enum import Enum


class NetworkRequestDriverType1(str, Enum):
    BRIDGE = "bridge"
    HOST = "host"
    VALUE_2 = ""

    def __str__(self) -> str:
        return str(self.value)
