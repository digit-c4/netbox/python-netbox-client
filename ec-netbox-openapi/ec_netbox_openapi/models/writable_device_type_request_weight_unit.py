from enum import Enum


class WritableDeviceTypeRequestWeightUnit(str, Enum):
    G = "g"
    KG = "kg"
    LB = "lb"
    OZ = "oz"
    VALUE_4 = ""

    def __str__(self) -> str:
        return str(self.value)
