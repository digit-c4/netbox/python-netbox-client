from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.nested_hsts_protocol_request_max_age import (
    NestedHstsProtocolRequestMaxAge,
)
from ..types import UNSET, Unset

T = TypeVar("T", bound="NestedHstsProtocolRequest")


@_attrs_define
class NestedHstsProtocolRequest:
    """Nested Hsts Protocol Serializer class

    Attributes:
        mapping (int):
        subdomains (bool):
        preload_flag (bool):
        max_age (Union[Unset, NestedHstsProtocolRequestMaxAge]): * `0` - no cache
            * `31536000` - one year
            * `63072000` - two years
    """

    mapping: int
    subdomains: bool
    preload_flag: bool
    max_age: Union[Unset, NestedHstsProtocolRequestMaxAge] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        mapping = self.mapping

        subdomains = self.subdomains

        preload_flag = self.preload_flag

        max_age: Union[Unset, int] = UNSET
        if not isinstance(self.max_age, Unset):
            max_age = self.max_age.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "mapping": mapping,
                "subdomains": subdomains,
                "preload_flag": preload_flag,
            }
        )
        if max_age is not UNSET:
            field_dict["max_age"] = max_age

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        mapping = d.pop("mapping")

        subdomains = d.pop("subdomains")

        preload_flag = d.pop("preload_flag")

        _max_age = d.pop("max_age", UNSET)
        max_age: Union[Unset, NestedHstsProtocolRequestMaxAge]
        if isinstance(_max_age, Unset):
            max_age = UNSET
        else:
            max_age = NestedHstsProtocolRequestMaxAge(_max_age)

        nested_hsts_protocol_request = cls(
            mapping=mapping,
            subdomains=subdomains,
            preload_flag=preload_flag,
            max_age=max_age,
        )

        nested_hsts_protocol_request.additional_properties = d
        return nested_hsts_protocol_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
