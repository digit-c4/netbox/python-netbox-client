from enum import Enum


class PatchedWritableMappingRequestAuth(str, Enum):
    ECAS = "ecas"
    LDAP = "ldap"
    NONE = "none"

    def __str__(self) -> str:
        return str(self.value)
