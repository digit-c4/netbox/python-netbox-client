import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_device_with_config_context_request_airflow import (
    WritableDeviceWithConfigContextRequestAirflow,
)
from ..models.writable_device_with_config_context_request_rack_face import (
    WritableDeviceWithConfigContextRequestRackFace,
)
from ..models.writable_device_with_config_context_request_status import (
    WritableDeviceWithConfigContextRequestStatus,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_device_with_config_context_request_custom_fields import (
        WritableDeviceWithConfigContextRequestCustomFields,
    )


T = TypeVar("T", bound="WritableDeviceWithConfigContextRequest")


@_attrs_define
class WritableDeviceWithConfigContextRequest:
    """Adds support for custom fields and tags.

    Attributes:
        device_type (int):
        role (int): The function this device serves
        site (int):
        name (Union[None, Unset, str]):
        tenant (Union[None, Unset, int]):
        platform (Union[None, Unset, int]):
        serial (Union[Unset, str]): Chassis serial number, assigned by the manufacturer
        asset_tag (Union[None, Unset, str]): A unique tag used to identify this device
        location (Union[None, Unset, int]):
        rack (Union[None, Unset, int]):
        position (Union[None, Unset, float]):
        face (Union[Unset, WritableDeviceWithConfigContextRequestRackFace]): * `front` - Front
            * `rear` - Rear
        latitude (Union[None, Unset, float]): GPS coordinate in decimal format (xx.yyyyyy)
        longitude (Union[None, Unset, float]): GPS coordinate in decimal format (xx.yyyyyy)
        status (Union[Unset, WritableDeviceWithConfigContextRequestStatus]): * `offline` - Offline
            * `active` - Active
            * `planned` - Planned
            * `staged` - Staged
            * `failed` - Failed
            * `inventory` - Inventory
            * `decommissioning` - Decommissioning
        airflow (Union[Unset, WritableDeviceWithConfigContextRequestAirflow]): * `front-to-rear` - Front to rear
            * `rear-to-front` - Rear to front
            * `left-to-right` - Left to right
            * `right-to-left` - Right to left
            * `side-to-rear` - Side to rear
            * `passive` - Passive
            * `mixed` - Mixed
        primary_ip4 (Union[None, Unset, int]):
        primary_ip6 (Union[None, Unset, int]):
        oob_ip (Union[None, Unset, int]):
        cluster (Union[None, Unset, int]):
        virtual_chassis (Union[None, Unset, int]):
        vc_position (Union[None, Unset, int]):
        vc_priority (Union[None, Unset, int]): Virtual chassis master election priority
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        config_template (Union[None, Unset, int]):
        local_context_data (Union[Unset, Any]): Local config context data takes precedence over source contexts in the
            final rendered config context
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritableDeviceWithConfigContextRequestCustomFields]):
    """

    device_type: int
    role: int
    site: int
    name: Union[None, Unset, str] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    platform: Union[None, Unset, int] = UNSET
    serial: Union[Unset, str] = UNSET
    asset_tag: Union[None, Unset, str] = UNSET
    location: Union[None, Unset, int] = UNSET
    rack: Union[None, Unset, int] = UNSET
    position: Union[None, Unset, float] = UNSET
    face: Union[Unset, WritableDeviceWithConfigContextRequestRackFace] = UNSET
    latitude: Union[None, Unset, float] = UNSET
    longitude: Union[None, Unset, float] = UNSET
    status: Union[Unset, WritableDeviceWithConfigContextRequestStatus] = UNSET
    airflow: Union[Unset, WritableDeviceWithConfigContextRequestAirflow] = UNSET
    primary_ip4: Union[None, Unset, int] = UNSET
    primary_ip6: Union[None, Unset, int] = UNSET
    oob_ip: Union[None, Unset, int] = UNSET
    cluster: Union[None, Unset, int] = UNSET
    virtual_chassis: Union[None, Unset, int] = UNSET
    vc_position: Union[None, Unset, int] = UNSET
    vc_priority: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    config_template: Union[None, Unset, int] = UNSET
    local_context_data: Union[Unset, Any] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[
        Unset, "WritableDeviceWithConfigContextRequestCustomFields"
    ] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        device_type = self.device_type

        role = self.role

        site = self.site

        name: Union[None, Unset, str]
        if isinstance(self.name, Unset):
            name = UNSET
        else:
            name = self.name

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        platform: Union[None, Unset, int]
        if isinstance(self.platform, Unset):
            platform = UNSET
        else:
            platform = self.platform

        serial = self.serial

        asset_tag: Union[None, Unset, str]
        if isinstance(self.asset_tag, Unset):
            asset_tag = UNSET
        else:
            asset_tag = self.asset_tag

        location: Union[None, Unset, int]
        if isinstance(self.location, Unset):
            location = UNSET
        else:
            location = self.location

        rack: Union[None, Unset, int]
        if isinstance(self.rack, Unset):
            rack = UNSET
        else:
            rack = self.rack

        position: Union[None, Unset, float]
        if isinstance(self.position, Unset):
            position = UNSET
        else:
            position = self.position

        face: Union[Unset, str] = UNSET
        if not isinstance(self.face, Unset):
            face = self.face.value

        latitude: Union[None, Unset, float]
        if isinstance(self.latitude, Unset):
            latitude = UNSET
        else:
            latitude = self.latitude

        longitude: Union[None, Unset, float]
        if isinstance(self.longitude, Unset):
            longitude = UNSET
        else:
            longitude = self.longitude

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        airflow: Union[Unset, str] = UNSET
        if not isinstance(self.airflow, Unset):
            airflow = self.airflow.value

        primary_ip4: Union[None, Unset, int]
        if isinstance(self.primary_ip4, Unset):
            primary_ip4 = UNSET
        else:
            primary_ip4 = self.primary_ip4

        primary_ip6: Union[None, Unset, int]
        if isinstance(self.primary_ip6, Unset):
            primary_ip6 = UNSET
        else:
            primary_ip6 = self.primary_ip6

        oob_ip: Union[None, Unset, int]
        if isinstance(self.oob_ip, Unset):
            oob_ip = UNSET
        else:
            oob_ip = self.oob_ip

        cluster: Union[None, Unset, int]
        if isinstance(self.cluster, Unset):
            cluster = UNSET
        else:
            cluster = self.cluster

        virtual_chassis: Union[None, Unset, int]
        if isinstance(self.virtual_chassis, Unset):
            virtual_chassis = UNSET
        else:
            virtual_chassis = self.virtual_chassis

        vc_position: Union[None, Unset, int]
        if isinstance(self.vc_position, Unset):
            vc_position = UNSET
        else:
            vc_position = self.vc_position

        vc_priority: Union[None, Unset, int]
        if isinstance(self.vc_priority, Unset):
            vc_priority = UNSET
        else:
            vc_priority = self.vc_priority

        description = self.description

        comments = self.comments

        config_template: Union[None, Unset, int]
        if isinstance(self.config_template, Unset):
            config_template = UNSET
        else:
            config_template = self.config_template

        local_context_data = self.local_context_data

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "device_type": device_type,
                "role": role,
                "site": site,
            }
        )
        if name is not UNSET:
            field_dict["name"] = name
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if platform is not UNSET:
            field_dict["platform"] = platform
        if serial is not UNSET:
            field_dict["serial"] = serial
        if asset_tag is not UNSET:
            field_dict["asset_tag"] = asset_tag
        if location is not UNSET:
            field_dict["location"] = location
        if rack is not UNSET:
            field_dict["rack"] = rack
        if position is not UNSET:
            field_dict["position"] = position
        if face is not UNSET:
            field_dict["face"] = face
        if latitude is not UNSET:
            field_dict["latitude"] = latitude
        if longitude is not UNSET:
            field_dict["longitude"] = longitude
        if status is not UNSET:
            field_dict["status"] = status
        if airflow is not UNSET:
            field_dict["airflow"] = airflow
        if primary_ip4 is not UNSET:
            field_dict["primary_ip4"] = primary_ip4
        if primary_ip6 is not UNSET:
            field_dict["primary_ip6"] = primary_ip6
        if oob_ip is not UNSET:
            field_dict["oob_ip"] = oob_ip
        if cluster is not UNSET:
            field_dict["cluster"] = cluster
        if virtual_chassis is not UNSET:
            field_dict["virtual_chassis"] = virtual_chassis
        if vc_position is not UNSET:
            field_dict["vc_position"] = vc_position
        if vc_priority is not UNSET:
            field_dict["vc_priority"] = vc_priority
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if config_template is not UNSET:
            field_dict["config_template"] = config_template
        if local_context_data is not UNSET:
            field_dict["local_context_data"] = local_context_data
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        device_type = (None, str(self.device_type).encode(), "text/plain")

        role = (None, str(self.role).encode(), "text/plain")

        site = (None, str(self.site).encode(), "text/plain")

        name: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.name, Unset):
            name = UNSET
        elif isinstance(self.name, str):
            name = (None, str(self.name).encode(), "text/plain")
        else:
            name = (None, str(self.name).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        platform: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.platform, Unset):
            platform = UNSET
        elif isinstance(self.platform, int):
            platform = (None, str(self.platform).encode(), "text/plain")
        else:
            platform = (None, str(self.platform).encode(), "text/plain")

        serial = (
            self.serial
            if isinstance(self.serial, Unset)
            else (None, str(self.serial).encode(), "text/plain")
        )

        asset_tag: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.asset_tag, Unset):
            asset_tag = UNSET
        elif isinstance(self.asset_tag, str):
            asset_tag = (None, str(self.asset_tag).encode(), "text/plain")
        else:
            asset_tag = (None, str(self.asset_tag).encode(), "text/plain")

        location: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.location, Unset):
            location = UNSET
        elif isinstance(self.location, int):
            location = (None, str(self.location).encode(), "text/plain")
        else:
            location = (None, str(self.location).encode(), "text/plain")

        rack: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.rack, Unset):
            rack = UNSET
        elif isinstance(self.rack, int):
            rack = (None, str(self.rack).encode(), "text/plain")
        else:
            rack = (None, str(self.rack).encode(), "text/plain")

        position: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.position, Unset):
            position = UNSET
        elif isinstance(self.position, float):
            position = (None, str(self.position).encode(), "text/plain")
        else:
            position = (None, str(self.position).encode(), "text/plain")

        face: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.face, Unset):
            face = (None, str(self.face.value).encode(), "text/plain")

        latitude: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.latitude, Unset):
            latitude = UNSET
        elif isinstance(self.latitude, float):
            latitude = (None, str(self.latitude).encode(), "text/plain")
        else:
            latitude = (None, str(self.latitude).encode(), "text/plain")

        longitude: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.longitude, Unset):
            longitude = UNSET
        elif isinstance(self.longitude, float):
            longitude = (None, str(self.longitude).encode(), "text/plain")
        else:
            longitude = (None, str(self.longitude).encode(), "text/plain")

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        airflow: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.airflow, Unset):
            airflow = (None, str(self.airflow.value).encode(), "text/plain")

        primary_ip4: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.primary_ip4, Unset):
            primary_ip4 = UNSET
        elif isinstance(self.primary_ip4, int):
            primary_ip4 = (None, str(self.primary_ip4).encode(), "text/plain")
        else:
            primary_ip4 = (None, str(self.primary_ip4).encode(), "text/plain")

        primary_ip6: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.primary_ip6, Unset):
            primary_ip6 = UNSET
        elif isinstance(self.primary_ip6, int):
            primary_ip6 = (None, str(self.primary_ip6).encode(), "text/plain")
        else:
            primary_ip6 = (None, str(self.primary_ip6).encode(), "text/plain")

        oob_ip: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.oob_ip, Unset):
            oob_ip = UNSET
        elif isinstance(self.oob_ip, int):
            oob_ip = (None, str(self.oob_ip).encode(), "text/plain")
        else:
            oob_ip = (None, str(self.oob_ip).encode(), "text/plain")

        cluster: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.cluster, Unset):
            cluster = UNSET
        elif isinstance(self.cluster, int):
            cluster = (None, str(self.cluster).encode(), "text/plain")
        else:
            cluster = (None, str(self.cluster).encode(), "text/plain")

        virtual_chassis: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.virtual_chassis, Unset):
            virtual_chassis = UNSET
        elif isinstance(self.virtual_chassis, int):
            virtual_chassis = (None, str(self.virtual_chassis).encode(), "text/plain")
        else:
            virtual_chassis = (None, str(self.virtual_chassis).encode(), "text/plain")

        vc_position: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.vc_position, Unset):
            vc_position = UNSET
        elif isinstance(self.vc_position, int):
            vc_position = (None, str(self.vc_position).encode(), "text/plain")
        else:
            vc_position = (None, str(self.vc_position).encode(), "text/plain")

        vc_priority: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.vc_priority, Unset):
            vc_priority = UNSET
        elif isinstance(self.vc_priority, int):
            vc_priority = (None, str(self.vc_priority).encode(), "text/plain")
        else:
            vc_priority = (None, str(self.vc_priority).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        config_template: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.config_template, Unset):
            config_template = UNSET
        elif isinstance(self.config_template, int):
            config_template = (None, str(self.config_template).encode(), "text/plain")
        else:
            config_template = (None, str(self.config_template).encode(), "text/plain")

        local_context_data = (
            self.local_context_data
            if isinstance(self.local_context_data, Unset)
            else (None, str(self.local_context_data).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "device_type": device_type,
                "role": role,
                "site": site,
            }
        )
        if name is not UNSET:
            field_dict["name"] = name
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if platform is not UNSET:
            field_dict["platform"] = platform
        if serial is not UNSET:
            field_dict["serial"] = serial
        if asset_tag is not UNSET:
            field_dict["asset_tag"] = asset_tag
        if location is not UNSET:
            field_dict["location"] = location
        if rack is not UNSET:
            field_dict["rack"] = rack
        if position is not UNSET:
            field_dict["position"] = position
        if face is not UNSET:
            field_dict["face"] = face
        if latitude is not UNSET:
            field_dict["latitude"] = latitude
        if longitude is not UNSET:
            field_dict["longitude"] = longitude
        if status is not UNSET:
            field_dict["status"] = status
        if airflow is not UNSET:
            field_dict["airflow"] = airflow
        if primary_ip4 is not UNSET:
            field_dict["primary_ip4"] = primary_ip4
        if primary_ip6 is not UNSET:
            field_dict["primary_ip6"] = primary_ip6
        if oob_ip is not UNSET:
            field_dict["oob_ip"] = oob_ip
        if cluster is not UNSET:
            field_dict["cluster"] = cluster
        if virtual_chassis is not UNSET:
            field_dict["virtual_chassis"] = virtual_chassis
        if vc_position is not UNSET:
            field_dict["vc_position"] = vc_position
        if vc_priority is not UNSET:
            field_dict["vc_priority"] = vc_priority
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if config_template is not UNSET:
            field_dict["config_template"] = config_template
        if local_context_data is not UNSET:
            field_dict["local_context_data"] = local_context_data
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_device_with_config_context_request_custom_fields import (
            WritableDeviceWithConfigContextRequestCustomFields,
        )

        d = src_dict.copy()
        device_type = d.pop("device_type")

        role = d.pop("role")

        site = d.pop("site")

        def _parse_name(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        name = _parse_name(d.pop("name", UNSET))

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        def _parse_platform(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        platform = _parse_platform(d.pop("platform", UNSET))

        serial = d.pop("serial", UNSET)

        def _parse_asset_tag(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        asset_tag = _parse_asset_tag(d.pop("asset_tag", UNSET))

        def _parse_location(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        location = _parse_location(d.pop("location", UNSET))

        def _parse_rack(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        rack = _parse_rack(d.pop("rack", UNSET))

        def _parse_position(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        position = _parse_position(d.pop("position", UNSET))

        _face = d.pop("face", UNSET)
        face: Union[Unset, WritableDeviceWithConfigContextRequestRackFace]
        if isinstance(_face, Unset):
            face = UNSET
        else:
            face = WritableDeviceWithConfigContextRequestRackFace(_face)

        def _parse_latitude(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        latitude = _parse_latitude(d.pop("latitude", UNSET))

        def _parse_longitude(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        longitude = _parse_longitude(d.pop("longitude", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, WritableDeviceWithConfigContextRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = WritableDeviceWithConfigContextRequestStatus(_status)

        _airflow = d.pop("airflow", UNSET)
        airflow: Union[Unset, WritableDeviceWithConfigContextRequestAirflow]
        if isinstance(_airflow, Unset):
            airflow = UNSET
        else:
            airflow = WritableDeviceWithConfigContextRequestAirflow(_airflow)

        def _parse_primary_ip4(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        primary_ip4 = _parse_primary_ip4(d.pop("primary_ip4", UNSET))

        def _parse_primary_ip6(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        primary_ip6 = _parse_primary_ip6(d.pop("primary_ip6", UNSET))

        def _parse_oob_ip(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        oob_ip = _parse_oob_ip(d.pop("oob_ip", UNSET))

        def _parse_cluster(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        cluster = _parse_cluster(d.pop("cluster", UNSET))

        def _parse_virtual_chassis(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        virtual_chassis = _parse_virtual_chassis(d.pop("virtual_chassis", UNSET))

        def _parse_vc_position(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vc_position = _parse_vc_position(d.pop("vc_position", UNSET))

        def _parse_vc_priority(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vc_priority = _parse_vc_priority(d.pop("vc_priority", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        def _parse_config_template(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        config_template = _parse_config_template(d.pop("config_template", UNSET))

        local_context_data = d.pop("local_context_data", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableDeviceWithConfigContextRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = (
                WritableDeviceWithConfigContextRequestCustomFields.from_dict(
                    _custom_fields
                )
            )

        writable_device_with_config_context_request = cls(
            device_type=device_type,
            role=role,
            site=site,
            name=name,
            tenant=tenant,
            platform=platform,
            serial=serial,
            asset_tag=asset_tag,
            location=location,
            rack=rack,
            position=position,
            face=face,
            latitude=latitude,
            longitude=longitude,
            status=status,
            airflow=airflow,
            primary_ip4=primary_ip4,
            primary_ip6=primary_ip6,
            oob_ip=oob_ip,
            cluster=cluster,
            virtual_chassis=virtual_chassis,
            vc_position=vc_position,
            vc_priority=vc_priority,
            description=description,
            comments=comments,
            config_template=config_template,
            local_context_data=local_context_data,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_device_with_config_context_request.additional_properties = d
        return writable_device_with_config_context_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
