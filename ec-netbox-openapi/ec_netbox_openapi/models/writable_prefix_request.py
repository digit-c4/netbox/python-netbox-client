import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_prefix_request_status import WritablePrefixRequestStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_prefix_request_custom_fields import (
        WritablePrefixRequestCustomFields,
    )


T = TypeVar("T", bound="WritablePrefixRequest")


@_attrs_define
class WritablePrefixRequest:
    """Adds support for custom fields and tags.

    Attributes:
        prefix (str):
        site (Union[None, Unset, int]):
        vrf (Union[None, Unset, int]):
        tenant (Union[None, Unset, int]):
        vlan (Union[None, Unset, int]):
        status (Union[Unset, WritablePrefixRequestStatus]): Operational status of this prefix

            * `container` - Container
            * `active` - Active
            * `reserved` - Reserved
            * `deprecated` - Deprecated
        role (Union[None, Unset, int]): The primary function of this prefix
        is_pool (Union[Unset, bool]): All IP addresses within this prefix are considered usable
        mark_utilized (Union[Unset, bool]): Treat as 100% utilized
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritablePrefixRequestCustomFields]):
    """

    prefix: str
    site: Union[None, Unset, int] = UNSET
    vrf: Union[None, Unset, int] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    vlan: Union[None, Unset, int] = UNSET
    status: Union[Unset, WritablePrefixRequestStatus] = UNSET
    role: Union[None, Unset, int] = UNSET
    is_pool: Union[Unset, bool] = UNSET
    mark_utilized: Union[Unset, bool] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WritablePrefixRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        prefix = self.prefix

        site: Union[None, Unset, int]
        if isinstance(self.site, Unset):
            site = UNSET
        else:
            site = self.site

        vrf: Union[None, Unset, int]
        if isinstance(self.vrf, Unset):
            vrf = UNSET
        else:
            vrf = self.vrf

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        vlan: Union[None, Unset, int]
        if isinstance(self.vlan, Unset):
            vlan = UNSET
        else:
            vlan = self.vlan

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        role: Union[None, Unset, int]
        if isinstance(self.role, Unset):
            role = UNSET
        else:
            role = self.role

        is_pool = self.is_pool

        mark_utilized = self.mark_utilized

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "prefix": prefix,
            }
        )
        if site is not UNSET:
            field_dict["site"] = site
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if vlan is not UNSET:
            field_dict["vlan"] = vlan
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if is_pool is not UNSET:
            field_dict["is_pool"] = is_pool
        if mark_utilized is not UNSET:
            field_dict["mark_utilized"] = mark_utilized
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        prefix = (None, str(self.prefix).encode(), "text/plain")

        site: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.site, Unset):
            site = UNSET
        elif isinstance(self.site, int):
            site = (None, str(self.site).encode(), "text/plain")
        else:
            site = (None, str(self.site).encode(), "text/plain")

        vrf: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.vrf, Unset):
            vrf = UNSET
        elif isinstance(self.vrf, int):
            vrf = (None, str(self.vrf).encode(), "text/plain")
        else:
            vrf = (None, str(self.vrf).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        vlan: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.vlan, Unset):
            vlan = UNSET
        elif isinstance(self.vlan, int):
            vlan = (None, str(self.vlan).encode(), "text/plain")
        else:
            vlan = (None, str(self.vlan).encode(), "text/plain")

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        role: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.role, Unset):
            role = UNSET
        elif isinstance(self.role, int):
            role = (None, str(self.role).encode(), "text/plain")
        else:
            role = (None, str(self.role).encode(), "text/plain")

        is_pool = (
            self.is_pool
            if isinstance(self.is_pool, Unset)
            else (None, str(self.is_pool).encode(), "text/plain")
        )

        mark_utilized = (
            self.mark_utilized
            if isinstance(self.mark_utilized, Unset)
            else (None, str(self.mark_utilized).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "prefix": prefix,
            }
        )
        if site is not UNSET:
            field_dict["site"] = site
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if vlan is not UNSET:
            field_dict["vlan"] = vlan
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if is_pool is not UNSET:
            field_dict["is_pool"] = is_pool
        if mark_utilized is not UNSET:
            field_dict["mark_utilized"] = mark_utilized
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_prefix_request_custom_fields import (
            WritablePrefixRequestCustomFields,
        )

        d = src_dict.copy()
        prefix = d.pop("prefix")

        def _parse_site(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        site = _parse_site(d.pop("site", UNSET))

        def _parse_vrf(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vrf = _parse_vrf(d.pop("vrf", UNSET))

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        def _parse_vlan(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vlan = _parse_vlan(d.pop("vlan", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, WritablePrefixRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = WritablePrefixRequestStatus(_status)

        def _parse_role(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        role = _parse_role(d.pop("role", UNSET))

        is_pool = d.pop("is_pool", UNSET)

        mark_utilized = d.pop("mark_utilized", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritablePrefixRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritablePrefixRequestCustomFields.from_dict(_custom_fields)

        writable_prefix_request = cls(
            prefix=prefix,
            site=site,
            vrf=vrf,
            tenant=tenant,
            vlan=vlan,
            status=status,
            role=role,
            is_pool=is_pool,
            mark_utilized=mark_utilized,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_prefix_request.additional_properties = d
        return writable_prefix_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
