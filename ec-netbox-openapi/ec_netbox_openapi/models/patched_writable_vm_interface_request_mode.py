from enum import Enum


class PatchedWritableVMInterfaceRequestMode(str, Enum):
    ACCESS = "access"
    TAGGED = "tagged"
    TAGGED_ALL = "tagged-all"
    VALUE_3 = ""

    def __str__(self) -> str:
        return str(self.value)
