from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.http_header_request_apply_to import HttpHeaderRequestApplyTo
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.http_header_request_custom_fields import HttpHeaderRequestCustomFields
    from ..models.nested_tag_request import NestedTagRequest


T = TypeVar("T", bound="HttpHeaderRequest")


@_attrs_define
class HttpHeaderRequest:
    """HTTP Header Serializer class

    Attributes:
        name (str):
        value (Union[None, Unset, str]):
        apply_to (Union[Unset, HttpHeaderRequestApplyTo]): * `request` - Request
            * `response` - Response Default: HttpHeaderRequestApplyTo.REQUEST.
        custom_fields (Union[Unset, HttpHeaderRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    name: str
    value: Union[None, Unset, str] = UNSET
    apply_to: Union[Unset, HttpHeaderRequestApplyTo] = HttpHeaderRequestApplyTo.REQUEST
    custom_fields: Union[Unset, "HttpHeaderRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        value: Union[None, Unset, str]
        if isinstance(self.value, Unset):
            value = UNSET
        else:
            value = self.value

        apply_to: Union[Unset, str] = UNSET
        if not isinstance(self.apply_to, Unset):
            apply_to = self.apply_to.value

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
            }
        )
        if value is not UNSET:
            field_dict["value"] = value
        if apply_to is not UNSET:
            field_dict["apply_to"] = apply_to
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.http_header_request_custom_fields import (
            HttpHeaderRequestCustomFields,
        )
        from ..models.nested_tag_request import NestedTagRequest

        d = src_dict.copy()
        name = d.pop("name")

        def _parse_value(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        value = _parse_value(d.pop("value", UNSET))

        _apply_to = d.pop("apply_to", UNSET)
        apply_to: Union[Unset, HttpHeaderRequestApplyTo]
        if isinstance(_apply_to, Unset):
            apply_to = UNSET
        else:
            apply_to = HttpHeaderRequestApplyTo(_apply_to)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, HttpHeaderRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = HttpHeaderRequestCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        http_header_request = cls(
            name=name,
            value=value,
            apply_to=apply_to,
            custom_fields=custom_fields,
            tags=tags,
        )

        http_header_request.additional_properties = d
        return http_header_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
