from enum import Enum


class WritableCustomFieldRequestUiVisibility(str, Enum):
    HIDDEN = "hidden"
    HIDDEN_IFUNSET = "hidden-ifunset"
    READ_ONLY = "read-only"
    READ_WRITE = "read-write"

    def __str__(self) -> str:
        return str(self.value)
