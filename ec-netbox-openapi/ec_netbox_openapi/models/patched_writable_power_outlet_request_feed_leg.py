from enum import Enum


class PatchedWritablePowerOutletRequestFeedLeg(str, Enum):
    A = "A"
    B = "B"
    C = "C"
    VALUE_3 = ""

    def __str__(self) -> str:
        return str(self.value)
