from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

if TYPE_CHECKING:
    from ..models.nested_fhrp_group_request import NestedFHRPGroupRequest


T = TypeVar("T", bound="FHRPGroupAssignmentRequest")


@_attrs_define
class FHRPGroupAssignmentRequest:
    """Adds support for custom fields and tags.

    Attributes:
        group (NestedFHRPGroupRequest): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        interface_type (str):
        interface_id (int):
        priority (int):
    """

    group: "NestedFHRPGroupRequest"
    interface_type: str
    interface_id: int
    priority: int
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        group = self.group.to_dict()

        interface_type = self.interface_type

        interface_id = self.interface_id

        priority = self.priority

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "group": group,
                "interface_type": interface_type,
                "interface_id": interface_id,
                "priority": priority,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_fhrp_group_request import NestedFHRPGroupRequest

        d = src_dict.copy()
        group = NestedFHRPGroupRequest.from_dict(d.pop("group"))

        interface_type = d.pop("interface_type")

        interface_id = d.pop("interface_id")

        priority = d.pop("priority")

        fhrp_group_assignment_request = cls(
            group=group,
            interface_type=interface_type,
            interface_id=interface_id,
            priority=priority,
        )

        fhrp_group_assignment_request.additional_properties = d
        return fhrp_group_assignment_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
