import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.component_nested_module import ComponentNestedModule
    from ..models.nested_cable import NestedCable
    from ..models.nested_device import NestedDevice
    from ..models.nested_power_port import NestedPowerPort
    from ..models.nested_tag import NestedTag
    from ..models.power_outlet_custom_fields import PowerOutletCustomFields
    from ..models.power_outlet_feed_leg_type_0 import PowerOutletFeedLegType0
    from ..models.power_outlet_type_type_0 import PowerOutletTypeType0


T = TypeVar("T", bound="PowerOutlet")


@_attrs_define
class PowerOutlet:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        device (NestedDevice): Represents an object related through a ForeignKey field. On write, it accepts a primary
            key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (str):
        cable (Union['NestedCable', None]):
        cable_end (str):
        link_peers (List[Any]):
        link_peers_type (str): Return the type of the peer link terminations, or None.
        connected_endpoints (List[Any]):
        connected_endpoints_type (str):
        connected_endpoints_reachable (bool):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        field_occupied (bool):
        module (Union['ComponentNestedModule', None, Unset]):
        label (Union[Unset, str]): Physical label
        type (Union['PowerOutletTypeType0', None, Unset]):
        power_port (Union['NestedPowerPort', None, Unset]):
        feed_leg (Union['PowerOutletFeedLegType0', None, Unset]):
        description (Union[Unset, str]):
        mark_connected (Union[Unset, bool]): Treat as if a cable is connected
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, PowerOutletCustomFields]):
    """

    id: int
    url: str
    display: str
    device: "NestedDevice"
    name: str
    cable: Union["NestedCable", None]
    cable_end: str
    link_peers: List[Any]
    link_peers_type: str
    connected_endpoints: List[Any]
    connected_endpoints_type: str
    connected_endpoints_reachable: bool
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    field_occupied: bool
    module: Union["ComponentNestedModule", None, Unset] = UNSET
    label: Union[Unset, str] = UNSET
    type: Union["PowerOutletTypeType0", None, Unset] = UNSET
    power_port: Union["NestedPowerPort", None, Unset] = UNSET
    feed_leg: Union["PowerOutletFeedLegType0", None, Unset] = UNSET
    description: Union[Unset, str] = UNSET
    mark_connected: Union[Unset, bool] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "PowerOutletCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.component_nested_module import ComponentNestedModule
        from ..models.nested_cable import NestedCable
        from ..models.nested_power_port import NestedPowerPort
        from ..models.power_outlet_feed_leg_type_0 import PowerOutletFeedLegType0
        from ..models.power_outlet_type_type_0 import PowerOutletTypeType0

        id = self.id

        url = self.url

        display = self.display

        device = self.device.to_dict()

        name = self.name

        cable: Union[Dict[str, Any], None]
        if isinstance(self.cable, NestedCable):
            cable = self.cable.to_dict()
        else:
            cable = self.cable

        cable_end = self.cable_end

        link_peers = self.link_peers

        link_peers_type = self.link_peers_type

        connected_endpoints = self.connected_endpoints

        connected_endpoints_type = self.connected_endpoints_type

        connected_endpoints_reachable = self.connected_endpoints_reachable

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        field_occupied = self.field_occupied

        module: Union[Dict[str, Any], None, Unset]
        if isinstance(self.module, Unset):
            module = UNSET
        elif isinstance(self.module, ComponentNestedModule):
            module = self.module.to_dict()
        else:
            module = self.module

        label = self.label

        type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.type, Unset):
            type = UNSET
        elif isinstance(self.type, PowerOutletTypeType0):
            type = self.type.to_dict()
        else:
            type = self.type

        power_port: Union[Dict[str, Any], None, Unset]
        if isinstance(self.power_port, Unset):
            power_port = UNSET
        elif isinstance(self.power_port, NestedPowerPort):
            power_port = self.power_port.to_dict()
        else:
            power_port = self.power_port

        feed_leg: Union[Dict[str, Any], None, Unset]
        if isinstance(self.feed_leg, Unset):
            feed_leg = UNSET
        elif isinstance(self.feed_leg, PowerOutletFeedLegType0):
            feed_leg = self.feed_leg.to_dict()
        else:
            feed_leg = self.feed_leg

        description = self.description

        mark_connected = self.mark_connected

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "device": device,
                "name": name,
                "cable": cable,
                "cable_end": cable_end,
                "link_peers": link_peers,
                "link_peers_type": link_peers_type,
                "connected_endpoints": connected_endpoints,
                "connected_endpoints_type": connected_endpoints_type,
                "connected_endpoints_reachable": connected_endpoints_reachable,
                "created": created,
                "last_updated": last_updated,
                "_occupied": field_occupied,
            }
        )
        if module is not UNSET:
            field_dict["module"] = module
        if label is not UNSET:
            field_dict["label"] = label
        if type is not UNSET:
            field_dict["type"] = type
        if power_port is not UNSET:
            field_dict["power_port"] = power_port
        if feed_leg is not UNSET:
            field_dict["feed_leg"] = feed_leg
        if description is not UNSET:
            field_dict["description"] = description
        if mark_connected is not UNSET:
            field_dict["mark_connected"] = mark_connected
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.component_nested_module import ComponentNestedModule
        from ..models.nested_cable import NestedCable
        from ..models.nested_device import NestedDevice
        from ..models.nested_power_port import NestedPowerPort
        from ..models.nested_tag import NestedTag
        from ..models.power_outlet_custom_fields import PowerOutletCustomFields
        from ..models.power_outlet_feed_leg_type_0 import PowerOutletFeedLegType0
        from ..models.power_outlet_type_type_0 import PowerOutletTypeType0

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        device = NestedDevice.from_dict(d.pop("device"))

        name = d.pop("name")

        def _parse_cable(data: object) -> Union["NestedCable", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                cable_type_1 = NestedCable.from_dict(data)

                return cable_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedCable", None], data)

        cable = _parse_cable(d.pop("cable"))

        cable_end = d.pop("cable_end")

        link_peers = cast(List[Any], d.pop("link_peers"))

        link_peers_type = d.pop("link_peers_type")

        connected_endpoints = cast(List[Any], d.pop("connected_endpoints"))

        connected_endpoints_type = d.pop("connected_endpoints_type")

        connected_endpoints_reachable = d.pop("connected_endpoints_reachable")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        field_occupied = d.pop("_occupied")

        def _parse_module(data: object) -> Union["ComponentNestedModule", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                module_type_1 = ComponentNestedModule.from_dict(data)

                return module_type_1
            except:  # noqa: E722
                pass
            return cast(Union["ComponentNestedModule", None, Unset], data)

        module = _parse_module(d.pop("module", UNSET))

        label = d.pop("label", UNSET)

        def _parse_type(data: object) -> Union["PowerOutletTypeType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                type_type_0 = PowerOutletTypeType0.from_dict(data)

                return type_type_0
            except:  # noqa: E722
                pass
            return cast(Union["PowerOutletTypeType0", None, Unset], data)

        type = _parse_type(d.pop("type", UNSET))

        def _parse_power_port(data: object) -> Union["NestedPowerPort", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                power_port_type_1 = NestedPowerPort.from_dict(data)

                return power_port_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedPowerPort", None, Unset], data)

        power_port = _parse_power_port(d.pop("power_port", UNSET))

        def _parse_feed_leg(
            data: object,
        ) -> Union["PowerOutletFeedLegType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                feed_leg_type_0 = PowerOutletFeedLegType0.from_dict(data)

                return feed_leg_type_0
            except:  # noqa: E722
                pass
            return cast(Union["PowerOutletFeedLegType0", None, Unset], data)

        feed_leg = _parse_feed_leg(d.pop("feed_leg", UNSET))

        description = d.pop("description", UNSET)

        mark_connected = d.pop("mark_connected", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PowerOutletCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PowerOutletCustomFields.from_dict(_custom_fields)

        power_outlet = cls(
            id=id,
            url=url,
            display=display,
            device=device,
            name=name,
            cable=cable,
            cable_end=cable_end,
            link_peers=link_peers,
            link_peers_type=link_peers_type,
            connected_endpoints=connected_endpoints,
            connected_endpoints_type=connected_endpoints_type,
            connected_endpoints_reachable=connected_endpoints_reachable,
            created=created,
            last_updated=last_updated,
            field_occupied=field_occupied,
            module=module,
            label=label,
            type=type,
            power_port=power_port,
            feed_leg=feed_leg,
            description=description,
            mark_connected=mark_connected,
            tags=tags,
            custom_fields=custom_fields,
        )

        power_outlet.additional_properties = d
        return power_outlet

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
