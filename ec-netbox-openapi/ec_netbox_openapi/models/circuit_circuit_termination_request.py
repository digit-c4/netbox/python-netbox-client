from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_provider_network_request import NestedProviderNetworkRequest
    from ..models.nested_site_request import NestedSiteRequest


T = TypeVar("T", bound="CircuitCircuitTerminationRequest")


@_attrs_define
class CircuitCircuitTerminationRequest:
    """Represents an object related through a ForeignKey field. On write, it accepts a primary key (PK) value or a
    dictionary of attributes which can be used to uniquely identify the related object. This class should be
    subclassed to return a full representation of the related object on read.

        Attributes:
            site (Union['NestedSiteRequest', None]):
            provider_network (Union['NestedProviderNetworkRequest', None]):
            port_speed (Union[None, Unset, int]): Physical circuit speed
            upstream_speed (Union[None, Unset, int]): Upstream speed, if different from port speed
            xconnect_id (Union[Unset, str]): ID of the local cross-connect
            description (Union[Unset, str]):
    """

    site: Union["NestedSiteRequest", None]
    provider_network: Union["NestedProviderNetworkRequest", None]
    port_speed: Union[None, Unset, int] = UNSET
    upstream_speed: Union[None, Unset, int] = UNSET
    xconnect_id: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_provider_network_request import (
            NestedProviderNetworkRequest,
        )
        from ..models.nested_site_request import NestedSiteRequest

        site: Union[Dict[str, Any], None]
        if isinstance(self.site, NestedSiteRequest):
            site = self.site.to_dict()
        else:
            site = self.site

        provider_network: Union[Dict[str, Any], None]
        if isinstance(self.provider_network, NestedProviderNetworkRequest):
            provider_network = self.provider_network.to_dict()
        else:
            provider_network = self.provider_network

        port_speed: Union[None, Unset, int]
        if isinstance(self.port_speed, Unset):
            port_speed = UNSET
        else:
            port_speed = self.port_speed

        upstream_speed: Union[None, Unset, int]
        if isinstance(self.upstream_speed, Unset):
            upstream_speed = UNSET
        else:
            upstream_speed = self.upstream_speed

        xconnect_id = self.xconnect_id

        description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "site": site,
                "provider_network": provider_network,
            }
        )
        if port_speed is not UNSET:
            field_dict["port_speed"] = port_speed
        if upstream_speed is not UNSET:
            field_dict["upstream_speed"] = upstream_speed
        if xconnect_id is not UNSET:
            field_dict["xconnect_id"] = xconnect_id
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_provider_network_request import (
            NestedProviderNetworkRequest,
        )
        from ..models.nested_site_request import NestedSiteRequest

        d = src_dict.copy()

        def _parse_site(data: object) -> Union["NestedSiteRequest", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                site_type_1 = NestedSiteRequest.from_dict(data)

                return site_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedSiteRequest", None], data)

        site = _parse_site(d.pop("site"))

        def _parse_provider_network(
            data: object,
        ) -> Union["NestedProviderNetworkRequest", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                provider_network_type_1 = NestedProviderNetworkRequest.from_dict(data)

                return provider_network_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedProviderNetworkRequest", None], data)

        provider_network = _parse_provider_network(d.pop("provider_network"))

        def _parse_port_speed(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        port_speed = _parse_port_speed(d.pop("port_speed", UNSET))

        def _parse_upstream_speed(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        upstream_speed = _parse_upstream_speed(d.pop("upstream_speed", UNSET))

        xconnect_id = d.pop("xconnect_id", UNSET)

        description = d.pop("description", UNSET)

        circuit_circuit_termination_request = cls(
            site=site,
            provider_network=provider_network,
            port_speed=port_speed,
            upstream_speed=upstream_speed,
            xconnect_id=xconnect_id,
            description=description,
        )

        circuit_circuit_termination_request.additional_properties = d
        return circuit_circuit_termination_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
