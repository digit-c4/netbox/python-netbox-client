import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_location import NestedLocation
    from ..models.nested_rack_role import NestedRackRole
    from ..models.nested_site import NestedSite
    from ..models.nested_tag import NestedTag
    from ..models.nested_tenant import NestedTenant
    from ..models.rack_custom_fields import RackCustomFields
    from ..models.rack_outer_unit_type_0 import RackOuterUnitType0
    from ..models.rack_status import RackStatus
    from ..models.rack_type_type_0 import RackTypeType0
    from ..models.rack_weight_unit_type_0 import RackWeightUnitType0
    from ..models.rack_width import RackWidth


T = TypeVar("T", bound="Rack")


@_attrs_define
class Rack:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        name (str):
        site (NestedSite): Represents an object related through a ForeignKey field. On write, it accepts a primary key
            (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        device_count (int):
        powerfeed_count (int):
        facility_id (Union[None, Unset, str]):
        location (Union['NestedLocation', None, Unset]):
        tenant (Union['NestedTenant', None, Unset]):
        status (Union[Unset, RackStatus]):
        role (Union['NestedRackRole', None, Unset]):
        serial (Union[Unset, str]):
        asset_tag (Union[None, Unset, str]): A unique tag used to identify this rack
        type (Union['RackTypeType0', None, Unset]):
        width (Union[Unset, RackWidth]):
        u_height (Union[Unset, int]): Height in rack units
        starting_unit (Union[Unset, int]): Starting unit for rack
        weight (Union[None, Unset, float]):
        max_weight (Union[None, Unset, int]): Maximum load capacity for the rack
        weight_unit (Union['RackWeightUnitType0', None, Unset]):
        desc_units (Union[Unset, bool]): Units are numbered top-to-bottom
        outer_width (Union[None, Unset, int]): Outer dimension of rack (width)
        outer_depth (Union[None, Unset, int]): Outer dimension of rack (depth)
        outer_unit (Union['RackOuterUnitType0', None, Unset]):
        mounting_depth (Union[None, Unset, int]): Maximum depth of a mounted device, in millimeters. For four-post
            racks, this is the distance between the front and rear rails.
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, RackCustomFields]):
    """

    id: int
    url: str
    display: str
    name: str
    site: "NestedSite"
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    device_count: int
    powerfeed_count: int
    facility_id: Union[None, Unset, str] = UNSET
    location: Union["NestedLocation", None, Unset] = UNSET
    tenant: Union["NestedTenant", None, Unset] = UNSET
    status: Union[Unset, "RackStatus"] = UNSET
    role: Union["NestedRackRole", None, Unset] = UNSET
    serial: Union[Unset, str] = UNSET
    asset_tag: Union[None, Unset, str] = UNSET
    type: Union["RackTypeType0", None, Unset] = UNSET
    width: Union[Unset, "RackWidth"] = UNSET
    u_height: Union[Unset, int] = UNSET
    starting_unit: Union[Unset, int] = UNSET
    weight: Union[None, Unset, float] = UNSET
    max_weight: Union[None, Unset, int] = UNSET
    weight_unit: Union["RackWeightUnitType0", None, Unset] = UNSET
    desc_units: Union[Unset, bool] = UNSET
    outer_width: Union[None, Unset, int] = UNSET
    outer_depth: Union[None, Unset, int] = UNSET
    outer_unit: Union["RackOuterUnitType0", None, Unset] = UNSET
    mounting_depth: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "RackCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_location import NestedLocation
        from ..models.nested_rack_role import NestedRackRole
        from ..models.nested_tenant import NestedTenant
        from ..models.rack_outer_unit_type_0 import RackOuterUnitType0
        from ..models.rack_type_type_0 import RackTypeType0
        from ..models.rack_weight_unit_type_0 import RackWeightUnitType0

        id = self.id

        url = self.url

        display = self.display

        name = self.name

        site = self.site.to_dict()

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        device_count = self.device_count

        powerfeed_count = self.powerfeed_count

        facility_id: Union[None, Unset, str]
        if isinstance(self.facility_id, Unset):
            facility_id = UNSET
        else:
            facility_id = self.facility_id

        location: Union[Dict[str, Any], None, Unset]
        if isinstance(self.location, Unset):
            location = UNSET
        elif isinstance(self.location, NestedLocation):
            location = self.location.to_dict()
        else:
            location = self.location

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenant):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        status: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.to_dict()

        role: Union[Dict[str, Any], None, Unset]
        if isinstance(self.role, Unset):
            role = UNSET
        elif isinstance(self.role, NestedRackRole):
            role = self.role.to_dict()
        else:
            role = self.role

        serial = self.serial

        asset_tag: Union[None, Unset, str]
        if isinstance(self.asset_tag, Unset):
            asset_tag = UNSET
        else:
            asset_tag = self.asset_tag

        type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.type, Unset):
            type = UNSET
        elif isinstance(self.type, RackTypeType0):
            type = self.type.to_dict()
        else:
            type = self.type

        width: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.width, Unset):
            width = self.width.to_dict()

        u_height = self.u_height

        starting_unit = self.starting_unit

        weight: Union[None, Unset, float]
        if isinstance(self.weight, Unset):
            weight = UNSET
        else:
            weight = self.weight

        max_weight: Union[None, Unset, int]
        if isinstance(self.max_weight, Unset):
            max_weight = UNSET
        else:
            max_weight = self.max_weight

        weight_unit: Union[Dict[str, Any], None, Unset]
        if isinstance(self.weight_unit, Unset):
            weight_unit = UNSET
        elif isinstance(self.weight_unit, RackWeightUnitType0):
            weight_unit = self.weight_unit.to_dict()
        else:
            weight_unit = self.weight_unit

        desc_units = self.desc_units

        outer_width: Union[None, Unset, int]
        if isinstance(self.outer_width, Unset):
            outer_width = UNSET
        else:
            outer_width = self.outer_width

        outer_depth: Union[None, Unset, int]
        if isinstance(self.outer_depth, Unset):
            outer_depth = UNSET
        else:
            outer_depth = self.outer_depth

        outer_unit: Union[Dict[str, Any], None, Unset]
        if isinstance(self.outer_unit, Unset):
            outer_unit = UNSET
        elif isinstance(self.outer_unit, RackOuterUnitType0):
            outer_unit = self.outer_unit.to_dict()
        else:
            outer_unit = self.outer_unit

        mounting_depth: Union[None, Unset, int]
        if isinstance(self.mounting_depth, Unset):
            mounting_depth = UNSET
        else:
            mounting_depth = self.mounting_depth

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "name": name,
                "site": site,
                "created": created,
                "last_updated": last_updated,
                "device_count": device_count,
                "powerfeed_count": powerfeed_count,
            }
        )
        if facility_id is not UNSET:
            field_dict["facility_id"] = facility_id
        if location is not UNSET:
            field_dict["location"] = location
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if serial is not UNSET:
            field_dict["serial"] = serial
        if asset_tag is not UNSET:
            field_dict["asset_tag"] = asset_tag
        if type is not UNSET:
            field_dict["type"] = type
        if width is not UNSET:
            field_dict["width"] = width
        if u_height is not UNSET:
            field_dict["u_height"] = u_height
        if starting_unit is not UNSET:
            field_dict["starting_unit"] = starting_unit
        if weight is not UNSET:
            field_dict["weight"] = weight
        if max_weight is not UNSET:
            field_dict["max_weight"] = max_weight
        if weight_unit is not UNSET:
            field_dict["weight_unit"] = weight_unit
        if desc_units is not UNSET:
            field_dict["desc_units"] = desc_units
        if outer_width is not UNSET:
            field_dict["outer_width"] = outer_width
        if outer_depth is not UNSET:
            field_dict["outer_depth"] = outer_depth
        if outer_unit is not UNSET:
            field_dict["outer_unit"] = outer_unit
        if mounting_depth is not UNSET:
            field_dict["mounting_depth"] = mounting_depth
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_location import NestedLocation
        from ..models.nested_rack_role import NestedRackRole
        from ..models.nested_site import NestedSite
        from ..models.nested_tag import NestedTag
        from ..models.nested_tenant import NestedTenant
        from ..models.rack_custom_fields import RackCustomFields
        from ..models.rack_outer_unit_type_0 import RackOuterUnitType0
        from ..models.rack_status import RackStatus
        from ..models.rack_type_type_0 import RackTypeType0
        from ..models.rack_weight_unit_type_0 import RackWeightUnitType0
        from ..models.rack_width import RackWidth

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        name = d.pop("name")

        site = NestedSite.from_dict(d.pop("site"))

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        device_count = d.pop("device_count")

        powerfeed_count = d.pop("powerfeed_count")

        def _parse_facility_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        facility_id = _parse_facility_id(d.pop("facility_id", UNSET))

        def _parse_location(data: object) -> Union["NestedLocation", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                location_type_1 = NestedLocation.from_dict(data)

                return location_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedLocation", None, Unset], data)

        location = _parse_location(d.pop("location", UNSET))

        def _parse_tenant(data: object) -> Union["NestedTenant", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenant.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenant", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, RackStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = RackStatus.from_dict(_status)

        def _parse_role(data: object) -> Union["NestedRackRole", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                role_type_1 = NestedRackRole.from_dict(data)

                return role_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedRackRole", None, Unset], data)

        role = _parse_role(d.pop("role", UNSET))

        serial = d.pop("serial", UNSET)

        def _parse_asset_tag(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        asset_tag = _parse_asset_tag(d.pop("asset_tag", UNSET))

        def _parse_type(data: object) -> Union["RackTypeType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                type_type_0 = RackTypeType0.from_dict(data)

                return type_type_0
            except:  # noqa: E722
                pass
            return cast(Union["RackTypeType0", None, Unset], data)

        type = _parse_type(d.pop("type", UNSET))

        _width = d.pop("width", UNSET)
        width: Union[Unset, RackWidth]
        if isinstance(_width, Unset):
            width = UNSET
        else:
            width = RackWidth.from_dict(_width)

        u_height = d.pop("u_height", UNSET)

        starting_unit = d.pop("starting_unit", UNSET)

        def _parse_weight(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        weight = _parse_weight(d.pop("weight", UNSET))

        def _parse_max_weight(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        max_weight = _parse_max_weight(d.pop("max_weight", UNSET))

        def _parse_weight_unit(
            data: object,
        ) -> Union["RackWeightUnitType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                weight_unit_type_0 = RackWeightUnitType0.from_dict(data)

                return weight_unit_type_0
            except:  # noqa: E722
                pass
            return cast(Union["RackWeightUnitType0", None, Unset], data)

        weight_unit = _parse_weight_unit(d.pop("weight_unit", UNSET))

        desc_units = d.pop("desc_units", UNSET)

        def _parse_outer_width(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        outer_width = _parse_outer_width(d.pop("outer_width", UNSET))

        def _parse_outer_depth(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        outer_depth = _parse_outer_depth(d.pop("outer_depth", UNSET))

        def _parse_outer_unit(data: object) -> Union["RackOuterUnitType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                outer_unit_type_0 = RackOuterUnitType0.from_dict(data)

                return outer_unit_type_0
            except:  # noqa: E722
                pass
            return cast(Union["RackOuterUnitType0", None, Unset], data)

        outer_unit = _parse_outer_unit(d.pop("outer_unit", UNSET))

        def _parse_mounting_depth(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        mounting_depth = _parse_mounting_depth(d.pop("mounting_depth", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, RackCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = RackCustomFields.from_dict(_custom_fields)

        rack = cls(
            id=id,
            url=url,
            display=display,
            name=name,
            site=site,
            created=created,
            last_updated=last_updated,
            device_count=device_count,
            powerfeed_count=powerfeed_count,
            facility_id=facility_id,
            location=location,
            tenant=tenant,
            status=status,
            role=role,
            serial=serial,
            asset_tag=asset_tag,
            type=type,
            width=width,
            u_height=u_height,
            starting_unit=starting_unit,
            weight=weight,
            max_weight=max_weight,
            weight_unit=weight_unit,
            desc_units=desc_units,
            outer_width=outer_width,
            outer_depth=outer_depth,
            outer_unit=outer_unit,
            mounting_depth=mounting_depth,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        rack.additional_properties = d
        return rack

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
