import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

if TYPE_CHECKING:
    from ..models.nested_data_source import NestedDataSource


T = TypeVar("T", bound="DataFile")


@_attrs_define
class DataFile:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        source (NestedDataSource): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        path (str): File path relative to the data source's root
        last_updated (datetime.datetime):
        size (int):
        hash_ (str): SHA256 hash of the file data
    """

    id: int
    url: str
    display: str
    source: "NestedDataSource"
    path: str
    last_updated: datetime.datetime
    size: int
    hash_: str
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        source = self.source.to_dict()

        path = self.path

        last_updated = self.last_updated.isoformat()

        size = self.size

        hash_ = self.hash_

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "source": source,
                "path": path,
                "last_updated": last_updated,
                "size": size,
                "hash": hash_,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_data_source import NestedDataSource

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        source = NestedDataSource.from_dict(d.pop("source"))

        path = d.pop("path")

        last_updated = isoparse(d.pop("last_updated"))

        size = d.pop("size")

        hash_ = d.pop("hash")

        data_file = cls(
            id=id,
            url=url,
            display=display,
            source=source,
            path=path,
            last_updated=last_updated,
            size=size,
            hash_=hash_,
        )

        data_file.additional_properties = d
        return data_file

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
