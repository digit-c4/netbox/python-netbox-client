import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_user_request import NestedUserRequest


T = TypeVar("T", bound="TokenRequest")


@_attrs_define
class TokenRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            user (NestedUserRequest): Represents an object related through a ForeignKey field. On write, it accepts a
                primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            expires (Union[None, Unset, datetime.datetime]):
            last_used (Union[None, Unset, datetime.datetime]):
            key (Union[Unset, str]):
            write_enabled (Union[Unset, bool]): Permit create/update/delete operations using this key
            description (Union[Unset, str]):
    """

    user: "NestedUserRequest"
    expires: Union[None, Unset, datetime.datetime] = UNSET
    last_used: Union[None, Unset, datetime.datetime] = UNSET
    key: Union[Unset, str] = UNSET
    write_enabled: Union[Unset, bool] = UNSET
    description: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        user = self.user.to_dict()

        expires: Union[None, Unset, str]
        if isinstance(self.expires, Unset):
            expires = UNSET
        elif isinstance(self.expires, datetime.datetime):
            expires = self.expires.isoformat()
        else:
            expires = self.expires

        last_used: Union[None, Unset, str]
        if isinstance(self.last_used, Unset):
            last_used = UNSET
        elif isinstance(self.last_used, datetime.datetime):
            last_used = self.last_used.isoformat()
        else:
            last_used = self.last_used

        key = self.key

        write_enabled = self.write_enabled

        description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "user": user,
            }
        )
        if expires is not UNSET:
            field_dict["expires"] = expires
        if last_used is not UNSET:
            field_dict["last_used"] = last_used
        if key is not UNSET:
            field_dict["key"] = key
        if write_enabled is not UNSET:
            field_dict["write_enabled"] = write_enabled
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_user_request import NestedUserRequest

        d = src_dict.copy()
        user = NestedUserRequest.from_dict(d.pop("user"))

        def _parse_expires(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                expires_type_0 = isoparse(data)

                return expires_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        expires = _parse_expires(d.pop("expires", UNSET))

        def _parse_last_used(data: object) -> Union[None, Unset, datetime.datetime]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_used_type_0 = isoparse(data)

                return last_used_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.datetime], data)

        last_used = _parse_last_used(d.pop("last_used", UNSET))

        key = d.pop("key", UNSET)

        write_enabled = d.pop("write_enabled", UNSET)

        description = d.pop("description", UNSET)

        token_request = cls(
            user=user,
            expires=expires,
            last_used=last_used,
            key=key,
            write_enabled=write_enabled,
            description=description,
        )

        token_request.additional_properties = d
        return token_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
