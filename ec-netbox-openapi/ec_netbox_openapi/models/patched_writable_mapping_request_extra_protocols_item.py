from enum import Enum


class PatchedWritableMappingRequestExtraProtocolsItem(str, Enum):
    WEBSOCKET = "websocket"

    def __str__(self) -> str:
        return str(self.value)
