import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_http_header_request_apply_to import (
    WritableHttpHeaderRequestApplyTo,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_http_header_request_custom_fields import (
        WritableHttpHeaderRequestCustomFields,
    )


T = TypeVar("T", bound="WritableHttpHeaderRequest")


@_attrs_define
class WritableHttpHeaderRequest:
    """HTTP Header Serializer class

    Attributes:
        name (str):
        mapping (int):
        value (Union[None, Unset, str]):
        apply_to (Union[Unset, WritableHttpHeaderRequestApplyTo]): * `request` - Request
            * `response` - Response Default: WritableHttpHeaderRequestApplyTo.REQUEST.
        custom_fields (Union[Unset, WritableHttpHeaderRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    name: str
    mapping: int
    value: Union[None, Unset, str] = UNSET
    apply_to: Union[Unset, WritableHttpHeaderRequestApplyTo] = (
        WritableHttpHeaderRequestApplyTo.REQUEST
    )
    custom_fields: Union[Unset, "WritableHttpHeaderRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        mapping = self.mapping

        value: Union[None, Unset, str]
        if isinstance(self.value, Unset):
            value = UNSET
        else:
            value = self.value

        apply_to: Union[Unset, str] = UNSET
        if not isinstance(self.apply_to, Unset):
            apply_to = self.apply_to.value

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "mapping": mapping,
            }
        )
        if value is not UNSET:
            field_dict["value"] = value
        if apply_to is not UNSET:
            field_dict["apply_to"] = apply_to
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (None, str(self.name).encode(), "text/plain")

        mapping = (None, str(self.mapping).encode(), "text/plain")

        value: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.value, Unset):
            value = UNSET
        elif isinstance(self.value, str):
            value = (None, str(self.value).encode(), "text/plain")
        else:
            value = (None, str(self.value).encode(), "text/plain")

        apply_to: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.apply_to, Unset):
            apply_to = (None, str(self.apply_to.value).encode(), "text/plain")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "name": name,
                "mapping": mapping,
            }
        )
        if value is not UNSET:
            field_dict["value"] = value
        if apply_to is not UNSET:
            field_dict["apply_to"] = apply_to
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_http_header_request_custom_fields import (
            WritableHttpHeaderRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name")

        mapping = d.pop("mapping")

        def _parse_value(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        value = _parse_value(d.pop("value", UNSET))

        _apply_to = d.pop("apply_to", UNSET)
        apply_to: Union[Unset, WritableHttpHeaderRequestApplyTo]
        if isinstance(_apply_to, Unset):
            apply_to = UNSET
        else:
            apply_to = WritableHttpHeaderRequestApplyTo(_apply_to)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableHttpHeaderRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableHttpHeaderRequestCustomFields.from_dict(
                _custom_fields
            )

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        writable_http_header_request = cls(
            name=name,
            mapping=mapping,
            value=value,
            apply_to=apply_to,
            custom_fields=custom_fields,
            tags=tags,
        )

        writable_http_header_request.additional_properties = d
        return writable_http_header_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
