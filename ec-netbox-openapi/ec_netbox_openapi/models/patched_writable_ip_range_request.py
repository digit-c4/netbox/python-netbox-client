import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_ip_range_request_status import (
    PatchedWritableIPRangeRequestStatus,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_writable_ip_range_request_custom_fields import (
        PatchedWritableIPRangeRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedWritableIPRangeRequest")


@_attrs_define
class PatchedWritableIPRangeRequest:
    """Adds support for custom fields and tags.

    Attributes:
        start_address (Union[Unset, str]):
        end_address (Union[Unset, str]):
        vrf (Union[None, Unset, int]):
        tenant (Union[None, Unset, int]):
        status (Union[Unset, PatchedWritableIPRangeRequestStatus]): Operational status of this range

            * `active` - Active
            * `reserved` - Reserved
            * `deprecated` - Deprecated
        role (Union[None, Unset, int]): The primary function of this range
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, PatchedWritableIPRangeRequestCustomFields]):
        mark_utilized (Union[Unset, bool]): Treat as 100% utilized
    """

    start_address: Union[Unset, str] = UNSET
    end_address: Union[Unset, str] = UNSET
    vrf: Union[None, Unset, int] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    status: Union[Unset, PatchedWritableIPRangeRequestStatus] = UNSET
    role: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "PatchedWritableIPRangeRequestCustomFields"] = UNSET
    mark_utilized: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        start_address = self.start_address

        end_address = self.end_address

        vrf: Union[None, Unset, int]
        if isinstance(self.vrf, Unset):
            vrf = UNSET
        else:
            vrf = self.vrf

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        role: Union[None, Unset, int]
        if isinstance(self.role, Unset):
            role = UNSET
        else:
            role = self.role

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        mark_utilized = self.mark_utilized

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if start_address is not UNSET:
            field_dict["start_address"] = start_address
        if end_address is not UNSET:
            field_dict["end_address"] = end_address
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if mark_utilized is not UNSET:
            field_dict["mark_utilized"] = mark_utilized

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        start_address = (
            self.start_address
            if isinstance(self.start_address, Unset)
            else (None, str(self.start_address).encode(), "text/plain")
        )

        end_address = (
            self.end_address
            if isinstance(self.end_address, Unset)
            else (None, str(self.end_address).encode(), "text/plain")
        )

        vrf: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.vrf, Unset):
            vrf = UNSET
        elif isinstance(self.vrf, int):
            vrf = (None, str(self.vrf).encode(), "text/plain")
        else:
            vrf = (None, str(self.vrf).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        role: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.role, Unset):
            role = UNSET
        elif isinstance(self.role, int):
            role = (None, str(self.role).encode(), "text/plain")
        else:
            role = (None, str(self.role).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        mark_utilized = (
            self.mark_utilized
            if isinstance(self.mark_utilized, Unset)
            else (None, str(self.mark_utilized).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if start_address is not UNSET:
            field_dict["start_address"] = start_address
        if end_address is not UNSET:
            field_dict["end_address"] = end_address
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if mark_utilized is not UNSET:
            field_dict["mark_utilized"] = mark_utilized

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_writable_ip_range_request_custom_fields import (
            PatchedWritableIPRangeRequestCustomFields,
        )

        d = src_dict.copy()
        start_address = d.pop("start_address", UNSET)

        end_address = d.pop("end_address", UNSET)

        def _parse_vrf(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vrf = _parse_vrf(d.pop("vrf", UNSET))

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, PatchedWritableIPRangeRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = PatchedWritableIPRangeRequestStatus(_status)

        def _parse_role(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        role = _parse_role(d.pop("role", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedWritableIPRangeRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedWritableIPRangeRequestCustomFields.from_dict(
                _custom_fields
            )

        mark_utilized = d.pop("mark_utilized", UNSET)

        patched_writable_ip_range_request = cls(
            start_address=start_address,
            end_address=end_address,
            vrf=vrf,
            tenant=tenant,
            status=status,
            role=role,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
            mark_utilized=mark_utilized,
        )

        patched_writable_ip_range_request.additional_properties = d
        return patched_writable_ip_range_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
