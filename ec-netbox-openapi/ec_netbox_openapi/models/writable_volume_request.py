import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_volume_request_driver import WritableVolumeRequestDriver
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_volume_request_custom_fields import (
        WritableVolumeRequestCustomFields,
    )


T = TypeVar("T", bound="WritableVolumeRequest")


@_attrs_define
class WritableVolumeRequest:
    """Volume Serializer class

    Attributes:
        host (int):
        name (str):
        driver (Union[Unset, WritableVolumeRequestDriver]): * `local` - local
        custom_fields (Union[Unset, WritableVolumeRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    host: int
    name: str
    driver: Union[Unset, WritableVolumeRequestDriver] = UNSET
    custom_fields: Union[Unset, "WritableVolumeRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        host = self.host

        name = self.name

        driver: Union[Unset, str] = UNSET
        if not isinstance(self.driver, Unset):
            driver = self.driver.value

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "host": host,
                "name": name,
            }
        )
        if driver is not UNSET:
            field_dict["driver"] = driver
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        host = (None, str(self.host).encode(), "text/plain")

        name = (None, str(self.name).encode(), "text/plain")

        driver: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.driver, Unset):
            driver = (None, str(self.driver.value).encode(), "text/plain")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "host": host,
                "name": name,
            }
        )
        if driver is not UNSET:
            field_dict["driver"] = driver
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_volume_request_custom_fields import (
            WritableVolumeRequestCustomFields,
        )

        d = src_dict.copy()
        host = d.pop("host")

        name = d.pop("name")

        _driver = d.pop("driver", UNSET)
        driver: Union[Unset, WritableVolumeRequestDriver]
        if isinstance(_driver, Unset):
            driver = UNSET
        else:
            driver = WritableVolumeRequestDriver(_driver)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableVolumeRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableVolumeRequestCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        writable_volume_request = cls(
            host=host,
            name=name,
            driver=driver,
            custom_fields=custom_fields,
            tags=tags,
        )

        writable_volume_request.additional_properties = d
        return writable_volume_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
