from typing import Any, Dict, List, Tuple, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_data_source_request_type import (
    PatchedWritableDataSourceRequestType,
)
from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedWritableDataSourceRequest")


@_attrs_define
class PatchedWritableDataSourceRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (Union[Unset, str]):
        type (Union[Unset, PatchedWritableDataSourceRequestType]): * `local` - Local
            * `git` - Git
            * `amazon-s3` - Amazon S3
        source_url (Union[Unset, str]):
        enabled (Union[Unset, bool]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        parameters (Union[Unset, Any]):
        ignore_rules (Union[Unset, str]): Patterns (one per line) matching files to ignore when syncing
    """

    name: Union[Unset, str] = UNSET
    type: Union[Unset, PatchedWritableDataSourceRequestType] = UNSET
    source_url: Union[Unset, str] = UNSET
    enabled: Union[Unset, bool] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    parameters: Union[Unset, Any] = UNSET
    ignore_rules: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        source_url = self.source_url

        enabled = self.enabled

        description = self.description

        comments = self.comments

        parameters = self.parameters

        ignore_rules = self.ignore_rules

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if type is not UNSET:
            field_dict["type"] = type
        if source_url is not UNSET:
            field_dict["source_url"] = source_url
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if parameters is not UNSET:
            field_dict["parameters"] = parameters
        if ignore_rules is not UNSET:
            field_dict["ignore_rules"] = ignore_rules

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        type: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.type, Unset):
            type = (None, str(self.type.value).encode(), "text/plain")

        source_url = (
            self.source_url
            if isinstance(self.source_url, Unset)
            else (None, str(self.source_url).encode(), "text/plain")
        )

        enabled = (
            self.enabled
            if isinstance(self.enabled, Unset)
            else (None, str(self.enabled).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        parameters = (
            self.parameters
            if isinstance(self.parameters, Unset)
            else (None, str(self.parameters).encode(), "text/plain")
        )

        ignore_rules = (
            self.ignore_rules
            if isinstance(self.ignore_rules, Unset)
            else (None, str(self.ignore_rules).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if type is not UNSET:
            field_dict["type"] = type
        if source_url is not UNSET:
            field_dict["source_url"] = source_url
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if parameters is not UNSET:
            field_dict["parameters"] = parameters
        if ignore_rules is not UNSET:
            field_dict["ignore_rules"] = ignore_rules

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name", UNSET)

        _type = d.pop("type", UNSET)
        type: Union[Unset, PatchedWritableDataSourceRequestType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = PatchedWritableDataSourceRequestType(_type)

        source_url = d.pop("source_url", UNSET)

        enabled = d.pop("enabled", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        parameters = d.pop("parameters", UNSET)

        ignore_rules = d.pop("ignore_rules", UNSET)

        patched_writable_data_source_request = cls(
            name=name,
            type=type,
            source_url=source_url,
            enabled=enabled,
            description=description,
            comments=comments,
            parameters=parameters,
            ignore_rules=ignore_rules,
        )

        patched_writable_data_source_request.additional_properties = d
        return patched_writable_data_source_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
