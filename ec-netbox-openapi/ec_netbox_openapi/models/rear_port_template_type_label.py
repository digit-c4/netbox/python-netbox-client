from enum import Enum


class RearPortTemplateTypeLabel(str, Enum):
    BNC = "BNC"
    CS = "CS"
    FC = "FC"
    F_CONNECTOR = "F Connector"
    GG45 = "GG45"
    LC = "LC"
    LCAPC = "LC/APC"
    LCPC = "LC/PC"
    LCUPC = "LC/UPC"
    LSH = "LSH"
    LSHAPC = "LSH/APC"
    LSHPC = "LSH/PC"
    LSHUPC = "LSH/UPC"
    LX_5 = "LX.5"
    LX_5APC = "LX.5/APC"
    LX_5PC = "LX.5/PC"
    LX_5UPC = "LX.5/UPC"
    MPO = "MPO"
    MRJ21 = "MRJ21"
    MTRJ = "MTRJ"
    N_CONNECTOR = "N Connector"
    OTHER = "Other"
    SC = "SC"
    SCAPC = "SC/APC"
    SCPC = "SC/PC"
    SCUPC = "SC/UPC"
    SMA_905 = "SMA 905"
    SMA_906 = "SMA 906"
    SN = "SN"
    SPLICE = "Splice"
    ST = "ST"
    TERA_1P = "TERA 1P"
    TERA_2P = "TERA 2P"
    TERA_4P = "TERA 4P"
    URM_P2 = "URM-P2"
    URM_P4 = "URM-P4"
    URM_P8 = "URM-P8"
    VALUE_0 = "8P8C"
    VALUE_1 = "8P6C"
    VALUE_13 = "110 Punch"
    VALUE_2 = "8P4C"
    VALUE_3 = "8P2C"
    VALUE_4 = "6P6C"
    VALUE_5 = "6P4C"
    VALUE_6 = "6P2C"
    VALUE_7 = "4P4C"
    VALUE_8 = "4P2C"

    def __str__(self) -> str:
        return str(self.value)
