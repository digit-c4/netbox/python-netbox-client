import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_device_role_request_custom_fields import (
        WritableDeviceRoleRequestCustomFields,
    )


T = TypeVar("T", bound="WritableDeviceRoleRequest")


@_attrs_define
class WritableDeviceRoleRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        slug (str):
        color (Union[Unset, str]):
        vm_role (Union[Unset, bool]): Virtual machines may be assigned to this role
        config_template (Union[None, Unset, int]):
        description (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritableDeviceRoleRequestCustomFields]):
    """

    name: str
    slug: str
    color: Union[Unset, str] = UNSET
    vm_role: Union[Unset, bool] = UNSET
    config_template: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WritableDeviceRoleRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        slug = self.slug

        color = self.color

        vm_role = self.vm_role

        config_template: Union[None, Unset, int]
        if isinstance(self.config_template, Unset):
            config_template = UNSET
        else:
            config_template = self.config_template

        description = self.description

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "slug": slug,
            }
        )
        if color is not UNSET:
            field_dict["color"] = color
        if vm_role is not UNSET:
            field_dict["vm_role"] = vm_role
        if config_template is not UNSET:
            field_dict["config_template"] = config_template
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (None, str(self.name).encode(), "text/plain")

        slug = (None, str(self.slug).encode(), "text/plain")

        color = (
            self.color
            if isinstance(self.color, Unset)
            else (None, str(self.color).encode(), "text/plain")
        )

        vm_role = (
            self.vm_role
            if isinstance(self.vm_role, Unset)
            else (None, str(self.vm_role).encode(), "text/plain")
        )

        config_template: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.config_template, Unset):
            config_template = UNSET
        elif isinstance(self.config_template, int):
            config_template = (None, str(self.config_template).encode(), "text/plain")
        else:
            config_template = (None, str(self.config_template).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "name": name,
                "slug": slug,
            }
        )
        if color is not UNSET:
            field_dict["color"] = color
        if vm_role is not UNSET:
            field_dict["vm_role"] = vm_role
        if config_template is not UNSET:
            field_dict["config_template"] = config_template
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_device_role_request_custom_fields import (
            WritableDeviceRoleRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name")

        slug = d.pop("slug")

        color = d.pop("color", UNSET)

        vm_role = d.pop("vm_role", UNSET)

        def _parse_config_template(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        config_template = _parse_config_template(d.pop("config_template", UNSET))

        description = d.pop("description", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableDeviceRoleRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableDeviceRoleRequestCustomFields.from_dict(
                _custom_fields
            )

        writable_device_role_request = cls(
            name=name,
            slug=slug,
            color=color,
            vm_role=vm_role,
            config_template=config_template,
            description=description,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_device_role_request.additional_properties = d
        return writable_device_role_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
