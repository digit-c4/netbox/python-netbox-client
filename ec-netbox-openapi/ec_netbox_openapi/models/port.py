from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.port_type import PortType
from ..types import UNSET, Unset

T = TypeVar("T", bound="Port")


@_attrs_define
class Port:
    """Container Port Serializer class

    Attributes:
        public_port (int):
        private_port (int):
        type (Union[Unset, PortType]): * `tcp` - TCP
            * `udp` - UDP
    """

    public_port: int
    private_port: int
    type: Union[Unset, PortType] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        public_port = self.public_port

        private_port = self.private_port

        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "public_port": public_port,
                "private_port": private_port,
            }
        )
        if type is not UNSET:
            field_dict["type"] = type

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        public_port = d.pop("public_port")

        private_port = d.pop("private_port")

        _type = d.pop("type", UNSET)
        type: Union[Unset, PortType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = PortType(_type)

        port = cls(
            public_port=public_port,
            private_port=private_port,
            type=type,
        )

        port.additional_properties = d
        return port

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
