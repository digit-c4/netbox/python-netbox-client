import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_contact_assignment_request_priority import (
    WritableContactAssignmentRequestPriority,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest


T = TypeVar("T", bound="WritableContactAssignmentRequest")


@_attrs_define
class WritableContactAssignmentRequest:
    """Adds support for custom fields and tags.

    Attributes:
        content_type (str):
        object_id (int):
        contact (int):
        role (int):
        priority (Union[Unset, WritableContactAssignmentRequestPriority]): * `primary` - Primary
            * `secondary` - Secondary
            * `tertiary` - Tertiary
            * `inactive` - Inactive
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    content_type: str
    object_id: int
    contact: int
    role: int
    priority: Union[Unset, WritableContactAssignmentRequestPriority] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        content_type = self.content_type

        object_id = self.object_id

        contact = self.contact

        role = self.role

        priority: Union[Unset, str] = UNSET
        if not isinstance(self.priority, Unset):
            priority = self.priority.value

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "content_type": content_type,
                "object_id": object_id,
                "contact": contact,
                "role": role,
            }
        )
        if priority is not UNSET:
            field_dict["priority"] = priority
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        content_type = (None, str(self.content_type).encode(), "text/plain")

        object_id = (None, str(self.object_id).encode(), "text/plain")

        contact = (None, str(self.contact).encode(), "text/plain")

        role = (None, str(self.role).encode(), "text/plain")

        priority: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.priority, Unset):
            priority = (None, str(self.priority.value).encode(), "text/plain")

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "content_type": content_type,
                "object_id": object_id,
                "contact": contact,
                "role": role,
            }
        )
        if priority is not UNSET:
            field_dict["priority"] = priority
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest

        d = src_dict.copy()
        content_type = d.pop("content_type")

        object_id = d.pop("object_id")

        contact = d.pop("contact")

        role = d.pop("role")

        _priority = d.pop("priority", UNSET)
        priority: Union[Unset, WritableContactAssignmentRequestPriority]
        if isinstance(_priority, Unset):
            priority = UNSET
        else:
            priority = WritableContactAssignmentRequestPriority(_priority)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        writable_contact_assignment_request = cls(
            content_type=content_type,
            object_id=object_id,
            contact=contact,
            role=role,
            priority=priority,
            tags=tags,
        )

        writable_contact_assignment_request.additional_properties = d
        return writable_contact_assignment_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
