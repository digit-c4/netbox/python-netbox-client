from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.l2vpn_termination_request_custom_fields import (
        L2VPNTerminationRequestCustomFields,
    )
    from ..models.nested_l2vpn_request import NestedL2VPNRequest
    from ..models.nested_tag_request import NestedTagRequest


T = TypeVar("T", bound="L2VPNTerminationRequest")


@_attrs_define
class L2VPNTerminationRequest:
    """Adds support for custom fields and tags.

    Attributes:
        l2vpn (NestedL2VPNRequest): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        assigned_object_type (str):
        assigned_object_id (int):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, L2VPNTerminationRequestCustomFields]):
    """

    l2vpn: "NestedL2VPNRequest"
    assigned_object_type: str
    assigned_object_id: int
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "L2VPNTerminationRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        l2vpn = self.l2vpn.to_dict()

        assigned_object_type = self.assigned_object_type

        assigned_object_id = self.assigned_object_id

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "l2vpn": l2vpn,
                "assigned_object_type": assigned_object_type,
                "assigned_object_id": assigned_object_id,
            }
        )
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.l2vpn_termination_request_custom_fields import (
            L2VPNTerminationRequestCustomFields,
        )
        from ..models.nested_l2vpn_request import NestedL2VPNRequest
        from ..models.nested_tag_request import NestedTagRequest

        d = src_dict.copy()
        l2vpn = NestedL2VPNRequest.from_dict(d.pop("l2vpn"))

        assigned_object_type = d.pop("assigned_object_type")

        assigned_object_id = d.pop("assigned_object_id")

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, L2VPNTerminationRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = L2VPNTerminationRequestCustomFields.from_dict(
                _custom_fields
            )

        l2vpn_termination_request = cls(
            l2vpn=l2vpn,
            assigned_object_type=assigned_object_type,
            assigned_object_id=assigned_object_id,
            tags=tags,
            custom_fields=custom_fields,
        )

        l2vpn_termination_request.additional_properties = d
        return l2vpn_termination_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
