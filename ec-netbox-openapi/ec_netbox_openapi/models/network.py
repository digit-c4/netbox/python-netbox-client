import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.network_driver_type_1 import NetworkDriverType1
from ..models.network_state import NetworkState
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_host import NestedHost
    from ..models.nested_network_setting import NestedNetworkSetting
    from ..models.nested_tag import NestedTag
    from ..models.network_custom_fields import NetworkCustomFields


T = TypeVar("T", bound="Network")


@_attrs_define
class Network:
    """Network Serializer class

    Attributes:
        id (int):
        url (str):
        display (str):
        host (NestedHost): Nested Host Serializer class
        name (str):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        network_settings (List['NestedNetworkSetting']):
        driver (Union[NetworkDriverType1, None, Unset]): * `None` - null
            * `bridge` - Bridge
            * `host` - Host
        network_id (Union[None, Unset, str]):
        state (Union[Unset, NetworkState]): * `creating` - Creating
            * `created` - Created
        custom_fields (Union[Unset, NetworkCustomFields]):
        tags (Union[Unset, List['NestedTag']]):
    """

    id: int
    url: str
    display: str
    host: "NestedHost"
    name: str
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    network_settings: List["NestedNetworkSetting"]
    driver: Union[NetworkDriverType1, None, Unset] = UNSET
    network_id: Union[None, Unset, str] = UNSET
    state: Union[Unset, NetworkState] = UNSET
    custom_fields: Union[Unset, "NetworkCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        host = self.host.to_dict()

        name = self.name

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        network_settings = []
        for network_settings_item_data in self.network_settings:
            network_settings_item = network_settings_item_data.to_dict()
            network_settings.append(network_settings_item)

        driver: Union[None, Unset, str]
        if isinstance(self.driver, Unset):
            driver = UNSET
        elif isinstance(self.driver, NetworkDriverType1):
            driver = self.driver.value
        else:
            driver = self.driver

        network_id: Union[None, Unset, str]
        if isinstance(self.network_id, Unset):
            network_id = UNSET
        else:
            network_id = self.network_id

        state: Union[Unset, str] = UNSET
        if not isinstance(self.state, Unset):
            state = self.state.value

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "host": host,
                "name": name,
                "created": created,
                "last_updated": last_updated,
                "network_settings": network_settings,
            }
        )
        if driver is not UNSET:
            field_dict["driver"] = driver
        if network_id is not UNSET:
            field_dict["NetworkID"] = network_id
        if state is not UNSET:
            field_dict["state"] = state
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_host import NestedHost
        from ..models.nested_network_setting import NestedNetworkSetting
        from ..models.nested_tag import NestedTag
        from ..models.network_custom_fields import NetworkCustomFields

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        host = NestedHost.from_dict(d.pop("host"))

        name = d.pop("name")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        network_settings = []
        _network_settings = d.pop("network_settings")
        for network_settings_item_data in _network_settings:
            network_settings_item = NestedNetworkSetting.from_dict(
                network_settings_item_data
            )

            network_settings.append(network_settings_item)

        def _parse_driver(data: object) -> Union[NetworkDriverType1, None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                driver_type_1 = NetworkDriverType1(data)

                return driver_type_1
            except:  # noqa: E722
                pass
            return cast(Union[NetworkDriverType1, None, Unset], data)

        driver = _parse_driver(d.pop("driver", UNSET))

        def _parse_network_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        network_id = _parse_network_id(d.pop("NetworkID", UNSET))

        _state = d.pop("state", UNSET)
        state: Union[Unset, NetworkState]
        if isinstance(_state, Unset):
            state = UNSET
        else:
            state = NetworkState(_state)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, NetworkCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = NetworkCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        network = cls(
            id=id,
            url=url,
            display=display,
            host=host,
            name=name,
            created=created,
            last_updated=last_updated,
            network_settings=network_settings,
            driver=driver,
            network_id=network_id,
            state=state,
            custom_fields=custom_fields,
            tags=tags,
        )

        network.additional_properties = d
        return network

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
