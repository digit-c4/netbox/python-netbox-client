import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_vlan_group_request_custom_fields import (
        PatchedVLANGroupRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedVLANGroupRequest")


@_attrs_define
class PatchedVLANGroupRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (Union[Unset, str]):
        slug (Union[Unset, str]):
        scope_type (Union[None, Unset, str]):
        scope_id (Union[None, Unset, int]):
        min_vid (Union[Unset, int]): Lowest permissible ID of a child VLAN
        max_vid (Union[Unset, int]): Highest permissible ID of a child VLAN
        description (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, PatchedVLANGroupRequestCustomFields]):
    """

    name: Union[Unset, str] = UNSET
    slug: Union[Unset, str] = UNSET
    scope_type: Union[None, Unset, str] = UNSET
    scope_id: Union[None, Unset, int] = UNSET
    min_vid: Union[Unset, int] = UNSET
    max_vid: Union[Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "PatchedVLANGroupRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        slug = self.slug

        scope_type: Union[None, Unset, str]
        if isinstance(self.scope_type, Unset):
            scope_type = UNSET
        else:
            scope_type = self.scope_type

        scope_id: Union[None, Unset, int]
        if isinstance(self.scope_id, Unset):
            scope_id = UNSET
        else:
            scope_id = self.scope_id

        min_vid = self.min_vid

        max_vid = self.max_vid

        description = self.description

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if slug is not UNSET:
            field_dict["slug"] = slug
        if scope_type is not UNSET:
            field_dict["scope_type"] = scope_type
        if scope_id is not UNSET:
            field_dict["scope_id"] = scope_id
        if min_vid is not UNSET:
            field_dict["min_vid"] = min_vid
        if max_vid is not UNSET:
            field_dict["max_vid"] = max_vid
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        slug = (
            self.slug
            if isinstance(self.slug, Unset)
            else (None, str(self.slug).encode(), "text/plain")
        )

        scope_type: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.scope_type, Unset):
            scope_type = UNSET
        elif isinstance(self.scope_type, str):
            scope_type = (None, str(self.scope_type).encode(), "text/plain")
        else:
            scope_type = (None, str(self.scope_type).encode(), "text/plain")

        scope_id: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.scope_id, Unset):
            scope_id = UNSET
        elif isinstance(self.scope_id, int):
            scope_id = (None, str(self.scope_id).encode(), "text/plain")
        else:
            scope_id = (None, str(self.scope_id).encode(), "text/plain")

        min_vid = (
            self.min_vid
            if isinstance(self.min_vid, Unset)
            else (None, str(self.min_vid).encode(), "text/plain")
        )

        max_vid = (
            self.max_vid
            if isinstance(self.max_vid, Unset)
            else (None, str(self.max_vid).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if slug is not UNSET:
            field_dict["slug"] = slug
        if scope_type is not UNSET:
            field_dict["scope_type"] = scope_type
        if scope_id is not UNSET:
            field_dict["scope_id"] = scope_id
        if min_vid is not UNSET:
            field_dict["min_vid"] = min_vid
        if max_vid is not UNSET:
            field_dict["max_vid"] = max_vid
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_vlan_group_request_custom_fields import (
            PatchedVLANGroupRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name", UNSET)

        slug = d.pop("slug", UNSET)

        def _parse_scope_type(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        scope_type = _parse_scope_type(d.pop("scope_type", UNSET))

        def _parse_scope_id(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        scope_id = _parse_scope_id(d.pop("scope_id", UNSET))

        min_vid = d.pop("min_vid", UNSET)

        max_vid = d.pop("max_vid", UNSET)

        description = d.pop("description", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedVLANGroupRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedVLANGroupRequestCustomFields.from_dict(
                _custom_fields
            )

        patched_vlan_group_request = cls(
            name=name,
            slug=slug,
            scope_type=scope_type,
            scope_id=scope_id,
            min_vid=min_vid,
            max_vid=max_vid,
            description=description,
            tags=tags,
            custom_fields=custom_fields,
        )

        patched_vlan_group_request.additional_properties = d
        return patched_vlan_group_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
