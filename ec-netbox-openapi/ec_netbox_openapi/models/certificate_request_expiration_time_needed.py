from enum import Enum


class CertificateRequestExpirationTimeNeeded(str, Enum):
    VALUE_0 = "1m"
    VALUE_1 = "3m"
    VALUE_2 = "6m"
    VALUE_3 = "1y"
    VALUE_4 = "3y"

    def __str__(self) -> str:
        return str(self.value)
