from enum import Enum


class MacAddressStatusType1(str, Enum):
    ACTIVE = "active"
    INACTIVE = "inactive"
    QUARANTINED = "quarantined"
    VALUE_3 = ""

    def __str__(self) -> str:
        return str(self.value)
