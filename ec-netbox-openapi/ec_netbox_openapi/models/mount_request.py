from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_volume_request import NestedVolumeRequest


T = TypeVar("T", bound="MountRequest")


@_attrs_define
class MountRequest:
    """Container Mount Serializer class

    Attributes:
        source (str):
        volume (NestedVolumeRequest): Nested Volume Serializer class
        read_only (Union[Unset, bool]):
    """

    source: str
    volume: "NestedVolumeRequest"
    read_only: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        source = self.source

        volume = self.volume.to_dict()

        read_only = self.read_only

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "source": source,
                "volume": volume,
            }
        )
        if read_only is not UNSET:
            field_dict["read_only"] = read_only

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_volume_request import NestedVolumeRequest

        d = src_dict.copy()
        source = d.pop("source")

        volume = NestedVolumeRequest.from_dict(d.pop("volume"))

        read_only = d.pop("read_only", UNSET)

        mount_request = cls(
            source=source,
            volume=volume,
            read_only=read_only,
        )

        mount_request.additional_properties = d
        return mount_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
