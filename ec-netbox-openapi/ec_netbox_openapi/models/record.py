import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.record_status import RecordStatus
from ..models.record_type import RecordType
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_record import NestedRecord
    from ..models.nested_tag import NestedTag
    from ..models.nested_tenant import NestedTenant
    from ..models.nested_zone import NestedZone
    from ..models.record_custom_fields import RecordCustomFields


T = TypeVar("T", bound="Record")


@_attrs_define
class Record:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        type (RecordType): * `A` - A
            * `A6` - A6
            * `AAAA` - AAAA
            * `AFSDB` - AFSDB
            * `AMTRELAY` - AMTRELAY
            * `APL` - APL
            * `AVC` - AVC
            * `CAA` - CAA
            * `CDNSKEY` - CDNSKEY
            * `CDS` - CDS
            * `CERT` - CERT
            * `CNAME` - CNAME
            * `CSYNC` - CSYNC
            * `DHCID` - DHCID
            * `DLV` - DLV
            * `DNAME` - DNAME
            * `DNSKEY` - DNSKEY
            * `DS` - DS
            * `EUI48` - EUI48
            * `EUI64` - EUI64
            * `GPOS` - GPOS
            * `HINFO` - HINFO
            * `HIP` - HIP
            * `HTTPS` - HTTPS
            * `IPSECKEY` - IPSECKEY
            * `ISDN` - ISDN
            * `KEY` - KEY
            * `KX` - KX
            * `L32` - L32
            * `L64` - L64
            * `LOC` - LOC
            * `LP` - LP
            * `MB` - MB
            * `MD` - MD
            * `MF` - MF
            * `MG` - MG
            * `MINFO` - MINFO
            * `MR` - MR
            * `MX` - MX
            * `NAPTR` - NAPTR
            * `NID` - NID
            * `NINFO` - NINFO
            * `NS` - NS
            * `NSAP` - NSAP
            * `NSAP_PTR` - NSAP_PTR
            * `NSEC` - NSEC
            * `NSEC3` - NSEC3
            * `NSEC3PARAM` - NSEC3PARAM
            * `NULL` - NULL
            * `NXT` - NXT
            * `OPENPGPKEY` - OPENPGPKEY
            * `PTR` - PTR
            * `PX` - PX
            * `RESINFO` - RESINFO
            * `RP` - RP
            * `RRSIG` - RRSIG
            * `RT` - RT
            * `SIG` - SIG
            * `SMIMEA` - SMIMEA
            * `SOA` - SOA
            * `SPF` - SPF
            * `SRV` - SRV
            * `SSHFP` - SSHFP
            * `SVCB` - SVCB
            * `TA` - TA
            * `TLSA` - TLSA
            * `TXT` - TXT
            * `TYPE0` - TYPE0
            * `UNSPEC` - UNSPEC
            * `URI` - URI
            * `WALLET` - WALLET
            * `WKS` - WKS
            * `X25` - X25
            * `ZONEMD` - ZONEMD
        name (str):
        value (str):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        ptr_record (Union['NestedRecord', None]): PTR record generated from an address
        address_record (Union['NestedRecord', None]): Address record defining the PTR
        active (bool):
        zone (Union[Unset, NestedZone]): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        status (Union[Unset, RecordStatus]): * `active` - Active
            * `inactive` - Inactive
        ttl (Union[None, Unset, int]):
        description (Union[Unset, str]):
        tags (Union[Unset, List['NestedTag']]):
        managed (Union[Unset, bool]):
        disable_ptr (Union[Unset, bool]): Disable PTR record creation
        custom_fields (Union[Unset, RecordCustomFields]):
        tenant (Union['NestedTenant', None, Unset]):
    """

    id: int
    url: str
    display: str
    type: RecordType
    name: str
    value: str
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    ptr_record: Union["NestedRecord", None]
    address_record: Union["NestedRecord", None]
    active: bool
    zone: Union[Unset, "NestedZone"] = UNSET
    status: Union[Unset, RecordStatus] = UNSET
    ttl: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    managed: Union[Unset, bool] = UNSET
    disable_ptr: Union[Unset, bool] = UNSET
    custom_fields: Union[Unset, "RecordCustomFields"] = UNSET
    tenant: Union["NestedTenant", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_record import NestedRecord
        from ..models.nested_tenant import NestedTenant

        id = self.id

        url = self.url

        display = self.display

        type = self.type.value

        name = self.name

        value = self.value

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        ptr_record: Union[Dict[str, Any], None]
        if isinstance(self.ptr_record, NestedRecord):
            ptr_record = self.ptr_record.to_dict()
        else:
            ptr_record = self.ptr_record

        address_record: Union[Dict[str, Any], None]
        if isinstance(self.address_record, NestedRecord):
            address_record = self.address_record.to_dict()
        else:
            address_record = self.address_record

        active = self.active

        zone: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.zone, Unset):
            zone = self.zone.to_dict()

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        ttl: Union[None, Unset, int]
        if isinstance(self.ttl, Unset):
            ttl = UNSET
        else:
            ttl = self.ttl

        description = self.description

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        managed = self.managed

        disable_ptr = self.disable_ptr

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenant):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "type": type,
                "name": name,
                "value": value,
                "created": created,
                "last_updated": last_updated,
                "ptr_record": ptr_record,
                "address_record": address_record,
                "active": active,
            }
        )
        if zone is not UNSET:
            field_dict["zone"] = zone
        if status is not UNSET:
            field_dict["status"] = status
        if ttl is not UNSET:
            field_dict["ttl"] = ttl
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if managed is not UNSET:
            field_dict["managed"] = managed
        if disable_ptr is not UNSET:
            field_dict["disable_ptr"] = disable_ptr
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tenant is not UNSET:
            field_dict["tenant"] = tenant

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_record import NestedRecord
        from ..models.nested_tag import NestedTag
        from ..models.nested_tenant import NestedTenant
        from ..models.nested_zone import NestedZone
        from ..models.record_custom_fields import RecordCustomFields

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        type = RecordType(d.pop("type"))

        name = d.pop("name")

        value = d.pop("value")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        def _parse_ptr_record(data: object) -> Union["NestedRecord", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                ptr_record_type_1 = NestedRecord.from_dict(data)

                return ptr_record_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedRecord", None], data)

        ptr_record = _parse_ptr_record(d.pop("ptr_record"))

        def _parse_address_record(data: object) -> Union["NestedRecord", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                address_record_type_1 = NestedRecord.from_dict(data)

                return address_record_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedRecord", None], data)

        address_record = _parse_address_record(d.pop("address_record"))

        active = d.pop("active")

        _zone = d.pop("zone", UNSET)
        zone: Union[Unset, NestedZone]
        if isinstance(_zone, Unset):
            zone = UNSET
        else:
            zone = NestedZone.from_dict(_zone)

        _status = d.pop("status", UNSET)
        status: Union[Unset, RecordStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = RecordStatus(_status)

        def _parse_ttl(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        ttl = _parse_ttl(d.pop("ttl", UNSET))

        description = d.pop("description", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        managed = d.pop("managed", UNSET)

        disable_ptr = d.pop("disable_ptr", UNSET)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, RecordCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = RecordCustomFields.from_dict(_custom_fields)

        def _parse_tenant(data: object) -> Union["NestedTenant", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenant.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenant", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        record = cls(
            id=id,
            url=url,
            display=display,
            type=type,
            name=name,
            value=value,
            created=created,
            last_updated=last_updated,
            ptr_record=ptr_record,
            address_record=address_record,
            active=active,
            zone=zone,
            status=status,
            ttl=ttl,
            description=description,
            tags=tags,
            managed=managed,
            disable_ptr=disable_ptr,
            custom_fields=custom_fields,
            tenant=tenant,
        )

        record.additional_properties = d
        return record

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
