import json
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Tuple,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_container_request_cap_add_type_0_item_type_1 import (
    WritableContainerRequestCapAddType0ItemType1,
)
from ..models.writable_container_request_cap_add_type_0_item_type_2_type_1 import (
    WritableContainerRequestCapAddType0ItemType2Type1,
)
from ..models.writable_container_request_cap_add_type_0_item_type_3_type_1 import (
    WritableContainerRequestCapAddType0ItemType3Type1,
)
from ..models.writable_container_request_operation import (
    WritableContainerRequestOperation,
)
from ..models.writable_container_request_restart_policy import (
    WritableContainerRequestRestartPolicy,
)
from ..models.writable_container_request_state import WritableContainerRequestState
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.bind_request import BindRequest
    from ..models.device_request import DeviceRequest
    from ..models.env_request import EnvRequest
    from ..models.label_request import LabelRequest
    from ..models.mount_request import MountRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.network_setting_request import NetworkSettingRequest
    from ..models.port_request import PortRequest
    from ..models.writable_container_request_custom_fields import (
        WritableContainerRequestCustomFields,
    )


T = TypeVar("T", bound="WritableContainerRequest")


@_attrs_define
class WritableContainerRequest:
    """Container Serializer class

    Attributes:
        host (int):
        image (int):
        name (str):
        state (Union[Unset, WritableContainerRequestState]): * `created` - Created
            * `restarting` - Restarting
            * `running` - Running
            * `paused` - Paused
            * `exited` - Exited
            * `dead` - Dead
            * `none` - None
        operation (Union[Unset, WritableContainerRequestOperation]): * `create` - Create
            * `start` - Start
            * `restart` - Restart
            * `stop` - Stop
            * `recreate` - Recreate
            * `kill` - Kill
            * `none` - None
        status (Union[None, Unset, str]):
        container_id (Union[None, Unset, str]):
        hostname (Union[None, Unset, str]):
        restart_policy (Union[Unset, WritableContainerRequestRestartPolicy]): * `no` - no
            * `on-failure` - on-failure
            * `always` - always
            * `unless-stopped` - unless-stopped
        cap_add (Union[List[Union[None, WritableContainerRequestCapAddType0ItemType1,
            WritableContainerRequestCapAddType0ItemType2Type1, WritableContainerRequestCapAddType0ItemType3Type1]], None,
            Unset]):
        ports (Union[Unset, List['PortRequest']]):
        env (Union[Unset, List['EnvRequest']]):
        labels (Union[Unset, List['LabelRequest']]):
        mounts (Union[Unset, List['MountRequest']]):
        binds (Union[Unset, List['BindRequest']]):
        network_settings (Union[Unset, List['NetworkSettingRequest']]):
        devices (Union[Unset, List['DeviceRequest']]):
        custom_fields (Union[Unset, WritableContainerRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    host: int
    image: int
    name: str
    state: Union[Unset, WritableContainerRequestState] = UNSET
    operation: Union[Unset, WritableContainerRequestOperation] = UNSET
    status: Union[None, Unset, str] = UNSET
    container_id: Union[None, Unset, str] = UNSET
    hostname: Union[None, Unset, str] = UNSET
    restart_policy: Union[Unset, WritableContainerRequestRestartPolicy] = UNSET
    cap_add: Union[
        List[
            Union[
                None,
                WritableContainerRequestCapAddType0ItemType1,
                WritableContainerRequestCapAddType0ItemType2Type1,
                WritableContainerRequestCapAddType0ItemType3Type1,
            ]
        ],
        None,
        Unset,
    ] = UNSET
    ports: Union[Unset, List["PortRequest"]] = UNSET
    env: Union[Unset, List["EnvRequest"]] = UNSET
    labels: Union[Unset, List["LabelRequest"]] = UNSET
    mounts: Union[Unset, List["MountRequest"]] = UNSET
    binds: Union[Unset, List["BindRequest"]] = UNSET
    network_settings: Union[Unset, List["NetworkSettingRequest"]] = UNSET
    devices: Union[Unset, List["DeviceRequest"]] = UNSET
    custom_fields: Union[Unset, "WritableContainerRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        host = self.host

        image = self.image

        name = self.name

        state: Union[Unset, str] = UNSET
        if not isinstance(self.state, Unset):
            state = self.state.value

        operation: Union[Unset, str] = UNSET
        if not isinstance(self.operation, Unset):
            operation = self.operation.value

        status: Union[None, Unset, str]
        if isinstance(self.status, Unset):
            status = UNSET
        else:
            status = self.status

        container_id: Union[None, Unset, str]
        if isinstance(self.container_id, Unset):
            container_id = UNSET
        else:
            container_id = self.container_id

        hostname: Union[None, Unset, str]
        if isinstance(self.hostname, Unset):
            hostname = UNSET
        else:
            hostname = self.hostname

        restart_policy: Union[Unset, str] = UNSET
        if not isinstance(self.restart_policy, Unset):
            restart_policy = self.restart_policy.value

        cap_add: Union[List[Union[None, str]], None, Unset]
        if isinstance(self.cap_add, Unset):
            cap_add = UNSET
        elif isinstance(self.cap_add, list):
            cap_add = []
            for cap_add_type_0_item_data in self.cap_add:
                cap_add_type_0_item: Union[None, str]
                if isinstance(
                    cap_add_type_0_item_data,
                    WritableContainerRequestCapAddType0ItemType1,
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                elif isinstance(
                    cap_add_type_0_item_data,
                    WritableContainerRequestCapAddType0ItemType2Type1,
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                elif isinstance(
                    cap_add_type_0_item_data,
                    WritableContainerRequestCapAddType0ItemType3Type1,
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                else:
                    cap_add_type_0_item = cap_add_type_0_item_data
                cap_add.append(cap_add_type_0_item)

        else:
            cap_add = self.cap_add

        ports: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.ports, Unset):
            ports = []
            for ports_item_data in self.ports:
                ports_item = ports_item_data.to_dict()
                ports.append(ports_item)

        env: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.env, Unset):
            env = []
            for env_item_data in self.env:
                env_item = env_item_data.to_dict()
                env.append(env_item)

        labels: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.labels, Unset):
            labels = []
            for labels_item_data in self.labels:
                labels_item = labels_item_data.to_dict()
                labels.append(labels_item)

        mounts: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.mounts, Unset):
            mounts = []
            for mounts_item_data in self.mounts:
                mounts_item = mounts_item_data.to_dict()
                mounts.append(mounts_item)

        binds: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.binds, Unset):
            binds = []
            for binds_item_data in self.binds:
                binds_item = binds_item_data.to_dict()
                binds.append(binds_item)

        network_settings: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.network_settings, Unset):
            network_settings = []
            for network_settings_item_data in self.network_settings:
                network_settings_item = network_settings_item_data.to_dict()
                network_settings.append(network_settings_item)

        devices: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.devices, Unset):
            devices = []
            for devices_item_data in self.devices:
                devices_item = devices_item_data.to_dict()
                devices.append(devices_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "host": host,
                "image": image,
                "name": name,
            }
        )
        if state is not UNSET:
            field_dict["state"] = state
        if operation is not UNSET:
            field_dict["operation"] = operation
        if status is not UNSET:
            field_dict["status"] = status
        if container_id is not UNSET:
            field_dict["ContainerID"] = container_id
        if hostname is not UNSET:
            field_dict["hostname"] = hostname
        if restart_policy is not UNSET:
            field_dict["restart_policy"] = restart_policy
        if cap_add is not UNSET:
            field_dict["cap_add"] = cap_add
        if ports is not UNSET:
            field_dict["ports"] = ports
        if env is not UNSET:
            field_dict["env"] = env
        if labels is not UNSET:
            field_dict["labels"] = labels
        if mounts is not UNSET:
            field_dict["mounts"] = mounts
        if binds is not UNSET:
            field_dict["binds"] = binds
        if network_settings is not UNSET:
            field_dict["network_settings"] = network_settings
        if devices is not UNSET:
            field_dict["devices"] = devices
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        host = (None, str(self.host).encode(), "text/plain")

        image = (None, str(self.image).encode(), "text/plain")

        name = (None, str(self.name).encode(), "text/plain")

        state: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.state, Unset):
            state = (None, str(self.state.value).encode(), "text/plain")

        operation: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.operation, Unset):
            operation = (None, str(self.operation.value).encode(), "text/plain")

        status: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.status, Unset):
            status = UNSET
        elif isinstance(self.status, str):
            status = (None, str(self.status).encode(), "text/plain")
        else:
            status = (None, str(self.status).encode(), "text/plain")

        container_id: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.container_id, Unset):
            container_id = UNSET
        elif isinstance(self.container_id, str):
            container_id = (None, str(self.container_id).encode(), "text/plain")
        else:
            container_id = (None, str(self.container_id).encode(), "text/plain")

        hostname: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.hostname, Unset):
            hostname = UNSET
        elif isinstance(self.hostname, str):
            hostname = (None, str(self.hostname).encode(), "text/plain")
        else:
            hostname = (None, str(self.hostname).encode(), "text/plain")

        restart_policy: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.restart_policy, Unset):
            restart_policy = (
                None,
                str(self.restart_policy.value).encode(),
                "text/plain",
            )

        cap_add: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.cap_add, Unset):
            cap_add = UNSET
        elif isinstance(self.cap_add, list):
            _temp_cap_add = []
            for cap_add_type_0_item_data in self.cap_add:
                cap_add_type_0_item: Union[None, str]
                if isinstance(
                    cap_add_type_0_item_data,
                    WritableContainerRequestCapAddType0ItemType1,
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                elif isinstance(
                    cap_add_type_0_item_data,
                    WritableContainerRequestCapAddType0ItemType2Type1,
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                elif isinstance(
                    cap_add_type_0_item_data,
                    WritableContainerRequestCapAddType0ItemType3Type1,
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                else:
                    cap_add_type_0_item = cap_add_type_0_item_data
                _temp_cap_add.append(cap_add_type_0_item)
            cap_add = (None, json.dumps(_temp_cap_add).encode(), "application/json")
        else:
            cap_add = (None, str(self.cap_add).encode(), "text/plain")

        ports: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.ports, Unset):
            _temp_ports = []
            for ports_item_data in self.ports:
                ports_item = ports_item_data.to_dict()
                _temp_ports.append(ports_item)
            ports = (None, json.dumps(_temp_ports).encode(), "application/json")

        env: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.env, Unset):
            _temp_env = []
            for env_item_data in self.env:
                env_item = env_item_data.to_dict()
                _temp_env.append(env_item)
            env = (None, json.dumps(_temp_env).encode(), "application/json")

        labels: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.labels, Unset):
            _temp_labels = []
            for labels_item_data in self.labels:
                labels_item = labels_item_data.to_dict()
                _temp_labels.append(labels_item)
            labels = (None, json.dumps(_temp_labels).encode(), "application/json")

        mounts: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.mounts, Unset):
            _temp_mounts = []
            for mounts_item_data in self.mounts:
                mounts_item = mounts_item_data.to_dict()
                _temp_mounts.append(mounts_item)
            mounts = (None, json.dumps(_temp_mounts).encode(), "application/json")

        binds: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.binds, Unset):
            _temp_binds = []
            for binds_item_data in self.binds:
                binds_item = binds_item_data.to_dict()
                _temp_binds.append(binds_item)
            binds = (None, json.dumps(_temp_binds).encode(), "application/json")

        network_settings: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.network_settings, Unset):
            _temp_network_settings = []
            for network_settings_item_data in self.network_settings:
                network_settings_item = network_settings_item_data.to_dict()
                _temp_network_settings.append(network_settings_item)
            network_settings = (
                None,
                json.dumps(_temp_network_settings).encode(),
                "application/json",
            )

        devices: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.devices, Unset):
            _temp_devices = []
            for devices_item_data in self.devices:
                devices_item = devices_item_data.to_dict()
                _temp_devices.append(devices_item)
            devices = (None, json.dumps(_temp_devices).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "host": host,
                "image": image,
                "name": name,
            }
        )
        if state is not UNSET:
            field_dict["state"] = state
        if operation is not UNSET:
            field_dict["operation"] = operation
        if status is not UNSET:
            field_dict["status"] = status
        if container_id is not UNSET:
            field_dict["ContainerID"] = container_id
        if hostname is not UNSET:
            field_dict["hostname"] = hostname
        if restart_policy is not UNSET:
            field_dict["restart_policy"] = restart_policy
        if cap_add is not UNSET:
            field_dict["cap_add"] = cap_add
        if ports is not UNSET:
            field_dict["ports"] = ports
        if env is not UNSET:
            field_dict["env"] = env
        if labels is not UNSET:
            field_dict["labels"] = labels
        if mounts is not UNSET:
            field_dict["mounts"] = mounts
        if binds is not UNSET:
            field_dict["binds"] = binds
        if network_settings is not UNSET:
            field_dict["network_settings"] = network_settings
        if devices is not UNSET:
            field_dict["devices"] = devices
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.bind_request import BindRequest
        from ..models.device_request import DeviceRequest
        from ..models.env_request import EnvRequest
        from ..models.label_request import LabelRequest
        from ..models.mount_request import MountRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.network_setting_request import NetworkSettingRequest
        from ..models.port_request import PortRequest
        from ..models.writable_container_request_custom_fields import (
            WritableContainerRequestCustomFields,
        )

        d = src_dict.copy()
        host = d.pop("host")

        image = d.pop("image")

        name = d.pop("name")

        _state = d.pop("state", UNSET)
        state: Union[Unset, WritableContainerRequestState]
        if isinstance(_state, Unset):
            state = UNSET
        else:
            state = WritableContainerRequestState(_state)

        _operation = d.pop("operation", UNSET)
        operation: Union[Unset, WritableContainerRequestOperation]
        if isinstance(_operation, Unset):
            operation = UNSET
        else:
            operation = WritableContainerRequestOperation(_operation)

        def _parse_status(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        status = _parse_status(d.pop("status", UNSET))

        def _parse_container_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        container_id = _parse_container_id(d.pop("ContainerID", UNSET))

        def _parse_hostname(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        hostname = _parse_hostname(d.pop("hostname", UNSET))

        _restart_policy = d.pop("restart_policy", UNSET)
        restart_policy: Union[Unset, WritableContainerRequestRestartPolicy]
        if isinstance(_restart_policy, Unset):
            restart_policy = UNSET
        else:
            restart_policy = WritableContainerRequestRestartPolicy(_restart_policy)

        def _parse_cap_add(
            data: object,
        ) -> Union[
            List[
                Union[
                    None,
                    WritableContainerRequestCapAddType0ItemType1,
                    WritableContainerRequestCapAddType0ItemType2Type1,
                    WritableContainerRequestCapAddType0ItemType3Type1,
                ]
            ],
            None,
            Unset,
        ]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                cap_add_type_0 = []
                _cap_add_type_0 = data
                for cap_add_type_0_item_data in _cap_add_type_0:

                    def _parse_cap_add_type_0_item(
                        data: object,
                    ) -> Union[
                        None,
                        WritableContainerRequestCapAddType0ItemType1,
                        WritableContainerRequestCapAddType0ItemType2Type1,
                        WritableContainerRequestCapAddType0ItemType3Type1,
                    ]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_1 = (
                                WritableContainerRequestCapAddType0ItemType1(data)
                            )

                            return cap_add_type_0_item_type_1
                        except:  # noqa: E722
                            pass
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_2_type_1 = (
                                WritableContainerRequestCapAddType0ItemType2Type1(data)
                            )

                            return cap_add_type_0_item_type_2_type_1
                        except:  # noqa: E722
                            pass
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_3_type_1 = (
                                WritableContainerRequestCapAddType0ItemType3Type1(data)
                            )

                            return cap_add_type_0_item_type_3_type_1
                        except:  # noqa: E722
                            pass
                        return cast(
                            Union[
                                None,
                                WritableContainerRequestCapAddType0ItemType1,
                                WritableContainerRequestCapAddType0ItemType2Type1,
                                WritableContainerRequestCapAddType0ItemType3Type1,
                            ],
                            data,
                        )

                    cap_add_type_0_item = _parse_cap_add_type_0_item(
                        cap_add_type_0_item_data
                    )

                    cap_add_type_0.append(cap_add_type_0_item)

                return cap_add_type_0
            except:  # noqa: E722
                pass
            return cast(
                Union[
                    List[
                        Union[
                            None,
                            WritableContainerRequestCapAddType0ItemType1,
                            WritableContainerRequestCapAddType0ItemType2Type1,
                            WritableContainerRequestCapAddType0ItemType3Type1,
                        ]
                    ],
                    None,
                    Unset,
                ],
                data,
            )

        cap_add = _parse_cap_add(d.pop("cap_add", UNSET))

        ports = []
        _ports = d.pop("ports", UNSET)
        for ports_item_data in _ports or []:
            ports_item = PortRequest.from_dict(ports_item_data)

            ports.append(ports_item)

        env = []
        _env = d.pop("env", UNSET)
        for env_item_data in _env or []:
            env_item = EnvRequest.from_dict(env_item_data)

            env.append(env_item)

        labels = []
        _labels = d.pop("labels", UNSET)
        for labels_item_data in _labels or []:
            labels_item = LabelRequest.from_dict(labels_item_data)

            labels.append(labels_item)

        mounts = []
        _mounts = d.pop("mounts", UNSET)
        for mounts_item_data in _mounts or []:
            mounts_item = MountRequest.from_dict(mounts_item_data)

            mounts.append(mounts_item)

        binds = []
        _binds = d.pop("binds", UNSET)
        for binds_item_data in _binds or []:
            binds_item = BindRequest.from_dict(binds_item_data)

            binds.append(binds_item)

        network_settings = []
        _network_settings = d.pop("network_settings", UNSET)
        for network_settings_item_data in _network_settings or []:
            network_settings_item = NetworkSettingRequest.from_dict(
                network_settings_item_data
            )

            network_settings.append(network_settings_item)

        devices = []
        _devices = d.pop("devices", UNSET)
        for devices_item_data in _devices or []:
            devices_item = DeviceRequest.from_dict(devices_item_data)

            devices.append(devices_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableContainerRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableContainerRequestCustomFields.from_dict(
                _custom_fields
            )

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        writable_container_request = cls(
            host=host,
            image=image,
            name=name,
            state=state,
            operation=operation,
            status=status,
            container_id=container_id,
            hostname=hostname,
            restart_policy=restart_policy,
            cap_add=cap_add,
            ports=ports,
            env=env,
            labels=labels,
            mounts=mounts,
            binds=binds,
            network_settings=network_settings,
            devices=devices,
            custom_fields=custom_fields,
            tags=tags,
        )

        writable_container_request.additional_properties = d
        return writable_container_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
