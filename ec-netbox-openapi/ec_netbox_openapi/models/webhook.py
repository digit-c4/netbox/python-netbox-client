import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.webhook_http_method import WebhookHttpMethod
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag import NestedTag
    from ..models.webhook_custom_fields import WebhookCustomFields


T = TypeVar("T", bound="Webhook")


@_attrs_define
class Webhook:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        content_types (List[str]):
        name (str):
        payload_url (str): This URL will be called using the HTTP method defined when the webhook is called. Jinja2
            template processing is supported with the same context as the request body.
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        type_create (Union[Unset, bool]): Triggers when a matching object is created.
        type_update (Union[Unset, bool]): Triggers when a matching object is updated.
        type_delete (Union[Unset, bool]): Triggers when a matching object is deleted.
        type_job_start (Union[Unset, bool]): Triggers when a job for a matching object is started.
        type_job_end (Union[Unset, bool]): Triggers when a job for a matching object terminates.
        enabled (Union[Unset, bool]):
        http_method (Union[Unset, WebhookHttpMethod]): * `GET` - GET
            * `POST` - POST
            * `PUT` - PUT
            * `PATCH` - PATCH
            * `DELETE` - DELETE
        http_content_type (Union[Unset, str]): The complete list of official content types is available <a
            href="https://www.iana.org/assignments/media-types/media-types.xhtml">here</a>.
        additional_headers (Union[Unset, str]): User-supplied HTTP headers to be sent with the request in addition to
            the HTTP content type. Headers should be defined in the format <code>Name: Value</code>. Jinja2 template
            processing is supported with the same context as the request body (below).
        body_template (Union[Unset, str]): Jinja2 template for a custom request body. If blank, a JSON object
            representing the change will be included. Available context data includes: <code>event</code>,
            <code>model</code>, <code>timestamp</code>, <code>username</code>, <code>request_id</code>, and
            <code>data</code>.
        secret (Union[Unset, str]): When provided, the request will include a <code>X-Hook-Signature</code> header
            containing a HMAC hex digest of the payload body using the secret as the key. The secret is not transmitted in
            the request.
        conditions (Union[Unset, Any]): A set of conditions which determine whether the webhook will be generated.
        ssl_verification (Union[Unset, bool]): Enable SSL certificate verification. Disable with caution!
        ca_file_path (Union[None, Unset, str]): The specific CA certificate file to use for SSL verification. Leave
            blank to use the system defaults.
        custom_fields (Union[Unset, WebhookCustomFields]):
        tags (Union[Unset, List['NestedTag']]):
    """

    id: int
    url: str
    display: str
    content_types: List[str]
    name: str
    payload_url: str
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    type_create: Union[Unset, bool] = UNSET
    type_update: Union[Unset, bool] = UNSET
    type_delete: Union[Unset, bool] = UNSET
    type_job_start: Union[Unset, bool] = UNSET
    type_job_end: Union[Unset, bool] = UNSET
    enabled: Union[Unset, bool] = UNSET
    http_method: Union[Unset, WebhookHttpMethod] = UNSET
    http_content_type: Union[Unset, str] = UNSET
    additional_headers: Union[Unset, str] = UNSET
    body_template: Union[Unset, str] = UNSET
    secret: Union[Unset, str] = UNSET
    conditions: Union[Unset, Any] = UNSET
    ssl_verification: Union[Unset, bool] = UNSET
    ca_file_path: Union[None, Unset, str] = UNSET
    custom_fields: Union[Unset, "WebhookCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        content_types = self.content_types

        name = self.name

        payload_url = self.payload_url

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        type_create = self.type_create

        type_update = self.type_update

        type_delete = self.type_delete

        type_job_start = self.type_job_start

        type_job_end = self.type_job_end

        enabled = self.enabled

        http_method: Union[Unset, str] = UNSET
        if not isinstance(self.http_method, Unset):
            http_method = self.http_method.value

        http_content_type = self.http_content_type

        additional_headers = self.additional_headers

        body_template = self.body_template

        secret = self.secret

        conditions = self.conditions

        ssl_verification = self.ssl_verification

        ca_file_path: Union[None, Unset, str]
        if isinstance(self.ca_file_path, Unset):
            ca_file_path = UNSET
        else:
            ca_file_path = self.ca_file_path

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "content_types": content_types,
                "name": name,
                "payload_url": payload_url,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if type_create is not UNSET:
            field_dict["type_create"] = type_create
        if type_update is not UNSET:
            field_dict["type_update"] = type_update
        if type_delete is not UNSET:
            field_dict["type_delete"] = type_delete
        if type_job_start is not UNSET:
            field_dict["type_job_start"] = type_job_start
        if type_job_end is not UNSET:
            field_dict["type_job_end"] = type_job_end
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if http_method is not UNSET:
            field_dict["http_method"] = http_method
        if http_content_type is not UNSET:
            field_dict["http_content_type"] = http_content_type
        if additional_headers is not UNSET:
            field_dict["additional_headers"] = additional_headers
        if body_template is not UNSET:
            field_dict["body_template"] = body_template
        if secret is not UNSET:
            field_dict["secret"] = secret
        if conditions is not UNSET:
            field_dict["conditions"] = conditions
        if ssl_verification is not UNSET:
            field_dict["ssl_verification"] = ssl_verification
        if ca_file_path is not UNSET:
            field_dict["ca_file_path"] = ca_file_path
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag import NestedTag
        from ..models.webhook_custom_fields import WebhookCustomFields

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        content_types = cast(List[str], d.pop("content_types"))

        name = d.pop("name")

        payload_url = d.pop("payload_url")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        type_create = d.pop("type_create", UNSET)

        type_update = d.pop("type_update", UNSET)

        type_delete = d.pop("type_delete", UNSET)

        type_job_start = d.pop("type_job_start", UNSET)

        type_job_end = d.pop("type_job_end", UNSET)

        enabled = d.pop("enabled", UNSET)

        _http_method = d.pop("http_method", UNSET)
        http_method: Union[Unset, WebhookHttpMethod]
        if isinstance(_http_method, Unset):
            http_method = UNSET
        else:
            http_method = WebhookHttpMethod(_http_method)

        http_content_type = d.pop("http_content_type", UNSET)

        additional_headers = d.pop("additional_headers", UNSET)

        body_template = d.pop("body_template", UNSET)

        secret = d.pop("secret", UNSET)

        conditions = d.pop("conditions", UNSET)

        ssl_verification = d.pop("ssl_verification", UNSET)

        def _parse_ca_file_path(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        ca_file_path = _parse_ca_file_path(d.pop("ca_file_path", UNSET))

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WebhookCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WebhookCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        webhook = cls(
            id=id,
            url=url,
            display=display,
            content_types=content_types,
            name=name,
            payload_url=payload_url,
            created=created,
            last_updated=last_updated,
            type_create=type_create,
            type_update=type_update,
            type_delete=type_delete,
            type_job_start=type_job_start,
            type_job_end=type_job_end,
            enabled=enabled,
            http_method=http_method,
            http_content_type=http_content_type,
            additional_headers=additional_headers,
            body_template=body_template,
            secret=secret,
            conditions=conditions,
            ssl_verification=ssl_verification,
            ca_file_path=ca_file_path,
            custom_fields=custom_fields,
            tags=tags,
        )

        webhook.additional_properties = d
        return webhook

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
