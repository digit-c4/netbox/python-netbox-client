from typing import Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="WritableRegistryRequest")


@_attrs_define
class WritableRegistryRequest:
    """Registry Serializer class

    Attributes:
        name (str):
        host (int):
        serveraddress (str):
        username (Union[None, Unset, str]):
        password (Union[None, Unset, str]):
        email (Union[None, Unset, str]):
    """

    name: str
    host: int
    serveraddress: str
    username: Union[None, Unset, str] = UNSET
    password: Union[None, Unset, str] = UNSET
    email: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        host = self.host

        serveraddress = self.serveraddress

        username: Union[None, Unset, str]
        if isinstance(self.username, Unset):
            username = UNSET
        else:
            username = self.username

        password: Union[None, Unset, str]
        if isinstance(self.password, Unset):
            password = UNSET
        else:
            password = self.password

        email: Union[None, Unset, str]
        if isinstance(self.email, Unset):
            email = UNSET
        else:
            email = self.email

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "host": host,
                "serveraddress": serveraddress,
            }
        )
        if username is not UNSET:
            field_dict["username"] = username
        if password is not UNSET:
            field_dict["password"] = password
        if email is not UNSET:
            field_dict["email"] = email

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (None, str(self.name).encode(), "text/plain")

        host = (None, str(self.host).encode(), "text/plain")

        serveraddress = (None, str(self.serveraddress).encode(), "text/plain")

        username: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.username, Unset):
            username = UNSET
        elif isinstance(self.username, str):
            username = (None, str(self.username).encode(), "text/plain")
        else:
            username = (None, str(self.username).encode(), "text/plain")

        password: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.password, Unset):
            password = UNSET
        elif isinstance(self.password, str):
            password = (None, str(self.password).encode(), "text/plain")
        else:
            password = (None, str(self.password).encode(), "text/plain")

        email: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.email, Unset):
            email = UNSET
        elif isinstance(self.email, str):
            email = (None, str(self.email).encode(), "text/plain")
        else:
            email = (None, str(self.email).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "name": name,
                "host": host,
                "serveraddress": serveraddress,
            }
        )
        if username is not UNSET:
            field_dict["username"] = username
        if password is not UNSET:
            field_dict["password"] = password
        if email is not UNSET:
            field_dict["email"] = email

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name")

        host = d.pop("host")

        serveraddress = d.pop("serveraddress")

        def _parse_username(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        username = _parse_username(d.pop("username", UNSET))

        def _parse_password(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        password = _parse_password(d.pop("password", UNSET))

        def _parse_email(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        email = _parse_email(d.pop("email", UNSET))

        writable_registry_request = cls(
            name=name,
            host=host,
            serveraddress=serveraddress,
            username=username,
            password=password,
            email=email,
        )

        writable_registry_request.additional_properties = d
        return writable_registry_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
