import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.ip_address_custom_fields import IPAddressCustomFields
    from ..models.ip_address_family import IPAddressFamily
    from ..models.ip_address_role import IPAddressRole
    from ..models.ip_address_status import IPAddressStatus
    from ..models.nested_ip_address import NestedIPAddress
    from ..models.nested_tag import NestedTag
    from ..models.nested_tenant import NestedTenant
    from ..models.nested_vrf import NestedVRF


T = TypeVar("T", bound="IPAddress")


@_attrs_define
class IPAddress:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        family (IPAddressFamily):
        address (str):
        assigned_object (Any):
        nat_outside (List['NestedIPAddress']):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        vrf (Union['NestedVRF', None, Unset]):
        tenant (Union['NestedTenant', None, Unset]):
        status (Union[Unset, IPAddressStatus]):
        role (Union[Unset, IPAddressRole]):
        assigned_object_type (Union[None, Unset, str]):
        assigned_object_id (Union[None, Unset, int]):
        nat_inside (Union['NestedIPAddress', None, Unset]):
        dns_name (Union[Unset, str]): Hostname or FQDN (not case-sensitive)
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, IPAddressCustomFields]):
    """

    id: int
    url: str
    display: str
    family: "IPAddressFamily"
    address: str
    assigned_object: Any
    nat_outside: List["NestedIPAddress"]
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    vrf: Union["NestedVRF", None, Unset] = UNSET
    tenant: Union["NestedTenant", None, Unset] = UNSET
    status: Union[Unset, "IPAddressStatus"] = UNSET
    role: Union[Unset, "IPAddressRole"] = UNSET
    assigned_object_type: Union[None, Unset, str] = UNSET
    assigned_object_id: Union[None, Unset, int] = UNSET
    nat_inside: Union["NestedIPAddress", None, Unset] = UNSET
    dns_name: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "IPAddressCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_ip_address import NestedIPAddress
        from ..models.nested_tenant import NestedTenant
        from ..models.nested_vrf import NestedVRF

        id = self.id

        url = self.url

        display = self.display

        family = self.family.to_dict()

        address = self.address

        assigned_object = self.assigned_object

        nat_outside = []
        for nat_outside_item_data in self.nat_outside:
            nat_outside_item = nat_outside_item_data.to_dict()
            nat_outside.append(nat_outside_item)

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        vrf: Union[Dict[str, Any], None, Unset]
        if isinstance(self.vrf, Unset):
            vrf = UNSET
        elif isinstance(self.vrf, NestedVRF):
            vrf = self.vrf.to_dict()
        else:
            vrf = self.vrf

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenant):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        status: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.to_dict()

        role: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.role, Unset):
            role = self.role.to_dict()

        assigned_object_type: Union[None, Unset, str]
        if isinstance(self.assigned_object_type, Unset):
            assigned_object_type = UNSET
        else:
            assigned_object_type = self.assigned_object_type

        assigned_object_id: Union[None, Unset, int]
        if isinstance(self.assigned_object_id, Unset):
            assigned_object_id = UNSET
        else:
            assigned_object_id = self.assigned_object_id

        nat_inside: Union[Dict[str, Any], None, Unset]
        if isinstance(self.nat_inside, Unset):
            nat_inside = UNSET
        elif isinstance(self.nat_inside, NestedIPAddress):
            nat_inside = self.nat_inside.to_dict()
        else:
            nat_inside = self.nat_inside

        dns_name = self.dns_name

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "family": family,
                "address": address,
                "assigned_object": assigned_object,
                "nat_outside": nat_outside,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if assigned_object_type is not UNSET:
            field_dict["assigned_object_type"] = assigned_object_type
        if assigned_object_id is not UNSET:
            field_dict["assigned_object_id"] = assigned_object_id
        if nat_inside is not UNSET:
            field_dict["nat_inside"] = nat_inside
        if dns_name is not UNSET:
            field_dict["dns_name"] = dns_name
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.ip_address_custom_fields import IPAddressCustomFields
        from ..models.ip_address_family import IPAddressFamily
        from ..models.ip_address_role import IPAddressRole
        from ..models.ip_address_status import IPAddressStatus
        from ..models.nested_ip_address import NestedIPAddress
        from ..models.nested_tag import NestedTag
        from ..models.nested_tenant import NestedTenant
        from ..models.nested_vrf import NestedVRF

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        family = IPAddressFamily.from_dict(d.pop("family"))

        address = d.pop("address")

        assigned_object = d.pop("assigned_object")

        nat_outside = []
        _nat_outside = d.pop("nat_outside")
        for nat_outside_item_data in _nat_outside:
            nat_outside_item = NestedIPAddress.from_dict(nat_outside_item_data)

            nat_outside.append(nat_outside_item)

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        def _parse_vrf(data: object) -> Union["NestedVRF", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                vrf_type_1 = NestedVRF.from_dict(data)

                return vrf_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVRF", None, Unset], data)

        vrf = _parse_vrf(d.pop("vrf", UNSET))

        def _parse_tenant(data: object) -> Union["NestedTenant", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenant.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenant", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, IPAddressStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = IPAddressStatus.from_dict(_status)

        _role = d.pop("role", UNSET)
        role: Union[Unset, IPAddressRole]
        if isinstance(_role, Unset):
            role = UNSET
        else:
            role = IPAddressRole.from_dict(_role)

        def _parse_assigned_object_type(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        assigned_object_type = _parse_assigned_object_type(
            d.pop("assigned_object_type", UNSET)
        )

        def _parse_assigned_object_id(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        assigned_object_id = _parse_assigned_object_id(
            d.pop("assigned_object_id", UNSET)
        )

        def _parse_nat_inside(data: object) -> Union["NestedIPAddress", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                nat_inside_type_1 = NestedIPAddress.from_dict(data)

                return nat_inside_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedIPAddress", None, Unset], data)

        nat_inside = _parse_nat_inside(d.pop("nat_inside", UNSET))

        dns_name = d.pop("dns_name", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, IPAddressCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = IPAddressCustomFields.from_dict(_custom_fields)

        ip_address = cls(
            id=id,
            url=url,
            display=display,
            family=family,
            address=address,
            assigned_object=assigned_object,
            nat_outside=nat_outside,
            created=created,
            last_updated=last_updated,
            vrf=vrf,
            tenant=tenant,
            status=status,
            role=role,
            assigned_object_type=assigned_object_type,
            assigned_object_id=assigned_object_id,
            nat_inside=nat_inside,
            dns_name=dns_name,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        ip_address.additional_properties = d
        return ip_address

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
