import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.container_cap_add_type_0_item_type_1 import ContainerCapAddType0ItemType1
from ..models.container_cap_add_type_0_item_type_2_type_1 import (
    ContainerCapAddType0ItemType2Type1,
)
from ..models.container_cap_add_type_0_item_type_3_type_1 import (
    ContainerCapAddType0ItemType3Type1,
)
from ..models.container_operation import ContainerOperation
from ..models.container_restart_policy import ContainerRestartPolicy
from ..models.container_state import ContainerState
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.bind import Bind
    from ..models.container_custom_fields import ContainerCustomFields
    from ..models.device import Device
    from ..models.env import Env
    from ..models.label import Label
    from ..models.mount import Mount
    from ..models.nested_host import NestedHost
    from ..models.nested_image import NestedImage
    from ..models.nested_tag import NestedTag
    from ..models.network_setting import NetworkSetting
    from ..models.port import Port


T = TypeVar("T", bound="Container")


@_attrs_define
class Container:
    """Container Serializer class

    Attributes:
        id (int):
        url (str):
        display (str):
        host (NestedHost): Nested Host Serializer class
        image (NestedImage): Nested Image Serializer class
        name (str):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        state (Union[Unset, ContainerState]): * `created` - Created
            * `restarting` - Restarting
            * `running` - Running
            * `paused` - Paused
            * `exited` - Exited
            * `dead` - Dead
            * `none` - None
        operation (Union[Unset, ContainerOperation]): * `create` - Create
            * `start` - Start
            * `restart` - Restart
            * `stop` - Stop
            * `recreate` - Recreate
            * `kill` - Kill
            * `none` - None
        status (Union[None, Unset, str]):
        container_id (Union[None, Unset, str]):
        hostname (Union[None, Unset, str]):
        restart_policy (Union[Unset, ContainerRestartPolicy]): * `no` - no
            * `on-failure` - on-failure
            * `always` - always
            * `unless-stopped` - unless-stopped
        cap_add (Union[List[Union[ContainerCapAddType0ItemType1, ContainerCapAddType0ItemType2Type1,
            ContainerCapAddType0ItemType3Type1, None]], None, Unset]):
        ports (Union[Unset, List['Port']]):
        env (Union[Unset, List['Env']]):
        labels (Union[Unset, List['Label']]):
        mounts (Union[Unset, List['Mount']]):
        binds (Union[Unset, List['Bind']]):
        network_settings (Union[Unset, List['NetworkSetting']]):
        devices (Union[Unset, List['Device']]):
        custom_fields (Union[Unset, ContainerCustomFields]):
        tags (Union[Unset, List['NestedTag']]):
    """

    id: int
    url: str
    display: str
    host: "NestedHost"
    image: "NestedImage"
    name: str
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    state: Union[Unset, ContainerState] = UNSET
    operation: Union[Unset, ContainerOperation] = UNSET
    status: Union[None, Unset, str] = UNSET
    container_id: Union[None, Unset, str] = UNSET
    hostname: Union[None, Unset, str] = UNSET
    restart_policy: Union[Unset, ContainerRestartPolicy] = UNSET
    cap_add: Union[
        List[
            Union[
                ContainerCapAddType0ItemType1,
                ContainerCapAddType0ItemType2Type1,
                ContainerCapAddType0ItemType3Type1,
                None,
            ]
        ],
        None,
        Unset,
    ] = UNSET
    ports: Union[Unset, List["Port"]] = UNSET
    env: Union[Unset, List["Env"]] = UNSET
    labels: Union[Unset, List["Label"]] = UNSET
    mounts: Union[Unset, List["Mount"]] = UNSET
    binds: Union[Unset, List["Bind"]] = UNSET
    network_settings: Union[Unset, List["NetworkSetting"]] = UNSET
    devices: Union[Unset, List["Device"]] = UNSET
    custom_fields: Union[Unset, "ContainerCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        host = self.host.to_dict()

        image = self.image.to_dict()

        name = self.name

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        state: Union[Unset, str] = UNSET
        if not isinstance(self.state, Unset):
            state = self.state.value

        operation: Union[Unset, str] = UNSET
        if not isinstance(self.operation, Unset):
            operation = self.operation.value

        status: Union[None, Unset, str]
        if isinstance(self.status, Unset):
            status = UNSET
        else:
            status = self.status

        container_id: Union[None, Unset, str]
        if isinstance(self.container_id, Unset):
            container_id = UNSET
        else:
            container_id = self.container_id

        hostname: Union[None, Unset, str]
        if isinstance(self.hostname, Unset):
            hostname = UNSET
        else:
            hostname = self.hostname

        restart_policy: Union[Unset, str] = UNSET
        if not isinstance(self.restart_policy, Unset):
            restart_policy = self.restart_policy.value

        cap_add: Union[List[Union[None, str]], None, Unset]
        if isinstance(self.cap_add, Unset):
            cap_add = UNSET
        elif isinstance(self.cap_add, list):
            cap_add = []
            for cap_add_type_0_item_data in self.cap_add:
                cap_add_type_0_item: Union[None, str]
                if isinstance(cap_add_type_0_item_data, ContainerCapAddType0ItemType1):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                elif isinstance(
                    cap_add_type_0_item_data, ContainerCapAddType0ItemType2Type1
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                elif isinstance(
                    cap_add_type_0_item_data, ContainerCapAddType0ItemType3Type1
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                else:
                    cap_add_type_0_item = cap_add_type_0_item_data
                cap_add.append(cap_add_type_0_item)

        else:
            cap_add = self.cap_add

        ports: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.ports, Unset):
            ports = []
            for ports_item_data in self.ports:
                ports_item = ports_item_data.to_dict()
                ports.append(ports_item)

        env: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.env, Unset):
            env = []
            for env_item_data in self.env:
                env_item = env_item_data.to_dict()
                env.append(env_item)

        labels: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.labels, Unset):
            labels = []
            for labels_item_data in self.labels:
                labels_item = labels_item_data.to_dict()
                labels.append(labels_item)

        mounts: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.mounts, Unset):
            mounts = []
            for mounts_item_data in self.mounts:
                mounts_item = mounts_item_data.to_dict()
                mounts.append(mounts_item)

        binds: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.binds, Unset):
            binds = []
            for binds_item_data in self.binds:
                binds_item = binds_item_data.to_dict()
                binds.append(binds_item)

        network_settings: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.network_settings, Unset):
            network_settings = []
            for network_settings_item_data in self.network_settings:
                network_settings_item = network_settings_item_data.to_dict()
                network_settings.append(network_settings_item)

        devices: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.devices, Unset):
            devices = []
            for devices_item_data in self.devices:
                devices_item = devices_item_data.to_dict()
                devices.append(devices_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "host": host,
                "image": image,
                "name": name,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if state is not UNSET:
            field_dict["state"] = state
        if operation is not UNSET:
            field_dict["operation"] = operation
        if status is not UNSET:
            field_dict["status"] = status
        if container_id is not UNSET:
            field_dict["ContainerID"] = container_id
        if hostname is not UNSET:
            field_dict["hostname"] = hostname
        if restart_policy is not UNSET:
            field_dict["restart_policy"] = restart_policy
        if cap_add is not UNSET:
            field_dict["cap_add"] = cap_add
        if ports is not UNSET:
            field_dict["ports"] = ports
        if env is not UNSET:
            field_dict["env"] = env
        if labels is not UNSET:
            field_dict["labels"] = labels
        if mounts is not UNSET:
            field_dict["mounts"] = mounts
        if binds is not UNSET:
            field_dict["binds"] = binds
        if network_settings is not UNSET:
            field_dict["network_settings"] = network_settings
        if devices is not UNSET:
            field_dict["devices"] = devices
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.bind import Bind
        from ..models.container_custom_fields import ContainerCustomFields
        from ..models.device import Device
        from ..models.env import Env
        from ..models.label import Label
        from ..models.mount import Mount
        from ..models.nested_host import NestedHost
        from ..models.nested_image import NestedImage
        from ..models.nested_tag import NestedTag
        from ..models.network_setting import NetworkSetting
        from ..models.port import Port

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        host = NestedHost.from_dict(d.pop("host"))

        image = NestedImage.from_dict(d.pop("image"))

        name = d.pop("name")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        _state = d.pop("state", UNSET)
        state: Union[Unset, ContainerState]
        if isinstance(_state, Unset):
            state = UNSET
        else:
            state = ContainerState(_state)

        _operation = d.pop("operation", UNSET)
        operation: Union[Unset, ContainerOperation]
        if isinstance(_operation, Unset):
            operation = UNSET
        else:
            operation = ContainerOperation(_operation)

        def _parse_status(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        status = _parse_status(d.pop("status", UNSET))

        def _parse_container_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        container_id = _parse_container_id(d.pop("ContainerID", UNSET))

        def _parse_hostname(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        hostname = _parse_hostname(d.pop("hostname", UNSET))

        _restart_policy = d.pop("restart_policy", UNSET)
        restart_policy: Union[Unset, ContainerRestartPolicy]
        if isinstance(_restart_policy, Unset):
            restart_policy = UNSET
        else:
            restart_policy = ContainerRestartPolicy(_restart_policy)

        def _parse_cap_add(
            data: object,
        ) -> Union[
            List[
                Union[
                    ContainerCapAddType0ItemType1,
                    ContainerCapAddType0ItemType2Type1,
                    ContainerCapAddType0ItemType3Type1,
                    None,
                ]
            ],
            None,
            Unset,
        ]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                cap_add_type_0 = []
                _cap_add_type_0 = data
                for cap_add_type_0_item_data in _cap_add_type_0:

                    def _parse_cap_add_type_0_item(
                        data: object,
                    ) -> Union[
                        ContainerCapAddType0ItemType1,
                        ContainerCapAddType0ItemType2Type1,
                        ContainerCapAddType0ItemType3Type1,
                        None,
                    ]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_1 = ContainerCapAddType0ItemType1(
                                data
                            )

                            return cap_add_type_0_item_type_1
                        except:  # noqa: E722
                            pass
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_2_type_1 = (
                                ContainerCapAddType0ItemType2Type1(data)
                            )

                            return cap_add_type_0_item_type_2_type_1
                        except:  # noqa: E722
                            pass
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_3_type_1 = (
                                ContainerCapAddType0ItemType3Type1(data)
                            )

                            return cap_add_type_0_item_type_3_type_1
                        except:  # noqa: E722
                            pass
                        return cast(
                            Union[
                                ContainerCapAddType0ItemType1,
                                ContainerCapAddType0ItemType2Type1,
                                ContainerCapAddType0ItemType3Type1,
                                None,
                            ],
                            data,
                        )

                    cap_add_type_0_item = _parse_cap_add_type_0_item(
                        cap_add_type_0_item_data
                    )

                    cap_add_type_0.append(cap_add_type_0_item)

                return cap_add_type_0
            except:  # noqa: E722
                pass
            return cast(
                Union[
                    List[
                        Union[
                            ContainerCapAddType0ItemType1,
                            ContainerCapAddType0ItemType2Type1,
                            ContainerCapAddType0ItemType3Type1,
                            None,
                        ]
                    ],
                    None,
                    Unset,
                ],
                data,
            )

        cap_add = _parse_cap_add(d.pop("cap_add", UNSET))

        ports = []
        _ports = d.pop("ports", UNSET)
        for ports_item_data in _ports or []:
            ports_item = Port.from_dict(ports_item_data)

            ports.append(ports_item)

        env = []
        _env = d.pop("env", UNSET)
        for env_item_data in _env or []:
            env_item = Env.from_dict(env_item_data)

            env.append(env_item)

        labels = []
        _labels = d.pop("labels", UNSET)
        for labels_item_data in _labels or []:
            labels_item = Label.from_dict(labels_item_data)

            labels.append(labels_item)

        mounts = []
        _mounts = d.pop("mounts", UNSET)
        for mounts_item_data in _mounts or []:
            mounts_item = Mount.from_dict(mounts_item_data)

            mounts.append(mounts_item)

        binds = []
        _binds = d.pop("binds", UNSET)
        for binds_item_data in _binds or []:
            binds_item = Bind.from_dict(binds_item_data)

            binds.append(binds_item)

        network_settings = []
        _network_settings = d.pop("network_settings", UNSET)
        for network_settings_item_data in _network_settings or []:
            network_settings_item = NetworkSetting.from_dict(network_settings_item_data)

            network_settings.append(network_settings_item)

        devices = []
        _devices = d.pop("devices", UNSET)
        for devices_item_data in _devices or []:
            devices_item = Device.from_dict(devices_item_data)

            devices.append(devices_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ContainerCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ContainerCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        container = cls(
            id=id,
            url=url,
            display=display,
            host=host,
            image=image,
            name=name,
            created=created,
            last_updated=last_updated,
            state=state,
            operation=operation,
            status=status,
            container_id=container_id,
            hostname=hostname,
            restart_policy=restart_policy,
            cap_add=cap_add,
            ports=ports,
            env=env,
            labels=labels,
            mounts=mounts,
            binds=binds,
            network_settings=network_settings,
            devices=devices,
            custom_fields=custom_fields,
            tags=tags,
        )

        container.additional_properties = d
        return container

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
