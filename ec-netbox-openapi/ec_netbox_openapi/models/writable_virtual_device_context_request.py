import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_virtual_device_context_request_status import (
    WritableVirtualDeviceContextRequestStatus,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_virtual_device_context_request_custom_fields import (
        WritableVirtualDeviceContextRequestCustomFields,
    )


T = TypeVar("T", bound="WritableVirtualDeviceContextRequest")


@_attrs_define
class WritableVirtualDeviceContextRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        status (WritableVirtualDeviceContextRequestStatus): * `active` - Active
            * `planned` - Planned
            * `offline` - Offline
        device (Union[None, Unset, int]):
        identifier (Union[None, Unset, int]): Numeric identifier unique to the parent device
        tenant (Union[None, Unset, int]):
        primary_ip4 (Union[None, Unset, int]):
        primary_ip6 (Union[None, Unset, int]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritableVirtualDeviceContextRequestCustomFields]):
    """

    name: str
    status: WritableVirtualDeviceContextRequestStatus
    device: Union[None, Unset, int] = UNSET
    identifier: Union[None, Unset, int] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    primary_ip4: Union[None, Unset, int] = UNSET
    primary_ip6: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WritableVirtualDeviceContextRequestCustomFields"] = (
        UNSET
    )
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        status = self.status.value

        device: Union[None, Unset, int]
        if isinstance(self.device, Unset):
            device = UNSET
        else:
            device = self.device

        identifier: Union[None, Unset, int]
        if isinstance(self.identifier, Unset):
            identifier = UNSET
        else:
            identifier = self.identifier

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        primary_ip4: Union[None, Unset, int]
        if isinstance(self.primary_ip4, Unset):
            primary_ip4 = UNSET
        else:
            primary_ip4 = self.primary_ip4

        primary_ip6: Union[None, Unset, int]
        if isinstance(self.primary_ip6, Unset):
            primary_ip6 = UNSET
        else:
            primary_ip6 = self.primary_ip6

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "status": status,
            }
        )
        if device is not UNSET:
            field_dict["device"] = device
        if identifier is not UNSET:
            field_dict["identifier"] = identifier
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if primary_ip4 is not UNSET:
            field_dict["primary_ip4"] = primary_ip4
        if primary_ip6 is not UNSET:
            field_dict["primary_ip6"] = primary_ip6
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (None, str(self.name).encode(), "text/plain")

        status = (None, str(self.status.value).encode(), "text/plain")

        device: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.device, Unset):
            device = UNSET
        elif isinstance(self.device, int):
            device = (None, str(self.device).encode(), "text/plain")
        else:
            device = (None, str(self.device).encode(), "text/plain")

        identifier: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.identifier, Unset):
            identifier = UNSET
        elif isinstance(self.identifier, int):
            identifier = (None, str(self.identifier).encode(), "text/plain")
        else:
            identifier = (None, str(self.identifier).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        primary_ip4: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.primary_ip4, Unset):
            primary_ip4 = UNSET
        elif isinstance(self.primary_ip4, int):
            primary_ip4 = (None, str(self.primary_ip4).encode(), "text/plain")
        else:
            primary_ip4 = (None, str(self.primary_ip4).encode(), "text/plain")

        primary_ip6: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.primary_ip6, Unset):
            primary_ip6 = UNSET
        elif isinstance(self.primary_ip6, int):
            primary_ip6 = (None, str(self.primary_ip6).encode(), "text/plain")
        else:
            primary_ip6 = (None, str(self.primary_ip6).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "name": name,
                "status": status,
            }
        )
        if device is not UNSET:
            field_dict["device"] = device
        if identifier is not UNSET:
            field_dict["identifier"] = identifier
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if primary_ip4 is not UNSET:
            field_dict["primary_ip4"] = primary_ip4
        if primary_ip6 is not UNSET:
            field_dict["primary_ip6"] = primary_ip6
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_virtual_device_context_request_custom_fields import (
            WritableVirtualDeviceContextRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name")

        status = WritableVirtualDeviceContextRequestStatus(d.pop("status"))

        def _parse_device(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        device = _parse_device(d.pop("device", UNSET))

        def _parse_identifier(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        identifier = _parse_identifier(d.pop("identifier", UNSET))

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        def _parse_primary_ip4(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        primary_ip4 = _parse_primary_ip4(d.pop("primary_ip4", UNSET))

        def _parse_primary_ip6(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        primary_ip6 = _parse_primary_ip6(d.pop("primary_ip6", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableVirtualDeviceContextRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableVirtualDeviceContextRequestCustomFields.from_dict(
                _custom_fields
            )

        writable_virtual_device_context_request = cls(
            name=name,
            status=status,
            device=device,
            identifier=identifier,
            tenant=tenant,
            primary_ip4=primary_ip4,
            primary_ip6=primary_ip6,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_virtual_device_context_request.additional_properties = d
        return writable_virtual_device_context_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
