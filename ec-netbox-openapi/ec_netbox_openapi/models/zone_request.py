from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.zone_request_status import ZoneRequestStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_name_server_request import NestedNameServerRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_tenant_request import NestedTenantRequest
    from ..models.nested_view_request import NestedViewRequest
    from ..models.zone_request_custom_fields import ZoneRequestCustomFields


T = TypeVar("T", bound="ZoneRequest")


@_attrs_define
class ZoneRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        soa_ttl (int):
        soa_rname (str):
        soa_refresh (int):
        soa_retry (int):
        soa_expire (int):
        soa_minimum (int):
        view (Union[Unset, NestedViewRequest]): Represents an object related through a ForeignKey field. On write, it
            accepts a primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        nameservers (Union[Unset, List['NestedNameServerRequest']]): Nameservers for the zone
        status (Union[Unset, ZoneRequestStatus]): * `active` - Active
            * `reserved` - Reserved
            * `deprecated` - Deprecated
            * `parked` - Parked
        description (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        default_ttl (Union[Unset, int]):
        soa_mname (Union[Unset, NestedNameServerRequest]): Represents an object related through a ForeignKey field. On
            write, it accepts a primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        soa_serial (Union[None, Unset, int]):
        soa_serial_auto (Union[Unset, bool]): Automatically generate the SOA Serial field
        custom_fields (Union[Unset, ZoneRequestCustomFields]):
        tenant (Union['NestedTenantRequest', None, Unset]):
    """

    name: str
    soa_ttl: int
    soa_rname: str
    soa_refresh: int
    soa_retry: int
    soa_expire: int
    soa_minimum: int
    view: Union[Unset, "NestedViewRequest"] = UNSET
    nameservers: Union[Unset, List["NestedNameServerRequest"]] = UNSET
    status: Union[Unset, ZoneRequestStatus] = UNSET
    description: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    default_ttl: Union[Unset, int] = UNSET
    soa_mname: Union[Unset, "NestedNameServerRequest"] = UNSET
    soa_serial: Union[None, Unset, int] = UNSET
    soa_serial_auto: Union[Unset, bool] = UNSET
    custom_fields: Union[Unset, "ZoneRequestCustomFields"] = UNSET
    tenant: Union["NestedTenantRequest", None, Unset] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_tenant_request import NestedTenantRequest

        name = self.name

        soa_ttl = self.soa_ttl

        soa_rname = self.soa_rname

        soa_refresh = self.soa_refresh

        soa_retry = self.soa_retry

        soa_expire = self.soa_expire

        soa_minimum = self.soa_minimum

        view: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.view, Unset):
            view = self.view.to_dict()

        nameservers: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.nameservers, Unset):
            nameservers = []
            for nameservers_item_data in self.nameservers:
                nameservers_item = nameservers_item_data.to_dict()
                nameservers.append(nameservers_item)

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        description = self.description

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        default_ttl = self.default_ttl

        soa_mname: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.soa_mname, Unset):
            soa_mname = self.soa_mname.to_dict()

        soa_serial: Union[None, Unset, int]
        if isinstance(self.soa_serial, Unset):
            soa_serial = UNSET
        else:
            soa_serial = self.soa_serial

        soa_serial_auto = self.soa_serial_auto

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenantRequest):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "soa_ttl": soa_ttl,
                "soa_rname": soa_rname,
                "soa_refresh": soa_refresh,
                "soa_retry": soa_retry,
                "soa_expire": soa_expire,
                "soa_minimum": soa_minimum,
            }
        )
        if view is not UNSET:
            field_dict["view"] = view
        if nameservers is not UNSET:
            field_dict["nameservers"] = nameservers
        if status is not UNSET:
            field_dict["status"] = status
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if default_ttl is not UNSET:
            field_dict["default_ttl"] = default_ttl
        if soa_mname is not UNSET:
            field_dict["soa_mname"] = soa_mname
        if soa_serial is not UNSET:
            field_dict["soa_serial"] = soa_serial
        if soa_serial_auto is not UNSET:
            field_dict["soa_serial_auto"] = soa_serial_auto
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tenant is not UNSET:
            field_dict["tenant"] = tenant

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_name_server_request import NestedNameServerRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_tenant_request import NestedTenantRequest
        from ..models.nested_view_request import NestedViewRequest
        from ..models.zone_request_custom_fields import ZoneRequestCustomFields

        d = src_dict.copy()
        name = d.pop("name")

        soa_ttl = d.pop("soa_ttl")

        soa_rname = d.pop("soa_rname")

        soa_refresh = d.pop("soa_refresh")

        soa_retry = d.pop("soa_retry")

        soa_expire = d.pop("soa_expire")

        soa_minimum = d.pop("soa_minimum")

        _view = d.pop("view", UNSET)
        view: Union[Unset, NestedViewRequest]
        if isinstance(_view, Unset):
            view = UNSET
        else:
            view = NestedViewRequest.from_dict(_view)

        nameservers = []
        _nameservers = d.pop("nameservers", UNSET)
        for nameservers_item_data in _nameservers or []:
            nameservers_item = NestedNameServerRequest.from_dict(nameservers_item_data)

            nameservers.append(nameservers_item)

        _status = d.pop("status", UNSET)
        status: Union[Unset, ZoneRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = ZoneRequestStatus(_status)

        description = d.pop("description", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        default_ttl = d.pop("default_ttl", UNSET)

        _soa_mname = d.pop("soa_mname", UNSET)
        soa_mname: Union[Unset, NestedNameServerRequest]
        if isinstance(_soa_mname, Unset):
            soa_mname = UNSET
        else:
            soa_mname = NestedNameServerRequest.from_dict(_soa_mname)

        def _parse_soa_serial(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        soa_serial = _parse_soa_serial(d.pop("soa_serial", UNSET))

        soa_serial_auto = d.pop("soa_serial_auto", UNSET)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ZoneRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ZoneRequestCustomFields.from_dict(_custom_fields)

        def _parse_tenant(data: object) -> Union["NestedTenantRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenantRequest.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenantRequest", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        zone_request = cls(
            name=name,
            soa_ttl=soa_ttl,
            soa_rname=soa_rname,
            soa_refresh=soa_refresh,
            soa_retry=soa_retry,
            soa_expire=soa_expire,
            soa_minimum=soa_minimum,
            view=view,
            nameservers=nameservers,
            status=status,
            description=description,
            tags=tags,
            default_ttl=default_ttl,
            soa_mname=soa_mname,
            soa_serial=soa_serial,
            soa_serial_auto=soa_serial_auto,
            custom_fields=custom_fields,
            tenant=tenant,
        )

        zone_request.additional_properties = d
        return zone_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
