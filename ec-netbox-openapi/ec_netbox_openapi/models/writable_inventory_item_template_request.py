from typing import Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="WritableInventoryItemTemplateRequest")


@_attrs_define
class WritableInventoryItemTemplateRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            device_type (int):
            name (str): {module} is accepted as a substitution for the module bay position when attached to a module type.
            parent (Union[None, Unset, int]):
            label (Union[Unset, str]): Physical label
            role (Union[None, Unset, int]):
            manufacturer (Union[None, Unset, int]):
            part_id (Union[Unset, str]): Manufacturer-assigned part identifier
            description (Union[Unset, str]):
            component_type (Union[None, Unset, str]):
            component_id (Union[None, Unset, int]):
    """

    device_type: int
    name: str
    parent: Union[None, Unset, int] = UNSET
    label: Union[Unset, str] = UNSET
    role: Union[None, Unset, int] = UNSET
    manufacturer: Union[None, Unset, int] = UNSET
    part_id: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    component_type: Union[None, Unset, str] = UNSET
    component_id: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        device_type = self.device_type

        name = self.name

        parent: Union[None, Unset, int]
        if isinstance(self.parent, Unset):
            parent = UNSET
        else:
            parent = self.parent

        label = self.label

        role: Union[None, Unset, int]
        if isinstance(self.role, Unset):
            role = UNSET
        else:
            role = self.role

        manufacturer: Union[None, Unset, int]
        if isinstance(self.manufacturer, Unset):
            manufacturer = UNSET
        else:
            manufacturer = self.manufacturer

        part_id = self.part_id

        description = self.description

        component_type: Union[None, Unset, str]
        if isinstance(self.component_type, Unset):
            component_type = UNSET
        else:
            component_type = self.component_type

        component_id: Union[None, Unset, int]
        if isinstance(self.component_id, Unset):
            component_id = UNSET
        else:
            component_id = self.component_id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "device_type": device_type,
                "name": name,
            }
        )
        if parent is not UNSET:
            field_dict["parent"] = parent
        if label is not UNSET:
            field_dict["label"] = label
        if role is not UNSET:
            field_dict["role"] = role
        if manufacturer is not UNSET:
            field_dict["manufacturer"] = manufacturer
        if part_id is not UNSET:
            field_dict["part_id"] = part_id
        if description is not UNSET:
            field_dict["description"] = description
        if component_type is not UNSET:
            field_dict["component_type"] = component_type
        if component_id is not UNSET:
            field_dict["component_id"] = component_id

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        device_type = (None, str(self.device_type).encode(), "text/plain")

        name = (None, str(self.name).encode(), "text/plain")

        parent: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.parent, Unset):
            parent = UNSET
        elif isinstance(self.parent, int):
            parent = (None, str(self.parent).encode(), "text/plain")
        else:
            parent = (None, str(self.parent).encode(), "text/plain")

        label = (
            self.label
            if isinstance(self.label, Unset)
            else (None, str(self.label).encode(), "text/plain")
        )

        role: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.role, Unset):
            role = UNSET
        elif isinstance(self.role, int):
            role = (None, str(self.role).encode(), "text/plain")
        else:
            role = (None, str(self.role).encode(), "text/plain")

        manufacturer: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.manufacturer, Unset):
            manufacturer = UNSET
        elif isinstance(self.manufacturer, int):
            manufacturer = (None, str(self.manufacturer).encode(), "text/plain")
        else:
            manufacturer = (None, str(self.manufacturer).encode(), "text/plain")

        part_id = (
            self.part_id
            if isinstance(self.part_id, Unset)
            else (None, str(self.part_id).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        component_type: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.component_type, Unset):
            component_type = UNSET
        elif isinstance(self.component_type, str):
            component_type = (None, str(self.component_type).encode(), "text/plain")
        else:
            component_type = (None, str(self.component_type).encode(), "text/plain")

        component_id: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.component_id, Unset):
            component_id = UNSET
        elif isinstance(self.component_id, int):
            component_id = (None, str(self.component_id).encode(), "text/plain")
        else:
            component_id = (None, str(self.component_id).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "device_type": device_type,
                "name": name,
            }
        )
        if parent is not UNSET:
            field_dict["parent"] = parent
        if label is not UNSET:
            field_dict["label"] = label
        if role is not UNSET:
            field_dict["role"] = role
        if manufacturer is not UNSET:
            field_dict["manufacturer"] = manufacturer
        if part_id is not UNSET:
            field_dict["part_id"] = part_id
        if description is not UNSET:
            field_dict["description"] = description
        if component_type is not UNSET:
            field_dict["component_type"] = component_type
        if component_id is not UNSET:
            field_dict["component_id"] = component_id

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        device_type = d.pop("device_type")

        name = d.pop("name")

        def _parse_parent(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        parent = _parse_parent(d.pop("parent", UNSET))

        label = d.pop("label", UNSET)

        def _parse_role(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        role = _parse_role(d.pop("role", UNSET))

        def _parse_manufacturer(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        manufacturer = _parse_manufacturer(d.pop("manufacturer", UNSET))

        part_id = d.pop("part_id", UNSET)

        description = d.pop("description", UNSET)

        def _parse_component_type(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        component_type = _parse_component_type(d.pop("component_type", UNSET))

        def _parse_component_id(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        component_id = _parse_component_id(d.pop("component_id", UNSET))

        writable_inventory_item_template_request = cls(
            device_type=device_type,
            name=name,
            parent=parent,
            label=label,
            role=role,
            manufacturer=manufacturer,
            part_id=part_id,
            description=description,
            component_type=component_type,
            component_id=component_id,
        )

        writable_inventory_item_template_request.additional_properties = d
        return writable_inventory_item_template_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
