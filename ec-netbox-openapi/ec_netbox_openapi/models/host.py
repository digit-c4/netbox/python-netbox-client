import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.host_operation import HostOperation
from ..models.host_state import HostState
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.host_custom_fields import HostCustomFields
    from ..models.nested_container import NestedContainer
    from ..models.nested_image import NestedImage
    from ..models.nested_network import NestedNetwork
    from ..models.nested_registry import NestedRegistry
    from ..models.nested_tag import NestedTag
    from ..models.nested_token import NestedToken
    from ..models.nested_volume import NestedVolume


T = TypeVar("T", bound="Host")


@_attrs_define
class Host:
    """Host Serializer class

    Attributes:
        id (int):
        url (str):
        display (str):
        endpoint (str):
        name (str):
        token (NestedToken): Represents an object related through a ForeignKey field. On write, it accepts a primary key
            (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        images (List['NestedImage']):
        volumes (List['NestedVolume']):
        networks (List['NestedNetwork']):
        containers (List['NestedContainer']):
        registries (List['NestedRegistry']):
        state (Union[Unset, HostState]): * `created` - Created
            * `running` - Running
            * `deleted` - Deleted
            * `refreshing` - Refreshing
        netbox_base_url (Union[None, Unset, str]):
        agent_version (Union[None, Unset, str]):
        docker_api_version (Union[None, Unset, str]):
        operation (Union[Unset, HostOperation]): * `refresh` - Refresh
            * `none` - None
        custom_fields (Union[Unset, HostCustomFields]):
        tags (Union[Unset, List['NestedTag']]):
    """

    id: int
    url: str
    display: str
    endpoint: str
    name: str
    token: "NestedToken"
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    images: List["NestedImage"]
    volumes: List["NestedVolume"]
    networks: List["NestedNetwork"]
    containers: List["NestedContainer"]
    registries: List["NestedRegistry"]
    state: Union[Unset, HostState] = UNSET
    netbox_base_url: Union[None, Unset, str] = UNSET
    agent_version: Union[None, Unset, str] = UNSET
    docker_api_version: Union[None, Unset, str] = UNSET
    operation: Union[Unset, HostOperation] = UNSET
    custom_fields: Union[Unset, "HostCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        endpoint = self.endpoint

        name = self.name

        token = self.token.to_dict()

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        images = []
        for images_item_data in self.images:
            images_item = images_item_data.to_dict()
            images.append(images_item)

        volumes = []
        for volumes_item_data in self.volumes:
            volumes_item = volumes_item_data.to_dict()
            volumes.append(volumes_item)

        networks = []
        for networks_item_data in self.networks:
            networks_item = networks_item_data.to_dict()
            networks.append(networks_item)

        containers = []
        for containers_item_data in self.containers:
            containers_item = containers_item_data.to_dict()
            containers.append(containers_item)

        registries = []
        for registries_item_data in self.registries:
            registries_item = registries_item_data.to_dict()
            registries.append(registries_item)

        state: Union[Unset, str] = UNSET
        if not isinstance(self.state, Unset):
            state = self.state.value

        netbox_base_url: Union[None, Unset, str]
        if isinstance(self.netbox_base_url, Unset):
            netbox_base_url = UNSET
        else:
            netbox_base_url = self.netbox_base_url

        agent_version: Union[None, Unset, str]
        if isinstance(self.agent_version, Unset):
            agent_version = UNSET
        else:
            agent_version = self.agent_version

        docker_api_version: Union[None, Unset, str]
        if isinstance(self.docker_api_version, Unset):
            docker_api_version = UNSET
        else:
            docker_api_version = self.docker_api_version

        operation: Union[Unset, str] = UNSET
        if not isinstance(self.operation, Unset):
            operation = self.operation.value

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "endpoint": endpoint,
                "name": name,
                "token": token,
                "created": created,
                "last_updated": last_updated,
                "images": images,
                "volumes": volumes,
                "networks": networks,
                "containers": containers,
                "registries": registries,
            }
        )
        if state is not UNSET:
            field_dict["state"] = state
        if netbox_base_url is not UNSET:
            field_dict["netbox_base_url"] = netbox_base_url
        if agent_version is not UNSET:
            field_dict["agent_version"] = agent_version
        if docker_api_version is not UNSET:
            field_dict["docker_api_version"] = docker_api_version
        if operation is not UNSET:
            field_dict["operation"] = operation
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.host_custom_fields import HostCustomFields
        from ..models.nested_container import NestedContainer
        from ..models.nested_image import NestedImage
        from ..models.nested_network import NestedNetwork
        from ..models.nested_registry import NestedRegistry
        from ..models.nested_tag import NestedTag
        from ..models.nested_token import NestedToken
        from ..models.nested_volume import NestedVolume

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        endpoint = d.pop("endpoint")

        name = d.pop("name")

        token = NestedToken.from_dict(d.pop("token"))

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        images = []
        _images = d.pop("images")
        for images_item_data in _images:
            images_item = NestedImage.from_dict(images_item_data)

            images.append(images_item)

        volumes = []
        _volumes = d.pop("volumes")
        for volumes_item_data in _volumes:
            volumes_item = NestedVolume.from_dict(volumes_item_data)

            volumes.append(volumes_item)

        networks = []
        _networks = d.pop("networks")
        for networks_item_data in _networks:
            networks_item = NestedNetwork.from_dict(networks_item_data)

            networks.append(networks_item)

        containers = []
        _containers = d.pop("containers")
        for containers_item_data in _containers:
            containers_item = NestedContainer.from_dict(containers_item_data)

            containers.append(containers_item)

        registries = []
        _registries = d.pop("registries")
        for registries_item_data in _registries:
            registries_item = NestedRegistry.from_dict(registries_item_data)

            registries.append(registries_item)

        _state = d.pop("state", UNSET)
        state: Union[Unset, HostState]
        if isinstance(_state, Unset):
            state = UNSET
        else:
            state = HostState(_state)

        def _parse_netbox_base_url(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        netbox_base_url = _parse_netbox_base_url(d.pop("netbox_base_url", UNSET))

        def _parse_agent_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        agent_version = _parse_agent_version(d.pop("agent_version", UNSET))

        def _parse_docker_api_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        docker_api_version = _parse_docker_api_version(
            d.pop("docker_api_version", UNSET)
        )

        _operation = d.pop("operation", UNSET)
        operation: Union[Unset, HostOperation]
        if isinstance(_operation, Unset):
            operation = UNSET
        else:
            operation = HostOperation(_operation)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, HostCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = HostCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        host = cls(
            id=id,
            url=url,
            display=display,
            endpoint=endpoint,
            name=name,
            token=token,
            created=created,
            last_updated=last_updated,
            images=images,
            volumes=volumes,
            networks=networks,
            containers=containers,
            registries=registries,
            state=state,
            netbox_base_url=netbox_base_url,
            agent_version=agent_version,
            docker_api_version=docker_api_version,
            operation=operation,
            custom_fields=custom_fields,
            tags=tags,
        )

        host.additional_properties = d
        return host

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
