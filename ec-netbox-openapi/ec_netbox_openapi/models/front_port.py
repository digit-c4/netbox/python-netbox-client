import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.component_nested_module import ComponentNestedModule
    from ..models.front_port_custom_fields import FrontPortCustomFields
    from ..models.front_port_rear_port import FrontPortRearPort
    from ..models.front_port_type import FrontPortType
    from ..models.nested_cable import NestedCable
    from ..models.nested_device import NestedDevice
    from ..models.nested_tag import NestedTag


T = TypeVar("T", bound="FrontPort")


@_attrs_define
class FrontPort:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        device (NestedDevice): Represents an object related through a ForeignKey field. On write, it accepts a primary
            key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (str):
        type (FrontPortType):
        rear_port (FrontPortRearPort): NestedRearPortSerializer but with parent device omitted (since front and rear
            ports must belong to same device)
        cable (Union['NestedCable', None]):
        cable_end (str):
        link_peers (List[Any]):
        link_peers_type (str): Return the type of the peer link terminations, or None.
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        field_occupied (bool):
        module (Union['ComponentNestedModule', None, Unset]):
        label (Union[Unset, str]): Physical label
        color (Union[Unset, str]):
        rear_port_position (Union[Unset, int]): Mapped position on corresponding rear port
        description (Union[Unset, str]):
        mark_connected (Union[Unset, bool]): Treat as if a cable is connected
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, FrontPortCustomFields]):
    """

    id: int
    url: str
    display: str
    device: "NestedDevice"
    name: str
    type: "FrontPortType"
    rear_port: "FrontPortRearPort"
    cable: Union["NestedCable", None]
    cable_end: str
    link_peers: List[Any]
    link_peers_type: str
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    field_occupied: bool
    module: Union["ComponentNestedModule", None, Unset] = UNSET
    label: Union[Unset, str] = UNSET
    color: Union[Unset, str] = UNSET
    rear_port_position: Union[Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    mark_connected: Union[Unset, bool] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "FrontPortCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.component_nested_module import ComponentNestedModule
        from ..models.nested_cable import NestedCable

        id = self.id

        url = self.url

        display = self.display

        device = self.device.to_dict()

        name = self.name

        type = self.type.to_dict()

        rear_port = self.rear_port.to_dict()

        cable: Union[Dict[str, Any], None]
        if isinstance(self.cable, NestedCable):
            cable = self.cable.to_dict()
        else:
            cable = self.cable

        cable_end = self.cable_end

        link_peers = self.link_peers

        link_peers_type = self.link_peers_type

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        field_occupied = self.field_occupied

        module: Union[Dict[str, Any], None, Unset]
        if isinstance(self.module, Unset):
            module = UNSET
        elif isinstance(self.module, ComponentNestedModule):
            module = self.module.to_dict()
        else:
            module = self.module

        label = self.label

        color = self.color

        rear_port_position = self.rear_port_position

        description = self.description

        mark_connected = self.mark_connected

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "device": device,
                "name": name,
                "type": type,
                "rear_port": rear_port,
                "cable": cable,
                "cable_end": cable_end,
                "link_peers": link_peers,
                "link_peers_type": link_peers_type,
                "created": created,
                "last_updated": last_updated,
                "_occupied": field_occupied,
            }
        )
        if module is not UNSET:
            field_dict["module"] = module
        if label is not UNSET:
            field_dict["label"] = label
        if color is not UNSET:
            field_dict["color"] = color
        if rear_port_position is not UNSET:
            field_dict["rear_port_position"] = rear_port_position
        if description is not UNSET:
            field_dict["description"] = description
        if mark_connected is not UNSET:
            field_dict["mark_connected"] = mark_connected
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.component_nested_module import ComponentNestedModule
        from ..models.front_port_custom_fields import FrontPortCustomFields
        from ..models.front_port_rear_port import FrontPortRearPort
        from ..models.front_port_type import FrontPortType
        from ..models.nested_cable import NestedCable
        from ..models.nested_device import NestedDevice
        from ..models.nested_tag import NestedTag

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        device = NestedDevice.from_dict(d.pop("device"))

        name = d.pop("name")

        type = FrontPortType.from_dict(d.pop("type"))

        rear_port = FrontPortRearPort.from_dict(d.pop("rear_port"))

        def _parse_cable(data: object) -> Union["NestedCable", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                cable_type_1 = NestedCable.from_dict(data)

                return cable_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedCable", None], data)

        cable = _parse_cable(d.pop("cable"))

        cable_end = d.pop("cable_end")

        link_peers = cast(List[Any], d.pop("link_peers"))

        link_peers_type = d.pop("link_peers_type")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        field_occupied = d.pop("_occupied")

        def _parse_module(data: object) -> Union["ComponentNestedModule", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                module_type_1 = ComponentNestedModule.from_dict(data)

                return module_type_1
            except:  # noqa: E722
                pass
            return cast(Union["ComponentNestedModule", None, Unset], data)

        module = _parse_module(d.pop("module", UNSET))

        label = d.pop("label", UNSET)

        color = d.pop("color", UNSET)

        rear_port_position = d.pop("rear_port_position", UNSET)

        description = d.pop("description", UNSET)

        mark_connected = d.pop("mark_connected", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, FrontPortCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = FrontPortCustomFields.from_dict(_custom_fields)

        front_port = cls(
            id=id,
            url=url,
            display=display,
            device=device,
            name=name,
            type=type,
            rear_port=rear_port,
            cable=cable,
            cable_end=cable_end,
            link_peers=link_peers,
            link_peers_type=link_peers_type,
            created=created,
            last_updated=last_updated,
            field_occupied=field_occupied,
            module=module,
            label=label,
            color=color,
            rear_port_position=rear_port_position,
            description=description,
            mark_connected=mark_connected,
            tags=tags,
            custom_fields=custom_fields,
        )

        front_port.additional_properties = d
        return front_port

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
