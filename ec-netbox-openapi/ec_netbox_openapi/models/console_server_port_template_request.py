from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.console_server_port_template_request_type import (
    ConsoleServerPortTemplateRequestType,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_device_type_request import NestedDeviceTypeRequest
    from ..models.nested_module_type_request import NestedModuleTypeRequest


T = TypeVar("T", bound="ConsoleServerPortTemplateRequest")


@_attrs_define
class ConsoleServerPortTemplateRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            name (str): {module} is accepted as a substitution for the module bay position when attached to a module type.
            device_type (Union['NestedDeviceTypeRequest', None, Unset]):
            module_type (Union['NestedModuleTypeRequest', None, Unset]):
            label (Union[Unset, str]): Physical label
            type (Union[Unset, ConsoleServerPortTemplateRequestType]): * `de-9` - DE-9
                * `db-25` - DB-25
                * `rj-11` - RJ-11
                * `rj-12` - RJ-12
                * `rj-45` - RJ-45
                * `mini-din-8` - Mini-DIN 8
                * `usb-a` - USB Type A
                * `usb-b` - USB Type B
                * `usb-c` - USB Type C
                * `usb-mini-a` - USB Mini A
                * `usb-mini-b` - USB Mini B
                * `usb-micro-a` - USB Micro A
                * `usb-micro-b` - USB Micro B
                * `usb-micro-ab` - USB Micro AB
                * `other` - Other
            description (Union[Unset, str]):
    """

    name: str
    device_type: Union["NestedDeviceTypeRequest", None, Unset] = UNSET
    module_type: Union["NestedModuleTypeRequest", None, Unset] = UNSET
    label: Union[Unset, str] = UNSET
    type: Union[Unset, ConsoleServerPortTemplateRequestType] = UNSET
    description: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_device_type_request import NestedDeviceTypeRequest
        from ..models.nested_module_type_request import NestedModuleTypeRequest

        name = self.name

        device_type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.device_type, Unset):
            device_type = UNSET
        elif isinstance(self.device_type, NestedDeviceTypeRequest):
            device_type = self.device_type.to_dict()
        else:
            device_type = self.device_type

        module_type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.module_type, Unset):
            module_type = UNSET
        elif isinstance(self.module_type, NestedModuleTypeRequest):
            module_type = self.module_type.to_dict()
        else:
            module_type = self.module_type

        label = self.label

        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
            }
        )
        if device_type is not UNSET:
            field_dict["device_type"] = device_type
        if module_type is not UNSET:
            field_dict["module_type"] = module_type
        if label is not UNSET:
            field_dict["label"] = label
        if type is not UNSET:
            field_dict["type"] = type
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_device_type_request import NestedDeviceTypeRequest
        from ..models.nested_module_type_request import NestedModuleTypeRequest

        d = src_dict.copy()
        name = d.pop("name")

        def _parse_device_type(
            data: object,
        ) -> Union["NestedDeviceTypeRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                device_type_type_1 = NestedDeviceTypeRequest.from_dict(data)

                return device_type_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedDeviceTypeRequest", None, Unset], data)

        device_type = _parse_device_type(d.pop("device_type", UNSET))

        def _parse_module_type(
            data: object,
        ) -> Union["NestedModuleTypeRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                module_type_type_1 = NestedModuleTypeRequest.from_dict(data)

                return module_type_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedModuleTypeRequest", None, Unset], data)

        module_type = _parse_module_type(d.pop("module_type", UNSET))

        label = d.pop("label", UNSET)

        _type = d.pop("type", UNSET)
        type: Union[Unset, ConsoleServerPortTemplateRequestType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = ConsoleServerPortTemplateRequestType(_type)

        description = d.pop("description", UNSET)

        console_server_port_template_request = cls(
            name=name,
            device_type=device_type,
            module_type=module_type,
            label=label,
            type=type,
            description=description,
        )

        console_server_port_template_request.additional_properties = d
        return console_server_port_template_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
