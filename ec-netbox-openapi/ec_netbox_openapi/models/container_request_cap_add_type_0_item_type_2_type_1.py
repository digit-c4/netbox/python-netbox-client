from enum import Enum


class ContainerRequestCapAddType0ItemType2Type1(str, Enum):
    NET_ADMIN = "NET_ADMIN"
    VALUE_1 = ""

    def __str__(self) -> str:
        return str(self.value)
