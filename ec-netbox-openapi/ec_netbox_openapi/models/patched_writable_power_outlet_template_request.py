from typing import Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_power_outlet_template_request_feed_leg import (
    PatchedWritablePowerOutletTemplateRequestFeedLeg,
)
from ..models.patched_writable_power_outlet_template_request_type import (
    PatchedWritablePowerOutletTemplateRequestType,
)
from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedWritablePowerOutletTemplateRequest")


@_attrs_define
class PatchedWritablePowerOutletTemplateRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            device_type (Union[None, Unset, int]):
            module_type (Union[None, Unset, int]):
            name (Union[Unset, str]): {module} is accepted as a substitution for the module bay position when attached to a
                module type.
            label (Union[Unset, str]): Physical label
            type (Union[Unset, PatchedWritablePowerOutletTemplateRequestType]): * `iec-60320-c5` - C5
                * `iec-60320-c7` - C7
                * `iec-60320-c13` - C13
                * `iec-60320-c15` - C15
                * `iec-60320-c19` - C19
                * `iec-60320-c21` - C21
                * `iec-60309-p-n-e-4h` - P+N+E 4H
                * `iec-60309-p-n-e-6h` - P+N+E 6H
                * `iec-60309-p-n-e-9h` - P+N+E 9H
                * `iec-60309-2p-e-4h` - 2P+E 4H
                * `iec-60309-2p-e-6h` - 2P+E 6H
                * `iec-60309-2p-e-9h` - 2P+E 9H
                * `iec-60309-3p-e-4h` - 3P+E 4H
                * `iec-60309-3p-e-6h` - 3P+E 6H
                * `iec-60309-3p-e-9h` - 3P+E 9H
                * `iec-60309-3p-n-e-4h` - 3P+N+E 4H
                * `iec-60309-3p-n-e-6h` - 3P+N+E 6H
                * `iec-60309-3p-n-e-9h` - 3P+N+E 9H
                * `iec-60906-1` - IEC 60906-1
                * `nbr-14136-10a` - 2P+T 10A (NBR 14136)
                * `nbr-14136-20a` - 2P+T 20A (NBR 14136)
                * `nema-1-15r` - NEMA 1-15R
                * `nema-5-15r` - NEMA 5-15R
                * `nema-5-20r` - NEMA 5-20R
                * `nema-5-30r` - NEMA 5-30R
                * `nema-5-50r` - NEMA 5-50R
                * `nema-6-15r` - NEMA 6-15R
                * `nema-6-20r` - NEMA 6-20R
                * `nema-6-30r` - NEMA 6-30R
                * `nema-6-50r` - NEMA 6-50R
                * `nema-10-30r` - NEMA 10-30R
                * `nema-10-50r` - NEMA 10-50R
                * `nema-14-20r` - NEMA 14-20R
                * `nema-14-30r` - NEMA 14-30R
                * `nema-14-50r` - NEMA 14-50R
                * `nema-14-60r` - NEMA 14-60R
                * `nema-15-15r` - NEMA 15-15R
                * `nema-15-20r` - NEMA 15-20R
                * `nema-15-30r` - NEMA 15-30R
                * `nema-15-50r` - NEMA 15-50R
                * `nema-15-60r` - NEMA 15-60R
                * `nema-l1-15r` - NEMA L1-15R
                * `nema-l5-15r` - NEMA L5-15R
                * `nema-l5-20r` - NEMA L5-20R
                * `nema-l5-30r` - NEMA L5-30R
                * `nema-l5-50r` - NEMA L5-50R
                * `nema-l6-15r` - NEMA L6-15R
                * `nema-l6-20r` - NEMA L6-20R
                * `nema-l6-30r` - NEMA L6-30R
                * `nema-l6-50r` - NEMA L6-50R
                * `nema-l10-30r` - NEMA L10-30R
                * `nema-l14-20r` - NEMA L14-20R
                * `nema-l14-30r` - NEMA L14-30R
                * `nema-l14-50r` - NEMA L14-50R
                * `nema-l14-60r` - NEMA L14-60R
                * `nema-l15-20r` - NEMA L15-20R
                * `nema-l15-30r` - NEMA L15-30R
                * `nema-l15-50r` - NEMA L15-50R
                * `nema-l15-60r` - NEMA L15-60R
                * `nema-l21-20r` - NEMA L21-20R
                * `nema-l21-30r` - NEMA L21-30R
                * `nema-l22-30r` - NEMA L22-30R
                * `CS6360C` - CS6360C
                * `CS6364C` - CS6364C
                * `CS8164C` - CS8164C
                * `CS8264C` - CS8264C
                * `CS8364C` - CS8364C
                * `CS8464C` - CS8464C
                * `ita-e` - ITA Type E (CEE 7/5)
                * `ita-f` - ITA Type F (CEE 7/3)
                * `ita-g` - ITA Type G (BS 1363)
                * `ita-h` - ITA Type H
                * `ita-i` - ITA Type I
                * `ita-j` - ITA Type J
                * `ita-k` - ITA Type K
                * `ita-l` - ITA Type L (CEI 23-50)
                * `ita-m` - ITA Type M (BS 546)
                * `ita-n` - ITA Type N
                * `ita-o` - ITA Type O
                * `ita-multistandard` - ITA Multistandard
                * `usb-a` - USB Type A
                * `usb-micro-b` - USB Micro B
                * `usb-c` - USB Type C
                * `dc-terminal` - DC Terminal
                * `hdot-cx` - HDOT Cx
                * `saf-d-grid` - Saf-D-Grid
                * `neutrik-powercon-20a` - Neutrik powerCON (20A)
                * `neutrik-powercon-32a` - Neutrik powerCON (32A)
                * `neutrik-powercon-true1` - Neutrik powerCON TRUE1
                * `neutrik-powercon-true1-top` - Neutrik powerCON TRUE1 TOP
                * `ubiquiti-smartpower` - Ubiquiti SmartPower
                * `hardwired` - Hardwired
                * `other` - Other
            power_port (Union[None, Unset, int]):
            feed_leg (Union[Unset, PatchedWritablePowerOutletTemplateRequestFeedLeg]): Phase (for three-phase feeds)

                * `A` - A
                * `B` - B
                * `C` - C
            description (Union[Unset, str]):
    """

    device_type: Union[None, Unset, int] = UNSET
    module_type: Union[None, Unset, int] = UNSET
    name: Union[Unset, str] = UNSET
    label: Union[Unset, str] = UNSET
    type: Union[Unset, PatchedWritablePowerOutletTemplateRequestType] = UNSET
    power_port: Union[None, Unset, int] = UNSET
    feed_leg: Union[Unset, PatchedWritablePowerOutletTemplateRequestFeedLeg] = UNSET
    description: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        device_type: Union[None, Unset, int]
        if isinstance(self.device_type, Unset):
            device_type = UNSET
        else:
            device_type = self.device_type

        module_type: Union[None, Unset, int]
        if isinstance(self.module_type, Unset):
            module_type = UNSET
        else:
            module_type = self.module_type

        name = self.name

        label = self.label

        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        power_port: Union[None, Unset, int]
        if isinstance(self.power_port, Unset):
            power_port = UNSET
        else:
            power_port = self.power_port

        feed_leg: Union[Unset, str] = UNSET
        if not isinstance(self.feed_leg, Unset):
            feed_leg = self.feed_leg.value

        description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if device_type is not UNSET:
            field_dict["device_type"] = device_type
        if module_type is not UNSET:
            field_dict["module_type"] = module_type
        if name is not UNSET:
            field_dict["name"] = name
        if label is not UNSET:
            field_dict["label"] = label
        if type is not UNSET:
            field_dict["type"] = type
        if power_port is not UNSET:
            field_dict["power_port"] = power_port
        if feed_leg is not UNSET:
            field_dict["feed_leg"] = feed_leg
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        device_type: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.device_type, Unset):
            device_type = UNSET
        elif isinstance(self.device_type, int):
            device_type = (None, str(self.device_type).encode(), "text/plain")
        else:
            device_type = (None, str(self.device_type).encode(), "text/plain")

        module_type: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.module_type, Unset):
            module_type = UNSET
        elif isinstance(self.module_type, int):
            module_type = (None, str(self.module_type).encode(), "text/plain")
        else:
            module_type = (None, str(self.module_type).encode(), "text/plain")

        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        label = (
            self.label
            if isinstance(self.label, Unset)
            else (None, str(self.label).encode(), "text/plain")
        )

        type: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.type, Unset):
            type = (None, str(self.type.value).encode(), "text/plain")

        power_port: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.power_port, Unset):
            power_port = UNSET
        elif isinstance(self.power_port, int):
            power_port = (None, str(self.power_port).encode(), "text/plain")
        else:
            power_port = (None, str(self.power_port).encode(), "text/plain")

        feed_leg: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.feed_leg, Unset):
            feed_leg = (None, str(self.feed_leg.value).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if device_type is not UNSET:
            field_dict["device_type"] = device_type
        if module_type is not UNSET:
            field_dict["module_type"] = module_type
        if name is not UNSET:
            field_dict["name"] = name
        if label is not UNSET:
            field_dict["label"] = label
        if type is not UNSET:
            field_dict["type"] = type
        if power_port is not UNSET:
            field_dict["power_port"] = power_port
        if feed_leg is not UNSET:
            field_dict["feed_leg"] = feed_leg
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()

        def _parse_device_type(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        device_type = _parse_device_type(d.pop("device_type", UNSET))

        def _parse_module_type(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        module_type = _parse_module_type(d.pop("module_type", UNSET))

        name = d.pop("name", UNSET)

        label = d.pop("label", UNSET)

        _type = d.pop("type", UNSET)
        type: Union[Unset, PatchedWritablePowerOutletTemplateRequestType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = PatchedWritablePowerOutletTemplateRequestType(_type)

        def _parse_power_port(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        power_port = _parse_power_port(d.pop("power_port", UNSET))

        _feed_leg = d.pop("feed_leg", UNSET)
        feed_leg: Union[Unset, PatchedWritablePowerOutletTemplateRequestFeedLeg]
        if isinstance(_feed_leg, Unset):
            feed_leg = UNSET
        else:
            feed_leg = PatchedWritablePowerOutletTemplateRequestFeedLeg(_feed_leg)

        description = d.pop("description", UNSET)

        patched_writable_power_outlet_template_request = cls(
            device_type=device_type,
            module_type=module_type,
            name=name,
            label=label,
            type=type,
            power_port=power_port,
            feed_leg=feed_leg,
            description=description,
        )

        patched_writable_power_outlet_template_request.additional_properties = d
        return patched_writable_power_outlet_template_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
