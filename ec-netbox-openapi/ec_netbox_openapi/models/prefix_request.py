from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.prefix_request_status import PrefixRequestStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_role_request import NestedRoleRequest
    from ..models.nested_site_request import NestedSiteRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_tenant_request import NestedTenantRequest
    from ..models.nested_vlan_request import NestedVLANRequest
    from ..models.nested_vrf_request import NestedVRFRequest
    from ..models.prefix_request_custom_fields import PrefixRequestCustomFields


T = TypeVar("T", bound="PrefixRequest")


@_attrs_define
class PrefixRequest:
    """Adds support for custom fields and tags.

    Attributes:
        prefix (str):
        site (Union['NestedSiteRequest', None, Unset]):
        vrf (Union['NestedVRFRequest', None, Unset]):
        tenant (Union['NestedTenantRequest', None, Unset]):
        vlan (Union['NestedVLANRequest', None, Unset]):
        status (Union[Unset, PrefixRequestStatus]): * `container` - Container
            * `active` - Active
            * `reserved` - Reserved
            * `deprecated` - Deprecated
        role (Union['NestedRoleRequest', None, Unset]):
        is_pool (Union[Unset, bool]): All IP addresses within this prefix are considered usable
        mark_utilized (Union[Unset, bool]): Treat as 100% utilized
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, PrefixRequestCustomFields]):
    """

    prefix: str
    site: Union["NestedSiteRequest", None, Unset] = UNSET
    vrf: Union["NestedVRFRequest", None, Unset] = UNSET
    tenant: Union["NestedTenantRequest", None, Unset] = UNSET
    vlan: Union["NestedVLANRequest", None, Unset] = UNSET
    status: Union[Unset, PrefixRequestStatus] = UNSET
    role: Union["NestedRoleRequest", None, Unset] = UNSET
    is_pool: Union[Unset, bool] = UNSET
    mark_utilized: Union[Unset, bool] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "PrefixRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_role_request import NestedRoleRequest
        from ..models.nested_site_request import NestedSiteRequest
        from ..models.nested_tenant_request import NestedTenantRequest
        from ..models.nested_vlan_request import NestedVLANRequest
        from ..models.nested_vrf_request import NestedVRFRequest

        prefix = self.prefix

        site: Union[Dict[str, Any], None, Unset]
        if isinstance(self.site, Unset):
            site = UNSET
        elif isinstance(self.site, NestedSiteRequest):
            site = self.site.to_dict()
        else:
            site = self.site

        vrf: Union[Dict[str, Any], None, Unset]
        if isinstance(self.vrf, Unset):
            vrf = UNSET
        elif isinstance(self.vrf, NestedVRFRequest):
            vrf = self.vrf.to_dict()
        else:
            vrf = self.vrf

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenantRequest):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        vlan: Union[Dict[str, Any], None, Unset]
        if isinstance(self.vlan, Unset):
            vlan = UNSET
        elif isinstance(self.vlan, NestedVLANRequest):
            vlan = self.vlan.to_dict()
        else:
            vlan = self.vlan

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        role: Union[Dict[str, Any], None, Unset]
        if isinstance(self.role, Unset):
            role = UNSET
        elif isinstance(self.role, NestedRoleRequest):
            role = self.role.to_dict()
        else:
            role = self.role

        is_pool = self.is_pool

        mark_utilized = self.mark_utilized

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "prefix": prefix,
            }
        )
        if site is not UNSET:
            field_dict["site"] = site
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if vlan is not UNSET:
            field_dict["vlan"] = vlan
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if is_pool is not UNSET:
            field_dict["is_pool"] = is_pool
        if mark_utilized is not UNSET:
            field_dict["mark_utilized"] = mark_utilized
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_role_request import NestedRoleRequest
        from ..models.nested_site_request import NestedSiteRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_tenant_request import NestedTenantRequest
        from ..models.nested_vlan_request import NestedVLANRequest
        from ..models.nested_vrf_request import NestedVRFRequest
        from ..models.prefix_request_custom_fields import PrefixRequestCustomFields

        d = src_dict.copy()
        prefix = d.pop("prefix")

        def _parse_site(data: object) -> Union["NestedSiteRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                site_type_1 = NestedSiteRequest.from_dict(data)

                return site_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedSiteRequest", None, Unset], data)

        site = _parse_site(d.pop("site", UNSET))

        def _parse_vrf(data: object) -> Union["NestedVRFRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                vrf_type_1 = NestedVRFRequest.from_dict(data)

                return vrf_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVRFRequest", None, Unset], data)

        vrf = _parse_vrf(d.pop("vrf", UNSET))

        def _parse_tenant(data: object) -> Union["NestedTenantRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenantRequest.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenantRequest", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        def _parse_vlan(data: object) -> Union["NestedVLANRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                vlan_type_1 = NestedVLANRequest.from_dict(data)

                return vlan_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVLANRequest", None, Unset], data)

        vlan = _parse_vlan(d.pop("vlan", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, PrefixRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = PrefixRequestStatus(_status)

        def _parse_role(data: object) -> Union["NestedRoleRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                role_type_1 = NestedRoleRequest.from_dict(data)

                return role_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedRoleRequest", None, Unset], data)

        role = _parse_role(d.pop("role", UNSET))

        is_pool = d.pop("is_pool", UNSET)

        mark_utilized = d.pop("mark_utilized", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PrefixRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PrefixRequestCustomFields.from_dict(_custom_fields)

        prefix_request = cls(
            prefix=prefix,
            site=site,
            vrf=vrf,
            tenant=tenant,
            vlan=vlan,
            status=status,
            role=role,
            is_pool=is_pool,
            mark_utilized=mark_utilized,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        prefix_request.additional_properties = d
        return prefix_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
