from enum import Enum


class L2VPNRequestType(str, Enum):
    EPL = "epl"
    EP_LAN = "ep-lan"
    EP_TREE = "ep-tree"
    EVPL = "evpl"
    EVP_LAN = "evp-lan"
    EVP_TREE = "evp-tree"
    MPLS_EVPN = "mpls-evpn"
    PBB_EVPN = "pbb-evpn"
    VPLS = "vpls"
    VPWS = "vpws"
    VXLAN = "vxlan"
    VXLAN_EVPN = "vxlan-evpn"

    def __str__(self) -> str:
        return str(self.value)
