import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_data_file import NestedDataFile
    from ..models.nested_data_source import NestedDataSource


T = TypeVar("T", bound="ConfigContext")


@_attrs_define
class ConfigContext:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            id (int):
            url (str):
            display (str):
            name (str):
            data_path (str): Path to remote file (relative to data source root)
            data_file (NestedDataFile): Represents an object related through a ForeignKey field. On write, it accepts a
                primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            data_synced (Union[None, datetime.datetime]):
            data (Any):
            created (Union[None, datetime.datetime]):
            last_updated (Union[None, datetime.datetime]):
            weight (Union[Unset, int]):
            description (Union[Unset, str]):
            is_active (Union[Unset, bool]):
            regions (Union[Unset, List[int]]):
            site_groups (Union[Unset, List[int]]):
            sites (Union[Unset, List[int]]):
            locations (Union[Unset, List[int]]):
            device_types (Union[Unset, List[int]]):
            roles (Union[Unset, List[int]]):
            platforms (Union[Unset, List[int]]):
            cluster_types (Union[Unset, List[int]]):
            cluster_groups (Union[Unset, List[int]]):
            clusters (Union[Unset, List[int]]):
            tenant_groups (Union[Unset, List[int]]):
            tenants (Union[Unset, List[int]]):
            tags (Union[Unset, List[str]]):
            data_source (Union[Unset, NestedDataSource]): Represents an object related through a ForeignKey field. On write,
                it accepts a primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
    """

    id: int
    url: str
    display: str
    name: str
    data_path: str
    data_file: "NestedDataFile"
    data_synced: Union[None, datetime.datetime]
    data: Any
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    weight: Union[Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    is_active: Union[Unset, bool] = UNSET
    regions: Union[Unset, List[int]] = UNSET
    site_groups: Union[Unset, List[int]] = UNSET
    sites: Union[Unset, List[int]] = UNSET
    locations: Union[Unset, List[int]] = UNSET
    device_types: Union[Unset, List[int]] = UNSET
    roles: Union[Unset, List[int]] = UNSET
    platforms: Union[Unset, List[int]] = UNSET
    cluster_types: Union[Unset, List[int]] = UNSET
    cluster_groups: Union[Unset, List[int]] = UNSET
    clusters: Union[Unset, List[int]] = UNSET
    tenant_groups: Union[Unset, List[int]] = UNSET
    tenants: Union[Unset, List[int]] = UNSET
    tags: Union[Unset, List[str]] = UNSET
    data_source: Union[Unset, "NestedDataSource"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        name = self.name

        data_path = self.data_path

        data_file = self.data_file.to_dict()

        data_synced: Union[None, str]
        if isinstance(self.data_synced, datetime.datetime):
            data_synced = self.data_synced.isoformat()
        else:
            data_synced = self.data_synced

        data = self.data

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        weight = self.weight

        description = self.description

        is_active = self.is_active

        regions: Union[Unset, List[int]] = UNSET
        if not isinstance(self.regions, Unset):
            regions = self.regions

        site_groups: Union[Unset, List[int]] = UNSET
        if not isinstance(self.site_groups, Unset):
            site_groups = self.site_groups

        sites: Union[Unset, List[int]] = UNSET
        if not isinstance(self.sites, Unset):
            sites = self.sites

        locations: Union[Unset, List[int]] = UNSET
        if not isinstance(self.locations, Unset):
            locations = self.locations

        device_types: Union[Unset, List[int]] = UNSET
        if not isinstance(self.device_types, Unset):
            device_types = self.device_types

        roles: Union[Unset, List[int]] = UNSET
        if not isinstance(self.roles, Unset):
            roles = self.roles

        platforms: Union[Unset, List[int]] = UNSET
        if not isinstance(self.platforms, Unset):
            platforms = self.platforms

        cluster_types: Union[Unset, List[int]] = UNSET
        if not isinstance(self.cluster_types, Unset):
            cluster_types = self.cluster_types

        cluster_groups: Union[Unset, List[int]] = UNSET
        if not isinstance(self.cluster_groups, Unset):
            cluster_groups = self.cluster_groups

        clusters: Union[Unset, List[int]] = UNSET
        if not isinstance(self.clusters, Unset):
            clusters = self.clusters

        tenant_groups: Union[Unset, List[int]] = UNSET
        if not isinstance(self.tenant_groups, Unset):
            tenant_groups = self.tenant_groups

        tenants: Union[Unset, List[int]] = UNSET
        if not isinstance(self.tenants, Unset):
            tenants = self.tenants

        tags: Union[Unset, List[str]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = self.tags

        data_source: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.data_source, Unset):
            data_source = self.data_source.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "name": name,
                "data_path": data_path,
                "data_file": data_file,
                "data_synced": data_synced,
                "data": data,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if weight is not UNSET:
            field_dict["weight"] = weight
        if description is not UNSET:
            field_dict["description"] = description
        if is_active is not UNSET:
            field_dict["is_active"] = is_active
        if regions is not UNSET:
            field_dict["regions"] = regions
        if site_groups is not UNSET:
            field_dict["site_groups"] = site_groups
        if sites is not UNSET:
            field_dict["sites"] = sites
        if locations is not UNSET:
            field_dict["locations"] = locations
        if device_types is not UNSET:
            field_dict["device_types"] = device_types
        if roles is not UNSET:
            field_dict["roles"] = roles
        if platforms is not UNSET:
            field_dict["platforms"] = platforms
        if cluster_types is not UNSET:
            field_dict["cluster_types"] = cluster_types
        if cluster_groups is not UNSET:
            field_dict["cluster_groups"] = cluster_groups
        if clusters is not UNSET:
            field_dict["clusters"] = clusters
        if tenant_groups is not UNSET:
            field_dict["tenant_groups"] = tenant_groups
        if tenants is not UNSET:
            field_dict["tenants"] = tenants
        if tags is not UNSET:
            field_dict["tags"] = tags
        if data_source is not UNSET:
            field_dict["data_source"] = data_source

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_data_file import NestedDataFile
        from ..models.nested_data_source import NestedDataSource

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        name = d.pop("name")

        data_path = d.pop("data_path")

        data_file = NestedDataFile.from_dict(d.pop("data_file"))

        def _parse_data_synced(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                data_synced_type_0 = isoparse(data)

                return data_synced_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        data_synced = _parse_data_synced(d.pop("data_synced"))

        data = d.pop("data")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        weight = d.pop("weight", UNSET)

        description = d.pop("description", UNSET)

        is_active = d.pop("is_active", UNSET)

        regions = cast(List[int], d.pop("regions", UNSET))

        site_groups = cast(List[int], d.pop("site_groups", UNSET))

        sites = cast(List[int], d.pop("sites", UNSET))

        locations = cast(List[int], d.pop("locations", UNSET))

        device_types = cast(List[int], d.pop("device_types", UNSET))

        roles = cast(List[int], d.pop("roles", UNSET))

        platforms = cast(List[int], d.pop("platforms", UNSET))

        cluster_types = cast(List[int], d.pop("cluster_types", UNSET))

        cluster_groups = cast(List[int], d.pop("cluster_groups", UNSET))

        clusters = cast(List[int], d.pop("clusters", UNSET))

        tenant_groups = cast(List[int], d.pop("tenant_groups", UNSET))

        tenants = cast(List[int], d.pop("tenants", UNSET))

        tags = cast(List[str], d.pop("tags", UNSET))

        _data_source = d.pop("data_source", UNSET)
        data_source: Union[Unset, NestedDataSource]
        if isinstance(_data_source, Unset):
            data_source = UNSET
        else:
            data_source = NestedDataSource.from_dict(_data_source)

        config_context = cls(
            id=id,
            url=url,
            display=display,
            name=name,
            data_path=data_path,
            data_file=data_file,
            data_synced=data_synced,
            data=data,
            created=created,
            last_updated=last_updated,
            weight=weight,
            description=description,
            is_active=is_active,
            regions=regions,
            site_groups=site_groups,
            sites=sites,
            locations=locations,
            device_types=device_types,
            roles=roles,
            platforms=platforms,
            cluster_types=cluster_types,
            cluster_groups=cluster_groups,
            clusters=clusters,
            tenant_groups=tenant_groups,
            tenants=tenants,
            tags=tags,
            data_source=data_source,
        )

        config_context.additional_properties = d
        return config_context

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
