from enum import Enum


class PatchedCertificateRequestCertificateAuthority(str, Enum):
    COMMISSIGN = "commissign"
    GLOBALSIGN = "globalsign"
    LETSENCRYPT = "letsencrypt"
    OTHER = "other"

    def __str__(self) -> str:
        return str(self.value)
