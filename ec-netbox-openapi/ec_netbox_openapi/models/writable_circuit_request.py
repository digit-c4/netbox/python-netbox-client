import datetime
import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.writable_circuit_request_status import WritableCircuitRequestStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_circuit_request_custom_fields import (
        WritableCircuitRequestCustomFields,
    )


T = TypeVar("T", bound="WritableCircuitRequest")


@_attrs_define
class WritableCircuitRequest:
    """Adds support for custom fields and tags.

    Attributes:
        cid (str): Unique circuit ID
        provider (int):
        type (int):
        provider_account (Union[None, Unset, int]):
        status (Union[Unset, WritableCircuitRequestStatus]): * `planned` - Planned
            * `provisioning` - Provisioning
            * `active` - Active
            * `offline` - Offline
            * `deprovisioning` - Deprovisioning
            * `decommissioned` - Decommissioned
        tenant (Union[None, Unset, int]):
        install_date (Union[None, Unset, datetime.date]):
        termination_date (Union[None, Unset, datetime.date]):
        commit_rate (Union[None, Unset, int]): Committed rate
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritableCircuitRequestCustomFields]):
    """

    cid: str
    provider: int
    type: int
    provider_account: Union[None, Unset, int] = UNSET
    status: Union[Unset, WritableCircuitRequestStatus] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    install_date: Union[None, Unset, datetime.date] = UNSET
    termination_date: Union[None, Unset, datetime.date] = UNSET
    commit_rate: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WritableCircuitRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        cid = self.cid

        provider = self.provider

        type = self.type

        provider_account: Union[None, Unset, int]
        if isinstance(self.provider_account, Unset):
            provider_account = UNSET
        else:
            provider_account = self.provider_account

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        install_date: Union[None, Unset, str]
        if isinstance(self.install_date, Unset):
            install_date = UNSET
        elif isinstance(self.install_date, datetime.date):
            install_date = self.install_date.isoformat()
        else:
            install_date = self.install_date

        termination_date: Union[None, Unset, str]
        if isinstance(self.termination_date, Unset):
            termination_date = UNSET
        elif isinstance(self.termination_date, datetime.date):
            termination_date = self.termination_date.isoformat()
        else:
            termination_date = self.termination_date

        commit_rate: Union[None, Unset, int]
        if isinstance(self.commit_rate, Unset):
            commit_rate = UNSET
        else:
            commit_rate = self.commit_rate

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "cid": cid,
                "provider": provider,
                "type": type,
            }
        )
        if provider_account is not UNSET:
            field_dict["provider_account"] = provider_account
        if status is not UNSET:
            field_dict["status"] = status
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if install_date is not UNSET:
            field_dict["install_date"] = install_date
        if termination_date is not UNSET:
            field_dict["termination_date"] = termination_date
        if commit_rate is not UNSET:
            field_dict["commit_rate"] = commit_rate
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        cid = (None, str(self.cid).encode(), "text/plain")

        provider = (None, str(self.provider).encode(), "text/plain")

        type = (None, str(self.type).encode(), "text/plain")

        provider_account: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.provider_account, Unset):
            provider_account = UNSET
        elif isinstance(self.provider_account, int):
            provider_account = (None, str(self.provider_account).encode(), "text/plain")
        else:
            provider_account = (None, str(self.provider_account).encode(), "text/plain")

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        install_date: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.install_date, Unset):
            install_date = UNSET
        elif isinstance(self.install_date, datetime.date):
            install_date = self.install_date.isoformat().encode()
        else:
            install_date = (None, str(self.install_date).encode(), "text/plain")

        termination_date: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.termination_date, Unset):
            termination_date = UNSET
        elif isinstance(self.termination_date, datetime.date):
            termination_date = self.termination_date.isoformat().encode()
        else:
            termination_date = (None, str(self.termination_date).encode(), "text/plain")

        commit_rate: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.commit_rate, Unset):
            commit_rate = UNSET
        elif isinstance(self.commit_rate, int):
            commit_rate = (None, str(self.commit_rate).encode(), "text/plain")
        else:
            commit_rate = (None, str(self.commit_rate).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "cid": cid,
                "provider": provider,
                "type": type,
            }
        )
        if provider_account is not UNSET:
            field_dict["provider_account"] = provider_account
        if status is not UNSET:
            field_dict["status"] = status
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if install_date is not UNSET:
            field_dict["install_date"] = install_date
        if termination_date is not UNSET:
            field_dict["termination_date"] = termination_date
        if commit_rate is not UNSET:
            field_dict["commit_rate"] = commit_rate
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_circuit_request_custom_fields import (
            WritableCircuitRequestCustomFields,
        )

        d = src_dict.copy()
        cid = d.pop("cid")

        provider = d.pop("provider")

        type = d.pop("type")

        def _parse_provider_account(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        provider_account = _parse_provider_account(d.pop("provider_account", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, WritableCircuitRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = WritableCircuitRequestStatus(_status)

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        def _parse_install_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                install_date_type_0 = isoparse(data).date()

                return install_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        install_date = _parse_install_date(d.pop("install_date", UNSET))

        def _parse_termination_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                termination_date_type_0 = isoparse(data).date()

                return termination_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        termination_date = _parse_termination_date(d.pop("termination_date", UNSET))

        def _parse_commit_rate(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        commit_rate = _parse_commit_rate(d.pop("commit_rate", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableCircuitRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableCircuitRequestCustomFields.from_dict(_custom_fields)

        writable_circuit_request = cls(
            cid=cid,
            provider=provider,
            type=type,
            provider_account=provider_account,
            status=status,
            tenant=tenant,
            install_date=install_date,
            termination_date=termination_date,
            commit_rate=commit_rate,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_circuit_request.additional_properties = d
        return writable_circuit_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
