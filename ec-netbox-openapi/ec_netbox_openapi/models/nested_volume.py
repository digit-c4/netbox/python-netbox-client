from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.nested_volume_driver import NestedVolumeDriver
from ..types import UNSET, Unset

T = TypeVar("T", bound="NestedVolume")


@_attrs_define
class NestedVolume:
    """Nested Volume Serializer class

    Attributes:
        id (int):
        url (str):
        display (str):
        name (str):
        driver (Union[Unset, NestedVolumeDriver]): * `local` - local
    """

    id: int
    url: str
    display: str
    name: str
    driver: Union[Unset, NestedVolumeDriver] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        name = self.name

        driver: Union[Unset, str] = UNSET
        if not isinstance(self.driver, Unset):
            driver = self.driver.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "name": name,
            }
        )
        if driver is not UNSET:
            field_dict["driver"] = driver

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        name = d.pop("name")

        _driver = d.pop("driver", UNSET)
        driver: Union[Unset, NestedVolumeDriver]
        if isinstance(_driver, Unset):
            driver = UNSET
        else:
            driver = NestedVolumeDriver(_driver)

        nested_volume = cls(
            id=id,
            url=url,
            display=display,
            name=name,
            driver=driver,
        )

        nested_volume.additional_properties = d
        return nested_volume

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
