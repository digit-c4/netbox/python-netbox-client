from enum import Enum


class NestedHostOperation(str, Enum):
    NONE = "none"
    REFRESH = "refresh"

    def __str__(self) -> str:
        return str(self.value)
