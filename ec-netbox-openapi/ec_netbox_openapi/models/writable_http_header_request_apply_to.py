from enum import Enum


class WritableHttpHeaderRequestApplyTo(str, Enum):
    REQUEST = "request"
    RESPONSE = "response"

    def __str__(self) -> str:
        return str(self.value)
