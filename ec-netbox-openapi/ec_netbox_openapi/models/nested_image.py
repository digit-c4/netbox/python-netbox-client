from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="NestedImage")


@_attrs_define
class NestedImage:
    """Nested Image Serializer class

    Attributes:
        id (int):
        url (str):
        display (str):
        name (str):
        version (Union[Unset, str]):
        size (Union[Unset, int]):
        image_id (Union[None, Unset, str]):
        digest (Union[None, Unset, str]):
    """

    id: int
    url: str
    display: str
    name: str
    version: Union[Unset, str] = UNSET
    size: Union[Unset, int] = UNSET
    image_id: Union[None, Unset, str] = UNSET
    digest: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        name = self.name

        version = self.version

        size = self.size

        image_id: Union[None, Unset, str]
        if isinstance(self.image_id, Unset):
            image_id = UNSET
        else:
            image_id = self.image_id

        digest: Union[None, Unset, str]
        if isinstance(self.digest, Unset):
            digest = UNSET
        else:
            digest = self.digest

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "name": name,
            }
        )
        if version is not UNSET:
            field_dict["version"] = version
        if size is not UNSET:
            field_dict["size"] = size
        if image_id is not UNSET:
            field_dict["ImageID"] = image_id
        if digest is not UNSET:
            field_dict["Digest"] = digest

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        name = d.pop("name")

        version = d.pop("version", UNSET)

        size = d.pop("size", UNSET)

        def _parse_image_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        image_id = _parse_image_id(d.pop("ImageID", UNSET))

        def _parse_digest(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        digest = _parse_digest(d.pop("Digest", UNSET))

        nested_image = cls(
            id=id,
            url=url,
            display=display,
            name=name,
            version=version,
            size=size,
            image_id=image_id,
            digest=digest,
        )

        nested_image.additional_properties = d
        return nested_image

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
