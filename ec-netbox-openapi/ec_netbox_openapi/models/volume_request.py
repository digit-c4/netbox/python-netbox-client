from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.volume_request_driver import VolumeRequestDriver
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_host_request import NestedHostRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.volume_request_custom_fields import VolumeRequestCustomFields


T = TypeVar("T", bound="VolumeRequest")


@_attrs_define
class VolumeRequest:
    """Volume Serializer class

    Attributes:
        host (NestedHostRequest): Nested Host Serializer class
        name (str):
        driver (Union[Unset, VolumeRequestDriver]): * `local` - local
        custom_fields (Union[Unset, VolumeRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    host: "NestedHostRequest"
    name: str
    driver: Union[Unset, VolumeRequestDriver] = UNSET
    custom_fields: Union[Unset, "VolumeRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        host = self.host.to_dict()

        name = self.name

        driver: Union[Unset, str] = UNSET
        if not isinstance(self.driver, Unset):
            driver = self.driver.value

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "host": host,
                "name": name,
            }
        )
        if driver is not UNSET:
            field_dict["driver"] = driver
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_host_request import NestedHostRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.volume_request_custom_fields import VolumeRequestCustomFields

        d = src_dict.copy()
        host = NestedHostRequest.from_dict(d.pop("host"))

        name = d.pop("name")

        _driver = d.pop("driver", UNSET)
        driver: Union[Unset, VolumeRequestDriver]
        if isinstance(_driver, Unset):
            driver = UNSET
        else:
            driver = VolumeRequestDriver(_driver)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, VolumeRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = VolumeRequestCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        volume_request = cls(
            host=host,
            name=name,
            driver=driver,
            custom_fields=custom_fields,
            tags=tags,
        )

        volume_request.additional_properties = d
        return volume_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
