import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_service_request_protocol import WritableServiceRequestProtocol
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_service_request_custom_fields import (
        WritableServiceRequestCustomFields,
    )


T = TypeVar("T", bound="WritableServiceRequest")


@_attrs_define
class WritableServiceRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        ports (List[int]):
        protocol (WritableServiceRequestProtocol): * `tcp` - TCP
            * `udp` - UDP
            * `sctp` - SCTP
        device (Union[None, Unset, int]):
        virtual_machine (Union[None, Unset, int]):
        ipaddresses (Union[Unset, List[int]]): The specific IP addresses (if any) to which this service is bound
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritableServiceRequestCustomFields]):
    """

    name: str
    ports: List[int]
    protocol: WritableServiceRequestProtocol
    device: Union[None, Unset, int] = UNSET
    virtual_machine: Union[None, Unset, int] = UNSET
    ipaddresses: Union[Unset, List[int]] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WritableServiceRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        ports = self.ports

        protocol = self.protocol.value

        device: Union[None, Unset, int]
        if isinstance(self.device, Unset):
            device = UNSET
        else:
            device = self.device

        virtual_machine: Union[None, Unset, int]
        if isinstance(self.virtual_machine, Unset):
            virtual_machine = UNSET
        else:
            virtual_machine = self.virtual_machine

        ipaddresses: Union[Unset, List[int]] = UNSET
        if not isinstance(self.ipaddresses, Unset):
            ipaddresses = self.ipaddresses

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "ports": ports,
                "protocol": protocol,
            }
        )
        if device is not UNSET:
            field_dict["device"] = device
        if virtual_machine is not UNSET:
            field_dict["virtual_machine"] = virtual_machine
        if ipaddresses is not UNSET:
            field_dict["ipaddresses"] = ipaddresses
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (None, str(self.name).encode(), "text/plain")

        _temp_ports = self.ports
        ports = (None, json.dumps(_temp_ports).encode(), "application/json")

        protocol = (None, str(self.protocol.value).encode(), "text/plain")

        device: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.device, Unset):
            device = UNSET
        elif isinstance(self.device, int):
            device = (None, str(self.device).encode(), "text/plain")
        else:
            device = (None, str(self.device).encode(), "text/plain")

        virtual_machine: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.virtual_machine, Unset):
            virtual_machine = UNSET
        elif isinstance(self.virtual_machine, int):
            virtual_machine = (None, str(self.virtual_machine).encode(), "text/plain")
        else:
            virtual_machine = (None, str(self.virtual_machine).encode(), "text/plain")

        ipaddresses: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.ipaddresses, Unset):
            _temp_ipaddresses = self.ipaddresses
            ipaddresses = (
                None,
                json.dumps(_temp_ipaddresses).encode(),
                "application/json",
            )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "name": name,
                "ports": ports,
                "protocol": protocol,
            }
        )
        if device is not UNSET:
            field_dict["device"] = device
        if virtual_machine is not UNSET:
            field_dict["virtual_machine"] = virtual_machine
        if ipaddresses is not UNSET:
            field_dict["ipaddresses"] = ipaddresses
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_service_request_custom_fields import (
            WritableServiceRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name")

        ports = cast(List[int], d.pop("ports"))

        protocol = WritableServiceRequestProtocol(d.pop("protocol"))

        def _parse_device(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        device = _parse_device(d.pop("device", UNSET))

        def _parse_virtual_machine(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        virtual_machine = _parse_virtual_machine(d.pop("virtual_machine", UNSET))

        ipaddresses = cast(List[int], d.pop("ipaddresses", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableServiceRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableServiceRequestCustomFields.from_dict(_custom_fields)

        writable_service_request = cls(
            name=name,
            ports=ports,
            protocol=protocol,
            device=device,
            virtual_machine=virtual_machine,
            ipaddresses=ipaddresses,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_service_request.additional_properties = d
        return writable_service_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
