import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.mapping_auth import MappingAuth
from ..models.mapping_client_max_body_size import MappingClientMaxBodySize
from ..models.mapping_extra_protocols_item import MappingExtraProtocolsItem
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.mapping_custom_fields import MappingCustomFields
    from ..models.nested_acl_denied_source import NestedAclDeniedSource
    from ..models.nested_cache_config import NestedCacheConfig
    from ..models.nested_hsts_protocol import NestedHstsProtocol
    from ..models.nested_http_header import NestedHttpHeader
    from ..models.nested_saml_config import NestedSamlConfig
    from ..models.nested_tag import NestedTag


T = TypeVar("T", bound="Mapping")


@_attrs_define
class Mapping:
    """Mapping Serializer class

    Attributes:
        id (int):
        url (str):
        source (str):
        target (str):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        acl_denied_source (List['NestedAclDeniedSource']):
        cache_configs (List['NestedCacheConfig']):
        hsts_protocol (Union['NestedHstsProtocol', None]):
        authentication (Union[Unset, MappingAuth]): * `none` - None
            * `ldap` - Ldap
            * `ecas` - Ecas
        testingpage (Union[None, Unset, str]):
        webdav (Union[Unset, bool]):
        comment (Union[Unset, str]):
        gzip_proxied (Union[Unset, bool]):
        keepalive_requests (Union[Unset, int]):
        keepalive_timeout (Union[Unset, int]):
        proxy_cache (Union[Unset, bool]):
        proxy_read_timeout (Union[Unset, int]):
        client_max_body_size (Union[Unset, MappingClientMaxBodySize]): * `1` - 1
            * `128` - 128
            * `512` - 512
            * `1024` - 1024
            * `2048` - 2048
        extra_protocols (Union[Unset, List[MappingExtraProtocolsItem]]):
        sorry_page (Union[Unset, str]):
        custom_fields (Union[Unset, MappingCustomFields]):
        tags (Union[Unset, List['NestedTag']]):
        http_headers (Union[Unset, List['NestedHttpHeader']]):
        saml_config (Union['NestedSamlConfig', None, Unset]):
        proxy_buffer_size (Union[None, Unset, int]):
        proxy_buffer (Union[None, Unset, int]):
        proxy_busy_buffer (Union[None, Unset, int]):
        proxy_buffer_responses (Union[Unset, bool]):
        proxy_buffer_requests (Union[Unset, bool]):
    """

    id: int
    url: str
    source: str
    target: str
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    acl_denied_source: List["NestedAclDeniedSource"]
    cache_configs: List["NestedCacheConfig"]
    hsts_protocol: Union["NestedHstsProtocol", None]
    authentication: Union[Unset, MappingAuth] = UNSET
    testingpage: Union[None, Unset, str] = UNSET
    webdav: Union[Unset, bool] = UNSET
    comment: Union[Unset, str] = UNSET
    gzip_proxied: Union[Unset, bool] = UNSET
    keepalive_requests: Union[Unset, int] = UNSET
    keepalive_timeout: Union[Unset, int] = UNSET
    proxy_cache: Union[Unset, bool] = UNSET
    proxy_read_timeout: Union[Unset, int] = UNSET
    client_max_body_size: Union[Unset, MappingClientMaxBodySize] = UNSET
    extra_protocols: Union[Unset, List[MappingExtraProtocolsItem]] = UNSET
    sorry_page: Union[Unset, str] = UNSET
    custom_fields: Union[Unset, "MappingCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    http_headers: Union[Unset, List["NestedHttpHeader"]] = UNSET
    saml_config: Union["NestedSamlConfig", None, Unset] = UNSET
    proxy_buffer_size: Union[None, Unset, int] = UNSET
    proxy_buffer: Union[None, Unset, int] = UNSET
    proxy_busy_buffer: Union[None, Unset, int] = UNSET
    proxy_buffer_responses: Union[Unset, bool] = UNSET
    proxy_buffer_requests: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_hsts_protocol import NestedHstsProtocol
        from ..models.nested_saml_config import NestedSamlConfig

        id = self.id

        url = self.url

        source = self.source

        target = self.target

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        acl_denied_source = []
        for acl_denied_source_item_data in self.acl_denied_source:
            acl_denied_source_item = acl_denied_source_item_data.to_dict()
            acl_denied_source.append(acl_denied_source_item)

        cache_configs = []
        for cache_configs_item_data in self.cache_configs:
            cache_configs_item = cache_configs_item_data.to_dict()
            cache_configs.append(cache_configs_item)

        hsts_protocol: Union[Dict[str, Any], None]
        if isinstance(self.hsts_protocol, NestedHstsProtocol):
            hsts_protocol = self.hsts_protocol.to_dict()
        else:
            hsts_protocol = self.hsts_protocol

        authentication: Union[Unset, str] = UNSET
        if not isinstance(self.authentication, Unset):
            authentication = self.authentication.value

        testingpage: Union[None, Unset, str]
        if isinstance(self.testingpage, Unset):
            testingpage = UNSET
        else:
            testingpage = self.testingpage

        webdav = self.webdav

        comment = self.comment

        gzip_proxied = self.gzip_proxied

        keepalive_requests = self.keepalive_requests

        keepalive_timeout = self.keepalive_timeout

        proxy_cache = self.proxy_cache

        proxy_read_timeout = self.proxy_read_timeout

        client_max_body_size: Union[Unset, int] = UNSET
        if not isinstance(self.client_max_body_size, Unset):
            client_max_body_size = self.client_max_body_size.value

        extra_protocols: Union[Unset, List[str]] = UNSET
        if not isinstance(self.extra_protocols, Unset):
            extra_protocols = []
            for extra_protocols_item_data in self.extra_protocols:
                extra_protocols_item = extra_protocols_item_data.value
                extra_protocols.append(extra_protocols_item)

        sorry_page = self.sorry_page

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        http_headers: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.http_headers, Unset):
            http_headers = []
            for http_headers_item_data in self.http_headers:
                http_headers_item = http_headers_item_data.to_dict()
                http_headers.append(http_headers_item)

        saml_config: Union[Dict[str, Any], None, Unset]
        if isinstance(self.saml_config, Unset):
            saml_config = UNSET
        elif isinstance(self.saml_config, NestedSamlConfig):
            saml_config = self.saml_config.to_dict()
        else:
            saml_config = self.saml_config

        proxy_buffer_size: Union[None, Unset, int]
        if isinstance(self.proxy_buffer_size, Unset):
            proxy_buffer_size = UNSET
        else:
            proxy_buffer_size = self.proxy_buffer_size

        proxy_buffer: Union[None, Unset, int]
        if isinstance(self.proxy_buffer, Unset):
            proxy_buffer = UNSET
        else:
            proxy_buffer = self.proxy_buffer

        proxy_busy_buffer: Union[None, Unset, int]
        if isinstance(self.proxy_busy_buffer, Unset):
            proxy_busy_buffer = UNSET
        else:
            proxy_busy_buffer = self.proxy_busy_buffer

        proxy_buffer_responses = self.proxy_buffer_responses

        proxy_buffer_requests = self.proxy_buffer_requests

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "source": source,
                "target": target,
                "created": created,
                "last_updated": last_updated,
                "acl_denied_source": acl_denied_source,
                "cache_configs": cache_configs,
                "hsts_protocol": hsts_protocol,
            }
        )
        if authentication is not UNSET:
            field_dict["authentication"] = authentication
        if testingpage is not UNSET:
            field_dict["testingpage"] = testingpage
        if webdav is not UNSET:
            field_dict["webdav"] = webdav
        if comment is not UNSET:
            field_dict["Comment"] = comment
        if gzip_proxied is not UNSET:
            field_dict["gzip_proxied"] = gzip_proxied
        if keepalive_requests is not UNSET:
            field_dict["keepalive_requests"] = keepalive_requests
        if keepalive_timeout is not UNSET:
            field_dict["keepalive_timeout"] = keepalive_timeout
        if proxy_cache is not UNSET:
            field_dict["proxy_cache"] = proxy_cache
        if proxy_read_timeout is not UNSET:
            field_dict["proxy_read_timeout"] = proxy_read_timeout
        if client_max_body_size is not UNSET:
            field_dict["client_max_body_size"] = client_max_body_size
        if extra_protocols is not UNSET:
            field_dict["extra_protocols"] = extra_protocols
        if sorry_page is not UNSET:
            field_dict["sorry_page"] = sorry_page
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags
        if http_headers is not UNSET:
            field_dict["http_headers"] = http_headers
        if saml_config is not UNSET:
            field_dict["saml_config"] = saml_config
        if proxy_buffer_size is not UNSET:
            field_dict["proxy_buffer_size"] = proxy_buffer_size
        if proxy_buffer is not UNSET:
            field_dict["proxy_buffer"] = proxy_buffer
        if proxy_busy_buffer is not UNSET:
            field_dict["proxy_busy_buffer"] = proxy_busy_buffer
        if proxy_buffer_responses is not UNSET:
            field_dict["proxy_buffer_responses"] = proxy_buffer_responses
        if proxy_buffer_requests is not UNSET:
            field_dict["proxy_buffer_requests"] = proxy_buffer_requests

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.mapping_custom_fields import MappingCustomFields
        from ..models.nested_acl_denied_source import NestedAclDeniedSource
        from ..models.nested_cache_config import NestedCacheConfig
        from ..models.nested_hsts_protocol import NestedHstsProtocol
        from ..models.nested_http_header import NestedHttpHeader
        from ..models.nested_saml_config import NestedSamlConfig
        from ..models.nested_tag import NestedTag

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        source = d.pop("source")

        target = d.pop("target")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        acl_denied_source = []
        _acl_denied_source = d.pop("acl_denied_source")
        for acl_denied_source_item_data in _acl_denied_source:
            acl_denied_source_item = NestedAclDeniedSource.from_dict(
                acl_denied_source_item_data
            )

            acl_denied_source.append(acl_denied_source_item)

        cache_configs = []
        _cache_configs = d.pop("cache_configs")
        for cache_configs_item_data in _cache_configs:
            cache_configs_item = NestedCacheConfig.from_dict(cache_configs_item_data)

            cache_configs.append(cache_configs_item)

        def _parse_hsts_protocol(data: object) -> Union["NestedHstsProtocol", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                hsts_protocol_type_1 = NestedHstsProtocol.from_dict(data)

                return hsts_protocol_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedHstsProtocol", None], data)

        hsts_protocol = _parse_hsts_protocol(d.pop("hsts_protocol"))

        _authentication = d.pop("authentication", UNSET)
        authentication: Union[Unset, MappingAuth]
        if isinstance(_authentication, Unset):
            authentication = UNSET
        else:
            authentication = MappingAuth(_authentication)

        def _parse_testingpage(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        testingpage = _parse_testingpage(d.pop("testingpage", UNSET))

        webdav = d.pop("webdav", UNSET)

        comment = d.pop("Comment", UNSET)

        gzip_proxied = d.pop("gzip_proxied", UNSET)

        keepalive_requests = d.pop("keepalive_requests", UNSET)

        keepalive_timeout = d.pop("keepalive_timeout", UNSET)

        proxy_cache = d.pop("proxy_cache", UNSET)

        proxy_read_timeout = d.pop("proxy_read_timeout", UNSET)

        _client_max_body_size = d.pop("client_max_body_size", UNSET)
        client_max_body_size: Union[Unset, MappingClientMaxBodySize]
        if isinstance(_client_max_body_size, Unset):
            client_max_body_size = UNSET
        else:
            client_max_body_size = MappingClientMaxBodySize(_client_max_body_size)

        extra_protocols = []
        _extra_protocols = d.pop("extra_protocols", UNSET)
        for extra_protocols_item_data in _extra_protocols or []:
            extra_protocols_item = MappingExtraProtocolsItem(extra_protocols_item_data)

            extra_protocols.append(extra_protocols_item)

        sorry_page = d.pop("sorry_page", UNSET)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, MappingCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = MappingCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        http_headers = []
        _http_headers = d.pop("http_headers", UNSET)
        for http_headers_item_data in _http_headers or []:
            http_headers_item = NestedHttpHeader.from_dict(http_headers_item_data)

            http_headers.append(http_headers_item)

        def _parse_saml_config(data: object) -> Union["NestedSamlConfig", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                saml_config_type_1 = NestedSamlConfig.from_dict(data)

                return saml_config_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedSamlConfig", None, Unset], data)

        saml_config = _parse_saml_config(d.pop("saml_config", UNSET))

        def _parse_proxy_buffer_size(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        proxy_buffer_size = _parse_proxy_buffer_size(d.pop("proxy_buffer_size", UNSET))

        def _parse_proxy_buffer(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        proxy_buffer = _parse_proxy_buffer(d.pop("proxy_buffer", UNSET))

        def _parse_proxy_busy_buffer(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        proxy_busy_buffer = _parse_proxy_busy_buffer(d.pop("proxy_busy_buffer", UNSET))

        proxy_buffer_responses = d.pop("proxy_buffer_responses", UNSET)

        proxy_buffer_requests = d.pop("proxy_buffer_requests", UNSET)

        mapping = cls(
            id=id,
            url=url,
            source=source,
            target=target,
            created=created,
            last_updated=last_updated,
            acl_denied_source=acl_denied_source,
            cache_configs=cache_configs,
            hsts_protocol=hsts_protocol,
            authentication=authentication,
            testingpage=testingpage,
            webdav=webdav,
            comment=comment,
            gzip_proxied=gzip_proxied,
            keepalive_requests=keepalive_requests,
            keepalive_timeout=keepalive_timeout,
            proxy_cache=proxy_cache,
            proxy_read_timeout=proxy_read_timeout,
            client_max_body_size=client_max_body_size,
            extra_protocols=extra_protocols,
            sorry_page=sorry_page,
            custom_fields=custom_fields,
            tags=tags,
            http_headers=http_headers,
            saml_config=saml_config,
            proxy_buffer_size=proxy_buffer_size,
            proxy_buffer=proxy_buffer,
            proxy_busy_buffer=proxy_busy_buffer,
            proxy_buffer_responses=proxy_buffer_responses,
            proxy_buffer_requests=proxy_buffer_requests,
        )

        mapping.additional_properties = d
        return mapping

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
