import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_module_type_request_weight_unit import (
    WritableModuleTypeRequestWeightUnit,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_module_type_request_custom_fields import (
        WritableModuleTypeRequestCustomFields,
    )


T = TypeVar("T", bound="WritableModuleTypeRequest")


@_attrs_define
class WritableModuleTypeRequest:
    """Adds support for custom fields and tags.

    Attributes:
        manufacturer (int):
        model (str):
        part_number (Union[Unset, str]): Discrete part number (optional)
        weight (Union[None, Unset, float]):
        weight_unit (Union[Unset, WritableModuleTypeRequestWeightUnit]): * `kg` - Kilograms
            * `g` - Grams
            * `lb` - Pounds
            * `oz` - Ounces
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritableModuleTypeRequestCustomFields]):
    """

    manufacturer: int
    model: str
    part_number: Union[Unset, str] = UNSET
    weight: Union[None, Unset, float] = UNSET
    weight_unit: Union[Unset, WritableModuleTypeRequestWeightUnit] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WritableModuleTypeRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        manufacturer = self.manufacturer

        model = self.model

        part_number = self.part_number

        weight: Union[None, Unset, float]
        if isinstance(self.weight, Unset):
            weight = UNSET
        else:
            weight = self.weight

        weight_unit: Union[Unset, str] = UNSET
        if not isinstance(self.weight_unit, Unset):
            weight_unit = self.weight_unit.value

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "manufacturer": manufacturer,
                "model": model,
            }
        )
        if part_number is not UNSET:
            field_dict["part_number"] = part_number
        if weight is not UNSET:
            field_dict["weight"] = weight
        if weight_unit is not UNSET:
            field_dict["weight_unit"] = weight_unit
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        manufacturer = (None, str(self.manufacturer).encode(), "text/plain")

        model = (None, str(self.model).encode(), "text/plain")

        part_number = (
            self.part_number
            if isinstance(self.part_number, Unset)
            else (None, str(self.part_number).encode(), "text/plain")
        )

        weight: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.weight, Unset):
            weight = UNSET
        elif isinstance(self.weight, float):
            weight = (None, str(self.weight).encode(), "text/plain")
        else:
            weight = (None, str(self.weight).encode(), "text/plain")

        weight_unit: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.weight_unit, Unset):
            weight_unit = (None, str(self.weight_unit.value).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "manufacturer": manufacturer,
                "model": model,
            }
        )
        if part_number is not UNSET:
            field_dict["part_number"] = part_number
        if weight is not UNSET:
            field_dict["weight"] = weight
        if weight_unit is not UNSET:
            field_dict["weight_unit"] = weight_unit
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_module_type_request_custom_fields import (
            WritableModuleTypeRequestCustomFields,
        )

        d = src_dict.copy()
        manufacturer = d.pop("manufacturer")

        model = d.pop("model")

        part_number = d.pop("part_number", UNSET)

        def _parse_weight(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        weight = _parse_weight(d.pop("weight", UNSET))

        _weight_unit = d.pop("weight_unit", UNSET)
        weight_unit: Union[Unset, WritableModuleTypeRequestWeightUnit]
        if isinstance(_weight_unit, Unset):
            weight_unit = UNSET
        else:
            weight_unit = WritableModuleTypeRequestWeightUnit(_weight_unit)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableModuleTypeRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableModuleTypeRequestCustomFields.from_dict(
                _custom_fields
            )

        writable_module_type_request = cls(
            manufacturer=manufacturer,
            model=model,
            part_number=part_number,
            weight=weight,
            weight_unit=weight_unit,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_module_type_request.additional_properties = d
        return writable_module_type_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
