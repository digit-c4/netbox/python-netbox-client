import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_record_request_status import WritableRecordRequestStatus
from ..models.writable_record_request_type import WritableRecordRequestType
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_record_request_custom_fields import (
        WritableRecordRequestCustomFields,
    )


T = TypeVar("T", bound="WritableRecordRequest")


@_attrs_define
class WritableRecordRequest:
    """Adds support for custom fields and tags.

    Attributes:
        zone (int):
        type (WritableRecordRequestType): * `A` - A
            * `A6` - A6
            * `AAAA` - AAAA
            * `AFSDB` - AFSDB
            * `AMTRELAY` - AMTRELAY
            * `APL` - APL
            * `AVC` - AVC
            * `CAA` - CAA
            * `CDNSKEY` - CDNSKEY
            * `CDS` - CDS
            * `CERT` - CERT
            * `CNAME` - CNAME
            * `CSYNC` - CSYNC
            * `DHCID` - DHCID
            * `DLV` - DLV
            * `DNAME` - DNAME
            * `DNSKEY` - DNSKEY
            * `DS` - DS
            * `EUI48` - EUI48
            * `EUI64` - EUI64
            * `GPOS` - GPOS
            * `HINFO` - HINFO
            * `HIP` - HIP
            * `HTTPS` - HTTPS
            * `IPSECKEY` - IPSECKEY
            * `ISDN` - ISDN
            * `KEY` - KEY
            * `KX` - KX
            * `L32` - L32
            * `L64` - L64
            * `LOC` - LOC
            * `LP` - LP
            * `MB` - MB
            * `MD` - MD
            * `MF` - MF
            * `MG` - MG
            * `MINFO` - MINFO
            * `MR` - MR
            * `MX` - MX
            * `NAPTR` - NAPTR
            * `NID` - NID
            * `NINFO` - NINFO
            * `NS` - NS
            * `NSAP` - NSAP
            * `NSAP_PTR` - NSAP_PTR
            * `NSEC` - NSEC
            * `NSEC3` - NSEC3
            * `NSEC3PARAM` - NSEC3PARAM
            * `NULL` - NULL
            * `NXT` - NXT
            * `OPENPGPKEY` - OPENPGPKEY
            * `PTR` - PTR
            * `PX` - PX
            * `RESINFO` - RESINFO
            * `RP` - RP
            * `RRSIG` - RRSIG
            * `RT` - RT
            * `SIG` - SIG
            * `SMIMEA` - SMIMEA
            * `SOA` - SOA
            * `SPF` - SPF
            * `SRV` - SRV
            * `SSHFP` - SSHFP
            * `SVCB` - SVCB
            * `TA` - TA
            * `TLSA` - TLSA
            * `TXT` - TXT
            * `TYPE0` - TYPE0
            * `UNSPEC` - UNSPEC
            * `URI` - URI
            * `WALLET` - WALLET
            * `WKS` - WKS
            * `X25` - X25
            * `ZONEMD` - ZONEMD
        name (str):
        value (str):
        status (Union[Unset, WritableRecordRequestStatus]): * `active` - Active
            * `inactive` - Inactive
        ttl (Union[None, Unset, int]):
        description (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        managed (Union[Unset, bool]):
        disable_ptr (Union[Unset, bool]): Disable PTR record creation
        custom_fields (Union[Unset, WritableRecordRequestCustomFields]):
        tenant (Union[None, Unset, int]):
    """

    zone: int
    type: WritableRecordRequestType
    name: str
    value: str
    status: Union[Unset, WritableRecordRequestStatus] = UNSET
    ttl: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    managed: Union[Unset, bool] = UNSET
    disable_ptr: Union[Unset, bool] = UNSET
    custom_fields: Union[Unset, "WritableRecordRequestCustomFields"] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        zone = self.zone

        type = self.type.value

        name = self.name

        value = self.value

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        ttl: Union[None, Unset, int]
        if isinstance(self.ttl, Unset):
            ttl = UNSET
        else:
            ttl = self.ttl

        description = self.description

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        managed = self.managed

        disable_ptr = self.disable_ptr

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "zone": zone,
                "type": type,
                "name": name,
                "value": value,
            }
        )
        if status is not UNSET:
            field_dict["status"] = status
        if ttl is not UNSET:
            field_dict["ttl"] = ttl
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if managed is not UNSET:
            field_dict["managed"] = managed
        if disable_ptr is not UNSET:
            field_dict["disable_ptr"] = disable_ptr
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tenant is not UNSET:
            field_dict["tenant"] = tenant

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        zone = (None, str(self.zone).encode(), "text/plain")

        type = (None, str(self.type.value).encode(), "text/plain")

        name = (None, str(self.name).encode(), "text/plain")

        value = (None, str(self.value).encode(), "text/plain")

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        ttl: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.ttl, Unset):
            ttl = UNSET
        elif isinstance(self.ttl, int):
            ttl = (None, str(self.ttl).encode(), "text/plain")
        else:
            ttl = (None, str(self.ttl).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        managed = (
            self.managed
            if isinstance(self.managed, Unset)
            else (None, str(self.managed).encode(), "text/plain")
        )

        disable_ptr = (
            self.disable_ptr
            if isinstance(self.disable_ptr, Unset)
            else (None, str(self.disable_ptr).encode(), "text/plain")
        )

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "zone": zone,
                "type": type,
                "name": name,
                "value": value,
            }
        )
        if status is not UNSET:
            field_dict["status"] = status
        if ttl is not UNSET:
            field_dict["ttl"] = ttl
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if managed is not UNSET:
            field_dict["managed"] = managed
        if disable_ptr is not UNSET:
            field_dict["disable_ptr"] = disable_ptr
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tenant is not UNSET:
            field_dict["tenant"] = tenant

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_record_request_custom_fields import (
            WritableRecordRequestCustomFields,
        )

        d = src_dict.copy()
        zone = d.pop("zone")

        type = WritableRecordRequestType(d.pop("type"))

        name = d.pop("name")

        value = d.pop("value")

        _status = d.pop("status", UNSET)
        status: Union[Unset, WritableRecordRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = WritableRecordRequestStatus(_status)

        def _parse_ttl(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        ttl = _parse_ttl(d.pop("ttl", UNSET))

        description = d.pop("description", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        managed = d.pop("managed", UNSET)

        disable_ptr = d.pop("disable_ptr", UNSET)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableRecordRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableRecordRequestCustomFields.from_dict(_custom_fields)

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        writable_record_request = cls(
            zone=zone,
            type=type,
            name=name,
            value=value,
            status=status,
            ttl=ttl,
            description=description,
            tags=tags,
            managed=managed,
            disable_ptr=disable_ptr,
            custom_fields=custom_fields,
            tenant=tenant,
        )

        writable_record_request.additional_properties = d
        return writable_record_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
