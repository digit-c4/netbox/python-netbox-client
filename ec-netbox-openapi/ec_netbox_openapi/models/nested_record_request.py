from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.nested_record_request_status import NestedRecordRequestStatus
from ..models.nested_record_request_type import NestedRecordRequestType
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_zone_request import NestedZoneRequest


T = TypeVar("T", bound="NestedRecordRequest")


@_attrs_define
class NestedRecordRequest:
    """Represents an object related through a ForeignKey field. On write, it accepts a primary key (PK) value or a
    dictionary of attributes which can be used to uniquely identify the related object. This class should be
    subclassed to return a full representation of the related object on read.

        Attributes:
            type (NestedRecordRequestType): * `A` - A
                * `A6` - A6
                * `AAAA` - AAAA
                * `AFSDB` - AFSDB
                * `AMTRELAY` - AMTRELAY
                * `APL` - APL
                * `AVC` - AVC
                * `CAA` - CAA
                * `CDNSKEY` - CDNSKEY
                * `CDS` - CDS
                * `CERT` - CERT
                * `CNAME` - CNAME
                * `CSYNC` - CSYNC
                * `DHCID` - DHCID
                * `DLV` - DLV
                * `DNAME` - DNAME
                * `DNSKEY` - DNSKEY
                * `DS` - DS
                * `EUI48` - EUI48
                * `EUI64` - EUI64
                * `GPOS` - GPOS
                * `HINFO` - HINFO
                * `HIP` - HIP
                * `HTTPS` - HTTPS
                * `IPSECKEY` - IPSECKEY
                * `ISDN` - ISDN
                * `KEY` - KEY
                * `KX` - KX
                * `L32` - L32
                * `L64` - L64
                * `LOC` - LOC
                * `LP` - LP
                * `MB` - MB
                * `MD` - MD
                * `MF` - MF
                * `MG` - MG
                * `MINFO` - MINFO
                * `MR` - MR
                * `MX` - MX
                * `NAPTR` - NAPTR
                * `NID` - NID
                * `NINFO` - NINFO
                * `NS` - NS
                * `NSAP` - NSAP
                * `NSAP_PTR` - NSAP_PTR
                * `NSEC` - NSEC
                * `NSEC3` - NSEC3
                * `NSEC3PARAM` - NSEC3PARAM
                * `NULL` - NULL
                * `NXT` - NXT
                * `OPENPGPKEY` - OPENPGPKEY
                * `PTR` - PTR
                * `PX` - PX
                * `RESINFO` - RESINFO
                * `RP` - RP
                * `RRSIG` - RRSIG
                * `RT` - RT
                * `SIG` - SIG
                * `SMIMEA` - SMIMEA
                * `SOA` - SOA
                * `SPF` - SPF
                * `SRV` - SRV
                * `SSHFP` - SSHFP
                * `SVCB` - SVCB
                * `TA` - TA
                * `TLSA` - TLSA
                * `TXT` - TXT
                * `TYPE0` - TYPE0
                * `UNSPEC` - UNSPEC
                * `URI` - URI
                * `WALLET` - WALLET
                * `WKS` - WKS
                * `X25` - X25
                * `ZONEMD` - ZONEMD
            name (str):
            value (str):
            status (Union[Unset, NestedRecordRequestStatus]): * `active` - Active
                * `inactive` - Inactive
            ttl (Union[None, Unset, int]):
            zone (Union[Unset, NestedZoneRequest]): Represents an object related through a ForeignKey field. On write, it
                accepts a primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
    """

    type: NestedRecordRequestType
    name: str
    value: str
    status: Union[Unset, NestedRecordRequestStatus] = UNSET
    ttl: Union[None, Unset, int] = UNSET
    zone: Union[Unset, "NestedZoneRequest"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        type = self.type.value

        name = self.name

        value = self.value

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        ttl: Union[None, Unset, int]
        if isinstance(self.ttl, Unset):
            ttl = UNSET
        else:
            ttl = self.ttl

        zone: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.zone, Unset):
            zone = self.zone.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "type": type,
                "name": name,
                "value": value,
            }
        )
        if status is not UNSET:
            field_dict["status"] = status
        if ttl is not UNSET:
            field_dict["ttl"] = ttl
        if zone is not UNSET:
            field_dict["zone"] = zone

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_zone_request import NestedZoneRequest

        d = src_dict.copy()
        type = NestedRecordRequestType(d.pop("type"))

        name = d.pop("name")

        value = d.pop("value")

        _status = d.pop("status", UNSET)
        status: Union[Unset, NestedRecordRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = NestedRecordRequestStatus(_status)

        def _parse_ttl(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        ttl = _parse_ttl(d.pop("ttl", UNSET))

        _zone = d.pop("zone", UNSET)
        zone: Union[Unset, NestedZoneRequest]
        if isinstance(_zone, Unset):
            zone = UNSET
        else:
            zone = NestedZoneRequest.from_dict(_zone)

        nested_record_request = cls(
            type=type,
            name=name,
            value=value,
            status=status,
            ttl=ttl,
            zone=zone,
        )

        nested_record_request.additional_properties = d
        return nested_record_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
