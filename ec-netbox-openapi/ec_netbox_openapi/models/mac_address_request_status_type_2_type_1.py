from enum import Enum


class MacAddressRequestStatusType2Type1(str, Enum):
    ACTIVE = "active"
    INACTIVE = "inactive"
    QUARANTINED = "quarantined"
    VALUE_3 = ""

    def __str__(self) -> str:
        return str(self.value)
