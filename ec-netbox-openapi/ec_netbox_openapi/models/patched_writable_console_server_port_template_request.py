from typing import Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_console_server_port_template_request_type import (
    PatchedWritableConsoleServerPortTemplateRequestType,
)
from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedWritableConsoleServerPortTemplateRequest")


@_attrs_define
class PatchedWritableConsoleServerPortTemplateRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            device_type (Union[None, Unset, int]):
            module_type (Union[None, Unset, int]):
            name (Union[Unset, str]): {module} is accepted as a substitution for the module bay position when attached to a
                module type.
            label (Union[Unset, str]): Physical label
            type (Union[Unset, PatchedWritableConsoleServerPortTemplateRequestType]): * `de-9` - DE-9
                * `db-25` - DB-25
                * `rj-11` - RJ-11
                * `rj-12` - RJ-12
                * `rj-45` - RJ-45
                * `mini-din-8` - Mini-DIN 8
                * `usb-a` - USB Type A
                * `usb-b` - USB Type B
                * `usb-c` - USB Type C
                * `usb-mini-a` - USB Mini A
                * `usb-mini-b` - USB Mini B
                * `usb-micro-a` - USB Micro A
                * `usb-micro-b` - USB Micro B
                * `usb-micro-ab` - USB Micro AB
                * `other` - Other
            description (Union[Unset, str]):
    """

    device_type: Union[None, Unset, int] = UNSET
    module_type: Union[None, Unset, int] = UNSET
    name: Union[Unset, str] = UNSET
    label: Union[Unset, str] = UNSET
    type: Union[Unset, PatchedWritableConsoleServerPortTemplateRequestType] = UNSET
    description: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        device_type: Union[None, Unset, int]
        if isinstance(self.device_type, Unset):
            device_type = UNSET
        else:
            device_type = self.device_type

        module_type: Union[None, Unset, int]
        if isinstance(self.module_type, Unset):
            module_type = UNSET
        else:
            module_type = self.module_type

        name = self.name

        label = self.label

        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if device_type is not UNSET:
            field_dict["device_type"] = device_type
        if module_type is not UNSET:
            field_dict["module_type"] = module_type
        if name is not UNSET:
            field_dict["name"] = name
        if label is not UNSET:
            field_dict["label"] = label
        if type is not UNSET:
            field_dict["type"] = type
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        device_type: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.device_type, Unset):
            device_type = UNSET
        elif isinstance(self.device_type, int):
            device_type = (None, str(self.device_type).encode(), "text/plain")
        else:
            device_type = (None, str(self.device_type).encode(), "text/plain")

        module_type: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.module_type, Unset):
            module_type = UNSET
        elif isinstance(self.module_type, int):
            module_type = (None, str(self.module_type).encode(), "text/plain")
        else:
            module_type = (None, str(self.module_type).encode(), "text/plain")

        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        label = (
            self.label
            if isinstance(self.label, Unset)
            else (None, str(self.label).encode(), "text/plain")
        )

        type: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.type, Unset):
            type = (None, str(self.type.value).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if device_type is not UNSET:
            field_dict["device_type"] = device_type
        if module_type is not UNSET:
            field_dict["module_type"] = module_type
        if name is not UNSET:
            field_dict["name"] = name
        if label is not UNSET:
            field_dict["label"] = label
        if type is not UNSET:
            field_dict["type"] = type
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()

        def _parse_device_type(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        device_type = _parse_device_type(d.pop("device_type", UNSET))

        def _parse_module_type(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        module_type = _parse_module_type(d.pop("module_type", UNSET))

        name = d.pop("name", UNSET)

        label = d.pop("label", UNSET)

        _type = d.pop("type", UNSET)
        type: Union[Unset, PatchedWritableConsoleServerPortTemplateRequestType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = PatchedWritableConsoleServerPortTemplateRequestType(_type)

        description = d.pop("description", UNSET)

        patched_writable_console_server_port_template_request = cls(
            device_type=device_type,
            module_type=module_type,
            name=name,
            label=label,
            type=type,
            description=description,
        )

        patched_writable_console_server_port_template_request.additional_properties = d
        return patched_writable_console_server_port_template_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
