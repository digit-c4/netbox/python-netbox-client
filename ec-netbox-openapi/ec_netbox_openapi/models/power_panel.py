import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_location import NestedLocation
    from ..models.nested_site import NestedSite
    from ..models.nested_tag import NestedTag
    from ..models.power_panel_custom_fields import PowerPanelCustomFields


T = TypeVar("T", bound="PowerPanel")


@_attrs_define
class PowerPanel:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        site (NestedSite): Represents an object related through a ForeignKey field. On write, it accepts a primary key
            (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (str):
        powerfeed_count (int):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        location (Union['NestedLocation', None, Unset]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, PowerPanelCustomFields]):
    """

    id: int
    url: str
    display: str
    site: "NestedSite"
    name: str
    powerfeed_count: int
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    location: Union["NestedLocation", None, Unset] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "PowerPanelCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_location import NestedLocation

        id = self.id

        url = self.url

        display = self.display

        site = self.site.to_dict()

        name = self.name

        powerfeed_count = self.powerfeed_count

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        location: Union[Dict[str, Any], None, Unset]
        if isinstance(self.location, Unset):
            location = UNSET
        elif isinstance(self.location, NestedLocation):
            location = self.location.to_dict()
        else:
            location = self.location

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "site": site,
                "name": name,
                "powerfeed_count": powerfeed_count,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if location is not UNSET:
            field_dict["location"] = location
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_location import NestedLocation
        from ..models.nested_site import NestedSite
        from ..models.nested_tag import NestedTag
        from ..models.power_panel_custom_fields import PowerPanelCustomFields

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        site = NestedSite.from_dict(d.pop("site"))

        name = d.pop("name")

        powerfeed_count = d.pop("powerfeed_count")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        def _parse_location(data: object) -> Union["NestedLocation", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                location_type_1 = NestedLocation.from_dict(data)

                return location_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedLocation", None, Unset], data)

        location = _parse_location(d.pop("location", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PowerPanelCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PowerPanelCustomFields.from_dict(_custom_fields)

        power_panel = cls(
            id=id,
            url=url,
            display=display,
            site=site,
            name=name,
            powerfeed_count=powerfeed_count,
            created=created,
            last_updated=last_updated,
            location=location,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        power_panel.additional_properties = d
        return power_panel

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
