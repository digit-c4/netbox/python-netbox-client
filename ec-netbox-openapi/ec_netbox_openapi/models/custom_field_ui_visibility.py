from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.custom_field_ui_visibility_label import CustomFieldUiVisibilityLabel
from ..models.custom_field_ui_visibility_value import CustomFieldUiVisibilityValue
from ..types import UNSET, Unset

T = TypeVar("T", bound="CustomFieldUiVisibility")


@_attrs_define
class CustomFieldUiVisibility:
    """
    Attributes:
        value (Union[Unset, CustomFieldUiVisibilityValue]): * `read-write` - Read/write
            * `read-only` - Read-only
            * `hidden` - Hidden
            * `hidden-ifunset` - Hidden (if unset)
        label (Union[Unset, CustomFieldUiVisibilityLabel]):
    """

    value: Union[Unset, CustomFieldUiVisibilityValue] = UNSET
    label: Union[Unset, CustomFieldUiVisibilityLabel] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        value: Union[Unset, str] = UNSET
        if not isinstance(self.value, Unset):
            value = self.value.value

        label: Union[Unset, str] = UNSET
        if not isinstance(self.label, Unset):
            label = self.label.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if value is not UNSET:
            field_dict["value"] = value
        if label is not UNSET:
            field_dict["label"] = label

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        _value = d.pop("value", UNSET)
        value: Union[Unset, CustomFieldUiVisibilityValue]
        if isinstance(_value, Unset):
            value = UNSET
        else:
            value = CustomFieldUiVisibilityValue(_value)

        _label = d.pop("label", UNSET)
        label: Union[Unset, CustomFieldUiVisibilityLabel]
        if isinstance(_label, Unset):
            label = UNSET
        else:
            label = CustomFieldUiVisibilityLabel(_label)

        custom_field_ui_visibility = cls(
            value=value,
            label=label,
        )

        custom_field_ui_visibility.additional_properties = d
        return custom_field_ui_visibility

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
