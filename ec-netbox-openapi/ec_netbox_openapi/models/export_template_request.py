from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_data_source_request import NestedDataSourceRequest


T = TypeVar("T", bound="ExportTemplateRequest")


@_attrs_define
class ExportTemplateRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            content_types (List[str]):
            name (str):
            template_code (str): Jinja2 template code. The list of objects being exported is passed as a context variable
                named <code>queryset</code>.
            description (Union[Unset, str]):
            mime_type (Union[Unset, str]): Defaults to <code>text/plain; charset=utf-8</code>
            file_extension (Union[Unset, str]): Extension to append to the rendered filename
            as_attachment (Union[Unset, bool]): Download file as attachment
            data_source (Union[Unset, NestedDataSourceRequest]): Represents an object related through a ForeignKey field. On
                write, it accepts a primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
    """

    content_types: List[str]
    name: str
    template_code: str
    description: Union[Unset, str] = UNSET
    mime_type: Union[Unset, str] = UNSET
    file_extension: Union[Unset, str] = UNSET
    as_attachment: Union[Unset, bool] = UNSET
    data_source: Union[Unset, "NestedDataSourceRequest"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        content_types = self.content_types

        name = self.name

        template_code = self.template_code

        description = self.description

        mime_type = self.mime_type

        file_extension = self.file_extension

        as_attachment = self.as_attachment

        data_source: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.data_source, Unset):
            data_source = self.data_source.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "content_types": content_types,
                "name": name,
                "template_code": template_code,
            }
        )
        if description is not UNSET:
            field_dict["description"] = description
        if mime_type is not UNSET:
            field_dict["mime_type"] = mime_type
        if file_extension is not UNSET:
            field_dict["file_extension"] = file_extension
        if as_attachment is not UNSET:
            field_dict["as_attachment"] = as_attachment
        if data_source is not UNSET:
            field_dict["data_source"] = data_source

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_data_source_request import NestedDataSourceRequest

        d = src_dict.copy()
        content_types = cast(List[str], d.pop("content_types"))

        name = d.pop("name")

        template_code = d.pop("template_code")

        description = d.pop("description", UNSET)

        mime_type = d.pop("mime_type", UNSET)

        file_extension = d.pop("file_extension", UNSET)

        as_attachment = d.pop("as_attachment", UNSET)

        _data_source = d.pop("data_source", UNSET)
        data_source: Union[Unset, NestedDataSourceRequest]
        if isinstance(_data_source, Unset):
            data_source = UNSET
        else:
            data_source = NestedDataSourceRequest.from_dict(_data_source)

        export_template_request = cls(
            content_types=content_types,
            name=name,
            template_code=template_code,
            description=description,
            mime_type=mime_type,
            file_extension=file_extension,
            as_attachment=as_attachment,
            data_source=data_source,
        )

        export_template_request.additional_properties = d
        return export_template_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
