from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

if TYPE_CHECKING:
    from ..models.nested_module import NestedModule


T = TypeVar("T", bound="NestedModuleBay")


@_attrs_define
class NestedModuleBay:
    """Represents an object related through a ForeignKey field. On write, it accepts a primary key (PK) value or a
    dictionary of attributes which can be used to uniquely identify the related object. This class should be
    subclassed to return a full representation of the related object on read.

        Attributes:
            id (int):
            url (str):
            display (str):
            module (Union['NestedModule', None]):
            name (str):
    """

    id: int
    url: str
    display: str
    module: Union["NestedModule", None]
    name: str
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_module import NestedModule

        id = self.id

        url = self.url

        display = self.display

        module: Union[Dict[str, Any], None]
        if isinstance(self.module, NestedModule):
            module = self.module.to_dict()
        else:
            module = self.module

        name = self.name

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "module": module,
                "name": name,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_module import NestedModule

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        def _parse_module(data: object) -> Union["NestedModule", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                module_type_1 = NestedModule.from_dict(data)

                return module_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedModule", None], data)

        module = _parse_module(d.pop("module"))

        name = d.pop("name")

        nested_module_bay = cls(
            id=id,
            url=url,
            display=display,
            module=module,
            name=name,
        )

        nested_module_bay.additional_properties = d
        return nested_module_bay

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
