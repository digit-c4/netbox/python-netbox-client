import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_wireless_lan_request_authentication_cipher import (
    PatchedWritableWirelessLANRequestAuthenticationCipher,
)
from ..models.patched_writable_wireless_lan_request_authentication_type import (
    PatchedWritableWirelessLANRequestAuthenticationType,
)
from ..models.patched_writable_wireless_lan_request_status import (
    PatchedWritableWirelessLANRequestStatus,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_writable_wireless_lan_request_custom_fields import (
        PatchedWritableWirelessLANRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedWritableWirelessLANRequest")


@_attrs_define
class PatchedWritableWirelessLANRequest:
    """Adds support for custom fields and tags.

    Attributes:
        ssid (Union[Unset, str]):
        description (Union[Unset, str]):
        group (Union[None, Unset, int]):
        status (Union[Unset, PatchedWritableWirelessLANRequestStatus]): * `active` - Active
            * `reserved` - Reserved
            * `disabled` - Disabled
            * `deprecated` - Deprecated
        vlan (Union[None, Unset, int]):
        tenant (Union[None, Unset, int]):
        auth_type (Union[Unset, PatchedWritableWirelessLANRequestAuthenticationType]): * `open` - Open
            * `wep` - WEP
            * `wpa-personal` - WPA Personal (PSK)
            * `wpa-enterprise` - WPA Enterprise
        auth_cipher (Union[Unset, PatchedWritableWirelessLANRequestAuthenticationCipher]): * `auto` - Auto
            * `tkip` - TKIP
            * `aes` - AES
        auth_psk (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, PatchedWritableWirelessLANRequestCustomFields]):
    """

    ssid: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    group: Union[None, Unset, int] = UNSET
    status: Union[Unset, PatchedWritableWirelessLANRequestStatus] = UNSET
    vlan: Union[None, Unset, int] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    auth_type: Union[Unset, PatchedWritableWirelessLANRequestAuthenticationType] = UNSET
    auth_cipher: Union[Unset, PatchedWritableWirelessLANRequestAuthenticationCipher] = (
        UNSET
    )
    auth_psk: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "PatchedWritableWirelessLANRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        ssid = self.ssid

        description = self.description

        group: Union[None, Unset, int]
        if isinstance(self.group, Unset):
            group = UNSET
        else:
            group = self.group

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        vlan: Union[None, Unset, int]
        if isinstance(self.vlan, Unset):
            vlan = UNSET
        else:
            vlan = self.vlan

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        auth_type: Union[Unset, str] = UNSET
        if not isinstance(self.auth_type, Unset):
            auth_type = self.auth_type.value

        auth_cipher: Union[Unset, str] = UNSET
        if not isinstance(self.auth_cipher, Unset):
            auth_cipher = self.auth_cipher.value

        auth_psk = self.auth_psk

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if ssid is not UNSET:
            field_dict["ssid"] = ssid
        if description is not UNSET:
            field_dict["description"] = description
        if group is not UNSET:
            field_dict["group"] = group
        if status is not UNSET:
            field_dict["status"] = status
        if vlan is not UNSET:
            field_dict["vlan"] = vlan
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if auth_type is not UNSET:
            field_dict["auth_type"] = auth_type
        if auth_cipher is not UNSET:
            field_dict["auth_cipher"] = auth_cipher
        if auth_psk is not UNSET:
            field_dict["auth_psk"] = auth_psk
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        ssid = (
            self.ssid
            if isinstance(self.ssid, Unset)
            else (None, str(self.ssid).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        group: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.group, Unset):
            group = UNSET
        elif isinstance(self.group, int):
            group = (None, str(self.group).encode(), "text/plain")
        else:
            group = (None, str(self.group).encode(), "text/plain")

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        vlan: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.vlan, Unset):
            vlan = UNSET
        elif isinstance(self.vlan, int):
            vlan = (None, str(self.vlan).encode(), "text/plain")
        else:
            vlan = (None, str(self.vlan).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        auth_type: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.auth_type, Unset):
            auth_type = (None, str(self.auth_type.value).encode(), "text/plain")

        auth_cipher: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.auth_cipher, Unset):
            auth_cipher = (None, str(self.auth_cipher.value).encode(), "text/plain")

        auth_psk = (
            self.auth_psk
            if isinstance(self.auth_psk, Unset)
            else (None, str(self.auth_psk).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if ssid is not UNSET:
            field_dict["ssid"] = ssid
        if description is not UNSET:
            field_dict["description"] = description
        if group is not UNSET:
            field_dict["group"] = group
        if status is not UNSET:
            field_dict["status"] = status
        if vlan is not UNSET:
            field_dict["vlan"] = vlan
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if auth_type is not UNSET:
            field_dict["auth_type"] = auth_type
        if auth_cipher is not UNSET:
            field_dict["auth_cipher"] = auth_cipher
        if auth_psk is not UNSET:
            field_dict["auth_psk"] = auth_psk
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_writable_wireless_lan_request_custom_fields import (
            PatchedWritableWirelessLANRequestCustomFields,
        )

        d = src_dict.copy()
        ssid = d.pop("ssid", UNSET)

        description = d.pop("description", UNSET)

        def _parse_group(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        group = _parse_group(d.pop("group", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, PatchedWritableWirelessLANRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = PatchedWritableWirelessLANRequestStatus(_status)

        def _parse_vlan(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vlan = _parse_vlan(d.pop("vlan", UNSET))

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        _auth_type = d.pop("auth_type", UNSET)
        auth_type: Union[Unset, PatchedWritableWirelessLANRequestAuthenticationType]
        if isinstance(_auth_type, Unset):
            auth_type = UNSET
        else:
            auth_type = PatchedWritableWirelessLANRequestAuthenticationType(_auth_type)

        _auth_cipher = d.pop("auth_cipher", UNSET)
        auth_cipher: Union[Unset, PatchedWritableWirelessLANRequestAuthenticationCipher]
        if isinstance(_auth_cipher, Unset):
            auth_cipher = UNSET
        else:
            auth_cipher = PatchedWritableWirelessLANRequestAuthenticationCipher(
                _auth_cipher
            )

        auth_psk = d.pop("auth_psk", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedWritableWirelessLANRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedWritableWirelessLANRequestCustomFields.from_dict(
                _custom_fields
            )

        patched_writable_wireless_lan_request = cls(
            ssid=ssid,
            description=description,
            group=group,
            status=status,
            vlan=vlan,
            tenant=tenant,
            auth_type=auth_type,
            auth_cipher=auth_cipher,
            auth_psk=auth_psk,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        patched_writable_wireless_lan_request.additional_properties = d
        return patched_writable_wireless_lan_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
