from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="Bind")


@_attrs_define
class Bind:
    """Container Bind Serializer class

    Attributes:
        host_path (str):
        container_path (str):
        read_only (Union[Unset, bool]):
    """

    host_path: str
    container_path: str
    read_only: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        host_path = self.host_path

        container_path = self.container_path

        read_only = self.read_only

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "host_path": host_path,
                "container_path": container_path,
            }
        )
        if read_only is not UNSET:
            field_dict["read_only"] = read_only

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        host_path = d.pop("host_path")

        container_path = d.pop("container_path")

        read_only = d.pop("read_only", UNSET)

        bind = cls(
            host_path=host_path,
            container_path=container_path,
            read_only=read_only,
        )

        bind.additional_properties = d
        return bind

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
