from typing import Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

T = TypeVar("T", bound="WritableFHRPGroupAssignmentRequest")


@_attrs_define
class WritableFHRPGroupAssignmentRequest:
    """Adds support for custom fields and tags.

    Attributes:
        group (int):
        interface_type (str):
        interface_id (int):
        priority (int):
    """

    group: int
    interface_type: str
    interface_id: int
    priority: int
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        group = self.group

        interface_type = self.interface_type

        interface_id = self.interface_id

        priority = self.priority

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "group": group,
                "interface_type": interface_type,
                "interface_id": interface_id,
                "priority": priority,
            }
        )

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        group = (None, str(self.group).encode(), "text/plain")

        interface_type = (None, str(self.interface_type).encode(), "text/plain")

        interface_id = (None, str(self.interface_id).encode(), "text/plain")

        priority = (None, str(self.priority).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "group": group,
                "interface_type": interface_type,
                "interface_id": interface_id,
                "priority": priority,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        group = d.pop("group")

        interface_type = d.pop("interface_type")

        interface_id = d.pop("interface_id")

        priority = d.pop("priority")

        writable_fhrp_group_assignment_request = cls(
            group=group,
            interface_type=interface_type,
            interface_id=interface_id,
            priority=priority,
        )

        writable_fhrp_group_assignment_request.additional_properties = d
        return writable_fhrp_group_assignment_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
