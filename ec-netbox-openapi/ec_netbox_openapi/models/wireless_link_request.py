from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.wireless_link_request_auth_cipher import WirelessLinkRequestAuthCipher
from ..models.wireless_link_request_auth_type import WirelessLinkRequestAuthType
from ..models.wireless_link_request_status import WirelessLinkRequestStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_interface_request import NestedInterfaceRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_tenant_request import NestedTenantRequest
    from ..models.wireless_link_request_custom_fields import (
        WirelessLinkRequestCustomFields,
    )


T = TypeVar("T", bound="WirelessLinkRequest")


@_attrs_define
class WirelessLinkRequest:
    """Adds support for custom fields and tags.

    Attributes:
        interface_a (NestedInterfaceRequest): Represents an object related through a ForeignKey field. On write, it
            accepts a primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        interface_b (NestedInterfaceRequest): Represents an object related through a ForeignKey field. On write, it
            accepts a primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        ssid (Union[Unset, str]):
        status (Union[Unset, WirelessLinkRequestStatus]): * `connected` - Connected
            * `planned` - Planned
            * `decommissioning` - Decommissioning
        tenant (Union['NestedTenantRequest', None, Unset]):
        auth_type (Union[Unset, WirelessLinkRequestAuthType]): * `open` - Open
            * `wep` - WEP
            * `wpa-personal` - WPA Personal (PSK)
            * `wpa-enterprise` - WPA Enterprise
        auth_cipher (Union[Unset, WirelessLinkRequestAuthCipher]): * `auto` - Auto
            * `tkip` - TKIP
            * `aes` - AES
        auth_psk (Union[Unset, str]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WirelessLinkRequestCustomFields]):
    """

    interface_a: "NestedInterfaceRequest"
    interface_b: "NestedInterfaceRequest"
    ssid: Union[Unset, str] = UNSET
    status: Union[Unset, WirelessLinkRequestStatus] = UNSET
    tenant: Union["NestedTenantRequest", None, Unset] = UNSET
    auth_type: Union[Unset, WirelessLinkRequestAuthType] = UNSET
    auth_cipher: Union[Unset, WirelessLinkRequestAuthCipher] = UNSET
    auth_psk: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WirelessLinkRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_tenant_request import NestedTenantRequest

        interface_a = self.interface_a.to_dict()

        interface_b = self.interface_b.to_dict()

        ssid = self.ssid

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenantRequest):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        auth_type: Union[Unset, str] = UNSET
        if not isinstance(self.auth_type, Unset):
            auth_type = self.auth_type.value

        auth_cipher: Union[Unset, str] = UNSET
        if not isinstance(self.auth_cipher, Unset):
            auth_cipher = self.auth_cipher.value

        auth_psk = self.auth_psk

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "interface_a": interface_a,
                "interface_b": interface_b,
            }
        )
        if ssid is not UNSET:
            field_dict["ssid"] = ssid
        if status is not UNSET:
            field_dict["status"] = status
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if auth_type is not UNSET:
            field_dict["auth_type"] = auth_type
        if auth_cipher is not UNSET:
            field_dict["auth_cipher"] = auth_cipher
        if auth_psk is not UNSET:
            field_dict["auth_psk"] = auth_psk
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_interface_request import NestedInterfaceRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_tenant_request import NestedTenantRequest
        from ..models.wireless_link_request_custom_fields import (
            WirelessLinkRequestCustomFields,
        )

        d = src_dict.copy()
        interface_a = NestedInterfaceRequest.from_dict(d.pop("interface_a"))

        interface_b = NestedInterfaceRequest.from_dict(d.pop("interface_b"))

        ssid = d.pop("ssid", UNSET)

        _status = d.pop("status", UNSET)
        status: Union[Unset, WirelessLinkRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = WirelessLinkRequestStatus(_status)

        def _parse_tenant(data: object) -> Union["NestedTenantRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenantRequest.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenantRequest", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        _auth_type = d.pop("auth_type", UNSET)
        auth_type: Union[Unset, WirelessLinkRequestAuthType]
        if isinstance(_auth_type, Unset):
            auth_type = UNSET
        else:
            auth_type = WirelessLinkRequestAuthType(_auth_type)

        _auth_cipher = d.pop("auth_cipher", UNSET)
        auth_cipher: Union[Unset, WirelessLinkRequestAuthCipher]
        if isinstance(_auth_cipher, Unset):
            auth_cipher = UNSET
        else:
            auth_cipher = WirelessLinkRequestAuthCipher(_auth_cipher)

        auth_psk = d.pop("auth_psk", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WirelessLinkRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WirelessLinkRequestCustomFields.from_dict(_custom_fields)

        wireless_link_request = cls(
            interface_a=interface_a,
            interface_b=interface_b,
            ssid=ssid,
            status=status,
            tenant=tenant,
            auth_type=auth_type,
            auth_cipher=auth_cipher,
            auth_psk=auth_psk,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        wireless_link_request.additional_properties = d
        return wireless_link_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
