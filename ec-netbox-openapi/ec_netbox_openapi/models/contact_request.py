from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.contact_request_custom_fields import ContactRequestCustomFields
    from ..models.nested_contact_group_request import NestedContactGroupRequest
    from ..models.nested_tag_request import NestedTagRequest


T = TypeVar("T", bound="ContactRequest")


@_attrs_define
class ContactRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        group (Union['NestedContactGroupRequest', None, Unset]):
        title (Union[Unset, str]):
        phone (Union[Unset, str]):
        email (Union[Unset, str]):
        address (Union[Unset, str]):
        link (Union[Unset, str]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, ContactRequestCustomFields]):
    """

    name: str
    group: Union["NestedContactGroupRequest", None, Unset] = UNSET
    title: Union[Unset, str] = UNSET
    phone: Union[Unset, str] = UNSET
    email: Union[Unset, str] = UNSET
    address: Union[Unset, str] = UNSET
    link: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "ContactRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_contact_group_request import NestedContactGroupRequest

        name = self.name

        group: Union[Dict[str, Any], None, Unset]
        if isinstance(self.group, Unset):
            group = UNSET
        elif isinstance(self.group, NestedContactGroupRequest):
            group = self.group.to_dict()
        else:
            group = self.group

        title = self.title

        phone = self.phone

        email = self.email

        address = self.address

        link = self.link

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
            }
        )
        if group is not UNSET:
            field_dict["group"] = group
        if title is not UNSET:
            field_dict["title"] = title
        if phone is not UNSET:
            field_dict["phone"] = phone
        if email is not UNSET:
            field_dict["email"] = email
        if address is not UNSET:
            field_dict["address"] = address
        if link is not UNSET:
            field_dict["link"] = link
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.contact_request_custom_fields import ContactRequestCustomFields
        from ..models.nested_contact_group_request import NestedContactGroupRequest
        from ..models.nested_tag_request import NestedTagRequest

        d = src_dict.copy()
        name = d.pop("name")

        def _parse_group(
            data: object,
        ) -> Union["NestedContactGroupRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                group_type_1 = NestedContactGroupRequest.from_dict(data)

                return group_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedContactGroupRequest", None, Unset], data)

        group = _parse_group(d.pop("group", UNSET))

        title = d.pop("title", UNSET)

        phone = d.pop("phone", UNSET)

        email = d.pop("email", UNSET)

        address = d.pop("address", UNSET)

        link = d.pop("link", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ContactRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ContactRequestCustomFields.from_dict(_custom_fields)

        contact_request = cls(
            name=name,
            group=group,
            title=title,
            phone=phone,
            email=email,
            address=address,
            link=link,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        contact_request.additional_properties = d
        return contact_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
