from typing import Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

T = TypeVar("T", bound="WritableBookmarkRequest")


@_attrs_define
class WritableBookmarkRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            object_type (str):
            object_id (int):
            user (int):
    """

    object_type: str
    object_id: int
    user: int
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        object_type = self.object_type

        object_id = self.object_id

        user = self.user

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "object_type": object_type,
                "object_id": object_id,
                "user": user,
            }
        )

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        object_type = (None, str(self.object_type).encode(), "text/plain")

        object_id = (None, str(self.object_id).encode(), "text/plain")

        user = (None, str(self.user).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "object_type": object_type,
                "object_id": object_id,
                "user": user,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        object_type = d.pop("object_type")

        object_id = d.pop("object_id")

        user = d.pop("user")

        writable_bookmark_request = cls(
            object_type=object_type,
            object_id=object_id,
            user=user,
        )

        writable_bookmark_request.additional_properties = d
        return writable_bookmark_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
