from enum import Enum


class PatchedWritableRackRequestOuterUnit(str, Enum):
    IN = "in"
    MM = "mm"
    VALUE_2 = ""

    def __str__(self) -> str:
        return str(self.value)
