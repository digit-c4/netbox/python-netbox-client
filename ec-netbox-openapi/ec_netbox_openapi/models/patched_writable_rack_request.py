import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_rack_request_outer_unit import (
    PatchedWritableRackRequestOuterUnit,
)
from ..models.patched_writable_rack_request_status import (
    PatchedWritableRackRequestStatus,
)
from ..models.patched_writable_rack_request_type import PatchedWritableRackRequestType
from ..models.patched_writable_rack_request_weight_unit import (
    PatchedWritableRackRequestWeightUnit,
)
from ..models.patched_writable_rack_request_width import PatchedWritableRackRequestWidth
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_writable_rack_request_custom_fields import (
        PatchedWritableRackRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedWritableRackRequest")


@_attrs_define
class PatchedWritableRackRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (Union[Unset, str]):
        facility_id (Union[None, Unset, str]):
        site (Union[Unset, int]):
        location (Union[None, Unset, int]):
        tenant (Union[None, Unset, int]):
        status (Union[Unset, PatchedWritableRackRequestStatus]): * `reserved` - Reserved
            * `available` - Available
            * `planned` - Planned
            * `active` - Active
            * `deprecated` - Deprecated
        role (Union[None, Unset, int]): Functional role
        serial (Union[Unset, str]):
        asset_tag (Union[None, Unset, str]): A unique tag used to identify this rack
        type (Union[Unset, PatchedWritableRackRequestType]): * `2-post-frame` - 2-post frame
            * `4-post-frame` - 4-post frame
            * `4-post-cabinet` - 4-post cabinet
            * `wall-frame` - Wall-mounted frame
            * `wall-frame-vertical` - Wall-mounted frame (vertical)
            * `wall-cabinet` - Wall-mounted cabinet
            * `wall-cabinet-vertical` - Wall-mounted cabinet (vertical)
        width (Union[Unset, PatchedWritableRackRequestWidth]): Rail-to-rail width

            * `10` - 10 inches
            * `19` - 19 inches
            * `21` - 21 inches
            * `23` - 23 inches
        u_height (Union[Unset, int]): Height in rack units
        starting_unit (Union[Unset, int]): Starting unit for rack
        weight (Union[None, Unset, float]):
        max_weight (Union[None, Unset, int]): Maximum load capacity for the rack
        weight_unit (Union[Unset, PatchedWritableRackRequestWeightUnit]): * `kg` - Kilograms
            * `g` - Grams
            * `lb` - Pounds
            * `oz` - Ounces
        desc_units (Union[Unset, bool]): Units are numbered top-to-bottom
        outer_width (Union[None, Unset, int]): Outer dimension of rack (width)
        outer_depth (Union[None, Unset, int]): Outer dimension of rack (depth)
        outer_unit (Union[Unset, PatchedWritableRackRequestOuterUnit]): * `mm` - Millimeters
            * `in` - Inches
        mounting_depth (Union[None, Unset, int]): Maximum depth of a mounted device, in millimeters. For four-post
            racks, this is the distance between the front and rear rails.
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, PatchedWritableRackRequestCustomFields]):
    """

    name: Union[Unset, str] = UNSET
    facility_id: Union[None, Unset, str] = UNSET
    site: Union[Unset, int] = UNSET
    location: Union[None, Unset, int] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    status: Union[Unset, PatchedWritableRackRequestStatus] = UNSET
    role: Union[None, Unset, int] = UNSET
    serial: Union[Unset, str] = UNSET
    asset_tag: Union[None, Unset, str] = UNSET
    type: Union[Unset, PatchedWritableRackRequestType] = UNSET
    width: Union[Unset, PatchedWritableRackRequestWidth] = UNSET
    u_height: Union[Unset, int] = UNSET
    starting_unit: Union[Unset, int] = UNSET
    weight: Union[None, Unset, float] = UNSET
    max_weight: Union[None, Unset, int] = UNSET
    weight_unit: Union[Unset, PatchedWritableRackRequestWeightUnit] = UNSET
    desc_units: Union[Unset, bool] = UNSET
    outer_width: Union[None, Unset, int] = UNSET
    outer_depth: Union[None, Unset, int] = UNSET
    outer_unit: Union[Unset, PatchedWritableRackRequestOuterUnit] = UNSET
    mounting_depth: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "PatchedWritableRackRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        facility_id: Union[None, Unset, str]
        if isinstance(self.facility_id, Unset):
            facility_id = UNSET
        else:
            facility_id = self.facility_id

        site = self.site

        location: Union[None, Unset, int]
        if isinstance(self.location, Unset):
            location = UNSET
        else:
            location = self.location

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        role: Union[None, Unset, int]
        if isinstance(self.role, Unset):
            role = UNSET
        else:
            role = self.role

        serial = self.serial

        asset_tag: Union[None, Unset, str]
        if isinstance(self.asset_tag, Unset):
            asset_tag = UNSET
        else:
            asset_tag = self.asset_tag

        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        width: Union[Unset, int] = UNSET
        if not isinstance(self.width, Unset):
            width = self.width.value

        u_height = self.u_height

        starting_unit = self.starting_unit

        weight: Union[None, Unset, float]
        if isinstance(self.weight, Unset):
            weight = UNSET
        else:
            weight = self.weight

        max_weight: Union[None, Unset, int]
        if isinstance(self.max_weight, Unset):
            max_weight = UNSET
        else:
            max_weight = self.max_weight

        weight_unit: Union[Unset, str] = UNSET
        if not isinstance(self.weight_unit, Unset):
            weight_unit = self.weight_unit.value

        desc_units = self.desc_units

        outer_width: Union[None, Unset, int]
        if isinstance(self.outer_width, Unset):
            outer_width = UNSET
        else:
            outer_width = self.outer_width

        outer_depth: Union[None, Unset, int]
        if isinstance(self.outer_depth, Unset):
            outer_depth = UNSET
        else:
            outer_depth = self.outer_depth

        outer_unit: Union[Unset, str] = UNSET
        if not isinstance(self.outer_unit, Unset):
            outer_unit = self.outer_unit.value

        mounting_depth: Union[None, Unset, int]
        if isinstance(self.mounting_depth, Unset):
            mounting_depth = UNSET
        else:
            mounting_depth = self.mounting_depth

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if facility_id is not UNSET:
            field_dict["facility_id"] = facility_id
        if site is not UNSET:
            field_dict["site"] = site
        if location is not UNSET:
            field_dict["location"] = location
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if serial is not UNSET:
            field_dict["serial"] = serial
        if asset_tag is not UNSET:
            field_dict["asset_tag"] = asset_tag
        if type is not UNSET:
            field_dict["type"] = type
        if width is not UNSET:
            field_dict["width"] = width
        if u_height is not UNSET:
            field_dict["u_height"] = u_height
        if starting_unit is not UNSET:
            field_dict["starting_unit"] = starting_unit
        if weight is not UNSET:
            field_dict["weight"] = weight
        if max_weight is not UNSET:
            field_dict["max_weight"] = max_weight
        if weight_unit is not UNSET:
            field_dict["weight_unit"] = weight_unit
        if desc_units is not UNSET:
            field_dict["desc_units"] = desc_units
        if outer_width is not UNSET:
            field_dict["outer_width"] = outer_width
        if outer_depth is not UNSET:
            field_dict["outer_depth"] = outer_depth
        if outer_unit is not UNSET:
            field_dict["outer_unit"] = outer_unit
        if mounting_depth is not UNSET:
            field_dict["mounting_depth"] = mounting_depth
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        facility_id: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.facility_id, Unset):
            facility_id = UNSET
        elif isinstance(self.facility_id, str):
            facility_id = (None, str(self.facility_id).encode(), "text/plain")
        else:
            facility_id = (None, str(self.facility_id).encode(), "text/plain")

        site = (
            self.site
            if isinstance(self.site, Unset)
            else (None, str(self.site).encode(), "text/plain")
        )

        location: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.location, Unset):
            location = UNSET
        elif isinstance(self.location, int):
            location = (None, str(self.location).encode(), "text/plain")
        else:
            location = (None, str(self.location).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        role: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.role, Unset):
            role = UNSET
        elif isinstance(self.role, int):
            role = (None, str(self.role).encode(), "text/plain")
        else:
            role = (None, str(self.role).encode(), "text/plain")

        serial = (
            self.serial
            if isinstance(self.serial, Unset)
            else (None, str(self.serial).encode(), "text/plain")
        )

        asset_tag: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.asset_tag, Unset):
            asset_tag = UNSET
        elif isinstance(self.asset_tag, str):
            asset_tag = (None, str(self.asset_tag).encode(), "text/plain")
        else:
            asset_tag = (None, str(self.asset_tag).encode(), "text/plain")

        type: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.type, Unset):
            type = (None, str(self.type.value).encode(), "text/plain")

        width: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.width, Unset):
            width = (None, str(self.width.value).encode(), "text/plain")

        u_height = (
            self.u_height
            if isinstance(self.u_height, Unset)
            else (None, str(self.u_height).encode(), "text/plain")
        )

        starting_unit = (
            self.starting_unit
            if isinstance(self.starting_unit, Unset)
            else (None, str(self.starting_unit).encode(), "text/plain")
        )

        weight: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.weight, Unset):
            weight = UNSET
        elif isinstance(self.weight, float):
            weight = (None, str(self.weight).encode(), "text/plain")
        else:
            weight = (None, str(self.weight).encode(), "text/plain")

        max_weight: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.max_weight, Unset):
            max_weight = UNSET
        elif isinstance(self.max_weight, int):
            max_weight = (None, str(self.max_weight).encode(), "text/plain")
        else:
            max_weight = (None, str(self.max_weight).encode(), "text/plain")

        weight_unit: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.weight_unit, Unset):
            weight_unit = (None, str(self.weight_unit.value).encode(), "text/plain")

        desc_units = (
            self.desc_units
            if isinstance(self.desc_units, Unset)
            else (None, str(self.desc_units).encode(), "text/plain")
        )

        outer_width: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.outer_width, Unset):
            outer_width = UNSET
        elif isinstance(self.outer_width, int):
            outer_width = (None, str(self.outer_width).encode(), "text/plain")
        else:
            outer_width = (None, str(self.outer_width).encode(), "text/plain")

        outer_depth: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.outer_depth, Unset):
            outer_depth = UNSET
        elif isinstance(self.outer_depth, int):
            outer_depth = (None, str(self.outer_depth).encode(), "text/plain")
        else:
            outer_depth = (None, str(self.outer_depth).encode(), "text/plain")

        outer_unit: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.outer_unit, Unset):
            outer_unit = (None, str(self.outer_unit.value).encode(), "text/plain")

        mounting_depth: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.mounting_depth, Unset):
            mounting_depth = UNSET
        elif isinstance(self.mounting_depth, int):
            mounting_depth = (None, str(self.mounting_depth).encode(), "text/plain")
        else:
            mounting_depth = (None, str(self.mounting_depth).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if facility_id is not UNSET:
            field_dict["facility_id"] = facility_id
        if site is not UNSET:
            field_dict["site"] = site
        if location is not UNSET:
            field_dict["location"] = location
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if status is not UNSET:
            field_dict["status"] = status
        if role is not UNSET:
            field_dict["role"] = role
        if serial is not UNSET:
            field_dict["serial"] = serial
        if asset_tag is not UNSET:
            field_dict["asset_tag"] = asset_tag
        if type is not UNSET:
            field_dict["type"] = type
        if width is not UNSET:
            field_dict["width"] = width
        if u_height is not UNSET:
            field_dict["u_height"] = u_height
        if starting_unit is not UNSET:
            field_dict["starting_unit"] = starting_unit
        if weight is not UNSET:
            field_dict["weight"] = weight
        if max_weight is not UNSET:
            field_dict["max_weight"] = max_weight
        if weight_unit is not UNSET:
            field_dict["weight_unit"] = weight_unit
        if desc_units is not UNSET:
            field_dict["desc_units"] = desc_units
        if outer_width is not UNSET:
            field_dict["outer_width"] = outer_width
        if outer_depth is not UNSET:
            field_dict["outer_depth"] = outer_depth
        if outer_unit is not UNSET:
            field_dict["outer_unit"] = outer_unit
        if mounting_depth is not UNSET:
            field_dict["mounting_depth"] = mounting_depth
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_writable_rack_request_custom_fields import (
            PatchedWritableRackRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name", UNSET)

        def _parse_facility_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        facility_id = _parse_facility_id(d.pop("facility_id", UNSET))

        site = d.pop("site", UNSET)

        def _parse_location(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        location = _parse_location(d.pop("location", UNSET))

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, PatchedWritableRackRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = PatchedWritableRackRequestStatus(_status)

        def _parse_role(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        role = _parse_role(d.pop("role", UNSET))

        serial = d.pop("serial", UNSET)

        def _parse_asset_tag(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        asset_tag = _parse_asset_tag(d.pop("asset_tag", UNSET))

        _type = d.pop("type", UNSET)
        type: Union[Unset, PatchedWritableRackRequestType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = PatchedWritableRackRequestType(_type)

        _width = d.pop("width", UNSET)
        width: Union[Unset, PatchedWritableRackRequestWidth]
        if isinstance(_width, Unset):
            width = UNSET
        else:
            width = PatchedWritableRackRequestWidth(_width)

        u_height = d.pop("u_height", UNSET)

        starting_unit = d.pop("starting_unit", UNSET)

        def _parse_weight(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        weight = _parse_weight(d.pop("weight", UNSET))

        def _parse_max_weight(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        max_weight = _parse_max_weight(d.pop("max_weight", UNSET))

        _weight_unit = d.pop("weight_unit", UNSET)
        weight_unit: Union[Unset, PatchedWritableRackRequestWeightUnit]
        if isinstance(_weight_unit, Unset):
            weight_unit = UNSET
        else:
            weight_unit = PatchedWritableRackRequestWeightUnit(_weight_unit)

        desc_units = d.pop("desc_units", UNSET)

        def _parse_outer_width(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        outer_width = _parse_outer_width(d.pop("outer_width", UNSET))

        def _parse_outer_depth(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        outer_depth = _parse_outer_depth(d.pop("outer_depth", UNSET))

        _outer_unit = d.pop("outer_unit", UNSET)
        outer_unit: Union[Unset, PatchedWritableRackRequestOuterUnit]
        if isinstance(_outer_unit, Unset):
            outer_unit = UNSET
        else:
            outer_unit = PatchedWritableRackRequestOuterUnit(_outer_unit)

        def _parse_mounting_depth(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        mounting_depth = _parse_mounting_depth(d.pop("mounting_depth", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedWritableRackRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedWritableRackRequestCustomFields.from_dict(
                _custom_fields
            )

        patched_writable_rack_request = cls(
            name=name,
            facility_id=facility_id,
            site=site,
            location=location,
            tenant=tenant,
            status=status,
            role=role,
            serial=serial,
            asset_tag=asset_tag,
            type=type,
            width=width,
            u_height=u_height,
            starting_unit=starting_unit,
            weight=weight,
            max_weight=max_weight,
            weight_unit=weight_unit,
            desc_units=desc_units,
            outer_width=outer_width,
            outer_depth=outer_depth,
            outer_unit=outer_unit,
            mounting_depth=mounting_depth,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        patched_writable_rack_request.additional_properties = d
        return patched_writable_rack_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
