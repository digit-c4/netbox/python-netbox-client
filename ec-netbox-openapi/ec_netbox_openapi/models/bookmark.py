import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

if TYPE_CHECKING:
    from ..models.nested_user import NestedUser


T = TypeVar("T", bound="Bookmark")


@_attrs_define
class Bookmark:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            id (int):
            url (str):
            display (str):
            object_type (str):
            object_id (int):
            object_ (Any):
            user (NestedUser): Represents an object related through a ForeignKey field. On write, it accepts a primary key
                (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            created (datetime.datetime):
    """

    id: int
    url: str
    display: str
    object_type: str
    object_id: int
    object_: Any
    user: "NestedUser"
    created: datetime.datetime
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        object_type = self.object_type

        object_id = self.object_id

        object_ = self.object_

        user = self.user.to_dict()

        created = self.created.isoformat()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "object_type": object_type,
                "object_id": object_id,
                "object": object_,
                "user": user,
                "created": created,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_user import NestedUser

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        object_type = d.pop("object_type")

        object_id = d.pop("object_id")

        object_ = d.pop("object")

        user = NestedUser.from_dict(d.pop("user"))

        created = isoparse(d.pop("created"))

        bookmark = cls(
            id=id,
            url=url,
            display=display,
            object_type=object_type,
            object_id=object_id,
            object_=object_,
            user=user,
            created=created,
        )

        bookmark.additional_properties = d
        return bookmark

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
