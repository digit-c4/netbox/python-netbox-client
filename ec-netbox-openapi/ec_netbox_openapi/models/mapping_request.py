from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.mapping_request_auth import MappingRequestAuth
from ..models.mapping_request_client_max_body_size import (
    MappingRequestClientMaxBodySize,
)
from ..models.mapping_request_extra_protocols_item import (
    MappingRequestExtraProtocolsItem,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.mapping_request_custom_fields import MappingRequestCustomFields
    from ..models.nested_http_header_request import NestedHttpHeaderRequest
    from ..models.nested_saml_config_request import NestedSamlConfigRequest
    from ..models.nested_tag_request import NestedTagRequest


T = TypeVar("T", bound="MappingRequest")


@_attrs_define
class MappingRequest:
    """Mapping Serializer class

    Attributes:
        source (str):
        target (str):
        authentication (Union[Unset, MappingRequestAuth]): * `none` - None
            * `ldap` - Ldap
            * `ecas` - Ecas
        testingpage (Union[None, Unset, str]):
        webdav (Union[Unset, bool]):
        comment (Union[Unset, str]):
        gzip_proxied (Union[Unset, bool]):
        keepalive_requests (Union[Unset, int]):
        keepalive_timeout (Union[Unset, int]):
        proxy_cache (Union[Unset, bool]):
        proxy_read_timeout (Union[Unset, int]):
        client_max_body_size (Union[Unset, MappingRequestClientMaxBodySize]): * `1` - 1
            * `128` - 128
            * `512` - 512
            * `1024` - 1024
            * `2048` - 2048
        extra_protocols (Union[Unset, List[MappingRequestExtraProtocolsItem]]):
        sorry_page (Union[Unset, str]):
        custom_fields (Union[Unset, MappingRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
        http_headers (Union[Unset, List['NestedHttpHeaderRequest']]):
        saml_config (Union['NestedSamlConfigRequest', None, Unset]):
        proxy_buffer_size (Union[None, Unset, int]):
        proxy_buffer (Union[None, Unset, int]):
        proxy_busy_buffer (Union[None, Unset, int]):
        proxy_buffer_responses (Union[Unset, bool]):
        proxy_buffer_requests (Union[Unset, bool]):
    """

    source: str
    target: str
    authentication: Union[Unset, MappingRequestAuth] = UNSET
    testingpage: Union[None, Unset, str] = UNSET
    webdav: Union[Unset, bool] = UNSET
    comment: Union[Unset, str] = UNSET
    gzip_proxied: Union[Unset, bool] = UNSET
    keepalive_requests: Union[Unset, int] = UNSET
    keepalive_timeout: Union[Unset, int] = UNSET
    proxy_cache: Union[Unset, bool] = UNSET
    proxy_read_timeout: Union[Unset, int] = UNSET
    client_max_body_size: Union[Unset, MappingRequestClientMaxBodySize] = UNSET
    extra_protocols: Union[Unset, List[MappingRequestExtraProtocolsItem]] = UNSET
    sorry_page: Union[Unset, str] = UNSET
    custom_fields: Union[Unset, "MappingRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    http_headers: Union[Unset, List["NestedHttpHeaderRequest"]] = UNSET
    saml_config: Union["NestedSamlConfigRequest", None, Unset] = UNSET
    proxy_buffer_size: Union[None, Unset, int] = UNSET
    proxy_buffer: Union[None, Unset, int] = UNSET
    proxy_busy_buffer: Union[None, Unset, int] = UNSET
    proxy_buffer_responses: Union[Unset, bool] = UNSET
    proxy_buffer_requests: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_saml_config_request import NestedSamlConfigRequest

        source = self.source

        target = self.target

        authentication: Union[Unset, str] = UNSET
        if not isinstance(self.authentication, Unset):
            authentication = self.authentication.value

        testingpage: Union[None, Unset, str]
        if isinstance(self.testingpage, Unset):
            testingpage = UNSET
        else:
            testingpage = self.testingpage

        webdav = self.webdav

        comment = self.comment

        gzip_proxied = self.gzip_proxied

        keepalive_requests = self.keepalive_requests

        keepalive_timeout = self.keepalive_timeout

        proxy_cache = self.proxy_cache

        proxy_read_timeout = self.proxy_read_timeout

        client_max_body_size: Union[Unset, int] = UNSET
        if not isinstance(self.client_max_body_size, Unset):
            client_max_body_size = self.client_max_body_size.value

        extra_protocols: Union[Unset, List[str]] = UNSET
        if not isinstance(self.extra_protocols, Unset):
            extra_protocols = []
            for extra_protocols_item_data in self.extra_protocols:
                extra_protocols_item = extra_protocols_item_data.value
                extra_protocols.append(extra_protocols_item)

        sorry_page = self.sorry_page

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        http_headers: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.http_headers, Unset):
            http_headers = []
            for http_headers_item_data in self.http_headers:
                http_headers_item = http_headers_item_data.to_dict()
                http_headers.append(http_headers_item)

        saml_config: Union[Dict[str, Any], None, Unset]
        if isinstance(self.saml_config, Unset):
            saml_config = UNSET
        elif isinstance(self.saml_config, NestedSamlConfigRequest):
            saml_config = self.saml_config.to_dict()
        else:
            saml_config = self.saml_config

        proxy_buffer_size: Union[None, Unset, int]
        if isinstance(self.proxy_buffer_size, Unset):
            proxy_buffer_size = UNSET
        else:
            proxy_buffer_size = self.proxy_buffer_size

        proxy_buffer: Union[None, Unset, int]
        if isinstance(self.proxy_buffer, Unset):
            proxy_buffer = UNSET
        else:
            proxy_buffer = self.proxy_buffer

        proxy_busy_buffer: Union[None, Unset, int]
        if isinstance(self.proxy_busy_buffer, Unset):
            proxy_busy_buffer = UNSET
        else:
            proxy_busy_buffer = self.proxy_busy_buffer

        proxy_buffer_responses = self.proxy_buffer_responses

        proxy_buffer_requests = self.proxy_buffer_requests

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "source": source,
                "target": target,
            }
        )
        if authentication is not UNSET:
            field_dict["authentication"] = authentication
        if testingpage is not UNSET:
            field_dict["testingpage"] = testingpage
        if webdav is not UNSET:
            field_dict["webdav"] = webdav
        if comment is not UNSET:
            field_dict["Comment"] = comment
        if gzip_proxied is not UNSET:
            field_dict["gzip_proxied"] = gzip_proxied
        if keepalive_requests is not UNSET:
            field_dict["keepalive_requests"] = keepalive_requests
        if keepalive_timeout is not UNSET:
            field_dict["keepalive_timeout"] = keepalive_timeout
        if proxy_cache is not UNSET:
            field_dict["proxy_cache"] = proxy_cache
        if proxy_read_timeout is not UNSET:
            field_dict["proxy_read_timeout"] = proxy_read_timeout
        if client_max_body_size is not UNSET:
            field_dict["client_max_body_size"] = client_max_body_size
        if extra_protocols is not UNSET:
            field_dict["extra_protocols"] = extra_protocols
        if sorry_page is not UNSET:
            field_dict["sorry_page"] = sorry_page
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags
        if http_headers is not UNSET:
            field_dict["http_headers"] = http_headers
        if saml_config is not UNSET:
            field_dict["saml_config"] = saml_config
        if proxy_buffer_size is not UNSET:
            field_dict["proxy_buffer_size"] = proxy_buffer_size
        if proxy_buffer is not UNSET:
            field_dict["proxy_buffer"] = proxy_buffer
        if proxy_busy_buffer is not UNSET:
            field_dict["proxy_busy_buffer"] = proxy_busy_buffer
        if proxy_buffer_responses is not UNSET:
            field_dict["proxy_buffer_responses"] = proxy_buffer_responses
        if proxy_buffer_requests is not UNSET:
            field_dict["proxy_buffer_requests"] = proxy_buffer_requests

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.mapping_request_custom_fields import MappingRequestCustomFields
        from ..models.nested_http_header_request import NestedHttpHeaderRequest
        from ..models.nested_saml_config_request import NestedSamlConfigRequest
        from ..models.nested_tag_request import NestedTagRequest

        d = src_dict.copy()
        source = d.pop("source")

        target = d.pop("target")

        _authentication = d.pop("authentication", UNSET)
        authentication: Union[Unset, MappingRequestAuth]
        if isinstance(_authentication, Unset):
            authentication = UNSET
        else:
            authentication = MappingRequestAuth(_authentication)

        def _parse_testingpage(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        testingpage = _parse_testingpage(d.pop("testingpage", UNSET))

        webdav = d.pop("webdav", UNSET)

        comment = d.pop("Comment", UNSET)

        gzip_proxied = d.pop("gzip_proxied", UNSET)

        keepalive_requests = d.pop("keepalive_requests", UNSET)

        keepalive_timeout = d.pop("keepalive_timeout", UNSET)

        proxy_cache = d.pop("proxy_cache", UNSET)

        proxy_read_timeout = d.pop("proxy_read_timeout", UNSET)

        _client_max_body_size = d.pop("client_max_body_size", UNSET)
        client_max_body_size: Union[Unset, MappingRequestClientMaxBodySize]
        if isinstance(_client_max_body_size, Unset):
            client_max_body_size = UNSET
        else:
            client_max_body_size = MappingRequestClientMaxBodySize(
                _client_max_body_size
            )

        extra_protocols = []
        _extra_protocols = d.pop("extra_protocols", UNSET)
        for extra_protocols_item_data in _extra_protocols or []:
            extra_protocols_item = MappingRequestExtraProtocolsItem(
                extra_protocols_item_data
            )

            extra_protocols.append(extra_protocols_item)

        sorry_page = d.pop("sorry_page", UNSET)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, MappingRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = MappingRequestCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        http_headers = []
        _http_headers = d.pop("http_headers", UNSET)
        for http_headers_item_data in _http_headers or []:
            http_headers_item = NestedHttpHeaderRequest.from_dict(
                http_headers_item_data
            )

            http_headers.append(http_headers_item)

        def _parse_saml_config(
            data: object,
        ) -> Union["NestedSamlConfigRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                saml_config_type_1 = NestedSamlConfigRequest.from_dict(data)

                return saml_config_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedSamlConfigRequest", None, Unset], data)

        saml_config = _parse_saml_config(d.pop("saml_config", UNSET))

        def _parse_proxy_buffer_size(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        proxy_buffer_size = _parse_proxy_buffer_size(d.pop("proxy_buffer_size", UNSET))

        def _parse_proxy_buffer(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        proxy_buffer = _parse_proxy_buffer(d.pop("proxy_buffer", UNSET))

        def _parse_proxy_busy_buffer(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        proxy_busy_buffer = _parse_proxy_busy_buffer(d.pop("proxy_busy_buffer", UNSET))

        proxy_buffer_responses = d.pop("proxy_buffer_responses", UNSET)

        proxy_buffer_requests = d.pop("proxy_buffer_requests", UNSET)

        mapping_request = cls(
            source=source,
            target=target,
            authentication=authentication,
            testingpage=testingpage,
            webdav=webdav,
            comment=comment,
            gzip_proxied=gzip_proxied,
            keepalive_requests=keepalive_requests,
            keepalive_timeout=keepalive_timeout,
            proxy_cache=proxy_cache,
            proxy_read_timeout=proxy_read_timeout,
            client_max_body_size=client_max_body_size,
            extra_protocols=extra_protocols,
            sorry_page=sorry_page,
            custom_fields=custom_fields,
            tags=tags,
            http_headers=http_headers,
            saml_config=saml_config,
            proxy_buffer_size=proxy_buffer_size,
            proxy_buffer=proxy_buffer,
            proxy_busy_buffer=proxy_busy_buffer,
            proxy_buffer_responses=proxy_buffer_responses,
            proxy_buffer_requests=proxy_buffer_requests,
        )

        mapping_request.additional_properties = d
        return mapping_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
