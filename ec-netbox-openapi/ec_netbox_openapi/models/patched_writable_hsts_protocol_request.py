from typing import Any, Dict, List, Tuple, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_hsts_protocol_request_max_age import (
    PatchedWritableHstsProtocolRequestMaxAge,
)
from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedWritableHstsProtocolRequest")


@_attrs_define
class PatchedWritableHstsProtocolRequest:
    """Hsts Protocol Serializer class

    Attributes:
        mapping (Union[Unset, int]):
        subdomains (Union[Unset, bool]):
        preload_flag (Union[Unset, bool]):
        max_age (Union[Unset, PatchedWritableHstsProtocolRequestMaxAge]): * `0` - no cache
            * `31536000` - one year
            * `63072000` - two years
    """

    mapping: Union[Unset, int] = UNSET
    subdomains: Union[Unset, bool] = UNSET
    preload_flag: Union[Unset, bool] = UNSET
    max_age: Union[Unset, PatchedWritableHstsProtocolRequestMaxAge] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        mapping = self.mapping

        subdomains = self.subdomains

        preload_flag = self.preload_flag

        max_age: Union[Unset, int] = UNSET
        if not isinstance(self.max_age, Unset):
            max_age = self.max_age.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if mapping is not UNSET:
            field_dict["mapping"] = mapping
        if subdomains is not UNSET:
            field_dict["subdomains"] = subdomains
        if preload_flag is not UNSET:
            field_dict["preload_flag"] = preload_flag
        if max_age is not UNSET:
            field_dict["max_age"] = max_age

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        mapping = (
            self.mapping
            if isinstance(self.mapping, Unset)
            else (None, str(self.mapping).encode(), "text/plain")
        )

        subdomains = (
            self.subdomains
            if isinstance(self.subdomains, Unset)
            else (None, str(self.subdomains).encode(), "text/plain")
        )

        preload_flag = (
            self.preload_flag
            if isinstance(self.preload_flag, Unset)
            else (None, str(self.preload_flag).encode(), "text/plain")
        )

        max_age: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.max_age, Unset):
            max_age = (None, str(self.max_age.value).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if mapping is not UNSET:
            field_dict["mapping"] = mapping
        if subdomains is not UNSET:
            field_dict["subdomains"] = subdomains
        if preload_flag is not UNSET:
            field_dict["preload_flag"] = preload_flag
        if max_age is not UNSET:
            field_dict["max_age"] = max_age

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        mapping = d.pop("mapping", UNSET)

        subdomains = d.pop("subdomains", UNSET)

        preload_flag = d.pop("preload_flag", UNSET)

        _max_age = d.pop("max_age", UNSET)
        max_age: Union[Unset, PatchedWritableHstsProtocolRequestMaxAge]
        if isinstance(_max_age, Unset):
            max_age = UNSET
        else:
            max_age = PatchedWritableHstsProtocolRequestMaxAge(_max_age)

        patched_writable_hsts_protocol_request = cls(
            mapping=mapping,
            subdomains=subdomains,
            preload_flag=preload_flag,
            max_age=max_age,
        )

        patched_writable_hsts_protocol_request.additional_properties = d
        return patched_writable_hsts_protocol_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
