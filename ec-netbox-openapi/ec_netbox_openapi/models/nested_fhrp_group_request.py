from typing import Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.nested_fhrp_group_request_protocol import NestedFHRPGroupRequestProtocol

T = TypeVar("T", bound="NestedFHRPGroupRequest")


@_attrs_define
class NestedFHRPGroupRequest:
    """Represents an object related through a ForeignKey field. On write, it accepts a primary key (PK) value or a
    dictionary of attributes which can be used to uniquely identify the related object. This class should be
    subclassed to return a full representation of the related object on read.

        Attributes:
            protocol (NestedFHRPGroupRequestProtocol): * `vrrp2` - VRRPv2
                * `vrrp3` - VRRPv3
                * `carp` - CARP
                * `clusterxl` - ClusterXL
                * `hsrp` - HSRP
                * `glbp` - GLBP
                * `other` - Other
            group_id (int):
    """

    protocol: NestedFHRPGroupRequestProtocol
    group_id: int
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        protocol = self.protocol.value

        group_id = self.group_id

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "protocol": protocol,
                "group_id": group_id,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        protocol = NestedFHRPGroupRequestProtocol(d.pop("protocol"))

        group_id = d.pop("group_id")

        nested_fhrp_group_request = cls(
            protocol=protocol,
            group_id=group_id,
        )

        nested_fhrp_group_request.additional_properties = d
        return nested_fhrp_group_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
