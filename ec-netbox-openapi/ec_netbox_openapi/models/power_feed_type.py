from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.power_feed_type_label import PowerFeedTypeLabel
from ..models.power_feed_type_value import PowerFeedTypeValue
from ..types import UNSET, Unset

T = TypeVar("T", bound="PowerFeedType")


@_attrs_define
class PowerFeedType:
    """
    Attributes:
        value (Union[Unset, PowerFeedTypeValue]): * `primary` - Primary
            * `redundant` - Redundant
        label (Union[Unset, PowerFeedTypeLabel]):
    """

    value: Union[Unset, PowerFeedTypeValue] = UNSET
    label: Union[Unset, PowerFeedTypeLabel] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        value: Union[Unset, str] = UNSET
        if not isinstance(self.value, Unset):
            value = self.value.value

        label: Union[Unset, str] = UNSET
        if not isinstance(self.label, Unset):
            label = self.label.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if value is not UNSET:
            field_dict["value"] = value
        if label is not UNSET:
            field_dict["label"] = label

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        _value = d.pop("value", UNSET)
        value: Union[Unset, PowerFeedTypeValue]
        if isinstance(_value, Unset):
            value = UNSET
        else:
            value = PowerFeedTypeValue(_value)

        _label = d.pop("label", UNSET)
        label: Union[Unset, PowerFeedTypeLabel]
        if isinstance(_label, Unset):
            label = UNSET
        else:
            label = PowerFeedTypeLabel(_label)

        power_feed_type = cls(
            value=value,
            label=label,
        )

        power_feed_type.additional_properties = d
        return power_feed_type

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
