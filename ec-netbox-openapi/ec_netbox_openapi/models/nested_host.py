from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.nested_host_operation import NestedHostOperation
from ..models.nested_host_state import NestedHostState
from ..types import UNSET, Unset

T = TypeVar("T", bound="NestedHost")


@_attrs_define
class NestedHost:
    """Nested Host Serializer class

    Attributes:
        id (int):
        url (str):
        display (str):
        endpoint (str):
        name (str):
        state (Union[Unset, NestedHostState]): * `created` - Created
            * `running` - Running
            * `deleted` - Deleted
            * `refreshing` - Refreshing
        agent_version (Union[None, Unset, str]):
        docker_api_version (Union[None, Unset, str]):
        operation (Union[Unset, NestedHostOperation]): * `refresh` - Refresh
            * `none` - None
    """

    id: int
    url: str
    display: str
    endpoint: str
    name: str
    state: Union[Unset, NestedHostState] = UNSET
    agent_version: Union[None, Unset, str] = UNSET
    docker_api_version: Union[None, Unset, str] = UNSET
    operation: Union[Unset, NestedHostOperation] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        endpoint = self.endpoint

        name = self.name

        state: Union[Unset, str] = UNSET
        if not isinstance(self.state, Unset):
            state = self.state.value

        agent_version: Union[None, Unset, str]
        if isinstance(self.agent_version, Unset):
            agent_version = UNSET
        else:
            agent_version = self.agent_version

        docker_api_version: Union[None, Unset, str]
        if isinstance(self.docker_api_version, Unset):
            docker_api_version = UNSET
        else:
            docker_api_version = self.docker_api_version

        operation: Union[Unset, str] = UNSET
        if not isinstance(self.operation, Unset):
            operation = self.operation.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "endpoint": endpoint,
                "name": name,
            }
        )
        if state is not UNSET:
            field_dict["state"] = state
        if agent_version is not UNSET:
            field_dict["agent_version"] = agent_version
        if docker_api_version is not UNSET:
            field_dict["docker_api_version"] = docker_api_version
        if operation is not UNSET:
            field_dict["operation"] = operation

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        endpoint = d.pop("endpoint")

        name = d.pop("name")

        _state = d.pop("state", UNSET)
        state: Union[Unset, NestedHostState]
        if isinstance(_state, Unset):
            state = UNSET
        else:
            state = NestedHostState(_state)

        def _parse_agent_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        agent_version = _parse_agent_version(d.pop("agent_version", UNSET))

        def _parse_docker_api_version(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        docker_api_version = _parse_docker_api_version(
            d.pop("docker_api_version", UNSET)
        )

        _operation = d.pop("operation", UNSET)
        operation: Union[Unset, NestedHostOperation]
        if isinstance(_operation, Unset):
            operation = UNSET
        else:
            operation = NestedHostOperation(_operation)

        nested_host = cls(
            id=id,
            url=url,
            display=display,
            endpoint=endpoint,
            name=name,
            state=state,
            agent_version=agent_version,
            docker_api_version=docker_api_version,
            operation=operation,
        )

        nested_host.additional_properties = d
        return nested_host

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
