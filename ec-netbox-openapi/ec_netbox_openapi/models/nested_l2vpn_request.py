from typing import Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.nested_l2vpn_request_type import NestedL2VPNRequestType
from ..types import UNSET, Unset

T = TypeVar("T", bound="NestedL2VPNRequest")


@_attrs_define
class NestedL2VPNRequest:
    """Represents an object related through a ForeignKey field. On write, it accepts a primary key (PK) value or a
    dictionary of attributes which can be used to uniquely identify the related object. This class should be
    subclassed to return a full representation of the related object on read.

        Attributes:
            name (str):
            slug (str):
            type (NestedL2VPNRequestType): * `vpws` - VPWS
                * `vpls` - VPLS
                * `vxlan` - VXLAN
                * `vxlan-evpn` - VXLAN-EVPN
                * `mpls-evpn` - MPLS EVPN
                * `pbb-evpn` - PBB EVPN
                * `epl` - EPL
                * `evpl` - EVPL
                * `ep-lan` - Ethernet Private LAN
                * `evp-lan` - Ethernet Virtual Private LAN
                * `ep-tree` - Ethernet Private Tree
                * `evp-tree` - Ethernet Virtual Private Tree
            identifier (Union[None, Unset, int]):
    """

    name: str
    slug: str
    type: NestedL2VPNRequestType
    identifier: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        slug = self.slug

        type = self.type.value

        identifier: Union[None, Unset, int]
        if isinstance(self.identifier, Unset):
            identifier = UNSET
        else:
            identifier = self.identifier

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "slug": slug,
                "type": type,
            }
        )
        if identifier is not UNSET:
            field_dict["identifier"] = identifier

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name")

        slug = d.pop("slug")

        type = NestedL2VPNRequestType(d.pop("type"))

        def _parse_identifier(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        identifier = _parse_identifier(d.pop("identifier", UNSET))

        nested_l2vpn_request = cls(
            name=name,
            slug=slug,
            type=type,
            identifier=identifier,
        )

        nested_l2vpn_request.additional_properties = d
        return nested_l2vpn_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
