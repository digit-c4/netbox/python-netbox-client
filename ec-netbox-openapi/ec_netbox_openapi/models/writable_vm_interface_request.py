import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_vm_interface_request_mode import WritableVMInterfaceRequestMode
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_vm_interface_request_custom_fields import (
        WritableVMInterfaceRequestCustomFields,
    )


T = TypeVar("T", bound="WritableVMInterfaceRequest")


@_attrs_define
class WritableVMInterfaceRequest:
    """Adds support for custom fields and tags.

    Attributes:
        virtual_machine (int):
        name (str):
        enabled (Union[Unset, bool]):
        parent (Union[None, Unset, int]):
        bridge (Union[None, Unset, int]):
        mtu (Union[None, Unset, int]):
        mac_address (Union[None, Unset, str]):
        description (Union[Unset, str]):
        mode (Union[Unset, WritableVMInterfaceRequestMode]): IEEE 802.1Q tagging strategy

            * `access` - Access
            * `tagged` - Tagged
            * `tagged-all` - Tagged (All)
        untagged_vlan (Union[None, Unset, int]):
        tagged_vlans (Union[Unset, List[int]]):
        vrf (Union[None, Unset, int]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritableVMInterfaceRequestCustomFields]):
    """

    virtual_machine: int
    name: str
    enabled: Union[Unset, bool] = UNSET
    parent: Union[None, Unset, int] = UNSET
    bridge: Union[None, Unset, int] = UNSET
    mtu: Union[None, Unset, int] = UNSET
    mac_address: Union[None, Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    mode: Union[Unset, WritableVMInterfaceRequestMode] = UNSET
    untagged_vlan: Union[None, Unset, int] = UNSET
    tagged_vlans: Union[Unset, List[int]] = UNSET
    vrf: Union[None, Unset, int] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WritableVMInterfaceRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        virtual_machine = self.virtual_machine

        name = self.name

        enabled = self.enabled

        parent: Union[None, Unset, int]
        if isinstance(self.parent, Unset):
            parent = UNSET
        else:
            parent = self.parent

        bridge: Union[None, Unset, int]
        if isinstance(self.bridge, Unset):
            bridge = UNSET
        else:
            bridge = self.bridge

        mtu: Union[None, Unset, int]
        if isinstance(self.mtu, Unset):
            mtu = UNSET
        else:
            mtu = self.mtu

        mac_address: Union[None, Unset, str]
        if isinstance(self.mac_address, Unset):
            mac_address = UNSET
        else:
            mac_address = self.mac_address

        description = self.description

        mode: Union[Unset, str] = UNSET
        if not isinstance(self.mode, Unset):
            mode = self.mode.value

        untagged_vlan: Union[None, Unset, int]
        if isinstance(self.untagged_vlan, Unset):
            untagged_vlan = UNSET
        else:
            untagged_vlan = self.untagged_vlan

        tagged_vlans: Union[Unset, List[int]] = UNSET
        if not isinstance(self.tagged_vlans, Unset):
            tagged_vlans = self.tagged_vlans

        vrf: Union[None, Unset, int]
        if isinstance(self.vrf, Unset):
            vrf = UNSET
        else:
            vrf = self.vrf

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "virtual_machine": virtual_machine,
                "name": name,
            }
        )
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if parent is not UNSET:
            field_dict["parent"] = parent
        if bridge is not UNSET:
            field_dict["bridge"] = bridge
        if mtu is not UNSET:
            field_dict["mtu"] = mtu
        if mac_address is not UNSET:
            field_dict["mac_address"] = mac_address
        if description is not UNSET:
            field_dict["description"] = description
        if mode is not UNSET:
            field_dict["mode"] = mode
        if untagged_vlan is not UNSET:
            field_dict["untagged_vlan"] = untagged_vlan
        if tagged_vlans is not UNSET:
            field_dict["tagged_vlans"] = tagged_vlans
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        virtual_machine = (None, str(self.virtual_machine).encode(), "text/plain")

        name = (None, str(self.name).encode(), "text/plain")

        enabled = (
            self.enabled
            if isinstance(self.enabled, Unset)
            else (None, str(self.enabled).encode(), "text/plain")
        )

        parent: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.parent, Unset):
            parent = UNSET
        elif isinstance(self.parent, int):
            parent = (None, str(self.parent).encode(), "text/plain")
        else:
            parent = (None, str(self.parent).encode(), "text/plain")

        bridge: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.bridge, Unset):
            bridge = UNSET
        elif isinstance(self.bridge, int):
            bridge = (None, str(self.bridge).encode(), "text/plain")
        else:
            bridge = (None, str(self.bridge).encode(), "text/plain")

        mtu: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.mtu, Unset):
            mtu = UNSET
        elif isinstance(self.mtu, int):
            mtu = (None, str(self.mtu).encode(), "text/plain")
        else:
            mtu = (None, str(self.mtu).encode(), "text/plain")

        mac_address: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.mac_address, Unset):
            mac_address = UNSET
        elif isinstance(self.mac_address, str):
            mac_address = (None, str(self.mac_address).encode(), "text/plain")
        else:
            mac_address = (None, str(self.mac_address).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        mode: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.mode, Unset):
            mode = (None, str(self.mode.value).encode(), "text/plain")

        untagged_vlan: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.untagged_vlan, Unset):
            untagged_vlan = UNSET
        elif isinstance(self.untagged_vlan, int):
            untagged_vlan = (None, str(self.untagged_vlan).encode(), "text/plain")
        else:
            untagged_vlan = (None, str(self.untagged_vlan).encode(), "text/plain")

        tagged_vlans: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tagged_vlans, Unset):
            _temp_tagged_vlans = self.tagged_vlans
            tagged_vlans = (
                None,
                json.dumps(_temp_tagged_vlans).encode(),
                "application/json",
            )

        vrf: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.vrf, Unset):
            vrf = UNSET
        elif isinstance(self.vrf, int):
            vrf = (None, str(self.vrf).encode(), "text/plain")
        else:
            vrf = (None, str(self.vrf).encode(), "text/plain")

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "virtual_machine": virtual_machine,
                "name": name,
            }
        )
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if parent is not UNSET:
            field_dict["parent"] = parent
        if bridge is not UNSET:
            field_dict["bridge"] = bridge
        if mtu is not UNSET:
            field_dict["mtu"] = mtu
        if mac_address is not UNSET:
            field_dict["mac_address"] = mac_address
        if description is not UNSET:
            field_dict["description"] = description
        if mode is not UNSET:
            field_dict["mode"] = mode
        if untagged_vlan is not UNSET:
            field_dict["untagged_vlan"] = untagged_vlan
        if tagged_vlans is not UNSET:
            field_dict["tagged_vlans"] = tagged_vlans
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_vm_interface_request_custom_fields import (
            WritableVMInterfaceRequestCustomFields,
        )

        d = src_dict.copy()
        virtual_machine = d.pop("virtual_machine")

        name = d.pop("name")

        enabled = d.pop("enabled", UNSET)

        def _parse_parent(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        parent = _parse_parent(d.pop("parent", UNSET))

        def _parse_bridge(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        bridge = _parse_bridge(d.pop("bridge", UNSET))

        def _parse_mtu(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        mtu = _parse_mtu(d.pop("mtu", UNSET))

        def _parse_mac_address(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        mac_address = _parse_mac_address(d.pop("mac_address", UNSET))

        description = d.pop("description", UNSET)

        _mode = d.pop("mode", UNSET)
        mode: Union[Unset, WritableVMInterfaceRequestMode]
        if isinstance(_mode, Unset):
            mode = UNSET
        else:
            mode = WritableVMInterfaceRequestMode(_mode)

        def _parse_untagged_vlan(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        untagged_vlan = _parse_untagged_vlan(d.pop("untagged_vlan", UNSET))

        tagged_vlans = cast(List[int], d.pop("tagged_vlans", UNSET))

        def _parse_vrf(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vrf = _parse_vrf(d.pop("vrf", UNSET))

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableVMInterfaceRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableVMInterfaceRequestCustomFields.from_dict(
                _custom_fields
            )

        writable_vm_interface_request = cls(
            virtual_machine=virtual_machine,
            name=name,
            enabled=enabled,
            parent=parent,
            bridge=bridge,
            mtu=mtu,
            mac_address=mac_address,
            description=description,
            mode=mode,
            untagged_vlan=untagged_vlan,
            tagged_vlans=tagged_vlans,
            vrf=vrf,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_vm_interface_request.additional_properties = d
        return writable_vm_interface_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
