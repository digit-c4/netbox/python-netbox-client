from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.container_request_cap_add_type_0_item_type_1 import (
    ContainerRequestCapAddType0ItemType1,
)
from ..models.container_request_cap_add_type_0_item_type_2_type_1 import (
    ContainerRequestCapAddType0ItemType2Type1,
)
from ..models.container_request_cap_add_type_0_item_type_3_type_1 import (
    ContainerRequestCapAddType0ItemType3Type1,
)
from ..models.container_request_operation import ContainerRequestOperation
from ..models.container_request_restart_policy import ContainerRequestRestartPolicy
from ..models.container_request_state import ContainerRequestState
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.bind_request import BindRequest
    from ..models.container_request_custom_fields import ContainerRequestCustomFields
    from ..models.device_request import DeviceRequest
    from ..models.env_request import EnvRequest
    from ..models.label_request import LabelRequest
    from ..models.mount_request import MountRequest
    from ..models.nested_host_request import NestedHostRequest
    from ..models.nested_image_request import NestedImageRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.network_setting_request import NetworkSettingRequest
    from ..models.port_request import PortRequest


T = TypeVar("T", bound="ContainerRequest")


@_attrs_define
class ContainerRequest:
    """Container Serializer class

    Attributes:
        host (NestedHostRequest): Nested Host Serializer class
        image (NestedImageRequest): Nested Image Serializer class
        name (str):
        state (Union[Unset, ContainerRequestState]): * `created` - Created
            * `restarting` - Restarting
            * `running` - Running
            * `paused` - Paused
            * `exited` - Exited
            * `dead` - Dead
            * `none` - None
        operation (Union[Unset, ContainerRequestOperation]): * `create` - Create
            * `start` - Start
            * `restart` - Restart
            * `stop` - Stop
            * `recreate` - Recreate
            * `kill` - Kill
            * `none` - None
        status (Union[None, Unset, str]):
        container_id (Union[None, Unset, str]):
        hostname (Union[None, Unset, str]):
        restart_policy (Union[Unset, ContainerRequestRestartPolicy]): * `no` - no
            * `on-failure` - on-failure
            * `always` - always
            * `unless-stopped` - unless-stopped
        cap_add (Union[List[Union[ContainerRequestCapAddType0ItemType1, ContainerRequestCapAddType0ItemType2Type1,
            ContainerRequestCapAddType0ItemType3Type1, None]], None, Unset]):
        ports (Union[Unset, List['PortRequest']]):
        env (Union[Unset, List['EnvRequest']]):
        labels (Union[Unset, List['LabelRequest']]):
        mounts (Union[Unset, List['MountRequest']]):
        binds (Union[Unset, List['BindRequest']]):
        network_settings (Union[Unset, List['NetworkSettingRequest']]):
        devices (Union[Unset, List['DeviceRequest']]):
        custom_fields (Union[Unset, ContainerRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    host: "NestedHostRequest"
    image: "NestedImageRequest"
    name: str
    state: Union[Unset, ContainerRequestState] = UNSET
    operation: Union[Unset, ContainerRequestOperation] = UNSET
    status: Union[None, Unset, str] = UNSET
    container_id: Union[None, Unset, str] = UNSET
    hostname: Union[None, Unset, str] = UNSET
    restart_policy: Union[Unset, ContainerRequestRestartPolicy] = UNSET
    cap_add: Union[
        List[
            Union[
                ContainerRequestCapAddType0ItemType1,
                ContainerRequestCapAddType0ItemType2Type1,
                ContainerRequestCapAddType0ItemType3Type1,
                None,
            ]
        ],
        None,
        Unset,
    ] = UNSET
    ports: Union[Unset, List["PortRequest"]] = UNSET
    env: Union[Unset, List["EnvRequest"]] = UNSET
    labels: Union[Unset, List["LabelRequest"]] = UNSET
    mounts: Union[Unset, List["MountRequest"]] = UNSET
    binds: Union[Unset, List["BindRequest"]] = UNSET
    network_settings: Union[Unset, List["NetworkSettingRequest"]] = UNSET
    devices: Union[Unset, List["DeviceRequest"]] = UNSET
    custom_fields: Union[Unset, "ContainerRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        host = self.host.to_dict()

        image = self.image.to_dict()

        name = self.name

        state: Union[Unset, str] = UNSET
        if not isinstance(self.state, Unset):
            state = self.state.value

        operation: Union[Unset, str] = UNSET
        if not isinstance(self.operation, Unset):
            operation = self.operation.value

        status: Union[None, Unset, str]
        if isinstance(self.status, Unset):
            status = UNSET
        else:
            status = self.status

        container_id: Union[None, Unset, str]
        if isinstance(self.container_id, Unset):
            container_id = UNSET
        else:
            container_id = self.container_id

        hostname: Union[None, Unset, str]
        if isinstance(self.hostname, Unset):
            hostname = UNSET
        else:
            hostname = self.hostname

        restart_policy: Union[Unset, str] = UNSET
        if not isinstance(self.restart_policy, Unset):
            restart_policy = self.restart_policy.value

        cap_add: Union[List[Union[None, str]], None, Unset]
        if isinstance(self.cap_add, Unset):
            cap_add = UNSET
        elif isinstance(self.cap_add, list):
            cap_add = []
            for cap_add_type_0_item_data in self.cap_add:
                cap_add_type_0_item: Union[None, str]
                if isinstance(
                    cap_add_type_0_item_data, ContainerRequestCapAddType0ItemType1
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                elif isinstance(
                    cap_add_type_0_item_data, ContainerRequestCapAddType0ItemType2Type1
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                elif isinstance(
                    cap_add_type_0_item_data, ContainerRequestCapAddType0ItemType3Type1
                ):
                    cap_add_type_0_item = cap_add_type_0_item_data.value
                else:
                    cap_add_type_0_item = cap_add_type_0_item_data
                cap_add.append(cap_add_type_0_item)

        else:
            cap_add = self.cap_add

        ports: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.ports, Unset):
            ports = []
            for ports_item_data in self.ports:
                ports_item = ports_item_data.to_dict()
                ports.append(ports_item)

        env: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.env, Unset):
            env = []
            for env_item_data in self.env:
                env_item = env_item_data.to_dict()
                env.append(env_item)

        labels: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.labels, Unset):
            labels = []
            for labels_item_data in self.labels:
                labels_item = labels_item_data.to_dict()
                labels.append(labels_item)

        mounts: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.mounts, Unset):
            mounts = []
            for mounts_item_data in self.mounts:
                mounts_item = mounts_item_data.to_dict()
                mounts.append(mounts_item)

        binds: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.binds, Unset):
            binds = []
            for binds_item_data in self.binds:
                binds_item = binds_item_data.to_dict()
                binds.append(binds_item)

        network_settings: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.network_settings, Unset):
            network_settings = []
            for network_settings_item_data in self.network_settings:
                network_settings_item = network_settings_item_data.to_dict()
                network_settings.append(network_settings_item)

        devices: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.devices, Unset):
            devices = []
            for devices_item_data in self.devices:
                devices_item = devices_item_data.to_dict()
                devices.append(devices_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "host": host,
                "image": image,
                "name": name,
            }
        )
        if state is not UNSET:
            field_dict["state"] = state
        if operation is not UNSET:
            field_dict["operation"] = operation
        if status is not UNSET:
            field_dict["status"] = status
        if container_id is not UNSET:
            field_dict["ContainerID"] = container_id
        if hostname is not UNSET:
            field_dict["hostname"] = hostname
        if restart_policy is not UNSET:
            field_dict["restart_policy"] = restart_policy
        if cap_add is not UNSET:
            field_dict["cap_add"] = cap_add
        if ports is not UNSET:
            field_dict["ports"] = ports
        if env is not UNSET:
            field_dict["env"] = env
        if labels is not UNSET:
            field_dict["labels"] = labels
        if mounts is not UNSET:
            field_dict["mounts"] = mounts
        if binds is not UNSET:
            field_dict["binds"] = binds
        if network_settings is not UNSET:
            field_dict["network_settings"] = network_settings
        if devices is not UNSET:
            field_dict["devices"] = devices
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.bind_request import BindRequest
        from ..models.container_request_custom_fields import (
            ContainerRequestCustomFields,
        )
        from ..models.device_request import DeviceRequest
        from ..models.env_request import EnvRequest
        from ..models.label_request import LabelRequest
        from ..models.mount_request import MountRequest
        from ..models.nested_host_request import NestedHostRequest
        from ..models.nested_image_request import NestedImageRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.network_setting_request import NetworkSettingRequest
        from ..models.port_request import PortRequest

        d = src_dict.copy()
        host = NestedHostRequest.from_dict(d.pop("host"))

        image = NestedImageRequest.from_dict(d.pop("image"))

        name = d.pop("name")

        _state = d.pop("state", UNSET)
        state: Union[Unset, ContainerRequestState]
        if isinstance(_state, Unset):
            state = UNSET
        else:
            state = ContainerRequestState(_state)

        _operation = d.pop("operation", UNSET)
        operation: Union[Unset, ContainerRequestOperation]
        if isinstance(_operation, Unset):
            operation = UNSET
        else:
            operation = ContainerRequestOperation(_operation)

        def _parse_status(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        status = _parse_status(d.pop("status", UNSET))

        def _parse_container_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        container_id = _parse_container_id(d.pop("ContainerID", UNSET))

        def _parse_hostname(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        hostname = _parse_hostname(d.pop("hostname", UNSET))

        _restart_policy = d.pop("restart_policy", UNSET)
        restart_policy: Union[Unset, ContainerRequestRestartPolicy]
        if isinstance(_restart_policy, Unset):
            restart_policy = UNSET
        else:
            restart_policy = ContainerRequestRestartPolicy(_restart_policy)

        def _parse_cap_add(
            data: object,
        ) -> Union[
            List[
                Union[
                    ContainerRequestCapAddType0ItemType1,
                    ContainerRequestCapAddType0ItemType2Type1,
                    ContainerRequestCapAddType0ItemType3Type1,
                    None,
                ]
            ],
            None,
            Unset,
        ]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, list):
                    raise TypeError()
                cap_add_type_0 = []
                _cap_add_type_0 = data
                for cap_add_type_0_item_data in _cap_add_type_0:

                    def _parse_cap_add_type_0_item(
                        data: object,
                    ) -> Union[
                        ContainerRequestCapAddType0ItemType1,
                        ContainerRequestCapAddType0ItemType2Type1,
                        ContainerRequestCapAddType0ItemType3Type1,
                        None,
                    ]:
                        if data is None:
                            return data
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_1 = (
                                ContainerRequestCapAddType0ItemType1(data)
                            )

                            return cap_add_type_0_item_type_1
                        except:  # noqa: E722
                            pass
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_2_type_1 = (
                                ContainerRequestCapAddType0ItemType2Type1(data)
                            )

                            return cap_add_type_0_item_type_2_type_1
                        except:  # noqa: E722
                            pass
                        try:
                            if not isinstance(data, str):
                                raise TypeError()
                            cap_add_type_0_item_type_3_type_1 = (
                                ContainerRequestCapAddType0ItemType3Type1(data)
                            )

                            return cap_add_type_0_item_type_3_type_1
                        except:  # noqa: E722
                            pass
                        return cast(
                            Union[
                                ContainerRequestCapAddType0ItemType1,
                                ContainerRequestCapAddType0ItemType2Type1,
                                ContainerRequestCapAddType0ItemType3Type1,
                                None,
                            ],
                            data,
                        )

                    cap_add_type_0_item = _parse_cap_add_type_0_item(
                        cap_add_type_0_item_data
                    )

                    cap_add_type_0.append(cap_add_type_0_item)

                return cap_add_type_0
            except:  # noqa: E722
                pass
            return cast(
                Union[
                    List[
                        Union[
                            ContainerRequestCapAddType0ItemType1,
                            ContainerRequestCapAddType0ItemType2Type1,
                            ContainerRequestCapAddType0ItemType3Type1,
                            None,
                        ]
                    ],
                    None,
                    Unset,
                ],
                data,
            )

        cap_add = _parse_cap_add(d.pop("cap_add", UNSET))

        ports = []
        _ports = d.pop("ports", UNSET)
        for ports_item_data in _ports or []:
            ports_item = PortRequest.from_dict(ports_item_data)

            ports.append(ports_item)

        env = []
        _env = d.pop("env", UNSET)
        for env_item_data in _env or []:
            env_item = EnvRequest.from_dict(env_item_data)

            env.append(env_item)

        labels = []
        _labels = d.pop("labels", UNSET)
        for labels_item_data in _labels or []:
            labels_item = LabelRequest.from_dict(labels_item_data)

            labels.append(labels_item)

        mounts = []
        _mounts = d.pop("mounts", UNSET)
        for mounts_item_data in _mounts or []:
            mounts_item = MountRequest.from_dict(mounts_item_data)

            mounts.append(mounts_item)

        binds = []
        _binds = d.pop("binds", UNSET)
        for binds_item_data in _binds or []:
            binds_item = BindRequest.from_dict(binds_item_data)

            binds.append(binds_item)

        network_settings = []
        _network_settings = d.pop("network_settings", UNSET)
        for network_settings_item_data in _network_settings or []:
            network_settings_item = NetworkSettingRequest.from_dict(
                network_settings_item_data
            )

            network_settings.append(network_settings_item)

        devices = []
        _devices = d.pop("devices", UNSET)
        for devices_item_data in _devices or []:
            devices_item = DeviceRequest.from_dict(devices_item_data)

            devices.append(devices_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ContainerRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ContainerRequestCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        container_request = cls(
            host=host,
            image=image,
            name=name,
            state=state,
            operation=operation,
            status=status,
            container_id=container_id,
            hostname=hostname,
            restart_policy=restart_policy,
            cap_add=cap_add,
            ports=ports,
            env=env,
            labels=labels,
            mounts=mounts,
            binds=binds,
            network_settings=network_settings,
            devices=devices,
            custom_fields=custom_fields,
            tags=tags,
        )

        container_request.additional_properties = d
        return container_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
