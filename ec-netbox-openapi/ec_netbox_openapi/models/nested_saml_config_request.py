from typing import Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="NestedSamlConfigRequest")


@_attrs_define
class NestedSamlConfigRequest:
    """Nested SAML Config Serializer class

    Attributes:
        acs_url (str):
        logout_url (str):
        force_nauth (Union[Unset, bool]):
    """

    acs_url: str
    logout_url: str
    force_nauth: Union[Unset, bool] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        acs_url = self.acs_url

        logout_url = self.logout_url

        force_nauth = self.force_nauth

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "acs_url": acs_url,
                "logout_url": logout_url,
            }
        )
        if force_nauth is not UNSET:
            field_dict["force_nauth"] = force_nauth

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        acs_url = d.pop("acs_url")

        logout_url = d.pop("logout_url")

        force_nauth = d.pop("force_nauth", UNSET)

        nested_saml_config_request = cls(
            acs_url=acs_url,
            logout_url=logout_url,
            force_nauth=force_nauth,
        )

        nested_saml_config_request.additional_properties = d
        return nested_saml_config_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
