from typing import (
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.nested_network_driver_type_1 import NestedNetworkDriverType1
from ..models.nested_network_state import NestedNetworkState
from ..types import UNSET, Unset

T = TypeVar("T", bound="NestedNetwork")


@_attrs_define
class NestedNetwork:
    """Nested Network Serializer class

    Attributes:
        id (int):
        url (str):
        display (str):
        name (str):
        driver (Union[NestedNetworkDriverType1, None, Unset]): * `None` - null
            * `bridge` - Bridge
            * `host` - Host
        network_id (Union[None, Unset, str]):
        state (Union[Unset, NestedNetworkState]): * `creating` - Creating
            * `created` - Created
    """

    id: int
    url: str
    display: str
    name: str
    driver: Union[NestedNetworkDriverType1, None, Unset] = UNSET
    network_id: Union[None, Unset, str] = UNSET
    state: Union[Unset, NestedNetworkState] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        name = self.name

        driver: Union[None, Unset, str]
        if isinstance(self.driver, Unset):
            driver = UNSET
        elif isinstance(self.driver, NestedNetworkDriverType1):
            driver = self.driver.value
        else:
            driver = self.driver

        network_id: Union[None, Unset, str]
        if isinstance(self.network_id, Unset):
            network_id = UNSET
        else:
            network_id = self.network_id

        state: Union[Unset, str] = UNSET
        if not isinstance(self.state, Unset):
            state = self.state.value

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "name": name,
            }
        )
        if driver is not UNSET:
            field_dict["driver"] = driver
        if network_id is not UNSET:
            field_dict["NetworkID"] = network_id
        if state is not UNSET:
            field_dict["state"] = state

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        name = d.pop("name")

        def _parse_driver(data: object) -> Union[NestedNetworkDriverType1, None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                driver_type_1 = NestedNetworkDriverType1(data)

                return driver_type_1
            except:  # noqa: E722
                pass
            return cast(Union[NestedNetworkDriverType1, None, Unset], data)

        driver = _parse_driver(d.pop("driver", UNSET))

        def _parse_network_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        network_id = _parse_network_id(d.pop("NetworkID", UNSET))

        _state = d.pop("state", UNSET)
        state: Union[Unset, NestedNetworkState]
        if isinstance(_state, Unset):
            state = UNSET
        else:
            state = NestedNetworkState(_state)

        nested_network = cls(
            id=id,
            url=url,
            display=display,
            name=name,
            driver=driver,
            network_id=network_id,
            state=state,
        )

        nested_network.additional_properties = d
        return nested_network

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
