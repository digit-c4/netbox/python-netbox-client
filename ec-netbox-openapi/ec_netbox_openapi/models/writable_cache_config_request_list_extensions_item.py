from enum import Enum


class WritableCacheConfigRequestListExtensionsItem(str, Enum):
    CSS = "CSS"
    GIF = "GIF"
    HTML = "HTML"
    JPG = "JPG"
    JS = "JS"
    VALUE_6 = ""
    XML = "XML"

    def __str__(self) -> str:
        return str(self.value)
