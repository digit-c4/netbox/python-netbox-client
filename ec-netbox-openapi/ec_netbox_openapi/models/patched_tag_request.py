import json
from typing import Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedTagRequest")


@_attrs_define
class PatchedTagRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            name (Union[Unset, str]):
            slug (Union[Unset, str]):
            color (Union[Unset, str]):
            description (Union[Unset, str]):
            object_types (Union[Unset, List[str]]):
    """

    name: Union[Unset, str] = UNSET
    slug: Union[Unset, str] = UNSET
    color: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    object_types: Union[Unset, List[str]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        slug = self.slug

        color = self.color

        description = self.description

        object_types: Union[Unset, List[str]] = UNSET
        if not isinstance(self.object_types, Unset):
            object_types = self.object_types

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if slug is not UNSET:
            field_dict["slug"] = slug
        if color is not UNSET:
            field_dict["color"] = color
        if description is not UNSET:
            field_dict["description"] = description
        if object_types is not UNSET:
            field_dict["object_types"] = object_types

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        slug = (
            self.slug
            if isinstance(self.slug, Unset)
            else (None, str(self.slug).encode(), "text/plain")
        )

        color = (
            self.color
            if isinstance(self.color, Unset)
            else (None, str(self.color).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        object_types: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.object_types, Unset):
            _temp_object_types = self.object_types
            object_types = (
                None,
                json.dumps(_temp_object_types).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if slug is not UNSET:
            field_dict["slug"] = slug
        if color is not UNSET:
            field_dict["color"] = color
        if description is not UNSET:
            field_dict["description"] = description
        if object_types is not UNSET:
            field_dict["object_types"] = object_types

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name", UNSET)

        slug = d.pop("slug", UNSET)

        color = d.pop("color", UNSET)

        description = d.pop("description", UNSET)

        object_types = cast(List[str], d.pop("object_types", UNSET))

        patched_tag_request = cls(
            name=name,
            slug=slug,
            color=color,
            description=description,
            object_types=object_types,
        )

        patched_tag_request.additional_properties = d
        return patched_tag_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
