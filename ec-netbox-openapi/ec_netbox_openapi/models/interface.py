import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.component_nested_module import ComponentNestedModule
    from ..models.interface_custom_fields import InterfaceCustomFields
    from ..models.interface_duplex_type_0 import InterfaceDuplexType0
    from ..models.interface_mode import InterfaceMode
    from ..models.interface_poe_mode import InterfacePoeMode
    from ..models.interface_poe_type import InterfacePoeType
    from ..models.interface_rf_channel import InterfaceRfChannel
    from ..models.interface_rf_role import InterfaceRfRole
    from ..models.interface_type import InterfaceType
    from ..models.nested_cable import NestedCable
    from ..models.nested_device import NestedDevice
    from ..models.nested_interface import NestedInterface
    from ..models.nested_l2vpn_termination import NestedL2VPNTermination
    from ..models.nested_tag import NestedTag
    from ..models.nested_vlan import NestedVLAN
    from ..models.nested_vrf import NestedVRF
    from ..models.nested_wireless_link import NestedWirelessLink


T = TypeVar("T", bound="Interface")


@_attrs_define
class Interface:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        device (NestedDevice): Represents an object related through a ForeignKey field. On write, it accepts a primary
            key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (str):
        type (InterfaceType):
        cable (Union['NestedCable', None]):
        cable_end (str):
        wireless_link (Union['NestedWirelessLink', None]):
        link_peers (List[Any]):
        link_peers_type (str): Return the type of the peer link terminations, or None.
        l2vpn_termination (Union['NestedL2VPNTermination', None]):
        connected_endpoints (List[Any]):
        connected_endpoints_type (str):
        connected_endpoints_reachable (bool):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        count_ipaddresses (int):
        count_fhrp_groups (int):
        field_occupied (bool):
        vdcs (Union[Unset, List[int]]):
        module (Union['ComponentNestedModule', None, Unset]):
        label (Union[Unset, str]): Physical label
        enabled (Union[Unset, bool]):
        parent (Union['NestedInterface', None, Unset]):
        bridge (Union['NestedInterface', None, Unset]):
        lag (Union['NestedInterface', None, Unset]):
        mtu (Union[None, Unset, int]):
        mac_address (Union[None, Unset, str]):
        speed (Union[None, Unset, int]):
        duplex (Union['InterfaceDuplexType0', None, Unset]):
        wwn (Union[None, Unset, str]):
        mgmt_only (Union[Unset, bool]): This interface is used only for out-of-band management
        description (Union[Unset, str]):
        mode (Union[Unset, InterfaceMode]):
        rf_role (Union[Unset, InterfaceRfRole]):
        rf_channel (Union[Unset, InterfaceRfChannel]):
        poe_mode (Union[Unset, InterfacePoeMode]):
        poe_type (Union[Unset, InterfacePoeType]):
        rf_channel_frequency (Union[None, Unset, float]): Populated by selected channel (if set)
        rf_channel_width (Union[None, Unset, float]): Populated by selected channel (if set)
        tx_power (Union[None, Unset, int]):
        untagged_vlan (Union['NestedVLAN', None, Unset]):
        tagged_vlans (Union[Unset, List[int]]):
        mark_connected (Union[Unset, bool]): Treat as if a cable is connected
        wireless_lans (Union[Unset, List[int]]):
        vrf (Union['NestedVRF', None, Unset]):
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, InterfaceCustomFields]):
    """

    id: int
    url: str
    display: str
    device: "NestedDevice"
    name: str
    type: "InterfaceType"
    cable: Union["NestedCable", None]
    cable_end: str
    wireless_link: Union["NestedWirelessLink", None]
    link_peers: List[Any]
    link_peers_type: str
    l2vpn_termination: Union["NestedL2VPNTermination", None]
    connected_endpoints: List[Any]
    connected_endpoints_type: str
    connected_endpoints_reachable: bool
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    count_ipaddresses: int
    count_fhrp_groups: int
    field_occupied: bool
    vdcs: Union[Unset, List[int]] = UNSET
    module: Union["ComponentNestedModule", None, Unset] = UNSET
    label: Union[Unset, str] = UNSET
    enabled: Union[Unset, bool] = UNSET
    parent: Union["NestedInterface", None, Unset] = UNSET
    bridge: Union["NestedInterface", None, Unset] = UNSET
    lag: Union["NestedInterface", None, Unset] = UNSET
    mtu: Union[None, Unset, int] = UNSET
    mac_address: Union[None, Unset, str] = UNSET
    speed: Union[None, Unset, int] = UNSET
    duplex: Union["InterfaceDuplexType0", None, Unset] = UNSET
    wwn: Union[None, Unset, str] = UNSET
    mgmt_only: Union[Unset, bool] = UNSET
    description: Union[Unset, str] = UNSET
    mode: Union[Unset, "InterfaceMode"] = UNSET
    rf_role: Union[Unset, "InterfaceRfRole"] = UNSET
    rf_channel: Union[Unset, "InterfaceRfChannel"] = UNSET
    poe_mode: Union[Unset, "InterfacePoeMode"] = UNSET
    poe_type: Union[Unset, "InterfacePoeType"] = UNSET
    rf_channel_frequency: Union[None, Unset, float] = UNSET
    rf_channel_width: Union[None, Unset, float] = UNSET
    tx_power: Union[None, Unset, int] = UNSET
    untagged_vlan: Union["NestedVLAN", None, Unset] = UNSET
    tagged_vlans: Union[Unset, List[int]] = UNSET
    mark_connected: Union[Unset, bool] = UNSET
    wireless_lans: Union[Unset, List[int]] = UNSET
    vrf: Union["NestedVRF", None, Unset] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "InterfaceCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.component_nested_module import ComponentNestedModule
        from ..models.interface_duplex_type_0 import InterfaceDuplexType0
        from ..models.nested_cable import NestedCable
        from ..models.nested_interface import NestedInterface
        from ..models.nested_l2vpn_termination import NestedL2VPNTermination
        from ..models.nested_vlan import NestedVLAN
        from ..models.nested_vrf import NestedVRF
        from ..models.nested_wireless_link import NestedWirelessLink

        id = self.id

        url = self.url

        display = self.display

        device = self.device.to_dict()

        name = self.name

        type = self.type.to_dict()

        cable: Union[Dict[str, Any], None]
        if isinstance(self.cable, NestedCable):
            cable = self.cable.to_dict()
        else:
            cable = self.cable

        cable_end = self.cable_end

        wireless_link: Union[Dict[str, Any], None]
        if isinstance(self.wireless_link, NestedWirelessLink):
            wireless_link = self.wireless_link.to_dict()
        else:
            wireless_link = self.wireless_link

        link_peers = self.link_peers

        link_peers_type = self.link_peers_type

        l2vpn_termination: Union[Dict[str, Any], None]
        if isinstance(self.l2vpn_termination, NestedL2VPNTermination):
            l2vpn_termination = self.l2vpn_termination.to_dict()
        else:
            l2vpn_termination = self.l2vpn_termination

        connected_endpoints = self.connected_endpoints

        connected_endpoints_type = self.connected_endpoints_type

        connected_endpoints_reachable = self.connected_endpoints_reachable

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        count_ipaddresses = self.count_ipaddresses

        count_fhrp_groups = self.count_fhrp_groups

        field_occupied = self.field_occupied

        vdcs: Union[Unset, List[int]] = UNSET
        if not isinstance(self.vdcs, Unset):
            vdcs = self.vdcs

        module: Union[Dict[str, Any], None, Unset]
        if isinstance(self.module, Unset):
            module = UNSET
        elif isinstance(self.module, ComponentNestedModule):
            module = self.module.to_dict()
        else:
            module = self.module

        label = self.label

        enabled = self.enabled

        parent: Union[Dict[str, Any], None, Unset]
        if isinstance(self.parent, Unset):
            parent = UNSET
        elif isinstance(self.parent, NestedInterface):
            parent = self.parent.to_dict()
        else:
            parent = self.parent

        bridge: Union[Dict[str, Any], None, Unset]
        if isinstance(self.bridge, Unset):
            bridge = UNSET
        elif isinstance(self.bridge, NestedInterface):
            bridge = self.bridge.to_dict()
        else:
            bridge = self.bridge

        lag: Union[Dict[str, Any], None, Unset]
        if isinstance(self.lag, Unset):
            lag = UNSET
        elif isinstance(self.lag, NestedInterface):
            lag = self.lag.to_dict()
        else:
            lag = self.lag

        mtu: Union[None, Unset, int]
        if isinstance(self.mtu, Unset):
            mtu = UNSET
        else:
            mtu = self.mtu

        mac_address: Union[None, Unset, str]
        if isinstance(self.mac_address, Unset):
            mac_address = UNSET
        else:
            mac_address = self.mac_address

        speed: Union[None, Unset, int]
        if isinstance(self.speed, Unset):
            speed = UNSET
        else:
            speed = self.speed

        duplex: Union[Dict[str, Any], None, Unset]
        if isinstance(self.duplex, Unset):
            duplex = UNSET
        elif isinstance(self.duplex, InterfaceDuplexType0):
            duplex = self.duplex.to_dict()
        else:
            duplex = self.duplex

        wwn: Union[None, Unset, str]
        if isinstance(self.wwn, Unset):
            wwn = UNSET
        else:
            wwn = self.wwn

        mgmt_only = self.mgmt_only

        description = self.description

        mode: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.mode, Unset):
            mode = self.mode.to_dict()

        rf_role: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.rf_role, Unset):
            rf_role = self.rf_role.to_dict()

        rf_channel: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.rf_channel, Unset):
            rf_channel = self.rf_channel.to_dict()

        poe_mode: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.poe_mode, Unset):
            poe_mode = self.poe_mode.to_dict()

        poe_type: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.poe_type, Unset):
            poe_type = self.poe_type.to_dict()

        rf_channel_frequency: Union[None, Unset, float]
        if isinstance(self.rf_channel_frequency, Unset):
            rf_channel_frequency = UNSET
        else:
            rf_channel_frequency = self.rf_channel_frequency

        rf_channel_width: Union[None, Unset, float]
        if isinstance(self.rf_channel_width, Unset):
            rf_channel_width = UNSET
        else:
            rf_channel_width = self.rf_channel_width

        tx_power: Union[None, Unset, int]
        if isinstance(self.tx_power, Unset):
            tx_power = UNSET
        else:
            tx_power = self.tx_power

        untagged_vlan: Union[Dict[str, Any], None, Unset]
        if isinstance(self.untagged_vlan, Unset):
            untagged_vlan = UNSET
        elif isinstance(self.untagged_vlan, NestedVLAN):
            untagged_vlan = self.untagged_vlan.to_dict()
        else:
            untagged_vlan = self.untagged_vlan

        tagged_vlans: Union[Unset, List[int]] = UNSET
        if not isinstance(self.tagged_vlans, Unset):
            tagged_vlans = self.tagged_vlans

        mark_connected = self.mark_connected

        wireless_lans: Union[Unset, List[int]] = UNSET
        if not isinstance(self.wireless_lans, Unset):
            wireless_lans = self.wireless_lans

        vrf: Union[Dict[str, Any], None, Unset]
        if isinstance(self.vrf, Unset):
            vrf = UNSET
        elif isinstance(self.vrf, NestedVRF):
            vrf = self.vrf.to_dict()
        else:
            vrf = self.vrf

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "device": device,
                "name": name,
                "type": type,
                "cable": cable,
                "cable_end": cable_end,
                "wireless_link": wireless_link,
                "link_peers": link_peers,
                "link_peers_type": link_peers_type,
                "l2vpn_termination": l2vpn_termination,
                "connected_endpoints": connected_endpoints,
                "connected_endpoints_type": connected_endpoints_type,
                "connected_endpoints_reachable": connected_endpoints_reachable,
                "created": created,
                "last_updated": last_updated,
                "count_ipaddresses": count_ipaddresses,
                "count_fhrp_groups": count_fhrp_groups,
                "_occupied": field_occupied,
            }
        )
        if vdcs is not UNSET:
            field_dict["vdcs"] = vdcs
        if module is not UNSET:
            field_dict["module"] = module
        if label is not UNSET:
            field_dict["label"] = label
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if parent is not UNSET:
            field_dict["parent"] = parent
        if bridge is not UNSET:
            field_dict["bridge"] = bridge
        if lag is not UNSET:
            field_dict["lag"] = lag
        if mtu is not UNSET:
            field_dict["mtu"] = mtu
        if mac_address is not UNSET:
            field_dict["mac_address"] = mac_address
        if speed is not UNSET:
            field_dict["speed"] = speed
        if duplex is not UNSET:
            field_dict["duplex"] = duplex
        if wwn is not UNSET:
            field_dict["wwn"] = wwn
        if mgmt_only is not UNSET:
            field_dict["mgmt_only"] = mgmt_only
        if description is not UNSET:
            field_dict["description"] = description
        if mode is not UNSET:
            field_dict["mode"] = mode
        if rf_role is not UNSET:
            field_dict["rf_role"] = rf_role
        if rf_channel is not UNSET:
            field_dict["rf_channel"] = rf_channel
        if poe_mode is not UNSET:
            field_dict["poe_mode"] = poe_mode
        if poe_type is not UNSET:
            field_dict["poe_type"] = poe_type
        if rf_channel_frequency is not UNSET:
            field_dict["rf_channel_frequency"] = rf_channel_frequency
        if rf_channel_width is not UNSET:
            field_dict["rf_channel_width"] = rf_channel_width
        if tx_power is not UNSET:
            field_dict["tx_power"] = tx_power
        if untagged_vlan is not UNSET:
            field_dict["untagged_vlan"] = untagged_vlan
        if tagged_vlans is not UNSET:
            field_dict["tagged_vlans"] = tagged_vlans
        if mark_connected is not UNSET:
            field_dict["mark_connected"] = mark_connected
        if wireless_lans is not UNSET:
            field_dict["wireless_lans"] = wireless_lans
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.component_nested_module import ComponentNestedModule
        from ..models.interface_custom_fields import InterfaceCustomFields
        from ..models.interface_duplex_type_0 import InterfaceDuplexType0
        from ..models.interface_mode import InterfaceMode
        from ..models.interface_poe_mode import InterfacePoeMode
        from ..models.interface_poe_type import InterfacePoeType
        from ..models.interface_rf_channel import InterfaceRfChannel
        from ..models.interface_rf_role import InterfaceRfRole
        from ..models.interface_type import InterfaceType
        from ..models.nested_cable import NestedCable
        from ..models.nested_device import NestedDevice
        from ..models.nested_interface import NestedInterface
        from ..models.nested_l2vpn_termination import NestedL2VPNTermination
        from ..models.nested_tag import NestedTag
        from ..models.nested_vlan import NestedVLAN
        from ..models.nested_vrf import NestedVRF
        from ..models.nested_wireless_link import NestedWirelessLink

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        device = NestedDevice.from_dict(d.pop("device"))

        name = d.pop("name")

        type = InterfaceType.from_dict(d.pop("type"))

        def _parse_cable(data: object) -> Union["NestedCable", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                cable_type_1 = NestedCable.from_dict(data)

                return cable_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedCable", None], data)

        cable = _parse_cable(d.pop("cable"))

        cable_end = d.pop("cable_end")

        def _parse_wireless_link(data: object) -> Union["NestedWirelessLink", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                wireless_link_type_1 = NestedWirelessLink.from_dict(data)

                return wireless_link_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedWirelessLink", None], data)

        wireless_link = _parse_wireless_link(d.pop("wireless_link"))

        link_peers = cast(List[Any], d.pop("link_peers"))

        link_peers_type = d.pop("link_peers_type")

        def _parse_l2vpn_termination(
            data: object,
        ) -> Union["NestedL2VPNTermination", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                l2vpn_termination_type_1 = NestedL2VPNTermination.from_dict(data)

                return l2vpn_termination_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedL2VPNTermination", None], data)

        l2vpn_termination = _parse_l2vpn_termination(d.pop("l2vpn_termination"))

        connected_endpoints = cast(List[Any], d.pop("connected_endpoints"))

        connected_endpoints_type = d.pop("connected_endpoints_type")

        connected_endpoints_reachable = d.pop("connected_endpoints_reachable")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        count_ipaddresses = d.pop("count_ipaddresses")

        count_fhrp_groups = d.pop("count_fhrp_groups")

        field_occupied = d.pop("_occupied")

        vdcs = cast(List[int], d.pop("vdcs", UNSET))

        def _parse_module(data: object) -> Union["ComponentNestedModule", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                module_type_1 = ComponentNestedModule.from_dict(data)

                return module_type_1
            except:  # noqa: E722
                pass
            return cast(Union["ComponentNestedModule", None, Unset], data)

        module = _parse_module(d.pop("module", UNSET))

        label = d.pop("label", UNSET)

        enabled = d.pop("enabled", UNSET)

        def _parse_parent(data: object) -> Union["NestedInterface", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                parent_type_1 = NestedInterface.from_dict(data)

                return parent_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedInterface", None, Unset], data)

        parent = _parse_parent(d.pop("parent", UNSET))

        def _parse_bridge(data: object) -> Union["NestedInterface", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                bridge_type_1 = NestedInterface.from_dict(data)

                return bridge_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedInterface", None, Unset], data)

        bridge = _parse_bridge(d.pop("bridge", UNSET))

        def _parse_lag(data: object) -> Union["NestedInterface", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                lag_type_1 = NestedInterface.from_dict(data)

                return lag_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedInterface", None, Unset], data)

        lag = _parse_lag(d.pop("lag", UNSET))

        def _parse_mtu(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        mtu = _parse_mtu(d.pop("mtu", UNSET))

        def _parse_mac_address(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        mac_address = _parse_mac_address(d.pop("mac_address", UNSET))

        def _parse_speed(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        speed = _parse_speed(d.pop("speed", UNSET))

        def _parse_duplex(data: object) -> Union["InterfaceDuplexType0", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                duplex_type_0 = InterfaceDuplexType0.from_dict(data)

                return duplex_type_0
            except:  # noqa: E722
                pass
            return cast(Union["InterfaceDuplexType0", None, Unset], data)

        duplex = _parse_duplex(d.pop("duplex", UNSET))

        def _parse_wwn(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        wwn = _parse_wwn(d.pop("wwn", UNSET))

        mgmt_only = d.pop("mgmt_only", UNSET)

        description = d.pop("description", UNSET)

        _mode = d.pop("mode", UNSET)
        mode: Union[Unset, InterfaceMode]
        if isinstance(_mode, Unset):
            mode = UNSET
        else:
            mode = InterfaceMode.from_dict(_mode)

        _rf_role = d.pop("rf_role", UNSET)
        rf_role: Union[Unset, InterfaceRfRole]
        if isinstance(_rf_role, Unset):
            rf_role = UNSET
        else:
            rf_role = InterfaceRfRole.from_dict(_rf_role)

        _rf_channel = d.pop("rf_channel", UNSET)
        rf_channel: Union[Unset, InterfaceRfChannel]
        if isinstance(_rf_channel, Unset):
            rf_channel = UNSET
        else:
            rf_channel = InterfaceRfChannel.from_dict(_rf_channel)

        _poe_mode = d.pop("poe_mode", UNSET)
        poe_mode: Union[Unset, InterfacePoeMode]
        if isinstance(_poe_mode, Unset):
            poe_mode = UNSET
        else:
            poe_mode = InterfacePoeMode.from_dict(_poe_mode)

        _poe_type = d.pop("poe_type", UNSET)
        poe_type: Union[Unset, InterfacePoeType]
        if isinstance(_poe_type, Unset):
            poe_type = UNSET
        else:
            poe_type = InterfacePoeType.from_dict(_poe_type)

        def _parse_rf_channel_frequency(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        rf_channel_frequency = _parse_rf_channel_frequency(
            d.pop("rf_channel_frequency", UNSET)
        )

        def _parse_rf_channel_width(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        rf_channel_width = _parse_rf_channel_width(d.pop("rf_channel_width", UNSET))

        def _parse_tx_power(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tx_power = _parse_tx_power(d.pop("tx_power", UNSET))

        def _parse_untagged_vlan(data: object) -> Union["NestedVLAN", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                untagged_vlan_type_1 = NestedVLAN.from_dict(data)

                return untagged_vlan_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVLAN", None, Unset], data)

        untagged_vlan = _parse_untagged_vlan(d.pop("untagged_vlan", UNSET))

        tagged_vlans = cast(List[int], d.pop("tagged_vlans", UNSET))

        mark_connected = d.pop("mark_connected", UNSET)

        wireless_lans = cast(List[int], d.pop("wireless_lans", UNSET))

        def _parse_vrf(data: object) -> Union["NestedVRF", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                vrf_type_1 = NestedVRF.from_dict(data)

                return vrf_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVRF", None, Unset], data)

        vrf = _parse_vrf(d.pop("vrf", UNSET))

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, InterfaceCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = InterfaceCustomFields.from_dict(_custom_fields)

        interface = cls(
            id=id,
            url=url,
            display=display,
            device=device,
            name=name,
            type=type,
            cable=cable,
            cable_end=cable_end,
            wireless_link=wireless_link,
            link_peers=link_peers,
            link_peers_type=link_peers_type,
            l2vpn_termination=l2vpn_termination,
            connected_endpoints=connected_endpoints,
            connected_endpoints_type=connected_endpoints_type,
            connected_endpoints_reachable=connected_endpoints_reachable,
            created=created,
            last_updated=last_updated,
            count_ipaddresses=count_ipaddresses,
            count_fhrp_groups=count_fhrp_groups,
            field_occupied=field_occupied,
            vdcs=vdcs,
            module=module,
            label=label,
            enabled=enabled,
            parent=parent,
            bridge=bridge,
            lag=lag,
            mtu=mtu,
            mac_address=mac_address,
            speed=speed,
            duplex=duplex,
            wwn=wwn,
            mgmt_only=mgmt_only,
            description=description,
            mode=mode,
            rf_role=rf_role,
            rf_channel=rf_channel,
            poe_mode=poe_mode,
            poe_type=poe_type,
            rf_channel_frequency=rf_channel_frequency,
            rf_channel_width=rf_channel_width,
            tx_power=tx_power,
            untagged_vlan=untagged_vlan,
            tagged_vlans=tagged_vlans,
            mark_connected=mark_connected,
            wireless_lans=wireless_lans,
            vrf=vrf,
            tags=tags,
            custom_fields=custom_fields,
        )

        interface.additional_properties = d
        return interface

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
