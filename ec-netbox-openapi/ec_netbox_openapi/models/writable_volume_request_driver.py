from enum import Enum


class WritableVolumeRequestDriver(str, Enum):
    LOCAL = "local"

    def __str__(self) -> str:
        return str(self.value)
