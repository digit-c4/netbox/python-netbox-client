import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_circuit_termination_request_termination import (
    WritableCircuitTerminationRequestTermination,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_circuit_termination_request_custom_fields import (
        WritableCircuitTerminationRequestCustomFields,
    )


T = TypeVar("T", bound="WritableCircuitTerminationRequest")


@_attrs_define
class WritableCircuitTerminationRequest:
    """Adds support for custom fields and tags.

    Attributes:
        circuit (int):
        term_side (WritableCircuitTerminationRequestTermination): * `A` - A
            * `Z` - Z
        site (Union[None, Unset, int]):
        provider_network (Union[None, Unset, int]):
        port_speed (Union[None, Unset, int]): Physical circuit speed
        upstream_speed (Union[None, Unset, int]): Upstream speed, if different from port speed
        xconnect_id (Union[Unset, str]): ID of the local cross-connect
        pp_info (Union[Unset, str]): Patch panel ID and port number(s)
        description (Union[Unset, str]):
        mark_connected (Union[Unset, bool]): Treat as if a cable is connected
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritableCircuitTerminationRequestCustomFields]):
    """

    circuit: int
    term_side: WritableCircuitTerminationRequestTermination
    site: Union[None, Unset, int] = UNSET
    provider_network: Union[None, Unset, int] = UNSET
    port_speed: Union[None, Unset, int] = UNSET
    upstream_speed: Union[None, Unset, int] = UNSET
    xconnect_id: Union[Unset, str] = UNSET
    pp_info: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    mark_connected: Union[Unset, bool] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WritableCircuitTerminationRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        circuit = self.circuit

        term_side = self.term_side.value

        site: Union[None, Unset, int]
        if isinstance(self.site, Unset):
            site = UNSET
        else:
            site = self.site

        provider_network: Union[None, Unset, int]
        if isinstance(self.provider_network, Unset):
            provider_network = UNSET
        else:
            provider_network = self.provider_network

        port_speed: Union[None, Unset, int]
        if isinstance(self.port_speed, Unset):
            port_speed = UNSET
        else:
            port_speed = self.port_speed

        upstream_speed: Union[None, Unset, int]
        if isinstance(self.upstream_speed, Unset):
            upstream_speed = UNSET
        else:
            upstream_speed = self.upstream_speed

        xconnect_id = self.xconnect_id

        pp_info = self.pp_info

        description = self.description

        mark_connected = self.mark_connected

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "circuit": circuit,
                "term_side": term_side,
            }
        )
        if site is not UNSET:
            field_dict["site"] = site
        if provider_network is not UNSET:
            field_dict["provider_network"] = provider_network
        if port_speed is not UNSET:
            field_dict["port_speed"] = port_speed
        if upstream_speed is not UNSET:
            field_dict["upstream_speed"] = upstream_speed
        if xconnect_id is not UNSET:
            field_dict["xconnect_id"] = xconnect_id
        if pp_info is not UNSET:
            field_dict["pp_info"] = pp_info
        if description is not UNSET:
            field_dict["description"] = description
        if mark_connected is not UNSET:
            field_dict["mark_connected"] = mark_connected
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        circuit = (None, str(self.circuit).encode(), "text/plain")

        term_side = (None, str(self.term_side.value).encode(), "text/plain")

        site: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.site, Unset):
            site = UNSET
        elif isinstance(self.site, int):
            site = (None, str(self.site).encode(), "text/plain")
        else:
            site = (None, str(self.site).encode(), "text/plain")

        provider_network: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.provider_network, Unset):
            provider_network = UNSET
        elif isinstance(self.provider_network, int):
            provider_network = (None, str(self.provider_network).encode(), "text/plain")
        else:
            provider_network = (None, str(self.provider_network).encode(), "text/plain")

        port_speed: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.port_speed, Unset):
            port_speed = UNSET
        elif isinstance(self.port_speed, int):
            port_speed = (None, str(self.port_speed).encode(), "text/plain")
        else:
            port_speed = (None, str(self.port_speed).encode(), "text/plain")

        upstream_speed: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.upstream_speed, Unset):
            upstream_speed = UNSET
        elif isinstance(self.upstream_speed, int):
            upstream_speed = (None, str(self.upstream_speed).encode(), "text/plain")
        else:
            upstream_speed = (None, str(self.upstream_speed).encode(), "text/plain")

        xconnect_id = (
            self.xconnect_id
            if isinstance(self.xconnect_id, Unset)
            else (None, str(self.xconnect_id).encode(), "text/plain")
        )

        pp_info = (
            self.pp_info
            if isinstance(self.pp_info, Unset)
            else (None, str(self.pp_info).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        mark_connected = (
            self.mark_connected
            if isinstance(self.mark_connected, Unset)
            else (None, str(self.mark_connected).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "circuit": circuit,
                "term_side": term_side,
            }
        )
        if site is not UNSET:
            field_dict["site"] = site
        if provider_network is not UNSET:
            field_dict["provider_network"] = provider_network
        if port_speed is not UNSET:
            field_dict["port_speed"] = port_speed
        if upstream_speed is not UNSET:
            field_dict["upstream_speed"] = upstream_speed
        if xconnect_id is not UNSET:
            field_dict["xconnect_id"] = xconnect_id
        if pp_info is not UNSET:
            field_dict["pp_info"] = pp_info
        if description is not UNSET:
            field_dict["description"] = description
        if mark_connected is not UNSET:
            field_dict["mark_connected"] = mark_connected
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_circuit_termination_request_custom_fields import (
            WritableCircuitTerminationRequestCustomFields,
        )

        d = src_dict.copy()
        circuit = d.pop("circuit")

        term_side = WritableCircuitTerminationRequestTermination(d.pop("term_side"))

        def _parse_site(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        site = _parse_site(d.pop("site", UNSET))

        def _parse_provider_network(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        provider_network = _parse_provider_network(d.pop("provider_network", UNSET))

        def _parse_port_speed(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        port_speed = _parse_port_speed(d.pop("port_speed", UNSET))

        def _parse_upstream_speed(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        upstream_speed = _parse_upstream_speed(d.pop("upstream_speed", UNSET))

        xconnect_id = d.pop("xconnect_id", UNSET)

        pp_info = d.pop("pp_info", UNSET)

        description = d.pop("description", UNSET)

        mark_connected = d.pop("mark_connected", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableCircuitTerminationRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableCircuitTerminationRequestCustomFields.from_dict(
                _custom_fields
            )

        writable_circuit_termination_request = cls(
            circuit=circuit,
            term_side=term_side,
            site=site,
            provider_network=provider_network,
            port_speed=port_speed,
            upstream_speed=upstream_speed,
            xconnect_id=xconnect_id,
            pp_info=pp_info,
            description=description,
            mark_connected=mark_connected,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_circuit_termination_request.additional_properties = d
        return writable_circuit_termination_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
