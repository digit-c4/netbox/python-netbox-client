from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_site_group_request import NestedSiteGroupRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.site_group_request_custom_fields import SiteGroupRequestCustomFields


T = TypeVar("T", bound="SiteGroupRequest")


@_attrs_define
class SiteGroupRequest:
    """Extends PrimaryModelSerializer to include MPTT support.

    Attributes:
        name (str):
        slug (str):
        parent (Union['NestedSiteGroupRequest', None, Unset]):
        description (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, SiteGroupRequestCustomFields]):
    """

    name: str
    slug: str
    parent: Union["NestedSiteGroupRequest", None, Unset] = UNSET
    description: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "SiteGroupRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_site_group_request import NestedSiteGroupRequest

        name = self.name

        slug = self.slug

        parent: Union[Dict[str, Any], None, Unset]
        if isinstance(self.parent, Unset):
            parent = UNSET
        elif isinstance(self.parent, NestedSiteGroupRequest):
            parent = self.parent.to_dict()
        else:
            parent = self.parent

        description = self.description

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "slug": slug,
            }
        )
        if parent is not UNSET:
            field_dict["parent"] = parent
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_site_group_request import NestedSiteGroupRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.site_group_request_custom_fields import (
            SiteGroupRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name")

        slug = d.pop("slug")

        def _parse_parent(data: object) -> Union["NestedSiteGroupRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                parent_type_1 = NestedSiteGroupRequest.from_dict(data)

                return parent_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedSiteGroupRequest", None, Unset], data)

        parent = _parse_parent(d.pop("parent", UNSET))

        description = d.pop("description", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, SiteGroupRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = SiteGroupRequestCustomFields.from_dict(_custom_fields)

        site_group_request = cls(
            name=name,
            slug=slug,
            parent=parent,
            description=description,
            tags=tags,
            custom_fields=custom_fields,
        )

        site_group_request.additional_properties = d
        return site_group_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
