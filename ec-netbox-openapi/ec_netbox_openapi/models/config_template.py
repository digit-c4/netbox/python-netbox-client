import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_data_file import NestedDataFile
    from ..models.nested_data_source import NestedDataSource
    from ..models.nested_tag import NestedTag


T = TypeVar("T", bound="ConfigTemplate")


@_attrs_define
class ConfigTemplate:
    """Introduces support for Tag assignment. Adds `tags` serialization, and handles tag assignment
    on create() and update().

        Attributes:
            id (int):
            url (str):
            display (str):
            name (str):
            template_code (str): Jinja2 template code.
            data_path (str): Path to remote file (relative to data source root)
            data_synced (Union[None, datetime.datetime]):
            created (Union[None, datetime.datetime]):
            last_updated (Union[None, datetime.datetime]):
            description (Union[Unset, str]):
            environment_params (Union[Unset, Any]): Any <a
                href="https://jinja.palletsprojects.com/en/3.1.x/api/#jinja2.Environment">additional parameters</a> to pass when
                constructing the Jinja2 environment.
            data_source (Union[Unset, NestedDataSource]): Represents an object related through a ForeignKey field. On write,
                it accepts a primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            data_file (Union[Unset, NestedDataFile]): Represents an object related through a ForeignKey field. On write, it
                accepts a primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            tags (Union[Unset, List['NestedTag']]):
    """

    id: int
    url: str
    display: str
    name: str
    template_code: str
    data_path: str
    data_synced: Union[None, datetime.datetime]
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    description: Union[Unset, str] = UNSET
    environment_params: Union[Unset, Any] = UNSET
    data_source: Union[Unset, "NestedDataSource"] = UNSET
    data_file: Union[Unset, "NestedDataFile"] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        name = self.name

        template_code = self.template_code

        data_path = self.data_path

        data_synced: Union[None, str]
        if isinstance(self.data_synced, datetime.datetime):
            data_synced = self.data_synced.isoformat()
        else:
            data_synced = self.data_synced

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        description = self.description

        environment_params = self.environment_params

        data_source: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.data_source, Unset):
            data_source = self.data_source.to_dict()

        data_file: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.data_file, Unset):
            data_file = self.data_file.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "name": name,
                "template_code": template_code,
                "data_path": data_path,
                "data_synced": data_synced,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if description is not UNSET:
            field_dict["description"] = description
        if environment_params is not UNSET:
            field_dict["environment_params"] = environment_params
        if data_source is not UNSET:
            field_dict["data_source"] = data_source
        if data_file is not UNSET:
            field_dict["data_file"] = data_file
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_data_file import NestedDataFile
        from ..models.nested_data_source import NestedDataSource
        from ..models.nested_tag import NestedTag

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        name = d.pop("name")

        template_code = d.pop("template_code")

        data_path = d.pop("data_path")

        def _parse_data_synced(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                data_synced_type_0 = isoparse(data)

                return data_synced_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        data_synced = _parse_data_synced(d.pop("data_synced"))

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        description = d.pop("description", UNSET)

        environment_params = d.pop("environment_params", UNSET)

        _data_source = d.pop("data_source", UNSET)
        data_source: Union[Unset, NestedDataSource]
        if isinstance(_data_source, Unset):
            data_source = UNSET
        else:
            data_source = NestedDataSource.from_dict(_data_source)

        _data_file = d.pop("data_file", UNSET)
        data_file: Union[Unset, NestedDataFile]
        if isinstance(_data_file, Unset):
            data_file = UNSET
        else:
            data_file = NestedDataFile.from_dict(_data_file)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        config_template = cls(
            id=id,
            url=url,
            display=display,
            name=name,
            template_code=template_code,
            data_path=data_path,
            data_synced=data_synced,
            created=created,
            last_updated=last_updated,
            description=description,
            environment_params=environment_params,
            data_source=data_source,
            data_file=data_file,
            tags=tags,
        )

        config_template.additional_properties = d
        return config_template

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
