from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.vm_interface_request_mode import VMInterfaceRequestMode
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_virtual_machine_request import NestedVirtualMachineRequest
    from ..models.nested_vlan_request import NestedVLANRequest
    from ..models.nested_vm_interface_request import NestedVMInterfaceRequest
    from ..models.nested_vrf_request import NestedVRFRequest
    from ..models.vm_interface_request_custom_fields import (
        VMInterfaceRequestCustomFields,
    )


T = TypeVar("T", bound="VMInterfaceRequest")


@_attrs_define
class VMInterfaceRequest:
    """Adds support for custom fields and tags.

    Attributes:
        virtual_machine (NestedVirtualMachineRequest): Represents an object related through a ForeignKey field. On
            write, it accepts a primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (str):
        enabled (Union[Unset, bool]):
        parent (Union['NestedVMInterfaceRequest', None, Unset]):
        bridge (Union['NestedVMInterfaceRequest', None, Unset]):
        mtu (Union[None, Unset, int]):
        mac_address (Union[None, Unset, str]):
        description (Union[Unset, str]):
        mode (Union[Unset, VMInterfaceRequestMode]): * `access` - Access
            * `tagged` - Tagged
            * `tagged-all` - Tagged (All)
        untagged_vlan (Union['NestedVLANRequest', None, Unset]):
        tagged_vlans (Union[Unset, List[int]]):
        vrf (Union['NestedVRFRequest', None, Unset]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, VMInterfaceRequestCustomFields]):
    """

    virtual_machine: "NestedVirtualMachineRequest"
    name: str
    enabled: Union[Unset, bool] = UNSET
    parent: Union["NestedVMInterfaceRequest", None, Unset] = UNSET
    bridge: Union["NestedVMInterfaceRequest", None, Unset] = UNSET
    mtu: Union[None, Unset, int] = UNSET
    mac_address: Union[None, Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    mode: Union[Unset, VMInterfaceRequestMode] = UNSET
    untagged_vlan: Union["NestedVLANRequest", None, Unset] = UNSET
    tagged_vlans: Union[Unset, List[int]] = UNSET
    vrf: Union["NestedVRFRequest", None, Unset] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "VMInterfaceRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_vlan_request import NestedVLANRequest
        from ..models.nested_vm_interface_request import NestedVMInterfaceRequest
        from ..models.nested_vrf_request import NestedVRFRequest

        virtual_machine = self.virtual_machine.to_dict()

        name = self.name

        enabled = self.enabled

        parent: Union[Dict[str, Any], None, Unset]
        if isinstance(self.parent, Unset):
            parent = UNSET
        elif isinstance(self.parent, NestedVMInterfaceRequest):
            parent = self.parent.to_dict()
        else:
            parent = self.parent

        bridge: Union[Dict[str, Any], None, Unset]
        if isinstance(self.bridge, Unset):
            bridge = UNSET
        elif isinstance(self.bridge, NestedVMInterfaceRequest):
            bridge = self.bridge.to_dict()
        else:
            bridge = self.bridge

        mtu: Union[None, Unset, int]
        if isinstance(self.mtu, Unset):
            mtu = UNSET
        else:
            mtu = self.mtu

        mac_address: Union[None, Unset, str]
        if isinstance(self.mac_address, Unset):
            mac_address = UNSET
        else:
            mac_address = self.mac_address

        description = self.description

        mode: Union[Unset, str] = UNSET
        if not isinstance(self.mode, Unset):
            mode = self.mode.value

        untagged_vlan: Union[Dict[str, Any], None, Unset]
        if isinstance(self.untagged_vlan, Unset):
            untagged_vlan = UNSET
        elif isinstance(self.untagged_vlan, NestedVLANRequest):
            untagged_vlan = self.untagged_vlan.to_dict()
        else:
            untagged_vlan = self.untagged_vlan

        tagged_vlans: Union[Unset, List[int]] = UNSET
        if not isinstance(self.tagged_vlans, Unset):
            tagged_vlans = self.tagged_vlans

        vrf: Union[Dict[str, Any], None, Unset]
        if isinstance(self.vrf, Unset):
            vrf = UNSET
        elif isinstance(self.vrf, NestedVRFRequest):
            vrf = self.vrf.to_dict()
        else:
            vrf = self.vrf

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "virtual_machine": virtual_machine,
                "name": name,
            }
        )
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if parent is not UNSET:
            field_dict["parent"] = parent
        if bridge is not UNSET:
            field_dict["bridge"] = bridge
        if mtu is not UNSET:
            field_dict["mtu"] = mtu
        if mac_address is not UNSET:
            field_dict["mac_address"] = mac_address
        if description is not UNSET:
            field_dict["description"] = description
        if mode is not UNSET:
            field_dict["mode"] = mode
        if untagged_vlan is not UNSET:
            field_dict["untagged_vlan"] = untagged_vlan
        if tagged_vlans is not UNSET:
            field_dict["tagged_vlans"] = tagged_vlans
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_virtual_machine_request import NestedVirtualMachineRequest
        from ..models.nested_vlan_request import NestedVLANRequest
        from ..models.nested_vm_interface_request import NestedVMInterfaceRequest
        from ..models.nested_vrf_request import NestedVRFRequest
        from ..models.vm_interface_request_custom_fields import (
            VMInterfaceRequestCustomFields,
        )

        d = src_dict.copy()
        virtual_machine = NestedVirtualMachineRequest.from_dict(
            d.pop("virtual_machine")
        )

        name = d.pop("name")

        enabled = d.pop("enabled", UNSET)

        def _parse_parent(
            data: object,
        ) -> Union["NestedVMInterfaceRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                parent_type_1 = NestedVMInterfaceRequest.from_dict(data)

                return parent_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVMInterfaceRequest", None, Unset], data)

        parent = _parse_parent(d.pop("parent", UNSET))

        def _parse_bridge(
            data: object,
        ) -> Union["NestedVMInterfaceRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                bridge_type_1 = NestedVMInterfaceRequest.from_dict(data)

                return bridge_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVMInterfaceRequest", None, Unset], data)

        bridge = _parse_bridge(d.pop("bridge", UNSET))

        def _parse_mtu(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        mtu = _parse_mtu(d.pop("mtu", UNSET))

        def _parse_mac_address(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        mac_address = _parse_mac_address(d.pop("mac_address", UNSET))

        description = d.pop("description", UNSET)

        _mode = d.pop("mode", UNSET)
        mode: Union[Unset, VMInterfaceRequestMode]
        if isinstance(_mode, Unset):
            mode = UNSET
        else:
            mode = VMInterfaceRequestMode(_mode)

        def _parse_untagged_vlan(
            data: object,
        ) -> Union["NestedVLANRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                untagged_vlan_type_1 = NestedVLANRequest.from_dict(data)

                return untagged_vlan_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVLANRequest", None, Unset], data)

        untagged_vlan = _parse_untagged_vlan(d.pop("untagged_vlan", UNSET))

        tagged_vlans = cast(List[int], d.pop("tagged_vlans", UNSET))

        def _parse_vrf(data: object) -> Union["NestedVRFRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                vrf_type_1 = NestedVRFRequest.from_dict(data)

                return vrf_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVRFRequest", None, Unset], data)

        vrf = _parse_vrf(d.pop("vrf", UNSET))

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, VMInterfaceRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = VMInterfaceRequestCustomFields.from_dict(_custom_fields)

        vm_interface_request = cls(
            virtual_machine=virtual_machine,
            name=name,
            enabled=enabled,
            parent=parent,
            bridge=bridge,
            mtu=mtu,
            mac_address=mac_address,
            description=description,
            mode=mode,
            untagged_vlan=untagged_vlan,
            tagged_vlans=tagged_vlans,
            vrf=vrf,
            tags=tags,
            custom_fields=custom_fields,
        )

        vm_interface_request.additional_properties = d
        return vm_interface_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
