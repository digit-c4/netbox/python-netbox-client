import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_http_header_request_apply_to import (
    PatchedWritableHttpHeaderRequestApplyTo,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_writable_http_header_request_custom_fields import (
        PatchedWritableHttpHeaderRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedWritableHttpHeaderRequest")


@_attrs_define
class PatchedWritableHttpHeaderRequest:
    """HTTP Header Serializer class

    Attributes:
        name (Union[Unset, str]):
        value (Union[None, Unset, str]):
        apply_to (Union[Unset, PatchedWritableHttpHeaderRequestApplyTo]): * `request` - Request
            * `response` - Response Default: PatchedWritableHttpHeaderRequestApplyTo.REQUEST.
        mapping (Union[Unset, int]):
        custom_fields (Union[Unset, PatchedWritableHttpHeaderRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    name: Union[Unset, str] = UNSET
    value: Union[None, Unset, str] = UNSET
    apply_to: Union[Unset, PatchedWritableHttpHeaderRequestApplyTo] = (
        PatchedWritableHttpHeaderRequestApplyTo.REQUEST
    )
    mapping: Union[Unset, int] = UNSET
    custom_fields: Union[Unset, "PatchedWritableHttpHeaderRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        value: Union[None, Unset, str]
        if isinstance(self.value, Unset):
            value = UNSET
        else:
            value = self.value

        apply_to: Union[Unset, str] = UNSET
        if not isinstance(self.apply_to, Unset):
            apply_to = self.apply_to.value

        mapping = self.mapping

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if value is not UNSET:
            field_dict["value"] = value
        if apply_to is not UNSET:
            field_dict["apply_to"] = apply_to
        if mapping is not UNSET:
            field_dict["mapping"] = mapping
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        value: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.value, Unset):
            value = UNSET
        elif isinstance(self.value, str):
            value = (None, str(self.value).encode(), "text/plain")
        else:
            value = (None, str(self.value).encode(), "text/plain")

        apply_to: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.apply_to, Unset):
            apply_to = (None, str(self.apply_to.value).encode(), "text/plain")

        mapping = (
            self.mapping
            if isinstance(self.mapping, Unset)
            else (None, str(self.mapping).encode(), "text/plain")
        )

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if value is not UNSET:
            field_dict["value"] = value
        if apply_to is not UNSET:
            field_dict["apply_to"] = apply_to
        if mapping is not UNSET:
            field_dict["mapping"] = mapping
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_writable_http_header_request_custom_fields import (
            PatchedWritableHttpHeaderRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name", UNSET)

        def _parse_value(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        value = _parse_value(d.pop("value", UNSET))

        _apply_to = d.pop("apply_to", UNSET)
        apply_to: Union[Unset, PatchedWritableHttpHeaderRequestApplyTo]
        if isinstance(_apply_to, Unset):
            apply_to = UNSET
        else:
            apply_to = PatchedWritableHttpHeaderRequestApplyTo(_apply_to)

        mapping = d.pop("mapping", UNSET)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedWritableHttpHeaderRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedWritableHttpHeaderRequestCustomFields.from_dict(
                _custom_fields
            )

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        patched_writable_http_header_request = cls(
            name=name,
            value=value,
            apply_to=apply_to,
            mapping=mapping,
            custom_fields=custom_fields,
            tags=tags,
        )

        patched_writable_http_header_request.additional_properties = d
        return patched_writable_http_header_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
