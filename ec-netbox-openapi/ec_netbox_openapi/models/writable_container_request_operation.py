from enum import Enum


class WritableContainerRequestOperation(str, Enum):
    CREATE = "create"
    KILL = "kill"
    NONE = "none"
    RECREATE = "recreate"
    RESTART = "restart"
    START = "start"
    STOP = "stop"

    def __str__(self) -> str:
        return str(self.value)
