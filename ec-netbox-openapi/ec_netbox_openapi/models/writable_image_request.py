import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_image_request_custom_fields import (
        WritableImageRequestCustomFields,
    )


T = TypeVar("T", bound="WritableImageRequest")


@_attrs_define
class WritableImageRequest:
    """Image Serializer class

    Attributes:
        host (int):
        name (str):
        registry (int):
        version (Union[Unset, str]):
        size (Union[Unset, int]):
        image_id (Union[None, Unset, str]):
        digest (Union[None, Unset, str]):
        custom_fields (Union[Unset, WritableImageRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    host: int
    name: str
    registry: int
    version: Union[Unset, str] = UNSET
    size: Union[Unset, int] = UNSET
    image_id: Union[None, Unset, str] = UNSET
    digest: Union[None, Unset, str] = UNSET
    custom_fields: Union[Unset, "WritableImageRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        host = self.host

        name = self.name

        registry = self.registry

        version = self.version

        size = self.size

        image_id: Union[None, Unset, str]
        if isinstance(self.image_id, Unset):
            image_id = UNSET
        else:
            image_id = self.image_id

        digest: Union[None, Unset, str]
        if isinstance(self.digest, Unset):
            digest = UNSET
        else:
            digest = self.digest

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "host": host,
                "name": name,
                "registry": registry,
            }
        )
        if version is not UNSET:
            field_dict["version"] = version
        if size is not UNSET:
            field_dict["size"] = size
        if image_id is not UNSET:
            field_dict["ImageID"] = image_id
        if digest is not UNSET:
            field_dict["Digest"] = digest
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        host = (None, str(self.host).encode(), "text/plain")

        name = (None, str(self.name).encode(), "text/plain")

        registry = (None, str(self.registry).encode(), "text/plain")

        version = (
            self.version
            if isinstance(self.version, Unset)
            else (None, str(self.version).encode(), "text/plain")
        )

        size = (
            self.size
            if isinstance(self.size, Unset)
            else (None, str(self.size).encode(), "text/plain")
        )

        image_id: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.image_id, Unset):
            image_id = UNSET
        elif isinstance(self.image_id, str):
            image_id = (None, str(self.image_id).encode(), "text/plain")
        else:
            image_id = (None, str(self.image_id).encode(), "text/plain")

        digest: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.digest, Unset):
            digest = UNSET
        elif isinstance(self.digest, str):
            digest = (None, str(self.digest).encode(), "text/plain")
        else:
            digest = (None, str(self.digest).encode(), "text/plain")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "host": host,
                "name": name,
                "registry": registry,
            }
        )
        if version is not UNSET:
            field_dict["version"] = version
        if size is not UNSET:
            field_dict["size"] = size
        if image_id is not UNSET:
            field_dict["ImageID"] = image_id
        if digest is not UNSET:
            field_dict["Digest"] = digest
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_image_request_custom_fields import (
            WritableImageRequestCustomFields,
        )

        d = src_dict.copy()
        host = d.pop("host")

        name = d.pop("name")

        registry = d.pop("registry")

        version = d.pop("version", UNSET)

        size = d.pop("size", UNSET)

        def _parse_image_id(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        image_id = _parse_image_id(d.pop("ImageID", UNSET))

        def _parse_digest(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        digest = _parse_digest(d.pop("Digest", UNSET))

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableImageRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableImageRequestCustomFields.from_dict(_custom_fields)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        writable_image_request = cls(
            host=host,
            name=name,
            registry=registry,
            version=version,
            size=size,
            image_id=image_id,
            digest=digest,
            custom_fields=custom_fields,
            tags=tags,
        )

        writable_image_request.additional_properties = d
        return writable_image_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
