from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_data_source_request import NestedDataSourceRequest
    from ..models.nested_tag_request import NestedTagRequest


T = TypeVar("T", bound="ConfigTemplateRequest")


@_attrs_define
class ConfigTemplateRequest:
    """Introduces support for Tag assignment. Adds `tags` serialization, and handles tag assignment
    on create() and update().

        Attributes:
            name (str):
            template_code (str): Jinja2 template code.
            description (Union[Unset, str]):
            environment_params (Union[Unset, Any]): Any <a
                href="https://jinja.palletsprojects.com/en/3.1.x/api/#jinja2.Environment">additional parameters</a> to pass when
                constructing the Jinja2 environment.
            data_source (Union[Unset, NestedDataSourceRequest]): Represents an object related through a ForeignKey field. On
                write, it accepts a primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            tags (Union[Unset, List['NestedTagRequest']]):
    """

    name: str
    template_code: str
    description: Union[Unset, str] = UNSET
    environment_params: Union[Unset, Any] = UNSET
    data_source: Union[Unset, "NestedDataSourceRequest"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        template_code = self.template_code

        description = self.description

        environment_params = self.environment_params

        data_source: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.data_source, Unset):
            data_source = self.data_source.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "template_code": template_code,
            }
        )
        if description is not UNSET:
            field_dict["description"] = description
        if environment_params is not UNSET:
            field_dict["environment_params"] = environment_params
        if data_source is not UNSET:
            field_dict["data_source"] = data_source
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_data_source_request import NestedDataSourceRequest
        from ..models.nested_tag_request import NestedTagRequest

        d = src_dict.copy()
        name = d.pop("name")

        template_code = d.pop("template_code")

        description = d.pop("description", UNSET)

        environment_params = d.pop("environment_params", UNSET)

        _data_source = d.pop("data_source", UNSET)
        data_source: Union[Unset, NestedDataSourceRequest]
        if isinstance(_data_source, Unset):
            data_source = UNSET
        else:
            data_source = NestedDataSourceRequest.from_dict(_data_source)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        config_template_request = cls(
            name=name,
            template_code=template_code,
            description=description,
            environment_params=environment_params,
            data_source=data_source,
            tags=tags,
        )

        config_template_request.additional_properties = d
        return config_template_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
