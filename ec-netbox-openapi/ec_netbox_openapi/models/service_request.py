from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.service_request_protocol import ServiceRequestProtocol
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_device_request import NestedDeviceRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_virtual_machine_request import NestedVirtualMachineRequest
    from ..models.service_request_custom_fields import ServiceRequestCustomFields


T = TypeVar("T", bound="ServiceRequest")


@_attrs_define
class ServiceRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        ports (List[int]):
        device (Union['NestedDeviceRequest', None, Unset]):
        virtual_machine (Union['NestedVirtualMachineRequest', None, Unset]):
        protocol (Union[Unset, ServiceRequestProtocol]): * `tcp` - TCP
            * `udp` - UDP
            * `sctp` - SCTP
        ipaddresses (Union[Unset, List[int]]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, ServiceRequestCustomFields]):
    """

    name: str
    ports: List[int]
    device: Union["NestedDeviceRequest", None, Unset] = UNSET
    virtual_machine: Union["NestedVirtualMachineRequest", None, Unset] = UNSET
    protocol: Union[Unset, ServiceRequestProtocol] = UNSET
    ipaddresses: Union[Unset, List[int]] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "ServiceRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_device_request import NestedDeviceRequest
        from ..models.nested_virtual_machine_request import NestedVirtualMachineRequest

        name = self.name

        ports = self.ports

        device: Union[Dict[str, Any], None, Unset]
        if isinstance(self.device, Unset):
            device = UNSET
        elif isinstance(self.device, NestedDeviceRequest):
            device = self.device.to_dict()
        else:
            device = self.device

        virtual_machine: Union[Dict[str, Any], None, Unset]
        if isinstance(self.virtual_machine, Unset):
            virtual_machine = UNSET
        elif isinstance(self.virtual_machine, NestedVirtualMachineRequest):
            virtual_machine = self.virtual_machine.to_dict()
        else:
            virtual_machine = self.virtual_machine

        protocol: Union[Unset, str] = UNSET
        if not isinstance(self.protocol, Unset):
            protocol = self.protocol.value

        ipaddresses: Union[Unset, List[int]] = UNSET
        if not isinstance(self.ipaddresses, Unset):
            ipaddresses = self.ipaddresses

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "ports": ports,
            }
        )
        if device is not UNSET:
            field_dict["device"] = device
        if virtual_machine is not UNSET:
            field_dict["virtual_machine"] = virtual_machine
        if protocol is not UNSET:
            field_dict["protocol"] = protocol
        if ipaddresses is not UNSET:
            field_dict["ipaddresses"] = ipaddresses
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_device_request import NestedDeviceRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_virtual_machine_request import NestedVirtualMachineRequest
        from ..models.service_request_custom_fields import ServiceRequestCustomFields

        d = src_dict.copy()
        name = d.pop("name")

        ports = cast(List[int], d.pop("ports"))

        def _parse_device(data: object) -> Union["NestedDeviceRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                device_type_1 = NestedDeviceRequest.from_dict(data)

                return device_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedDeviceRequest", None, Unset], data)

        device = _parse_device(d.pop("device", UNSET))

        def _parse_virtual_machine(
            data: object,
        ) -> Union["NestedVirtualMachineRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                virtual_machine_type_1 = NestedVirtualMachineRequest.from_dict(data)

                return virtual_machine_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVirtualMachineRequest", None, Unset], data)

        virtual_machine = _parse_virtual_machine(d.pop("virtual_machine", UNSET))

        _protocol = d.pop("protocol", UNSET)
        protocol: Union[Unset, ServiceRequestProtocol]
        if isinstance(_protocol, Unset):
            protocol = UNSET
        else:
            protocol = ServiceRequestProtocol(_protocol)

        ipaddresses = cast(List[int], d.pop("ipaddresses", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ServiceRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ServiceRequestCustomFields.from_dict(_custom_fields)

        service_request = cls(
            name=name,
            ports=ports,
            device=device,
            virtual_machine=virtual_machine,
            protocol=protocol,
            ipaddresses=ipaddresses,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        service_request.additional_properties = d
        return service_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
