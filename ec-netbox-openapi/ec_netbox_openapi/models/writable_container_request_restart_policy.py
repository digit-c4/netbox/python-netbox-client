from enum import Enum


class WritableContainerRequestRestartPolicy(str, Enum):
    ALWAYS = "always"
    NO = "no"
    ON_FAILURE = "on-failure"
    UNLESS_STOPPED = "unless-stopped"

    def __str__(self) -> str:
        return str(self.value)
