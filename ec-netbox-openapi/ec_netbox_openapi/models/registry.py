from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_host import NestedHost
    from ..models.nested_image import NestedImage


T = TypeVar("T", bound="Registry")


@_attrs_define
class Registry:
    """Registry Serializer class

    Attributes:
        id (int):
        name (str):
        url (str):
        host (NestedHost): Nested Host Serializer class
        display (str):
        serveraddress (str):
        images (List['NestedImage']):
        username (Union[None, Unset, str]):
        password (Union[None, Unset, str]):
        email (Union[None, Unset, str]):
    """

    id: int
    name: str
    url: str
    host: "NestedHost"
    display: str
    serveraddress: str
    images: List["NestedImage"]
    username: Union[None, Unset, str] = UNSET
    password: Union[None, Unset, str] = UNSET
    email: Union[None, Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        name = self.name

        url = self.url

        host = self.host.to_dict()

        display = self.display

        serveraddress = self.serveraddress

        images = []
        for images_item_data in self.images:
            images_item = images_item_data.to_dict()
            images.append(images_item)

        username: Union[None, Unset, str]
        if isinstance(self.username, Unset):
            username = UNSET
        else:
            username = self.username

        password: Union[None, Unset, str]
        if isinstance(self.password, Unset):
            password = UNSET
        else:
            password = self.password

        email: Union[None, Unset, str]
        if isinstance(self.email, Unset):
            email = UNSET
        else:
            email = self.email

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "name": name,
                "url": url,
                "host": host,
                "display": display,
                "serveraddress": serveraddress,
                "images": images,
            }
        )
        if username is not UNSET:
            field_dict["username"] = username
        if password is not UNSET:
            field_dict["password"] = password
        if email is not UNSET:
            field_dict["email"] = email

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_host import NestedHost
        from ..models.nested_image import NestedImage

        d = src_dict.copy()
        id = d.pop("id")

        name = d.pop("name")

        url = d.pop("url")

        host = NestedHost.from_dict(d.pop("host"))

        display = d.pop("display")

        serveraddress = d.pop("serveraddress")

        images = []
        _images = d.pop("images")
        for images_item_data in _images:
            images_item = NestedImage.from_dict(images_item_data)

            images.append(images_item)

        def _parse_username(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        username = _parse_username(d.pop("username", UNSET))

        def _parse_password(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        password = _parse_password(d.pop("password", UNSET))

        def _parse_email(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        email = _parse_email(d.pop("email", UNSET))

        registry = cls(
            id=id,
            name=name,
            url=url,
            host=host,
            display=display,
            serveraddress=serveraddress,
            images=images,
            username=username,
            password=password,
            email=email,
        )

        registry.additional_properties = d
        return registry

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
