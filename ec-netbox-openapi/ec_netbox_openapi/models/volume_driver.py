from enum import Enum


class VolumeDriver(str, Enum):
    LOCAL = "local"

    def __str__(self) -> str:
        return str(self.value)
