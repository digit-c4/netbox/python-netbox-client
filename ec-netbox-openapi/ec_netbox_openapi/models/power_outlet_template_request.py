from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.power_outlet_template_request_feed_leg_type_1 import (
    PowerOutletTemplateRequestFeedLegType1,
)
from ..models.power_outlet_template_request_feed_leg_type_2_type_1 import (
    PowerOutletTemplateRequestFeedLegType2Type1,
)
from ..models.power_outlet_template_request_feed_leg_type_3_type_1 import (
    PowerOutletTemplateRequestFeedLegType3Type1,
)
from ..models.power_outlet_template_request_type_type_1 import (
    PowerOutletTemplateRequestTypeType1,
)
from ..models.power_outlet_template_request_type_type_2_type_1 import (
    PowerOutletTemplateRequestTypeType2Type1,
)
from ..models.power_outlet_template_request_type_type_3_type_1 import (
    PowerOutletTemplateRequestTypeType3Type1,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_device_type_request import NestedDeviceTypeRequest
    from ..models.nested_module_type_request import NestedModuleTypeRequest
    from ..models.nested_power_port_template_request import (
        NestedPowerPortTemplateRequest,
    )


T = TypeVar("T", bound="PowerOutletTemplateRequest")


@_attrs_define
class PowerOutletTemplateRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            name (str): {module} is accepted as a substitution for the module bay position when attached to a module type.
            device_type (Union['NestedDeviceTypeRequest', None, Unset]):
            module_type (Union['NestedModuleTypeRequest', None, Unset]):
            label (Union[Unset, str]): Physical label
            type (Union[None, PowerOutletTemplateRequestTypeType1, PowerOutletTemplateRequestTypeType2Type1,
                PowerOutletTemplateRequestTypeType3Type1, Unset]): * `iec-60320-c5` - C5
                * `iec-60320-c7` - C7
                * `iec-60320-c13` - C13
                * `iec-60320-c15` - C15
                * `iec-60320-c19` - C19
                * `iec-60320-c21` - C21
                * `iec-60309-p-n-e-4h` - P+N+E 4H
                * `iec-60309-p-n-e-6h` - P+N+E 6H
                * `iec-60309-p-n-e-9h` - P+N+E 9H
                * `iec-60309-2p-e-4h` - 2P+E 4H
                * `iec-60309-2p-e-6h` - 2P+E 6H
                * `iec-60309-2p-e-9h` - 2P+E 9H
                * `iec-60309-3p-e-4h` - 3P+E 4H
                * `iec-60309-3p-e-6h` - 3P+E 6H
                * `iec-60309-3p-e-9h` - 3P+E 9H
                * `iec-60309-3p-n-e-4h` - 3P+N+E 4H
                * `iec-60309-3p-n-e-6h` - 3P+N+E 6H
                * `iec-60309-3p-n-e-9h` - 3P+N+E 9H
                * `iec-60906-1` - IEC 60906-1
                * `nbr-14136-10a` - 2P+T 10A (NBR 14136)
                * `nbr-14136-20a` - 2P+T 20A (NBR 14136)
                * `nema-1-15r` - NEMA 1-15R
                * `nema-5-15r` - NEMA 5-15R
                * `nema-5-20r` - NEMA 5-20R
                * `nema-5-30r` - NEMA 5-30R
                * `nema-5-50r` - NEMA 5-50R
                * `nema-6-15r` - NEMA 6-15R
                * `nema-6-20r` - NEMA 6-20R
                * `nema-6-30r` - NEMA 6-30R
                * `nema-6-50r` - NEMA 6-50R
                * `nema-10-30r` - NEMA 10-30R
                * `nema-10-50r` - NEMA 10-50R
                * `nema-14-20r` - NEMA 14-20R
                * `nema-14-30r` - NEMA 14-30R
                * `nema-14-50r` - NEMA 14-50R
                * `nema-14-60r` - NEMA 14-60R
                * `nema-15-15r` - NEMA 15-15R
                * `nema-15-20r` - NEMA 15-20R
                * `nema-15-30r` - NEMA 15-30R
                * `nema-15-50r` - NEMA 15-50R
                * `nema-15-60r` - NEMA 15-60R
                * `nema-l1-15r` - NEMA L1-15R
                * `nema-l5-15r` - NEMA L5-15R
                * `nema-l5-20r` - NEMA L5-20R
                * `nema-l5-30r` - NEMA L5-30R
                * `nema-l5-50r` - NEMA L5-50R
                * `nema-l6-15r` - NEMA L6-15R
                * `nema-l6-20r` - NEMA L6-20R
                * `nema-l6-30r` - NEMA L6-30R
                * `nema-l6-50r` - NEMA L6-50R
                * `nema-l10-30r` - NEMA L10-30R
                * `nema-l14-20r` - NEMA L14-20R
                * `nema-l14-30r` - NEMA L14-30R
                * `nema-l14-50r` - NEMA L14-50R
                * `nema-l14-60r` - NEMA L14-60R
                * `nema-l15-20r` - NEMA L15-20R
                * `nema-l15-30r` - NEMA L15-30R
                * `nema-l15-50r` - NEMA L15-50R
                * `nema-l15-60r` - NEMA L15-60R
                * `nema-l21-20r` - NEMA L21-20R
                * `nema-l21-30r` - NEMA L21-30R
                * `nema-l22-30r` - NEMA L22-30R
                * `CS6360C` - CS6360C
                * `CS6364C` - CS6364C
                * `CS8164C` - CS8164C
                * `CS8264C` - CS8264C
                * `CS8364C` - CS8364C
                * `CS8464C` - CS8464C
                * `ita-e` - ITA Type E (CEE 7/5)
                * `ita-f` - ITA Type F (CEE 7/3)
                * `ita-g` - ITA Type G (BS 1363)
                * `ita-h` - ITA Type H
                * `ita-i` - ITA Type I
                * `ita-j` - ITA Type J
                * `ita-k` - ITA Type K
                * `ita-l` - ITA Type L (CEI 23-50)
                * `ita-m` - ITA Type M (BS 546)
                * `ita-n` - ITA Type N
                * `ita-o` - ITA Type O
                * `ita-multistandard` - ITA Multistandard
                * `usb-a` - USB Type A
                * `usb-micro-b` - USB Micro B
                * `usb-c` - USB Type C
                * `dc-terminal` - DC Terminal
                * `hdot-cx` - HDOT Cx
                * `saf-d-grid` - Saf-D-Grid
                * `neutrik-powercon-20a` - Neutrik powerCON (20A)
                * `neutrik-powercon-32a` - Neutrik powerCON (32A)
                * `neutrik-powercon-true1` - Neutrik powerCON TRUE1
                * `neutrik-powercon-true1-top` - Neutrik powerCON TRUE1 TOP
                * `ubiquiti-smartpower` - Ubiquiti SmartPower
                * `hardwired` - Hardwired
                * `other` - Other
            power_port (Union['NestedPowerPortTemplateRequest', None, Unset]):
            feed_leg (Union[None, PowerOutletTemplateRequestFeedLegType1, PowerOutletTemplateRequestFeedLegType2Type1,
                PowerOutletTemplateRequestFeedLegType3Type1, Unset]): * `A` - A
                * `B` - B
                * `C` - C
            description (Union[Unset, str]):
    """

    name: str
    device_type: Union["NestedDeviceTypeRequest", None, Unset] = UNSET
    module_type: Union["NestedModuleTypeRequest", None, Unset] = UNSET
    label: Union[Unset, str] = UNSET
    type: Union[
        None,
        PowerOutletTemplateRequestTypeType1,
        PowerOutletTemplateRequestTypeType2Type1,
        PowerOutletTemplateRequestTypeType3Type1,
        Unset,
    ] = UNSET
    power_port: Union["NestedPowerPortTemplateRequest", None, Unset] = UNSET
    feed_leg: Union[
        None,
        PowerOutletTemplateRequestFeedLegType1,
        PowerOutletTemplateRequestFeedLegType2Type1,
        PowerOutletTemplateRequestFeedLegType3Type1,
        Unset,
    ] = UNSET
    description: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_device_type_request import NestedDeviceTypeRequest
        from ..models.nested_module_type_request import NestedModuleTypeRequest
        from ..models.nested_power_port_template_request import (
            NestedPowerPortTemplateRequest,
        )

        name = self.name

        device_type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.device_type, Unset):
            device_type = UNSET
        elif isinstance(self.device_type, NestedDeviceTypeRequest):
            device_type = self.device_type.to_dict()
        else:
            device_type = self.device_type

        module_type: Union[Dict[str, Any], None, Unset]
        if isinstance(self.module_type, Unset):
            module_type = UNSET
        elif isinstance(self.module_type, NestedModuleTypeRequest):
            module_type = self.module_type.to_dict()
        else:
            module_type = self.module_type

        label = self.label

        type: Union[None, Unset, str]
        if isinstance(self.type, Unset):
            type = UNSET
        elif isinstance(self.type, PowerOutletTemplateRequestTypeType1):
            type = self.type.value
        elif isinstance(self.type, PowerOutletTemplateRequestTypeType2Type1):
            type = self.type.value
        elif isinstance(self.type, PowerOutletTemplateRequestTypeType3Type1):
            type = self.type.value
        else:
            type = self.type

        power_port: Union[Dict[str, Any], None, Unset]
        if isinstance(self.power_port, Unset):
            power_port = UNSET
        elif isinstance(self.power_port, NestedPowerPortTemplateRequest):
            power_port = self.power_port.to_dict()
        else:
            power_port = self.power_port

        feed_leg: Union[None, Unset, str]
        if isinstance(self.feed_leg, Unset):
            feed_leg = UNSET
        elif isinstance(self.feed_leg, PowerOutletTemplateRequestFeedLegType1):
            feed_leg = self.feed_leg.value
        elif isinstance(self.feed_leg, PowerOutletTemplateRequestFeedLegType2Type1):
            feed_leg = self.feed_leg.value
        elif isinstance(self.feed_leg, PowerOutletTemplateRequestFeedLegType3Type1):
            feed_leg = self.feed_leg.value
        else:
            feed_leg = self.feed_leg

        description = self.description

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
            }
        )
        if device_type is not UNSET:
            field_dict["device_type"] = device_type
        if module_type is not UNSET:
            field_dict["module_type"] = module_type
        if label is not UNSET:
            field_dict["label"] = label
        if type is not UNSET:
            field_dict["type"] = type
        if power_port is not UNSET:
            field_dict["power_port"] = power_port
        if feed_leg is not UNSET:
            field_dict["feed_leg"] = feed_leg
        if description is not UNSET:
            field_dict["description"] = description

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_device_type_request import NestedDeviceTypeRequest
        from ..models.nested_module_type_request import NestedModuleTypeRequest
        from ..models.nested_power_port_template_request import (
            NestedPowerPortTemplateRequest,
        )

        d = src_dict.copy()
        name = d.pop("name")

        def _parse_device_type(
            data: object,
        ) -> Union["NestedDeviceTypeRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                device_type_type_1 = NestedDeviceTypeRequest.from_dict(data)

                return device_type_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedDeviceTypeRequest", None, Unset], data)

        device_type = _parse_device_type(d.pop("device_type", UNSET))

        def _parse_module_type(
            data: object,
        ) -> Union["NestedModuleTypeRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                module_type_type_1 = NestedModuleTypeRequest.from_dict(data)

                return module_type_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedModuleTypeRequest", None, Unset], data)

        module_type = _parse_module_type(d.pop("module_type", UNSET))

        label = d.pop("label", UNSET)

        def _parse_type(
            data: object,
        ) -> Union[
            None,
            PowerOutletTemplateRequestTypeType1,
            PowerOutletTemplateRequestTypeType2Type1,
            PowerOutletTemplateRequestTypeType3Type1,
            Unset,
        ]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                type_type_1 = PowerOutletTemplateRequestTypeType1(data)

                return type_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                type_type_2_type_1 = PowerOutletTemplateRequestTypeType2Type1(data)

                return type_type_2_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                type_type_3_type_1 = PowerOutletTemplateRequestTypeType3Type1(data)

                return type_type_3_type_1
            except:  # noqa: E722
                pass
            return cast(
                Union[
                    None,
                    PowerOutletTemplateRequestTypeType1,
                    PowerOutletTemplateRequestTypeType2Type1,
                    PowerOutletTemplateRequestTypeType3Type1,
                    Unset,
                ],
                data,
            )

        type = _parse_type(d.pop("type", UNSET))

        def _parse_power_port(
            data: object,
        ) -> Union["NestedPowerPortTemplateRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                power_port_type_1 = NestedPowerPortTemplateRequest.from_dict(data)

                return power_port_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedPowerPortTemplateRequest", None, Unset], data)

        power_port = _parse_power_port(d.pop("power_port", UNSET))

        def _parse_feed_leg(
            data: object,
        ) -> Union[
            None,
            PowerOutletTemplateRequestFeedLegType1,
            PowerOutletTemplateRequestFeedLegType2Type1,
            PowerOutletTemplateRequestFeedLegType3Type1,
            Unset,
        ]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                feed_leg_type_1 = PowerOutletTemplateRequestFeedLegType1(data)

                return feed_leg_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                feed_leg_type_2_type_1 = PowerOutletTemplateRequestFeedLegType2Type1(
                    data
                )

                return feed_leg_type_2_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                feed_leg_type_3_type_1 = PowerOutletTemplateRequestFeedLegType3Type1(
                    data
                )

                return feed_leg_type_3_type_1
            except:  # noqa: E722
                pass
            return cast(
                Union[
                    None,
                    PowerOutletTemplateRequestFeedLegType1,
                    PowerOutletTemplateRequestFeedLegType2Type1,
                    PowerOutletTemplateRequestFeedLegType3Type1,
                    Unset,
                ],
                data,
            )

        feed_leg = _parse_feed_leg(d.pop("feed_leg", UNSET))

        description = d.pop("description", UNSET)

        power_outlet_template_request = cls(
            name=name,
            device_type=device_type,
            module_type=module_type,
            label=label,
            type=type,
            power_port=power_port,
            feed_leg=feed_leg,
            description=description,
        )

        power_outlet_template_request.additional_properties = d
        return power_outlet_template_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
