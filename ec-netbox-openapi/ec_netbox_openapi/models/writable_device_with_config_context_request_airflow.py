from enum import Enum


class WritableDeviceWithConfigContextRequestAirflow(str, Enum):
    FRONT_TO_REAR = "front-to-rear"
    LEFT_TO_RIGHT = "left-to-right"
    MIXED = "mixed"
    PASSIVE = "passive"
    REAR_TO_FRONT = "rear-to-front"
    RIGHT_TO_LEFT = "right-to-left"
    SIDE_TO_REAR = "side-to-rear"
    VALUE_7 = ""

    def __str__(self) -> str:
        return str(self.value)
