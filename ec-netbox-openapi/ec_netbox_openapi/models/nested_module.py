from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

if TYPE_CHECKING:
    from ..models.module_nested_module_bay import ModuleNestedModuleBay
    from ..models.nested_device import NestedDevice
    from ..models.nested_module_type import NestedModuleType


T = TypeVar("T", bound="NestedModule")


@_attrs_define
class NestedModule:
    """Represents an object related through a ForeignKey field. On write, it accepts a primary key (PK) value or a
    dictionary of attributes which can be used to uniquely identify the related object. This class should be
    subclassed to return a full representation of the related object on read.

        Attributes:
            id (int):
            url (str):
            display (str):
            device (NestedDevice): Represents an object related through a ForeignKey field. On write, it accepts a primary
                key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            module_bay (ModuleNestedModuleBay): Represents an object related through a ForeignKey field. On write, it
                accepts a primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            module_type (NestedModuleType): Represents an object related through a ForeignKey field. On write, it accepts a
                primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
    """

    id: int
    url: str
    display: str
    device: "NestedDevice"
    module_bay: "ModuleNestedModuleBay"
    module_type: "NestedModuleType"
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        device = self.device.to_dict()

        module_bay = self.module_bay.to_dict()

        module_type = self.module_type.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "device": device,
                "module_bay": module_bay,
                "module_type": module_type,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.module_nested_module_bay import ModuleNestedModuleBay
        from ..models.nested_device import NestedDevice
        from ..models.nested_module_type import NestedModuleType

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        device = NestedDevice.from_dict(d.pop("device"))

        module_bay = ModuleNestedModuleBay.from_dict(d.pop("module_bay"))

        module_type = NestedModuleType.from_dict(d.pop("module_type"))

        nested_module = cls(
            id=id,
            url=url,
            display=display,
            device=device,
            module_bay=module_bay,
            module_type=module_type,
        )

        nested_module.additional_properties = d
        return nested_module

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
