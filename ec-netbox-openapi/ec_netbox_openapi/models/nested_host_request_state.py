from enum import Enum


class NestedHostRequestState(str, Enum):
    CREATED = "created"
    DELETED = "deleted"
    REFRESHING = "refreshing"
    RUNNING = "running"

    def __str__(self) -> str:
        return str(self.value)
