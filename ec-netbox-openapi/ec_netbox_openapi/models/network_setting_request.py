from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

if TYPE_CHECKING:
    from ..models.nested_network_request import NestedNetworkRequest


T = TypeVar("T", bound="NetworkSettingRequest")


@_attrs_define
class NetworkSettingRequest:
    """Container NetworkSetting Serializer class

    Attributes:
        network (NestedNetworkRequest): Nested Network Serializer class
    """

    network: "NestedNetworkRequest"
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        network = self.network.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "network": network,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_network_request import NestedNetworkRequest

        d = src_dict.copy()
        network = NestedNetworkRequest.from_dict(d.pop("network"))

        network_setting_request = cls(
            network=network,
        )

        network_setting_request.additional_properties = d
        return network_setting_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
