import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.writable_console_port_request_speed_type_1 import (
    WritableConsolePortRequestSpeedType1,
)
from ..models.writable_console_port_request_speed_type_2_type_1 import (
    WritableConsolePortRequestSpeedType2Type1,
)
from ..models.writable_console_port_request_speed_type_3_type_1 import (
    WritableConsolePortRequestSpeedType3Type1,
)
from ..models.writable_console_port_request_type import WritableConsolePortRequestType
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.writable_console_port_request_custom_fields import (
        WritableConsolePortRequestCustomFields,
    )


T = TypeVar("T", bound="WritableConsolePortRequest")


@_attrs_define
class WritableConsolePortRequest:
    """Adds support for custom fields and tags.

    Attributes:
        device (int):
        name (str):
        module (Union[None, Unset, int]):
        label (Union[Unset, str]): Physical label
        type (Union[Unset, WritableConsolePortRequestType]): Physical port type

            * `de-9` - DE-9
            * `db-25` - DB-25
            * `rj-11` - RJ-11
            * `rj-12` - RJ-12
            * `rj-45` - RJ-45
            * `mini-din-8` - Mini-DIN 8
            * `usb-a` - USB Type A
            * `usb-b` - USB Type B
            * `usb-c` - USB Type C
            * `usb-mini-a` - USB Mini A
            * `usb-mini-b` - USB Mini B
            * `usb-micro-a` - USB Micro A
            * `usb-micro-b` - USB Micro B
            * `usb-micro-ab` - USB Micro AB
            * `other` - Other
        speed (Union[None, Unset, WritableConsolePortRequestSpeedType1, WritableConsolePortRequestSpeedType2Type1,
            WritableConsolePortRequestSpeedType3Type1]): Port speed in bits per second

            * `1200` - 1200 bps
            * `2400` - 2400 bps
            * `4800` - 4800 bps
            * `9600` - 9600 bps
            * `19200` - 19.2 kbps
            * `38400` - 38.4 kbps
            * `57600` - 57.6 kbps
            * `115200` - 115.2 kbps
        description (Union[Unset, str]):
        mark_connected (Union[Unset, bool]): Treat as if a cable is connected
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, WritableConsolePortRequestCustomFields]):
    """

    device: int
    name: str
    module: Union[None, Unset, int] = UNSET
    label: Union[Unset, str] = UNSET
    type: Union[Unset, WritableConsolePortRequestType] = UNSET
    speed: Union[
        None,
        Unset,
        WritableConsolePortRequestSpeedType1,
        WritableConsolePortRequestSpeedType2Type1,
        WritableConsolePortRequestSpeedType3Type1,
    ] = UNSET
    description: Union[Unset, str] = UNSET
    mark_connected: Union[Unset, bool] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "WritableConsolePortRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        device = self.device

        name = self.name

        module: Union[None, Unset, int]
        if isinstance(self.module, Unset):
            module = UNSET
        else:
            module = self.module

        label = self.label

        type: Union[Unset, str] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.value

        speed: Union[None, Unset, int]
        if isinstance(self.speed, Unset):
            speed = UNSET
        elif isinstance(self.speed, WritableConsolePortRequestSpeedType1):
            speed = self.speed.value
        elif isinstance(self.speed, WritableConsolePortRequestSpeedType2Type1):
            speed = self.speed.value
        elif isinstance(self.speed, WritableConsolePortRequestSpeedType3Type1):
            speed = self.speed.value
        else:
            speed = self.speed

        description = self.description

        mark_connected = self.mark_connected

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "device": device,
                "name": name,
            }
        )
        if module is not UNSET:
            field_dict["module"] = module
        if label is not UNSET:
            field_dict["label"] = label
        if type is not UNSET:
            field_dict["type"] = type
        if speed is not UNSET:
            field_dict["speed"] = speed
        if description is not UNSET:
            field_dict["description"] = description
        if mark_connected is not UNSET:
            field_dict["mark_connected"] = mark_connected
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        device = (None, str(self.device).encode(), "text/plain")

        name = (None, str(self.name).encode(), "text/plain")

        module: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.module, Unset):
            module = UNSET
        elif isinstance(self.module, int):
            module = (None, str(self.module).encode(), "text/plain")
        else:
            module = (None, str(self.module).encode(), "text/plain")

        label = (
            self.label
            if isinstance(self.label, Unset)
            else (None, str(self.label).encode(), "text/plain")
        )

        type: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.type, Unset):
            type = (None, str(self.type.value).encode(), "text/plain")

        speed: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.speed, Unset):
            speed = UNSET
        elif isinstance(self.speed, None):
            speed = (None, str(self.speed).encode(), "text/plain")
        elif isinstance(self.speed, WritableConsolePortRequestSpeedType1):
            speed = (None, str(self.speed.value).encode(), "text/plain")
        elif isinstance(self.speed, None):
            speed = (None, str(self.speed).encode(), "text/plain")
        elif isinstance(self.speed, WritableConsolePortRequestSpeedType2Type1):
            speed = (None, str(self.speed.value).encode(), "text/plain")
        elif isinstance(self.speed, None):
            speed = (None, str(self.speed).encode(), "text/plain")
        else:
            speed = (None, str(self.speed.value).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        mark_connected = (
            self.mark_connected
            if isinstance(self.mark_connected, Unset)
            else (None, str(self.mark_connected).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "device": device,
                "name": name,
            }
        )
        if module is not UNSET:
            field_dict["module"] = module
        if label is not UNSET:
            field_dict["label"] = label
        if type is not UNSET:
            field_dict["type"] = type
        if speed is not UNSET:
            field_dict["speed"] = speed
        if description is not UNSET:
            field_dict["description"] = description
        if mark_connected is not UNSET:
            field_dict["mark_connected"] = mark_connected
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.writable_console_port_request_custom_fields import (
            WritableConsolePortRequestCustomFields,
        )

        d = src_dict.copy()
        device = d.pop("device")

        name = d.pop("name")

        def _parse_module(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        module = _parse_module(d.pop("module", UNSET))

        label = d.pop("label", UNSET)

        _type = d.pop("type", UNSET)
        type: Union[Unset, WritableConsolePortRequestType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = WritableConsolePortRequestType(_type)

        def _parse_speed(
            data: object,
        ) -> Union[
            None,
            Unset,
            WritableConsolePortRequestSpeedType1,
            WritableConsolePortRequestSpeedType2Type1,
            WritableConsolePortRequestSpeedType3Type1,
        ]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, int):
                    raise TypeError()
                speed_type_1 = WritableConsolePortRequestSpeedType1(data)

                return speed_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, int):
                    raise TypeError()
                speed_type_2_type_1 = WritableConsolePortRequestSpeedType2Type1(data)

                return speed_type_2_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, int):
                    raise TypeError()
                speed_type_3_type_1 = WritableConsolePortRequestSpeedType3Type1(data)

                return speed_type_3_type_1
            except:  # noqa: E722
                pass
            return cast(
                Union[
                    None,
                    Unset,
                    WritableConsolePortRequestSpeedType1,
                    WritableConsolePortRequestSpeedType2Type1,
                    WritableConsolePortRequestSpeedType3Type1,
                ],
                data,
            )

        speed = _parse_speed(d.pop("speed", UNSET))

        description = d.pop("description", UNSET)

        mark_connected = d.pop("mark_connected", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, WritableConsolePortRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = WritableConsolePortRequestCustomFields.from_dict(
                _custom_fields
            )

        writable_console_port_request = cls(
            device=device,
            name=name,
            module=module,
            label=label,
            type=type,
            speed=speed,
            description=description,
            mark_connected=mark_connected,
            tags=tags,
            custom_fields=custom_fields,
        )

        writable_console_port_request.additional_properties = d
        return writable_console_port_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
