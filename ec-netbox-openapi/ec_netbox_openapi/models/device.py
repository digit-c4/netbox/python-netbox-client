import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.device_airflow import DeviceAirflow
    from ..models.device_custom_fields import DeviceCustomFields
    from ..models.device_face import DeviceFace
    from ..models.device_status import DeviceStatus
    from ..models.nested_cluster import NestedCluster
    from ..models.nested_config_template import NestedConfigTemplate
    from ..models.nested_device import NestedDevice
    from ..models.nested_device_role import NestedDeviceRole
    from ..models.nested_device_type import NestedDeviceType
    from ..models.nested_ip_address import NestedIPAddress
    from ..models.nested_location import NestedLocation
    from ..models.nested_platform import NestedPlatform
    from ..models.nested_rack import NestedRack
    from ..models.nested_site import NestedSite
    from ..models.nested_tag import NestedTag
    from ..models.nested_tenant import NestedTenant
    from ..models.nested_virtual_chassis import NestedVirtualChassis


T = TypeVar("T", bound="Device")


@_attrs_define
class Device:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        device_type (NestedDeviceType): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        role (NestedDeviceRole): Represents an object related through a ForeignKey field. On write, it accepts a primary
            key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        device_role (NestedDeviceRole): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        site (NestedSite): Represents an object related through a ForeignKey field. On write, it accepts a primary key
            (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        parent_device (NestedDevice): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        primary_ip (NestedIPAddress): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        console_port_count (int):
        console_server_port_count (int):
        power_port_count (int):
        power_outlet_count (int):
        interface_count (int):
        front_port_count (int):
        rear_port_count (int):
        device_bay_count (int):
        module_bay_count (int):
        inventory_item_count (int):
        name (Union[None, Unset, str]):
        tenant (Union['NestedTenant', None, Unset]):
        platform (Union['NestedPlatform', None, Unset]):
        serial (Union[Unset, str]): Chassis serial number, assigned by the manufacturer
        asset_tag (Union[None, Unset, str]): A unique tag used to identify this device
        location (Union['NestedLocation', None, Unset]):
        rack (Union['NestedRack', None, Unset]):
        position (Union[None, Unset, float]):
        face (Union[Unset, DeviceFace]):
        latitude (Union[None, Unset, float]): GPS coordinate in decimal format (xx.yyyyyy)
        longitude (Union[None, Unset, float]): GPS coordinate in decimal format (xx.yyyyyy)
        status (Union[Unset, DeviceStatus]):
        airflow (Union[Unset, DeviceAirflow]):
        primary_ip4 (Union['NestedIPAddress', None, Unset]):
        primary_ip6 (Union['NestedIPAddress', None, Unset]):
        oob_ip (Union['NestedIPAddress', None, Unset]):
        cluster (Union['NestedCluster', None, Unset]):
        virtual_chassis (Union['NestedVirtualChassis', None, Unset]):
        vc_position (Union[None, Unset, int]):
        vc_priority (Union[None, Unset, int]): Virtual chassis master election priority
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        config_template (Union['NestedConfigTemplate', None, Unset]):
        local_context_data (Union[Unset, Any]): Local config context data takes precedence over source contexts in the
            final rendered config context
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, DeviceCustomFields]):
    """

    id: int
    url: str
    display: str
    device_type: "NestedDeviceType"
    role: "NestedDeviceRole"
    device_role: "NestedDeviceRole"
    site: "NestedSite"
    parent_device: "NestedDevice"
    primary_ip: "NestedIPAddress"
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    console_port_count: int
    console_server_port_count: int
    power_port_count: int
    power_outlet_count: int
    interface_count: int
    front_port_count: int
    rear_port_count: int
    device_bay_count: int
    module_bay_count: int
    inventory_item_count: int
    name: Union[None, Unset, str] = UNSET
    tenant: Union["NestedTenant", None, Unset] = UNSET
    platform: Union["NestedPlatform", None, Unset] = UNSET
    serial: Union[Unset, str] = UNSET
    asset_tag: Union[None, Unset, str] = UNSET
    location: Union["NestedLocation", None, Unset] = UNSET
    rack: Union["NestedRack", None, Unset] = UNSET
    position: Union[None, Unset, float] = UNSET
    face: Union[Unset, "DeviceFace"] = UNSET
    latitude: Union[None, Unset, float] = UNSET
    longitude: Union[None, Unset, float] = UNSET
    status: Union[Unset, "DeviceStatus"] = UNSET
    airflow: Union[Unset, "DeviceAirflow"] = UNSET
    primary_ip4: Union["NestedIPAddress", None, Unset] = UNSET
    primary_ip6: Union["NestedIPAddress", None, Unset] = UNSET
    oob_ip: Union["NestedIPAddress", None, Unset] = UNSET
    cluster: Union["NestedCluster", None, Unset] = UNSET
    virtual_chassis: Union["NestedVirtualChassis", None, Unset] = UNSET
    vc_position: Union[None, Unset, int] = UNSET
    vc_priority: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    config_template: Union["NestedConfigTemplate", None, Unset] = UNSET
    local_context_data: Union[Unset, Any] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "DeviceCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_cluster import NestedCluster
        from ..models.nested_config_template import NestedConfigTemplate
        from ..models.nested_ip_address import NestedIPAddress
        from ..models.nested_location import NestedLocation
        from ..models.nested_platform import NestedPlatform
        from ..models.nested_rack import NestedRack
        from ..models.nested_tenant import NestedTenant
        from ..models.nested_virtual_chassis import NestedVirtualChassis

        id = self.id

        url = self.url

        display = self.display

        device_type = self.device_type.to_dict()

        role = self.role.to_dict()

        device_role = self.device_role.to_dict()

        site = self.site.to_dict()

        parent_device = self.parent_device.to_dict()

        primary_ip = self.primary_ip.to_dict()

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        console_port_count = self.console_port_count

        console_server_port_count = self.console_server_port_count

        power_port_count = self.power_port_count

        power_outlet_count = self.power_outlet_count

        interface_count = self.interface_count

        front_port_count = self.front_port_count

        rear_port_count = self.rear_port_count

        device_bay_count = self.device_bay_count

        module_bay_count = self.module_bay_count

        inventory_item_count = self.inventory_item_count

        name: Union[None, Unset, str]
        if isinstance(self.name, Unset):
            name = UNSET
        else:
            name = self.name

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenant):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        platform: Union[Dict[str, Any], None, Unset]
        if isinstance(self.platform, Unset):
            platform = UNSET
        elif isinstance(self.platform, NestedPlatform):
            platform = self.platform.to_dict()
        else:
            platform = self.platform

        serial = self.serial

        asset_tag: Union[None, Unset, str]
        if isinstance(self.asset_tag, Unset):
            asset_tag = UNSET
        else:
            asset_tag = self.asset_tag

        location: Union[Dict[str, Any], None, Unset]
        if isinstance(self.location, Unset):
            location = UNSET
        elif isinstance(self.location, NestedLocation):
            location = self.location.to_dict()
        else:
            location = self.location

        rack: Union[Dict[str, Any], None, Unset]
        if isinstance(self.rack, Unset):
            rack = UNSET
        elif isinstance(self.rack, NestedRack):
            rack = self.rack.to_dict()
        else:
            rack = self.rack

        position: Union[None, Unset, float]
        if isinstance(self.position, Unset):
            position = UNSET
        else:
            position = self.position

        face: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.face, Unset):
            face = self.face.to_dict()

        latitude: Union[None, Unset, float]
        if isinstance(self.latitude, Unset):
            latitude = UNSET
        else:
            latitude = self.latitude

        longitude: Union[None, Unset, float]
        if isinstance(self.longitude, Unset):
            longitude = UNSET
        else:
            longitude = self.longitude

        status: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.to_dict()

        airflow: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.airflow, Unset):
            airflow = self.airflow.to_dict()

        primary_ip4: Union[Dict[str, Any], None, Unset]
        if isinstance(self.primary_ip4, Unset):
            primary_ip4 = UNSET
        elif isinstance(self.primary_ip4, NestedIPAddress):
            primary_ip4 = self.primary_ip4.to_dict()
        else:
            primary_ip4 = self.primary_ip4

        primary_ip6: Union[Dict[str, Any], None, Unset]
        if isinstance(self.primary_ip6, Unset):
            primary_ip6 = UNSET
        elif isinstance(self.primary_ip6, NestedIPAddress):
            primary_ip6 = self.primary_ip6.to_dict()
        else:
            primary_ip6 = self.primary_ip6

        oob_ip: Union[Dict[str, Any], None, Unset]
        if isinstance(self.oob_ip, Unset):
            oob_ip = UNSET
        elif isinstance(self.oob_ip, NestedIPAddress):
            oob_ip = self.oob_ip.to_dict()
        else:
            oob_ip = self.oob_ip

        cluster: Union[Dict[str, Any], None, Unset]
        if isinstance(self.cluster, Unset):
            cluster = UNSET
        elif isinstance(self.cluster, NestedCluster):
            cluster = self.cluster.to_dict()
        else:
            cluster = self.cluster

        virtual_chassis: Union[Dict[str, Any], None, Unset]
        if isinstance(self.virtual_chassis, Unset):
            virtual_chassis = UNSET
        elif isinstance(self.virtual_chassis, NestedVirtualChassis):
            virtual_chassis = self.virtual_chassis.to_dict()
        else:
            virtual_chassis = self.virtual_chassis

        vc_position: Union[None, Unset, int]
        if isinstance(self.vc_position, Unset):
            vc_position = UNSET
        else:
            vc_position = self.vc_position

        vc_priority: Union[None, Unset, int]
        if isinstance(self.vc_priority, Unset):
            vc_priority = UNSET
        else:
            vc_priority = self.vc_priority

        description = self.description

        comments = self.comments

        config_template: Union[Dict[str, Any], None, Unset]
        if isinstance(self.config_template, Unset):
            config_template = UNSET
        elif isinstance(self.config_template, NestedConfigTemplate):
            config_template = self.config_template.to_dict()
        else:
            config_template = self.config_template

        local_context_data = self.local_context_data

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "device_type": device_type,
                "role": role,
                "device_role": device_role,
                "site": site,
                "parent_device": parent_device,
                "primary_ip": primary_ip,
                "created": created,
                "last_updated": last_updated,
                "console_port_count": console_port_count,
                "console_server_port_count": console_server_port_count,
                "power_port_count": power_port_count,
                "power_outlet_count": power_outlet_count,
                "interface_count": interface_count,
                "front_port_count": front_port_count,
                "rear_port_count": rear_port_count,
                "device_bay_count": device_bay_count,
                "module_bay_count": module_bay_count,
                "inventory_item_count": inventory_item_count,
            }
        )
        if name is not UNSET:
            field_dict["name"] = name
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if platform is not UNSET:
            field_dict["platform"] = platform
        if serial is not UNSET:
            field_dict["serial"] = serial
        if asset_tag is not UNSET:
            field_dict["asset_tag"] = asset_tag
        if location is not UNSET:
            field_dict["location"] = location
        if rack is not UNSET:
            field_dict["rack"] = rack
        if position is not UNSET:
            field_dict["position"] = position
        if face is not UNSET:
            field_dict["face"] = face
        if latitude is not UNSET:
            field_dict["latitude"] = latitude
        if longitude is not UNSET:
            field_dict["longitude"] = longitude
        if status is not UNSET:
            field_dict["status"] = status
        if airflow is not UNSET:
            field_dict["airflow"] = airflow
        if primary_ip4 is not UNSET:
            field_dict["primary_ip4"] = primary_ip4
        if primary_ip6 is not UNSET:
            field_dict["primary_ip6"] = primary_ip6
        if oob_ip is not UNSET:
            field_dict["oob_ip"] = oob_ip
        if cluster is not UNSET:
            field_dict["cluster"] = cluster
        if virtual_chassis is not UNSET:
            field_dict["virtual_chassis"] = virtual_chassis
        if vc_position is not UNSET:
            field_dict["vc_position"] = vc_position
        if vc_priority is not UNSET:
            field_dict["vc_priority"] = vc_priority
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if config_template is not UNSET:
            field_dict["config_template"] = config_template
        if local_context_data is not UNSET:
            field_dict["local_context_data"] = local_context_data
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.device_airflow import DeviceAirflow
        from ..models.device_custom_fields import DeviceCustomFields
        from ..models.device_face import DeviceFace
        from ..models.device_status import DeviceStatus
        from ..models.nested_cluster import NestedCluster
        from ..models.nested_config_template import NestedConfigTemplate
        from ..models.nested_device import NestedDevice
        from ..models.nested_device_role import NestedDeviceRole
        from ..models.nested_device_type import NestedDeviceType
        from ..models.nested_ip_address import NestedIPAddress
        from ..models.nested_location import NestedLocation
        from ..models.nested_platform import NestedPlatform
        from ..models.nested_rack import NestedRack
        from ..models.nested_site import NestedSite
        from ..models.nested_tag import NestedTag
        from ..models.nested_tenant import NestedTenant
        from ..models.nested_virtual_chassis import NestedVirtualChassis

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        device_type = NestedDeviceType.from_dict(d.pop("device_type"))

        role = NestedDeviceRole.from_dict(d.pop("role"))

        device_role = NestedDeviceRole.from_dict(d.pop("device_role"))

        site = NestedSite.from_dict(d.pop("site"))

        parent_device = NestedDevice.from_dict(d.pop("parent_device"))

        primary_ip = NestedIPAddress.from_dict(d.pop("primary_ip"))

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        console_port_count = d.pop("console_port_count")

        console_server_port_count = d.pop("console_server_port_count")

        power_port_count = d.pop("power_port_count")

        power_outlet_count = d.pop("power_outlet_count")

        interface_count = d.pop("interface_count")

        front_port_count = d.pop("front_port_count")

        rear_port_count = d.pop("rear_port_count")

        device_bay_count = d.pop("device_bay_count")

        module_bay_count = d.pop("module_bay_count")

        inventory_item_count = d.pop("inventory_item_count")

        def _parse_name(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        name = _parse_name(d.pop("name", UNSET))

        def _parse_tenant(data: object) -> Union["NestedTenant", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenant.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenant", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        def _parse_platform(data: object) -> Union["NestedPlatform", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                platform_type_1 = NestedPlatform.from_dict(data)

                return platform_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedPlatform", None, Unset], data)

        platform = _parse_platform(d.pop("platform", UNSET))

        serial = d.pop("serial", UNSET)

        def _parse_asset_tag(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        asset_tag = _parse_asset_tag(d.pop("asset_tag", UNSET))

        def _parse_location(data: object) -> Union["NestedLocation", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                location_type_1 = NestedLocation.from_dict(data)

                return location_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedLocation", None, Unset], data)

        location = _parse_location(d.pop("location", UNSET))

        def _parse_rack(data: object) -> Union["NestedRack", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                rack_type_1 = NestedRack.from_dict(data)

                return rack_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedRack", None, Unset], data)

        rack = _parse_rack(d.pop("rack", UNSET))

        def _parse_position(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        position = _parse_position(d.pop("position", UNSET))

        _face = d.pop("face", UNSET)
        face: Union[Unset, DeviceFace]
        if isinstance(_face, Unset):
            face = UNSET
        else:
            face = DeviceFace.from_dict(_face)

        def _parse_latitude(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        latitude = _parse_latitude(d.pop("latitude", UNSET))

        def _parse_longitude(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        longitude = _parse_longitude(d.pop("longitude", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, DeviceStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = DeviceStatus.from_dict(_status)

        _airflow = d.pop("airflow", UNSET)
        airflow: Union[Unset, DeviceAirflow]
        if isinstance(_airflow, Unset):
            airflow = UNSET
        else:
            airflow = DeviceAirflow.from_dict(_airflow)

        def _parse_primary_ip4(data: object) -> Union["NestedIPAddress", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                primary_ip4_type_1 = NestedIPAddress.from_dict(data)

                return primary_ip4_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedIPAddress", None, Unset], data)

        primary_ip4 = _parse_primary_ip4(d.pop("primary_ip4", UNSET))

        def _parse_primary_ip6(data: object) -> Union["NestedIPAddress", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                primary_ip6_type_1 = NestedIPAddress.from_dict(data)

                return primary_ip6_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedIPAddress", None, Unset], data)

        primary_ip6 = _parse_primary_ip6(d.pop("primary_ip6", UNSET))

        def _parse_oob_ip(data: object) -> Union["NestedIPAddress", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                oob_ip_type_1 = NestedIPAddress.from_dict(data)

                return oob_ip_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedIPAddress", None, Unset], data)

        oob_ip = _parse_oob_ip(d.pop("oob_ip", UNSET))

        def _parse_cluster(data: object) -> Union["NestedCluster", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                cluster_type_1 = NestedCluster.from_dict(data)

                return cluster_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedCluster", None, Unset], data)

        cluster = _parse_cluster(d.pop("cluster", UNSET))

        def _parse_virtual_chassis(
            data: object,
        ) -> Union["NestedVirtualChassis", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                virtual_chassis_type_1 = NestedVirtualChassis.from_dict(data)

                return virtual_chassis_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVirtualChassis", None, Unset], data)

        virtual_chassis = _parse_virtual_chassis(d.pop("virtual_chassis", UNSET))

        def _parse_vc_position(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vc_position = _parse_vc_position(d.pop("vc_position", UNSET))

        def _parse_vc_priority(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        vc_priority = _parse_vc_priority(d.pop("vc_priority", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        def _parse_config_template(
            data: object,
        ) -> Union["NestedConfigTemplate", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                config_template_type_1 = NestedConfigTemplate.from_dict(data)

                return config_template_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedConfigTemplate", None, Unset], data)

        config_template = _parse_config_template(d.pop("config_template", UNSET))

        local_context_data = d.pop("local_context_data", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, DeviceCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = DeviceCustomFields.from_dict(_custom_fields)

        device = cls(
            id=id,
            url=url,
            display=display,
            device_type=device_type,
            role=role,
            device_role=device_role,
            site=site,
            parent_device=parent_device,
            primary_ip=primary_ip,
            created=created,
            last_updated=last_updated,
            console_port_count=console_port_count,
            console_server_port_count=console_server_port_count,
            power_port_count=power_port_count,
            power_outlet_count=power_outlet_count,
            interface_count=interface_count,
            front_port_count=front_port_count,
            rear_port_count=rear_port_count,
            device_bay_count=device_bay_count,
            module_bay_count=module_bay_count,
            inventory_item_count=inventory_item_count,
            name=name,
            tenant=tenant,
            platform=platform,
            serial=serial,
            asset_tag=asset_tag,
            location=location,
            rack=rack,
            position=position,
            face=face,
            latitude=latitude,
            longitude=longitude,
            status=status,
            airflow=airflow,
            primary_ip4=primary_ip4,
            primary_ip6=primary_ip6,
            oob_ip=oob_ip,
            cluster=cluster,
            virtual_chassis=virtual_chassis,
            vc_position=vc_position,
            vc_priority=vc_priority,
            description=description,
            comments=comments,
            config_template=config_template,
            local_context_data=local_context_data,
            tags=tags,
            custom_fields=custom_fields,
        )

        device.additional_properties = d
        return device

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
