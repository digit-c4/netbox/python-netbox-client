from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.virtual_machine_with_config_context_request_status import (
    VirtualMachineWithConfigContextRequestStatus,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_cluster_request import NestedClusterRequest
    from ..models.nested_device_request import NestedDeviceRequest
    from ..models.nested_device_role_request import NestedDeviceRoleRequest
    from ..models.nested_ip_address_request import NestedIPAddressRequest
    from ..models.nested_platform_request import NestedPlatformRequest
    from ..models.nested_site_request import NestedSiteRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_tenant_request import NestedTenantRequest
    from ..models.virtual_machine_with_config_context_request_custom_fields import (
        VirtualMachineWithConfigContextRequestCustomFields,
    )


T = TypeVar("T", bound="VirtualMachineWithConfigContextRequest")


@_attrs_define
class VirtualMachineWithConfigContextRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        status (Union[Unset, VirtualMachineWithConfigContextRequestStatus]): * `offline` - Offline
            * `active` - Active
            * `planned` - Planned
            * `staged` - Staged
            * `failed` - Failed
            * `decommissioning` - Decommissioning
        site (Union['NestedSiteRequest', None, Unset]):
        cluster (Union['NestedClusterRequest', None, Unset]):
        device (Union['NestedDeviceRequest', None, Unset]):
        role (Union['NestedDeviceRoleRequest', None, Unset]):
        tenant (Union['NestedTenantRequest', None, Unset]):
        platform (Union['NestedPlatformRequest', None, Unset]):
        primary_ip4 (Union['NestedIPAddressRequest', None, Unset]):
        primary_ip6 (Union['NestedIPAddressRequest', None, Unset]):
        vcpus (Union[None, Unset, float]):
        memory (Union[None, Unset, int]):
        disk (Union[None, Unset, int]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        local_context_data (Union[Unset, Any]): Local config context data takes precedence over source contexts in the
            final rendered config context
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, VirtualMachineWithConfigContextRequestCustomFields]):
    """

    name: str
    status: Union[Unset, VirtualMachineWithConfigContextRequestStatus] = UNSET
    site: Union["NestedSiteRequest", None, Unset] = UNSET
    cluster: Union["NestedClusterRequest", None, Unset] = UNSET
    device: Union["NestedDeviceRequest", None, Unset] = UNSET
    role: Union["NestedDeviceRoleRequest", None, Unset] = UNSET
    tenant: Union["NestedTenantRequest", None, Unset] = UNSET
    platform: Union["NestedPlatformRequest", None, Unset] = UNSET
    primary_ip4: Union["NestedIPAddressRequest", None, Unset] = UNSET
    primary_ip6: Union["NestedIPAddressRequest", None, Unset] = UNSET
    vcpus: Union[None, Unset, float] = UNSET
    memory: Union[None, Unset, int] = UNSET
    disk: Union[None, Unset, int] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    local_context_data: Union[Unset, Any] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[
        Unset, "VirtualMachineWithConfigContextRequestCustomFields"
    ] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_cluster_request import NestedClusterRequest
        from ..models.nested_device_request import NestedDeviceRequest
        from ..models.nested_device_role_request import NestedDeviceRoleRequest
        from ..models.nested_ip_address_request import NestedIPAddressRequest
        from ..models.nested_platform_request import NestedPlatformRequest
        from ..models.nested_site_request import NestedSiteRequest
        from ..models.nested_tenant_request import NestedTenantRequest

        name = self.name

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        site: Union[Dict[str, Any], None, Unset]
        if isinstance(self.site, Unset):
            site = UNSET
        elif isinstance(self.site, NestedSiteRequest):
            site = self.site.to_dict()
        else:
            site = self.site

        cluster: Union[Dict[str, Any], None, Unset]
        if isinstance(self.cluster, Unset):
            cluster = UNSET
        elif isinstance(self.cluster, NestedClusterRequest):
            cluster = self.cluster.to_dict()
        else:
            cluster = self.cluster

        device: Union[Dict[str, Any], None, Unset]
        if isinstance(self.device, Unset):
            device = UNSET
        elif isinstance(self.device, NestedDeviceRequest):
            device = self.device.to_dict()
        else:
            device = self.device

        role: Union[Dict[str, Any], None, Unset]
        if isinstance(self.role, Unset):
            role = UNSET
        elif isinstance(self.role, NestedDeviceRoleRequest):
            role = self.role.to_dict()
        else:
            role = self.role

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenantRequest):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        platform: Union[Dict[str, Any], None, Unset]
        if isinstance(self.platform, Unset):
            platform = UNSET
        elif isinstance(self.platform, NestedPlatformRequest):
            platform = self.platform.to_dict()
        else:
            platform = self.platform

        primary_ip4: Union[Dict[str, Any], None, Unset]
        if isinstance(self.primary_ip4, Unset):
            primary_ip4 = UNSET
        elif isinstance(self.primary_ip4, NestedIPAddressRequest):
            primary_ip4 = self.primary_ip4.to_dict()
        else:
            primary_ip4 = self.primary_ip4

        primary_ip6: Union[Dict[str, Any], None, Unset]
        if isinstance(self.primary_ip6, Unset):
            primary_ip6 = UNSET
        elif isinstance(self.primary_ip6, NestedIPAddressRequest):
            primary_ip6 = self.primary_ip6.to_dict()
        else:
            primary_ip6 = self.primary_ip6

        vcpus: Union[None, Unset, float]
        if isinstance(self.vcpus, Unset):
            vcpus = UNSET
        else:
            vcpus = self.vcpus

        memory: Union[None, Unset, int]
        if isinstance(self.memory, Unset):
            memory = UNSET
        else:
            memory = self.memory

        disk: Union[None, Unset, int]
        if isinstance(self.disk, Unset):
            disk = UNSET
        else:
            disk = self.disk

        description = self.description

        comments = self.comments

        local_context_data = self.local_context_data

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
            }
        )
        if status is not UNSET:
            field_dict["status"] = status
        if site is not UNSET:
            field_dict["site"] = site
        if cluster is not UNSET:
            field_dict["cluster"] = cluster
        if device is not UNSET:
            field_dict["device"] = device
        if role is not UNSET:
            field_dict["role"] = role
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if platform is not UNSET:
            field_dict["platform"] = platform
        if primary_ip4 is not UNSET:
            field_dict["primary_ip4"] = primary_ip4
        if primary_ip6 is not UNSET:
            field_dict["primary_ip6"] = primary_ip6
        if vcpus is not UNSET:
            field_dict["vcpus"] = vcpus
        if memory is not UNSET:
            field_dict["memory"] = memory
        if disk is not UNSET:
            field_dict["disk"] = disk
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if local_context_data is not UNSET:
            field_dict["local_context_data"] = local_context_data
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_cluster_request import NestedClusterRequest
        from ..models.nested_device_request import NestedDeviceRequest
        from ..models.nested_device_role_request import NestedDeviceRoleRequest
        from ..models.nested_ip_address_request import NestedIPAddressRequest
        from ..models.nested_platform_request import NestedPlatformRequest
        from ..models.nested_site_request import NestedSiteRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_tenant_request import NestedTenantRequest
        from ..models.virtual_machine_with_config_context_request_custom_fields import (
            VirtualMachineWithConfigContextRequestCustomFields,
        )

        d = src_dict.copy()
        name = d.pop("name")

        _status = d.pop("status", UNSET)
        status: Union[Unset, VirtualMachineWithConfigContextRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = VirtualMachineWithConfigContextRequestStatus(_status)

        def _parse_site(data: object) -> Union["NestedSiteRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                site_type_1 = NestedSiteRequest.from_dict(data)

                return site_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedSiteRequest", None, Unset], data)

        site = _parse_site(d.pop("site", UNSET))

        def _parse_cluster(data: object) -> Union["NestedClusterRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                cluster_type_1 = NestedClusterRequest.from_dict(data)

                return cluster_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedClusterRequest", None, Unset], data)

        cluster = _parse_cluster(d.pop("cluster", UNSET))

        def _parse_device(data: object) -> Union["NestedDeviceRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                device_type_1 = NestedDeviceRequest.from_dict(data)

                return device_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedDeviceRequest", None, Unset], data)

        device = _parse_device(d.pop("device", UNSET))

        def _parse_role(data: object) -> Union["NestedDeviceRoleRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                role_type_1 = NestedDeviceRoleRequest.from_dict(data)

                return role_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedDeviceRoleRequest", None, Unset], data)

        role = _parse_role(d.pop("role", UNSET))

        def _parse_tenant(data: object) -> Union["NestedTenantRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenantRequest.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenantRequest", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        def _parse_platform(
            data: object,
        ) -> Union["NestedPlatformRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                platform_type_1 = NestedPlatformRequest.from_dict(data)

                return platform_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedPlatformRequest", None, Unset], data)

        platform = _parse_platform(d.pop("platform", UNSET))

        def _parse_primary_ip4(
            data: object,
        ) -> Union["NestedIPAddressRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                primary_ip4_type_1 = NestedIPAddressRequest.from_dict(data)

                return primary_ip4_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedIPAddressRequest", None, Unset], data)

        primary_ip4 = _parse_primary_ip4(d.pop("primary_ip4", UNSET))

        def _parse_primary_ip6(
            data: object,
        ) -> Union["NestedIPAddressRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                primary_ip6_type_1 = NestedIPAddressRequest.from_dict(data)

                return primary_ip6_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedIPAddressRequest", None, Unset], data)

        primary_ip6 = _parse_primary_ip6(d.pop("primary_ip6", UNSET))

        def _parse_vcpus(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        vcpus = _parse_vcpus(d.pop("vcpus", UNSET))

        def _parse_memory(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        memory = _parse_memory(d.pop("memory", UNSET))

        def _parse_disk(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        disk = _parse_disk(d.pop("disk", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        local_context_data = d.pop("local_context_data", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, VirtualMachineWithConfigContextRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = (
                VirtualMachineWithConfigContextRequestCustomFields.from_dict(
                    _custom_fields
                )
            )

        virtual_machine_with_config_context_request = cls(
            name=name,
            status=status,
            site=site,
            cluster=cluster,
            device=device,
            role=role,
            tenant=tenant,
            platform=platform,
            primary_ip4=primary_ip4,
            primary_ip6=primary_ip6,
            vcpus=vcpus,
            memory=memory,
            disk=disk,
            description=description,
            comments=comments,
            local_context_data=local_context_data,
            tags=tags,
            custom_fields=custom_fields,
        )

        virtual_machine_with_config_context_request.additional_properties = d
        return virtual_machine_with_config_context_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
