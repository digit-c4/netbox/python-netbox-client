from enum import Enum


class CustomFieldUiVisibilityLabel(str, Enum):
    HIDDEN = "Hidden"
    HIDDEN_IF_UNSET = "Hidden (if unset)"
    READWRITE = "Read/write"
    READ_ONLY = "Read-only"

    def __str__(self) -> str:
        return str(self.value)
