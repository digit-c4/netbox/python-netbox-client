from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.cluster_request_status import ClusterRequestStatus
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.cluster_request_custom_fields import ClusterRequestCustomFields
    from ..models.nested_cluster_group_request import NestedClusterGroupRequest
    from ..models.nested_cluster_type_request import NestedClusterTypeRequest
    from ..models.nested_site_request import NestedSiteRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.nested_tenant_request import NestedTenantRequest


T = TypeVar("T", bound="ClusterRequest")


@_attrs_define
class ClusterRequest:
    """Adds support for custom fields and tags.

    Attributes:
        name (str):
        type (NestedClusterTypeRequest): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        group (Union['NestedClusterGroupRequest', None, Unset]):
        status (Union[Unset, ClusterRequestStatus]): * `planned` - Planned
            * `staging` - Staging
            * `active` - Active
            * `decommissioning` - Decommissioning
            * `offline` - Offline
        tenant (Union['NestedTenantRequest', None, Unset]):
        site (Union['NestedSiteRequest', None, Unset]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, ClusterRequestCustomFields]):
    """

    name: str
    type: "NestedClusterTypeRequest"
    group: Union["NestedClusterGroupRequest", None, Unset] = UNSET
    status: Union[Unset, ClusterRequestStatus] = UNSET
    tenant: Union["NestedTenantRequest", None, Unset] = UNSET
    site: Union["NestedSiteRequest", None, Unset] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "ClusterRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_cluster_group_request import NestedClusterGroupRequest
        from ..models.nested_site_request import NestedSiteRequest
        from ..models.nested_tenant_request import NestedTenantRequest

        name = self.name

        type = self.type.to_dict()

        group: Union[Dict[str, Any], None, Unset]
        if isinstance(self.group, Unset):
            group = UNSET
        elif isinstance(self.group, NestedClusterGroupRequest):
            group = self.group.to_dict()
        else:
            group = self.group

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenantRequest):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        site: Union[Dict[str, Any], None, Unset]
        if isinstance(self.site, Unset):
            site = UNSET
        elif isinstance(self.site, NestedSiteRequest):
            site = self.site.to_dict()
        else:
            site = self.site

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "name": name,
                "type": type,
            }
        )
        if group is not UNSET:
            field_dict["group"] = group
        if status is not UNSET:
            field_dict["status"] = status
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if site is not UNSET:
            field_dict["site"] = site
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.cluster_request_custom_fields import ClusterRequestCustomFields
        from ..models.nested_cluster_group_request import NestedClusterGroupRequest
        from ..models.nested_cluster_type_request import NestedClusterTypeRequest
        from ..models.nested_site_request import NestedSiteRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.nested_tenant_request import NestedTenantRequest

        d = src_dict.copy()
        name = d.pop("name")

        type = NestedClusterTypeRequest.from_dict(d.pop("type"))

        def _parse_group(
            data: object,
        ) -> Union["NestedClusterGroupRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                group_type_1 = NestedClusterGroupRequest.from_dict(data)

                return group_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedClusterGroupRequest", None, Unset], data)

        group = _parse_group(d.pop("group", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, ClusterRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = ClusterRequestStatus(_status)

        def _parse_tenant(data: object) -> Union["NestedTenantRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenantRequest.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenantRequest", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        def _parse_site(data: object) -> Union["NestedSiteRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                site_type_1 = NestedSiteRequest.from_dict(data)

                return site_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedSiteRequest", None, Unset], data)

        site = _parse_site(d.pop("site", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ClusterRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ClusterRequestCustomFields.from_dict(_custom_fields)

        cluster_request = cls(
            name=name,
            type=type,
            group=group,
            status=status,
            tenant=tenant,
            site=site,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        cluster_request.additional_properties = d
        return cluster_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
