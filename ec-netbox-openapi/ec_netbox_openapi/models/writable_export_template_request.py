import json
from typing import Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="WritableExportTemplateRequest")


@_attrs_define
class WritableExportTemplateRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            content_types (List[str]):
            name (str):
            template_code (str): Jinja2 template code. The list of objects being exported is passed as a context variable
                named <code>queryset</code>.
            description (Union[Unset, str]):
            mime_type (Union[Unset, str]): Defaults to <code>text/plain; charset=utf-8</code>
            file_extension (Union[Unset, str]): Extension to append to the rendered filename
            as_attachment (Union[Unset, bool]): Download file as attachment
            data_source (Union[None, Unset, int]): Remote data source
    """

    content_types: List[str]
    name: str
    template_code: str
    description: Union[Unset, str] = UNSET
    mime_type: Union[Unset, str] = UNSET
    file_extension: Union[Unset, str] = UNSET
    as_attachment: Union[Unset, bool] = UNSET
    data_source: Union[None, Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        content_types = self.content_types

        name = self.name

        template_code = self.template_code

        description = self.description

        mime_type = self.mime_type

        file_extension = self.file_extension

        as_attachment = self.as_attachment

        data_source: Union[None, Unset, int]
        if isinstance(self.data_source, Unset):
            data_source = UNSET
        else:
            data_source = self.data_source

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "content_types": content_types,
                "name": name,
                "template_code": template_code,
            }
        )
        if description is not UNSET:
            field_dict["description"] = description
        if mime_type is not UNSET:
            field_dict["mime_type"] = mime_type
        if file_extension is not UNSET:
            field_dict["file_extension"] = file_extension
        if as_attachment is not UNSET:
            field_dict["as_attachment"] = as_attachment
        if data_source is not UNSET:
            field_dict["data_source"] = data_source

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        _temp_content_types = self.content_types
        content_types = (
            None,
            json.dumps(_temp_content_types).encode(),
            "application/json",
        )

        name = (None, str(self.name).encode(), "text/plain")

        template_code = (None, str(self.template_code).encode(), "text/plain")

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        mime_type = (
            self.mime_type
            if isinstance(self.mime_type, Unset)
            else (None, str(self.mime_type).encode(), "text/plain")
        )

        file_extension = (
            self.file_extension
            if isinstance(self.file_extension, Unset)
            else (None, str(self.file_extension).encode(), "text/plain")
        )

        as_attachment = (
            self.as_attachment
            if isinstance(self.as_attachment, Unset)
            else (None, str(self.as_attachment).encode(), "text/plain")
        )

        data_source: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.data_source, Unset):
            data_source = UNSET
        elif isinstance(self.data_source, int):
            data_source = (None, str(self.data_source).encode(), "text/plain")
        else:
            data_source = (None, str(self.data_source).encode(), "text/plain")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update(
            {
                "content_types": content_types,
                "name": name,
                "template_code": template_code,
            }
        )
        if description is not UNSET:
            field_dict["description"] = description
        if mime_type is not UNSET:
            field_dict["mime_type"] = mime_type
        if file_extension is not UNSET:
            field_dict["file_extension"] = file_extension
        if as_attachment is not UNSET:
            field_dict["as_attachment"] = as_attachment
        if data_source is not UNSET:
            field_dict["data_source"] = data_source

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        content_types = cast(List[str], d.pop("content_types"))

        name = d.pop("name")

        template_code = d.pop("template_code")

        description = d.pop("description", UNSET)

        mime_type = d.pop("mime_type", UNSET)

        file_extension = d.pop("file_extension", UNSET)

        as_attachment = d.pop("as_attachment", UNSET)

        def _parse_data_source(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        data_source = _parse_data_source(d.pop("data_source", UNSET))

        writable_export_template_request = cls(
            content_types=content_types,
            name=name,
            template_code=template_code,
            description=description,
            mime_type=mime_type,
            file_extension=file_extension,
            as_attachment=as_attachment,
            data_source=data_source,
        )

        writable_export_template_request.additional_properties = d
        return writable_export_template_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
