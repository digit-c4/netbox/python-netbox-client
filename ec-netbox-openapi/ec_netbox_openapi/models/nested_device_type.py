from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar

from attrs import define as _attrs_define
from attrs import field as _attrs_field

if TYPE_CHECKING:
    from ..models.nested_manufacturer import NestedManufacturer


T = TypeVar("T", bound="NestedDeviceType")


@_attrs_define
class NestedDeviceType:
    """Represents an object related through a ForeignKey field. On write, it accepts a primary key (PK) value or a
    dictionary of attributes which can be used to uniquely identify the related object. This class should be
    subclassed to return a full representation of the related object on read.

        Attributes:
            id (int):
            url (str):
            display (str):
            manufacturer (NestedManufacturer): Represents an object related through a ForeignKey field. On write, it accepts
                a primary key (PK) value or a
                dictionary of attributes which can be used to uniquely identify the related object. This class should be
                subclassed to return a full representation of the related object on read.
            model (str):
            slug (str):
    """

    id: int
    url: str
    display: str
    manufacturer: "NestedManufacturer"
    model: str
    slug: str
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        manufacturer = self.manufacturer.to_dict()

        model = self.model

        slug = self.slug

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "manufacturer": manufacturer,
                "model": model,
                "slug": slug,
            }
        )

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_manufacturer import NestedManufacturer

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        manufacturer = NestedManufacturer.from_dict(d.pop("manufacturer"))

        model = d.pop("model")

        slug = d.pop("slug")

        nested_device_type = cls(
            id=id,
            url=url,
            display=display,
            manufacturer=manufacturer,
            model=model,
            slug=slug,
        )

        nested_device_type.additional_properties = d
        return nested_device_type

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
