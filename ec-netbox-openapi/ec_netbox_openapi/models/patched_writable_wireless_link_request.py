import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.patched_writable_wireless_link_request_authentication_cipher import (
    PatchedWritableWirelessLinkRequestAuthenticationCipher,
)
from ..models.patched_writable_wireless_link_request_authentication_type import (
    PatchedWritableWirelessLinkRequestAuthenticationType,
)
from ..models.patched_writable_wireless_link_request_status import (
    PatchedWritableWirelessLinkRequestStatus,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_writable_wireless_link_request_custom_fields import (
        PatchedWritableWirelessLinkRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedWritableWirelessLinkRequest")


@_attrs_define
class PatchedWritableWirelessLinkRequest:
    """Adds support for custom fields and tags.

    Attributes:
        interface_a (Union[Unset, int]):
        interface_b (Union[Unset, int]):
        ssid (Union[Unset, str]):
        status (Union[Unset, PatchedWritableWirelessLinkRequestStatus]): * `connected` - Connected
            * `planned` - Planned
            * `decommissioning` - Decommissioning
        tenant (Union[None, Unset, int]):
        auth_type (Union[Unset, PatchedWritableWirelessLinkRequestAuthenticationType]): * `open` - Open
            * `wep` - WEP
            * `wpa-personal` - WPA Personal (PSK)
            * `wpa-enterprise` - WPA Enterprise
        auth_cipher (Union[Unset, PatchedWritableWirelessLinkRequestAuthenticationCipher]): * `auto` - Auto
            * `tkip` - TKIP
            * `aes` - AES
        auth_psk (Union[Unset, str]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, PatchedWritableWirelessLinkRequestCustomFields]):
    """

    interface_a: Union[Unset, int] = UNSET
    interface_b: Union[Unset, int] = UNSET
    ssid: Union[Unset, str] = UNSET
    status: Union[Unset, PatchedWritableWirelessLinkRequestStatus] = UNSET
    tenant: Union[None, Unset, int] = UNSET
    auth_type: Union[Unset, PatchedWritableWirelessLinkRequestAuthenticationType] = (
        UNSET
    )
    auth_cipher: Union[
        Unset, PatchedWritableWirelessLinkRequestAuthenticationCipher
    ] = UNSET
    auth_psk: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "PatchedWritableWirelessLinkRequestCustomFields"] = (
        UNSET
    )
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        interface_a = self.interface_a

        interface_b = self.interface_b

        ssid = self.ssid

        status: Union[Unset, str] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.value

        tenant: Union[None, Unset, int]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        else:
            tenant = self.tenant

        auth_type: Union[Unset, str] = UNSET
        if not isinstance(self.auth_type, Unset):
            auth_type = self.auth_type.value

        auth_cipher: Union[Unset, str] = UNSET
        if not isinstance(self.auth_cipher, Unset):
            auth_cipher = self.auth_cipher.value

        auth_psk = self.auth_psk

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if interface_a is not UNSET:
            field_dict["interface_a"] = interface_a
        if interface_b is not UNSET:
            field_dict["interface_b"] = interface_b
        if ssid is not UNSET:
            field_dict["ssid"] = ssid
        if status is not UNSET:
            field_dict["status"] = status
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if auth_type is not UNSET:
            field_dict["auth_type"] = auth_type
        if auth_cipher is not UNSET:
            field_dict["auth_cipher"] = auth_cipher
        if auth_psk is not UNSET:
            field_dict["auth_psk"] = auth_psk
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        interface_a = (
            self.interface_a
            if isinstance(self.interface_a, Unset)
            else (None, str(self.interface_a).encode(), "text/plain")
        )

        interface_b = (
            self.interface_b
            if isinstance(self.interface_b, Unset)
            else (None, str(self.interface_b).encode(), "text/plain")
        )

        ssid = (
            self.ssid
            if isinstance(self.ssid, Unset)
            else (None, str(self.ssid).encode(), "text/plain")
        )

        status: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.status, Unset):
            status = (None, str(self.status.value).encode(), "text/plain")

        tenant: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, int):
            tenant = (None, str(self.tenant).encode(), "text/plain")
        else:
            tenant = (None, str(self.tenant).encode(), "text/plain")

        auth_type: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.auth_type, Unset):
            auth_type = (None, str(self.auth_type.value).encode(), "text/plain")

        auth_cipher: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.auth_cipher, Unset):
            auth_cipher = (None, str(self.auth_cipher.value).encode(), "text/plain")

        auth_psk = (
            self.auth_psk
            if isinstance(self.auth_psk, Unset)
            else (None, str(self.auth_psk).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        comments = (
            self.comments
            if isinstance(self.comments, Unset)
            else (None, str(self.comments).encode(), "text/plain")
        )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if interface_a is not UNSET:
            field_dict["interface_a"] = interface_a
        if interface_b is not UNSET:
            field_dict["interface_b"] = interface_b
        if ssid is not UNSET:
            field_dict["ssid"] = ssid
        if status is not UNSET:
            field_dict["status"] = status
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if auth_type is not UNSET:
            field_dict["auth_type"] = auth_type
        if auth_cipher is not UNSET:
            field_dict["auth_cipher"] = auth_cipher
        if auth_psk is not UNSET:
            field_dict["auth_psk"] = auth_psk
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_writable_wireless_link_request_custom_fields import (
            PatchedWritableWirelessLinkRequestCustomFields,
        )

        d = src_dict.copy()
        interface_a = d.pop("interface_a", UNSET)

        interface_b = d.pop("interface_b", UNSET)

        ssid = d.pop("ssid", UNSET)

        _status = d.pop("status", UNSET)
        status: Union[Unset, PatchedWritableWirelessLinkRequestStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = PatchedWritableWirelessLinkRequestStatus(_status)

        def _parse_tenant(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        _auth_type = d.pop("auth_type", UNSET)
        auth_type: Union[Unset, PatchedWritableWirelessLinkRequestAuthenticationType]
        if isinstance(_auth_type, Unset):
            auth_type = UNSET
        else:
            auth_type = PatchedWritableWirelessLinkRequestAuthenticationType(_auth_type)

        _auth_cipher = d.pop("auth_cipher", UNSET)
        auth_cipher: Union[
            Unset, PatchedWritableWirelessLinkRequestAuthenticationCipher
        ]
        if isinstance(_auth_cipher, Unset):
            auth_cipher = UNSET
        else:
            auth_cipher = PatchedWritableWirelessLinkRequestAuthenticationCipher(
                _auth_cipher
            )

        auth_psk = d.pop("auth_psk", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedWritableWirelessLinkRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedWritableWirelessLinkRequestCustomFields.from_dict(
                _custom_fields
            )

        patched_writable_wireless_link_request = cls(
            interface_a=interface_a,
            interface_b=interface_b,
            ssid=ssid,
            status=status,
            tenant=tenant,
            auth_type=auth_type,
            auth_cipher=auth_cipher,
            auth_psk=auth_psk,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        patched_writable_wireless_link_request.additional_properties = d
        return patched_writable_wireless_link_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
