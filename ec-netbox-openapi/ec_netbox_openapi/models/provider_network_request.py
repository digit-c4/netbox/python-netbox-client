from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_provider_request import NestedProviderRequest
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.provider_network_request_custom_fields import (
        ProviderNetworkRequestCustomFields,
    )


T = TypeVar("T", bound="ProviderNetworkRequest")


@_attrs_define
class ProviderNetworkRequest:
    """Adds support for custom fields and tags.

    Attributes:
        provider (NestedProviderRequest): Represents an object related through a ForeignKey field. On write, it accepts
            a primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (str):
        service_id (Union[Unset, str]):
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, ProviderNetworkRequestCustomFields]):
    """

    provider: "NestedProviderRequest"
    name: str
    service_id: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "ProviderNetworkRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        provider = self.provider.to_dict()

        name = self.name

        service_id = self.service_id

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "provider": provider,
                "name": name,
            }
        )
        if service_id is not UNSET:
            field_dict["service_id"] = service_id
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_provider_request import NestedProviderRequest
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.provider_network_request_custom_fields import (
            ProviderNetworkRequestCustomFields,
        )

        d = src_dict.copy()
        provider = NestedProviderRequest.from_dict(d.pop("provider"))

        name = d.pop("name")

        service_id = d.pop("service_id", UNSET)

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ProviderNetworkRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ProviderNetworkRequestCustomFields.from_dict(_custom_fields)

        provider_network_request = cls(
            provider=provider,
            name=name,
            service_id=service_id,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        provider_network_request.additional_properties = d
        return provider_network_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
