import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_cable import NestedCable
    from ..models.nested_power_panel import NestedPowerPanel
    from ..models.nested_rack import NestedRack
    from ..models.nested_tag import NestedTag
    from ..models.nested_tenant import NestedTenant
    from ..models.power_feed_custom_fields import PowerFeedCustomFields
    from ..models.power_feed_phase import PowerFeedPhase
    from ..models.power_feed_status import PowerFeedStatus
    from ..models.power_feed_supply import PowerFeedSupply
    from ..models.power_feed_type import PowerFeedType


T = TypeVar("T", bound="PowerFeed")


@_attrs_define
class PowerFeed:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        power_panel (NestedPowerPanel): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (str):
        cable (Union['NestedCable', None]):
        cable_end (str):
        link_peers (List[Any]):
        link_peers_type (str): Return the type of the peer link terminations, or None.
        connected_endpoints (List[Any]):
        connected_endpoints_type (str):
        connected_endpoints_reachable (bool):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        field_occupied (bool):
        rack (Union['NestedRack', None, Unset]):
        status (Union[Unset, PowerFeedStatus]):
        type (Union[Unset, PowerFeedType]):
        supply (Union[Unset, PowerFeedSupply]):
        phase (Union[Unset, PowerFeedPhase]):
        voltage (Union[Unset, int]):
        amperage (Union[Unset, int]):
        max_utilization (Union[Unset, int]): Maximum permissible draw (percentage)
        mark_connected (Union[Unset, bool]): Treat as if a cable is connected
        description (Union[Unset, str]):
        tenant (Union['NestedTenant', None, Unset]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, PowerFeedCustomFields]):
    """

    id: int
    url: str
    display: str
    power_panel: "NestedPowerPanel"
    name: str
    cable: Union["NestedCable", None]
    cable_end: str
    link_peers: List[Any]
    link_peers_type: str
    connected_endpoints: List[Any]
    connected_endpoints_type: str
    connected_endpoints_reachable: bool
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    field_occupied: bool
    rack: Union["NestedRack", None, Unset] = UNSET
    status: Union[Unset, "PowerFeedStatus"] = UNSET
    type: Union[Unset, "PowerFeedType"] = UNSET
    supply: Union[Unset, "PowerFeedSupply"] = UNSET
    phase: Union[Unset, "PowerFeedPhase"] = UNSET
    voltage: Union[Unset, int] = UNSET
    amperage: Union[Unset, int] = UNSET
    max_utilization: Union[Unset, int] = UNSET
    mark_connected: Union[Unset, bool] = UNSET
    description: Union[Unset, str] = UNSET
    tenant: Union["NestedTenant", None, Unset] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "PowerFeedCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_cable import NestedCable
        from ..models.nested_rack import NestedRack
        from ..models.nested_tenant import NestedTenant

        id = self.id

        url = self.url

        display = self.display

        power_panel = self.power_panel.to_dict()

        name = self.name

        cable: Union[Dict[str, Any], None]
        if isinstance(self.cable, NestedCable):
            cable = self.cable.to_dict()
        else:
            cable = self.cable

        cable_end = self.cable_end

        link_peers = self.link_peers

        link_peers_type = self.link_peers_type

        connected_endpoints = self.connected_endpoints

        connected_endpoints_type = self.connected_endpoints_type

        connected_endpoints_reachable = self.connected_endpoints_reachable

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        field_occupied = self.field_occupied

        rack: Union[Dict[str, Any], None, Unset]
        if isinstance(self.rack, Unset):
            rack = UNSET
        elif isinstance(self.rack, NestedRack):
            rack = self.rack.to_dict()
        else:
            rack = self.rack

        status: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.status, Unset):
            status = self.status.to_dict()

        type: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.type, Unset):
            type = self.type.to_dict()

        supply: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.supply, Unset):
            supply = self.supply.to_dict()

        phase: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.phase, Unset):
            phase = self.phase.to_dict()

        voltage = self.voltage

        amperage = self.amperage

        max_utilization = self.max_utilization

        mark_connected = self.mark_connected

        description = self.description

        tenant: Union[Dict[str, Any], None, Unset]
        if isinstance(self.tenant, Unset):
            tenant = UNSET
        elif isinstance(self.tenant, NestedTenant):
            tenant = self.tenant.to_dict()
        else:
            tenant = self.tenant

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "power_panel": power_panel,
                "name": name,
                "cable": cable,
                "cable_end": cable_end,
                "link_peers": link_peers,
                "link_peers_type": link_peers_type,
                "connected_endpoints": connected_endpoints,
                "connected_endpoints_type": connected_endpoints_type,
                "connected_endpoints_reachable": connected_endpoints_reachable,
                "created": created,
                "last_updated": last_updated,
                "_occupied": field_occupied,
            }
        )
        if rack is not UNSET:
            field_dict["rack"] = rack
        if status is not UNSET:
            field_dict["status"] = status
        if type is not UNSET:
            field_dict["type"] = type
        if supply is not UNSET:
            field_dict["supply"] = supply
        if phase is not UNSET:
            field_dict["phase"] = phase
        if voltage is not UNSET:
            field_dict["voltage"] = voltage
        if amperage is not UNSET:
            field_dict["amperage"] = amperage
        if max_utilization is not UNSET:
            field_dict["max_utilization"] = max_utilization
        if mark_connected is not UNSET:
            field_dict["mark_connected"] = mark_connected
        if description is not UNSET:
            field_dict["description"] = description
        if tenant is not UNSET:
            field_dict["tenant"] = tenant
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_cable import NestedCable
        from ..models.nested_power_panel import NestedPowerPanel
        from ..models.nested_rack import NestedRack
        from ..models.nested_tag import NestedTag
        from ..models.nested_tenant import NestedTenant
        from ..models.power_feed_custom_fields import PowerFeedCustomFields
        from ..models.power_feed_phase import PowerFeedPhase
        from ..models.power_feed_status import PowerFeedStatus
        from ..models.power_feed_supply import PowerFeedSupply
        from ..models.power_feed_type import PowerFeedType

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        power_panel = NestedPowerPanel.from_dict(d.pop("power_panel"))

        name = d.pop("name")

        def _parse_cable(data: object) -> Union["NestedCable", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                cable_type_1 = NestedCable.from_dict(data)

                return cable_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedCable", None], data)

        cable = _parse_cable(d.pop("cable"))

        cable_end = d.pop("cable_end")

        link_peers = cast(List[Any], d.pop("link_peers"))

        link_peers_type = d.pop("link_peers_type")

        connected_endpoints = cast(List[Any], d.pop("connected_endpoints"))

        connected_endpoints_type = d.pop("connected_endpoints_type")

        connected_endpoints_reachable = d.pop("connected_endpoints_reachable")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        field_occupied = d.pop("_occupied")

        def _parse_rack(data: object) -> Union["NestedRack", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                rack_type_1 = NestedRack.from_dict(data)

                return rack_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedRack", None, Unset], data)

        rack = _parse_rack(d.pop("rack", UNSET))

        _status = d.pop("status", UNSET)
        status: Union[Unset, PowerFeedStatus]
        if isinstance(_status, Unset):
            status = UNSET
        else:
            status = PowerFeedStatus.from_dict(_status)

        _type = d.pop("type", UNSET)
        type: Union[Unset, PowerFeedType]
        if isinstance(_type, Unset):
            type = UNSET
        else:
            type = PowerFeedType.from_dict(_type)

        _supply = d.pop("supply", UNSET)
        supply: Union[Unset, PowerFeedSupply]
        if isinstance(_supply, Unset):
            supply = UNSET
        else:
            supply = PowerFeedSupply.from_dict(_supply)

        _phase = d.pop("phase", UNSET)
        phase: Union[Unset, PowerFeedPhase]
        if isinstance(_phase, Unset):
            phase = UNSET
        else:
            phase = PowerFeedPhase.from_dict(_phase)

        voltage = d.pop("voltage", UNSET)

        amperage = d.pop("amperage", UNSET)

        max_utilization = d.pop("max_utilization", UNSET)

        mark_connected = d.pop("mark_connected", UNSET)

        description = d.pop("description", UNSET)

        def _parse_tenant(data: object) -> Union["NestedTenant", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                tenant_type_1 = NestedTenant.from_dict(data)

                return tenant_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedTenant", None, Unset], data)

        tenant = _parse_tenant(d.pop("tenant", UNSET))

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PowerFeedCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PowerFeedCustomFields.from_dict(_custom_fields)

        power_feed = cls(
            id=id,
            url=url,
            display=display,
            power_panel=power_panel,
            name=name,
            cable=cable,
            cable_end=cable_end,
            link_peers=link_peers,
            link_peers_type=link_peers_type,
            connected_endpoints=connected_endpoints,
            connected_endpoints_type=connected_endpoints_type,
            connected_endpoints_reachable=connected_endpoints_reachable,
            created=created,
            last_updated=last_updated,
            field_occupied=field_occupied,
            rack=rack,
            status=status,
            type=type,
            supply=supply,
            phase=phase,
            voltage=voltage,
            amperage=amperage,
            max_utilization=max_utilization,
            mark_connected=mark_connected,
            description=description,
            tenant=tenant,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        power_feed.additional_properties = d
        return power_feed

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
