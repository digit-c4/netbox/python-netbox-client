import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.cache_config_list_extensions_item import CacheConfigListExtensionsItem
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_mapping import NestedMapping


T = TypeVar("T", bound="CacheConfig")


@_attrs_define
class CacheConfig:
    """Cache Configuration Serializer class

    Attributes:
        id (int):
        url (str):
        mapping (NestedMapping): Nested Mapping Serializer class
        list_extensions (List[CacheConfigListExtensionsItem]):
        last_updated (datetime.datetime):
        created (datetime.datetime):
        ttl (Union[Unset, int]):
        max_size_limit (Union[Unset, int]):
    """

    id: int
    url: str
    mapping: "NestedMapping"
    list_extensions: List[CacheConfigListExtensionsItem]
    last_updated: datetime.datetime
    created: datetime.datetime
    ttl: Union[Unset, int] = UNSET
    max_size_limit: Union[Unset, int] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        mapping = self.mapping.to_dict()

        list_extensions = []
        for list_extensions_item_data in self.list_extensions:
            list_extensions_item = list_extensions_item_data.value
            list_extensions.append(list_extensions_item)

        last_updated = self.last_updated.isoformat()

        created = self.created.isoformat()

        ttl = self.ttl

        max_size_limit = self.max_size_limit

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "mapping": mapping,
                "list_extensions": list_extensions,
                "last_updated": last_updated,
                "created": created,
            }
        )
        if ttl is not UNSET:
            field_dict["ttl"] = ttl
        if max_size_limit is not UNSET:
            field_dict["max_size_limit"] = max_size_limit

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_mapping import NestedMapping

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        mapping = NestedMapping.from_dict(d.pop("mapping"))

        list_extensions = []
        _list_extensions = d.pop("list_extensions")
        for list_extensions_item_data in _list_extensions:
            list_extensions_item = CacheConfigListExtensionsItem(
                list_extensions_item_data
            )

            list_extensions.append(list_extensions_item)

        last_updated = isoparse(d.pop("last_updated"))

        created = isoparse(d.pop("created"))

        ttl = d.pop("ttl", UNSET)

        max_size_limit = d.pop("max_size_limit", UNSET)

        cache_config = cls(
            id=id,
            url=url,
            mapping=mapping,
            list_extensions=list_extensions,
            last_updated=last_updated,
            created=created,
            ttl=ttl,
            max_size_limit=max_size_limit,
        )

        cache_config.additional_properties = d
        return cache_config

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
