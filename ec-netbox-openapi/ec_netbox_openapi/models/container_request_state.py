from enum import Enum


class ContainerRequestState(str, Enum):
    CREATED = "created"
    DEAD = "dead"
    EXITED = "exited"
    NONE = "none"
    PAUSED = "paused"
    RESTARTING = "restarting"
    RUNNING = "running"

    def __str__(self) -> str:
        return str(self.value)
