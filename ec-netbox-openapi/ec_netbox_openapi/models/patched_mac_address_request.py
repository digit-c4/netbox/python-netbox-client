import datetime
from typing import Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..models.patched_mac_address_request_status_type_1 import (
    PatchedMacAddressRequestStatusType1,
)
from ..models.patched_mac_address_request_status_type_2_type_1 import (
    PatchedMacAddressRequestStatusType2Type1,
)
from ..models.patched_mac_address_request_status_type_3_type_1 import (
    PatchedMacAddressRequestStatusType3Type1,
)
from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedMacAddressRequest")


@_attrs_define
class PatchedMacAddressRequest:
    """
    Attributes:
        mac_address (Union[Unset, str]):
        status (Union[None, PatchedMacAddressRequestStatusType1, PatchedMacAddressRequestStatusType2Type1,
            PatchedMacAddressRequestStatusType3Type1, Unset]): * `active` - Active
            * `inactive` - Inactive
            * `quarantined` - Quarantined Default: PatchedMacAddressRequestStatusType1.INACTIVE.
        end_date (Union[None, Unset, datetime.date]):
        description (Union[None, Unset, str]):
        assigned_object_id (Union[Unset, int]):
        assigned_object_type (Union[Unset, str]):
    """

    mac_address: Union[Unset, str] = UNSET
    status: Union[
        None,
        PatchedMacAddressRequestStatusType1,
        PatchedMacAddressRequestStatusType2Type1,
        PatchedMacAddressRequestStatusType3Type1,
        Unset,
    ] = PatchedMacAddressRequestStatusType1.INACTIVE
    end_date: Union[None, Unset, datetime.date] = UNSET
    description: Union[None, Unset, str] = UNSET
    assigned_object_id: Union[Unset, int] = UNSET
    assigned_object_type: Union[Unset, str] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        mac_address = self.mac_address

        status: Union[None, Unset, str]
        if isinstance(self.status, Unset):
            status = UNSET
        elif isinstance(self.status, PatchedMacAddressRequestStatusType1):
            status = self.status.value
        elif isinstance(self.status, PatchedMacAddressRequestStatusType2Type1):
            status = self.status.value
        elif isinstance(self.status, PatchedMacAddressRequestStatusType3Type1):
            status = self.status.value
        else:
            status = self.status

        end_date: Union[None, Unset, str]
        if isinstance(self.end_date, Unset):
            end_date = UNSET
        elif isinstance(self.end_date, datetime.date):
            end_date = self.end_date.isoformat()
        else:
            end_date = self.end_date

        description: Union[None, Unset, str]
        if isinstance(self.description, Unset):
            description = UNSET
        else:
            description = self.description

        assigned_object_id = self.assigned_object_id

        assigned_object_type = self.assigned_object_type

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if mac_address is not UNSET:
            field_dict["mac_address"] = mac_address
        if status is not UNSET:
            field_dict["status"] = status
        if end_date is not UNSET:
            field_dict["end_date"] = end_date
        if description is not UNSET:
            field_dict["description"] = description
        if assigned_object_id is not UNSET:
            field_dict["assigned_object_id"] = assigned_object_id
        if assigned_object_type is not UNSET:
            field_dict["assigned_object_type"] = assigned_object_type

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        mac_address = (
            self.mac_address
            if isinstance(self.mac_address, Unset)
            else (None, str(self.mac_address).encode(), "text/plain")
        )

        status: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.status, Unset):
            status = UNSET
        elif isinstance(self.status, None):
            status = (None, str(self.status).encode(), "text/plain")
        elif isinstance(self.status, PatchedMacAddressRequestStatusType1):
            status = (None, str(self.status.value).encode(), "text/plain")
        elif isinstance(self.status, None):
            status = (None, str(self.status).encode(), "text/plain")
        elif isinstance(self.status, PatchedMacAddressRequestStatusType2Type1):
            status = (None, str(self.status.value).encode(), "text/plain")
        elif isinstance(self.status, None):
            status = (None, str(self.status).encode(), "text/plain")
        else:
            status = (None, str(self.status.value).encode(), "text/plain")

        end_date: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.end_date, Unset):
            end_date = UNSET
        elif isinstance(self.end_date, datetime.date):
            end_date = self.end_date.isoformat().encode()
        else:
            end_date = (None, str(self.end_date).encode(), "text/plain")

        description: Union[Tuple[None, bytes, str], Unset]

        if isinstance(self.description, Unset):
            description = UNSET
        elif isinstance(self.description, str):
            description = (None, str(self.description).encode(), "text/plain")
        else:
            description = (None, str(self.description).encode(), "text/plain")

        assigned_object_id = (
            self.assigned_object_id
            if isinstance(self.assigned_object_id, Unset)
            else (None, str(self.assigned_object_id).encode(), "text/plain")
        )

        assigned_object_type = (
            self.assigned_object_type
            if isinstance(self.assigned_object_type, Unset)
            else (None, str(self.assigned_object_type).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if mac_address is not UNSET:
            field_dict["mac_address"] = mac_address
        if status is not UNSET:
            field_dict["status"] = status
        if end_date is not UNSET:
            field_dict["end_date"] = end_date
        if description is not UNSET:
            field_dict["description"] = description
        if assigned_object_id is not UNSET:
            field_dict["assigned_object_id"] = assigned_object_id
        if assigned_object_type is not UNSET:
            field_dict["assigned_object_type"] = assigned_object_type

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        mac_address = d.pop("mac_address", UNSET)

        def _parse_status(
            data: object,
        ) -> Union[
            None,
            PatchedMacAddressRequestStatusType1,
            PatchedMacAddressRequestStatusType2Type1,
            PatchedMacAddressRequestStatusType3Type1,
            Unset,
        ]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                status_type_1 = PatchedMacAddressRequestStatusType1(data)

                return status_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                status_type_2_type_1 = PatchedMacAddressRequestStatusType2Type1(data)

                return status_type_2_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                status_type_3_type_1 = PatchedMacAddressRequestStatusType3Type1(data)

                return status_type_3_type_1
            except:  # noqa: E722
                pass
            return cast(
                Union[
                    None,
                    PatchedMacAddressRequestStatusType1,
                    PatchedMacAddressRequestStatusType2Type1,
                    PatchedMacAddressRequestStatusType3Type1,
                    Unset,
                ],
                data,
            )

        status = _parse_status(d.pop("status", UNSET))

        def _parse_end_date(data: object) -> Union[None, Unset, datetime.date]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                end_date_type_0 = isoparse(data).date()

                return end_date_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, Unset, datetime.date], data)

        end_date = _parse_end_date(d.pop("end_date", UNSET))

        def _parse_description(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        description = _parse_description(d.pop("description", UNSET))

        assigned_object_id = d.pop("assigned_object_id", UNSET)

        assigned_object_type = d.pop("assigned_object_type", UNSET)

        patched_mac_address_request = cls(
            mac_address=mac_address,
            status=status,
            end_date=end_date,
            description=description,
            assigned_object_id=assigned_object_id,
            assigned_object_type=assigned_object_type,
        )

        patched_mac_address_request.additional_properties = d
        return patched_mac_address_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
