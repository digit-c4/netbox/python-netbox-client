import json
from typing import TYPE_CHECKING, Any, Dict, List, Tuple, Type, TypeVar, Union

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_tag_request import NestedTagRequest
    from ..models.patched_writable_saml_config_request_custom_fields import (
        PatchedWritableSamlConfigRequestCustomFields,
    )


T = TypeVar("T", bound="PatchedWritableSamlConfigRequest")


@_attrs_define
class PatchedWritableSamlConfigRequest:
    """SAML Config Serializer class

    Attributes:
        acs_url (Union[Unset, str]):
        logout_url (Union[Unset, str]):
        force_nauth (Union[Unset, bool]):
        mapping (Union[Unset, int]):
        custom_fields (Union[Unset, PatchedWritableSamlConfigRequestCustomFields]):
        tags (Union[Unset, List['NestedTagRequest']]):
    """

    acs_url: Union[Unset, str] = UNSET
    logout_url: Union[Unset, str] = UNSET
    force_nauth: Union[Unset, bool] = UNSET
    mapping: Union[Unset, int] = UNSET
    custom_fields: Union[Unset, "PatchedWritableSamlConfigRequestCustomFields"] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        acs_url = self.acs_url

        logout_url = self.logout_url

        force_nauth = self.force_nauth

        mapping = self.mapping

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if acs_url is not UNSET:
            field_dict["acs_url"] = acs_url
        if logout_url is not UNSET:
            field_dict["logout_url"] = logout_url
        if force_nauth is not UNSET:
            field_dict["force_nauth"] = force_nauth
        if mapping is not UNSET:
            field_dict["mapping"] = mapping
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        acs_url = (
            self.acs_url
            if isinstance(self.acs_url, Unset)
            else (None, str(self.acs_url).encode(), "text/plain")
        )

        logout_url = (
            self.logout_url
            if isinstance(self.logout_url, Unset)
            else (None, str(self.logout_url).encode(), "text/plain")
        )

        force_nauth = (
            self.force_nauth
            if isinstance(self.force_nauth, Unset)
            else (None, str(self.force_nauth).encode(), "text/plain")
        )

        mapping = (
            self.mapping
            if isinstance(self.mapping, Unset)
            else (None, str(self.mapping).encode(), "text/plain")
        )

        custom_fields: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = (
                None,
                json.dumps(self.custom_fields.to_dict()).encode(),
                "application/json",
            )

        tags: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.tags, Unset):
            _temp_tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                _temp_tags.append(tags_item)
            tags = (None, json.dumps(_temp_tags).encode(), "application/json")

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if acs_url is not UNSET:
            field_dict["acs_url"] = acs_url
        if logout_url is not UNSET:
            field_dict["logout_url"] = logout_url
        if force_nauth is not UNSET:
            field_dict["force_nauth"] = force_nauth
        if mapping is not UNSET:
            field_dict["mapping"] = mapping
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields
        if tags is not UNSET:
            field_dict["tags"] = tags

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_tag_request import NestedTagRequest
        from ..models.patched_writable_saml_config_request_custom_fields import (
            PatchedWritableSamlConfigRequestCustomFields,
        )

        d = src_dict.copy()
        acs_url = d.pop("acs_url", UNSET)

        logout_url = d.pop("logout_url", UNSET)

        force_nauth = d.pop("force_nauth", UNSET)

        mapping = d.pop("mapping", UNSET)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, PatchedWritableSamlConfigRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = PatchedWritableSamlConfigRequestCustomFields.from_dict(
                _custom_fields
            )

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        patched_writable_saml_config_request = cls(
            acs_url=acs_url,
            logout_url=logout_url,
            force_nauth=force_nauth,
            mapping=mapping,
            custom_fields=custom_fields,
            tags=tags,
        )

        patched_writable_saml_config_request.additional_properties = d
        return patched_writable_saml_config_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
