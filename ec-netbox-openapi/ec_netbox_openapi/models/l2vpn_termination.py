import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.l2vpn_termination_custom_fields import L2VPNTerminationCustomFields
    from ..models.nested_l2vpn import NestedL2VPN
    from ..models.nested_tag import NestedTag


T = TypeVar("T", bound="L2VPNTermination")


@_attrs_define
class L2VPNTermination:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        l2vpn (NestedL2VPN): Represents an object related through a ForeignKey field. On write, it accepts a primary key
            (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        assigned_object_type (str):
        assigned_object_id (int):
        assigned_object (Any):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, L2VPNTerminationCustomFields]):
    """

    id: int
    url: str
    display: str
    l2vpn: "NestedL2VPN"
    assigned_object_type: str
    assigned_object_id: int
    assigned_object: Any
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "L2VPNTerminationCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        id = self.id

        url = self.url

        display = self.display

        l2vpn = self.l2vpn.to_dict()

        assigned_object_type = self.assigned_object_type

        assigned_object_id = self.assigned_object_id

        assigned_object = self.assigned_object

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "l2vpn": l2vpn,
                "assigned_object_type": assigned_object_type,
                "assigned_object_id": assigned_object_id,
                "assigned_object": assigned_object,
                "created": created,
                "last_updated": last_updated,
            }
        )
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.l2vpn_termination_custom_fields import (
            L2VPNTerminationCustomFields,
        )
        from ..models.nested_l2vpn import NestedL2VPN
        from ..models.nested_tag import NestedTag

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        l2vpn = NestedL2VPN.from_dict(d.pop("l2vpn"))

        assigned_object_type = d.pop("assigned_object_type")

        assigned_object_id = d.pop("assigned_object_id")

        assigned_object = d.pop("assigned_object")

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, L2VPNTerminationCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = L2VPNTerminationCustomFields.from_dict(_custom_fields)

        l2vpn_termination = cls(
            id=id,
            url=url,
            display=display,
            l2vpn=l2vpn,
            assigned_object_type=assigned_object_type,
            assigned_object_id=assigned_object_id,
            assigned_object=assigned_object,
            created=created,
            last_updated=last_updated,
            tags=tags,
            custom_fields=custom_fields,
        )

        l2vpn_termination.additional_properties = d
        return l2vpn_termination

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
