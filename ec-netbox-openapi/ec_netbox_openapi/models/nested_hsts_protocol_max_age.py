from enum import IntEnum


class NestedHstsProtocolMaxAge(IntEnum):
    VALUE_0 = 0
    VALUE_31536000 = 31536000
    VALUE_63072000 = 63072000

    def __str__(self) -> str:
        return str(self.value)
