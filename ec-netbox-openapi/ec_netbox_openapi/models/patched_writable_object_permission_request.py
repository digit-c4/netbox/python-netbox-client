import json
from typing import Any, Dict, List, Tuple, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

T = TypeVar("T", bound="PatchedWritableObjectPermissionRequest")


@_attrs_define
class PatchedWritableObjectPermissionRequest:
    """Extends the built-in ModelSerializer to enforce calling full_clean() on a copy of the associated instance during
    validation. (DRF does not do this by default; see https://github.com/encode/django-rest-framework/issues/3144)

        Attributes:
            name (Union[Unset, str]):
            description (Union[Unset, str]):
            enabled (Union[Unset, bool]):
            object_types (Union[Unset, List[str]]):
            groups (Union[Unset, List[int]]):
            users (Union[Unset, List[int]]):
            actions (Union[Unset, List[str]]): The list of actions granted by this permission
            constraints (Union[Unset, Any]): Queryset filter matching the applicable objects of the selected type(s)
    """

    name: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    enabled: Union[Unset, bool] = UNSET
    object_types: Union[Unset, List[str]] = UNSET
    groups: Union[Unset, List[int]] = UNSET
    users: Union[Unset, List[int]] = UNSET
    actions: Union[Unset, List[str]] = UNSET
    constraints: Union[Unset, Any] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        name = self.name

        description = self.description

        enabled = self.enabled

        object_types: Union[Unset, List[str]] = UNSET
        if not isinstance(self.object_types, Unset):
            object_types = self.object_types

        groups: Union[Unset, List[int]] = UNSET
        if not isinstance(self.groups, Unset):
            groups = self.groups

        users: Union[Unset, List[int]] = UNSET
        if not isinstance(self.users, Unset):
            users = self.users

        actions: Union[Unset, List[str]] = UNSET
        if not isinstance(self.actions, Unset):
            actions = self.actions

        constraints = self.constraints

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if description is not UNSET:
            field_dict["description"] = description
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if object_types is not UNSET:
            field_dict["object_types"] = object_types
        if groups is not UNSET:
            field_dict["groups"] = groups
        if users is not UNSET:
            field_dict["users"] = users
        if actions is not UNSET:
            field_dict["actions"] = actions
        if constraints is not UNSET:
            field_dict["constraints"] = constraints

        return field_dict

    def to_multipart(self) -> Dict[str, Any]:
        name = (
            self.name
            if isinstance(self.name, Unset)
            else (None, str(self.name).encode(), "text/plain")
        )

        description = (
            self.description
            if isinstance(self.description, Unset)
            else (None, str(self.description).encode(), "text/plain")
        )

        enabled = (
            self.enabled
            if isinstance(self.enabled, Unset)
            else (None, str(self.enabled).encode(), "text/plain")
        )

        object_types: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.object_types, Unset):
            _temp_object_types = self.object_types
            object_types = (
                None,
                json.dumps(_temp_object_types).encode(),
                "application/json",
            )

        groups: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.groups, Unset):
            _temp_groups = self.groups
            groups = (None, json.dumps(_temp_groups).encode(), "application/json")

        users: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.users, Unset):
            _temp_users = self.users
            users = (None, json.dumps(_temp_users).encode(), "application/json")

        actions: Union[Unset, Tuple[None, bytes, str]] = UNSET
        if not isinstance(self.actions, Unset):
            _temp_actions = self.actions
            actions = (None, json.dumps(_temp_actions).encode(), "application/json")

        constraints = (
            self.constraints
            if isinstance(self.constraints, Unset)
            else (None, str(self.constraints).encode(), "text/plain")
        )

        field_dict: Dict[str, Any] = {}
        for prop_name, prop in self.additional_properties.items():
            field_dict[prop_name] = (None, str(prop).encode(), "text/plain")

        field_dict.update({})
        if name is not UNSET:
            field_dict["name"] = name
        if description is not UNSET:
            field_dict["description"] = description
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if object_types is not UNSET:
            field_dict["object_types"] = object_types
        if groups is not UNSET:
            field_dict["groups"] = groups
        if users is not UNSET:
            field_dict["users"] = users
        if actions is not UNSET:
            field_dict["actions"] = actions
        if constraints is not UNSET:
            field_dict["constraints"] = constraints

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        d = src_dict.copy()
        name = d.pop("name", UNSET)

        description = d.pop("description", UNSET)

        enabled = d.pop("enabled", UNSET)

        object_types = cast(List[str], d.pop("object_types", UNSET))

        groups = cast(List[int], d.pop("groups", UNSET))

        users = cast(List[int], d.pop("users", UNSET))

        actions = cast(List[str], d.pop("actions", UNSET))

        constraints = d.pop("constraints", UNSET)

        patched_writable_object_permission_request = cls(
            name=name,
            description=description,
            enabled=enabled,
            object_types=object_types,
            groups=groups,
            users=users,
            actions=actions,
            constraints=constraints,
        )

        patched_writable_object_permission_request.additional_properties = d
        return patched_writable_object_permission_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
