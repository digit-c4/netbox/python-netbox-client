from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    List,
    Type,
    TypeVar,
    Union,
    cast,
)

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.module_bay_nested_module_request import ModuleBayNestedModuleRequest
    from ..models.module_bay_request_custom_fields import ModuleBayRequestCustomFields
    from ..models.nested_device_request import NestedDeviceRequest
    from ..models.nested_tag_request import NestedTagRequest


T = TypeVar("T", bound="ModuleBayRequest")


@_attrs_define
class ModuleBayRequest:
    """Adds support for custom fields and tags.

    Attributes:
        device (NestedDeviceRequest): Represents an object related through a ForeignKey field. On write, it accepts a
            primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (str):
        installed_module (Union['ModuleBayNestedModuleRequest', None, Unset]):
        label (Union[Unset, str]): Physical label
        position (Union[Unset, str]): Identifier to reference when renaming installed components
        description (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, ModuleBayRequestCustomFields]):
    """

    device: "NestedDeviceRequest"
    name: str
    installed_module: Union["ModuleBayNestedModuleRequest", None, Unset] = UNSET
    label: Union[Unset, str] = UNSET
    position: Union[Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "ModuleBayRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.module_bay_nested_module_request import (
            ModuleBayNestedModuleRequest,
        )

        device = self.device.to_dict()

        name = self.name

        installed_module: Union[Dict[str, Any], None, Unset]
        if isinstance(self.installed_module, Unset):
            installed_module = UNSET
        elif isinstance(self.installed_module, ModuleBayNestedModuleRequest):
            installed_module = self.installed_module.to_dict()
        else:
            installed_module = self.installed_module

        label = self.label

        position = self.position

        description = self.description

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "device": device,
                "name": name,
            }
        )
        if installed_module is not UNSET:
            field_dict["installed_module"] = installed_module
        if label is not UNSET:
            field_dict["label"] = label
        if position is not UNSET:
            field_dict["position"] = position
        if description is not UNSET:
            field_dict["description"] = description
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.module_bay_nested_module_request import (
            ModuleBayNestedModuleRequest,
        )
        from ..models.module_bay_request_custom_fields import (
            ModuleBayRequestCustomFields,
        )
        from ..models.nested_device_request import NestedDeviceRequest
        from ..models.nested_tag_request import NestedTagRequest

        d = src_dict.copy()
        device = NestedDeviceRequest.from_dict(d.pop("device"))

        name = d.pop("name")

        def _parse_installed_module(
            data: object,
        ) -> Union["ModuleBayNestedModuleRequest", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                installed_module_type_1 = ModuleBayNestedModuleRequest.from_dict(data)

                return installed_module_type_1
            except:  # noqa: E722
                pass
            return cast(Union["ModuleBayNestedModuleRequest", None, Unset], data)

        installed_module = _parse_installed_module(d.pop("installed_module", UNSET))

        label = d.pop("label", UNSET)

        position = d.pop("position", UNSET)

        description = d.pop("description", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ModuleBayRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ModuleBayRequestCustomFields.from_dict(_custom_fields)

        module_bay_request = cls(
            device=device,
            name=name,
            installed_module=installed_module,
            label=label,
            position=position,
            description=description,
            tags=tags,
            custom_fields=custom_fields,
        )

        module_bay_request.additional_properties = d
        return module_bay_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
