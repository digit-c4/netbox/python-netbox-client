from enum import Enum


class NestedHostState(str, Enum):
    CREATED = "created"
    DELETED = "deleted"
    REFRESHING = "refreshing"
    RUNNING = "running"

    def __str__(self) -> str:
        return str(self.value)
