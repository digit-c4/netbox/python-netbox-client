import datetime
from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field
from dateutil.parser import isoparse

from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.nested_l2vpn_termination import NestedL2VPNTermination
    from ..models.nested_tag import NestedTag
    from ..models.nested_virtual_machine import NestedVirtualMachine
    from ..models.nested_vlan import NestedVLAN
    from ..models.nested_vm_interface import NestedVMInterface
    from ..models.nested_vrf import NestedVRF
    from ..models.vm_interface_custom_fields import VMInterfaceCustomFields
    from ..models.vm_interface_mode import VMInterfaceMode


T = TypeVar("T", bound="VMInterface")


@_attrs_define
class VMInterface:
    """Adds support for custom fields and tags.

    Attributes:
        id (int):
        url (str):
        display (str):
        virtual_machine (NestedVirtualMachine): Represents an object related through a ForeignKey field. On write, it
            accepts a primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        name (str):
        l2vpn_termination (Union['NestedL2VPNTermination', None]):
        created (Union[None, datetime.datetime]):
        last_updated (Union[None, datetime.datetime]):
        count_ipaddresses (int):
        count_fhrp_groups (int):
        enabled (Union[Unset, bool]):
        parent (Union['NestedVMInterface', None, Unset]):
        bridge (Union['NestedVMInterface', None, Unset]):
        mtu (Union[None, Unset, int]):
        mac_address (Union[None, Unset, str]):
        description (Union[Unset, str]):
        mode (Union[Unset, VMInterfaceMode]):
        untagged_vlan (Union['NestedVLAN', None, Unset]):
        tagged_vlans (Union[Unset, List[int]]):
        vrf (Union['NestedVRF', None, Unset]):
        tags (Union[Unset, List['NestedTag']]):
        custom_fields (Union[Unset, VMInterfaceCustomFields]):
    """

    id: int
    url: str
    display: str
    virtual_machine: "NestedVirtualMachine"
    name: str
    l2vpn_termination: Union["NestedL2VPNTermination", None]
    created: Union[None, datetime.datetime]
    last_updated: Union[None, datetime.datetime]
    count_ipaddresses: int
    count_fhrp_groups: int
    enabled: Union[Unset, bool] = UNSET
    parent: Union["NestedVMInterface", None, Unset] = UNSET
    bridge: Union["NestedVMInterface", None, Unset] = UNSET
    mtu: Union[None, Unset, int] = UNSET
    mac_address: Union[None, Unset, str] = UNSET
    description: Union[Unset, str] = UNSET
    mode: Union[Unset, "VMInterfaceMode"] = UNSET
    untagged_vlan: Union["NestedVLAN", None, Unset] = UNSET
    tagged_vlans: Union[Unset, List[int]] = UNSET
    vrf: Union["NestedVRF", None, Unset] = UNSET
    tags: Union[Unset, List["NestedTag"]] = UNSET
    custom_fields: Union[Unset, "VMInterfaceCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        from ..models.nested_l2vpn_termination import NestedL2VPNTermination
        from ..models.nested_vlan import NestedVLAN
        from ..models.nested_vm_interface import NestedVMInterface
        from ..models.nested_vrf import NestedVRF

        id = self.id

        url = self.url

        display = self.display

        virtual_machine = self.virtual_machine.to_dict()

        name = self.name

        l2vpn_termination: Union[Dict[str, Any], None]
        if isinstance(self.l2vpn_termination, NestedL2VPNTermination):
            l2vpn_termination = self.l2vpn_termination.to_dict()
        else:
            l2vpn_termination = self.l2vpn_termination

        created: Union[None, str]
        if isinstance(self.created, datetime.datetime):
            created = self.created.isoformat()
        else:
            created = self.created

        last_updated: Union[None, str]
        if isinstance(self.last_updated, datetime.datetime):
            last_updated = self.last_updated.isoformat()
        else:
            last_updated = self.last_updated

        count_ipaddresses = self.count_ipaddresses

        count_fhrp_groups = self.count_fhrp_groups

        enabled = self.enabled

        parent: Union[Dict[str, Any], None, Unset]
        if isinstance(self.parent, Unset):
            parent = UNSET
        elif isinstance(self.parent, NestedVMInterface):
            parent = self.parent.to_dict()
        else:
            parent = self.parent

        bridge: Union[Dict[str, Any], None, Unset]
        if isinstance(self.bridge, Unset):
            bridge = UNSET
        elif isinstance(self.bridge, NestedVMInterface):
            bridge = self.bridge.to_dict()
        else:
            bridge = self.bridge

        mtu: Union[None, Unset, int]
        if isinstance(self.mtu, Unset):
            mtu = UNSET
        else:
            mtu = self.mtu

        mac_address: Union[None, Unset, str]
        if isinstance(self.mac_address, Unset):
            mac_address = UNSET
        else:
            mac_address = self.mac_address

        description = self.description

        mode: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.mode, Unset):
            mode = self.mode.to_dict()

        untagged_vlan: Union[Dict[str, Any], None, Unset]
        if isinstance(self.untagged_vlan, Unset):
            untagged_vlan = UNSET
        elif isinstance(self.untagged_vlan, NestedVLAN):
            untagged_vlan = self.untagged_vlan.to_dict()
        else:
            untagged_vlan = self.untagged_vlan

        tagged_vlans: Union[Unset, List[int]] = UNSET
        if not isinstance(self.tagged_vlans, Unset):
            tagged_vlans = self.tagged_vlans

        vrf: Union[Dict[str, Any], None, Unset]
        if isinstance(self.vrf, Unset):
            vrf = UNSET
        elif isinstance(self.vrf, NestedVRF):
            vrf = self.vrf.to_dict()
        else:
            vrf = self.vrf

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "id": id,
                "url": url,
                "display": display,
                "virtual_machine": virtual_machine,
                "name": name,
                "l2vpn_termination": l2vpn_termination,
                "created": created,
                "last_updated": last_updated,
                "count_ipaddresses": count_ipaddresses,
                "count_fhrp_groups": count_fhrp_groups,
            }
        )
        if enabled is not UNSET:
            field_dict["enabled"] = enabled
        if parent is not UNSET:
            field_dict["parent"] = parent
        if bridge is not UNSET:
            field_dict["bridge"] = bridge
        if mtu is not UNSET:
            field_dict["mtu"] = mtu
        if mac_address is not UNSET:
            field_dict["mac_address"] = mac_address
        if description is not UNSET:
            field_dict["description"] = description
        if mode is not UNSET:
            field_dict["mode"] = mode
        if untagged_vlan is not UNSET:
            field_dict["untagged_vlan"] = untagged_vlan
        if tagged_vlans is not UNSET:
            field_dict["tagged_vlans"] = tagged_vlans
        if vrf is not UNSET:
            field_dict["vrf"] = vrf
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.nested_l2vpn_termination import NestedL2VPNTermination
        from ..models.nested_tag import NestedTag
        from ..models.nested_virtual_machine import NestedVirtualMachine
        from ..models.nested_vlan import NestedVLAN
        from ..models.nested_vm_interface import NestedVMInterface
        from ..models.nested_vrf import NestedVRF
        from ..models.vm_interface_custom_fields import VMInterfaceCustomFields
        from ..models.vm_interface_mode import VMInterfaceMode

        d = src_dict.copy()
        id = d.pop("id")

        url = d.pop("url")

        display = d.pop("display")

        virtual_machine = NestedVirtualMachine.from_dict(d.pop("virtual_machine"))

        name = d.pop("name")

        def _parse_l2vpn_termination(
            data: object,
        ) -> Union["NestedL2VPNTermination", None]:
            if data is None:
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                l2vpn_termination_type_1 = NestedL2VPNTermination.from_dict(data)

                return l2vpn_termination_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedL2VPNTermination", None], data)

        l2vpn_termination = _parse_l2vpn_termination(d.pop("l2vpn_termination"))

        def _parse_created(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                created_type_0 = isoparse(data)

                return created_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        created = _parse_created(d.pop("created"))

        def _parse_last_updated(data: object) -> Union[None, datetime.datetime]:
            if data is None:
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                last_updated_type_0 = isoparse(data)

                return last_updated_type_0
            except:  # noqa: E722
                pass
            return cast(Union[None, datetime.datetime], data)

        last_updated = _parse_last_updated(d.pop("last_updated"))

        count_ipaddresses = d.pop("count_ipaddresses")

        count_fhrp_groups = d.pop("count_fhrp_groups")

        enabled = d.pop("enabled", UNSET)

        def _parse_parent(data: object) -> Union["NestedVMInterface", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                parent_type_1 = NestedVMInterface.from_dict(data)

                return parent_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVMInterface", None, Unset], data)

        parent = _parse_parent(d.pop("parent", UNSET))

        def _parse_bridge(data: object) -> Union["NestedVMInterface", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                bridge_type_1 = NestedVMInterface.from_dict(data)

                return bridge_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVMInterface", None, Unset], data)

        bridge = _parse_bridge(d.pop("bridge", UNSET))

        def _parse_mtu(data: object) -> Union[None, Unset, int]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, int], data)

        mtu = _parse_mtu(d.pop("mtu", UNSET))

        def _parse_mac_address(data: object) -> Union[None, Unset, str]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, str], data)

        mac_address = _parse_mac_address(d.pop("mac_address", UNSET))

        description = d.pop("description", UNSET)

        _mode = d.pop("mode", UNSET)
        mode: Union[Unset, VMInterfaceMode]
        if isinstance(_mode, Unset):
            mode = UNSET
        else:
            mode = VMInterfaceMode.from_dict(_mode)

        def _parse_untagged_vlan(data: object) -> Union["NestedVLAN", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                untagged_vlan_type_1 = NestedVLAN.from_dict(data)

                return untagged_vlan_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVLAN", None, Unset], data)

        untagged_vlan = _parse_untagged_vlan(d.pop("untagged_vlan", UNSET))

        tagged_vlans = cast(List[int], d.pop("tagged_vlans", UNSET))

        def _parse_vrf(data: object) -> Union["NestedVRF", None, Unset]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, dict):
                    raise TypeError()
                vrf_type_1 = NestedVRF.from_dict(data)

                return vrf_type_1
            except:  # noqa: E722
                pass
            return cast(Union["NestedVRF", None, Unset], data)

        vrf = _parse_vrf(d.pop("vrf", UNSET))

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTag.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, VMInterfaceCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = VMInterfaceCustomFields.from_dict(_custom_fields)

        vm_interface = cls(
            id=id,
            url=url,
            display=display,
            virtual_machine=virtual_machine,
            name=name,
            l2vpn_termination=l2vpn_termination,
            created=created,
            last_updated=last_updated,
            count_ipaddresses=count_ipaddresses,
            count_fhrp_groups=count_fhrp_groups,
            enabled=enabled,
            parent=parent,
            bridge=bridge,
            mtu=mtu,
            mac_address=mac_address,
            description=description,
            mode=mode,
            untagged_vlan=untagged_vlan,
            tagged_vlans=tagged_vlans,
            vrf=vrf,
            tags=tags,
            custom_fields=custom_fields,
        )

        vm_interface.additional_properties = d
        return vm_interface

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
