from typing import TYPE_CHECKING, Any, Dict, List, Type, TypeVar, Union, cast

from attrs import define as _attrs_define
from attrs import field as _attrs_field

from ..models.module_type_request_weight_unit_type_1 import (
    ModuleTypeRequestWeightUnitType1,
)
from ..models.module_type_request_weight_unit_type_2_type_1 import (
    ModuleTypeRequestWeightUnitType2Type1,
)
from ..models.module_type_request_weight_unit_type_3_type_1 import (
    ModuleTypeRequestWeightUnitType3Type1,
)
from ..types import UNSET, Unset

if TYPE_CHECKING:
    from ..models.module_type_request_custom_fields import ModuleTypeRequestCustomFields
    from ..models.nested_manufacturer_request import NestedManufacturerRequest
    from ..models.nested_tag_request import NestedTagRequest


T = TypeVar("T", bound="ModuleTypeRequest")


@_attrs_define
class ModuleTypeRequest:
    """Adds support for custom fields and tags.

    Attributes:
        manufacturer (NestedManufacturerRequest): Represents an object related through a ForeignKey field. On write, it
            accepts a primary key (PK) value or a
            dictionary of attributes which can be used to uniquely identify the related object. This class should be
            subclassed to return a full representation of the related object on read.
        model (str):
        part_number (Union[Unset, str]): Discrete part number (optional)
        weight (Union[None, Unset, float]):
        weight_unit (Union[ModuleTypeRequestWeightUnitType1, ModuleTypeRequestWeightUnitType2Type1,
            ModuleTypeRequestWeightUnitType3Type1, None, Unset]): * `kg` - Kilograms
            * `g` - Grams
            * `lb` - Pounds
            * `oz` - Ounces
        description (Union[Unset, str]):
        comments (Union[Unset, str]):
        tags (Union[Unset, List['NestedTagRequest']]):
        custom_fields (Union[Unset, ModuleTypeRequestCustomFields]):
    """

    manufacturer: "NestedManufacturerRequest"
    model: str
    part_number: Union[Unset, str] = UNSET
    weight: Union[None, Unset, float] = UNSET
    weight_unit: Union[
        ModuleTypeRequestWeightUnitType1,
        ModuleTypeRequestWeightUnitType2Type1,
        ModuleTypeRequestWeightUnitType3Type1,
        None,
        Unset,
    ] = UNSET
    description: Union[Unset, str] = UNSET
    comments: Union[Unset, str] = UNSET
    tags: Union[Unset, List["NestedTagRequest"]] = UNSET
    custom_fields: Union[Unset, "ModuleTypeRequestCustomFields"] = UNSET
    additional_properties: Dict[str, Any] = _attrs_field(init=False, factory=dict)

    def to_dict(self) -> Dict[str, Any]:
        manufacturer = self.manufacturer.to_dict()

        model = self.model

        part_number = self.part_number

        weight: Union[None, Unset, float]
        if isinstance(self.weight, Unset):
            weight = UNSET
        else:
            weight = self.weight

        weight_unit: Union[None, Unset, str]
        if isinstance(self.weight_unit, Unset):
            weight_unit = UNSET
        elif isinstance(self.weight_unit, ModuleTypeRequestWeightUnitType1):
            weight_unit = self.weight_unit.value
        elif isinstance(self.weight_unit, ModuleTypeRequestWeightUnitType2Type1):
            weight_unit = self.weight_unit.value
        elif isinstance(self.weight_unit, ModuleTypeRequestWeightUnitType3Type1):
            weight_unit = self.weight_unit.value
        else:
            weight_unit = self.weight_unit

        description = self.description

        comments = self.comments

        tags: Union[Unset, List[Dict[str, Any]]] = UNSET
        if not isinstance(self.tags, Unset):
            tags = []
            for tags_item_data in self.tags:
                tags_item = tags_item_data.to_dict()
                tags.append(tags_item)

        custom_fields: Union[Unset, Dict[str, Any]] = UNSET
        if not isinstance(self.custom_fields, Unset):
            custom_fields = self.custom_fields.to_dict()

        field_dict: Dict[str, Any] = {}
        field_dict.update(self.additional_properties)
        field_dict.update(
            {
                "manufacturer": manufacturer,
                "model": model,
            }
        )
        if part_number is not UNSET:
            field_dict["part_number"] = part_number
        if weight is not UNSET:
            field_dict["weight"] = weight
        if weight_unit is not UNSET:
            field_dict["weight_unit"] = weight_unit
        if description is not UNSET:
            field_dict["description"] = description
        if comments is not UNSET:
            field_dict["comments"] = comments
        if tags is not UNSET:
            field_dict["tags"] = tags
        if custom_fields is not UNSET:
            field_dict["custom_fields"] = custom_fields

        return field_dict

    @classmethod
    def from_dict(cls: Type[T], src_dict: Dict[str, Any]) -> T:
        from ..models.module_type_request_custom_fields import (
            ModuleTypeRequestCustomFields,
        )
        from ..models.nested_manufacturer_request import NestedManufacturerRequest
        from ..models.nested_tag_request import NestedTagRequest

        d = src_dict.copy()
        manufacturer = NestedManufacturerRequest.from_dict(d.pop("manufacturer"))

        model = d.pop("model")

        part_number = d.pop("part_number", UNSET)

        def _parse_weight(data: object) -> Union[None, Unset, float]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            return cast(Union[None, Unset, float], data)

        weight = _parse_weight(d.pop("weight", UNSET))

        def _parse_weight_unit(
            data: object,
        ) -> Union[
            ModuleTypeRequestWeightUnitType1,
            ModuleTypeRequestWeightUnitType2Type1,
            ModuleTypeRequestWeightUnitType3Type1,
            None,
            Unset,
        ]:
            if data is None:
                return data
            if isinstance(data, Unset):
                return data
            try:
                if not isinstance(data, str):
                    raise TypeError()
                weight_unit_type_1 = ModuleTypeRequestWeightUnitType1(data)

                return weight_unit_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                weight_unit_type_2_type_1 = ModuleTypeRequestWeightUnitType2Type1(data)

                return weight_unit_type_2_type_1
            except:  # noqa: E722
                pass
            try:
                if not isinstance(data, str):
                    raise TypeError()
                weight_unit_type_3_type_1 = ModuleTypeRequestWeightUnitType3Type1(data)

                return weight_unit_type_3_type_1
            except:  # noqa: E722
                pass
            return cast(
                Union[
                    ModuleTypeRequestWeightUnitType1,
                    ModuleTypeRequestWeightUnitType2Type1,
                    ModuleTypeRequestWeightUnitType3Type1,
                    None,
                    Unset,
                ],
                data,
            )

        weight_unit = _parse_weight_unit(d.pop("weight_unit", UNSET))

        description = d.pop("description", UNSET)

        comments = d.pop("comments", UNSET)

        tags = []
        _tags = d.pop("tags", UNSET)
        for tags_item_data in _tags or []:
            tags_item = NestedTagRequest.from_dict(tags_item_data)

            tags.append(tags_item)

        _custom_fields = d.pop("custom_fields", UNSET)
        custom_fields: Union[Unset, ModuleTypeRequestCustomFields]
        if isinstance(_custom_fields, Unset):
            custom_fields = UNSET
        else:
            custom_fields = ModuleTypeRequestCustomFields.from_dict(_custom_fields)

        module_type_request = cls(
            manufacturer=manufacturer,
            model=model,
            part_number=part_number,
            weight=weight,
            weight_unit=weight_unit,
            description=description,
            comments=comments,
            tags=tags,
            custom_fields=custom_fields,
        )

        module_type_request.additional_properties = d
        return module_type_request

    @property
    def additional_keys(self) -> List[str]:
        return list(self.additional_properties.keys())

    def __getitem__(self, key: str) -> Any:
        return self.additional_properties[key]

    def __setitem__(self, key: str, value: Any) -> None:
        self.additional_properties[key] = value

    def __delitem__(self, key: str) -> None:
        del self.additional_properties[key]

    def __contains__(self, key: str) -> bool:
        return key in self.additional_properties
