# ec-netbox-openapi
A client library for accessing NetBox REST API

## Usage
First, create a client:

```python
from ec_netbox import ECNetboxClient

client = ECNetboxClient()
```


Now call your endpoint and use your models:

```python
from ec_netbox_openapi.models import VirtualMachineWithConfigContext
from ec_netbox_openapi.api.virtualization import (
    virtualization_virtual_machines_list
)

with client as client:
    vms = virtualization_virtual_machines_list.sync(client=client)
    print(vms)
```

Or do the same thing with an async version:

```python
from ec_netbox_openapi.models import VirtualMachineWithConfigContext
from ec_netbox_openapi.api.virtualization import (
    virtualization_virtual_machines_list
)

async with client as client:
    vms = await virtualization_virtual_machines_list.asyncio(client=client)
```

By default, when you're calling an HTTPS API it will attempt to verify that SSL is working correctly. Using certificate verification is highly recommended most of the time, but sometimes you may need to authenticate to a server (especially an internal server) using a custom certificate bundle.

```python
client = ECNetboxClient(
    verify_ssl="/path/to/certificate_bundle.pem",
)
```

You can also disable certificate validation altogether, but beware that **this is a security risk**.

```python
client = ECNetboxClient(
    verify_ssl=False,
)
```

Things to know:
1. Every path/method combo becomes a Python module with four functions:
    1. `sync`: Blocking request that returns parsed data (if successful) or `None`
    1. `sync_detailed`: Blocking request that always returns a `Request`, optionally with `parsed` set if the request was successful.
    1. `asyncio`: Like `sync` but async instead of blocking
    1. `asyncio_detailed`: Like `sync_detailed` but async instead of blocking

1. All path/query params, and bodies become method arguments.
1. If your endpoint had any tags on it, the first tag will be used as a module name for the function (my_tag above)
1. Any endpoint which did not have a tag will be in `ec_netbox_openapi.api.default`

To enable logging of API requests and responses, configure the Python logging
framework accordingly:

```python
import sys

from logbook import Logger, StreamHandler

from ec_netbox import ECNetboxClient


logger = Logger("ec_netbox")
client = ECNetboxClient(logger=logger)

with StreamHandler(sys.stderr, level="DEBUG").applicationbound():
    with client as client:
        ...
```
