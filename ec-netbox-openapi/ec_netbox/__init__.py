from typing import Any, Optional

import os

from httpx import Request, Response
from httpx._client import AsyncClient, Client
from logbook import Logger

from ec_netbox_openapi import AuthenticatedClient as BaseClient


class ECNetboxClient(BaseClient):
    def __init__(self, *, logger: Optional[Logger] = None, **kwargs: Any):
        if not kwargs.get("base_url"):
            kwargs["base_url"] = os.getenv("NETBOX_URL")

        if not kwargs.get("token"):
            kwargs["token"] = os.getenv("NETBOX_TOKEN")

        if not kwargs.get("prefix"):
            kwargs["prefix"] = "Token"

        super().__init__(**kwargs)

        self._logger = logger

    def get_httpx_client(self) -> Client:
        client = super().get_httpx_client()
        client._event_hooks["request"] = [self._log_request]
        client._event_hooks["response"] = [
            Response.read,
            self._log_response,
        ]
        return client

    def get_async_httpx_client(self) -> AsyncClient:
        client = super().get_async_httpx_client()
        client._event_hooks["request"] = [self._async_log_request]
        client._event_hooks["response"] = [
            Response.aread,
            self._async_log_response,
        ]
        return client

    def _log_request(self, request: Request) -> None:
        assert request
        if self._logger is not None:
            self._logger.info(
                "netbox request",
                extra={
                    "request.method": request.method,
                    "request.url": request.url,
                },
            )

    async def _async_log_request(self, request: Request) -> None:
        self._log_request(request)

    def _log_response(self, response: Response) -> None:
        if self._logger is not None:
            extra = {
                "request.method": response.request.method,
                "request.url": response.request.url,
                "response.status_code": response.status_code,
                "response.reason_phrase": response.reason_phrase,
            }

            if response.is_error:
                log = self._logger.error
                extra["response.body"] = response.text

            else:
                log = self._logger.info

            log("netbox response", extra=extra)

    async def _async_log_response(self, response: Response) -> None:
        self._log_response(response)
