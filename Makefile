.PHONY: all
all:
	@openapi-python-client generate \
		--path ./openapi.json \
		--config ./codegen.yml \
		--custom-template-path=./templates/ \
		--meta=none \
		--output-path=ec-netbox-openapi/ec_netbox_openapi \
		--overwrite

.PHONY: bootstrap
bootstrap:
	@openapi-python-client generate \
		--path ./openapi.json \
		--config ./codegen.yml \
		--custom-template-path=./templates/
